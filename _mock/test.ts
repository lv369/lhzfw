import { MockRequest, MockStatusError } from '@delon/mock';

const r = (min: number, max: number) => Math.floor(Math.random() * (max - min + 1) + min);

export const TEST={
  'GET /test':(req:MockRequest)=>{
    const total=+(req.queryString.total || 100);
    const res:any={
      list:[],
      total
    };
    const onlyList=req.queryString!.field==='list';
    let num=onlyList? total : +req.queryString.ps;
    if (isNaN(num) || num <= 0) {
      num = total;
    }
    for(let i=0;i<num;i++)
    {
      res.list.push({
        ake035:i,
        ake036:"",
        ake039:new Date(),
        ckf001:`name${i}`,
        ake024:r(1000000,1000000000),
        aka010:r(1000000,1000000000),
        ake027:['自身疾病','住院康复','其他'][i%3],
        aae00: `疾病编码${r(0,100)}`,
        ake021:`疾病${r(0,100)}`,
        ake022:`${r(0,100)}病`,
        aae035:r(0,100),
        aae013:r(1000000000,100000000000),
      })
    }
    return onlyList? res.list:res;
  }
}