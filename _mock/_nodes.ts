import { MockRequest } from '@delon/mock';


export const NODES = {
  'GET /nodes':(req:MockRequest)=>{
    const json={
      "main":[   // 主分支 节点数组
        {
          "id":"1",  // id唯一  类型 string
          "text":"1", // 文字
          "state":2  //状态 0:已完成 1:正在进行 2:未进行
        },
        {
          "id":"2",
          "text":"2",
          "state":1
        },
        {
          "id":"3",
          "text":"3",
          "state":0
        }
      ],
      "branch":[    // 分支
        {
          "start":"2",  // 开始分支的id
          "end":"end",  // 结束分支的id 默认为终点id
          "nodes":[     // 节点数组
            {
              "id":"4",
              "text":"4",
              "state":2
            },
            {
              "id":"5",
              "text":"5",
              "state":1
            },
            {
              "id":"6",
              "text":"6",
              "state":0
            }
          ]
        },
        {
          "start":"1",
          "end":"end",
          "nodes":[
            {
              "id":"7",
              "text":"7",
              "state":0
            }
          ]
        },
        {
          "start":"3",
          "end":"end",
          "nodes":[
            {
              "id":"8",
              "text":"8",
              "state":0
            }
          ]
        }
      ]
    }
      return json;
  }
}