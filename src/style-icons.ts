// Custom icon static resources

import {
  InfoOutline,
  BulbOutline,
  ProfileOutline,
  ExceptionOutline,
  LinkOutline,
  FolderOutline,
  FileZipOutline,
  FileSearchOutline,
  BookOutline,
  DashboardOutline,
  CodepenOutline,
  PayCircleOutline,
  DownloadOutline,
} from '@ant-design/icons-angular/icons';

export const ICONS = [
  InfoOutline,
  BulbOutline,
  ProfileOutline,
  ExceptionOutline,
  LinkOutline,
  FolderOutline,
  FileZipOutline,
  BookOutline,
  FileSearchOutline,
  DashboardOutline,
  CodepenOutline,
  PayCircleOutline,
  DownloadOutline
];
