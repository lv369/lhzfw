import { Injectable } from '@angular/core';
import { HttpService } from 'lbf';


@Injectable({
  providedIn: 'root',
})
export class RepserviceService {
  constructor(private lbservice: HttpService) {}

  url = null;

  getRep(repid: string, param: any) {
    let urlparam = '';
    return new Promise((fulfill, reject) => {
      this.lbservice
        .lbservice('repurl_query', { para: { REPID: repid } })
        .then(resdata => {
          if (resdata.code === 1) {
            for (const key of Object.keys(param)) {
              if (param.hasOwnProperty(key)) {
                urlparam = urlparam + '&' + key + '=' + param[key];
                console.log('urlparam===' + urlparam);
              }
            }
            this.url = resdata.message.URL + urlparam;
          } else {
            this.url = null;
          }
          fulfill(this.url);
        });
    });
  }

}
