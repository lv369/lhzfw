import { Injectable } from '@angular/core';
import * as dayjs from "dayjs";
import * as isLeapYear from 'dayjs/plugin/isLeapYear'
import * as dayOfYear from 'dayjs/plugin/dayOfYear'


@Injectable({ providedIn: 'root' })
export class SupDate {

  constructor() {
    // dayjs.extend(isLeapYear)
    // dayjs.extend(dayOfYear)
  }

  /**
   * 是否闰年
   * @param year 年份 
   */
  isLeapYear(year?: number): boolean {

    const date = dayjs();
    if (year) {
      date.year(year);
    }

    return date.isLeapYear();
  }

  /**
   * 年中第几天
   * @param day 日期
   */
  getDayinYear(day?: Date): number {

    let date = dayjs();
    if (day) {
      date = dayjs(day);
    }

    return date.dayOfYear();

  }

  /**
   * 一年多少天
   * @param year 年份
   */
  getYearDays(year?: number): number {

    if (this.isLeapYear(year)) {
      return 366
    }
    else {
      return 365;
    }

  }

  /**
   * 今年
   */
  getThisYear(): number {
    const date = dayjs();
    return date.year();
  }

  /**
   * 去年 
   * @param year 减几年，默认1 去年
   */
  getLastYear(v?:number): number {
    if(!v){
      v = 1;
    }
    return this.getThisYear() - v;
  }

}