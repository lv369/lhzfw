import { Injectable } from '@angular/core';
import { webSocket, WebSocketSubject } from 'rxjs/webSocket';
import { ServiceInfo } from 'lbf';
import { interval, Subject, Observable } from 'rxjs';
import { LayoutProWidgetNotifyComponent } from '@brand/components/notify/notify.component';

@Injectable({ providedIn: 'root' })
export class WsMessageService {


  private logtoken: any;

  private ws: WebSocketSubject<any>;
  private keepLive;
  // public showMessageBox = false; // 是否显示收件箱
  public notify:LayoutProWidgetNotifyComponent; //消息模组

  constructor(sinfo: ServiceInfo) {
    this.logtoken = sinfo.LOGTOKEN;
  }

  // 将收件框对象赋值进来
  setNotify(notify:LayoutProWidgetNotifyComponent){
    this.notify = notify;
    console.log('----getNotify------')
    console.log(this.notify)
  }

  // 是否显示收件框
  showNotify(flag:boolean){
    if(flag){
      this.notify.click();
    }
    else{
      this.notify.close();
    }
    
  }

  /**
   * 获取消息websocket地址
   *  通过 ws+ip+端口+ws+message.ws 拼接
   */
  getWsUrl() {
    // TODO 实现拼接
    return "ws://10.49.15.3/ws/message.ws";
  }


  /**
   * 连接
   */
  login(logintoken?: string) {

    if (logintoken) {
      this.logtoken = logintoken;
    }

    if (!this.ws) {
      const close$ = new Subject();
      this.ws = webSocket({ url: this.getWsUrl(), closeObserver: close$ });
      // 如果收到断线请求，自动重连
      // 如果多次重连失败？
      close$.subscribe((event) => {
        this.ws = webSocket({ url: this.getWsUrl(), closeObserver: close$ });
        this.ws.next({ type: "LOGIN", loginid: this.logtoken });
      })
    }

    if (!this.logtoken) return;

    const login$ = this.ws.multiplex(
      () => ({ type: "TEST", loginid: '', message: {} }),
      () => ({ type: "TEST", loginid: '', message: {} }),
      message => message.type === 'LOGIN'
    );

    login$.subscribe(message => {

      console.log('login!', message)

      if (message.message.code > 0) {
        this.keepLive = interval(30000);
        this.keepLive.pipe().subscribe(x => this.ws.next({ type: "KeepLive", loginid: this.logtoken }))
      }
    });

    this.ws.next({ type: "LOGIN", loginid: this.logtoken });


  }

  getMessage(): Observable<any> {

    if (!this.ws) {
      this.login();
    }

    const msg$ = this.ws.multiplex(
      () => ({ type: "TEST", loginid: '', message: {} }),
      () => ({ type: "TEST", loginid: '', message: {} }),
      message => message.type === 'MSG'
    );
    msg$.subscribe(message => console.log(message));

    return msg$;

  }




}