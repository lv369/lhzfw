import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { HttpService, ServiceInfo } from 'lbf';
import { NzMessageService } from 'ng-zorro-antd';
import { Injectable } from '@angular/core';
import { stat } from 'fs';

@Injectable({ providedIn: 'root' })
export class RoleGuard implements CanActivate {

  constructor(private lbservice: HttpService, private message: NzMessageService, private sinfo: ServiceInfo, private router: Router) {

  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    return new Promise((resolve, reject) => {
      this.lbservice.lbservice('FR_route', { para: { URL: state.url } }, 0, true).then(resdata => {

        if (resdata.code < 1) {
          this.message.error(resdata.errmsg);
          resolve(false);
        } else {
          //  todo 不同情况下的处理
          if (!resdata.message.pass) {
            if (resdata.message.goto) {
              resolve(this.router.parseUrl(resdata.message.goto));
            } else {
              this.message.error(resdata.message.err ? resdata.message.err : "权限不足");
              resolve(false);
            }
          } else {
            resolve(true);
          }
        }
      })
    });
  }
}