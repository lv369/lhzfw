import { Injectable } from '@angular/core';
import { Subject, BehaviorSubject } from 'rxjs';
import { filter } from 'rxjs/operators';
import { url } from 'inspector';
import { Router } from '@angular/router';

@Injectable({ providedIn: 'root' })
export class RefService {

  private refMessageSub = new BehaviorSubject<RefMessage>(null);

  constructor(private routers: Router, ) {
    this.refMessageSub.subscribe(msg => {
      if(msg){
        this.refMessageSub.next(null);
      }});
  }

  /**
   * 发送关联事项到第三方
   * @param ref 关联内容
   */
  sendRef(ref: RefMessage, isGoto: boolean = false) {
    if (ref.isGoto && ref.url) {
      this.routers.navigate([ref.url], { queryParams: ref.message });
    }
    
    this.refMessageSub.next(ref);


  }

  /**
   * 获取关联订阅
   * @param recive 接受者
   */
  getRef(recive: string) {

    return this.refMessageSub.pipe(filter(msg => msg&&msg.receive === recive))

  }


}


interface RefMessage {

  /**
   * 是否跳转
   */
  isGoto?: boolean
  /**
   * 跳转地址
   */
  url?: string;
  /**
   * 接收地址
   */
  receive: string;
  /**
   * 发送人
   */
  sender?: string;
  /**
   * 通知内容
   */
  message: any;

  /**
   * 通知时间
   */
  createTime: Date;


}