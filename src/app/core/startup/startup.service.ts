import { Injectable, Inject, Injector } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {
  MenuService,
  SettingsService,
  TitleService,
  ALAIN_I18N_TOKEN,
} from '@delon/theme';
// import { I18NService } from '../i18n/i18n.service';

import { NzIconService } from 'ng-zorro-antd';
import { ICONS_AUTO } from '../../../style-icons-auto';
import { ICONS } from '../../../style-icons';
import { ServiceInfo, HttpService, SupDic } from 'lbf';
import { DA_SERVICE_TOKEN, ITokenService } from '@delon/auth';
import { Router } from '@angular/router';
import { ACLService } from '@delon/acl';
import { WsMessageService } from '@core/lb/MessageService';
import { PlatformLocation } from '@angular/common';

/**
 * 用于应用启动时
 * 一般用来获取应用所需要的基础数据等
 */
@Injectable()
export class StartupService {
  constructor(
    iconSrv: NzIconService,
    private location: PlatformLocation,
    private menuService: MenuService,
    //  @Inject(ALAIN_I18N_TOKEN) private i18n: I18NService,
    @Inject(DA_SERVICE_TOKEN) private tokenService: ITokenService,
    private injector: Injector,
    private settingService: SettingsService,
    private titleService: TitleService,
    private serviceInfo: ServiceInfo,
    private aclService: ACLService,
    private lbservice: HttpService,
    private supdic: SupDic,
    private httpclient: HttpClient,
    private msgWs:WsMessageService,
  ) {
    iconSrv.addIcon(...ICONS_AUTO, ...ICONS);
    iconSrv.fetchFromIconfont({scriptUrl:'../../../assets/js/iconfont.js'});
    serviceInfo.setSinfo({ APPID: 'DA2020', APPKEY: 'DA20202020', VERSION: "20200308", APPNAME: '临海市社会矛盾纠纷调处化解中心' });
    serviceInfo.molssUrl = "/da/service";

  }

  private goTo(url: string) {
    setTimeout(() => this.injector.get(Router).navigateByUrl(url));
  }

  async load(): Promise<any> {



    // 应用信息：包括站点名、描述、年份
    this.settingService.setApp(this.serviceInfo.APPINFO);
    // 设置页面标题的后缀
    this.titleService.suffix = this.serviceInfo.APPSUFFIX;

    // 加载应用信息
    this.serviceInfo.getToken();

    // 读取用户信息
    this.serviceInfo.loaduser();

    if (this.serviceInfo.user && this.serviceInfo.user.roleid) {
      this.aclService.setRole([this.serviceInfo.user.roleid]);

      this.tokenService.set({
        token: this.serviceInfo.LOGTOKEN,
        name: this.serviceInfo.user.name,
        aac003: this.serviceInfo.user.aac003,
        aaf001: this.serviceInfo.user.dept,
        time: +new Date(),
      });

      if(this.serviceInfo.LOGTOKEN){
        console.log('login。。。')
        console.log(location.href);
         this.msgWs.login(this.serviceInfo.LOGTOKEN);
      }

    }

    




    // 加载字典
    await this.supdic.loadAllDic();

   /*
    this.httpclient.get('assets/tmp/KA03.json').subscribe(ka03 => {
      this.supdic.ka010203 = ka03;
    })
   */

    return new Promise((resolve, reject) => {
      if(this.serviceInfo.LOGTOKEN != undefined && this.serviceInfo.LOGTOKEN.length>0){
        this.lbservice.lbservice("FR_memu", {}).then((resdata) => {

          if (resdata.code > 0) {
            // 初始化菜单
            this.menuService.add(resdata.message.list);
  
          } else {
  
            this.goTo('/passport/login');
  
          }
  
          resolve(null);
  
        })
      }else{
        resolve(null);
      }


    });
  }
}
