import { OnInit, OnChanges, OnDestroy, Component, ChangeDetectionStrategy, ViewEncapsulation, Input, TemplateRef, NgZone, ViewChild, ElementRef } from '@angular/core';
import { InputNumber, InputBoolean } from 'ng-zorro-antd';
import { Lbchart } from '@shared';
import { Subscription } from 'rxjs';
/**
 * 词云图
 */
@Component({
  selector: 'lb-basic-wordcloud',
  template: `
  <div #basicwordcloud></div>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush, // 检测改为按需，即不自动刷新
})
export class LbBasicWordcloudComponent implements OnInit, OnChanges, OnDestroy,Lbchart {
  private chart: any;
  private resize$: Subscription;
  @ViewChild('basicwordcloud', { static: true }) private node: ElementRef;
  @Input() padding: Array<number | string> | string = 'auto';
  @Input() @InputNumber() delay = 0;
  @Input() color:string[] = null;
  @Input() colorFiled='name';
  @Input() position: any

  @Input() data: any []

  // @Input() width: number;
  @Input() height: number;

  @Input() @InputBoolean() tooltipshow = true;
  @Input() @InputBoolean() tooltipshowtitle = false

  @Input() legendposition;
  @Input() legendlayout;
  @Input() @InputBoolean() legendshow = true;

  @Input() sortx = true
  @Input() sortz
  @Input() sortrule = 'ASC'

  constructor(private ngZone: NgZone) { }

  commonStyleHandle(chart, data) {
    // 计算百分比
    const dv = new DataSet.View().source(data);
    // if(this.sortx && !this.sortz) {
    //   dv.transform({
    //     type: 'sort-by',
    //     fields: [ this.sortx ], // 根据指定的字段集进行排序，与lodash的sortBy行为一致
    //     order: this.sortrule,        // 默认为 ASC，DESC 则为逆序
    //   })
    // } else if(!this.sortx && this.sortz){
    //   dv.transform({
    //     type: 'sort-by',
    //     fields: [ this.sortz ], // 根据指定的字段集进行排序，与lodash的sortBy行为一致
    //     order: this.sortrule,   // 默认为 ASC，DESC 则为逆序
    //   })
    // } else if(this.sortx && this.sortz) {
    //   dv.transform({
    //     type: 'sort-by',
    //     fields: [ this.sortx, this.sortz ], // 根据指定的字段集进行排序，与lodash的sortBy行为一致
    //     order: this.sortrule,   // 默认为 ASC，DESC 则为逆序
    //   })
    // }    
    const range = dv.range(this.position.y);
    const min = range[0];
    const max = range[1];
    dv.transform({
      type: 'tag-cloud',
      fields: [ this.position.x, this.position.y],
      size: [ 600, 400 ],
      font: 'Verdana',
      padding: 0,
      timeInterval: 5000, // max execute time
      rotate() {
        let random = ~~(Math.random() * 4) % 4;
        if (random === 2) {
          random = 0;
        }
        return random * 90; // 0, 90, 270
      },
      fontSize(d) {
        if (d.value) {
          return ((d.value - min) / (max - min)) * (32 - 12) + 12;
        }
        return 0;
      }
    });

    // Step 2: 载入数据源
    chart.source(dv, {
      x: {
        nice: false,
      },
      y: {
        nice: false,
      }
    });

    chart.legend(false);
    chart.axis(false);
    // 工具提示
    if (this.tooltipshow) {
      chart.tooltip({
        showTitle: this.tooltipshowtitle,
        // itemTpl: '<li><span style="background-color:{color};" class="g2-tooltip-marker"></span>{name}: {value}</li>'
      });
    } else {
      chart.tooltip(false);
    }
    
    chart.coord().reflect();

    return dv.rows;
  }

  install () {
    this.render()
  }

  render() {
        
    // 给point注册一个词云的shape
G2.Shape.registerShape('point', 'cloud', {
  drawShape(cfg, container) {
    const attrs = {
    ...cfg.style,

    fillOpacity: cfg.opacity,
    fontSize: cfg.origin._origin.size,
    rotate: cfg.origin._origin.rotate,
    text: cfg.origin._origin.text,
    textAlign: 'center',
    fontFamily: cfg.origin._origin.font,
    fill: cfg.color,
    textBaseline: 'Alphabetic'
  }
    return container.addShape('text', {
      attrs: {
        ...attrs,
        x: cfg.x,
        y: cfg.y
      }
    });
  }
});


    const { node, padding } = this;
    const container = node.nativeElement as HTMLElement;

    const chart = (this.chart = new G2.Chart({
      container,
      forceFit: true,  // 图表的宽度自适应开关
      height: this.getHeight(),
      padding,
    }));
  
      this.commonStyleHandle(chart, this.data)
      chart.point()
      .position('x*y')
      .color(this.position.z)
      .shape('cloud')
      .tooltip(`${this.position.y}*${this.position.z}`);
  
      // Step 4: 渲染图表
      chart.render();
  }

   attachChart() {
    const { chart, padding, data, color } = this;
    if (!chart || !data || data.length <= 0) return;
    const height = this.getHeight();
    if (chart.get('height') !== height) {
      chart.changeHeight(height);
    }
  const dvrows =   this.commonStyleHandle(chart, data)
     // color
    //  chart.get('geoms')[0].color(this.colorFiled,this.color);
     chart.set('padding', padding);
     chart.changeData(dvrows);
  }

  destroy(){
    if (this.resize$) {
      this.resize$.unsubscribe();
    }
    if (this.chart) {
      this.ngZone.runOutsideAngular(() => this.chart.destroy());
    }
  }

  /**
   * 报表高度
   * 存在报表头，则需要减掉报表头的高度
   */
   getHeight() {
     return this.height
    // return this.title ? (this.height - TITLE_HEIGHT) : this.height;
  }


  ngOnInit(): void {
    if(this.position&&this.data.length>0){
      this.ngZone.runOutsideAngular(() => setTimeout(() => this.install(), this.delay));
    }
   
  }  
  
  ngOnChanges(changes: import("@angular/core").SimpleChanges): void {
    this.ngZone.runOutsideAngular(() => this.attachChart());
  }
  ngOnDestroy(): void {
    this.destroy();
  }
}