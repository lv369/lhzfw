import { Component, Input, TemplateRef, ViewChild, ElementRef, Renderer2, NgZone, ChangeDetectorRef, OnDestroy, OnChanges, OnInit } from '@angular/core';
import { InputNumber , InputBoolean} from 'ng-zorro-antd';
import { Subscription, fromEvent } from 'rxjs';
import { debounceTime } from 'rxjs/operators';

@Component({
  selector: 'lb-water-wave',
  templateUrl: './lb-water-wave.component.html',
  host: { '[class.g2-water-wave]': 'true' },
})
export class LbWaterWaveComponent implements OnDestroy, OnChanges, OnInit {
  private chart: any;
  private resize$: Subscription | null = null;
  @ViewChild('waterwave', { static: true }) private node: ElementRef;
  @Input() padding: Array<number | string> | string = 'auto';
  @Input() @InputNumber() delay = 0;
  // @Input() color: string[] = null;
  @Input() colorFiled = 'name';
  @Input() position: any;

  // @Input() style: { [key: string]: string };
  // @Input() position: { x: string, y: string, z: string } = { x: 'adc', y: 'sold', z: 'type' }

  @Input() data: any[];

  @Input() height: number;

  @Input() @InputBoolean() centertitleshow = false; // 是否显示内部标题
  @Input() centertitletext; // 内部标题
  @Input() @InputNumber() centertitletextsize
  @Input() @InputBoolean() centertitleshowtotal = false;
  @Input() @InputNumber() centertitletotalsize


  // private resize$: Subscription | null = null;
  private timer: number;

  // // #region fields

  // @Input() @InputNumber() delay = 0;
  // @Input() centertitletext: string | TemplateRef<void>;
  @Input() color = '#1890FF';
  // @Input() @InputNumber() height = 160;
  // @Input() @InputNumber() percent: number;

  // #endregion

  constructor(private el: ElementRef, private renderer: Renderer2, private ngZone: NgZone, private cdr: ChangeDetectorRef) { }

 
 
  install(resdata){
    const tb = resdata.message.tb;
    let DAA040
    try {
      DAA040 = JSON.parse(tb.DAA040);
    } catch(err ) {
      DAA040 = {}
    }

    this.centertitletext=tb.DAA031;
    // this.percent =  Math.round(a / b * 100);
   
    if (DAA040.color)
      this.color = DAA040.color;

    this.height = DAA040.height;


    this.ngZone.runOutsideAngular(() => setTimeout(() => this.renderChart(''), this.delay));

  }

  attachChart(resdata){

    const tb = resdata.message.tb;
    let DAA040: any
    try {
      DAA040 = JSON.parse(tb.DAA040);
    } catch(err) {
      DAA040 = {}
      console.log(err)
    }

    this.centertitletext=tb.DAA031;
    // this.percent =  Math.round(a / b * 100);
   
    if (DAA040.color)
      this.color = DAA040.color;

    this.height = DAA040.height;

    this.ngOnChanges();


  }
 
 
  private renderChart(type: string) {
    if (!this.resize$) return;

    this.updateRadio();

    const { color, node } = this;
    const percent = this.data[0][this.position.y]

    const data = Math.min(Math.max(percent / 100, 0), 100);
    const self = this;
    cancelAnimationFrame(this.timer);

    const canvas = node.nativeElement as HTMLCanvasElement;
    const ctx = canvas.getContext('2d') as CanvasRenderingContext2D;
    const canvasWidth = canvas.width;
    const canvasHeight = canvas.height;
    const radius = canvasWidth / 2;
    const lineWidth = 2;
    const cR = radius - lineWidth;

    ctx.beginPath();
    ctx.lineWidth = lineWidth * 2;

    const axisLength = canvasWidth - lineWidth;
    const unit = axisLength / 8;
    const range = 0.2; // 振幅
    let currRange = range;
    const xOffset = lineWidth;
    let sp = 0; // 周期偏移量
    let currData = 0;
    const waveupsp = 0.005; // 水波上涨速度

    let arcStack: [[number, number]?] | null = [];
    const bR = radius - lineWidth;
    const circleOffset = -(Math.PI / 2);
    let circleLock = true;

    // tslint:disable-next-line:binary-expression-operand-order
    for (let i = circleOffset; i < circleOffset + 2 * Math.PI; i += 1 / (8 * Math.PI)) {
      arcStack.push([radius + bR * Math.cos(i), radius + bR * Math.sin(i)]);
    }

    const cStartPoint = arcStack.shift() as [number, number];
    ctx.strokeStyle = color;
    ctx.moveTo(cStartPoint[0], cStartPoint[1]);

    function drawSin() {
      ctx.beginPath();
      ctx.save();

      const sinStack: [[number, number]?] = [];
      for (let i = xOffset; i <= xOffset + axisLength; i += 20 / axisLength) {
        const x = sp + (xOffset + i) / unit;
        const y = Math.sin(x) * currRange;
        const dx = i;
        // tslint:disable-next-line:binary-expression-operand-order
        const dy = 2 * cR * (1 - currData) + (radius - cR) - unit * y;

        ctx.lineTo(dx, dy);
        sinStack.push([dx, dy]);
      }

      const startPoint = sinStack.shift() as [number, number];
      // console.log(startPoint);
      if(!startPoint||startPoint.length<2) return;

      ctx.lineTo(xOffset + axisLength, canvasHeight);
      ctx.lineTo(xOffset, canvasHeight);
      ctx.lineTo(startPoint[0], startPoint[1]);

      const gradient = ctx.createLinearGradient(0, 0, 0, canvasHeight);
      gradient.addColorStop(0, '#ffffff');
      gradient.addColorStop(1, color);
      ctx.fillStyle = gradient;
      ctx.fill();
      ctx.restore();
    }

    function render() {
      ctx.clearRect(0, 0, canvasWidth, canvasHeight);
      if (circleLock && type !== 'update') {
        if (arcStack!.length) {
          const temp = arcStack!.shift() as [number, number];
          ctx.lineTo(temp[0], temp[1]);
          ctx.stroke();
        } else {
          circleLock = false;
          ctx.lineTo(cStartPoint[0], cStartPoint[1]);
          ctx.stroke();
          arcStack = null;

          ctx.globalCompositeOperation = 'destination-over';
          ctx.beginPath();
          ctx.lineWidth = lineWidth;
          // tslint:disable-next-line:binary-expression-operand-order
          ctx.arc(radius, radius, bR, 0, 2 * Math.PI, true);

          ctx.beginPath();
          ctx.save();
          // tslint:disable-next-line:binary-expression-operand-order
          ctx.arc(radius, radius, radius - 3 * lineWidth, 0, 2 * Math.PI, true);

          ctx.restore();
          ctx.clip();
          ctx.fillStyle = color;
        }
      } else {
        if (data >= 0.85) {
          if (currRange > range / 4) {
            const t = range * 0.01;
            currRange -= t;
          }
        } else if (data <= 0.1) {
          if (currRange < range * 1.5) {
            const t = range * 0.01;
            currRange += t;
          }
        } else {
          if (currRange <= range) {
            const t = range * 0.01;
            currRange += t;
          }
          if (currRange >= range) {
            const t = range * 0.01;
            currRange -= t;
          }
        }
        if (data - currData > 0) {
          currData += waveupsp;
        }
        if (data - currData < 0) {
          currData -= waveupsp;
        }

        sp += 0.07;
        drawSin();
      }
      self.timer = requestAnimationFrame(render);
    }

    render();
  }

  private updateRadio() {
    const { offsetWidth } = this.el.nativeElement.parentNode;
    const radio = offsetWidth < this.height ? offsetWidth / this.height : 1;
    this.renderer.setStyle(this.el.nativeElement, 'transform', `scale(${radio})`);
  }

  private installResizeEvent() {
    this.resize$ = fromEvent(window, 'resize')
      .pipe(debounceTime(200))
      .subscribe(() => this.updateRadio());
  }

  ngOnInit(): void {
    this.installResizeEvent();
   // this.ngZone.runOutsideAngular(() => setTimeout(() => this.renderChart(''), this.delay));
  }

  ngOnChanges(): void {
    this.ngZone.runOutsideAngular(() => this.renderChart('update'));
    this.cdr.detectChanges();
  }

  ngOnDestroy(): void {
    if (this.timer) {
      cancelAnimationFrame(this.timer);
    }
    this.resize$!.unsubscribe();
  }
}

// implements Lbchart {


//   @Input() @InputNumber() delay = 500;
//   @Input() centertitletext: string | TemplateRef<void>;
//   @Input() color = '#1890FF';
//   @Input() @InputNumber() height = 160;
//   @Input() @InputNumber() percent: number;

//   @ViewChild('water', { static: false }) water: G2WaterWaveComponent;

//   install(resdata: any) {

//     const tb = resdata.message.tb;


//     const cols: string[] = tb.DAA035.split(',');
//     const data = resdata.message.data;
//     const DAA040 = JSON.parse(tb.DAA040);



//     const a = data[cols[0]].list.length < 1 ? 0 : Number.parseFloat(data[cols[0]].list[0].VAL);
//     const b = data[cols[1]].list.length < 1 ? 0 : Number.parseFloat(data[cols[1]].list[0].VAL);

//     this.water.centertitletext = tb.DAA031;
//     this.water.percent = Math.round(a / b * 100);
//     if (DAA040.color)
//       this.water.color = DAA040.color;
//     this.water.height = DAA040.height;

//   }
//   attachChart(resdata: any) {


//   }
//   destroy() {


//   }


// }