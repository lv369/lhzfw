import { OnInit, OnChanges, OnDestroy, Component, ChangeDetectionStrategy, ViewEncapsulation, Input, TemplateRef, NgZone, ViewChild, ElementRef } from '@angular/core';
import { InputNumber, InputBoolean } from 'ng-zorro-antd';
import { Lbchart } from '@shared';
import { Subscription } from 'rxjs';
import { SupData } from '@core/lb/SupData';
/**
 * 分组柱状图
 */
@Component({
  selector: 'lb-group-bar',
  template: `
  <div #groupbar></div>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush, // 检测改为按需，即不自动刷新
})
export class LbGroupBarComponent implements OnInit, OnChanges, OnDestroy, Lbchart {
  private chart: any;
  private resize$: Subscription;
  @ViewChild('groupbar', { static: true }) private node: ElementRef;
  @Input() padding: Array<number | string> | string = 'auto';
  @Input() @InputNumber() delay = 0;
  @Input() color: string[] = null;
  @Input() colorFiled = 'name';
  @Input() position: any

  // @Input() style: { [key: string]: string };
  // @Input() position: {x: string, y: string, z: string} = {x: 'genre', y: 'sold', z: 'type'}
  @Input() xcolors: Array<string>
  @Input() ycolors: Array<string>
  @Input() zcolormap: object

  @Input() data: any[]

  // @Input() width: number;
  @Input() height: number;

  @Input() @InputBoolean() tooltipshow;
  @Input() @InputBoolean() tooltipshowtitle;
  @Input() @InputBoolean() legendshow = false
  @Input() legendposition 
  @Input() legendlayout

  @Input() sortx = true
  @Input() sortz
  @Input() sortrule = 'ASC'

  constructor(private ngZone: NgZone, private supdata: SupData) { }


  install() {
    this.render();
  }

  commonStylehandle(chart, data) {
    const dv = new DataSet.View().source(data)
    // if(this.sortx && !this.sortz) {
    //   dv.transform({
    //     type: 'sort-by',
    //     fields: [ this.sortx ], // 根据指定的字段集进行排序，与lodash的sortBy行为一致
    //     order: this.sortrule,        // 默认为 ASC，DESC 则为逆序
    //   })
    // } else if(!this.sortx && this.sortz){
    //   dv.transform({
    //     type: 'sort-by',
    //     fields: [ this.sortz ], // 根据指定的字段集进行排序，与lodash的sortBy行为一致
    //     order: this.sortrule,   // 默认为 ASC，DESC 则为逆序
    //   })
    // } else if(this.sortx && this.sortz) {
    //   dv.transform({
    //     type: 'sort-by',
    //     fields: [ this.sortx, this.sortz ], // 根据指定的字段集进行排序，与lodash的sortBy行为一致
    //     order: this.sortrule,   // 默认为 ASC，DESC 则为逆序
    //   })
    // }    
chart.source(dv.rows)

        // legend
        chart.legend(this.position.x, false)
        if (this.legendshow) {
          chart.legend(this.position.z, {
            position: this.legendposition,
            layout: this.legendlayout,
          });
        } else {
          chart.legend(false);
        }

    if (this.tooltipshow) {
      chart.tooltip({
        showTitle: this.tooltipshowtitle,
        itemTpl: '<li><span style="background-color:{color};" class="g2-tooltip-marker"></span>{name}: {value}</li>'
      });
    } else {
      chart.tooltip(false);
    }
    return dv.rows
  }

  render() {
    const { node, padding } = this;
    const container = node.nativeElement as HTMLElement;

    const chart = (this.chart = new G2.Chart({
      container,
      forceFit: true,  // 图表的宽度自适应开关
      height: this.getHeight(),
      padding,
    }));

 this.commonStylehandle(chart, this.data)

    const rchart =
      chart
        .interval()
        .position(`${this.position.x}*${this.position.y}`)
        .adjust([{
          type: 'dodge',
          dodgeBy: this.position.z, // 按照 type 字段进行分组
          marginRatio: 1 / 32
        }])

    // if(this.zcolormap != undefined )

    if (this.xcolors !== undefined && this.xcolors.length > 0) {
      rchart.color(this.position.x, this.xcolors)
    } else if (this.xcolors !== undefined && this.xcolors.length === 0) {
      rchart.color(this.position.x)
    }
    if (this.ycolors !== undefined && this.ycolors.length > 0) {
      rchart.color(this.position.y, this.ycolors)
    } else if (this.xcolors !== undefined && this.ycolors.length === 0) {
      rchart.color(this.position.y)
    }
    if (this.zcolormap !== undefined && Object.keys(this.zcolormap).length > 0) {
      rchart.color(this.position.z, r => {
        return this.zcolormap[r]
      })
    } else {
      rchart.color(this.position.z)
    }
    // Step 4: 渲染图表
    chart.render();

    
  }

  attachChart() {
    const { chart, padding, data, color } = this;
    if (!chart || !data || data.length <= 0) return;
    const height = this.getHeight();
    if (chart.get('height') !== height) {
      chart.changeHeight(height);
    }

    const dvrows =  this.commonStylehandle(chart, this.data)
    // color
    //  chart.get('geoms')[0].color(this.colorFiled,this.color);
    chart.set('padding', padding);
    chart.changeData(dvrows);
  }

  destroy() {
    if (this.resize$) {
      this.resize$.unsubscribe();
    }
    if (this.chart) {
      this.ngZone.runOutsideAngular(() => this.chart.destroy());
    }
  }

  /**
   * 报表高度
   * 存在报表头，则需要减掉报表头的高度
   */
  getHeight() {
    return this.height
    // return this.title ? (this.height - TITLE_HEIGHT) : this.height;
  }


  ngOnInit(): void {
    if (this.position && this.data.length > 0) {
      this.ngZone.runOutsideAngular(() => setTimeout(() => this.render(), this.delay));
    }

  }

  ngOnChanges(changes: import("@angular/core").SimpleChanges): void {
    this.ngZone.runOutsideAngular(() => this.attachChart());
  }
  ngOnDestroy(): void {
    this.destroy();
  }
}