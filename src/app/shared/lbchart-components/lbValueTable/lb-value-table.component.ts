import {
  OnInit,
  OnChanges,
  OnDestroy,
  Component,
  ChangeDetectionStrategy,
  ViewEncapsulation,
  Input,
  TemplateRef,
  NgZone,
  ViewChild,
  ElementRef,
} from '@angular/core';
import { InputNumber, InputBoolean } from 'ng-zorro-antd';
import { Lbchart } from '@shared';
import { Subscription } from 'rxjs';
import { SupData } from '@core/lb/SupData';
/**
 * 值表格
 */
@Component({
  selector: 'lb-value-table',
  templateUrl: './lb-value-table.component.html',
  // template: `
  //   <div #valuetable></div>
  // `,
  styleUrls: ['./lb-value-table.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush, // 检测改为按需，即不自动刷新
})
export class LbValueTableComponent
  implements OnInit, OnChanges, OnDestroy, Lbchart {
  private chart: any;
  private resize$: Subscription;
  @ViewChild('valuetable', { static: true }) private node: ElementRef;
  @Input() padding: Array<number | string> | string = 'auto';
  @Input() @InputNumber() delay = 0;
  @Input() color: string[] = null;
  @Input() colorFiled = 'name';
  @Input() position: any;

  // @Input() style: { [key: string]: string };
  // @Input() position: { x: string, y: string, z: string } = { x: 'adc', y: 'sold', z: 'type' }

  @Input() data: any[];

  @Input() height: number;

  @Input() @InputBoolean() tooltipshow = true;
  @Input() @InputBoolean() tooltipshowpercent = true;
  @Input() @InputBoolean() tooltipshowtitle = false;

  @Input() legendposition;
  @Input() legendlayout;
  @Input() @InputBoolean() legendshow = true;
  @Input() @InputBoolean() legendshowpercent = true;

  @Input() @InputBoolean() labelshow = false;

  @Input() @InputBoolean() istransxy = false;

  innerpie: any;
  outterpie: any;


  findata: any
  zlabel: Array<string> = []
  xlabel: Array<string> = []

  @Input() outerheadx
  @Input() outerheadz

  constructor(private ngZone: NgZone, private supdata: SupData) {}

  commonStyleHandle(node, data) {
    const dv = new DataSet.View().source(data);
    // chart.source(dv.rows);
    dv.transform({
      type: 'fill-rows',
      groupBy: [ this.position.x, this.position.z ],
      orderBy: [ this.position.y ],
      fillBy: 'group' // 默认为 group，可选值：order
    })

    console.log(dv.rows)

    const tmpdata = {}
    const xlabelset: Set<string> = new Set()
    const zlabelset: Set<string> = new Set()
    dv.rows.forEach(ele => {
      xlabelset.add(ele[this.position.x])
      zlabelset.add(ele[this.position.z])
      if(!tmpdata[ele[this.position.x]]) tmpdata[ele[this.position.x]] = {}
      tmpdata[ele[this.position.x]][ele[this.position.z]] = ele[this.position.y]
    })
    
this.findata = tmpdata

this.xlabel = [...xlabelset]
this.zlabel = [...zlabelset]
    console.log(tmpdata)
   console.log(this.zlabel)
   console.log(this.xlabel)
    return dv.rows;
  }

  install() {
    this.render();
  }

  render() {
    console.log('line-value-table rendered')
    console.log(this.data)
    console.log(this.height)
    const { node, padding } = this;

    this.commonStyleHandle(node, this.data);

  }

  attachChart() {
    console.log("lb-value-table attachChart trigged!")
    const { node, padding, data, color } = this;
    if (!data || data.length <= 0) return;
    const height = this.getHeight();

    const dvrows = this.commonStyleHandle(node, this.data);
  }

  destroy() {
    if (this.resize$) {
      this.resize$.unsubscribe();
    }
    if (this.chart) {
      this.ngZone.runOutsideAngular(() => this.chart.destroy());
    }
  }

  /**
   * 报表高度
   * 存在报表头，则需要减掉报表头的高度
   */
  getHeight() {
    return this.height;
    // return this.title ? (this.height - TITLE_HEIGHT) : this.height;
  }

  ngOnInit(): void {
    if (this.position && this.data.length > 0) {
      this.ngZone.runOutsideAngular(() =>
        setTimeout(() => this.render(), this.delay),
      );
    }
  }

  ngOnChanges(changes: import('@angular/core').SimpleChanges): void {
    this.ngZone.runOutsideAngular(() => this.attachChart());
  }
  ngOnDestroy(): void {
    this.destroy();
  }
}
