import { OnInit, OnChanges, OnDestroy, Component, ChangeDetectionStrategy, ViewEncapsulation, Input, TemplateRef, NgZone, ViewChild, ElementRef } from '@angular/core';
import { InputNumber, InputBoolean } from 'ng-zorro-antd';
import { Lbchart } from '@shared';
import { Subscription } from 'rxjs';
/**
 * 基础散点图
 */
@Component({
  selector: 'lb-basic-point',
  template: `
  <div #basicpoint></div>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush, // 检测改为按需，即不自动刷新
})
export class LbBasicPointComponent  implements OnInit, OnChanges, OnDestroy,Lbchart{
  private chart: any;
  private resize$: Subscription;
  @ViewChild('basicpoint', { static: true }) private node: ElementRef;
  @Input() padding: Array<number | string> | string = 'auto';
  @Input() @InputNumber() delay = 0;
  @Input() color:string[] = null;
  @Input() colorFiled='name';
  @Input() position: any

  // @Input() style: { [key: string]: string };
  // @Input() position: {x: string, y: string, } = {x: 'genre', y: 'sold'}
  @Input() xylabel: { x: string, y: string } = { x: 'a', y: 'b'}

  @Input() data: any [] 

  // @Input() width: number;
  @Input() height: number;

  @Input() @InputBoolean() legendshow = false
  @Input() @InputBoolean() tooltipshow = true;
  @Input() @InputBoolean() tooltipshowtitle = true
  @Input() @InputBoolean() pointshow = true;
  @Input() pointshape = 'circle';
  @Input() @InputNumber() pointsize = 5
  @Input() pointcolor = ''

  @Input() sortx = true
  @Input() sortz
  @Input() sortrule = 'ASC'

  point: any

  constructor(private ngZone: NgZone) { }

  install() {}

  commonStyleHandle(chart, data) {
    const dv = new DataSet.View().source(data);
    // if(this.sortx && !this.sortz) {
    //   dv.transform({
    //     type: 'sort-by',
    //     fields: [ this.sortx ], // 根据指定的字段集进行排序，与lodash的sortBy行为一致
    //     order: this.sortrule,        // 默认为 ASC，DESC 则为逆序
    //   })
    // } else if(!this.sortx && this.sortz){
    //   dv.transform({
    //     type: 'sort-by',
    //     fields: [ this.sortz ], // 根据指定的字段集进行排序，与lodash的sortBy行为一致
    //     order: this.sortrule,   // 默认为 ASC，DESC 则为逆序
    //   })
    // } else if(this.sortx && this.sortz) {
    //   dv.transform({
    //     type: 'sort-by',
    //     fields: [ this.sortx, this.sortz ], // 根据指定的字段集进行排序，与lodash的sortBy行为一致
    //     order: this.sortrule,   // 默认为 ASC，DESC 则为逆序
    //   })
    // }    
    chart.source(dv.rows);

    // legend
    if(!this.legendshow) chart.legend(false)

    // tooltip
    if(this.tooltipshow) {
      chart.tooltip({
        showTitle: this.tooltipshowtitle,
        crosshairs: {
          type: 'cross'
        },
      });
    } else {
      chart.tooltip(false)
    }

    // point
    if (this.pointshow === true) {
      if (this.point === undefined) {
        this.point = chart
          .point()
          .position(`${this.position.x}*${this.position.y}`)
          .size(this.position.x, x => {
            return this.pointsize
          })
          .color(this.position.x, x => this.pointcolor)
          .shape(this.position.x ,x=> {
            return this.pointshape
          })
      } else {
        this.point.show();
      }
    } else {
      if (this.point) this.point.hide();
    }

    return dv.rows;
  }

  render() {
    const { node, padding } = this;
    const container = node.nativeElement as HTMLElement;

    const chart = (this.chart = new G2.Chart({
      container,
      forceFit: true,  // 图表的宽度自适应开关
      height: this.getHeight(),
      padding,
    }));

    this.commonStyleHandle(chart, this.data);
    chart.render();
  }

   attachChart() {
    const { chart, padding, data, color } = this;
    if (!chart || !data || data.length <= 0) return;
    const height = this.getHeight();
    if (chart.get('height') !== height) {
      chart.changeHeight(height);
    }
    
    const dvrows = this.commonStyleHandle(chart, this.data);
     // color
    //  chart.get('geoms')[0].color(this.colorFiled,this.color);
     chart.set('padding', padding);
     chart.changeData(dvrows);
  }

  destroy(){
    if (this.resize$) {
      this.resize$.unsubscribe();
    }
    if (this.chart) {
      this.ngZone.runOutsideAngular(() => this.chart.destroy());
    }
  }

  /**
   * 报表高度
   * 存在报表头，则需要减掉报表头的高度
   */
   getHeight() {
     return this.height
    // return this.title ? (this.height - TITLE_HEIGHT) : this.height;
  }


  ngOnInit(): void {
    if(this.position&&this.data.length>0){
      this.ngZone.runOutsideAngular(() => setTimeout(() => this.render(), this.delay));
    }
   
  }  
  
  ngOnChanges(changes: import("@angular/core").SimpleChanges): void {
    this.ngZone.runOutsideAngular(() => this.attachChart());
  }
  ngOnDestroy(): void {
    this.destroy();
  }
}