import { OnInit, OnChanges, OnDestroy, Component, ChangeDetectionStrategy, ViewEncapsulation, Input, TemplateRef, NgZone, ViewChild, ElementRef } from '@angular/core';
import { InputNumber, InputBoolean } from 'ng-zorro-antd';
import { Lbchart } from '@shared';
import { Subscription } from 'rxjs';
import { SupData } from '@core/lb/SupData';
/**
 * 基础雷达图
 */
@Component({
  selector: 'lb-basic-radar',
  template: `
  <div #basicradar></div>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush, // 检测改为按需，即不自动刷新
})
export class LbBasicRadarComponent implements OnInit, OnChanges, OnDestroy, Lbchart {
  private chart: any;
  private resize$: Subscription;
  @ViewChild('basicradar', { static: true }) private node: ElementRef;
  @Input() padding: Array<number | string> | string = 'auto';
  @Input() @InputNumber() delay = 0;
  @Input() color: string[] = null;
  @Input() colorFiled = 'name';
  @Input() position: any

  // @Input() style: { [key: string]: string };
  // @Input() position: {x: string, y: string, z: string } = { x: 'genre', y: 'sold', z: 'type' }
  @Input() xylabel: { x: string, y: string } = { x: 'a', y: 'b' }

  @Input() data: any[]

  // @Input() width: number;
  @Input() height: number;

  @Input() @InputBoolean() tooltipshow = true;
  @Input() @InputBoolean() tooltipshowtitle = false

  @Input() legendposition;
  @Input() legendlayout;
  @Input() @InputBoolean() legendshow = true;

  @Input() sortx = true
  @Input() sortz
  @Input() sortrule = 'ASC'


  constructor(private ngZone: NgZone, private supdata: SupData) { }

  commonStyleHandle(chart, data) {

   
    const ndata = this.supdata.coverValToPercentVal(data, this.position.x, this.position.z);

    // 计算百分比
    const dv = new DataSet.View().source(ndata);
    // if(this.sortx && !this.sortz) {
    //   dv.transform({
    //     type: 'sort-by',
    //     fields: [ this.sortx ], // 根据指定的字段集进行排序，与lodash的sortBy行为一致
    //     order: this.sortrule,        // 默认为 ASC，DESC 则为逆序
    //   })
    // } else if(!this.sortx && this.sortz){
    //   dv.transform({
    //     type: 'sort-by',
    //     fields: [ this.sortz ], // 根据指定的字段集进行排序，与lodash的sortBy行为一致
    //     order: this.sortrule,   // 默认为 ASC，DESC 则为逆序
    //   })
    // } else if(this.sortx && this.sortz) {
    //   dv.transform({
    //     type: 'sort-by',
    //     fields: [ this.sortx, this.sortz ], // 根据指定的字段集进行排序，与lodash的sortBy行为一致
    //     order: this.sortrule,   // 默认为 ASC，DESC 则为逆序
    //   })
    // }    

    // Step 2: 载入数据源
    chart.source(dv.rows);

    chart.coord('polar', {
      radius: 0.9
    });
    chart.axis(this.position.z, {
      line: null,
      tickLine: null,
      grid: {
        lineStyle: {
          lineDash: null
        },
        hideFirstLine: false
      }
    });
    chart.axis(this.position.y, {
      line: null,
      tickLine: null,
      label:null,
      grid: {
        type: 'polygon',
        lineStyle: {
          lineDash: null
        },
        alternateColor: 'rgba(0, 0, 0, 0.04)'
      }
    });

    // 工具提示
    if (this.tooltipshow) {
      chart.tooltip({
        showTitle: this.tooltipshowtitle,
        itemTpl: '<li><span style="background-color:{color};" class="g2-tooltip-marker"></span>{name}:{orgval} </li>'
      });
    } else {
      chart.tooltip(false);
    }

    // 图例
    if (this.legendshow) {
      chart.legend(this.position.x, {
        // marker: 'circle',
        // offset: 30,
        position: this.legendposition,
        layout: this.legendlayout,
      });
    } else {
      chart.legend(false);
    }

    return dv.rows;
  }

  install() {
    this.render()
  }

  render() {
    const { node, padding } = this;
    const container = node.nativeElement as HTMLElement;

    const chart = (this.chart = new G2.Chart({
      container,
      forceFit: true,  // 图表的宽度自适应开关
      height: this.getHeight(),
      padding,
    }));

    this.commonStyleHandle(chart, this.data)
    chart.line().position(`${this.position.z}*${this.position.y}`).color(`${this.position.x}`)
      .size(2)
      .tooltip(
        `${this.position.x}*${this.position.y}*orgval`,
        (x, y, orgval) => {
          return {
            name: x,
            value: y,
            orgval,
          };
        },
      );
    chart.point().position(`${this.position.z}*${this.position.y}`).color(`${this.position.x}`)
      .shape('circle')
      .size(4)
      .style({
        stroke: '#fff',
        lineWidth: 1,
        fillOpacity: 1
      })
      .tooltip(
        `${this.position.x}*${this.position.y}*orgval`,
        (x, y, orgval) => {
          return {
            name: x,
            value: y,
            orgval,
          };
        },
      );;

    // Step 4: 渲染图表
    chart.render();
  }

  attachChart() {
    const { chart, padding, data, color } = this;
    if (!chart || !data || data.length <= 0) return;
    const height = this.getHeight();
    if (chart.get('height') !== height) {
      chart.changeHeight(height);
    }
    const dvrows = this.commonStyleHandle(chart, data)
    // color
    //  chart.get('geoms')[0].color(this.colorFiled,this.color);
    chart.set('padding', padding);
    chart.changeData(dvrows);
  }

  destroy() {
    if (this.resize$) {
      this.resize$.unsubscribe();
    }
    if (this.chart) {
      this.ngZone.runOutsideAngular(() => this.chart.destroy());
    }
  }

  /**
   * 报表高度
   * 存在报表头，则需要减掉报表头的高度
   */
  getHeight() {
    return this.height
    // return this.title ? (this.height - TITLE_HEIGHT) : this.height;
  }


  ngOnInit(): void {
    if (this.position && this.data.length > 0) {
      this.ngZone.runOutsideAngular(() => setTimeout(() => this.install(), this.delay));
    }

  }

  ngOnChanges(changes: import("@angular/core").SimpleChanges): void {
    this.ngZone.runOutsideAngular(() => this.attachChart());
  }
  ngOnDestroy(): void {
    this.destroy();
  }
}