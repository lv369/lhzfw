import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
// delon
import { AlainThemeModule } from '@delon/theme';
import { DelonABCModule } from '@delon/abc';
import { DelonChartModule } from '@delon/chart';
import { DelonACLModule } from '@delon/acl';
import { DelonFormModule, WidgetRegistry } from '@delon/form';

// #region third libs
import { NgZorroAntdModule } from 'ng-zorro-antd';
import { CountdownModule } from 'ngx-countdown';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { NgxImageGalleryModule } from 'ngx-image-gallery';
import { LbfModule } from 'lbf';
import { NgxTinymceModule } from 'ngx-tinymce';

const THIRDMODULES = [
  NgZorroAntdModule,
  CountdownModule,
  DragDropModule,
  NgxImageGalleryModule,
  LbfModule,
  NgxTinymceModule,
];
// #endregion

// #region your componets & directives
import { PRO_SHARED_COMPONENTS } from '../layout/pro';
import { LangsComponent } from './components/langs/langs.component';
import { ImageWrapperComponent } from './components/imageWrapper';
import { ChartWrapperComponent } from './components/chart-wrapper/chart-wrapper.component';
import { LbChartWrapperDirective } from './directive/LbChartWrapperDirective';

import { LbBasicBarComponent } from './lbchart-components/lbBasicBar/lb-basic-bar.component';
import { LbGroupBarComponent } from './lbchart-components/lbGroupBar/lb-group-bar.component';
import { LbStackBarComponent } from './lbchart-components/lbStackBar/lb-stack-bar.component';
import { LbBasicLineComponent } from './lbchart-components/lbBasicLine/lb-basic-line.component';
import { LbMutipleLineComponent } from './lbchart-components/lbMutipleLine/lb-mutiple-line.component';
import { LbBasicPieComponent } from './lbchart-components/lbBasicPie/lb-basic-pie.component';
import { LbNightingalePieComponent } from './lbchart-components/lbNightingalePie/lb-nightingale-pie.component';
import { LbBasicPointComponent } from './lbchart-components/lbBasicPoint/lb-basic-point.component';
import { LbGroupPointComponent } from './lbchart-components/lbGroupPoint/lb-group-point.component';
import { LbStackLineComponent } from './lbchart-components/lbStackLine/lb-stack-line.component';
import { LbBasicHeatmapComponent } from './lbchart-components/lbBasicHeatmap/lb-basic-heatmap.component';
import { LbBasicGaugeComponent } from './lbchart-components/lbBasicGauge/lb-basic-gauge.component';
import { LbBasicFunnelComponent } from './lbchart-components/lbBasicFunnel/lb-basic-funnel.component';
import { LbBasicRadarComponent } from './lbchart-components/lbBasicRadar/lb-basic-radar.component';
import { LbStackPieComponent } from './lbchart-components/lbStackPie/lb-stack-pie.component';
import { LbBasicWordcloudComponent } from './lbchart-components/lbBasicWordcloud/lb-basic-wordcloud.component';
import { LbWaterWaveComponent } from './lbchart-components/lbWaterWave/lb-water-wave.component';
import { LbValueTableComponent } from './lbchart-components/lbValueTable/lb-value-table.component';
import { LbBasicStripComponent } from './lbchart-components/lbBasicStrip/lb-basic-strip.component';
import { LbDataCardComponent } from './lbchart-components/lbDataCard/lb-data-card.component';
import { LbStackStripComponent } from './lbchart-components/lbStackStrip/lb-stack-strip.component';

import { LbChartComponent } from './components/lb-chart/lb-chart.component';
import { TinymceWidget } from './json-schema/widgets/tinymce/tinymce.widget';

const CHARTS_COMPONENTS = [
  LbBasicBarComponent,
  LbGroupBarComponent,
  LbStackBarComponent,
  LbBasicLineComponent,
  LbMutipleLineComponent,
  LbBasicPieComponent,
  LbNightingalePieComponent,
  LbBasicPointComponent,
  LbGroupPointComponent,
  LbStackLineComponent,
  LbBasicHeatmapComponent,
  LbBasicGaugeComponent,
  LbBasicFunnelComponent,
  LbBasicRadarComponent,
  LbStackPieComponent,
  LbBasicWordcloudComponent,
  LbWaterWaveComponent,
  LbValueTableComponent,
  LbBasicStripComponent,
  LbDataCardComponent,
  LbStackStripComponent,
];

import { LbBasicBarDirective } from './directive/lbchart/LbBasicBarDirective';
import { LbBasicPieDirective } from './directive/lbchart/LbBasicPieDirective';
import { LbBasicFunnelDirective } from './directive/lbchart/LbBasicFunnelDirective';
import { LbBasicLineDirective } from './directive/lbchart/LbBasicLineDirective';
import { LbBasicGaugeDirective } from './directive/lbchart/LbBasicGaugeDirective';
import { LbBasicHeatmapDirective } from './directive/lbchart/LbBasicHeatmapDirective';
import { LbBasicPointDirective } from './directive/lbchart/LbBasicPointDirective';
import { LbBasicRadarDirective } from './directive/lbchart/LbBasicRadarDirective';
import { LbGroupBarDirective } from './directive/lbchart/LbGroupBarDirective';
import { LbGroupPointDirective } from './directive/lbchart/LbGroupPointDirective';
import { LbMutipleLineDirective } from './directive/lbchart/LbMutipleLineDirective';
import { LbNightingalePieDirective } from './directive/lbchart/LbNightingalePieDirective';
import { LbStackBarDirective } from './directive/lbchart/LbStackBarDirective';
import { LbStackLineDirective } from './directive/lbchart/LbStackLineDirective';
import { LbValueTableDirective } from './directive/lbchart/LbValueTableDirective';
import { LbBasicStripDirective } from './directive/lbchart/LbBasicStripDirective';
import { LbDataCardDirective } from './directive/lbchart/LbDataCardDirective';

import { LbGridChartDirective } from './directive/LbGridChartDirective';
import { LbWaterwaveComponent } from './components/lbchart/water-wave/waterwave.component';
import { LbG2cardDirective } from './directive/LbG2cardDirective';
import { LbWaterChartDirective } from './directive/LbWaterChartDirective';
import { LbChartDirective } from './directive/LbCharDirective';
import { LbPercentCardComponent } from './components/lbchart/percentCard/percentCard.component';
import { LbPercentDirective } from './directive/LbPercentDirective';
import { LbBarChartComponent } from './components/lbchart/barChart/barChart.component';
import { LbBarChartDirective } from './directive/LbBarChartDective';
import { LbPieChartDirective } from './directive/LbPieChartDirective';
import { LbStackPieDirective } from './directive/lbchart/LbStackPieDirective';
import { LbAreaChartComponent } from './components/lbchart/areaChart/areaChart.component';
import { LbAreaChartDirective } from './directive/LbAreaChartDective';
import { LbCronComponent } from './components/lb-cron/lb-cron.component';
import { LbG2CardComponent } from './components/lbchart/g2card/g2Card.component';
import { LbDebugDirective } from './directive/LbDebugDective';
import { LbChartGridComponent } from './components/lbchart/chartGrid/chartGrid.component';

// 自定义小部件
import { ColorSelectComponent } from './json-schema/widgets/colorSelect/colorSelect.component';

import { LbBasicWordcloudDirective } from './directive/lbchart/LbBasicWordcloudDirective';
import { PanelWrapperComponent } from './components/panel-wrapper/panel-wrapper.component';
import { ChartLoadingComponent } from './components/chart-loading/chart-loading.component';
import { ChartEmptyComponent } from './components/chart-empty/chart-empty.component';
import { LbCompaCardComponent } from './components/lbchart/compaCard/compaCard.component';
import { HtmlPipe } from './components/innerhtmlpipe/innerhtmlpipe';
import { LbWjfwComponent } from './components/lb_wjfw/lbwjfw.component';
import { LbRepComponent } from './components/lb-rep/lb-rep.component';
import { ErrorInfoComponent } from './components/error-info/error-info.component';
import { SxListComponent } from './components/tyjb/lb_sxlist';
import { GrComponent } from './components/tyjb/lb_gr';
import { DwComponent } from './components/tyjb/lb_dw';
import { GrFormComponent } from './components/tyjb/gr_form';
import { PrintComponent } from './components/tyjb/lb_print';
import { DwFormComponent } from './components/tyjb/dw_form';
import { SqListComponent } from './components/tyjb/lb_sqlist';
import { UserSelectComponent } from './components/tyjb/lb_user_select';
import { TyekouTyblComponent } from './components/tyjb/tyekou_tybl';
import { TyekouTyslComponent } from './components/tyjb/tyekou_tysl';
import { TyekouQueryComponent } from './components/tyjb/tyekou_query';
import { DwFormQueryComponent } from './components/tyjb/dw_form_query';
import { GrFormQueryComponent } from './components/tyjb/gr_form_query';
import { SlListComponent } from './components/tyjb/lb_sllist';
import { TyekouStepComponent } from './components/tyjb/tyekou_step';
import { BlListComponent } from './components/tyjb/lb_bllist';
import { BlFormComponent } from './components/tyjb/bl_form';
import { LbMsgComponent } from './components/lb_msg/lb_msg';
import { BlFormQueryComponent } from './components/tyjb/bl_form_query';
import { PrintListComponent } from './components/tyjb/lb_print_list';
import { LbBmSelectComponent } from './components/tyjb/lb_bm_select';
import { LbZtSelectComponent } from './components/tyjb/lb_zt_select';
import { TyekouSjspComponent } from './components/tyjb/tyekou_sjsp';
import { SjFormComponent } from './components/tyjb/sj_form';
import { LbSxSelectComponent } from './components/tyjb/lb_sx_select';

// import { LbBasicChartDirective } from './directive/LbBasicChartDirective'

const COMPONENTS_ENTRY = [
  LangsComponent,
  ImageWrapperComponent,
  LbChartComponent,
  LbWaterwaveComponent,
  LbPercentCardComponent,
  LbBarChartComponent,
  LbAreaChartComponent,
  TinymceWidget,
  ColorSelectComponent,
  LbCronComponent,
  LbG2CardComponent,
  LbCompaCardComponent,
  LbChartGridComponent,
  ChartWrapperComponent,
  PanelWrapperComponent,
  ChartLoadingComponent,
  ChartEmptyComponent,
  SxListComponent,
  SlListComponent,
  BlListComponent,
  TyekouTyblComponent,
  TyekouTyslComponent,
  TyekouQueryComponent,
  DwFormQueryComponent,
  GrFormQueryComponent,
  TyekouStepComponent,
  BlFormComponent,
  GrComponent,
  DwComponent,
  BlFormQueryComponent,
  LbMsgComponent,
  GrFormComponent,
  DwFormComponent,
  PrintComponent,
  PrintListComponent,
  SqListComponent,
  LbBmSelectComponent,
  LbZtSelectComponent,
  TyekouSjspComponent,
  UserSelectComponent,
  LbWjfwComponent,
  ErrorInfoComponent,
  SjFormComponent,
  LbSxSelectComponent,
  ...CHARTS_COMPONENTS,
];
const COMPONENTS = [...COMPONENTS_ENTRY, ...PRO_SHARED_COMPONENTS];

const LBCHART_DIRECTIVES = [
  LbBasicBarDirective,
  LbBasicPieDirective,
  LbBasicFunnelDirective,
  LbBasicLineDirective,
  LbBasicGaugeDirective,
  LbBasicHeatmapDirective,
  LbBasicPointDirective,
  LbBasicRadarDirective,
  LbGroupBarDirective,
  LbGroupPointDirective,
  LbMutipleLineDirective,
  LbNightingalePieDirective,
  LbStackBarDirective,
  LbStackLineDirective,
  LbPieChartDirective,
  LbStackPieDirective,
  LbBasicWordcloudDirective,

  LbChartWrapperDirective, // 图表容器
  // LbBasicChartDirective, // 在此directive中动态创建以上组件
  LbValueTableDirective,
  LbBasicStripDirective,
  LbDataCardDirective,
  //LbRepComponent,
];

const DIRECTIVES = [
  LbDebugDirective,
  LbChartDirective,
  LbGridChartDirective,
  LbG2cardDirective,
  LbWaterChartDirective,
  LbPercentDirective,
  LbBarChartDirective,
  LbAreaChartDirective,
  ...LBCHART_DIRECTIVES,
];
// #endregion

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    RouterModule,
    ReactiveFormsModule,
    AlainThemeModule.forChild(),
    DelonABCModule,
    DelonChartModule,
    DelonACLModule,
    DelonFormModule,

    // third libs
    ...THIRDMODULES,
  ],
  declarations: [
    // your components
    ...COMPONENTS,
    ...DIRECTIVES,
    HtmlPipe,

  ],
  entryComponents: COMPONENTS_ENTRY,
  exports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
    AlainThemeModule,
    DelonABCModule,
    DelonChartModule,
    DelonACLModule,
    DelonFormModule,
    // i18n
    // TranslateModule,
    // third libs
    ...THIRDMODULES,
    // your components
    ...COMPONENTS,
    ...DIRECTIVES,
  ],
})
export class SharedModule {
  constructor(widgetRegistry: WidgetRegistry) {
    widgetRegistry.register(ColorSelectComponent.KEY, ColorSelectComponent);
    widgetRegistry.register(TinymceWidget.KEY, TinymceWidget);
  }
}
