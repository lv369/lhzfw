import { Component, OnInit } from '@angular/core';
import { ControlWidget } from '@delon/form';

@Component({
  selector: 'colorSelect',
  templateUrl: `./colorSelect.component.html`
})
export class ColorSelectComponent extends ControlWidget implements OnInit {
  /* 用于注册小部件 KEY 值 */
  static readonly KEY = 'colorSelect';

  // 组件所需要的参数，建议使用 `ngOnInit` 获取
  selectList=[];
  colorMap={};
  mode="";
  optionMode="";
  

  ngOnInit(): void {
    this.selectList=this.ui.selectList;
    this.selectList.forEach(ele=>{
      this.colorMap[ele.value]=ele.color;
    });
    this.mode=this.ui.mode || 'default';
    this.optionMode=this.ui.optionMode || 'badge';
  }

  change(value: string) {
    if (this.ui.change) this.ui.change(value);
    this.setValue(value);
  }

  getStyle(para){
    const style={color:this.colorMap[para]};
    return style;
  }

}