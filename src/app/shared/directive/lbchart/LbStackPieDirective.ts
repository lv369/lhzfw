import { Directive, Input, OnInit, OnChanges, ChangeDetectorRef } from '@angular/core';
import { NzMessageService, InputNumber, } from 'ng-zorro-antd';
import { HttpService } from 'lbf';
import { SupData } from '@core/lb/SupData';
import { LbStackPieComponent } from '@shared/lbchart-components/lbStackPie/lb-stack-pie.component'
@Directive({
  selector: '[stackPie]'
})
export class LbStackPieDirective implements OnInit, OnChanges {

  // 图表ID
  @Input('stackPie') @InputNumber() DAA030: string;

  @Input() querypara = {};


  constructor(
    private chart: LbStackPieComponent,
    private message: NzMessageService,
    private lbservice: HttpService,
    private cdr: ChangeDetectorRef,  private supdata: SupData) {

  }


  commonStyleHandle(chart, style) {
    for (const key of Object.keys(style)) {
      if (typeof style[key] === 'object') {
        for (const key1 of Object.keys(style[key])) {
          chart[key.toString() + key1] = style[key][key1];
        }
      }
    }
  }

  ngOnInit(): void {
    this.lbservice
      .lbservice('TB_TBXX', {
        para: { DAA030: this.DAA030, Q: this.querypara },
      })
      .then(resdata => {
        if (resdata.code < 1) {
          this.message.error(resdata.errmsg);
        } else {
          const dataFilds: string[] = resdata.message.tb.DAA035.split(',');
          const groupFileds: string[] = resdata.message.tb.DAA036.split(',');

          let data = this.supdata.rebuildData(
            resdata.message.data,
            resdata.message.tb,
            groupFileds,
            groupFileds,
          );
          data = this.supdata.rebuildDsDataToLabel(data);
          if(dataFilds.length > 1) {
            data = this.supdata.partition(data, this.supdata.getKeyFeild( resdata.message.data,))
            this.chart.position = { x: groupFileds[0], y: 'val', z: 'item' };
          } else {
            this.chart.position = { x: groupFileds[0], y: dataFilds[0], z: groupFileds[1] };
          }
          this.chart.data = data;
          try {
            const style = JSON.parse(resdata.message.tb.DAA040);
            this.commonStyleHandle(this.chart, style);
          } catch (err) {
            this.message.error('图表参数配置错误');
          }

          this.chart.render();
        }
      });
  }

  ngOnChanges(changes: import('@angular/core').SimpleChanges): void {
    if (!changes.firstChange) {
      this.lbservice
        .lbservice('TB_TBXX', {
          para: { DAA030: this.DAA030, Q: this.querypara },
        })
        .then(resdata => {
          if (resdata.code < 1) {
            this.message.error(resdata.errmsg);
          } else {
            const dataFilds: string[] = resdata.message.tb.DAA035.split(',');
          const groupFileds: string[] = resdata.message.tb.DAA036.split(',');

          let data = this.supdata.rebuildData(
            resdata.message.data,
            resdata.message.tb,
            groupFileds,
            groupFileds,
          );
          data = this.supdata.rebuildDsDataToLabel(data);
          if(dataFilds.length > 1) {
            data = this.supdata.partition(data, this.supdata.getKeyFeild( resdata.message.data,))
            this.chart.position = { x: groupFileds[0], y: 'val', z: 'item' };
          } else {
            this.chart.position = { x: groupFileds[0], y: dataFilds[0], z: groupFileds[1] };
          }
          this.chart.data = data;
            this.chart.attachChart()
          }
        });
    }
  }

}
