import { Directive, Input, OnInit, OnChanges, ChangeDetectorRef } from '@angular/core';
import { NzMessageService, InputNumber, } from 'ng-zorro-antd';
import { HttpService } from 'lbf';
import { SupData } from '@core/lb/SupData';
import { LbBasicStripComponent } from '@shared/lbchart-components/lbBasicStrip/lb-basic-strip.component'
@Directive({
  selector: '[basicStrip]'
})
export class LbBasicStripDirective implements OnInit,OnChanges {

  // 图表ID
  @Input('basicStrip')  @InputNumber() DAA030: string;
  @Input() querypara={};


  constructor(
    private chart: LbBasicStripComponent,
    private message: NzMessageService,
    private lbservice:HttpService,
    private supdata: SupData, ) {

  }
  commonStyleHandle(chart, style) {
    for (const key of Object.keys(style)) {
      if (typeof style[key] === 'object') {
        for (const key1 of Object.keys(style[key])) {
          chart[key.toString() + key1] = style[key][key1];
        }
      }
    }
  }

  ngOnInit(): void {
    this.lbservice
      .lbservice('TB_TBXX', {
        para: { DAA030: this.DAA030, Q: this.querypara },
      })
      .then(resdata => {
        if (resdata.code < 1) {
          this.message.error(resdata.errmsg);
        } else {
          // 图表信息
          const dataFilds: string[] = resdata.message.tb.DAA035.split(','); // 指标
          const groupFileds: string[] = resdata.message.tb.DAA036.split(','); // 视角
          // 字段信息

          // 数据处理
          let data = this.supdata.rebuildData(
            resdata.message.data,
            resdata.message.tb,
            groupFileds,
            groupFileds,
          );
          // 字典转化
          data = this.supdata.rebuildDsDataToLabel(data);

          this.chart.data = data;
          this.chart.position = { x: groupFileds[0], y: dataFilds[0] };
          // this.basicpie.innerRadius = 0.5;

          try {
            const style = JSON.parse(resdata.message.tb.DAA040); // chartstyle
            this.commonStyleHandle(this.chart, style);
          } catch (err) {
            this.message.error('图表参数配置错误');
          }

          this.chart.render();
        }
      });
  }

  ngOnChanges(changes: import('@angular/core').SimpleChanges): void {
    if (!changes.firstChange) {
      this.lbservice
        .lbservice('TB_TBXX', {
          para: { DAA030: this.DAA030, Q: this.querypara },
        })
        .then(resdata => {
          if (resdata.code < 1) {
            this.message.error(resdata.errmsg);
          } else {
            const groupFileds: string[] = resdata.message.tb.DAA036.split(',');
            // 字段信息

            // 数据处理
            let data = this.supdata.rebuildData(
              resdata.message.data,
              resdata.message.tb,
              groupFileds,
              groupFileds,
            );
            // 字典转化
            data = this.supdata.rebuildDsDataToLabel(data);
            this.chart.data = data;
            this.chart.attachChart()
          }
        });
    }
  }

  // ngOnInit(): void {
 
  //   this.lbservice.lbservice('TB_TBXX', { para: { DAA030: this.DAA030,Q:this.querypara } }).then(resdata => {
  //     if (resdata.code < 1) {
  //       this.message.error(resdata.errmsg);
  //     }
  //     else {
  //       this.chart.install(resdata);
  //       this.cdr.markForCheck();
  //     }
  //   });
  // }

  // ngOnChanges(changes: import("@angular/core").SimpleChanges): void {
  //   if(!changes.firstChange){
  
  //     this.lbservice.lbservice('TB_TBXX', { para: { DAA030: this.DAA030,Q:this.querypara } }).then(resdata => {
  //       if (resdata.code < 1) {
  //         this.message.error(resdata.errmsg);
  //       }
  //       else {
  //         this.chart.attachChart(resdata);
  //         this.cdr.markForCheck();
  //       }
  //     });

  //   }
  // }
  
  
}
