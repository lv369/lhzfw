import { Directive, Input, OnInit, OnChanges } from '@angular/core';
import { NzMessageService, InputNumber } from 'ng-zorro-antd';
import { HttpService } from 'lbf';
import { SupData } from '@core/lb/SupData';
import { LbBasicPieComponent } from '@shared/lbchart-components/lbBasicPie/lb-basic-pie.component';
@Directive({
  selector: '[basicPie]',
})
export class LbBasicPieDirective implements OnInit, OnChanges {
  // @Input('lbBasicBar') param: {
  //   height: number, // 公共属性

  //   DAA030: string, // 图表ID

  //   data: any[], // 通用数据

  //   DAA020: string | Array<string>, // 指标ID

  //   type: string,
  //   position: any,
  //   style: object,

  //   where: string,
  // }

  // // 公共属性
  // @InputNumber() height: number

  // 图表ID
  @Input('basicPie') @InputNumber() DAA030: string;
  @Input() querypara = {};

  // // 通用数据
  // @Input('lbChart') @Input() data: any[]

  // // 指标ID
  // @Input('lbChart') @Input() DAA020: string | Array<string>

  // @Input('lbChart') @Input() type: string
  // @Input('lbChart') @Input() position: any
  // @Input('lbChart') @Input() style: object // 图表样式, 不同的图也不一样

  // @Input('lbChart') @Input() where: string

  constructor(
    private chart: LbBasicPieComponent,

    private message: NzMessageService,
    private lbservice: HttpService,
    private supdata: SupData,
  ) {}

  transformdata(data, daa020) {
    // let daa020 = this.DAA020
    let datas = [];
    let z;
    if (daa020.length > 1) {
      for (const key of Object.keys(data)) {
        datas = [
          ...datas,
          ...data[key].list.map(m => {
            return {
              dz: data[key].DAA022,
              ...m,
            };
          }),
        ];
      }
      z = 'dz';
    } else if (daa020.length === 1) {
      for (const key of Object.keys(data)) {
        datas = data[key].list;
      }
    }
    return { datas, z };
  }

  queryCommonData() {
    // console.log(this)
    return new Promise((resolve, reject) => {
      if (this.DAA030) {
        this.lbservice
          .lbservice('TB_TBXX', { para: { DAA030: this.DAA030 } })
          .then(resdata => {
            if (resdata.code < 1) {
              reject(resdata.errmsg);
              return;
            }

            // 观察视角，按xyz顺序
            const positionArr: string[] = resdata.message.tb.DAA036.split(',');

            // 指标域
            const zbs: string[] = resdata.message.tb.DAA035.split(',');

            const { datas, z } = this.transformdata(resdata.message.data, zbs);

            resolve({
              data: datas,
              type: resdata.message.tb.DAA033,
              position: {
                x: positionArr[0],
                z: z ? z : positionArr[1],
              },
            });
          })
          .catch(err => {
            reject(err);
          });
      }
    });
  }

  commonStyleHandle(chart, style) {
    for (const key of Object.keys(style)) {
      if (typeof style[key] === 'object') {
        for (const key1 of Object.keys(style[key])) {
          chart[key.toString() + key1] = style[key][key1];
        }
      }
    }
  }

  ngOnInit(): void {
    this.lbservice
      .lbservice('TB_TBXX', {
        para: { DAA030: this.DAA030, Q: this.querypara },
      })
      .then(resdata => {
        if (resdata.code < 1) {
          this.message.error(resdata.errmsg);
        } else {
          // 图表信息
          const dataFilds: string[] = resdata.message.tb.DAA035.split(',');
          const groupFileds: string[] = resdata.message.tb.DAA036.split(',');
          // 字段信息

          // 数据处理
          let data = this.supdata.rebuildData(
            resdata.message.data,
            resdata.message.tb,
            groupFileds,
            groupFileds,
          );
          // 字典转化
          data = this.supdata.rebuildDsDataToLabel(data);

          this.chart.data = data;
          this.chart.position = { x: groupFileds[0], y: dataFilds[0] };
          // this.chart.innerRadius = 0.5;

          try {
            let style = JSON.parse(resdata.message.tb.DAA040);
            this.commonStyleHandle(this.chart, style);
          } catch (err) {
            this.message.error('图表参数配置错误');
          }

          this.chart.render();
        }
      });
  }

  ngOnChanges(changes: import('@angular/core').SimpleChanges): void {
    if (!changes.firstChange) {
      this.lbservice
        .lbservice('TB_TBXX', {
          para: { DAA030: this.DAA030, Q: this.querypara },
        })
        .then(resdata => {
          if (resdata.code < 1) {
            this.message.error(resdata.errmsg);
          } else {
            const groupFileds: string[] = resdata.message.tb.DAA036.split(',');
            // 字段信息

            // 数据处理
            let data = this.supdata.rebuildData(
              resdata.message.data,
              resdata.message.tb,
              groupFileds,
              groupFileds,
            );
            // 字典转化
            data = this.supdata.rebuildDsDataToLabel(data);
            this.chart.data = data;
            this.chart.attachChart()
          }
        });
    }
  }

  // ngOnInit(): void {
  //   this.queryCommonData()
  //     .then((option: any) => {
  //       const { type, position, data } = option
  //       const chart = this.chart

  //       const { x, z } = position
  //       chart.position = {
  //         x,
  //         y: 'VAL',
  //         z,
  //       }
  //       chart.data = data;

  //       // console.log(chart)

  //       chart.ngOnInit();
  //     })

  // }
}
