import { Directive, Input, OnInit } from '@angular/core';
import { NzMessageService, InputNumber, } from 'ng-zorro-antd';
import { HttpService } from 'lbf';
import { SupData } from '@core/lb/SupData';
import { LbBasicPointComponent } from '@shared/lbchart-components/lbBasicPoint/lb-basic-point.component'
@Directive({
  selector: '[basicPoint]'
})
export class LbBasicPointDirective implements OnInit {
  // @Input('lbBasicBar') param: {
  //   height: number, // 公共属性

  //   DAA030: string, // 图表ID

  //   data: any[], // 通用数据

  //   DAA020: string | Array<string>, // 指标ID

  //   type: string,
  //   position: any,
  //   style: object,

  //   where: string,
  // }

  // // 公共属性
  // @InputNumber() height: number

  // 图表ID
  @Input('basicPoint')  @InputNumber() DAA030: string;
  querypara = {}
  // // 通用数据
  // @Input('lbChart') @Input() data: any[]

  // // 指标ID
  // @Input('lbChart') @Input() DAA020: string | Array<string>

  // @Input('lbChart') @Input() type: string
  // @Input('lbChart') @Input() position: any
  // @Input('lbChart') @Input() style: object // 图表样式, 不同的图也不一样

  // @Input('lbChart') @Input() where: string
  
  constructor(private chart: LbBasicPointComponent,

    private message: NzMessageService,
    private lbservice: HttpService,
    private supdata: SupData) {
  }

  commonStyleHandle(chart, style) {
    for (const key of Object.keys(style)) {
      if (typeof style[key] === 'object') {
        for (const key1 of Object.keys(style[key])) {
          chart[key.toString() + key1] = style[key][key1];
        }
      }
    }
  }

  ngOnInit(): void {
    this.lbservice
      .lbservice('TB_TBXX', {
        para: { DAA030: this.DAA030, Q: this.querypara },
      })
      .then(resdata => {
        if (resdata.code < 1) {
          this.message.error(resdata.errmsg);
        } else {
          // 图表信息
          const dataFilds: string[] = resdata.message.tb.DAA035.split(',');
          const groupFileds: string[] = resdata.message.tb.DAA036.split(',');
          // 字段信息

          // 数据处理
          let data = this.supdata.rebuildData(
            resdata.message.data,
            resdata.message.tb,
            groupFileds,
            groupFileds,
          );
          // 字典转化
          data = this.supdata.rebuildDsDataToLabel(data);

          this.chart.data = data;
          this.chart.position = { x: groupFileds[0], y: dataFilds[0] };
          // this.chart.innerRadius = 0.5;

          try {
            let style = JSON.parse(resdata.message.tb.DAA040);
            this.commonStyleHandle(this.chart, style);
          } catch (err) {
            this.message.error('图表参数配置错误');
          }

          this.chart.render();
        }
      });
  }

  ngOnChanges(changes: import('@angular/core').SimpleChanges): void {
    if (!changes.firstChange) {
      this.lbservice
        .lbservice('TB_TBXX', {
          para: { DAA030: this.DAA030, Q: this.querypara },
        })
        .then(resdata => {
          if (resdata.code < 1) {
            this.message.error(resdata.errmsg);
          } else {
            const groupFileds: string[] = resdata.message.tb.DAA036.split(',');
            // 字段信息

            // 数据处理
            let data = this.supdata.rebuildData(
              resdata.message.data,
              resdata.message.tb,
              groupFileds,
              groupFileds,
            );
            // 字典转化
            data = this.supdata.rebuildDsDataToLabel(data);
            this.chart.data = data;
            this.chart.attachChart()
          }
        });
    }
  }
}
