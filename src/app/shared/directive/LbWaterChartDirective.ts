import { OnInit, OnChanges, Directive, Input, ChangeDetectorRef } from '@angular/core';
import { HttpService } from 'lbf';
import { InputNumber, NzMessageService } from 'ng-zorro-antd';
import { LbWaterwaveComponent } from '@shared/components/lbchart/water-wave/waterwave.component';
import { SupData } from '@core/lb/SupData';

@Directive({
  selector: '[lbWater]'
})
export class LbWaterChartDirective implements OnInit, OnChanges {
  constructor(private water: LbWaterwaveComponent, private lbservice: HttpService, private message: NzMessageService,
    private cdr: ChangeDetectorRef, private supdata: SupData) {

  }


  @Input('lbWater') @InputNumber() DAA030: string;
  @Input() querypara = {};

  ngOnInit(): void {

    this.lbservice.lbservice('TB_TBXX', { para: { DAA030: this.DAA030, Q: this.querypara } }).then(resdata => {
      if (resdata.code < 1) {
        this.message.error(resdata.errmsg);
      }
      else {

        // const tb = resdata.message.tb;
        // const cols: string[] = tb.DAA035.split(',');
        // const data = resdata.message.data;
        // const DAA040 = JSON.parse(tb.DAA040);


        // const a = Number.parseFloat(data[cols[0]].list[0].VAL);
        // const b = Number.parseFloat(data[cols[1]].list[0].VAL);

        // this.water.title = tb.DAA031;
        // this.water.percent = Math.round(a / b * 100);
        // if (DAA040.color)
        //   this.water.color = DAA040.color;
        // this.water.height = DAA040.height;

        this.water.install(resdata);
        this.cdr.markForCheck();


      }

    });

  }

  ngOnChanges(changes: import("@angular/core").SimpleChanges): void {
    if (!changes.firstChange) {

      this.lbservice.lbservice('TB_TBXX', { para: { DAA030: this.DAA030, Q: this.querypara } }).then(resdata => {
        if (resdata.code < 1) {
          this.message.error(resdata.errmsg);
        }
        else {
          this.water.attachChart(resdata);
          // const tb = resdata.message.tb;
          // const cols: string[] = tb.DAA035.split(',');
          // const data = resdata.message.data;

          // if(data[cols[0]].list.length<1||data[cols[1]].list.length<1){
          //   this.water.percent = 0;
          // }else{

          //   const a = Number.parseFloat(data[cols[0]].list[0].VAL);
          //   const b = Number.parseFloat(data[cols[1]].list[0].VAL);

          //   this.water.percent = Math.round(a / b * 100);

          // }



          this.cdr.markForCheck();
        }
      });

    }
  }


}