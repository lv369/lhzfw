import { Directive, Input, OnInit, SimpleChanges, OnChanges, ChangeDetectorRef, Output, EventEmitter, HostListener } from '@angular/core';
import { STComponent, STColumn, STChange, STData } from '@delon/abc';
import { NzMessageService, InputNumber } from 'ng-zorro-antd';
import { HttpService } from 'lbf';
import { SupData } from '@core/lb/SupData';
@Directive({
  selector: '[lbGridChart]'
})
export class LbGridChartDirective implements OnInit, OnChanges {
  @Input('lbGridChart') @InputNumber() DAA030: string;
  @Input() querypara = {};
  @Output() chartClick = new EventEmitter<any>();

  constructor(private st: STComponent, private message: NzMessageService, private lbservice: HttpService, private supdata: SupData,
    private cdr: ChangeDetectorRef, ) {
  }
  columns: STColumn[] = [
    { title: '年度', index: 'AAE001' }
  ];
  ngOnInit() {
    // // 迷你列表
    // this.st.size = 'small';
    // // 多排
    // this.st.multiSort = true;
    // // 不显示分页器
    // this.st.page = { show: false };
    
    if (!this.DAA030) {
      this.message.error('未配置图表ID！');
      return;
    }
    this.lbservice.lbservice('TB_TBXX', { para: { DAA030: this.DAA030, Q: this.querypara } }).then(resdata => {
      if (resdata.code < 1) {
        this.message.error(resdata.errmsg);
      }
      else {
        // 图表信息
        const groupFileds: string[] = resdata.message.tb.DAA036.split(',');
        // 字段信息
        this.columns = this.supdata.getStColums(resdata.message.jg);
        // 数据
        this.st.data = this.supdata.rebuildData(resdata.message.data, resdata.message.tb, groupFileds, groupFileds);
        this.st.resetColumns({ emitReload: true, columns: this.columns });

      }
    });
  }

  /**
   * 行点击事件处理
   * @param stchange stchange内容 
   */
  @HostListener("change", ["$event"])
  rowClick(stchange: STChange) {
    if (stchange.type === 'click') {
      console.log(stchange);
      this.chartClick.emit(stchange.click.item);
    }
  }

  ngOnChanges(changes: SimpleChanges): void {

    if (!changes.firstChange) {

      this.lbservice.lbservice('TB_TBXX', { para: { DAA030: this.DAA030, Q: this.querypara } }).then(resdata => {
        if (resdata.code < 1) {
          this.message.error(resdata.errmsg);
        }
        else {

          // 图表信息
          const groupFileds: string[] = resdata.message.tb.DAA036.split(',');
          this.st.data = [... this.supdata.rebuildData(resdata.message.data, resdata.message.tb, groupFileds, groupFileds)];
          this.st.reload();
        }

      });

    }
  }
}
