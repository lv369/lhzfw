import { OnInit, Directive, Input, ChangeDetectorRef, ElementRef, ContentChild, OnChanges } from '@angular/core';
import { HttpService } from 'lbf';
import { InputNumber, NzMessageService } from 'ng-zorro-antd';
import { LbBasicPieComponent } from '@shared/lbchart-components/lbBasicPie/lb-basic-pie.component';
import { G2PieComponent } from '@delon/chart';
import { SupData } from '@core/lb/SupData';

@Directive({
  selector: '[lbPiChart]'
})
export class LbPieChartDirective implements OnInit, OnChanges {


  @Input('lbPiChart') @InputNumber() DAA030: string;
  @Input() querypara = {};

  constructor(
    private chart: G2PieComponent,
    private message: NzMessageService,
    private lbservice: HttpService,
    private cdr: ChangeDetectorRef,
    private supdata: SupData) {

  }
  ngOnInit(): void {

    this.lbservice.lbservice('TB_TBXX', { para: { DAA030: this.DAA030, Q: this.querypara } }).then(resdata => {
      if (resdata.code < 1) {
        this.message.error(resdata.errmsg);
      }
      else {

        const tb = resdata.message.tb;

        // 图表信息
        const dataFilds: string[] = tb.DAA035.split(',');
        const groupFileds: string[] = tb.DAA036.split(',');

        // 数据
        let data = this.supdata.rebuildData(resdata.message.data, tb, groupFileds, groupFileds);
        // 字典转化
        data = this.supdata.rebuildDsDataToLabel(data);
        data = this.supdata.coverToXy(data, groupFileds[0], dataFilds[0]);

        this.chart.subTitle = tb.DAA031;
        this.chart.total = "0";
        this.chart.data = data;
        this.chart.hasLegend=true;
      //  this.chart.inner=0.5;
        this.chart.valueFormat = (val: number) => {
          return `&yen ${val.toFixed(2)}`;
        }


        //  this.chart.ngOnChanges();
        // this.cdr.markForCheck();
      }
    });

  }

  ngOnChanges(changes: import("@angular/core").SimpleChanges): void {
    if (!changes.firstChange) {

      this.lbservice.lbservice('TB_TBXX', { para: { DAA030: this.DAA030, Q: this.querypara } }).then(resdata => {
        if (resdata.code < 1) {
          this.message.error(resdata.errmsg);
        }
        else {

        }
      });

    }
  }

}