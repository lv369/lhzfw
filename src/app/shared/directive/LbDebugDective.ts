import { Directive, Input } from '@angular/core';

@Directive({
  selector: '[debug]'
})
export class LbDebugDirective {

  @Input() set debug(val) {
    console.log(val);
  }

}