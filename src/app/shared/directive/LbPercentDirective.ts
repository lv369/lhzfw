import { OnInit, Directive, Input, ChangeDetectorRef, ElementRef, ContentChild } from '@angular/core';
import { HttpService } from 'lbf';
import { InputNumber, NzMessageService } from 'ng-zorro-antd';
import { SupData } from '@core/lb/SupData';
import { LbPercentCardComponent } from '@shared/components/lbchart/percentCard/percentCard.component';
import { SupDate } from '@core/lb/SupDate';

@Directive({
  selector: '[lbpercent]'
})
export class LbPercentDirective implements OnInit {


  @Input('lbpercent') @InputNumber() DAA030: string;

  constructor(
    private chart: LbPercentCardComponent,
    private message: NzMessageService,
    private lbservice: HttpService,
    private cdr: ChangeDetectorRef,
    private supdata: SupData,
    public supdate:SupDate,
    ) {

  }
  ngOnInit(): void {
    this.lbservice.lbservice('TB_TBXX', { para: { DAA030: this.DAA030 } }).then(resdata => {
      if (resdata.code < 1) {
        this.message.error(resdata.errmsg);
      }
      else {

        const tb = resdata.message.tb;

        // 图表信息
        const cols: string[] = tb.DAA035.split(',');
        // 数据

        const data = resdata.message.data;
       

        let a = 0;
        if (data[cols[0]].list.length > 0) {
          a = Number.parseFloat(data[cols[0]].list[0].VAL);
        }
        const b = data[cols[1]].list[0]?Number.parseFloat(data[cols[1]].list[0].VAL):0;
        const centper =b===0?0:Math.round(a / b * 10000)/100;
        this.chart.title = tb.DAA031;

       
        this.chart.percent = centper;
    
        // 根据365|366天计算
        const target=Math.round(this.supdate.getDayinYear()/this.supdate.getYearDays()*10000)/100
        this.chart.target=target;

        if(target<centper){
          this.chart.color="#F78F28";
        }

        
        this.chart.total = `<span nzTooltipTitle="计划:${target}%" nzTooltipPlacement="right" nz-tooltip ><font color='#40a9ff'>${centper}%</font></span>`;


       // this.chart.target = centper;

        const groupFileds: string[] = resdata.message.tb.DAA036.split(',');
        const ndata =this.supdata.rebuildData(data,tb,groupFileds);

        this.chart.footer = "已经使用：" + (ndata.length>0? ndata[0][cols[0]]:0);

        const DAA040 = JSON.parse(tb.DAA040);
        this.chart.contentHeight = DAA040.contentHeight||"46px";
        

      }
    });

  }
}