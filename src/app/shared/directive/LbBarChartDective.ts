import { OnInit, Directive, Input, ChangeDetectorRef, ElementRef,  ContentChild, OnChanges } from '@angular/core';
import { HttpService } from 'lbf';
import { InputNumber, NzMessageService } from 'ng-zorro-antd';
import { SupData } from '@core/lb/SupData';
import { LbBarChartComponent } from '@shared/components/lbchart/barChart/barChart.component';

@Directive({
  selector: '[lbBarChart]'
})
export class LbBarChartDirective implements OnInit,OnChanges  {


  @Input('lbBarChart') @InputNumber() DAA030: string;
  @Input() querypara={};
  

  constructor(
    private chart: LbBarChartComponent,
    private message: NzMessageService,
    private lbservice:HttpService,
    private cdr: ChangeDetectorRef,
    private supdata:SupData ) {

  }
  ngOnInit(): void {

    this.lbservice.lbservice('TB_TBXX', { para: { DAA030: this.DAA030,Q:this.querypara } }).then(resdata => {
      if (resdata.code < 1) {
        this.message.error(resdata.errmsg);
      }
      else {

       this.chart.install(resdata);
       this.cdr.markForCheck();
      }
    });
    
  }

  ngOnChanges(changes: import("@angular/core").SimpleChanges): void {
    if(!changes.firstChange){
  
      this.lbservice.lbservice('TB_TBXX', { para: { DAA030: this.DAA030,Q:this.querypara } }).then(resdata => {
        if (resdata.code < 1) {
          this.message.error(resdata.errmsg);
        }
        else {
          this.chart.attachChart(resdata);
          this.cdr.markForCheck();
        }
      });

    }
  }
}