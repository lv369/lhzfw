import { OnInit, Directive, Input, ChangeDetectorRef, OnChanges, SimpleChanges } from '@angular/core';
import { HttpService } from 'lbf';
import { InputNumber, NzMessageService } from 'ng-zorro-antd';
import { SupData } from '@core/lb/SupData';
import { LbG2CardComponent } from '@shared/components/lbchart/g2card/g2Card.component';

@Directive({
  selector: '[lbG2card]'
})
export class LbG2cardDirective implements OnInit,OnChanges {
 
  constructor(private g2card: LbG2CardComponent, private lbservice: HttpService, private message: NzMessageService,
    private cdr: ChangeDetectorRef, private supdata:SupData) {

  }


  @Input('lbG2card') @InputNumber() DAA030: string;
  @Input() querypara={};

  ngOnInit(): void {

    this.lbservice.lbservice('TB_TBXX', { para: { DAA030: this.DAA030,Q:this.querypara } }).then(resdata => {
      if (resdata.code < 1) {
        this.message.error(resdata.errmsg);
      }
      else {

        const tb = resdata.message.tb;
        const cols: string[] = tb.DAA035.split(',');
        const data = resdata.message.data;
        const groupFileds: string[] = resdata.message.tb.DAA036.split(',');
        const ndata =this.supdata.rebuildData(data,tb,groupFileds);
        const DAA040 = JSON.parse(tb.DAA040);
        this.g2card.title = tb.DAA031;
        this.g2card.total = `<span><font color='${DAA040.color}'>${ndata.length>0? ndata[0][cols[0]]:0}</font></span>`;
        this.g2card.footer = data[cols[1]].DAA022 + ":" + (ndata.length>0? ndata[0][cols[1]]:0);
        this.g2card.bordered = DAA040.bordered;
        this.g2card.contentHeight = DAA040.contentHeight;

        this.cdr.markForCheck();
      }

    });

  }

  ngOnChanges(changes: SimpleChanges): void { 
  
    if(!changes.firstChange){

      this.lbservice.lbservice('TB_TBXX', { para: { DAA030: this.DAA030,Q:this.querypara } }).then(resdata => {
        if (resdata.code < 1) {
          this.message.error(resdata.errmsg);
        }
        else {
  
          const tb = resdata.message.tb;
          const cols: string[] = tb.DAA035.split(',');
          const data = resdata.message.data;
          const groupFileds: string[] = resdata.message.tb.DAA036.split(',');
          const ndata =this.supdata.rebuildData(data,tb,groupFileds);
          const DAA040 = JSON.parse(tb.DAA040);
          this.g2card.title = tb.DAA031;
          this.g2card.total = `<span><font color='${DAA040.color}'>${ndata.length>0? ndata[0][cols[0]]:0}</font></span>`;
          this.g2card.footer = data[cols[1]].DAA022 + ":" + (ndata.length>0? ndata[0][cols[1]]:0);
          this.g2card.bordered = DAA040.bordered;
          this.g2card.contentHeight = DAA040.contentHeight;
          this.cdr.markForCheck();
  
        }
  
      });

    }
  }


}