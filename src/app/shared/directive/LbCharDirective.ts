import { OnInit, Directive, Input, ChangeDetectorRef, ElementRef,  ContentChild, OnChanges } from '@angular/core';
import { HttpService } from 'lbf';
import { InputNumber, NzMessageService } from 'ng-zorro-antd';
import { LbBasicPieComponent } from '@shared/lbchart-components/lbBasicPie/lb-basic-pie.component';

@Directive({
  selector: '[lbChart]'
})
export class LbChartDirective implements OnInit,OnChanges {
 

  @Input('lbChart') @InputNumber() DAA030: string;
  @Input() querypara={};


  constructor(
    private chart: LbBasicPieComponent,
    private message: NzMessageService,
    private lbservice:HttpService,
    private cdr: ChangeDetectorRef, ) {

  }


  ngOnInit(): void {
 
    this.lbservice.lbservice('TB_TBXX', { para: { DAA030: this.DAA030,Q:this.querypara } }).then(resdata => {
      if (resdata.code < 1) {
        this.message.error(resdata.errmsg);
      }
      else {
        this.chart.install(resdata);
        this.cdr.markForCheck();
      }
    });
  }

  ngOnChanges(changes: import("@angular/core").SimpleChanges): void {
    if(!changes.firstChange){
  
      this.lbservice.lbservice('TB_TBXX', { para: { DAA030: this.DAA030,Q:this.querypara } }).then(resdata => {
        if (resdata.code < 1) {
          this.message.error(resdata.errmsg);
        }
        else {
          this.chart.attachChart();
          this.cdr.markForCheck();
        }
      });

    }
  }
}