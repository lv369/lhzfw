import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LbCronComponent } from './lb-cron.component';

describe('LbCronComponent', () => {
  let component: LbCronComponent;
  let fixture: ComponentFixture<LbCronComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LbCronComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LbCronComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
