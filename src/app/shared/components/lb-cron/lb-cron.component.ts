import { Component, OnInit, ViewChildren, QueryList, AfterViewInit, Input, Output, EventEmitter } from '@angular/core';
import { NzSelectComponent } from 'ng-zorro-antd';


@Component({
  selector: 'lb-cron',
  templateUrl: './lb-cron.component.html',
  styleUrls: ['./lb-cron.component.less']
})
export class LbCronComponent implements OnInit,AfterViewInit {


  @Input() title = '执行频率';

  selectItem = 'minutes'
  selectHour = '*'
  selectMinutes = '*'
  selectDay = '?'
  selectMonth = '*'
  selectYear = '*'
  selectSecond = '*'

  cron = '* * * * * *'
  list = [
    { label: '分', value: 'minutes' },
    { label: '时', value: 'hour' },
    { label: '天', value: 'day' },
    { label: '周', value: 'week' },
    { label: '月', value: 'month' },
    { label: '年', value: 'year' }
  ]
  list1 = [] // 分
  list2 = [] // 时
  list3 = [
    { label: '星期天', value: 0 },
    { label: '星期一', value: 1 },
    { label: '星期二', value: 2 },
    { label: '星期三', value: 3 },
    { label: '星期四', value: 4 },
    { label: '星期五', value: 5 },
    { label: '星期六', value: 6 },
  ]  // 周
  list4 = [] // 天
  list5 = [] // 月
  constructor() { }

  @ViewChildren('select') selects: QueryList<NzSelectComponent>;
  ngOnInit() {

    this.initArry();
  }

  ngAfterViewInit() {
    this.selects.map(s => {
      const temp = s.onChange
      s.onChange = (newValue) => {
        temp(newValue)
        this.displayCron()
       
      }
    })

  }
  initArry() {
    for (let index = 0; index < 60; index++) {
      this.list1.push({ value: index })
    }
    for (let index = 0; index < 24; index++) {
      this.list2.push({ value: index })
    }
    for (let index = 1; index < 32; index++) {
      this.list4.push({ value: index })
    }
    for (let index = 1; index < 13; index++) {
      this.list5.push({ value: index })
    }
  }



  selectChange($event) {
    
    this.reset($event)
    this.displayCron()
  }

  reset(s: string) {
    switch (s) {
      case 'minutes': {
        this.selectMinutes = null
        this.selectHour = null
        this.selectDay = null
        this.selectMonth = null
        this.selectYear = null
      } break;
      case 'hour': {
        this.selectHour = null
        this.selectDay = null
        this.selectMonth = null
        this.selectYear = null
      } break;
      case 'day': {
        this.selectDay = null
        this.selectMonth = null
        this.selectYear = null
      } break;
      case 'week': {
        this.selectMonth = null
        this.selectYear = null
      } break;
      case 'month': {
        this.selectDay = null
        this.selectYear = null
      } break;

      default:
        break;
    }
  }
  displayCron() {
    const arr = []
    arr.push(this.selectSecond !==null?this.selectSecond:'*');
    arr.push(this.selectMinutes !==null?this.selectMinutes:'*')
    arr.push(this.selectHour !==null?this.selectHour:'*')
    arr.push(this.selectMonth !==null?this.selectMonth:'*')
    arr.push(this.selectYear !==null?this.selectYear:'*')
    arr.push(this.selectDay !==null?this.selectDay:'?')
    this.cron = arr.join(' ');
    //this.cron = `${this.selectSecond} ${this.selectMinutes} ${this.selectHour} ${this.selectMonth} ${this.selectYear} ${this.selectDay} `
  }

}
