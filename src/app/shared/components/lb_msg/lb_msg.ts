import { Component, OnInit, TemplateRef, ViewChild, AfterViewInit, Output, EventEmitter } from '@angular/core';
import { _HttpClient } from '@delon/theme';
import { NzNotificationService,NzMessageService,NzConfigService} from 'ng-zorro-antd';
import {ServiceInfo,HttpService} from 'lbf';
import {Router} from '@angular/router';
import { ReuseTabService } from '@delon/abc';
import { RefService } from '@core/lb/RefService';

@Component({
  selector: 'lb-msg',
  templateUrl: './lb_msg.html',
})
export class LbMsgComponent implements OnInit,AfterViewInit {
  @Output() voted = new EventEmitter();

  // 设置颜色样式 默认通知类
  style = {}
  colorStyle:any = {color: '#2db7f5'}
  msgTitle:string = '标题' // 消息标题
  msgContent:string = '内容' // 消息内容
  msgType:string = '1' // 消息类型 默认是通知
  msgId:number; // 消息ID
  gotoUrl:string =''; // 跳转模块
  msgPara:string = ''; // 跳转
  
  // 设置消息源
  @Output() set MESSAGE(value:any){
    this._message = value;

    this.msgId = value.XXID;

    // 获取类型
    this.msgType = value.XTYPE;
    
    // 获取标题
    this.msgTitle = value.XTITLE;
    
    // 如果是通知，取内容的前100个字
    this.msgContent = this._message.XCONTENT.length>100? `${this._message.XCONTENT.slice(0,100)}...`:this._message.XCONTENT;

    // 提醒
    if(this.msgType==='2'){
      this.colorStyle = {color: '#fa8c16'}
      this.gotoUrl = value.URL;
      this.msgPara = value.PARA;
    }
    // 警报
    else if(this.msgType==='3'){
      this.colorStyle = {color: '#f50'}
      this.gotoUrl = value.URL;
    }
    // 其他为通知
    else{
      this.colorStyle = {color: '#2db7f5'}

      
    }

    console.log('----------into--------------')
    // 显示消息
    this.notification.template(this.msgInfo);
    console.log('----------out--------------')
  }
  _message:any;

  // 消息模板
  @ViewChild('msgInfo',{static: false}) msgInfo: TemplateRef<{}>;

  constructor(private notification: NzNotificationService,private router: Router,
      private service: ServiceInfo,private httpService: HttpService,private msgSrv: NzMessageService,
      private reuseTabSrv: ReuseTabService,
      private ref:RefService,
      private configSrv:NzConfigService) {
        this.configSrv.set('notification',{nzPlacement: 'bottomRight'})
      }
  

  ngOnInit() {
     
   }

   ngAfterViewInit(): void {    
    
   }

  // 点击查看
  click(){

    this.notification.remove();
    this.httpService.lbservice('XXGL_ZTUP',{XXID: this.msgId}).then(res => {
      if(res.code > 0){
        this.voted.emit(this._message);
      }
    });
    
    
    // 如果消息类型是1 通知，则跳转到收件箱，并显示消息内容    
    if(this.msgType!=='1'){
      this.ref.sendRef({receive:'shgl',message:JSON.parse(this.msgPara),sender:'message',createTime:new Date(),isGoto:true,url:this.gotoUrl})
      // const msgInfo:any = JSON.parse(this._message.XCONTENT)
      /*
      this.httpService.lbservice('PUBLIC_PARA',{XXID:this.msgId}).then(resdata=>{
        if(resdata.code<1)
        {
          this.msgSrv.error(resdata.errmsg);
        }
        else
        {
          // this.reuseTabSrv.close(this.gotoUrl);
          // 没办法，暂时到中转页面去转悠一下       
           this.router.navigate(['/tyjb/sxcl'],{queryParams:{R:this.gotoUrl}})
        }      
      })
      */
    }    
  }
}