export interface Lbchart {
  /**
   * 图表安装
   * @param data  图表接口返回的数据
   */
  install(data:any);
  /**
   * 图表更新
   * @param data 图表接口返回的数据
   */
  attachChart(data:any);
  /**
   * 图表删除
   */
  destroy();
}