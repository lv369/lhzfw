import { Component, Input, TemplateRef } from '@angular/core';
import {  InputBoolean, InputNumber } from 'ng-zorro-antd';


@Component({
  selector: 'lb-g2card',
  templateUrl: './g2Card.component.html',
})
export class LbG2CardComponent{

 @Input() title:string;
 @Input() @InputBoolean() bordered=true;
 @Input() footer: string;
 @Input() color = '#13C2C2';
 @Input() total:string;
 @Input() contentHeight:string; 
 @Input() @InputNumber() percent:number;
 @Input() @InputNumber() target:number;


}