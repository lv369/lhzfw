import { OnInit, OnChanges, OnDestroy, Component, ChangeDetectionStrategy, ViewEncapsulation, Input, TemplateRef, NgZone, ViewChild, ElementRef } from '@angular/core';
import { InputNumber, InputBoolean } from 'ng-zorro-antd';
import { Subscription } from 'rxjs';
import { SupData } from '@core/lb/SupData';

declare var G2: any;
const TITLE_HEIGHT = 41;

@Component({
  selector: 'lb-area',
  templateUrl: './areaChart.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush, // 检测改为按需，即不自动刷新
})
export class LbAreaChartComponent implements OnInit, OnChanges, OnDestroy {

  private chart: any;
  private resize$: Subscription;

  @ViewChild('container', { static: true }) private node: ElementRef;


  /**
   * 延迟加载
   */
  @Input() @InputNumber() delay = 0;
  /**
   * 标题
   */
  @Input() title: string | TemplateRef<void>;
  /**
   * 颜色
   */
  @Input() color: string[] = null;
  @Input() colorFiled = 'name';
  /**
   * 报表高度
   */
  @Input() @InputNumber() height = 200;
  /**
   * padding
   */
  @Input() padding: Array<number | string> | string = 'auto';
  /**
   * 报表数据
   */
  @Input() data: any[] = [];
  /**
   * 宽度较少时，是否延迟标签
   */
  @Input() @InputBoolean() autoLabel = true;

  /**
   * 坐标字段
   */
  @Input() position: { x: string, y: string, z: string };

  alias: any = {};


  constructor(private ngZone: NgZone, private supdata: SupData) { }

  install(resdata) {


    // 图表信息
    const dataFilds: string[] = resdata.message.tb.DAA035.split(',');
    const groupFileds: string[] = resdata.message.tb.DAA036.split(',');

    this.alias[dataFilds[0]] = { alias: resdata.message.data[dataFilds[0]].DAA022 }

    let data = this.supdata.rebuildData(resdata.message.data, resdata.message.tb, groupFileds, groupFileds);
    // 字典转化
    data = this.supdata.rebuildDsDataToLabel(data);
    this.data = data;
    this.position = { x: groupFileds[0], y: dataFilds[0], z: groupFileds[1] };

    this.render();
  }

  render() {

    const { node, padding } = this;
    const container = node.nativeElement as HTMLElement;

    const chart = (this.chart = new G2.Chart({
      container,
      forceFit: true,  // 图表的宽度自适应开关

      height: this.getHeight(),
      padding,
    }));

    chart.source(this.data, {
      year: {
        type: 'linear',
        tickInterval: 50
      }
    });

    chart.tooltip({
      crosshairs: {
        type: 'line'
      }
    });

    chart.areaStack().position(`${this.position.x}*${this.position.y}`).color(this.position.z).shape('smooth');
    chart.lineStack().position(`${this.position.x}*${this.position.y}`).color(this.position.z)
      .size(2).shape('smooth');
    chart.render();

  }

  attachChart(resdata) {

    // 图表信息
    const dataFilds: string[] = resdata.message.tb.DAA035.split(',');
    const groupFileds: string[] = resdata.message.tb.DAA036.split(',');

    this.alias[dataFilds[0]] = { alias: resdata.message.data[dataFilds[0]].DAA022 }

    let data = this.supdata.rebuildData(resdata.message.data, resdata.message.tb, groupFileds, groupFileds);
    // 字典转化
    data = this.supdata.rebuildDsDataToLabel(data);
    this.data = data;

    this.updateChart();

  }

  updateChart() {

    if (!this.chart) return;

    this.chart.changeData(this.data);
  }

  destroy() {
    if (this.resize$) {
      this.resize$.unsubscribe();
    }
    if (this.chart) {
      this.ngZone.runOutsideAngular(() => this.chart.destroy());
    }
  }

  /**
   * 报表高度
   * 存在报表头，则需要减掉报表头的高度
   */
  getHeight() {
    return this.title ? this.height - TITLE_HEIGHT : this.height;
  }


  ngOnInit(): void {
    if (this.position && this.data.length > 0) {
      this.ngZone.runOutsideAngular(() => setTimeout(() => this.render(), this.delay));
    }

  }

  ngOnChanges(changes: import("@angular/core").SimpleChanges): void {
    this.ngZone.runOutsideAngular(() => this.updateChart());
  }
  ngOnDestroy(): void {
    this.destroy();
  }


}