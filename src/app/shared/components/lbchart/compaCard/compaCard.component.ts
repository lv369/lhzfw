import { Component, OnInit, Input, OnChanges, SimpleChanges } from '@angular/core';
import { HttpService } from 'lbf';
import { InputNumber } from 'ng-zorro-antd';
import { SupDate } from '@core/lb/SupDate';
import { SupData } from '@core/lb/SupData';

@Component({
  selector: 'lb-compacard',
  templateUrl: './compaCard.component.html',
})
export class LbCompaCardComponent implements OnInit, OnChanges {


  c: any = {};

  isLaoding =false;

  @Input() @InputNumber() DAA030: number;
  @Input() querypara = {};

  constructor(private lbservice: HttpService, private supdate: SupDate, private supdata: SupData) {

  }

  ngOnInit(): void {

    this.isLaoding =true;
    this.lbservice.lbservice('TB_TBXX', { para: { DAA030: this.DAA030 } }).then(resdata => {
      this.isLaoding=false;
      if (resdata.code < 1) {

      }
      else {

        const tb = resdata.message.tb;
        const groupFileds: string[] = tb.DAA036.split(',');

        // 图表信息
        const cols: string[] = tb.DAA035.split(',');

        // 数据
        let data: any[] = resdata.message.data;
        data = this.supdata.rebuildData(data, tb, groupFileds);
        // 标题
        this.c.title = tb.DAA031;

        let last = 0;
        let now = 0;

        if (data.length === 0) {
          last = 0;
          now = 0;
        } else {

          // now = data[0].SYEAR  === this.supdate.getThisYear().toString()? data[0][cols[0]]:(data.length===2?data[1][cols[0]]:0);

          if (data[0].SYEAR === this.supdate.getThisYear().toString()) {
            now = data[0][cols[0]];
            last = data.length === 2 ? data[1][cols[0]] : 0;
          } else {
            last = data[0][cols[0]];
            now = data.length === 2 ? data[1][cols[0]] : 0;
          }
        }


        this.c.val =  `<span><font color='#40a9ff'>${now}</font></span>`;
        this.c.foot = last;
        this.c.flag = now >= last ? 'up' : 'down';
        this.c.per = last === 0 ? 'NA' : ((now - last) / last * 100).toFixed(2) + "%";
      }

    });

  }

  ngOnChanges(changes: SimpleChanges): void {

  }





}