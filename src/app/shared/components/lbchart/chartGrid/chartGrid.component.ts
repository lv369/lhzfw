import { Component, Input, TemplateRef, ViewChild, Output, EventEmitter, OnInit, OnChanges } from '@angular/core';
import { InputBoolean, InputNumber, NzMessageService } from 'ng-zorro-antd';
import { STColumn, STComponent, STData } from '@delon/abc';
import { Observable } from 'rxjs';
import { HttpService } from 'lbf';
import { SupData } from '@core/lb/SupData';
import { SupDate } from '@core/lb/SupDate';



@Component({
  selector: 'lb-gt',
  templateUrl: './chartGrid.component.html',
})
export class LbChartGridComponent implements OnInit, OnChanges {



  @ViewChild('st', { static: true }) st: STComponent;
  @Input() columns: STColumn[] = [
    { title: '年度', index: 'AAE001' }
  ];
  @Input() data: string | STData[] | Observable<STData[]>;
  /**
   * 矩阵旋转
   */
  @Input() RMFiled: string;
  @Input() @InputBoolean() bordered = true;

  @Input() @InputNumber() DAA030: string;
  @Input() querypara = {};
  @Output() chartClick = new EventEmitter<any>();

  constructor(private message: NzMessageService, private lbservice: HttpService, private supdata: SupData,
    private supdate: SupDate
  ) {

  }

  ngOnInit(): void {
    if (!this.DAA030) {
      this.message.error('未配置图表ID！');
      return;
    }
    this.st.loading = true;
    this.lbservice.lbservice('TB_TBXX', { para: { DAA030: this.DAA030, Q: this.querypara } }).then(resdata => {
      this.st.loading = false;
      if (resdata.code < 1) {
        this.message.error(resdata.errmsg);
      }
      else {
        // 图表信息
        const groupFileds: string[] = resdata.message.tb.DAA036.split(',');
        const daa040_str = resdata.message.tb.DAA040;
        const daa040 = JSON.parse(daa040_str ? daa040_str : "{}");

        // 字段信息
        this.columns = daa040.cols ? daa040.cols : this.supdata.getStColums(resdata.message.jg);
        this.columns.map(col => {
          if (col.f && col.f === "percent") {
            col.format = (item: STData, c: STColumn) => (item[c.index.toString()] * 100).toFixed(0) + "%"
          }
        });

        // 数据
        let data = this.supdata.rebuildData(resdata.message.data, resdata.message.tb, groupFileds, groupFileds);

        if (this.RMFiled) {
          let data1 = this.supdata.dataFold(data, resdata.message.tb.DAA035.split(','));
          let fileds: string[];
          data1 = this.supdata.rebuildDsDataToLabel(data1);
          [data1, fileds] = this.supdata.dataPartition(data1, this.RMFiled, "值");

          const newcols: STColumn[] = [];
          newcols.push({ title: "指标", index: "DAA020" });
          fileds.forEach(f => {
            newcols.push({ title: f, index: f, type: "number" });
          })

          data = data1;
          this.columns = newcols;

        }

        data = this.supdata.rebuildDsDataToLabel(data);
        this.st.data = data;
        this.st.resetColumns({ emitReload: true, columns: this.columns });


      }
    });
  }

  ngOnChanges(changes: import("@angular/core").SimpleChanges): void {
    if (changes.querypara && !changes.querypara.firstChange) {
      this.st.loading = true;
      this.lbservice.lbservice('TB_TBXX', { para: { DAA030: this.DAA030, Q: this.querypara } }).then(resdata => {
        this.st.loading = false;
        if (resdata.code < 1) {
          this.message.error(resdata.errmsg);
        }
        else {

          // 图表信息
          const groupFileds: string[] = resdata.message.tb.DAA036.split(',');
          let data = [... this.supdata.rebuildData(resdata.message.data, resdata.message.tb, groupFileds, groupFileds)];

          if (this.RMFiled) {
            if (this.RMFiled) {
              let data1 = this.supdata.dataFold(data, resdata.message.tb.DAA035.split(','));
              let fileds: string[];
              data1 = this.supdata.rebuildDsDataToLabel(data1);
              [data1, fileds] = this.supdata.dataPartition(data1, this.RMFiled, "值");
              data = data1;

            }
          }

          data = this.supdata.rebuildDsDataToLabel(data);
          this.st.data = data;
          this.st.reset();
        }

      });
    }

  }

  titleClick(c) {
  }

  change(event) {
    if (event.type === 'click') {
    }
  }



  getTarget() {

    return Math.round(this.supdate.getDayinYear() / this.supdate.getYearDays() * 10000) / 100;

  }

  getPercentString(item: any,c:any) {
    return this.getPercent(item,c).toFixed()+"%";
  } 

  getPercent(item: any,c:any) {
    
    const val = item[c.indexKey];
    // console.log(val,item,c);
    return val * 100;
  } 
  getColor(item,c) {
    const val = item[c.indexKey];
    return val > this.getTarget() ? "#F78F28" : "#1890FF";


  }
}