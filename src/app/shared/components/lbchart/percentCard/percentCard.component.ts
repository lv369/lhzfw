import { Component, Input, TemplateRef } from '@angular/core';
import {  InputBoolean, InputNumber } from 'ng-zorro-antd';


@Component({
  selector: 'lb-percent',
  templateUrl: './percentCard.component.html',
})
export class LbPercentCardComponent{

 @Input() title:string;
 @Input() @InputBoolean() bordered=true;
 @Input() footer: string | TemplateRef<void>;
 @Input() color = '#13C2C2';
 @Input() total:string;
 @Input() contentHeight:string; 
 @Input() @InputNumber() percent:number;
 @Input() @InputNumber() target:number;


}