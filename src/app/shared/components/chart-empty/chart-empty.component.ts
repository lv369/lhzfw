import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-chart-empty',
  templateUrl: './chart-empty.component.html',
  styleUrls: ['./chart-empty.component.less']
})
export class ChartEmptyComponent implements OnInit {
  @Input() margintop = 0;

  constructor() { }

  ngOnInit() {
  }

}
