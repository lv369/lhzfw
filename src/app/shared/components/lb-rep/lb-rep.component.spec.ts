import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LbRepComponent } from './lb-rep.component';

describe('LbRepComponent', () => {
  let component: LbRepComponent;
  let fixture: ComponentFixture<LbRepComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LbRepComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LbRepComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
