import { Component, OnInit, Input } from '@angular/core';
import { RepserviceService } from '@core/lb/repservice.service';
import { SafeResourceUrl, DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-lb-rep',
  templateUrl: './lb-rep.component.html',
  styleUrls: ['./lb-rep.component.less'],
})
export class LbRepComponent implements OnInit {
  @Input() repid: string;
  @Input() inpara: any;
  label01: string;

  url = '/webroot/decision/view/report?viewlet=test01.cpt';
  url1: any;
  ifrmeee: SafeResourceUrl;

  constructor(
    private sanitizer: DomSanitizer,
    private repservice: RepserviceService,
  ) {}

  ngOnInit() {
    this.repservice.getRep(this.repid, this.inpara).then(resdata => {
      this.url1 = resdata;
      console.log('this.url1 ==========' + this.url1);
      if (this.url1 != null) {
        document.getElementById('div1').style.display = 'block';
        this.ifrmeee = this.sanitizer.bypassSecurityTrustResourceUrl(this.url1);
      } else {
        this.label01 = '不存在对应报表';
        this.ifrmeee = this.sanitizer.bypassSecurityTrustResourceUrl(this.url);
        document.getElementById('div2').style.display = 'block';
      }
    });
  }
}
