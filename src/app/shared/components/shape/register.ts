// 导入画布的注册函数
import { registerNode } from 'topology-core/middles';
import {customNode} from './myShape';
import {customNodeAnchors} from './myShape.achor';
import {customNodeIconRect,customNodeTextRect} from './myShape.react';
// 导入自己的图形库函数（前面编写的4个函数）


// 注册到画布
export function register() {

    registerNode('item', customNode);
}