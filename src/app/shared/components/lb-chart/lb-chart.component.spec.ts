import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LbChartComponent } from './lb-chart.component';

describe('LbChartComponent', () => {
  let component: LbChartComponent;
  let fixture: ComponentFixture<LbChartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LbChartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LbChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
