import { Component, OnInit, Input } from '@angular/core';
import { SFSchema, SFSelectWidgetSchema } from '@delon/form';
import { NzMessageService } from 'ng-zorro-antd';
import { SupData } from '@core/lb/SupData';
import { HttpService } from 'lbf';
@Component({
  selector: 'app-lb-chart',
  templateUrl: './lb-chart.component.html',
  styleUrls: ['./lb-chart.component.less']
})
export class LbChartComponent implements OnInit {
  @Input() DAA030: string
  @Input() type: string
  @Input() height: number = 300
  @Input() daa020: Array<string>
  @Input() cols: string
  @Input() gwhere: string

  @Input() dx: string
  @Input() dz: string
  @Input() istransxz: boolean

  @Input() data: any

  @Input() chartstyle: any

  @Input() selectedItmes
  @Input() computedZbObj

  @Input() outerheadx: Array<{label: string, value: string}>
  @Input() outerheadz: Array<{label: string, value: string}>

  @Input() poly

  @Input() sortx = true
  @Input() sortz
  @Input() sortrule = 'ASC'

  tfdata: Array<object>

  keymap: any

  position: any

  constructor(public msg: NzMessageService, private httpService: HttpService, private supdata: SupData) {}

  transformdata(data, computedzbobj, selecteditems) {
    // let daa020 = this.daa020
    const self = this
    let datas=[];
    let z 
    const keymap = {}

    // let datatemp
    // 计算显示问题
    const dae005 = computedzbobj
    let orgData: any[] = data
    if (dae005 && Object.keys(dae005).length > 0) {
        Object.keys(dae005).forEach((k: string) => {
          const dae = dae005[k];
          if (dae.J) {
            const bl = dae.J.split("*") 
            orgData = this.supdata.rescalc(orgData, {
              key: k,
              name: k
            }, dae.J, this.dx, this.dz)
          }
          // if (dae.D) {
          //   row[k] = row[k] + dae.D
          // }
        })
    }

    console.log(orgData)

    if(selecteditems.length > 1) {
      for(const key of Object.keys(orgData)) {
        if(selecteditems.indexOf(key) > -1) {
          datas = [...datas, ...orgData[key].list.map(m=>{
            return {
             dz: orgData[key].DAA022,
             ...m,
            }
           })]
           keymap[key] = orgData[key].DAA022
        }
      }
      this.dz = 'dz'
      z = 'dz'
    } else if (selecteditems.length == 1){
      selecteditems.forEach(key => {
        datas = orgData[key].list

        keymap[key] = orgData[key].DAA022
      })
    }
   
    return {datas, z, keymap}
   }

  queryCommonData() {
    console.log(this.gwhere)
    return new Promise((resolve, reject) => {
      if(this.daa020) {
        let cols = ''
        if(this.dx) cols += this.dx
        if(this.dz) {
          if(cols ) {
            cols += `,${this.dz}`
          } else {
            cols += this.dz
          }
        }
        if(Array.isArray(this.daa020)) {
          let daa020 = ''
          this.daa020.forEach(ele => {
            daa020 += ele + ','
          })
          if(daa020) {
            daa020.substr(0, daa020.length - 1)
            this.httpService.lbservice('FR_getitemdata', {
              para: {
                daa020,
                cols: cols?cols:undefined,
                poly: this.poly,
                gwhere: this.gwhere
              }
            })
              .then(res => {
                if(res.code < 1) {
                  reject(res.errmsg)
                  return
                } 
                console.log(daa020)
                console.log(res.message.data)
                const {datas, z, keymap} = this.transformdata(res.message.data,this.computedZbObj, this.selectedItmes)

                let position
                if(this.dx) {
                  position = {
                    x: this.dx,
                    y: 'VAL',
                    z: z ? z: this.dz
                  }
                } else {
                  position = {
                    x: this.dz,
                    y: 'VAL',
                  }
                }
                resolve({
                  data: datas,
                  type: this.type,
                  position,
                  keymap,
                })
              })
              .catch(err => {
                reject(err)
              })
          } else {
            reject('指标数组为空')
          }
        } else {
          // 单指标转换个屁
          this.httpService.lbservice('FR_getitemdata', {
            para: {
              daa020: this.daa020,
              cols: cols?cols:undefined,
              poly: this.poly,
              gwhere: this.gwhere
            }
          })
            .then(res => {
              if(res.code < 1) {
                reject(res.errmsg)
                return
              } 
              const {datas, z, keymap } = this.transformdata(res.message.data,this.computedZbObj, this.selectedItmes)
              let position
                if(this.dx) {
                  position = {
                    x: this.dx,
                    y: 'VAL',
                    z: z ? z: this.dz
                  }
                } else {
                  position = {
                    x: this.dz,
                    y: 'VAL',
                  }
                }
              resolve({
                data: datas,
                type: this.type,
                position,
                keymap,
              })
            })
            .catch(err => {
              reject(err)
            })
        }
      } else {
        // 传入的数据就不用转换了
        resolve({
          data: this.data,
          type: this.type,
          position: {
            x: this.dx,
            y: 'VAL',
            z: this.dz
          },
        })
        
      }
    })
  }

  submit(value: any) {
    this.msg.success(JSON.stringify(value));
  }

  ngOnInit() {
    console.log(this.dx)
    console.log(this.dz)
    this.queryCommonData()
      .then((res: any) => {
        const {data, type, position, keymap} = res
        // data = this.supdata.rebuildDsDataToLabel(data);
        let temp = 
        this.outerheadx !== undefined && this.outerheadx.length > 0 ? data.filter(ele => {
          return this.outerheadx.find(ele1 => {
            return ele[this.dz] === ele1.value
          }) !== undefined
        }) : data

        temp = this.outerheadz !== undefined && this.outerheadz.length > 0 ? data.filter(ele => {
          return this.outerheadz.find(ele1 => {
            return ele[this.dz] === ele1.value
          }) !== undefined
        }) : data

        console.log(temp)
        const dv = new DataSet.View().source(temp);

        if(position.z && this.istransxz) { // 转置指标项与维度
          const px = position.x
          position.x = position.z
          position.z = px
        }
        if(this.sortx && !this.sortz) {
          dv.transform({
            type: 'sort-by',
            fields: [ position.x ], // 根据指定的字段集进行排序，与lodash的sortBy行为一致
            order: this.sortrule,        // 默认为 ASC，DESC 则为逆序
          })
        } else if(!this.sortx && this.sortz){
          dv.transform({
            type: 'sort-by',
            fields: [ position.z ], // 根据指定的字段集进行排序，与lodash的sortBy行为一致
            order: this.sortrule,   // 默认为 ASC，DESC 则为逆序
          })
        } else if(this.sortx && this.sortz) {
          dv.transform({
            type: 'sort-by',
            fields: [ position.x, position.z ], // 根据指定的字段集进行排序，与lodash的sortBy行为一致
            order: this.sortrule,   // 默认为 ASC，DESC 则为逆序
          })
        }    

        console.log(dv.rows)
        this.tfdata = this.supdata.rebuildDsDataToLabel(dv.rows)
        console.log(this.tfdata)
        this.position = position
        console.log(this.position)
        this.keymap = keymap
      })
  }

}
