import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-panel-wrapper',
  templateUrl: './panel-wrapper.component.html',
  styleUrls: ['./panel-wrapper.component.less']
})
export class PanelWrapperComponent implements OnInit {
  @Input() showheader = true;
  @Input() border = true;

  constructor() { }

  ngOnInit() {
  }

}
