import { OnInit, Component, Input, ViewChild, ElementRef, EventEmitter, Output, OnChanges, SimpleChanges } from '@angular/core';
import { NzMessageService, NzModalService, UploadXHRArgs, UploadFile, InputNumber, UploadChangeParam, InputBoolean } from 'ng-zorro-antd';
import { HttpService } from 'lbf';
import { Observable, Observer, Subscription } from 'rxjs';
import Viewer from 'viewerjs';

@Component({
  selector: 'lb-wjfw',
  templateUrl: './lbwjfw.component.html',
  styleUrls: ['./lbwjfw.component.less']
})
export class LbWjfwComponent implements OnInit, OnChanges {

  // @Input() @InputNumber() DAD069: number;
  // @Input() @InputNumber() DAD028: number;
  // @Input() @InputNumber() DAD017: number;

  @Input() snames: FileSname;
  @Input() param: any;
  // LL 最大文件数
  @Input() maxFileCount: any=999;

  @ViewChild("imglist", { static: false }) imgList: ElementRef;
  /**
   * 图表列表
   */
  fileList = [];

  /**
   * 上传按钮是否显示
   */
  @Input() @InputBoolean() showButton = true;

  /**
   * 文件上传成功后回调
   */
  @Output() addDone = new EventEmitter();
  /**
   * 文件删除后回调
   */
  @Output() delDone = new EventEmitter();

  isVisible = false
  viewer;

  constructor(private msgsvr: NzMessageService,
    private lbservice: HttpService, ) {

  }
  ngOnChanges(changes: SimpleChanges): void {
 
    if (changes.param && !changes.param.firstChange) {

      this.getFiles();

    }
  }

  getFiles(): void {
    console.log(this.snames.query)
    console.log(this.param)

    if (!this.snames.query) return;

    this.lbservice.lbservice(this.snames.query, { para: this.param }).then(resdata => {

      if (resdata.code < 1) {
        this.msgsvr.error(resdata.errmsg);
        return;
      }

      const flist: any[] = resdata.message.list;
      const list = [];

      flist.forEach(f => {
        list.push({ uid: f.CZE001, size: f.CZE007, name: f.CZE006, filename: f.CZE006, url: f.URL, })
      })

      this.fileList = list;
    })
  }

  ngOnInit(): void {

    if (this.snames.query) {
      this.getFiles();
    }
  }

  imgClick(img) {

    if (this.viewer) {
      this.viewer.destroy()
    }

    this.viewer = new Viewer(this.imgList.nativeElement);
    this.viewer.show();
  }

  delImg(img) {

    if (!this.snames.del) {
      this.msgsvr.error('未配置删除服务')
      return;
    }

    this.lbservice.lbservice(this.snames.del, { para: { CZE001: img.uid } }).then(resdata => {

      if (resdata.code < 1) {
        this.msgsvr.error('删除失败！')
      } else {
        this.msgsvr.info("删除成功");
        const idx = this.fileList.findIndex(f =>  f.uid === img.uid );
        this.fileList.splice(idx, 1);

        

        this.delDone.emit({...this.param, CZE001: img.uid });
      }

    })

  }

  addFile() {

    this.isVisible = true;
  }

  handleCancel() {
    this.isVisible = false;
  }

  uploadFile = (item: UploadXHRArgs) => {

    return this.lbservice.lbfileupload(this.snames.save, item, { ...this.param, });

  }

  uploadChanged(event) {


    if (event.type === "success") {
      const resdata = event.file.response;
      if (resdata.code < 1) {
        this.msgsvr.error(resdata.errmsg);
      } else {

        const filelist = resdata.message.list;
        const list = [];

        filelist.forEach(f => {
          list.push({ uid: f.CZE001, size: f.CZE007, name: f.CZE006, filename: f.CZE006, url: f.URL, })
          this.addDone.emit({ ...this.param, CZE001: f.CZE001 })
        })


        this.fileList = [...this.fileList, ...list];
        this.isVisible = false;
        this.msgsvr.success('上传成功！');

      }
    }

  }

  callGpy(t) {
    this.msgsvr.error("暂不支持该型号高拍仪！")
  }

}

interface FileSname {
  /**
   * 文件上传
   */
  save?: string;
  /**
   * 文件预览
   */
  query: string;
  /**
   * 文件删除
   */
  del?: string;
}