import {
  OnInit,
  OnChanges,
  OnDestroy,
  Component,
  ChangeDetectionStrategy,
  ViewEncapsulation,
  Input,
  TemplateRef,
  NgZone,
  ViewChild,
  ElementRef,
  Output,
  EventEmitter,
  ViewContainerRef,
  ComponentFactoryResolver,
  ComponentFactory,
  AfterViewInit,
} from '@angular/core';
import { HttpService } from 'lbf';
import { NzMessageService, InputNumber, InputBoolean } from 'ng-zorro-antd';
import { Lbchart } from '@shared';
import { Subscription } from 'rxjs';
import { SupData } from '@core/lb/SupData';

@Component({
  selector: 'app-chart-wrapper',
  templateUrl: './chart-wrapper.component.html',
  styleUrls: ['./chart-wrapper.component.less'],
})
export class ChartWrapperComponent implements OnInit, OnChanges {
  heightUnit = 10;
  timetypes = ['SYEAR', 'SHALFYEAR', 'SQUARTER', 'SMONTH', 'SDAY', 'SWEEK'];

  @ViewChild('content', { read: ViewContainerRef, static: false })
  content: ViewContainerRef;

  @Input() height = 200
  @Input() headerHeight = 40
  @Input() footerHeight = 40
  @Input() title = '';
  @Input() contentHeight: number;
  @Input() showborder = true
  @Input() showheader = true;
  @Input() showfooter = false;
  @Input() showtimepicker = true;
  @Input() timetype = 'SYEAR';

  visible = false;

  @Input() sameProps: any[]   // 图表的共同维度，可用来配置上转下探和条件筛选

  // @Input() para;
  // @Output() paraChange = new EventEmitter();

  @Input() cols;
  @Output() colsChange = new EventEmitter();

  @Input() lbChartId

  selectedpp = -1
  @Input() perspective = [
  ];

  chartids = ['55', '54', '28']

  querypara = {}

  constructor(private resolver: ComponentFactoryResolver, private message: NzMessageService, private lbservice: HttpService, ) { }

  ngOnInit() {
    // this.selectedpp = this.perspective.findIndex
    // this.cols = this.perspective[this.selectedpp].list.join('||\''+this.perspective[this.selectedpp-1].joinkey+'\'||')
    this.querydata()
  }

  ngOnChanges(changes: import('@angular/core').SimpleChanges): void {

    if (changes.lbChartId) this.querydata()
  }

  querydata() {
    this.lbservice
      .lbservice('TB_TBXX', {
        para: { DAA030: this.lbChartId, Q: this.querypara, jstinfo: '1' },
      })
      .then(resdata => {
        if (resdata.code < 1) {
          this.message.error(resdata.errmsg);
        } else {
          try {
            const perspective = JSON.parse(resdata.message.tb.DAA042);
            this.perspective = perspective;
            this.selectedpp = perspective.findIndex(
              ele => ele.default === true,
            );
            if (this.cols) {

            }
            this.cols = this.selectedpp > -1 ? this.perspective[
              this.selectedpp
            ].list.join(
              "||'" +
              this.perspective[this.selectedpp]
                .joinkey +
              "'||",
            ) : null
            this.colsChange.emit(this.cols);

          } catch (err) {
            this.message.error('图表' + resdata.message.tb.DAA031 + '参数配置错误')
            this.colsChange.emit(this.cols);
          }
        }
      });
  }

  up() {
    console.log(this.perspective)
    // 上转， 上转下探其实就是x视角的变化，如果没有x视角，就没有上转下探，上转下探需有多个视角选择配置
    if (this.selectedpp > 0) {
      this.cols = this.perspective[this.selectedpp - 1].list.join('||\'' + this.perspective[this.selectedpp - 1].joinkey + '\'||')
      this.selectedpp = this.selectedpp - 1
      this.colsChange.emit(this.cols)
    } else {
      this.message.error('不能再上转了')
    }

  }

  down() {
    console.log(this.perspective)
    // 下探
    // 上转， 上转下探其实就是x视角的变化，如果没有x视角，就没有上转下探，上转下探需有多个视角选择配置
    if (this.selectedpp < this.perspective.length - 1) {
      this.cols = this.perspective[this.selectedpp + 1].list.join('||\'' + this.perspective[this.selectedpp + 1].joinkey + '\'||')
      this.selectedpp = this.selectedpp + 1
      this.colsChange.emit(this.cols)
    } else {
      this.message.error('不能再下探了')
    }
  }

  close() {
    this.visible = false;
  }

  onChange(e) {
    console.log(e);
  }

  onOk(e) { }
}
