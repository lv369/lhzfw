import { Component, OnInit, Input, Output,EventEmitter } from '@angular/core';


@Component({
  selector: 'app-error-info',
  templateUrl: './error-info.component.html',
  styleUrls: ['./error-info.component.less']
})
export class ErrorInfoComponent implements OnInit {

  @Input() items = [
    { label: 'Apple', value: 'Apple', checked: true },
    { label: 'Pear', value: 'Pear' },
    { label: 'Orange', value: 'Orange' }
  ];

  @Output() ok = new EventEmitter<string>();
  @Output() cancle = new EventEmitter<string>();
  
  inputValue: string;

  constructor() { }

  ngOnInit() {
  }

  log(value: object[]): void {
    // console.log( value)
    this.inputValue = '';
    value.map((v,i)=>{
      this.inputValue+=(i+1)+'. '+v+'\r\n';
    })
  }

  okClick($event){
      this.ok.emit(this.inputValue)
  }
  cancleClick($event){
      this.cancle.emit('cancle')
  }
}
