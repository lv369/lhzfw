import { Component, OnInit, ViewChild, Output } from '@angular/core';
import { _HttpClient, ModalHelper } from '@delon/theme';
import { NzMessageService, NzFormatEmitEvent, NzTreeNode } from 'ng-zorro-antd';
import { HttpService, SupDic } from 'lbf';
import { GrFormQueryComponent } from './gr_form_query';
import { DwFormQueryComponent } from './dw_form_query';
import { SFSchema, SFComponent } from '@delon/form';

@Component({
  selector: 'tyekou-sjsp',
  templateUrl: './tyekou_sjsp.html',  
  styleUrls: ['./tyekou_sjsp.less'],
})
export class TyekouSjspComponent implements OnInit {
  topTitle = '申请信息详情';

  isInfoVisible = false; // 是否显示
  isSelectVisible = false; // 选择框是否显示
  showTitle = ''; // 显示的标题

  // 按钮组
  isBtnXz = false; // 派给乡镇


  styleParam = {}
  hidden_GrForm = true; // 个人申请表单
  hidden_DwForm = true; // 单位申请表单

  dataAYW001:number; // 事项编码
  dataAYW002:string; // 事项名称
  dataAYW004:string; // 事项主体类型
  dataSXLY:string; // 事项来源
  dataBMMC:string; // 部门名称
  dataBMLX:string; // 部门类型
  dataBMID:string; // 部门ID
  

  showInfo:string; // 提示框内容

  bmSchema: SFSchema= {
    properties: {
    
      BM: {
        type: 'string',
        title: '中心或部门',     
        enum: [{ title: '调解服务中心', key: 'ZX81' },{ title: '诉讼服务中心', key: 'ZX82' },{ title: '人民来访接待中心', key: 'ZX83' },
        { title: '公共法律服务中心', key: 'ZX84' },{ title: '社会力量帮扶中心', key: 'ZX85' },{ title: '劳动仲裁服务中心', key: 'ZX86' }
      ],        
        ui:{
          widget:'tree-select',
          dropdownStyle: {
            height: '200px'
          },
          expandChange: (e: NzFormatEmitEvent)=>this.getSdTree(e)
        }
      }
    },
    ui: {
      spanLabelFixed: 100,
      grid: {
        span: 24,
      },
    },
    required: ['BM']
  }
  
  // 部门指派页
  @ViewChild('selectBm', { static: false }) selectBm: SFComponent;

  @Output() set AYW009(value:number){
    this.dataAYW009 = value;
    // 初始化其他信息
    this.lbservice.lbservice('TYJB_DATA_INIT', {FLAG:'QUERY-SH',AYW009:this.dataAYW009 }).then(resdata => {
      console.log('-------query----------')
      if (resdata.code < 1) {
        this.msgSrv.error(resdata.errmsg);
      } else {
        console.log(resdata)
        this.dataAYW001 = resdata.message.AYW001;
        this.dataAYW002 = resdata.message.AYW002;
        this.dataAYW004 = resdata.message.AYW004;   
        this.dataSXLY = resdata.message.SXLY;   
        this.dataBMMC = resdata.message.BMMC;   
        this.dataBMLX = resdata.message.BMLX;   
        this.dataBMID = resdata.message.BMID;   
        
        if(this.dataAYW004==='1'){
          this.showDwForm();
        }
        else{
          this.showGrForm();
        }
        if(this.dataSXLY==='2'){
          this.showTitle = '<font color="red"><b>【流水号：'+this.dataAYW009+'】</b></font>--其他平台（信件）';
          this.isBtnXz = false;
        }
        else if(this.dataSXLY==='3'){
          this.showTitle = '<font color="red"><b>【流水号：'+this.dataAYW009+'】</b></font>--乡镇街道录入（'+this.dataBMMC+'）';
          this.isBtnXz = true; // 显示指派乡镇的按钮
          this.showInfo = '是否将该事项指派给申请镇街('+this.dataBMMC+')?'
        }
        else{
          this.showTitle = '<font color="red"><b>【流水号：'+this.dataAYW009+'】</b></font>--事项退回';
          this.isBtnXz = false;
        }
        
        this.isInfoVisible = true;
      }
    })
  }
  dataAYW009:number; // 业务流水号
  
   

  // 表单信息
  @ViewChild('grForm', { static: false }) grForm: GrFormQueryComponent; 
  // 表单信息
  @ViewChild('dwForm', { static: false }) dwForm: DwFormQueryComponent; 
 

  constructor(
    private lbservice: HttpService,
    public msgSrv: NzMessageService,
    private supDic: SupDic,
    ) { }

    node:NzTreeNode;
    getSdTree(e: NzFormatEmitEvent){
      this.node = e.node;
      this.lbservice.lbservice('sd_selecttree',{key:e.node.origin.key}).then(resdata=>{
         if(resdata.code<1)
         {
           this.msgSrv.error(resdata.errmsg);
         }
         else
         {
          if(this.node.getChildren().length === 0 && this.node.isExpanded)
          {
            this.node.addChildren(resdata.message.list);
          }
          
          
         }
      })
    }

  ngOnInit() {
    this.styleParam = {height: (window.innerHeight-250)+'px'}
  }

  ngAfterView(){
    // this.dwForm.isShowSf = true;
  }

  // 指派给乡镇
  ToXz() {
    this.lbservice.lbservice('TYJB_ZPBM', {para:{AYW009:this.dataAYW009,BMLX:this.dataBMLX,BMID:this.dataBMID}} ).then(resdata => {
      if (resdata.code < 1) {
        this.msgSrv.error(resdata.errmsg);
      } else {
        this.msgSrv.success('指派成功！');
        this.closeWindow('0')
      }
    })
    
  }

  ShowBmSelect(){
    this.isSelectVisible = true;
  }
  aryBm:Array<string>
  bmStr:string;
  //
  ToBm() {
    if(!this.selectBm.valid){
      this.msgSrv.error('请选择指派部门或中心');
      return;
    }
    this.bmStr = this.selectBm.getValue('/BM');
    /*
    if(this.aryBm.length===1){
      this.dataBMLX = '1';
      this.dataBMID = this.aryBm[0]
    }
    else{
      this.dataBMLX = '2';
      this.dataBMID = this.aryBm[1]
    }
    */
    this.lbservice.lbservice('TYJB_ZPBM', {para:{AYW009:this.dataAYW009,BMLX:'a',BMID:this.bmStr}} ).then(resdata => {
      if (resdata.code < 1) {
        this.msgSrv.error(resdata.errmsg);
      } else {
        this.msgSrv.success('指派成功！');
        this.closeWindow('0')
      }
    })
    
  }
 

  // 隐藏所有功能，所有按钮失效
  hiddenAll()
  {
    this.hidden_GrForm = true; // 个人申请表单
    this.hidden_DwForm = true; // 单位申请表单

  }

  /********************************************* */
  /********************按钮组事件**************** */
  /********************************************* */
  // 导航栏按钮单击事件
  btnClick(flag:string){
    // 申请信息
    if(flag==='info'){
       if(this.dataAYW004==='1'){
         this.showDwForm();
       }
       else{
         this.showGrForm();
       }
    }
    
    ///////////////////////////////////////////////////////////////////////////////////////////////

    
    else{
     
    }
    
  }

  // 显示个人申请表单
  showGrForm(){
   
    this.topTitle = '申请信息详情';    
    this.hiddenAll();

    // 流程图
    if(this.grForm.AYW009===undefined||this.grForm.AYW009!==this.dataAYW009){
      this.grForm.AYW009 = this.dataAYW009; // 业务流水号赋值
    }
    this.hidden_GrForm = false; // 个人申请表单
    
    
  }

  // 显示单位申请表单
  showDwForm(){
   
    this.topTitle = '申请信息详情';    
   
    this.hiddenAll();


    if(this.dwForm.AYW009===undefined||this.dwForm.AYW009!==this.dataAYW009){
      this.dwForm.AYW009 = this.dataAYW009; // 业务流水号赋值
    }
    this.hidden_DwForm = false; // 个人申请表单
    
  }

  

  


  closeWindow(flag:string){
    if(flag==='0'){
      this.isInfoVisible = false;
      this.isSelectVisible = false;
      // 弹出框关闭的时候，将流水号清空，下次再打开，就可以刷新内部信息了
    
      this.dwForm.AYW009 = undefined;
      this.grForm.AYW009 = undefined;
    }
    else{
      this.isSelectVisible = false;
    }
    
  }




  
  

}
