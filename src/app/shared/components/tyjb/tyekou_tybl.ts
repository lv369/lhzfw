import { Component, OnInit, ViewChild, Output } from '@angular/core';
import { _HttpClient } from '@delon/theme';
import { NzMessageService, NzModalService } from 'ng-zorro-antd';
import { HttpService, SupDic } from 'lbf';
import { PrintComponent } from '@shared/components/tyjb/lb_print';
import { UserSelectComponent } from '@shared/components/tyjb/lb_user_select';
import { SFSchema, SFComponent } from '@delon/form';
import { BlListComponent } from './lb_bllist';
import { TyekouQueryComponent } from './tyekou_query';
import { BlFormComponent } from './bl_form';

@Component({
  selector: 'tyekou-tybl',
  templateUrl: './tyekou_tybl.html',
  styleUrls: ['./tyekou_tybl.less'],
})
export class TyekouTyblComponent implements OnInit {
  topTitle = '我的办件清单';
  curStep = 0; // 当前步骤 0 首页 1 人员选择 2 单位选择 3 人员表单 4 单位表单 5 受理成功 6 不予受理 7 收件箱
  lastStep = 0; // 已完成的步骤

  // 控制页面显示 隐藏
  hidden_List = false;
  hidden_form = true; // 表单
  hidden_Print = true; // 打印界面
  hidden_userSelect = true; // 用户选择
  isVisible = false; //提示框

  // 按钮组
  isBtnList = false; // 显示列表
  isBtnInfo = false; // 查看详情
  isBtnCur = false; // 办结
  isBtnSave = false; // 延时申请
  isBtnSxzj = false; // 事项申请转交
  isBtnTj = false; // 调解
  isBtnQuery = false; // 刷新
  isBtnXt = false; // 部门协同邀约
  isBtnRollback = false;
  isResultVisible = false; // 调解办结弹出框
  isjudicial = true; // 司法确认
  styleParam = {}

  dataAYW001 = 0; // 事项编码
  dataAYW002 = ''; // 事项名称
  dataAYW004 = ''; // 事项主体类型
  dataAYW009: number; // 业务流水号
  dataYWINFO = ''; // 事项标题

  curParamId: number = -1;

  // 待办
  @ViewChild('bllist', { static: false }) bllist: BlListComponent;
  // 详情弹出框
  @ViewChild('queryInfo', { static: false }) queryInfo: TyekouQueryComponent;
  // 用户选择
  @ViewChild('userSelect', { static: false }) userSelect: UserSelectComponent;
  // 办理表单
  @ViewChild('blPage', { static: false }) blPage: BlFormComponent;
  // 办结表单
  @ViewChild('resultInfo', { static: false }) resultInfo: SFComponent;

  resultSchema: SFSchema = {
    properties: {
      AYW051: {
        type: 'string',
        title: '办理结果',
        enum: [{ label: '办理成功', value: '1' }, { label: '办理失败', value: '0' }],
        ui: {
          widget: 'radio',
          grid: {
            span: 24
          }
        },
      },
      AYW050: {
        type: 'string',
        title: '办理结果说明',
        ui: {
          widget: 'textarea',
          autosize: { minRows: 2, maxRows: 6 },
          grid: {
            span: 24
          }
        }
      },
      AYW020: {
        type: 'string',
        title: '事件简况',
        readOnly: true,
        ui: {
          widget: 'textarea',
          autosize: { minRows: 3, maxRows: 6 },
          grid: {
            span: 24
          }
        }
      },
      SJJK: {
        type: 'string',
        title: '事件简况补充说明',
        ui: {
          widget: 'textarea',
          autosize: { minRows: 2, maxRows: 6 },
          grid: {
            span: 24
          }
        }
      },
      FLAG: {
        type: 'string',
        title: '上报负责人',
        enum: [{ label: '是', value: '1' }, { label: '否', value: '0' }],
        ui: {
          widget: 'radio',
          grid: {
            span: 24
          }
        },
        default: '1',
      }
    },
    ui: {
      spanLabelFixed: 100,
      grid: {
        span: 24,
      },
    },
    required: ['AYW050', 'AYW051', 'FLAG']
  }


  constructor(
    private lbservice: HttpService,
    public msgSrv: NzMessageService,
    private supdic: SupDic
  ) { }

  ngOnInit() {
    this.styleParam = { height: (window.innerHeight - 180) + 'px' }
    this.S_hqgw();

  }

  // 获取岗位信息
  S_hqgw() {
    this.lbservice.lbservice('s_hqgwxx', {}).then(resdata => {
      if (resdata.code > 1) {
        this.isjudicial = true;
        this.isBtnCur = true;
      }

    })
  }


  /********************************************* */
  /********************初始化配置**************** */
  /********************************************* */

  // 去获取是否有传递参数
  @Output() todoIni1t() {
    this.lbservice.lbservice('PUBLIC_INIT', {}).then(resdata => {
      if (resdata.code < 1) {
        this.msgSrv.error(resdata.errmsg);
      }
      else {
        console.log(resdata)
        // 判断是否有初始化数据
        if (resdata.message.AYW009 !== undefined) {
          this.curParamId = resdata.message.AYW039;
          this.showInitList({ AYW009: resdata.message.AYW009 })

        }
        // 如果没有参数传递，默认打开首页
        else {
          if (this.curParamId < 0) {
            this.showSqList();
          }

        }
      }

    })
  }

  // 隐藏所有功能，所有按钮失效
  hiddenAll() {
    this.hidden_Print = true;
    this.hidden_List = true;
    this.hidden_form = true;
    this.hidden_userSelect = true;

    this.isBtnQuery = false; // 刷新
    this.isBtnSxzj = false; // 转交
    this.isBtnTj = false; // 调解
    this.isBtnCur = false; // 办结
    this.isBtnSave = false; //延时
    this.isBtnList = true; // 清单按钮
    this.isBtnXt = false;
    this.isjudicial = true;
    this.isBtnRollback = false;
    // 如果有业务流水号，则显示搁置按钮
    if (this.dataAYW009 !== undefined) {
      this.isBtnInfo = false; // 查看事项详情
    }
  }

  /********************************************* */
  /********************按钮组事件**************** */
  /********************************************* */
  // 导航栏按钮单击事件
  btnClick(flag: string) {
    // 详情
    if (flag === 'info') {
      this.showInfo();
    }

    /********************************/
    /***********办结*************/
    /********************************/
    else if (flag === 'cur') {
      this.openWindow('result')
    }

    /********************************/
    /*************我的办件***************/
    /********************************/
    else if (flag === 'list') {
      // this.dataAYW009 = undefined;
      this.lastStep = 0
      this.showSqList()
    }
    // 延时
    else if (flag === 'save') {
      this.blPage.TODO = 'yssqAdd';
    }
    else if (flag === 'sxzj') {
      this.blPage.TODO = 'zjsqAdd';
    }
    else if (flag === 'xt') {
      this.blPage.TODO = 'xtAdd';

    }


    /********************************/
    /************调解**************/
    /********************************/
    else if (flag === 'tj') {
      this.blPage.TODO = 'tjAdd'
    }
    /********************************/
    /************刷新**************/
    /********************************/
    else if (flag === 'query') {
      this.blPage.TODO = 'QUERY'
    }

    else {

    }

  }

  doRollback(flag: string) {
    if (flag === '1') {
      // 调用新增服务
      this.lbservice.lbservice('ROLLBACK_SAVE', { AYW009: this.dataAYW009 }).then(resdata => {
        if (resdata.code < 1) {
          this.msgSrv.error(resdata.errmsg);
        } else {
          this.msgSrv.success('退回操作完成')
          this.dataAYW009 = undefined; // 流水号清空
          this.lastStep = 0;
          this.showSqList();

        }
      })
    }
  }

  // 待办事项
  DBSX_Click(item: any) {
    this.dataAYW009 = item.AYW009;
    this.dataAYW001 = item.AYW001;
    this.dataAYW002 = this.supdic.getdicLabel('AYW001', item.AYW001);;
    this.dataAYW004 = item.ZTLX;
    this.dataYWINFO = item.YWINFO;

    //  判断要跳转的页面
    // 待受理-->表单页
    if (item.AYW034 === '7') {
      this.lastStep = 1;
      this.showForm();
    }
    else {
      // 显示详细信息
      this.showInfo();
    }


  }


  // 操作员指定
  UserSet(item: any) {
    // 调用新增服务
    this.lbservice.lbservice('TYJB_USER_SET', item).then(resdata => {
      if (resdata.code < 1) {
        this.msgSrv.error(resdata.errmsg);
      } else {
        this.msgSrv.success('申请登记完成')
        this.dataAYW009 = undefined; // 流水号清空
        this.lastStep = 0;

      }
    })

  }


  showInitList(where: any) {
    this.dataAYW009 = undefined;
    this.topTitle = '我的办件清单';
    this.hiddenAll();
    this.hidden_List = false; // 显示    
    this.isBtnList = false;
    this.bllist.Where = where;
    this.curStep = 0;
  }

  showSqList() {
    this.topTitle = '我的办件清单';
    this.hiddenAll();
    this.hidden_List = false; // 显示
    this.isBtnList = false;
    this.bllist.InitGrid = 0;
    this.curStep = 0;
  }

  showInfo() {
    this.queryInfo.AYW009 = this.dataAYW009;
  }




  // 显示表单
  showForm() {

    this.topTitle = this.dataYWINFO;
    this.curStep = 1;
    this.hiddenAll();
    this.blPage.AYW009 = this.dataAYW009;
    /* 
    if(this.blData.AYW043!==undefined){

    }
    */
    this.isBtnTj = true;
    this.isBtnCur = true;//
    this.isBtnQuery = true;
    this.isBtnSxzj = true;
    this.isBtnInfo = true;
    this.isBtnSave = true;
    this.isBtnRollback = true;
    this.isBtnXt = true;
    this.hidden_form = false;
    this.isjudicial = true;

  }


  // 打开弹出框
  openWindow(flag: string) {
    if (flag === "result") {
      this.lbservice.lbservice('SJJK_QUERY', { AYW009: this.dataAYW009 }).then(resdata => {
        if (resdata.code < 1) {
          this.msgSrv.error(resdata.errmsg);
        } else {
          this.resultInfo.setValue('/AYW020', resdata.message.AYW020);
          this.isResultVisible = true;
        }
      })
    }
  }

  closeWindow(flag: string) {
    if (flag === "result") {
      this.isResultVisible = false;
    }
  }

  save(flag: string) {
    if (flag === 'result') {
      if (!this.resultInfo.valid) {
        this.msgSrv.error('表单必输项不能为空');
        return;
      }
      // ****************************************ssss
      // 获取当前所在部门
      this.lbservice.lbservice('QUERY_DGB010', { AYW009: this.dataAYW009, para: this.resultInfo.value }).then(resdata => {
        if (resdata.code >= 1) {
          this.isVisible = true;
        } else {
          // 调用新增服务
          this.lbservice.lbservice('TYJB_RESULT_SAVE', { AYW009: this.dataAYW009, para: this.resultInfo.value, FLAG: '0' }).then(resdata => {
            if (resdata.code < 1) {
              this.msgSrv.error(resdata.errmsg);
            } else {
              this.msgSrv.success('提交成功')
              this.closeWindow('result')
              this.dataAYW009 = undefined; // 流水号清空
              this.lastStep = 0;
              this.showSqList();

            }
          })

        }
      })
    }
  }

  handleOk(): void {
    this.lbservice.lbservice('TYJB_RESULT_SAVE', { AYW009: this.dataAYW009, para: this.resultInfo.value, FLAG: '1' }).then(resdata => {
      if (resdata.code < 1) {
        this.msgSrv.error(resdata.errmsg);
      } else {
        this.msgSrv.success('提交成功')
        this.closeWindow('result')
        this.dataAYW009 = undefined; // 流水号清空
        this.lastStep = 0;
        this.showSqList();

      }
    })
    this.isVisible = false;
  }

  handleCancel(): void {
    this.isVisible = false;
    // 调用新增服务
    this.lbservice.lbservice('TYJB_RESULT_SAVE', { AYW009: this.dataAYW009, para: this.resultInfo.value, FLAG: '0' }).then(resdata => {
      if (resdata.code < 1) {
        this.msgSrv.error(resdata.errmsg);
      } else {
        this.msgSrv.success('提交成功')
        this.closeWindow('result')
        this.dataAYW009 = undefined; // 流水号清空
        this.lastStep = 0;
        this.showSqList();

      }
    })

  }

}
