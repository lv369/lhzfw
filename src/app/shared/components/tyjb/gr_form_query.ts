import { Component, OnInit, ViewChild, Input, Output, EventEmitter } from '@angular/core';
import { HttpService, SupDic, GridComponent } from 'lbf';
import { SFComponent, SFSchema, } from '@delon/form';
import { STColumn, STColumnTag } from '@delon/abc';
import { NzMessageService } from 'ng-zorro-antd';


@Component({
  selector: 'gr-form-query',
  templateUrl: './gr_form_query.html',
  styleUrls: ['./lb_sxlist.less'],
})
export class GrFormQueryComponent implements OnInit {
  queryPara = {}
  paramStyle = { height: (window.outerHeight - 262) + 'px', overflow: 'auto' }

  TAG: STColumnTag = {
    1: { text: '单位', color: 'green' },
    2: { text: '个人', color: 'red' }
  };

  // 申请人列表
  sqColumns: STColumn[] = [

    { title: '主体类型', index: 'ZTLX', type: 'tag', tag: this.TAG },
    { title: '主体编号', index: 'ZTBH' },
    { title: '主体名称', index: 'ZTMC' },
    { title: '乡镇街道', index: 'CAA001', dic: 'CAA001' },
    { title: '村社区', index: 'AAE025', dic: 'AAE025' },
    { title: '详细地址', index: 'ADDR' },
    { title: '联系电话', index: 'PHONE' },

  ];
  // 被申请人列表
  bsqColumns: STColumn[] = [

    { title: '主体类型', index: 'ZTLX', type: 'tag', tag: this.TAG },
    { title: '主体编号', index: 'ZTBH' },
    { title: '主体名称', index: 'ZTMC' },
    { title: '乡镇街道', index: 'CAA001', dic: 'CAA001' },
    { title: '村社区', index: 'AAE025', dic: 'AAE025' },
    { title: '详细地址', index: 'ADDR' },
    { title: '联系电话', index: 'PHONE' },

  ];

  // 矛盾纠纷表单
  mdjfSchema: SFSchema = {
    properties: {
      AYW014: {
        type: 'string',
        title: '发生日期',
        readOnly: true,
        ui: {
          widget: 'date',
        }
      },
      FSTIME: {
        type: 'string',
        title: '发生时间',
        readOnly: true,
        ui: { widget: 'time' }
      },
      AYW017: {
        type: 'string',
        title: '发生日期表述',
        readOnly: true
      },
      AYW015: {
        type: 'string',
        title: '发生地点',
        readOnly: true
      },
      CAA001: {
        type: 'number',
        title: '所属镇街',
        readOnly: true,
        enum: this.supDic.getSFDic('CAA001'),
        ui: {
          widget: 'select'
        }
      },
      AYW016: {
        type: 'number',
        title: '来访人数',
        readOnly: true,
        enum: this.supDic.getSFDic('AYW016'),
        ui: {
          widget: 'select'
        }
      },
      AYW018: {
        type: 'string',
        title: '重大事件',
        readOnly: true,
        enum: [{ label: '是', value: '1' }, { label: '否', value: '0' }],
        ui: {
          widget: 'radio',
        },
      },
      AYW019: {
        type: 'string',
        title: '紧急事件',
        readOnly: true,
        enum: [{ label: '是', value: '1' }, { label: '否', value: '0' }],
        ui: {
          widget: 'radio',
        },
      },
      AYW020: {
        type: 'string',
        title: '事件简况',
        readOnly: true,
        ui: {
          widget: 'textarea',
          autosize: { minRows: 3, maxRows: 10 },
          grid: {
            span: 24
          }
        },

      },
    },
    ui: {
      spanLabelFixed: 100,
      grid: {
        span: 6,
      },
    }
  }

  // 被申请列表
  @ViewChild('sqGrid', { static: false }) sqGrid: GridComponent;
  // 被申请列表
  @ViewChild('bsqGrid', { static: false }) bsqGrid: GridComponent;
  // 矛盾纠纷表单
  @ViewChild('mdjfForm', { static: false }) mdjfForm: SFComponent;


  @Output() set AYW009(value: number) {
    this.dataAYW009 = value;
    if (this.dataAYW009 !== undefined) {
      this.queryPara = { AYW009: value }
      // 初始化其他信息
      this.lbservice.lbservice('TYJB_DATA_INIT', { FLAG: 'QUERY_FORM', AYW009: this.dataAYW009 }).then(resdata => {
        if (resdata.code < 1) {
          this.msgSrv.error(resdata.errmsg);
        } else {
          this.setGrData();
          this.setInfoData(resdata.message.INFO)
        }
      })
    }

  }
  get AYW009() {
    return this.dataAYW009;
  }
  dataAYW009: number

  setInfoData(record: any) {
    this.mdjfForm.setValue('/AYW020', record.AYW020) // 事件简况
    this.mdjfForm.setValue('/AYW019', record.AYW019) // 紧急事件
    this.mdjfForm.setValue('/AYW018', record.AYW018) // 重大事件
    this.mdjfForm.setValue('/AYW017', record.AYW017) // 发生日期表述
    this.mdjfForm.setValue('/AYW016', record.AYW016) // 来访人数
    this.mdjfForm.setValue('/AYW015', record.AYW015) // 发生地点
    this.mdjfForm.setValue('/CAA001', record.CAA001) // 所属镇街
    this.mdjfForm.setValue('/FSTIME', record.FSTIME) // 发生时间
    this.mdjfForm.setValue('/AYW014', record.AYW014) // 发生日期
  }
  // 申请人信息赋值
  setGrData() {
    this.bsqGrid.page.show = false;
    this.sqGrid.page.show = false;
    // 列表刷新
    this.sqGrid.reload({ AYW009: this.dataAYW009 });
    this.bsqGrid.reload({ AYW009: this.dataAYW009 });
  }

  constructor(
    private lbservice: HttpService,
    private supDic: SupDic,
    public msgSrv: NzMessageService,
  ) {

  }

  ngOnInit() {

  }


}