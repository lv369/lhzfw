import { Component, OnInit, ViewChild, Input, Output, EventEmitter } from '@angular/core';
import { HttpService, SupDic, GridComponent } from 'lbf';
import { SFComponent, SFSchema } from '@delon/form';
import { STColumn, STColumnTag } from '@delon/abc';
import { NzMessageService } from 'ng-zorro-antd';
import { DwComponent } from './lb_dw';
import { GrComponent } from './lb_gr';
import { of } from 'rxjs';
import { delay } from 'rxjs/operators';


@Component({
    selector: 'sj-form',
    templateUrl: './sj_form.html',
    styleUrls: ['./lb_sxlist.less'],
})
export class SjFormComponent implements OnInit {

    isDwVisible = false; // 是否弹出申请单位选择框
    isGrVisible = false; // 是否弹出申请个人选择框
    isDwVisible1 = false; // 是否弹出被申请单位选择框
    isGrVisible1 = false; // 是否弹出被申请个人选择框

    dataZTBH: number // 主体编号
    dataAYW009: number // 业务流水号

    queryPara = {}
    paramStyle = { height: (window.outerHeight - 262) + 'px', overflow: 'auto' }

    TAG: STColumnTag = {
        1: { text: '单位', color: 'green' },
        2: { text: '个人', color: 'red' }
    };
    // 申请人列表
    sqrColumns: STColumn[] = [
        { title: '主体类型', index: 'ZTLX', type: 'tag', tag: this.TAG },
        { title: '主体编号', index: 'ZTBH' },
        { title: '主体名称', index: 'ZTMC' },
        { title: '乡镇街道', index: 'CAA001', dic: 'CAA001' },
        { title: '村社区', index: 'AAE025', dic: 'AAE025' },
        { title: '详细地址', index: 'ADDR' },
        { title: '联系电话', index: 'PHONE' },
        {
            title: '操作',
            fixed: 'right',
            buttons: [
                {
                    text: '删除',
                    icon: 'delete',
                    pop: '确定要删除该被申请(人/单位)的信息吗？',
                    click: (record: any) => this.sqrDel(record.AYW037),
                },
            ],
        },
    ];

    // 被申请人列表
    bsqColumns: STColumn[] = [

        { title: '主体类型', index: 'ZTLX', type: 'tag', tag: this.TAG },
        { title: '主体编号', index: 'ZTBH' },
        { title: '主体名称', index: 'ZTMC' },
        { title: '乡镇街道', index: 'CAA001', dic: 'CAA001' },
        { title: '村社区', index: 'AAE025', dic: 'AAE025' },
        { title: '详细地址', index: 'ADDR' },
        { title: '联系电话', index: 'PHONE' },
        {
            title: '操作',
            fixed: 'right',
            buttons: [
                {
                    text: '删除',
                    icon: 'delete',
                    pop: '确定要删除该被申请(人/单位)的信息吗？',
                    click: (record: any) => this.bsqDel(record.AYW037),
                },
            ],
        },
    ];

    // 矛盾纠纷表单
    mdjfSchema: SFSchema = {
        properties: {
            AYW014: {
                type: 'string',
                title: '发生日期',
                ui: { widget: 'date' }
            },
            FSTIME: {
                type: 'string',
                title: '发生时间',
                ui: { widget: 'time' }
            },
            AYW017: {
                type: 'string',
                title: '发生日期表述',
                ui: {
                    placeholder: '大致发生时间，非必填项',
                }
            },
            AYW015: {
                type: 'string',
                title: '发生地点',
                ui: {
                    placeholder: '详细地址，非必填项',
                }
            },
            CAA001: {
                type: 'string',
                title: '所属镇街',
                enum: this.supDic.getSFDic('CAA001'),
                ui: {
                    widget: 'select'
                }
            },
            AYW016: {
                type: 'number',
                title: '来访人数',
                enum: this.supDic.getSFDic('AYW016'),
                ui: {
                    widget: 'select'
                }
            },
            AYW018: {
                type: 'string',
                title: '重大事件',
                enum: [{ label: '是', value: '1' }, { label: '否', value: '0' }],
                ui: {
                    widget: 'radio',
                },
                default: '0',
            },
            AYW019: {
                type: 'string',
                title: '紧急事件',
                enum: [{ label: '是', value: '1' }, { label: '否', value: '0' }],
                ui: {
                    widget: 'radio',
                },
                default: '0'
            },
            AYW020: {
                type: 'string',
                title: '事件简况',
                ui: {
                    widget: 'textarea',
                    autosize: { minRows: 3, maxRows: 10 },
                    grid: {
                        span: 24
                    }
                }
            },
        },
        ui: {
            spanLabelFixed: 100,
            grid: {
                span: 6,
            },
        },
        required: ['CAA001', 'AYW016', 'AYW020']
    }

    // 申请单位选择界面
    @ViewChild('dw', { static: false }) dw: DwComponent;
    // 申请个人选择界面
    @ViewChild('gr', { static: false }) gr: GrComponent;
    // 申请人列表
    @ViewChild('sqrGrid', { static: false }) sqrGrid: GridComponent;
    // 被申请单位选择界面
    @ViewChild('dw1', { static: false }) dw1: DwComponent;
    // 被申请个人选择界面
    @ViewChild('gr1', { static: false }) gr1: GrComponent;
    // 被申请列表
    @ViewChild('bsqGrid', { static: false }) bsqGrid: GridComponent;
    // 矛盾纠纷表单
    @ViewChild('mdjfForm', { static: false }) mdjfForm: SFComponent;

    @Output() set AYW009(value: number) {
        this.dataAYW009 = value;
        this.queryPara = { AYW009: value }
        if (value !== undefined) {
            // 初始化其他信息
            this.lbservice.lbservice('TYJB_DATA_INIT', { FLAG: 'FORM', AYW009: this.dataAYW009 }).then(resdata => {
                if (resdata.code < 1) {
                    this.msgSrv.error(resdata.errmsg);
                } else {
                    //this.setInfoData(resdata.message.INFO)
                    this.mdjfForm.reset()
                }
            })
        }
        this.sqrGrid.reload(this.queryPara);
        this.bsqGrid.reload(this.queryPara);
    }
    get AYW009() {
        return this.dataAYW009;
    }

    // 获取矛盾纠纷信息
    @Output() get mdjfValue() {
        return this.mdjfForm.value;
    }

    // 获取矛盾纠纷有效校验信息
    @Output() get mdjfValid(): boolean {
        return this.mdjfForm.valid;
    }
    //申请人删除
    sqrDel(key: number) {
        // 调用删除服务
        this.lbservice.lbservice('TYJB_SQR_DEL', { AYW037: key }).then(resdata => {
            if (resdata.code < 1) {
                this.msgSrv.error(resdata.errmsg);
            } else {
                this.msgSrv.info('删除成功');
                // 列表刷新
                this.sqrGrid.reload({ AYW009: this.dataAYW009 });
            }
        })
    }
    //被申请人删除
    bsqDel(key: number) {
        // 调用删除服务
        this.lbservice.lbservice('TYJB_BSQ_DEL', { AYW037: key }).then(resdata => {
            if (resdata.code < 1) {
                this.msgSrv.error(resdata.errmsg);
            } else {
                this.msgSrv.info('删除成功');
                // 列表刷新
                this.bsqGrid.reload({ AYW009: this.dataAYW009 });
            }
        })
    }
    // 打开弹出框
    openWindow(flag: string) {
        if (flag === 'dw') {
            this.isDwVisible = true;
        }
        else if (flag === 'dw1') {
            this.isDwVisible1 = true;
        }
        else if (flag === 'gr') {
            this.isGrVisible = true;
        }
        else {
            this.isGrVisible1 = true;
        }
    }
    // 关闭弹出框
    closeWindow(flag: string) {
        if (flag === 'dw') {
            this.isDwVisible = false;
        }
        else if (flag === 'dw1') {
            this.isDwVisible1 = false;
        }
        else if (flag === 'gr') {
            this.isGrVisible = false;
        }
        else {
            this.isGrVisible1 = false;
        }
    }

    // 添加申请人(单位)
    AddSqrRow(item: any) {
        // 单位选中
        if (item.AAC001 === undefined) {
            this.dataZTBH = item.AAB001;
            // 申请单位
            this.lbservice.lbservice('TYJB_SQR_ADD', { TYPE: '1', ZTBH: item.AAB001, AYW009: this.dataAYW009 }).then(resdata => {
                if (resdata.code < 1) {
                    this.msgSrv.error(resdata.errmsg);
                } else {
                    this.closeWindow('dw')
                    this.sqrGrid.reload({ AYW009: this.dataAYW009 })
                    this.msgSrv.success('添加成功');

                }
            })
        }
        // 个人选中
        else {
            this.dataZTBH = item.AAC001;
            // 申请人
            this.lbservice.lbservice('TYJB_SQR_ADD', { TYPE: '2', ZTBH: item.AAC001, AYW009: this.dataAYW009 }).then(resdata => {
                if (resdata.code < 1) {
                    this.msgSrv.error(resdata.errmsg);
                } else {
                    this.closeWindow('gr')
                    this.sqrGrid.reload({ AYW009: this.dataAYW009 })
                    this.msgSrv.success('添加成功');

                }
            })
        }
    }

    // 添加被申请人(单位)
    AddBsqRow(item: any) {
        // 单位选中
        if (item.AAC001 === undefined) {
            this.dataZTBH = item.AAB001;
            // 被申请单位
            this.lbservice.lbservice('TYJB_BSQ_ADD', { TYPE: '1', ZTBH: item.AAB001, AYW009: this.dataAYW009 }).then(resdata => {
                if (resdata.code < 1) {
                    this.msgSrv.error(resdata.errmsg);
                } else {
                    this.closeWindow('dw1')
                    this.bsqGrid.reload({ AYW009: this.dataAYW009 })
                    this.msgSrv.success('添加成功');

                }
            })
        }
        // 个人选中
        else {
            this.dataZTBH = item.AAC001;
            // 被申请人
            this.lbservice.lbservice('TYJB_BSQ_ADD', { TYPE: '2', ZTBH: item.AAC001, AYW009: this.dataAYW009 }).then(resdata => {
                if (resdata.code < 1) {
                    this.msgSrv.error(resdata.errmsg);
                } else {
                    this.closeWindow('gr1')
                    this.bsqGrid.reload({ AYW009: this.dataAYW009 })
                    this.msgSrv.success('添加成功');

                }
            })
        }
    }

    //事件简况根据申请人信息生成
    AddAYW020() {
        this.lbservice.lbservice('TYJB_SQR_TQ', { AYW009: this.dataAYW009 }).then(resdata => {
            if (resdata.code < 1) {
                this.msgSrv.error(resdata.errmsg);
            } else {
                //this.setInfoData(resdata.message.INFO);
                this.mdjfForm.setValue('/AYW020', resdata.message.INFO);
            }
        })
    }

    setInfoData(record: any) {
        //.mdjfForm.setValue('/AYW014', record.AYW014)
        //this.mdjfForm.setValue('/FSTIME', record.FSTIME)
        //this.mdjfForm.setValue('/AYW015', record.AYW015)
        //this.mdjfForm.setValue('/AYW016', record.AYW016)
        //this.mdjfForm.setValue('/AYW017', record.AYW017)
        //this.mdjfForm.setValue('/AYW018', record.AYW018)
        //this.mdjfForm.setValue('/AYW019', record.AYW018)
        this.mdjfForm.setValue('/AYW020', record.AYW020)
    }

    constructor(
        private lbservice: HttpService,
        private supDic: SupDic,
        public msgSrv: NzMessageService,
    ) {

    }

    ngOnInit() {

    }


}