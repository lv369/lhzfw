import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { HttpService } from 'lbf';
import { _HttpClient } from '@delon/theme';
import { NzMessageService } from 'ng-zorro-antd';

@Component({
    selector: 'lb-sx-select',
    templateUrl: './lb_sx_select.html',
})

export class LbSxSelectComponent implements OnInit {
    rightStyle: any = { height: (window.outerHeight - 262) + 'px', overflow: 'auto' } // 右侧固定高度，添加滚动条
    showBmsxList: any = [] // 部门事项列表数据

    // 事项单击事件
    @Output() clickItem = new EventEmitter<string>();

    constructor(
        public msgSrv: NzMessageService,
        private lbservice: HttpService,
    ) { }

    ngOnInit() {

    }

    // 初始化后显示清单
    ngAfterViewInit() {
        this.querySx();
    }

    // 事项清单查询
    querySx() {
        this.lbservice.lbservice("TYJB_SL_SXLBLIST", { para: {} }).then(resdata => {
            // 右侧部门事项清单数据赋值
            this.showBmsxList = resdata.message.SXLIST;

        });
    }

    // 事项标题单击事件
    tysl(item: any, flag: string) {
        // 类别默认为初始的，如果是不限的，说明通过POP选择赋值，则取选择后的信息
        const curType: string = item.AYW004 === '3' ? flag : item.AYW004

        const newItem: any = {
            SXID: item.SXID,
            SXMC: item.SXMC,
            AYW004: curType
        }
        this.clickItem.emit(newItem);
    }
}