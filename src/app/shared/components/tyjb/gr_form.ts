import { Component, OnInit, ViewChild, Input, Output, EventEmitter } from '@angular/core';
import { HttpService, SupDic, GridComponent } from 'lbf';
import { SFComponent, SFSchema } from '@delon/form';
import { STColumn, STColumnTag } from '@delon/abc';
import { NzMessageService } from 'ng-zorro-antd';
import { DwComponent } from './lb_dw';
import { GrComponent } from './lb_gr';


@Component({
  selector: 'gr-form',
  templateUrl: './gr_form.html',
  styleUrls: ['./lb_sxlist.less'],
})
export class GrFormComponent implements OnInit {
  
  // 申请人表单
  sqrSchema: SFSchema = {
    properties: {
      AAC001: { 
        type: 'string', 
        title: '个人编号',
        ui: {
          hidden:true 
        }
      },
      AAC002: { type: 'string', title: '身份证号',readOnly:true },
      AAC003: { type: 'string', title: '姓名',readOnly:true },
      AAC005: {
        type: 'string',
        title: '民族',
        enum: this.supDic.getSFDic('AAC005'),
        ui: {
          widget: 'select',
        },
      },
      AAE005: { type: 'string', title: '手机号码' },
      AAE025: {
        type: 'string',
        title: '居住区域',
        enum: this.supDic.getDic('CASCADER_SD'),
        ui: {
          widget: 'cascader',
          
        }
      },
      AAE006: { 
        type: 'string', 
        title: '详细地址',
        ui:{
          grid:{
            span:12
          }
        } 
      },           
      
      AAC010: { type: 'string', title: '文化程度' },
      AAC011: { type: 'string', title: '政治面貌' },
      AAC012: { type: 'string', title: '宗教信仰' },
      AAC013: { type: 'string', title: '从事职业' },
      AAB004: { 
        type: 'string', 
        title: '工作单位',            

      },
      
    },
    ui: {
      spanLabelFixed: 100,
      grid: {
        span: 6,
      },
    },
    required: ['AAC002', 'AAC003', 'AAE025', 'AAE005']
  }
  

  isDwVisible = false; // 是否弹出单位选择框
  isGrVisible = false; // 是否弹出个人选择框

  dataZTBH:number // 主体编号
  dataAYW009:number // 业务流水号

  queryPara = {}
  paramStyle = {height: (window.outerHeight-262)+'px',overflow: 'auto'}

  TAG: STColumnTag = {
    1: { text: '单位', color: 'green' },
    2: { text: '个人', color: 'red' }
  };

  // 被申请人列表
  bsqColumns: STColumn[] = [  
    
    { title: '主体类型', index: 'ZTLX', type: 'tag', tag: this.TAG },
    { title: '主体编号', index: 'ZTBH' },    
    { title: '主体名称', index: 'ZTMC' },    
    { title: '乡镇街道', index: 'CAA001',dic:'CAA001' },
    { title: '村社区', index: 'AAE025',dic:'AAE025' },  
    { title: '详细地址', index: 'ADDR' },  
    { title: '联系电话', index: 'PHONE' },
    {
      title: '操作',
      fixed: 'right',
      buttons: [
        {
          text: '删除',
          icon: 'delete',
          pop: '确定要删除该被申请(人/单位)的信息吗？',
          click: (record: any) => this.doDel(record.AYW037),
        },
      ],
    },
  ];

  // 矛盾纠纷表单
  mdjfSchema:SFSchema = {
    properties: {
      AYW014: { 
        type: 'string', 
        title: '发生日期',
        ui: { widget: 'date' } 
      },
      FSTIME: {
        type: 'string', 
        title: '发生时间',
        ui: { widget: 'time' } 
      },
      AYW015: { type: 'string', title: '发生地点' },      
      AYW016: { type: 'number', title: '涉及人数' },
      AYW017: {
        type: 'string',
        title: '对象主体',
        enum: this.supDic.getSFDic('AYW017'),
        ui: {
          widget: 'radio',
          grid: {
            span: 12
          }
        }
      },
      AYW018: {
        type: 'string',
        title: '重大事件',
        enum: [{ label: '是', value: '1' }, { label: '否', value: '0' }],
        ui: {
          widget: 'radio',
          
        },
        default: '0'

      },
      AYW019: {
        type: 'string',
        title: '紧急事件',
        enum: [{ label: '是', value: '1' }, { label: '否', value: '0' }],
        ui: {
          widget: 'radio',
          
        }
        ,
        default: '0'
      },
      AYW020: { 
        type: 'string', 
        title: '事件简况',
        ui: {
          widget: 'textarea',
          autosize: { minRows: 3, maxRows: 10 },
          grid: {
            span: 24
          }
        } 
      },           
    },
    ui: {
      spanLabelFixed: 100,
      grid: {
        span: 6,
      },
    },
    required: [ 'AYW015', 'AYW016', 'AYW017', 'AYW018', 'AYW019', 'AYW020']
  }

  // 申请人表单
  @ViewChild('sqrForm', { static: false }) sqrForm: SFComponent;
  // 被申请单位选择界面
  @ViewChild('dw', { static: false }) dw: DwComponent;
  // 被个人单位选择界面
  @ViewChild('gr', { static: false }) gr: GrComponent;
  // 被申请列表
  @ViewChild('bsqGrid', { static: false }) bsqGrid: GridComponent;
  // 矛盾纠纷表单
  @ViewChild('mdjfForm', { static: false }) mdjfForm: SFComponent;

  @Output() set AYW009(value:number){
    this.dataAYW009 = value;
    if(value!==undefined){
      this.queryPara = { AYW009:value }
      // 初始化其他信息
      this.lbservice.lbservice('TYJB_DATA_INIT', {FLAG:'FORM',AYW009:this.dataAYW009 }).then(resdata => {
        console.log('-------form_gr----------')
        if (resdata.code < 1) {
          this.msgSrv.error(resdata.errmsg);
        } else {
          this.setGrData(resdata.message.ZT);
          this.setInfoData(resdata.message.INFO )
        }
      }) 
    }
    
  }
  get AYW009(){
    return this.dataAYW009;
  }

  setInfoData(record:any){
    this.mdjfForm.setValue('/AYW014',record.AYW014)
    this.mdjfForm.setValue('/FSTIME',record.FSTIME)
    this.mdjfForm.setValue('/AYW015',record.AYW015)
    this.mdjfForm.setValue('/AYW016',record.AYW016)
    this.mdjfForm.setValue('/AYW017',record.AYW017)
    this.mdjfForm.setValue('/AYW018',record.AYW018)
    this.mdjfForm.setValue('/AYW019',record.AYW018)
    this.mdjfForm.setValue('/AYW020',record.AYW018)
  }

  // 申请人信息赋值
  setGrData(record:any){
    this.sqrForm.setValue('/AAC001',record.AAC001) // 个人编号
    this.sqrForm.setValue('/AAC002',record.AAC002) // 身份证号
    this.sqrForm.setValue('/AAC003',record.AAC003) // 姓名
    this.sqrForm.setValue('/AAC005',record.AAC005) // 民族
    this.sqrForm.setValue('/AAE025',[record.CAA001,record.AAE025]) // 社区
    this.sqrForm.setValue('/AAE006',record.AAE006) // 手机号码
    this.sqrForm.setValue('/AAE005',record.AAE005) // 联系地址
    this.sqrForm.setValue('/AAC010',record.AAC010) // 文化程度
    this.sqrForm.setValue('/AAC011',record.AAC011) // 政治面貌
    this.sqrForm.setValue('/AAC012',record.AAC012) // 宗教信仰
    this.sqrForm.setValue('/AAC013',record.AAC013) // 从事职业
    this.sqrForm.setValue('/AAB004',record.AAB004) // 单位

    // 列表刷新
    this.bsqGrid.reload({AYW009:this.dataAYW009});
  }

  // 获取申请人信息
  @Output() get sqrValue(){
    return this.sqrForm.value;
  }

  // 获取申请人有效校验信息
  @Output() get sqrValid():boolean{
    return this.sqrForm.valid;
  }

  // 获取矛盾纠纷信息
  @Output() get mdjfValue(){
    return this.mdjfForm.value;
  }

  // 获取矛盾纠纷有效校验信息
  @Output() get mdjfValid():boolean{
    return this.mdjfForm.valid;
  }



  doDel(key:number){
    
    // 调用删除服务
    this.lbservice.lbservice('TYJB_BSQ_DEL', {AYW037:key}).then(resdata => {
      if (resdata.code < 1) {
        this.msgSrv.error(resdata.errmsg);
      } else {        
        this.msgSrv.info('删除成功');
        // 列表刷新
        this.bsqGrid.reload({AYW009:this.dataAYW009});
      }
    })
  }

  // 打开弹出框
  openWindow(flag:string){
    if(flag==='dw'){
      this.isDwVisible = true;
    }
    else{
      this.isGrVisible = true;
    }
  }

  // 关闭弹出框
  closeWindow(flag:string){
    if(flag==='dw'){
      this.isDwVisible = false;
    }
    else{
      this.isGrVisible = false;
    }
  }

  // 个人、单位信息 选择单击事件
  selectRow(item:any){
    // 单位选中
   if(item.AAC001===undefined){
     this.dataZTBH = item.AAB001;
     // 被申请人
     this.lbservice.lbservice('TYJB_BSQ_ADD', {TYPE:'1',ZTBH:item.AAB001,AYW009:this.dataAYW009 }).then(resdata => {
      if (resdata.code < 1) {
        this.msgSrv.error(resdata.errmsg);
      } else {
        this.closeWindow('dw')
        this.bsqGrid.reload({AYW009:this.dataAYW009})
        this.msgSrv.success('添加成功');
        
      }
     })
   }
   // 个人选中
   else{
     this.dataZTBH = item.AAC001;
     // 被申请人
    this.lbservice.lbservice('TYJB_BSQ_ADD', {TYPE:'2',ZTBH:item.AAC001,AYW009:this.dataAYW009 }).then(resdata => {
      if (resdata.code < 1) {
        this.msgSrv.error(resdata.errmsg);
      } else {
        this.closeWindow('gr')
        this.bsqGrid.reload({AYW009:this.dataAYW009})
        this.msgSrv.success('添加成功');
        
      }
    })
   }

   
   

  }
  
  constructor(
    private lbservice: HttpService,
    private supDic: SupDic,
    public msgSrv: NzMessageService,
  ) {
    
  }

  ngOnInit() {
    
  }


}