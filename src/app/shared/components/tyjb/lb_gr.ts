import { Component, OnInit, ViewChild, Input, Output, EventEmitter } from '@angular/core';
import { HttpService, SupDic, GridComponent } from 'lbf';
import { SFComponent, SFSchema } from '@delon/form';
import { STColumn } from '@delon/abc';
import { NzMessageService } from 'ng-zorro-antd';


@Component({
  selector: 'lb-gr',
  templateUrl: './lb_gr.html',
  styleUrls: ['./lb_gr.less'],
})
export class GrComponent implements OnInit {

  addSchema: SFSchema = {
    properties: {
      AAC001: {
        type: 'string',
        title: '个人编号',
        ui: {
          hidden: true
        }
      },
      AAC002: { type: 'string', title: '身份证号', format: 'id-card', },
      AAC003: { type: 'string', title: '姓名' },
      AAE025: {
        type: 'string',
        title: '本地居住区域',
        enum: this.supDic.getDic('CASCADER_SD'),
        ui: {
          placeholder: '本地人员，请填写此栏',
          widget: 'cascader',
          grid: {
            span: 24
          }
        }
      },
      AAE026: {
        type: 'string',
        title: '外地居住区域',
        ui: {
          placeholder: '外地人员，请填写此栏',
          grid: {
            span: 24
          }
        }
      },
      AAE006: {
        type: 'string',
        title: '详细地址',
        ui: {
          grid: {
            span: 24
          }
        }
      },
      AAE005: { type: 'string', title: '手机号码', format: 'mobile', },
      AAC005: {
        type: 'string',
        title: '民族',
        enum: this.supDic.getSFDic('AAC005'),
        ui: {
          widget: 'select',
        },
      },
      AAC010: { type: 'string', title: '文化程度' },
      AAC011: { type: 'string', title: '政治面貌' },
      AAC012: { type: 'string', title: '宗教信仰' },
      AAC013: { type: 'string', title: '从事职业' },
      AAB004: {
        type: 'string',
        title: '工作单位',
        ui: {
          grid: {
            span: 24
          }
        }
      },
    },
    ui: {
      spanLabelFixed: 100,
      grid: {
        span: 12,
      },
    },
    required: ['AAC003']
  }


  // 查询界面Schema
  querySchema: SFSchema = {
    properties: {
      AAC002: { type: 'string', title: '身份证号' },
      AAC003: { type: 'string', title: '姓名' }
    }
  }

  // 个人列表
  grColumns: STColumn[] = [

    {
      title: '操作区',
      fixed: 'left',
      width: 100,
      buttons: [
        {
          text: '修改',
          icon: 'edit',
          click: (record: any) => {
            // 修改框赋值
            this.modiForm.setValue('/AAC001', record.AAC001) // 个人编号
            this.modiForm.setValue('/AAC002', record.AAC002) // 身份证号
            this.modiForm.setValue('/AAC003', record.AAC003) // 姓名
            this.modiForm.setValue('/AAC005', record.AAC005) // 民族
            this.modiForm.setValue('/AAE025', [record.CAA001, record.AAE025]) // 社区
            this.modiForm.setValue('/AAE026', record.AAE026) // 单位
            this.modiForm.setValue('/AAE006', record.AAE006) // 手机号码
            this.modiForm.setValue('/AAE005', record.AAE005) // 联系地址
            this.modiForm.setValue('/AAC010', record.AAC010) // 文化程度
            this.modiForm.setValue('/AAC011', record.AAC011) // 政治面貌
            this.modiForm.setValue('/AAC012', record.AAC012) // 宗教信仰
            this.modiForm.setValue('/AAC013', record.AAC013) // 从事职业
            this.modiForm.setValue('/AAB004', record.AAB004) // 单位

            // 打开修改弹出框
            this.openWindow('modi');
          },
        },
        {
          text: '删除',
          icon: 'delete',
          pop: '确定要删除该人员的信息吗？',
          click: (record: any) => this.doDel(record.AAC001),
        }
      ],
    },
    { title: '身份证号', index: 'AAC002', width: 100 },
    { title: '姓名', index: 'AAC003', width: 100 },
    { title: '乡镇街道', index: 'CAA001', dic: 'CAA001', width: 100 },
    { title: '村社区', index: 'AAE025', dic: 'AAE025', width: 100 },
    { title: '外地居住区域', index: 'AAE026', width: 100 },
    { title: '详细地址', index: 'AAE006', width: 100 },
    { title: '手机号码', index: 'AAE005', width: 100 },
    { title: '性别', index: 'AAC004', dic: 'AAC004', width: 70 },
    { title: '年龄', index: 'AGE', type: 'number', width: 50 },
    { title: '民族', index: 'AAC005', dic: 'AAC005', width: 70 },
    { title: '文化程度', index: 'AAC010', width: 100 },
    { title: '政治面貌', index: 'AAC011', width: 100 },
    { title: '宗教信仰', index: 'AAC012', width: 100 },
    { title: '从事职业', index: 'AAC013', width: 100 },
    { title: '工作单位', index: 'AAB004', width: 150 },
    {
      title: '选择',
      fixed: 'right',
      width: 50,
      buttons: [
        {
          text: '选取',
          icon: 'audit',
          click: (record: any) => this.tysjsl(record),
        },
      ],
    },
  ];




  @Output() clickItem = new EventEmitter<string>();

  isAddVisible = false; // 是否显示新增弹出框
  isModiVisible = false; // 是否显示修改弹出框


  // 新增表单页
  @ViewChild('addForm', { static: false }) addForm: SFComponent;
  // 修改表单页
  @ViewChild('modiForm', { static: false }) modiForm: SFComponent;
  // 人员列表
  @ViewChild('grGrid', { static: false }) grGrid: GridComponent;
  // 查询表单
  @ViewChild('queryForm', { static: false }) queryForm: SFComponent;



  // 选中人员事件
  tysjsl(record: any) {
    this.clickItem.emit(record);
  }

  // 人员列表查询
  doQuery(pi1 = false) {
    if (pi1) {
      this.grGrid.pi = 1;
    }
    this.grGrid.reload(this.queryForm.value);
  }

  // 新增保存
  save(flag: string) {
    // 必输项校验
    if (flag === 'add') {
      if (!this.addForm.valid) {
        this.msgSrv.error('必输项不能为空！');
        return;
      }
    }
    else {
      if (!this.modiForm.valid) {
        this.msgSrv.error('必输项不能为空！');
        return;
      }
    }



    // 调用新增服务
    this.lbservice.lbservice('TYJB_GR_ADD', { type: flag, para: flag === 'add' ? this.addForm.value : this.modiForm.value }).then(resdata => {
      if (resdata.code < 1) {
        this.msgSrv.error(resdata.errmsg);
      } else {
        // 关闭窗口
        this.closeWindow(flag)

        if (flag === 'add') {
          //重置表单
          this.addForm.reset()
          this.msgSrv.info('新增成功');
        }
        else {
          //重置表单
          this.modiForm.reset()
          this.msgSrv.info('修改成功');
        }

        // 列表刷新
        this.grGrid.reload(this.queryForm.value);


      }
    });
  }

  // 人员删除操作
  doDel(dataAAC001: number) {
    // 调用删除服务
    this.lbservice.lbservice('TYJB_GR_DEL', { AAC001: dataAAC001 }).then(resdata => {
      if (resdata.code < 1) {
        this.msgSrv.error(resdata.errmsg);
      } else {
        this.msgSrv.info('删除成功');
        // 列表刷新
        this.grGrid.reload(this.queryForm.value);
      }
    })
  }


  // 打开弹出框
  openWindow(openType: string) {
    if (openType === 'add') {
      this.isAddVisible = true;
    }
    else {
      this.isModiVisible = true;
    }

  }

  // 关闭弹出框
  closeWindow(closeType: string) {
    if (closeType === 'add') {
      this.isAddVisible = false;
    }
    else {
      this.isModiVisible = false;
    }
  }



  constructor(
    private lbservice: HttpService,
    private supDic: SupDic,
    public msgSrv: NzMessageService,
  ) {

  }

  ngOnInit() {

  }


}