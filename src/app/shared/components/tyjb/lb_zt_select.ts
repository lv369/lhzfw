import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { _HttpClient } from '@delon/theme';
import { HttpService } from 'lbf';
import { NzMessageService } from 'ng-zorro-antd';

@Component({
  selector: 'lb-zt-select',
  templateUrl: './lb_zt_select.html',
  styleUrls: ['./lb_zt_select.less'],
})
export class LbZtSelectComponent implements OnInit {
  rightStyle:any = {height: (window.outerHeight-262)+'px',overflow: 'auto'} // 右侧固定高度，添加滚动条
  showBmsxList:any = [] // 部门事项列表数据
  showBmList:any = [] // 部门列表数据
  
  queryBmList = [] // 左侧部门选择区数据
  queryAYW002:string = '' // 左侧事项名称查询项数据

  // 事项单击事件
  @Output() clickItem = new EventEmitter<string>();
  
  constructor(
    public msgSrv: NzMessageService,
    private lbservice: HttpService,
  ) { }

  ngOnInit() {

  }

  ayw002Change(e:any){
    if(e.key==='Enter'){
      this.querySx();
    }
  }

  // 初始化后显示清单
  ngAfterViewInit() {
    this.querySx();
  }

  // 事项清单查询
  querySx(){

    this.lbservice.lbservice("TYJB_SL_SXLIST",{para:{DGB041:this.queryAYW002}}).then(resdata=>{
      // 右侧部门事项清单数据赋值
      this.showBmsxList=resdata.message.SXLIST;

      // 左侧部门列表
      if(resdata.message.BMLIST!==undefined&&resdata.message.BMLIST.length>0){
        this.showBmList = resdata.message.BMLIST;
      }

    });
  }

  // 部门标签选择回调方法
  bmSelect(curZt:boolean,curDGB010:string){
    // 如果当前是选中，则部门列表中添加部门
    if(curZt){
      this.queryBmList.push(curDGB010)
    }
    // 如果当前是取消选中，则部门列表中排除部门
    else{
      this.queryBmList = this.queryBmList.filter(item => item !== curDGB010);
    }
    
    // 执行事项查询
    this.querySx();
  }
   
  // 事项标题单击事件
  tysl(flag:string) {
    // 类别默认为初始的，如果是不限的，说明通过POP选择赋值，则取选择后的信息
    const newItem:any = {
      BMID:'0',
      BMLX:'a',
      BMMC:'0',
      AYW004:flag
    }
    this.clickItem.emit(newItem);
  }
  
}