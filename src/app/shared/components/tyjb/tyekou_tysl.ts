import { Component, OnInit, ViewChild, Output } from '@angular/core';
import { _HttpClient, ModalHelper } from '@delon/theme';
import { ActivatedRoute } from '@angular/router';
import { NzMessageService, NzModalService, NzFormatEmitEvent, NzTreeNode } from 'ng-zorro-antd';
import { HttpService, SupDic } from 'lbf';
import { UserSelectComponent } from '@shared/components/tyjb/lb_user_select';
import { SFSchema, SFComponent } from '@delon/form';
import { SlListComponent } from './lb_sllist';
import { TyekouQueryComponent } from './tyekou_query';

@Component({
  selector: 'tyekou-tysl',
  templateUrl: './tyekou_tysl.html',
  styleUrls: ['./tyekou_tysl.less'],
})
export class TyekouTyslComponent implements OnInit {
  topTitle = '我的办件清单';
  curStep = 0; // 当前步骤 0 首页 1 人员选择 2 单位选择 3 人员表单 4 单位表单 5 受理成功 6 不予受理 7 收件箱
  lastStep = 0; // 已完成的步骤

  // 控制页面显示 隐藏
  hidden_List = false;
  hidden_form = true; // 表单页
  hidden_userSelect = true; // 用户选择


  // 按钮组
  isBtnNext = false; // 下一步按钮不显示
  isBtnBack = false; // 上一步按钮不显示
  isBtnInfo = false; // 事项详情不显示
  isBtnList = false; //列表

  styleParam = {}
  // infoData:any // 申请人信息
  // mxData:any // 纠纷详细信息
  // slData:any // 受理信息


  dataAYW001 = 0; // 事项编码
  dataAYW002 = ''; // 事项名称
  dataAYW004 = ''; // 事项主体类型
  dataAYW009: number; // 业务流水号
  dataAYW026: string // 受理状态

  curParamId: number = -1;


  // 待办
  @ViewChild('sllist', { static: false }) sllist: SlListComponent;
  // 用户选择
  @ViewChild('userSelect', { static: false }) userSelect: UserSelectComponent;
  // 业务表单
  @ViewChild('slForm', { static: false }) slForm: SFComponent;
  // 详情弹出框
  @ViewChild('queryInfo', { static: false }) queryInfo: TyekouQueryComponent;

  // 受理表单
  slSchema: SFSchema = {
    properties: {
      AYW001: {
        type: 'string',
        title: '选择事项',
        enum: this.supdic.getSFDic('BMSX'),
        ui: {
          widget: 'select',
          grid: {
            span: 12
          }
        }
      },
      AYW026: {
        type: 'string',
        title: '受理意见',
        enum: [{ label: '受理通过', value: '1' }, { label: '不予受理', value: '0' }],
        ui: {
          hidden: true,
          widget: 'radio',
          grid: {
            span: 12
          }
        },
        default: '1',
      },
      AYW029: {
        type: 'string',
        title: '处理说明',
        ui: {
          widget: 'textarea',
          autosize: { minRows: 2, maxRows: 6 },

          grid: {
            span: 24
          }
        }
      }

    },
    ui: {
      spanLabelFixed: 100,
      grid: {
        span: 12,
      },
    },
    required: ['AYW026', 'AYW001']
  }


  constructor(
    private lbservice: HttpService,
    public msgSrv: NzMessageService,
    private supdic: SupDic
  ) { }

  ngOnInit() {
    this.styleParam = { height: (window.innerHeight - 180) + 'px' }

    // this.todoInit();
  }


  todoInit取消() {
    // 去获取是否有传递参数
    this.lbservice.lbservice('PUBLIC_INIT', {}).then(resdata => {
      if (resdata.code < 1) {
        this.msgSrv.error(resdata.errmsg);
      }
      else {
        console.log(resdata)
        // 判断是否有初始化数据
        if (resdata.message.AYW009 !== undefined) {
          this.curParamId = resdata.message.AYW039;
          this.showInitList({ AYW009: resdata.message.AYW009 })

        }
        // 如果没有参数传递，默认打开首页
        else {
          if (this.curParamId < 0) {
            this.showSqList();
          }

        }
      }

    })
  }

  // 隐藏所有功能，所有按钮失效
  hiddenAll() {
    this.hidden_List = true;
    this.hidden_userSelect = true;
    this.hidden_form = true;


    this.isBtnNext = false; // 下一步按钮不显示
    this.isBtnBack = false; // 上一步按钮不显示

    this.isBtnInfo = false; // 返回当前经办环节
    this.isBtnList = true;
  }

  aryAYW001: Array<string>
  /********************************************* */
  /********************按钮组事件**************** */
  /********************************************* */
  // 导航栏按钮单击事件
  btnClick(flag: string) {

    /********************************/
    /*************我的办件***************/
    /********************************/
    if (flag === 'list') {
      // this.dataAYW009 = undefined;
      this.lastStep = 0
      this.showSqList()
    }
    else if (flag === 'info') {
      this.showInfo()
    }

    /********************************/
    /************下一步**************/
    /********************************/
    else if (flag === 'next') {
      // 当前是表单页，下一步是人员指定
      if (this.curStep === 1) {
        // 获取表单处理意见字段
        //this.dataAYW026 = this.slForm.getValue('/AYW026')
        if (!this.slForm.valid) {
          this.msgSrv.error('必输项不能为空!');

          return;
        }
        // 调用表单保存过程
        this.lbservice.lbservice('TYJB_SL', { AYW009: this.dataAYW009, LX: this.dataCllx, para: this.slForm.value }).then(resdata => {
          if (resdata.code < 1) {
            this.msgSrv.error(resdata.errmsg);
          } else {
            // 如果是指派部门
            if (this.dataCllx === '1') {
              this.dataAYW009 = undefined;
              this.lastStep = 0;
              this.msgSrv.success('操作成功');
              this.showSqList();
              return;
            }
            else {
              this.dataAYW026 = this.slForm.getValue('/AYW026')
              if (this.dataAYW026 === '0') {
                this.dataAYW009 = undefined;
                this.lastStep = 0;
                this.msgSrv.success('操作成功');
                this.showSqList();
                return;
              }
            }
            // 如果是不予受理，则返回清单页

            // 定位最终步骤，是否是中途返回的
            if (this.lastStep < 2) {
              this.lastStep = 2;
            }
            this.showUserSelect();

          }
        })

      }

      else {

      }
    }
    // 上一步
    else if (flag === 'back') {
      // 当前是人员页，上一步是人员指定
      if (this.curStep === 2) {
        this.showForm();
      }

    }
    else {

    }

  }



  dataCllx: string; // 处理类型 1 中心指派部门 2 部门指派事项
  dataBM: any; //中心部门

  // 待办事项
  DBSX_Click(item: any) {

    this.dataAYW009 = item.AYW009;
    this.dataAYW001 = item.AYW001;
    this.dataAYW002 = this.supdic.getdicLabel('AYW001', item.AYW001);
    this.dataAYW004 = item.ZTLX;
    //  判断要跳转的页面
    // 待受理状态
    if (item.AYW034 === '4') {
      this.dataCllx = item.BMLX;
      if (item.BMLX === '2') {
        if (item.AYW001 === 0) {
          this.SetBm(item.BMID)
        }
        else {
          // 如果事项已指定，则直接指派人员

          this.lastStep = 2;
          this.showUserSelect();
        }

      }
      else {

        this.dataBM = JSON.parse(item.BMLIST);

        this.slSchema = {
          properties: {
            AYW001: {
              type: 'string',
              title: '指派部门或人员',
              /*
              enum: this.supdic.getSFDic('MYBM'),
              ui: {
                widget: 'select',

              }
              */
              enum: this.dataBM.message.list,
              ui: {
                widget: 'tree-select',
                dropdownStyle: {
                  height: '200px'
                },
                grid: {
                  span: 12
                },
                allowClear: true,
                expandChange: (e: NzFormatEmitEvent) => this.getSdTree(e),
                change: i => this.InitBm(i),
              }
            },
            AYW120: {
              type: 'string',
              title: '事项分类',
              // enum: this.supdic.getSFDic('BMSX'),
              ui: {
                widget: 'select',
                allowClear: true
              }
            },
            AYW029: {
              type: 'string',
              title: '处理说明',
              ui: {
                widget: 'textarea',
                autosize: { minRows: 2, maxRows: 6 },

                grid: {
                  span: 24
                }
              }
            }

          },
          ui: {
            spanLabelFixed: 100,
            grid: {
              span: 6,
            },
          },
          required: ['AYW001']
        }
      }

      if (item.AYW001 === 0) {
        this.lastStep = 1
        this.showForm()
      }

    }
    // 如果是受理通过，则指派人员
    else if (item.AYW034 === '6') {
      this.lastStep = 2;
      this.showUserSelect();
    }
    else {
      this.showInfo();
    }





  }

  private InitBm(province: string): void {

    this.lbservice.lbservice('INIT_BM_SX', { key: province }).then(resdata => {
      if (resdata.code < 1) {
        this.msgSrv.error(resdata.errmsg);
      }
      else {

        const bmProperty = this.slForm.getProperty('/AYW120');
        const items = resdata.message.list;
        bmProperty.schema.enum = items;
        bmProperty.widget.reset(items[0]);

      }
    })



  }

  private SetBm(province: string): void {

    this.lbservice.lbservice('INIT_BM_SX', { key: province }).then(resdata => {
      if (resdata.code < 1) {
        this.msgSrv.error(resdata.errmsg);
      }
      else {

        this.slSchema = {
          properties: {
            AYW001: {
              type: 'string',
              title: '选择事项',
              enum: resdata.message.list,
              ui: {
                widget: 'select',
                grid: {
                  span: 12
                }
              }
            },
            AYW026: {
              type: 'string',
              title: '受理意见',
              enum: [{ label: '受理通过', value: '1' }, { label: '不予受理', value: '0' }],
              ui: {
                hidden: true,
                widget: 'radio',
                grid: {
                  span: 12
                }
              },
              default: '1',
            },
            AYW029: {
              type: 'string',
              title: '处理说明',
              ui: {
                widget: 'textarea',
                autosize: { minRows: 2, maxRows: 6 },

                grid: {
                  span: 24
                }
              }
            }

          },
          ui: {
            spanLabelFixed: 100,
            grid: {
              span: 12,
            },
          },
          required: ['AYW026', 'AYW001']
        }

      }
    })



  }

  node: NzTreeNode;
  getSdTree(e: NzFormatEmitEvent) {
    this.node = e.node;
    this.lbservice.lbservice('tree_select_bmry', { key: e.node.origin.key }).then(resdata => {
      if (resdata.code < 1) {
        this.msgSrv.error(resdata.errmsg);
      }
      else {
        if (this.node.getChildren().length === 0 && this.node.isExpanded) {
          this.node.addChildren(resdata.message.list);
        }


      }
    })
  }

  // 操作员指定
  UserSet(item: any) {
    // 调用新增服务
    this.lbservice.lbservice('TYJB_BLUSER_SET', item).then(resdata => {
      if (resdata.code < 1) {
        this.msgSrv.error(resdata.errmsg);
      } else {
        this.msgSrv.success('流转完成')
        this.dataAYW009 = undefined; // 流水号清空
        this.lastStep = 0;
        this.showSqList();


      }
    })

  }

  showInfo() {
    this.queryInfo.AYW009 = this.dataAYW009;
  }


  /********************************************* */
  /******************页面显示切换**************** */
  /********************************************* */

  @Output() showInitList(where: any) {
    this.dataAYW009 = undefined;
    this.topTitle = '我的办件清单';
    this.hiddenAll();
    this.hidden_List = false; // 显示    
    this.isBtnList = false;
    this.sllist.Where = where;
    this.curStep = 0;
  }



  showSqList() {
    this.curParamId = -1;
    this.dataAYW009 = undefined;
    this.topTitle = '我的办件清单';
    this.hiddenAll();
    this.hidden_List = false; // 显示

    this.isBtnList = false;
    this.sllist.InitGrid = 0;
    this.curStep = 0;
  }

  // 受理表单页面
  showForm() {
    this.topTitle = '[事项流转意见]--' + this.dataAYW002;
    this.curStep = 1;
    this.hiddenAll();

    // 如果是从下一步回退回来的，则加载受理表单数据
    if (this.lastStep > this.curStep) {
      // 初始化其他信息
      this.lbservice.lbservice('TYJB_DATA_INIT', { FLAG: 'SL_FORM', AYW009: this.dataAYW009 }).then(resdata => {
        console.log('-------form_sl----------')
        if (resdata.code < 1) {
          this.msgSrv.error(resdata.errmsg);
        } else {
          this.slForm.setValue('/AYW026', resdata.message.AYW026)
          this.slForm.setValue('/AYW029', resdata.message.AYW029)
        }
      })

    }

    this.isBtnNext = true;
    this.isBtnInfo = true;

    // 显示表单页
    this.hidden_form = false;
  }

  // 显示用户选择界面
  showUserSelect() {
    this.topTitle = '[办理人员指派]--' + this.dataAYW002;
    this.curStep = 2;
    this.hiddenAll();

    // 上一步、下一步、搁置、作废
    this.isBtnBack = true;
    this.isBtnInfo = true;

    this.userSelect.SavePro = 'TYJB_BLUSER_QUERY'
    this.userSelect.dataAYW025 = '3'
    this.userSelect.AYW009 = this.dataAYW009;
    this.userSelect.AYW001 = this.dataAYW001;

    this.hidden_userSelect = false;



  }





}
