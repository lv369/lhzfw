import { NzMessageService } from 'ng-zorro-antd';
import { HttpService, GridComponent } from 'lbf';
import { Component, OnInit, Output, EventEmitter, ViewChild } from '@angular/core';
import { STColumn } from '@delon/abc';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';

@Component({
  selector: 'user-select',
  templateUrl: './lb_user_select.html',
  // styleUrls: ['./lb_sxlist.less'],
})
export class UserSelectComponent implements OnInit {
  isVisible = false; // 弹出框 
  
  queryPara:any;
  savePara:any;

  // 单位列表
  @ViewChild('userGrid', { static: false }) userGrid: GridComponent;

  // 个人列表
  userColumns: STColumn[] = [
    { title: '操作用户', index: 'NAME' },
    { title: '在办业务笔数', index: 'UCOUNT',type:'number'}, 
    {
      title: '选择',
      buttons: [
        {
          text: '选取',
          icon: 'audit',
          click: (record: any) => {
            this.isVisible = false;
            this.selectUser.emit(record);
          }
        },
      ],
    },
  ];
  @Output() set SavePro(value:string){
    this._savePro = value;
  }
  _savePro:string = 'TYJB_USER_QUERY';

  @Output() set AYW025(value:string){
    this.dataAYW025 = value;
  }
  dataAYW025:string = '2';
  
  @Output() set AYW009(value:number){
    this.dataAYW009 = value;
    this.savePara = { AYW009:value, FLAG:0}
  }
  dataAYW009:number;


  @Output() set AYW001(value:number){
    this.queryPara = { AYW001:value, AYW025:this.dataAYW025,AYW009:this.dataAYW009}
    console.log('******************queryPara**************')
    console.log(this.queryPara)
    console.log('******************queryPara**************')
    this.userGrid.reload(this.queryPara);
    
  }

  @Output() selectUser = new EventEmitter<string>();

  constructor(
    public msgSrv: NzMessageService,
    private lbservice: HttpService,
  ) { }

  ngOnInit() {

  }

  btnClick(flag:string){
    if(flag==='1'){
      this.selectUser.emit(this.savePara);
    }
    else{
      this.isVisible = true;
    }
  }

  // 关闭弹出框
  closeWindow(){
    this.isVisible = false;    
  }
}