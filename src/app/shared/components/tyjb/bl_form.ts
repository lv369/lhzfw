import { Component, OnInit, ViewChild, Input, Output, EventEmitter } from '@angular/core';
import { HttpService, SupDic, GridComponent } from 'lbf';
import { SFComponent, SFSchema, SFRadioWidgetSchema } from '@delon/form';
import { STColumn, STColumnTag, STColumnBadge } from '@delon/abc';
import { DwComponent } from './lb_dw';
import { NzMessageService, NzModalService, NzFormatEmitEvent, NzTreeNode } from 'ng-zorro-antd';


@Component({
  selector: 'bl-form',
  templateUrl: './bl_form.html',
  styleUrls: ['./lb_sxlist.less'],
})
export class BlFormComponent implements OnInit {

  isAddTjVisible = false; // 是否弹出单位选择框
  isModiTjVisible = false; // 是否弹出个人选择框
  isAddYyVisible = false; // 部门邀约弹出框
  isAddSqysVisible = false; // 申请延时
  isAddSqzjVisible = false; // 申请转交

  dataAYW009: number // 业务流水号

  dataAYW001: number // 事项编码

  queryPara = {}

  paramStyle = { height: (window.outerHeight - 262) + 'px', overflow: 'auto' }

  TAG: STColumnTag = {
    '2': { text: '暂未办理', color: 'blue' },
    '1': { text: '办理成功', color: 'green' },
    '0': { text: '办理失败', color: 'red' }
  };

  // 调解信息列表
  tjColumns: STColumn[] = [
    { title: '场次', index: 'AYW052', type: 'number' },
    { title: '办理结果', index: 'AYW051', type: 'tag', tag: this.TAG },
    { title: '办理地点', index: 'AYW045' },
    { title: '办理时间', index: 'AYW044' },
    { title: '结果说明', index: 'AYW050' },
    /*
    { title: '调解主持人', index: 'AYW047' },
    { title: '参加人员', index: 'AYW048' },*/
    {
      title: '操作区',
      fixed: 'right',
      buttons: [
        {
          text: '查看详情',
          icon: 'edit',
          click: (record: any) => {
            this.modiTjForm.setValue('/AYW043', record.AYW043)
            this.modiTjForm.setValue('/AYW020', record.AYW020)
            this.modiTjForm.setValue('/AYW044', record.AYW044)
            this.modiTjForm.setValue('/AYW045', record.AYW045 === undefined ? '' : record.AYW045)
            /*
            this.modiTjForm.setValue('/AYW046', record.AYW046 === undefined ? '' : record.AYW046)
            this.modiTjForm.setValue('/AYW047', record.AYW047 === undefined ? '' : record.AYW047)
            this.modiTjForm.setValue('/AYW048', record.AYW048 === undefined ? '' : record.AYW048)
            this.modiTjForm.setValue('/AYW049', record.AYW049 === undefined ? '' : record.AYW049)
            */
            this.modiTjForm.setValue('/AYW050', record.AYW050 === undefined ? '' : record.AYW050)
            this.modiTjForm.setValue('/AYW051', record.AYW051 === undefined ? '' : record.AYW051)
            //this.modiTjForm.setValue('/FSTIME', record.FSTIME === undefined ? '' : record.FSTIME)

            this.openWindow('tjModi')
          },
        },
        /*
        {
          text: '协同邀约',
          icon: 'user-add',
          click: (record: any) => {
            if (record.AYW051 !== '2') {
              this.msgSrv.error('该场次已调解结束，不能再发起协同邀约');
              return;
            }

            if (record.AYW045 === undefined || record.AYW045 === '') {
              this.msgSrv.error('调解场所还未确定，不能发起协同邀约');
              return;
            }

            if (record.AYW044 === undefined || record.AYW044 === '') {
              this.msgSrv.error('调解时间还未确定，不能发起协同邀约');
              return;
            }
            this.addYyForm.setValue('/AYW043', record.AYW043)
            this.openWindow('yyAdd');

          },
        },*/
        {
          text: '删除',
          icon: 'delete',
          pop: '确定要删除该场次的办理信息吗？',
          click: (record: any) => this.doDel('tjDel', { AYW043: record.AYW043 }),
        },
      ],
    },
  ];

  // 办理表单
  tjSchema: SFSchema = {
    properties: {
      AYW043: {
        type: 'number',
        ui: {
          hidden: true
        }
      },
      AYW044: {
        type: 'string',
        title: '办理日期',
        ui: { widget: 'date' }
      },

      FSTIME: {
        type: 'string',
        title: '办理时间',
        ui: { widget: 'time' }
      },

      AYW045: {
        type: 'string',
        title: '办理地点',
        ui: {
          grid: {
            span: 12
          }
        }
      },
      AYW020: {
        type: 'string',
        title: '事件简况',
        readOnly: true,
        ui: {
          widget: 'textarea',
          autosize: { minRows: 3, maxRows: 10 },
          grid: {
            span: 24
          }
        }
      },
      SJJK: {
        type: 'string',
        title: '事件简况补充说明',
        ui: {
          widget: 'textarea',
          autosize: { minRows: 2, maxRows: 10 },
          grid: {
            span: 24
          }
        }
      },
      AYW051: {
        type: 'string',
        title: '办理结果',
        enum: [{ label: '暂未办理', value: '2' }, { label: '办理成功', value: '1' }, { label: '办理失败', value: '0' }],
        ui: {
          widget: 'radio',
          hidden: true,
          grid: {
            span: 24
          }
        },
        default: '1',
      },
      AYW050: {
        type: 'string',
        title: '结果说明',
        ui: {
          widget: 'textarea',
          autosize: { minRows: 2, maxRows: 6 },
          grid: {
            span: 24
          }
        }
      },

    },
    ui: {
      spanLabelFixed: 100,
      grid: {
        span: 6,
      },
    },
    required: ['AYW045', 'AYW050', 'AYW051']
  }

  BADGE: STColumnBadge = {
    '2': { text: '未审核', color: 'default' },
    '1': { text: '审核通过', color: 'processing' },
    '0': { text: '不同意', color: 'error' },
    '3': { text: '已接受', color: 'success' },
    '4': { text: '已办结', color: 'success' },
  };


  // 邀约信息列表
  yyColumns: STColumn[] = [

    { title: '邀约状态', index: 'AYW023', type: 'badge', badge: this.BADGE },
    /* { title: '调解结果', index: 'AYW051', type: 'tag', tag: this.TAG },
     { title: '调解场所', index: 'AYW045' },   
     { title: '调解时间', index: 'AYW044' },  */
    { title: '邀约部门', index: 'DGB010', dic: 'BMMC' },
    { title: '邀约时间', index: 'AYW021' },

    { title: '接受时间', index: 'AYW022' },
    { title: '协同人员', index: 'AYW024', dic: 'USERID' },
    {
      title: '操作区',
      buttons: [
        {
          text: '删除',
          icon: 'delete',
          pop: '确定要删除该协同邀约的信息吗？',
          click: (record: any) => this.doDel('delYy', { TYPE: 'YY', AYW043: record.AYW043, DGB010: record.DGB010 }),
        },
      ],
    },
  ];

  // 邀约表单
  yySchema: SFSchema = {
    properties: {
      AYW043: {
        type: 'number',
        ui: {
          hidden: true
        }
      },
      DGB010: {
        type: 'string',
        title: '邀约中心或部门',
        enum: [{ title: '调解服务中心', key: 'ZX81' }, { title: '诉讼服务中心', key: 'ZX82' }, { title: '人民来访接待中心', key: 'ZX83' },
        { title: '公共法律服务中心', key: 'ZX84' }, { title: '社会力量帮扶中心', key: 'ZX85' }, { title: '劳动仲裁服务中心', key: 'ZX86' }
        ],
        ui: {
          widget: 'tree-select',
          dropdownStyle: {
            height: '200px'
          },
          expandChange: (e: NzFormatEmitEvent) => this.getSdTree(e)
        }
      },
    },
    ui: {
      spanLabelFixed: 100,
      grid: {
        span: 24,
      },
    },
    required: ['DGB010']
  }


  // 申请延时信息列表
  sqztTAG: STColumnTag = {
    '2': { text: '申请中', color: 'blue' },
    '1': { text: '同意申请', color: 'green' },
    '0': { text: '拒绝申请', color: 'red' }
  };
  sqysColumns: STColumn[] = [
    { title: '序号', index: 'no', type: 'no' },
    { title: '申请状态', index: 'AYW057', type: 'tag', tag: this.sqztTAG },
    { title: '申请时间', index: 'AYW055' },
    { title: '延长时限', index: 'YCSX' },
    { title: '申请原因', index: 'AYW059' },
    { title: '审核时间', index: 'AYW056' },
    { title: '审核人', index: 'AAE012', dic: 'USERID' },
    { title: '审核意见', index: 'AYW058' },
    {
      title: '操作区',
      buttons: [
        {
          text: '查看详情',
          icon: 'edit',
          click: (record: any) => {
            this.addSqysForm.setValue('/AYW053', record.AYW053)
            this.addSqysForm.setValue('/AYW054', record.AYW054)
            this.addSqysForm.setValue('/AYW006', record.AYW006)
            this.addSqysForm.setValue('/AYW059', record.AYW059)
            this.openWindow('yssqAdd')
          }
        },
        {
          text: '删除',
          icon: 'delete',
          pop: '确定要删除该申请信息吗？',
          click: (record: any) => this.doDel('delSqys', { TYPE: 'SQYS', AYW053: record.AYW053 }),
        }
      ],
    },
  ];

  // 申请延时表单
  sqysSchema: SFSchema = {
    properties: {
      AYW053: {
        type: 'number',
        ui: {
          hidden: true
        }
      },
      AYW054: {
        type: 'number',
        title: '延长时间'
      },
      AYW006: {
        type: 'string',
        title: '时限单位',
        enum: this.supDic.getSFDic('AYW006'),
        ui: {
          widget: 'select',
        }
      },
      AYW059: {
        type: 'string',
        title: '申请原因',
        ui: {
          widget: 'textarea',
          autosize: { minRows: 2, maxRows: 6 },
          grid: {
            span: 24
          }
        }
      },

    },
    ui: {
      spanLabelFixed: 100,
      grid: {
        span: 12,
      },
    },
    required: ['AYW054', 'AYW006', 'AYW059']
  }


  // 申请转交信息列表
  sqzjColumns: STColumn[] = [
    { title: '序号', index: 'no', type: 'no' },
    { title: '申请状态', index: 'AYW057', type: 'tag', tag: this.sqztTAG },
    { title: '申请时间', index: 'AYW055' },
    { title: '转交中心', index: 'DGB010', dic: 'ZXBM' },
    /*
    { title: '变更事项', index: 'AYW001', dic: 'AYW001' },
    */
    { title: '申请原因', index: 'AYW059' },
    { title: '审核时间', index: 'AYW056' },
    { title: '审核人', index: 'AAE012', dic: 'USERID' },
    { title: '审核意见', index: 'AYW058' },
    {
      title: '操作区',
      buttons: [
        {
          text: '查看详情',
          icon: 'edit',
          click: (record: any) => {
            this.addSqzjForm.setValue('/AYW053', record.AYW053)
            this.addSqzjForm.setValue('/AYW001', [record.DGB010, '' + record.AYW001])
            this.addSqzjForm.setValue('/AYW059', record.AYW059)
            this.openWindow('zjsqAdd')
          }
        },
        {
          text: '删除',
          icon: 'delete',
          pop: '确定要删除该申请信息吗？',
          click: (record: any) => this.doDel('delSqzj', { TYPE: 'SQZJ', AYW053: record.AYW053 }),
        }
      ],
    },
  ];

  // 外置方法
  @Output() set TODO(value: string) {

    if (value === "QUERY") {
      this.doQuery()
    }
    else {
      this.openWindow(value)
    }
  }

  // 申请转交表单
  sqzjSchema: SFSchema = {
    properties: {
      AYW053: {
        type: 'number',
        ui: {
          hidden: true
        }
      },
      /*
      AYW001: {
        type: 'string',
        title: '转交部门',
        enum: this.supDic.getDic('CASCADER_AYW001'),
        ui: {
          widget: 'cascader',

          grid: {
            span: 24
          }
        }
      },
      */
      AYW059: {
        type: 'string',
        title: '申请原因',
        ui: {
          widget: 'textarea',
          autosize: { minRows: 2, maxRows: 6 },
          grid: {
            span: 24
          }
        }
      },
    },
    ui: {
      spanLabelFixed: 100,
      grid: {
        span: 24,
      },
    },
    required: [/*'AYW001',*/ 'AYW059']
  }


  // 调解信息列表
  @ViewChild('tjGrid', { static: false }) tjGrid: GridComponent;
  // 协同邀约列表
  @ViewChild('yyGrid', { static: false }) yyGrid: GridComponent;
  // 申请延时列表
  @ViewChild('sqysGrid', { static: false }) sqysGrid: GridComponent;
  // 申请转交列表
  @ViewChild('sqzjGrid', { static: false }) sqzjGrid: GridComponent;

  // 新增调解表单
  @ViewChild('addTjForm', { static: false }) addTjForm: SFComponent;
  // 修改调解表单
  @ViewChild('modiTjForm', { static: false }) modiTjForm: SFComponent;
  // 新增邀约表单
  @ViewChild('addYyForm', { static: false }) addYyForm: SFComponent;
  // 申请延时表单
  @ViewChild('addSqysForm', { static: false }) addSqysForm: SFComponent;
  // 申请转交表单
  @ViewChild('addSqzjForm', { static: false }) addSqzjForm: SFComponent;


  @Output() set AYW009(value: number) {
    this.dataAYW009 = value;
    this.queryPara = { AYW009: value }
    this.tjGrid.page.show = false;
    this.yyGrid.page.show = false;
    this.sqysGrid.page.show = false;
    this.sqzjGrid.page.show = false;

    this.tjGrid.reload(this.queryPara);
    this.yyGrid.reload(this.queryPara);
    this.sqzjGrid.reload(this.queryPara);
    this.sqysGrid.reload(this.queryPara);
  }

  @Output() set AYW001(value: number) {
    this.dataAYW001 = value;
    this.queryPara = { AYW001: value }
    this.tjGrid.page.show = false;
    this.yyGrid.page.show = false;
    this.sqysGrid.page.show = false;
    this.sqzjGrid.page.show = false;

    this.tjGrid.reload(this.queryPara);
    this.yyGrid.reload(this.queryPara);
    this.sqzjGrid.reload(this.queryPara);
    this.sqysGrid.reload(this.queryPara);
  }


  doDel(flag: string, para: any) {
    // 调解表单删除
    if (flag === 'tjDel') {
      // 调用删除服务
      this.lbservice.lbservice('TYJB_TJFORM_DEL', para).then(resdata => {
        if (resdata.code < 1) {
          this.msgSrv.error(resdata.errmsg);
        } else {
          this.msgSrv.info('删除成功');
          // 列表刷新
          this.tjGrid.reload(this.queryPara);
        }
      })
    }
    else {
      this.lbservice.lbservice('TYJB_YYFORM_DEL', para).then(resdata => {
        if (resdata.code < 1) {
          this.msgSrv.error(resdata.errmsg);
        } else {
          this.msgSrv.info('删除成功');
          // 列表刷新
          this.yyGrid.reload(this.queryPara);
          this.sqysGrid.reload(this.queryPara);
          this.sqzjGrid.reload(this.queryPara);
        }
      })
    }

  }

  // 打开弹出框
  openWindow(flag: string) {
    // 调解信息新增
    if (flag === 'tjAdd') {
      this.lbservice.lbservice('SJJK_QUERY', { AYW009: this.dataAYW009 }).then(resdata => {
        if (resdata.code < 1) {
          this.msgSrv.error(resdata.errmsg);
        } else {
          this.addTjForm.setValue('/AYW020', resdata.message.AYW020);
          this.isAddTjVisible = true;
        }
      })
    }
    // 调解信息修改
    else if (flag === 'tjModi') {
      this.isModiTjVisible = true;
    }
    // 延时申请
    else if (flag === 'yssqAdd') {
      this.isAddSqysVisible = true;
    }
    // 转交申请
    else if (flag === 'zjsqAdd') {
      this.isAddSqzjVisible = true;
    }
    // 协同
    else if (flag === 'xtAdd') {
      this.isAddYyVisible = true;
    }
    else {
      // this.isAddYyVisible = true;
    }
  }

  // 关闭弹出框
  closeWindow(flag: string) {
    // 调解信息新增
    if (flag === 'tjAdd') {
      this.isAddTjVisible = false;
    }
    // 调解信息修改
    else if (flag === 'tjModi') {
      this.isModiTjVisible = false;
    }
    // 延时申请
    else if (flag === 'yssqAdd') {
      this.isAddSqysVisible = false;
    }
    // 转交申请
    else if (flag === 'yszjAdd') {
      this.isAddSqzjVisible = false;
    }
    else {
      this.isAddYyVisible = false;
    }
  }

  // 保存
  doSave(flag: string) {
    if (flag === 'tjAdd') {
      if (!this.addTjForm.valid) {
        this.msgSrv.error('表单必输项不能为空');
        return;
      }
      this.lbservice.lbservice('TYJB_BLFORM_SAVE', { AYW009: this.dataAYW009, para: this.addTjForm.value }).then(resdata => {
        if (resdata.code < 1) {
          this.msgSrv.error(resdata.errmsg);
        } else {
          this.msgSrv.success('保存成功');
          this.closeWindow('tjAdd')
          this.tjGrid.reload(this.queryPara)
        }
      })
    }
    else if (flag === 'tjModi') {
      if (!this.modiTjForm.valid) {
        this.msgSrv.error('表单必输项不能为空');
        return;
      }
      this.lbservice.lbservice('TYJB_BLFORM_SAVE', { AYW009: this.dataAYW009, para: this.modiTjForm.value }).then(resdata => {
        if (resdata.code < 1) {
          this.msgSrv.error(resdata.errmsg);
        } else {
          this.msgSrv.success('信息变更成功');
          this.closeWindow('tjModi')
          this.tjGrid.reload(this.queryPara)
          this.yyGrid.reload(this.queryPara)
        }
      })

    }
    else if (flag === 'yssqAdd') {
      if (!this.addSqysForm.valid) {
        this.msgSrv.error('表单必输项不能为空');
        return;
      }
      this.lbservice.lbservice('TYJB_YSSQ_ADD', { AYW009: this.dataAYW009, para: this.addSqysForm.value }).then(resdata => {
        if (resdata.code < 1) {
          this.msgSrv.error(resdata.errmsg);
        } else {
          this.msgSrv.success('成功');
          this.closeWindow('yssqAdd')
          this.sqysGrid.reload(this.queryPara)
        }
      })

    }
    else if (flag === 'yszjAdd') {
      if (!this.addSqzjForm.valid) {
        this.msgSrv.error('表单必输项不能为空');
        return;
      }
      this.lbservice.lbservice('TYJB_YSZJ_ADD', { AYW009: this.dataAYW009, AYW001: this.dataAYW001, para: this.addSqzjForm.value }).then(resdata => {
        if (resdata.code < 1) {
          this.msgSrv.error(resdata.errmsg);
        } else {
          this.msgSrv.success('成功');
          this.closeWindow('yszjAdd')
          this.sqzjGrid.reload(this.queryPara)
        }
      })

    }
    else {
      if (!this.addYyForm.valid) {
        this.msgSrv.error('请选择协同分中心');
        return;
      }
      this.lbservice.lbservice('TYJB_YY_ADD', { AYW009: this.dataAYW009, para: this.addYyForm.value }).then(resdata => {
        if (resdata.code < 1) {
          this.msgSrv.error(resdata.errmsg);
        } else {
          this.msgSrv.success('协同邀请发送成功');
          this.closeWindow('yyAdd')
          this.yyGrid.reload(this.queryPara)
        }
      })

    }
  }

  doQuery() {
    this.tjGrid.reload(this.queryPara)
    this.yyGrid.reload(this.queryPara)
    this.sqysGrid.reload(this.queryPara)
    this.sqzjGrid.reload(this.queryPara)
  }

  constructor(
    private lbservice: HttpService,
    private supDic: SupDic,
    public msgSrv: NzMessageService,
  ) {

  }

  ngOnInit() {

  }

  node: NzTreeNode;
  getSdTree(e: NzFormatEmitEvent) {

    this.node = e.node;
    this.lbservice.lbservice('sd_selecttree', { key: e.node.origin.key }).then(resdata => {

      if (resdata.code < 1) {
        this.msgSrv.error(resdata.errmsg);
      }
      else {
        if (this.node.getChildren().length === 0 && this.node.isExpanded) {
          this.node.addChildren(resdata.message.list);
        }
      }
    })
  }


}