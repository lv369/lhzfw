import { Component, OnInit, Output } from '@angular/core';
import { HttpService } from 'lbf';
import { NzMessageService } from 'ng-zorro-antd';

@Component({
  selector: 'tyekou-step',
  templateUrl: './tyekou_step.html',  
  // styleUrls: ['./tyekou_tybl.less'],
})
export class TyekouStepComponent implements OnInit {
  steps:any = []

  @Output() set AYW009(value:number){
    this.dataAYW009 = value;
    if(this.dataAYW009!==undefined){
      // 初始化其他信息
      this.lbservice.lbservice('TYJB_DATA_INIT', {FLAG:'STEP',AYW009:this.dataAYW009 }).then(resdata => {
        console.log('-------STEP----------')
        if (resdata.code < 1) {
          this.msgSrv.error(resdata.errmsg);
        } else {
          console.log(resdata)
          this.steps = resdata.message.STEP;
        }
      }) 
    }
    
  }
  get AYW009(){
    return this.dataAYW009;
  }
  dataAYW009:number;

  constructor(
    private lbservice: HttpService,
    public msgSrv: NzMessageService,
    ) { }

  ngOnInit() {}
}