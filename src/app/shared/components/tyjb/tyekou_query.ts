import { Component, OnInit, ViewChild, Output } from '@angular/core';
import { _HttpClient, ModalHelper } from '@delon/theme';
import { NzMessageService } from 'ng-zorro-antd';
import { HttpService } from 'lbf';
import { GrFormQueryComponent } from './gr_form_query';
import { DwFormQueryComponent } from './dw_form_query';
import { BlFormQueryComponent } from './bl_form_query';
import { timingSafeEqual } from 'crypto';
import { TyekouStepComponent } from './tyekou_step';
import { PrintListComponent } from './lb_print_list';
import { SFSchema } from '@delon/form';

@Component({
  selector: 'tyekou-query',
  templateUrl: './tyekou_query.html',
  styleUrls: ['./tyekou_query.less'],
})
export class TyekouQueryComponent implements OnInit {
  topTitle = '申请信息详情44';

  // 控制页面显示 隐藏
  hidden_GrForm = true; // 个人申请表单
  hidden_DwForm = true; // 单位申请表单
  hidden_Step = true; // 流程图界面
  hidden_Tj = true; // 调解记录界面
  hidden_Print = true; // 单据凭证
  isBtnCur = false; // 办结
  isInfoVisible = false; //是否显示
  showTitle = ''; // 显示的标题
  isResultVisible = false; // 调解办结弹出框
  // 按钮组
  isBtnInfo = false; //申请信息
  isBtnTj = true; // 调解记录
  isBtnStep = true; // 流程图
  isBtnPrint = true; // 单据凭证


  styleParam = {}

  dataAYW001: number; // 事项编码
  dataAYW002: string; // 事项名称
  dataAYW004: string; // 事项主体类型
  dataAYW019: string; // 是否紧急事件
  dataYWINFO: string; // 业务标题
  @Output() set AYW009(value: number) {
  
    this.dataAYW009 = value;
    // 初始化其他信息
    this.lbservice.lbservice('TYJB_DATA_INIT', { FLAG: 'QUERY', AYW009: this.dataAYW009 }).then(resdata => {
      if (resdata.code < 1) {
        this.msgSrv.error(resdata.errmsg);
      } else {
        this.dataAYW001 = resdata.message.AYW001;
        this.dataAYW002 = resdata.message.AYW002;
        this.dataAYW004 = resdata.message.AYW004;
        this.dataAYW019 = resdata.message.AYW019;
        this.dataYWINFO = resdata.message.YWINFO;

        if (this.dataAYW004 === '1') {
          this.showDwForm();
        }
        else {
          this.showGrForm();
        }
        if (this.dataAYW019 === '1') {
          this.showTitle = '<font color="red"><b>【流水号：' + this.dataAYW009 + '】</b></font>--' + this.dataYWINFO + '<font color="red"><b>★紧急事项★</b>';
          this.isInfoVisible = true;
        } else {
          this.showTitle = '<font color="red"><b>【流水号：' + this.dataAYW009 + '】</b></font>--' + this.dataYWINFO;
          this.isInfoVisible = true;
        }
      }
    })
  }
  dataAYW009: number; // 业务流水号


  resultSchema: SFSchema = {
    properties: {
      AYW051: {
        type: 'string',
        title: '办理结果',
        enum: [{ label: '办理成功', value: '1' }, { label: '办理失败', value: '0' }],
        ui: {
          widget: 'radio',
          grid: {
            span: 24
          }
        },

      },
      AYW050: {
        type: 'string',
        title: '办理结果说明',
        ui: {
          widget: 'textarea',
          autosize: { minRows: 2, maxRows: 6 },

          grid: {
            span: 24
          }
        }
      },
      FLAG: {
        type: 'string',
        title: '上报负责人',
        enum: [{ label: '是', value: '1' }, { label: '否', value: '0' }],
        ui: {
          widget: 'radio',
          grid: {
            span: 24
          }
        },
        default: '1',
      }
    },
    ui: {
      spanLabelFixed: 100,
      grid: {
        span: 24,
      },
    },
    required: ['AYW050', 'AYW051', 'FLAG']
  }




  // 表单信息
  @ViewChild('grForm', { static: false }) grForm: GrFormQueryComponent;
  // 表单信息
  @ViewChild('dwForm', { static: false }) dwForm: DwFormQueryComponent;
  // 调解信息
  @ViewChild('tjForm', { static: false }) tjForm: BlFormQueryComponent;
  // 流程图
  @ViewChild('step', { static: false }) step: TyekouStepComponent;
  // 凭证清单
  @ViewChild('printList', { static: false }) printList: PrintListComponent;


  constructor(
    private lbservice: HttpService,
    public msgSrv: NzMessageService,
  ) { }

  ngOnInit() {
    this.styleParam = { height: (window.innerHeight - 250) + 'px' }
  }

  ngAfterView() {
    // this.dwForm.isShowSf = true;
  }


  // 隐藏所有功能，所有按钮失效
  hiddenAll() {
    this.hidden_GrForm = true;
    this.hidden_DwForm = true;
    this.hidden_Tj = true;
    this.hidden_Step = true;
    this.hidden_Print = true;
  
    this.isBtnInfo = false;
    this.isBtnStep = false;
    this.isBtnTj = false;
    this.isBtnPrint = false;
  }

  /********************************************* */
  /********************按钮组事件**************** */
  /********************************************* */
  // 导航栏按钮单击事件
  btnClick(flag: string) {
    // 申请信息
    if (flag === 'info') {
      if (this.dataAYW004 === '1') {
        this.showDwForm();
      }
      else {
        this.showGrForm();
      }
    }


    ///////////////////////////////////////////////////////////////////////////////////////////////

    /********************************/
    /***********流程图*************/
    /********************************/
    else if (flag === 'step') {
      this.showStep();
    }
    /********************************/
    /***********调解记录*************/
    /********************************/
    else if (flag === 'tj') {
      this.showTj();
    }

    // else if(flag === 'cur22'){
    //  this.model();

    // }
    else {
      this.showPrint();
    }

  }

  // model(){
  //   this.isResultVisible =true;
  // }
// 司法审核通过
  // save22(){
  //   this.isResultVisible=false;

  // }


  // 显示个人申请表单
  showGrForm() {

    this.topTitle = '申请信息详情';
    this.hiddenAll();

    // 流程图
    this.isBtnStep = true;
    this.isBtnTj = true;
    this.isBtnPrint = true;
    if (this.grForm.AYW009 === undefined || this.grForm.AYW009 !== this.dataAYW009) {
      this.grForm.AYW009 = this.dataAYW009; // 业务流水号赋值
    }

    this.hidden_GrForm = false;

  }

  // 显示单位申请表单
  showDwForm() {

    this.topTitle = '申请信息详情';

    this.hiddenAll();

    // 流程图
    this.isBtnStep = true;
    this.isBtnTj = true;
    this.isBtnPrint = true;

    if (this.dwForm.AYW009 === undefined || this.dwForm.AYW009 !== this.dataAYW009) {
      this.dwForm.AYW009 = this.dataAYW009; // 业务流水号赋值
    }

    this.hidden_DwForm = false;

  }

  // 
  showTj() {
    this.topTitle = '工作记录';

    this.hiddenAll();

    if (this.tjForm.AYW009 === undefined || this.tjForm.AYW009 !== this.dataAYW009) {
      this.tjForm.AYW009 = this.dataAYW009; // 业务流水号赋值
    }
    // 流程图
    this.isBtnInfo = true;
    this.isBtnStep = true;
    this.isBtnPrint = true;
    this.hidden_Tj = false;
  }

  showStep() {
    this.topTitle = '事项进度';

    this.hiddenAll();
    console.log('-step-')
    if (this.step.AYW009 === undefined || this.step.AYW009 !== this.dataAYW009) {
      this.step.AYW009 = this.dataAYW009; // 业务流水号赋值
    }
    // 流程图
    this.isBtnInfo = true;
    this.isBtnTj = true;
    this.isBtnPrint = true;

    this.hidden_Step = false;
  }

  showPrint() {
    this.topTitle = '凭证清单';

    this.hiddenAll();
    console.log('-print-')
    if (this.printList.AYW009 === undefined || this.printList.AYW009 !== this.dataAYW009) {
      this.printList.AYW009 = this.dataAYW009; // 业务流水号赋值
    }
    // 流程图
    this.isBtnInfo = true;
    this.isBtnTj = true;
    this.isBtnStep = true;

    this.hidden_Print = false;
  }

  closeWindow() {
    this.isInfoVisible = false;

    // 弹出框关闭的时候，将流水号清空，下次再打开，就可以刷新内部信息了
    this.step.AYW009 = undefined;
    this.dwForm.AYW009 = undefined;
    this.grForm.AYW009 = undefined;
    this.tjForm.AYW009 = undefined;
    // if(falg === 'result'){
    //   this.isResultVisible=false; 

    // }
  }


}
