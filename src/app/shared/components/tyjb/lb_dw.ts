import { Component, OnInit, ViewChild, Input, Output, EventEmitter } from '@angular/core';
import { HttpService, SupDic, GridComponent } from 'lbf';
import { SFComponent, SFSchema } from '@delon/form';
import { STColumn } from '@delon/abc';
import { NzMessageService } from 'ng-zorro-antd';


@Component({
  selector: 'lb-dw',
  templateUrl: './lb_dw.html',
  styleUrls: ['./lb_dw.less'],
})
export class DwComponent implements OnInit {

  addSchema: SFSchema = {
    properties: {
      AAB001: {
        type: 'string',
        title: '单位编号',
        ui: {
          hidden: true
        }
      },
      AAB021: { type: 'string', title: '统一信用代码' },
      AAB004: { type: 'string', title: '单位名称' },
      AAE025: {
        type: 'string',
        title: '本地所属区域',
        enum: this.supDic.getDic('CASCADER_SD'),
        ui: {
          placeholder: '本地企业，请填写此栏',
          widget: 'cascader',
          grid: {
            span: 24
          }
        }
      },
      AAE026: {
        type: 'string',
        title: '外地所属区域',
        ui: {
          placeholder: '外地企业，请填写此栏',
          grid: {
            span: 24
          }
        }
      },
      AAB007: {
        type: 'string',
        title: '详细地址',
        ui: {
          grid: {
            span: 24
          }
        }
      },
      AAB005: { type: 'string', title: '法人姓名' },
      AAB006: { type: 'string', title: '手机号码', format: 'mobile', },

    },
    ui: {
      spanLabelFixed: 100,
      grid: {
        span: 12,
      },
    },
    required: ['AAB004']
  }

  // 查询界面Schema
  querySchema: SFSchema = {
    properties: {
      AAB021: { type: 'string', title: '统一信用代码' },
      AAB004: { type: 'string', title: '单位名称' }
    }
  }

  // 单位列表
  dwColumns: STColumn[] = [

    {
      title: '操作区',
      fixed: 'left',
      buttons: [
        {
          text: '修改',
          icon: 'edit',
          click: (record: any) => {
            // 修改框赋值
            this.modiForm.setValue('/AAB001', record.AAB001) // 单位编号
            this.modiForm.setValue('/AAB021', record.AAB021) // 统一信用代码
            this.modiForm.setValue('/AAB004', record.AAB004) // 单位名称
            this.modiForm.setValue('/AAE025', [record.CAA001, record.AAE025]) // 社区
            this.modiForm.setValue('/AAB005', record.AAB005) // 法人
            this.modiForm.setValue('/AAB006', record.AAB006) // 手机号码
            this.modiForm.setValue('/AAB007', record.AAB007) // 联系地址
            this.modiForm.setValue('/AAE026',record.AAE026)
           
            


            // 打开修改弹出框
            this.openWindow('modi');
          },
        },
        {
          text: '删除',
          icon: 'delete',
          pop: '确定要删除该单位的信息吗？',
          click: (record: any) => this.doDel(record.AAB001),
        }
      ],
    },
    { title: '统一信用代码', index: 'AAB021' },
    { title: '单位名称', index: 'AAB004' },
    { title: '乡镇街道', index: 'CAA001', dic: 'CAA001' },
    { title: '村社区', index: 'AAE025', dic: 'AAE025' },
    { title: '外地所属区域', index: 'AAE026' },
    { title: '详细地址', index: 'AAB007' },
    { title: '法人姓名', index: 'AAB005' },
    { title: '法人手机', index: 'AAB006', },
    {
      title: '选择',
      fixed: 'right',
      buttons: [
        {
          text: '选取',
          icon: 'audit',
          click: (record: any) => this.tysjsl(record),
        },
      ],
    },
  ];

  @Output() clickItem = new EventEmitter<string>();

  isAddVisible = false; // 是否显示新增弹出框
  isModiVisible = false; // 是否显示修改弹出框


  // 新增表单页
  @ViewChild('addForm', { static: false }) addForm: SFComponent;
  // 修改表单页
  @ViewChild('modiForm', { static: false }) modiForm: SFComponent;
  // 单位列表
  @ViewChild('dwGrid', { static: false }) dwGrid: GridComponent;
  // 查询表单
  @ViewChild('queryForm', { static: false }) queryForm: SFComponent;


  // 选中单位事件
  tysjsl(record: any) {
    this.clickItem.emit(record);



  }

  // 单位列表查询
  doQuery(pi1 = false) {
    if (pi1) {
      this.dwGrid.pi = 1;
    }
    this.dwGrid.reload(this.queryForm.value);
  }

  // 新增保存
  save(flag: string) {
    // 必输项校验
    if (flag === 'add') {
      if (!this.addForm.valid) {
        this.msgSrv.error('必输项不能为空！');
        return;
      }
    }
    else {
      if (!this.modiForm.valid) {
        this.msgSrv.error('必输项不能为空！');
        return;
      }
    }



    // 调用新增服务
    this.lbservice.lbservice('TYJB_DW_ADD', { type: flag, para: flag === 'add' ? this.addForm.value : this.modiForm.value }).then(resdata => {
      if (resdata.code < 1) {
        this.msgSrv.error(resdata.errmsg);
      } else {
        // 关闭窗口
        this.closeWindow(flag)

        if (flag === 'add') {
          //重置表单
          this.addForm.reset()
          this.msgSrv.info('新增成功');
        }
        else {
          //重置表单
          this.modiForm.reset()
          this.msgSrv.info('修改成功');
        }

        // 列表刷新
        this.dwGrid.reload(this.queryForm.value);


      }
    });
  }

  // 单位删除操作
  doDel(dataAAC001: number) {
    // 调用删除服务
    this.lbservice.lbservice('TYJB_DW_DEL', { AAB001: dataAAC001 }).then(resdata => {
      if (resdata.code < 1) {
        this.msgSrv.error(resdata.errmsg);
      } else {
        this.msgSrv.info('删除成功');
        // 列表刷新
        this.dwGrid.reload(this.queryForm.value);
      }
    })
  }


  // 打开弹出框
  openWindow(openType: string) {
    if (openType === 'add') {
      this.isAddVisible = true;
    }
    else {
      this.isModiVisible = true;
    }

  }

  // 关闭弹出框
  closeWindow(closeType: string) {
    if (closeType === 'add') {
      this.isAddVisible = false;
    }
    else {
      this.isModiVisible = false;
    }
  }



  constructor(
    private lbservice: HttpService,
    private supDic: SupDic,
    public msgSrv: NzMessageService,
  ) {

  }

  ngOnInit() {

  }


}