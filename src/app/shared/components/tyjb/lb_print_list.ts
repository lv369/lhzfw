import { Component, OnInit, ViewChild, Input, Output, EventEmitter } from '@angular/core';
import { HttpService, SupDic, GridComponent } from 'lbf';
import { STColumn,  } from '@delon/abc';
import { NzMessageService } from 'ng-zorro-antd';
import { PrintComponent } from './lb_print';


@Component({
  selector: 'lb-print-list',
  templateUrl: './lb_print_list.html',  
  // styleUrls: ['./lb_gr.less'],
})
export class PrintListComponent implements OnInit {
  
  queryPara:any;
  isShowPrint = false; // 打印窗口是否显示
  hidden_Print = true; // 隐藏打印框
  // 列表
  printColumns: STColumn[] = [    
    { title: '序号', index: 'SEQ'},
    { title: '凭证名称', index: 'REPNAME'}, 
    {
      title: '操作区',
      buttons: [
        {
          text: '打印',
          click: (record: any) => this.doPrint(record),
        },
      ],
    },
  ];

  // 列表
  @ViewChild('printGrid', { static: false }) printGrid: GridComponent;
  
  // 表单信息
  @ViewChild('print', { static: false }) print: PrintComponent;
  

  // 选中人员事件
  doPrint(record:any) {
    this.print.QueryPara = { ayw009: this.dataAYW009 }


    // 打印窗口显示
     this.print.RepId = record.REPID;
    
    // this.print.ShowReport();
    
    this.isShowPrint = true;
    this.hidden_Print = false;
  }

  @Output() set AYW009(value:number){
  
    this.dataAYW009 = value;
    if(this.dataAYW009!==undefined){
      this.queryPara = {AYW009:this.dataAYW009}
      this.printGrid.page.show = false;
      this.printGrid.reload(this.queryPara);

    }
  }
  get AYW009(){
    return this.dataAYW009;
  }
  dataAYW009:number;

  
  constructor(
    private lbservice: HttpService,
    private supDic: SupDic,
    public msgSrv: NzMessageService,
  ) {
    
  }

  ngOnInit() {
    
  }

  // 关闭打印窗口
  CloseWindow(){
    this.isShowPrint = false;
  }


}