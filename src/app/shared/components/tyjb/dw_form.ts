import { Component, OnInit, ViewChild, Input, Output, EventEmitter } from '@angular/core';
import { HttpService, SupDic, GridComponent } from 'lbf';
import { SFComponent, SFSchema, SFRadioWidgetSchema } from '@delon/form';
import { STColumn, STColumnTag } from '@delon/abc';
import { NzMessageService } from 'ng-zorro-antd';
import { delay } from 'rxjs/operators';
import { of } from 'rxjs';
import { DwComponent } from './lb_dw';
import { Key } from 'protractor';
import { GrComponent } from './lb_gr';


@Component({
  selector: 'dw-form',
  templateUrl: './dw_form.html',
  styleUrls: ['./lb_sxlist.less'],
})
export class DwFormComponent implements OnInit {
  
  // 申请人表单
  sqrSchema: SFSchema = {
    properties: {
      AAB001: { 
        type: 'string', 
        title: '单位编号',
        ui: {
          hidden:true 
        }
      },
      AAB021: { type: 'string', title: '统一信用代码',readOnly:true },
      AAB004: { type: 'string', title: '单位名称',readOnly:true },
      AAB005: { type: 'string', title: '法人姓名' },
      AAB006: { type: 'string', title: '手机号码',format: 'mobile', }, 
      AAE025: {
        type: 'string',
        title: '居住区域',
        enum: this.supDic.getDic('CASCADER_SD'),
        ui: {
          widget: 'cascader',
          
        }
      },

      AAB007: { 
        type: 'string', 
        title: '详细地址',
        ui:{
          grid:{
            span:12
          }
        } 
      },
      
    },
    ui: {
      spanLabelFixed: 100,
      grid: {
        span: 6,
      },
    },
    required: ['AAB004', 'AAE025', 'AAB006', 'AAB005']
  }
  isDwVisible = false; // 是否弹出单位选择框
  isGrVisible = false; // 是否弹出个人选择框

  dataZTBH:number // 主体编号
  dataAYW009:number // 业务流水号

  queryPara = {}
  paramStyle = {height: (window.outerHeight-262)+'px',overflow: 'auto'}

  TAG: STColumnTag = {
    1: { text: '单位', color: 'green' },
    2: { text: '个人', color: 'red' }
  };

  // 被申请人列表
  bsqColumns: STColumn[] = [  
    
    { title: '主体类型', index: 'ZTLX', type: 'tag', tag: this.TAG },
    { title: '主体编号', index: 'ZTBH' },    
    { title: '主体名称', index: 'ZTMC' },    
    { title: '乡镇街道', index: 'CAA001',dic:'CAA001' },
    { title: '村社区', index: 'AAE025',dic:'AAE025' },  
    { title: '详细地址', index: 'ADDR' },  
    { title: '联系电话', index: 'PHONE' },
    {
      title: '操作',
      fixed: 'right',
      buttons: [
        {
          text: '删除',
          icon: 'delete',
          pop: '确定要删除该被申请(人/单位)的信息吗？',
          click: (record: any) => this.doDel(record.AYW037),
        },
      ],
    },
  ];

  // 矛盾纠纷表单
  mdjfSchema:SFSchema = {
    properties: {
      AYW014: { 
        type: 'string', 
        title: '发生日期',
        ui: { widget: 'date' } 
      },
      FSTIME: {
        type: 'string', 
        title: '发生时间',
        ui: { widget: 'time' } 
      },
      AYW015: { type: 'string', title: '发生地点' },      
      AYW016: { type: 'number', title: '涉及人数' },
      AYW017: {
        type: 'string',
        title: '对象主体',
        enum: this.supDic.getSFDic('AYW017'),
        ui: {
          widget: 'radio',
          grid: {
            span: 12
          }
        }
      },
      AYW018: {
        type: 'string',
        title: '重大事件',
        enum: [{ label: '是', value: '1' }, { label: '否', value: '0' }],
        ui: {
          widget: 'radio',
          
        },
        default: '0'

      },
      AYW019: {
        type: 'string',
        title: '紧急事件',
        enum: [{ label: '是', value: '1' }, { label: '否', value: '0' }],
        ui: {
          widget: 'radio',
          
        }
        ,
        default: '0'
      },
      AYW020: { 
        type: 'string', 
        title: '事件简况',
        ui: {
          widget: 'textarea',
          autosize: { minRows: 3, maxRows: 10 },
          grid: {
            span: 24
          }
        } 
      },           
    },
    ui: {
      spanLabelFixed: 100,
      grid: {
        span: 6,
      },
    },
    required: [ 'AYW015', 'AYW016', 'AYW017', 'AYW018', 'AYW019', 'AYW020']
  }

  // 申请人表单
  @ViewChild('sqrForm', { static: false }) sqrForm: SFComponent;
  // 被申请单位选择界面
  @ViewChild('dw', { static: false }) dw: DwComponent;
  // 被个人单位选择界面
  @ViewChild('gr', { static: false }) gr: GrComponent;
  // 被申请列表
  @ViewChild('bsqGrid', { static: false }) bsqGrid: GridComponent;
  // 矛盾纠纷表单
  @ViewChild('mdjfForm', { static: false }) mdjfForm: SFComponent;

  @Output() set AYW009(value:number){
    this.dataAYW009 = value;
    if(value!==undefined){
      this.queryPara = { AYW009:value }
      // 初始化其他信息
      this.lbservice.lbservice('TYJB_DATA_INIT', {FLAG:'FORM',AYW009:this.dataAYW009 }).then(resdata => {
        console.log('-------form_dw----------')
        if (resdata.code < 1) {
          this.msgSrv.error(resdata.errmsg);
        } else {
          this.setDwData(resdata.message.ZT);
          this.setInfoData(resdata.message.INFO )
        }
      }) 
    }
    
  }
  get AYW009(){
    return this.dataAYW009;
  }

  
  setInfoData(record:any){
    this.mdjfForm.setValue('/AYW014',record.AYW014)
    this.mdjfForm.setValue('/FSTIME',record.FSTIME)
    this.mdjfForm.setValue('/AYW015',record.AYW015)
    this.mdjfForm.setValue('/AYW016',record.AYW016)
    this.mdjfForm.setValue('/AYW017',record.AYW017)
    this.mdjfForm.setValue('/AYW018',record.AYW018)
    this.mdjfForm.setValue('/AYW019',record.AYW018)
    this.mdjfForm.setValue('/AYW020',record.AYW018)
  }

  // 申请人信息赋值
  setDwData(record:any){
    this.sqrForm.setValue('/AAB001',record.AAB001) // 单位编号
    this.sqrForm.setValue('/AAB021',record.AAB021) // 统一信用代码
    this.sqrForm.setValue('/AAB004',record.AAB004) // 单位名称
    this.sqrForm.setValue('/AAE025',[record.CAA001,record.AAE025]) // 社区
    this.sqrForm.setValue('/AAB005',record.AAB005) // 法人姓名
    this.sqrForm.setValue('/AAB006',record.AAB006) // 手机号码
    this.sqrForm.setValue('/AAB007',record.AAB007) // 详细地址

    // 列表刷新
    this.bsqGrid.reload({AYW009:this.dataAYW009});
  }

  // 获取申请人信息
  @Output() get sqrValue(){
    return this.sqrForm.value;
  }

  // 获取申请人有效校验信息
  @Output() get sqrValid():boolean{
    return this.sqrForm.valid;
  }

  // 获取矛盾纠纷信息
  @Output() get mdjfValue(){
    return this.mdjfForm.value;
  }

  // 获取矛盾纠纷有效校验信息
  @Output() get mdjfValid():boolean{
    return this.mdjfForm.valid;
  }



  doDel(key:number){
    
    // 调用删除服务
    this.lbservice.lbservice('TYJB_BSQ_DEL', {AYW037:key}).then(resdata => {
      if (resdata.code < 1) {
        this.msgSrv.error(resdata.errmsg);
      } else {        
        this.msgSrv.info('删除成功');
        // 列表刷新
        this.bsqGrid.reload({AYW009:this.dataAYW009});
      }
    })
  }

  // 打开弹出框
  openWindow(flag:string){
    if(flag==='dw'){
      this.isDwVisible = true;
    }
    else{
      this.isGrVisible = true;
    }
  }

  // 关闭弹出框
  closeWindow(flag:string){
    if(flag==='dw'){
      this.isDwVisible = false;
    }
    else{
      this.isGrVisible = false;
    }
  }

  // 个人、单位信息 选择单击事件
  selectRow(item:any){
    // 单位选中
   if(item.AAC001===undefined){
     this.dataZTBH = item.AAB001;
     // 被申请人
     this.lbservice.lbservice('TYJB_BSQ_ADD', {TYPE:'1',ZTBH:item.AAB001,AYW009:this.dataAYW009 }).then(resdata => {
      if (resdata.code < 1) {
        this.msgSrv.error(resdata.errmsg);
      } else {
        this.closeWindow('dw')
        this.bsqGrid.reload({AYW009:this.dataAYW009})
        this.msgSrv.success('添加成功');
        
      }
     })
   }
   // 个人选中
   else{
     this.dataZTBH = item.AAC001;
     // 被申请人
    this.lbservice.lbservice('TYJB_BSQ_ADD', {TYPE:'2',ZTBH:item.AAC001,AYW009:this.dataAYW009 }).then(resdata => {
      if (resdata.code < 1) {
        this.msgSrv.error(resdata.errmsg);
      } else {
        this.closeWindow('gr')
        this.bsqGrid.reload({AYW009:this.dataAYW009})
        this.msgSrv.success('添加成功');
        
      }
    })
   }

   
   

  }
  
  constructor(
    private lbservice: HttpService,
    private supDic: SupDic,
    public msgSrv: NzMessageService,
  ) {
    
  }

  ngOnInit() {
    
  }


}