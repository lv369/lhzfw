import { NzMessageService } from 'ng-zorro-antd';
import { HttpService } from 'lbf';
import { Component, OnInit, Output } from '@angular/core';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';

@Component({
  selector: 'lb-print',
  templateUrl: './lb_print.html',
})
export class PrintComponent implements OnInit {
  ifrmeee: SafeResourceUrl;
  isShowRep = false;
  isShowErr = false;
  errLabel: string;

  // 传入报表ID
  @Output() set RepId(value: string) {
    this._repId = value;
    this.lbservice.lbservice('repurl_query', { para: { REPID: this._repId } }).then(resdata => {
      if (resdata.code > 0) {
        this._repUrl = resdata.message.URL;
        this.ShowReport()
      }
      else {
        this._repUrl = undefined;
        console.log('获取报表模板错误：' + resdata.errmsg)
      }
    })
  }
  _repId: string

  // 直接传入报表URL
  @Output() set RepUrl(value: string) {
    this._repUrl = value;
  }
  _repUrl: string

  // 报表参数
  @Output() set QueryPara(value: any) {
    this._queryPara = value;
  }
  _queryPara: any

  // 显示报表
  @Output() ShowReport() {
    //this._repUrl = '/webroot/decision/view/report?viewlet=lhzfw/2.cpt';
    // 大致校验一下URL格式
    if (this._repUrl !== undefined && this._repUrl.indexOf('.cpt') > 0) {
      this.isShowRep = true;
      this.isShowErr = false;

      // 拼接地址
      for (const key of Object.keys(this._queryPara)) {
      this._repUrl += ('&' + key + '=' + this._queryPara[key])
      }
      console.log('---------------------------------------------------');
      console.log( this._repUrl);
      console.log('---------------------------------------------------');
      this.ifrmeee = this.sanitizer.bypassSecurityTrustResourceUrl(this._repUrl);
   

    }
    // 显示错误信息
    else {
      this.isShowErr = true;
      this.isShowRep = false;
      this.errLabel = '不存在的报表，或报表未配置';
    }


  }

  constructor(
    public msgSrv: NzMessageService,
    private lbservice: HttpService,
    private sanitizer: DomSanitizer,
  ) { }

  ngOnInit() {

  }
}