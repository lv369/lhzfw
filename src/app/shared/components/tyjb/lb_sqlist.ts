import { Component, OnInit, ViewChild, Input, Output, EventEmitter } from '@angular/core';
import { HttpService, SupDic, GridComponent } from 'lbf';
import { SFComponent, SFSchema } from '@delon/form';
import { STColumn, STColumnTag, STColumnBadge } from '@delon/abc';
import { NzMessageService } from 'ng-zorro-antd';


@Component({
  selector: 'lb-sqlist',
  templateUrl: './lb_sqlist.html',  
  // styleUrls: ['./lb_gr.less'],
})
export class SqListComponent implements OnInit {
  addSchema: SFSchema
  querySchema: SFSchema = {
    properties: {
     
      ZTBH: { type: 'string', title: '主体编号' },
      ZTMC: { type: 'string', title: '主体名称' },
      KSSJ: { 
        type: 'string', 
        title: '开始时间',
        ui: { widget: 'date' } 
      },
      JSSJ: { 
        type: 'string', 
        title: '结束时间',
        ui: { widget: 'date' } 
      },
    }
  }
  

  // 主体对象类别渲染标签 AYW004
  TAG: STColumnTag = {
    '1': { text: '已确认主体', color: 'purple' },
    '2': { text: '表单已确认', color: 'blue' },
    '3': { text: '回执已打印', color: 'orange' },
    '4': { text: '申请成功', color: 'green' },
    '5': { text: '作废', color: 'red' },
  };

  BADGE: STColumnBadge = {
    '1': { text: '单位', color: 'success' },
    '2': { text: '个人', color: 'processing' },
  };


  // 列表
  grColumns: STColumn[] = [    
    { title: '流水号', index: 'AYW009',width:70 },
    { title: '部门指派', index: 'BMMC',width:100},
    { title: '事项大类', index: 'AYW120',dic:'AYW120',width:100},
    { title: '纠纷事项', index: 'AYW001',dic:'AYW001',width:100},
    { title: '当前状态', index: 'AYW034',type: 'tag', tag: this.TAG,width:100},
    { title: '最近操作时间', index: 'AAE035',width:100},   
    { title: '对象主体', index: 'ZTLX', type: 'badge', badge: this.BADGE,width:100},    
    { title: '主体编号', index: 'ZTBH',width:100 },
    { title: '主体名称', index: 'ZTMC',width:100 },    
    { title: '乡镇街道', index: 'CAA001',dic:'CAA001',width:100 },
    { title: '村社区', index: 'AAE025',dic:'AAE025',width:100 },  
    { title: '详细地址', index: 'ADDR',width:100 },  
    { title: '手机号码', index: 'PHONE',width:100 },      
    {
      title: '操作区',
      fixed: 'right',
      width: 80,
      buttons: [
        {
          text: record=>{
            // 人员选择完毕，下一步为表单登记
            if(Number(record.AYW034)===1){
              return '表单登记'
            }
            /*
            // 表单登记完毕--> 打印回执
            else if(Number(record.AYW034)===2){
              return '人员指派'
            }
            */
            // 剩下的环节不允许再操作
            else{
              return '查看详情'
            }
            
          },
          icon: 'audit',
          click: (record: any) => this.tysjsl(record),
        },
      ],
    },
  ];

  
  @Output() set InitGrid(value:number){
    this.doQuery();
  }
  
  @Output() clickItem = new EventEmitter<string>();


  // 列表
  @ViewChild('grGrid', { static: false }) grGrid: GridComponent;
  // 查询表单
  @ViewChild('queryForm', { static: false }) queryForm: SFComponent;
  
  

  // 选中人员事件
  tysjsl(record:any) {
    this.clickItem.emit(record);
  }

  // 人员列表查询
  doQuery(pi1=false) {
    if(pi1){
      this.grGrid.pi=1;
    }
    this.grGrid.reload(this.queryForm.value);
  }

    


 
  
  constructor(
    private lbservice: HttpService,
    private supDic: SupDic,
    public msgSrv: NzMessageService,
  ) {
    
  }

  ngOnInit() {
    
  }


}