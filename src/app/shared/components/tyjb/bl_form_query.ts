import { Component, OnInit, ViewChild, Output } from '@angular/core';
import { HttpService, SupDic, GridComponent } from 'lbf';
import { SFComponent, SFSchema } from '@delon/form';
import { STColumn, STColumnTag, STColumnBadge } from '@delon/abc';
import { NzMessageService } from 'ng-zorro-antd';


@Component({
  selector: 'bl-form-query',
  templateUrl: './bl_form_query.html',
  styleUrls: ['./lb_sxlist.less'],
})
export class BlFormQueryComponent implements OnInit {
  
  isAddTjVisible = false; // 是否弹出单位选择框
  isModiTjVisible = false; // 是否弹出个人选择框
  isAddYyVisible = false; // 部门邀约弹出框
  isAddSqysVisible = false; // 申请延时
  isAddSqzjVisible = false; // 申请转交

  dataAYW009:number // 业务流水号

  queryPara = {}

  paramStyle = {height: (window.outerHeight-262)+'px',overflow: 'auto'}


  TAG: STColumnTag = {
    '2': { text: '暂未办理', color: 'blue' },
    '1': { text: '办理成功', color: 'green' },
    '0': { text: '办理失败', color: 'red' }
  };

  // 调解信息列表
  tjColumns: STColumn[] = [  
    { title: '场次', index: 'AYW052',type:'number' },  
    { title: '办理结果', index: 'AYW051', type: 'tag', tag: this.TAG },
    { title: '办理场所', index: 'AYW045' },   
    { title: '办理时间', index: 'AYW044' },  
    { title: '记录人', index: 'AYW046' },   
    { title: '办理主持人', index: 'AYW047' },   
    { title: '参加人员', index: 'AYW048' },
    {
      title: '操作区',
      fixed: 'right',
      buttons: [
        {
          text: '查看详情',
          icon: 'edit',
          click: (record: any) => {
            this.modiTjForm.setValue('/AYW043',record.AYW043)
            this.modiTjForm.setValue('/AYW044',record.AYW044)
            this.modiTjForm.setValue('/AYW045',record.AYW045===undefined?'':record.AYW045)
            this.modiTjForm.setValue('/AYW046',record.AYW046===undefined?'':record.AYW046)
            this.modiTjForm.setValue('/AYW047',record.AYW047===undefined?'':record.AYW047)
            this.modiTjForm.setValue('/AYW048',record.AYW048===undefined?'':record.AYW048)
            this.modiTjForm.setValue('/AYW049',record.AYW049===undefined?'':record.AYW049)
            this.modiTjForm.setValue('/AYW050',record.AYW050===undefined?'':record.AYW050)
            this.modiTjForm.setValue('/AYW051',record.AYW051===undefined?'':record.AYW051)
            this.modiTjForm.setValue('/FSTIME',record.FSTIME===undefined?'':record.FSTIME)

            this.openWindow('tjModi')
          },
        },
       
      ],
    },
  ];

  // 办理表单
  tjSchema:SFSchema = {
    properties: {
      AYW043: { 
        type: 'number', 
        ui: {
          hidden: true
        }        
      },
      AYW044: { 
        type: 'string', 
        title: '调解日期',
        ui: { widget: 'date' } 
      },
      FSTIME: {
        type: 'string', 
        title: '调解时间',
        ui: { widget: 'time' } 
      },
      AYW045: {
        type: 'string',
        title: '调解场所',
        ui: {
          grid: {
            span: 12
          }
        }
      },
      AYW046: {
        type: 'string',
        title: '记录人',
      },
      AYW047: {
        type: 'string',
        title: '调解主持人',
      },
      AYW048: {
        type: 'string',
        title: '参加人员',
        ui: {
          grid: {
            span: 12
          }
        }
      },
      AYW049: {
        type: 'string',
        title: '纠纷主要事实',
        ui: {
          widget: 'textarea',
          autosize: { minRows: 2, maxRows: 6 },
          
          grid: {
            span:24
          }
        }
      },
      AYW050: {
        type: 'string',
        title: '调解结果说明',
        ui: {
          widget: 'textarea',
          autosize: { minRows: 2, maxRows: 6 },
          
          grid: {
            span:24
          }
        }
      },
      AYW051: {
        type: 'string',
          title: '调解结果',
          enum: [{ label: '暂未调解', value: '2' }, { label: '调解成功', value: '1' }, { label: '调解失败', value: '0' }],
          ui: {
            widget: 'radio',
            grid: {
              span: 24
            }
          },
          default: '2',
      }
    },
    ui: {
      spanLabelFixed: 100,
      grid: {
        span: 6,
      },
    },
    required: ['AYW045','AYW051']
  }

  BADGE: STColumnBadge = {
    '2': { text: '未审核', color: 'default' },
    '1': { text: '审核通过', color: 'processing' },
    '0': { text: '不同意', color: 'error' },
    '3': { text: '已接受', color: 'success' },
    '4': { text: '已办结', color: 'success' },
  };



  // 邀约信息列表
  yyColumns: STColumn[] = [      
   /* { title: '场次', index: 'AYW052',type:'number' },  */
    { title: '邀约状态', index: 'AYW023',type:'badge',badge:this.BADGE },  
    /*{ title: '调解结果', index: 'AYW051', type: 'tag', tag: this.TAG },
    { title: '调解场所', index: 'AYW045' },   
    { title: '调解时间', index: 'AYW044' },*/  
    { title: '邀约部门', index: 'DGB010', dic: 'ZXBM' },
    { title: '邀约时间', index: 'AYW021' },  
    
    { title: '接受时间', index: 'AYW022' }, 
    { title: '协同人员', index: 'AYW024',dic:'USERID' },
    
  ];
 
  // 邀约表单
  yySchema:SFSchema = {
    properties: {
      AYW043: { 
        type: 'number', 
        ui: {
          hidden: true
        }        
      },
      DGB010: { 
        type: 'string', 
        title: '协同部门',
        enum: this.supDic.getSFDic('DGB010'),
        ui: { widget: 'select' } 
      },
      
    },
    ui: {
      spanLabelFixed: 100,
      grid: {
        span: 24,
      },
    },
    required: ['DGB010']
  }

  
  // 申请延时信息列表
  sqztTAG: STColumnTag = {
    '2': { text: '申请中', color: 'blue' },
    '1': { text: '同意申请', color: 'green' },
    '0': { text: '拒绝申请', color: 'red' }
  };
  sqysColumns: STColumn[] = [     
    { title: '序号',index:'no',type:'no' },  
    { title: '申请状态', index: 'AYW057', type: 'tag', tag: this.sqztTAG },
    { title: '申请时间', index: 'AYW055' },   
    { title: '延长时限', index: 'YCSX' },  
    { title: '申请原因', index: 'AYW059' },
    { title: '审核时间', index: 'AYW056' },  
    { title: '审核人', index: 'AAE012',dic:'USERID' },  
    { title: '审核意见', index: 'AYW058' }, 
   
  ];
 
  // 申请延时表单
  sqysSchema:SFSchema = {
    properties: {
      AYW053: { 
        type: 'number', 
        ui: {
          hidden: true
        }        
      },
      AYW054: {
        type: 'number',
        title: '延长时间'
      },
      AYW006: {
        type: 'string',
        title: '时限单位',
        enum: this.supDic.getSFDic('AYW006'),
        ui: {
          widget: 'select',          
        }
      },
      AYW059: { 
        type: 'string', 
        title: '申请原因',
        ui: {
          widget: 'textarea',
          autosize: { minRows: 2, maxRows: 6 },
          grid: {
            span: 24
          }
        } 
      },
      
    },
    ui: {
      spanLabelFixed: 100,
      grid: {
        span: 12,
      },
    },
    required: ['AYW054','AYW006','AYW059']
  }


   // 申请转交信息列表
   sqzjColumns: STColumn[] = [      
    { title: '序号',index:'no',type:'no' },
    { title: '申请状态', index: 'AYW057', type: 'tag', tag: this.sqztTAG },
    { title: '申请时间', index: 'AYW055' },   
    { title: '转交部门', index: 'DGB010',dic:'DGB010' },  
    { title: '变更事项', index: 'AYW001',dic:'AYW001' },  
    { title: '申请原因', index: 'AYW059' },
    { title: '审核时间', index: 'AYW056' },  
    { title: '审核人', index: 'AAE012',dic:'USERID' },  
    { title: '审核意见', index: 'AYW058' }, 
    
  ];
 
  // 申请转交表单
  sqzjSchema:SFSchema = {
    properties: {
      AYW053: { 
        type: 'number', 
        ui: {
          hidden: true
        }        
      },
      AYW001: { 
        type: 'string', 
        title: '转交部门',
        enum: this.supDic.getDic('CASCADER_AYW001'),
        ui: {
          widget: 'cascader',
          
          grid: {
            span:24
          }
        }
      },
      AYW059: { 
        type: 'string', 
        title: '申请原因',
        ui: {
          widget: 'textarea',
          autosize: { minRows: 2, maxRows: 6 },
          grid: {
            span: 24
          }
        } 
      },
    },
    ui: {
      spanLabelFixed: 100,
      grid: {
        span: 24,
      },
    },
    required: ['AYW001','AYW059']
  }


  // 调解信息列表
  @ViewChild('tjGrid', { static: false }) tjGrid: GridComponent;
  // 协同邀约列表
  @ViewChild('yyGrid', { static: false }) yyGrid: GridComponent;
  // 申请延时列表
  @ViewChild('sqysGrid', { static: false }) sqysGrid: GridComponent;
  // 申请转交列表
  @ViewChild('sqzjGrid', { static: false }) sqzjGrid: GridComponent;
  
  // 修改调解表单
  @ViewChild('modiTjForm', { static: false }) modiTjForm: SFComponent;
  
  
  @Output() set AYW009(value:number){

      this.dataAYW009 = value;
      if(this.dataAYW009!==undefined){
        this.queryPara = { AYW009:value }
        this.tjGrid.page.show = false;
        this.yyGrid.page.show = false;
        this.sqysGrid.page.show = false;
        this.sqzjGrid.page.show = false;

        this.tjGrid.reload(this.queryPara);
        this.yyGrid.reload(this.queryPara);
        this.sqzjGrid.reload(this.queryPara);
        this.sqysGrid.reload(this.queryPara);
      }
      
    
  }
  get AYW009(){
    return this.dataAYW009;
  }


  

  // 打开弹出框
  openWindow(flag:string){
    if(flag==='tjModi'){
      this.isModiTjVisible = true;
    }
  }

  // 关闭弹出框
  closeWindow(flag:string){
    if(flag==='tjModi'){
      this.isModiTjVisible = false;
    }
  }

  constructor(
    private lbservice: HttpService,
    private supDic: SupDic,
    public msgSrv: NzMessageService,
  ) {
    
  }

  ngOnInit() {
    
  }


}