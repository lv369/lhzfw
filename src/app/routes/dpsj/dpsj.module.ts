import { NgModule } from '@angular/core';
import { SharedModule } from '@shared';
import { DpsjRoutingModule } from './dpsj-routing.module';
import { GaxxComponent } from './gaxx/gaxx.component';
import { JjxxComponent } from './jjxx/jjxx.component';
import { PatxxComponent } from './patxx/patxx.component';
import { XsajComponent } from './xsaj/xsaj.component';
import { BjxxComponent } from './bjxx/bjxx.component';

const COMPONENTS = [
  GaxxComponent
];
const COMPONENTS_NOROUNT = [];

@NgModule({
  imports: [
    SharedModule,
    DpsjRoutingModule
  ],
  declarations: [
    ...COMPONENTS,
    ...COMPONENTS_NOROUNT,
    GaxxComponent,
    JjxxComponent,
    PatxxComponent,
    XsajComponent,
    BjxxComponent
  ],
  entryComponents: COMPONENTS_NOROUNT
})
export class DpsjModule { }
