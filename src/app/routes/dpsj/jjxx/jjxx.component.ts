import { Component, OnInit, ViewChild } from '@angular/core';
import { _HttpClient } from '@delon/theme';
import { STColumn, STColumnTag } from '@delon/abc';
import { SFSchema, SFComponent } from '@delon/form';
import { SupDic, HttpService, GridComponent } from 'lbf';
import { NzMessageService, NzTreeComponent } from 'ng-zorro-antd';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-jjxx',
  templateUrl: './jjxx.component.html',
  styleUrls: ['./jjxx.component.less']
})
export class JjxxComponent implements OnInit {
  addFormData = {}; // 新增弹出框表单数据
  modiFormData = {}; // 修改弹出框表单数据
  queryFormData = {}; // 查询区表单数据
  showAddWindow = false; // 是否显示新增弹出框
  showModiWindow = false; // 是否显示修改弹出框
  apd: any = {};

  // 事项列表
  @ViewChild('st', { static: true }) st: GridComponent;
  // 事项新增表单
  @ViewChild('addWindow', { static: false }) addWindow: SFComponent;
  // 事项修改表单
  @ViewChild('modiWindow', { static: false }) modiWindow: SFComponent;
  // 查询区表单
  @ViewChild('searchForm', { static: false }) searchForm: SFComponent;
  // 人员树
  @ViewChild('userTree', { static: false }) userTree: NzTreeComponent;

  // 查询区
  searchSchema: SFSchema = {
    properties: {
      AAE002: {
        type: 'string',
        title: '月份',
      }
    },
    ui: {
      spanLabelFixed: 100,
      grid: {
        span: 6
      }
    }
  };

  // 新增表单
  addSchema = {
    properties: {
      AAE002: {
        type: 'string',
        title: '月份',
        ui:
        {
          hidden: true
        }
      },
      JGA020: {
        type: 'number',
        title: '上月总接警量'
      },
      JGA010: {
        type: 'number',
        title: '总接警量同比'
      },
      JGA021: {
        type: 'number',
        title: '上月有效警情'
      },
      JGA011: {
        type: 'number',
        title: '有效警情同比'
      },
      JGA022: {
        type: 'number',
        title: '上月指令处警'
      },
      JGA012: {
        type: 'number',
        title: '指令处警同比'
      },
      JGA023: {
        type: 'number',
        title: '上月重复警情'
      },
      JGA013: {
        type: 'number',
        title: '重复警情同比'
      },
    },
    ui: {
      spanLabelFixed: 90,
      grid: {
        span: 12
      }
    },
    required: ['JGA020', 'JGA021', 'JGA022', 'JGA023', 'JGA010', 'JGA011', 'JGA012', 'JGA013']
  };

  gridData = [];

  // 事项列表生成串
  columns: STColumn[] = [
    { title: '月份', index: 'AAE002' },
    { title: '总警情数量', index: 'JGA020' },
    { title: '总警情同比', index: 'JGA010' },
    { title: '有效警情数量', index: 'JGA021' },
    { title: '有效警情同比', index: 'JGA011' },
    { title: '指令处警数量', index: 'JGA022' },
    { title: '指令处警同比', index: 'JGA012' },
    { title: '重复警情数量', index: 'JGA023' },
    { title: '重复警情同比', index: 'JGA013' },
    { title: '操作人员', index: 'AAE011', dic: 'USERID' },
    { title: '操作时间', index: 'AAE036', type: 'date' },
    {
      title: '操作区', buttons: [
        {
          text: '修改',
          icon: 'edit',
          click: (record: any) => {
            // 给修改编辑框赋值
            this.modiWindow.setValue('/AAE002', (record.AAE002));
            this.modiWindow.setValue('/JGA020', (record.JGA020));
            this.modiWindow.setValue('/JGA010', (record.JGA010));
            this.modiWindow.setValue('/JGA021', (record.JGA021));
            this.modiWindow.setValue('/JGA011', (record.JGA011));
            this.modiWindow.setValue('/JGA022', (record.JGA022));
            this.modiWindow.setValue('/JGA012', (record.JGA012));
            this.modiWindow.setValue('/JGA023', (record.JGA023));
            this.modiWindow.setValue('/JGA013', (record.JGA013));
            // 打开修改编辑框
            this.openWindow('modi');
          }
        },
      ]
    },
  ];

  constructor(private http: HttpClient,
    private dic: SupDic,
    private lbs: HttpService,
    public msgSrv: NzMessageService,
  ) { }

  ngOnInit() {

  }

  ngAfterViewInit() {
    this.query();
  }

  // 打开弹出框 根据传入标签进行处理
  openWindow(flag: string) {
    // 新增
    if (flag === 'add') {
      this.showAddWindow = true;
    }
    // 修改
    else if (flag === 'modi') {
      this.showModiWindow = true;
    }
    // 主管领导设置
    else {

    }
  }

  // 关闭弹出框 根据传入标签进行处理
  closeWindow(flag: string) {
    // 新增
    if (flag === 'add') {
      this.showAddWindow = false;
    }
    // 修改
    else if (flag === 'modi') {
      this.showModiWindow = false;
    }
    else {

    }
  }

  // 新增保存事件
  addSave() {
    // 必输项校验
    if (!this.addWindow.valid) {
      this.msgSrv.error('必填项不能为空！');
      return;
    }
    // 调用新增服务
    this.lbs.lbservice('jk06_add', { para: this.addWindow.value }).then(resdata => {
      if (resdata.code < 1) {
        this.msgSrv.error(resdata.errmsg);
      }
      else {
        // 关闭新增框
        this.closeWindow('add');
        // 新增框内容情况
        this.addFormData = {};
        this.msgSrv.success('信息填报成功');
        // 成功后，重新加载列表
        this.query();
      }

    })

  }

  // 事项修改保存事件
  modiSave() {
    // 必输项校验
    if (!this.modiWindow.valid) {
      this.msgSrv.error('必填项不能为空！');
      return;
    }

    // 调用事项修改服
    this.lbs.lbservice('jk06_update', { para: this.modiWindow.value }).then(resdata => {
      if (resdata.code < 1) {
        this.msgSrv.error(resdata.errmsg);
      }
      else {
        // 关闭修改框
        this.closeWindow('modi');
        // 将修改框清空
        this.modiFormData = {};
        this.msgSrv.success('信息修改成功');
        // 成功后，重新加载列表
        this.query();
      }
    })
  }

  // 事项信息查询
  query(pi1 = false) {

    if (pi1) {
      this.st.pi = 1;
    }
    this.st.reload(this.searchForm.value);
  }

}

