import { Component, OnInit, ViewChild } from '@angular/core';
import { _HttpClient } from '@delon/theme';
import { STColumn, STColumnTag } from '@delon/abc';
import { SFSchema, SFComponent } from '@delon/form';
import { SupDic, HttpService, GridComponent } from 'lbf';
import { NzMessageService } from 'ng-zorro-antd';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-bjxx',
  templateUrl: './bjxx.component.html',
  styleUrls: ['./bjxx.component.less']
})
export class BjxxComponent implements OnInit {
  queryFormData = {}; // 查询区表单数据
  modiFormData = {}; // 修改弹出框表单数据
  gridData = [];
  showAddWindow = false; // 是否显示新增弹出框
  showModiWindow = false; // 是否显示修改弹出框
  apd: any = {};

  // 事项列表
  @ViewChild('st', { static: true }) st: GridComponent;
  // 查询区表单
  @ViewChild('searchForm', { static: false }) searchForm: SFComponent;
  // 事项修改表单
  @ViewChild('modiWindow', { static: false }) modiWindow: SFComponent;

  // 查询区
  searchSchema: SFSchema = {
    properties: {
      KSSJ: {
        type: 'string',
        title: '收件开始时间',
        ui: { widget: 'date' }
      },
      JSSJ: {
        type: 'string',
        title: '收件结束时间',
        ui: { widget: 'date' }
      },
    },
    ui: {
      spanLabelFixed: 100,
      grid: {
        span: 6
      }
    },
  };

  // 修改表单
  updateSchema = {
    properties: {
      SLRQ: {
        title: '受理日期',
        type: 'string',
        readOnly: true,
      },
      SL: {
        title: '数量',
        type: 'string',
        readOnly: true,
      },
      BJZT: {
        title: '修改办结状态',
        type: 'string',
        enum: [{ label: '修改', value: '1' }, { label: '恢复', value: '0' }],
        ui: {
          widget: 'radio',
          grid: {
            span: 24
          }
        },
        default: '1',
      },
    },
    ui: {
      spanLabelFixed: 100,
      grid: {
        span: 12
      }
    },
    required: ['BJZT']
  };

  // 事项列表生成串
  columns: STColumn[] = [
    { title: '收件日期', index: 'SLRQ' },
    { title: '数量', index: 'SL' },
    {
      title: '操作区', buttons: [
        {
          text: '修改',
          icon: 'edit',
          click: (record: any) => {
            // 给修改编辑框赋值
            this.modiWindow.setValue('/SLRQ', (record.SLRQ));
            this.modiWindow.setValue('/SL', (record.SL));
            // 打开修改编辑框
            this.openWindow('modi');
          }
        },
      ]
    },
  ];


  constructor(private http: HttpClient,
    private dic: SupDic,
    private lbs: HttpService,
    public msgSrv: NzMessageService,
  ) { }

  ngOnInit() {

  }

  ngAfterViewInit() {


  }

  // 打开弹出框 根据传入标签进行处理
  openWindow(flag: string) {
    // 新增
    if (flag === 'add') {
      this.showAddWindow = true;
    }
    // 修改
    else if (flag === 'modi') {
      this.showModiWindow = true;
    }
    // 主管领导设置
    else {

    }
  }

  // 关闭弹出框 根据传入标签进行处理
  closeWindow(flag: string) {
    // 新增
    if (flag === 'add') {
      this.showAddWindow = false;
    }
    // 修改
    else if (flag === 'modi') {
      this.showModiWindow = false;
    }
    else {

    }
  }

  // 新增保存事件
  modiSave() {
    // 必输项校验
    if (!this.modiWindow.valid) {
      this.msgSrv.error('必填项不能为空！');
      return;
    }
    // 调用新增服务
    this.lbs.lbservice('BJL_UPDATE', { para: this.modiWindow.value }).then(resdata => {
      if (resdata.code < 1) {
        this.msgSrv.error(resdata.errmsg);
      }
      else {
        // 关闭修改框
        this.closeWindow('modi');
        // 将修改框清空
        this.modiFormData = {};
        this.msgSrv.success('信息修改成功');
        // 成功后，重新加载列表
        this.query();
      }

    })

  }

  // 事项信息查询
  query(pi1 = false) {

    if (pi1) {
      this.st.pi = 1;
    }
    this.st.reload(this.searchForm.value);
  }

}

