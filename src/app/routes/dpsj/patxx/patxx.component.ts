import { Component, OnInit, ViewChild } from '@angular/core';
import { _HttpClient } from '@delon/theme';
import { STColumn, STColumnTag } from '@delon/abc';
import { SFSchema, SFComponent } from '@delon/form';
import { SupDic, HttpService, GridComponent } from 'lbf';
import { NzMessageService, NzTreeComponent } from 'ng-zorro-antd';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-patxx',
  templateUrl: './patxx.component.html',
  styleUrls: ['./patxx.component.less']
})
export class PatxxComponent implements OnInit {
  addFormData = {}; // 新增弹出框表单数据
  modiFormData = {}; // 修改弹出框表单数据
  queryFormData = {}; // 查询区表单数据
  showAddWindow = false; // 是否显示新增弹出框
  showModiWindow = false; // 是否显示修改弹出框
  apd: any = {};

  // 事项列表
  @ViewChild('st', { static: true }) st: GridComponent;
  // 事项新增表单
  @ViewChild('addWindow', { static: false }) addWindow: SFComponent;
  // 事项修改表单
  @ViewChild('modiWindow', { static: false }) modiWindow: SFComponent;
  // 人员树
  @ViewChild('userTree', { static: false }) userTree: NzTreeComponent;

  // 更改表单
  updateSchema = {
    properties: {
      SEQ: {
        type: 'number',
        title: '序号',
        ui:
        {
          hidden: true
        }
      },
      JPA030: {
        type: 'number',
        title: '平安指数'
      },
      JPA031: {
        type: 'number',
        title: '全省排名'
      },
      JPA032: {
        type: 'number',
        title: '社会政治安全'
      },
      JPA033: {
        type: 'number',
        title: '突发公共安全'
      },
      JPA034: {
        type: 'number',
        title: '环境生态安全'
      },
      JPA002: {
        type: 'number',
        title: '出租房屋数'
      },
      JPA003: {
        type: 'number',
        title: '流动人口数'
      },
      JPA035: {
        type: 'number',
        title: '网格数'
      },
      JPA036: {
        type: 'number',
        title: '网格员数'
      },
    },
    ui: {
      spanLabelFixed: 100,
      grid: {
        span: 12
      }
    },
    required: ['JPA030', 'JPA031', 'JPA002', 'JPA003', 'JPA035', 'JPA036']
  };

  gridData = [];

  // 事项列表生成串
  columns: STColumn[] = [
    { title: '平安指数', index: 'JPA030' },
    { title: '全省排名', index: 'JPA031' },
    { title: '社会政治安全', index: 'JPA032' },
    { title: '突发公共安全', index: 'JPA033' },
    { title: '环境生态安全', index: 'JPA034' },
    { title: '出租房屋数', index: 'JPA002' },
    { title: '流动人口数', index: 'JPA003' },
    { title: '网格数', index: 'JPA035' },
    { title: '网格员数', index: 'JPA036' },
    { title: '操作人员', index: 'AAE011', dic: 'USERID' },
    { title: '操作时间', index: 'AAE036', type: 'date' },
    {
      title: '操作区', buttons: [
        {
          text: '修改',
          icon: 'edit',
          click: (record: any) => {
            // 给修改编辑框赋值
            this.modiWindow.setValue('/JPA030', (record.JPA030));
            this.modiWindow.setValue('/JPA031', (record.JPA031));
            this.modiWindow.setValue('/JPA032', (record.JPA032));
            this.modiWindow.setValue('/JPA033', (record.JPA033));
            this.modiWindow.setValue('/JPA034', (record.JPA034));
            this.modiWindow.setValue('/JPA002', (record.JPA002));
            this.modiWindow.setValue('/JPA003', (record.JPA003));
            this.modiWindow.setValue('/JPA035', (record.JPA035));
            this.modiWindow.setValue('/JPA036', (record.JPA036));
            // 打开修改编辑框
            this.openWindow('modi');
          }
        },
      ]
    },
  ];

  constructor(private http: HttpClient,
    private dic: SupDic,
    private lbs: HttpService,
    public msgSrv: NzMessageService,
  ) { }

  ngOnInit() {

  }

  ngAfterViewInit() {
    this.query();
  }

  // 打开弹出框 根据传入标签进行处理
  openWindow(flag: string) {
    // 新增
    if (flag === 'add') {
      this.showAddWindow = true;
    }
    // 修改
    else if (flag === 'modi') {
      this.showModiWindow = true;
    }
    // 主管领导设置
    else {

    }
  }

  // 关闭弹出框 根据传入标签进行处理
  closeWindow(flag: string) {
    // 新增
    if (flag === 'add') {
      this.showAddWindow = false;
    }
    // 修改
    else if (flag === 'modi') {
      this.showModiWindow = false;
    }
    else {

    }
  }

  // 事项修改保存事件
  modiSave() {
    // 必输项校验
    if (!this.modiWindow.valid) {
      this.msgSrv.error('必填项不能为空！');
      return;
    }

    // 调用事项修改服
    this.lbs.lbservice('pat_update', { para: this.modiWindow.value }).then(resdata => {
      if (resdata.code < 1) {
        this.msgSrv.error(resdata.errmsg);
      }
      else {
        // 关闭修改框
        this.closeWindow('modi');
        // 将修改框清空
        this.modiFormData = {};
        this.msgSrv.success('信息修改成功');
        // 成功后，重新加载列表
        this.query();
      }
    })
  }

  // 事项信息查询
  query(pi1 = false) {

    if (pi1) {
      this.st.pi = 1;
    }
    this.st.reload();
  }

}


