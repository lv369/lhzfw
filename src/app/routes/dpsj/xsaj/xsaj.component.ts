import { Component, OnInit, ViewChild } from '@angular/core';
import { _HttpClient } from '@delon/theme';
import { STColumn, STColumnTag } from '@delon/abc';
import { SFSchema, SFComponent } from '@delon/form';
import { SupDic, HttpService, GridComponent } from 'lbf';
import { NzMessageService, NzTreeComponent } from 'ng-zorro-antd';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-xsaj',
  templateUrl: './xsaj.component.html',
  styleUrls: ['./xsaj.component.less']
})
export class XsajComponent implements OnInit {
  addFormData = {}; // 新增弹出框表单数据
  modiFormData = {}; // 修改弹出框表单数据
  queryFormData = {}; // 查询区表单数据
  showAddWindow = false; // 是否显示新增弹出框
  showModiWindow = false; // 是否显示修改弹出框
  apd: any = {};

  // 事项列表
  @ViewChild('st', { static: true }) st: GridComponent;
  // 事项新增表单
  @ViewChild('addWindow', { static: false }) addWindow: SFComponent;
  // 事项修改表单
  @ViewChild('modiWindow', { static: false }) modiWindow: SFComponent;
  // 查询区表单
  @ViewChild('searchForm', { static: false }) searchForm: SFComponent;
  // 人员树
  @ViewChild('userTree', { static: false }) userTree: NzTreeComponent;

  // 查询区
  searchSchema: SFSchema = {
    properties: {
      AAE002: {
        type: 'string',
        title: '所属月份',
      },
      JGA030: {
        type: 'string',
        title: '刑事案件类型',
        enum: this.dic.getSFDic('JGA030'),
        ui: {
          widget: 'select',
          allowClear: true,
        }
      },
    },
    ui: {
      spanLabelFixed: 100,
      grid: {
        span: 6
      }
    }
  };

  // 新增表单
  addSchema = {
    properties: {
      AAE002: {
        type: 'string',
        title: '月份',
        ui:
        {
          hidden: true
        }
      },
      zl: {
        type: 'number',
        title: '总量'
      },
      ma: {
        type: 'number',
        title: '命案'
      },
      zs: {
        type: 'number',
        title: '重伤'
      },
      sex: {
        type: 'number',
        title: '强奸'
      },
      qj: {
        type: 'number',
        title: '抢劫'
      },
      qd: {
        type: 'number',
        title: '抢夺'
      },
      sd: {
        type: 'number',
        title: '涉毒'
      },
      dq: {
        type: 'number',
        title: '盗窃'
      },
    },
    ui: {
      spanLabelFixed: 90,
      grid: {
        span: 12
      }
    },
    required: ['zl', 'ma', 'zs', 'sex', 'qj', 'qd', 'sd', 'dq']
  };

  // 修改表单
  updateSchema = {
    properties: {
      AAE002: {
        type: 'string',
        title: '月份',
        ui:
        {
          hidden: true
        }
      },
      JGA030: {
        type: 'string',
        title: '刑事案件类型',
        enum: this.dic.getSFDic('JGA030'),
        readOnly: true,
        ui: {
          widget: 'select',
          grid: {
            span: 24
          }
        }
      },
      JGA031: {
        type: 'number',
        title: '刑事案件数量'
      },
    },
    ui: {
      spanLabelFixed: 90,
      grid: {
        span: 12
      }
    },
    required: ['JGA031']
  };


  gridData = [];

  // 事项列表生成串
  columns: STColumn[] = [
    { title: '所属月份', index: 'AAE002' },
    { title: '刑事案件类型', index: 'JGA030', dic: 'JGA030' },
    { title: '刑事案件数量', index: 'JGA031' },
    { title: '操作人员', index: 'AAE011', dic: 'USERID' },
    { title: '操作时间', index: 'AAE036', type: 'date' },
    {
      title: '操作区', buttons: [
        {
          text: '修改',
          icon: 'edit',
          click: (record: any) => {
            // 给修改编辑框赋值
            this.modiWindow.setValue('/AAE002', (record.AAE002));
            this.modiWindow.setValue('/JGA030', (record.JGA030));
            this.modiWindow.setValue('/JGA031', (record.JGA031));
            // 打开修改编辑框
            this.openWindow('modi');
          }
        },
      ]
    },
  ];

  constructor(private http: HttpClient,
    private dic: SupDic,
    private lbs: HttpService,
    public msgSrv: NzMessageService,
  ) { }

  ngOnInit() {

  }

  ngAfterViewInit() {
    this.query();
  }

  // 打开弹出框 根据传入标签进行处理
  openWindow(flag: string) {
    // 新增
    if (flag === 'add') {
      this.showAddWindow = true;
    }
    // 修改
    else if (flag === 'modi') {
      this.showModiWindow = true;
    }
    // 主管领导设置
    else {

    }
  }

  // 关闭弹出框 根据传入标签进行处理
  closeWindow(flag: string) {
    // 新增
    if (flag === 'add') {
      this.showAddWindow = false;
    }
    // 修改
    else if (flag === 'modi') {
      this.showModiWindow = false;
    }
    else {

    }
  }

  // 新增保存事件
  addSave() {
    // 必输项校验
    if (!this.addWindow.valid) {
      this.msgSrv.error('必填项不能为空！');
      return;
    }
    // 调用新增服务
    this.lbs.lbservice('jk07_add', { para: this.addWindow.value }).then(resdata => {
      if (resdata.code < 1) {
        this.msgSrv.error(resdata.errmsg);
      }
      else {
        // 关闭新增框
        this.closeWindow('add');
        // 新增框内容情况
        this.addFormData = {};
        this.msgSrv.success('信息填报成功');
        // 成功后，重新加载列表
        this.query();
      }

    })

  }

  // 事项修改保存事件
  modiSave() {
    // 必输项校验
    if (!this.modiWindow.valid) {
      this.msgSrv.error('必填项不能为空！');
      return;
    }

    // 调用事项修改服
    this.lbs.lbservice('jk07_update', { para: this.modiWindow.value }).then(resdata => {
      if (resdata.code < 1) {
        this.msgSrv.error(resdata.errmsg);
      }
      else {
        // 关闭修改框
        this.closeWindow('modi');
        // 将修改框清空
        this.modiFormData = {};
        this.msgSrv.success('信息修改成功');
        // 成功后，重新加载列表
        this.query();
      }
    })
  }

  // 事项信息查询
  query(pi1 = false) {

    if (pi1) {
      this.st.pi = 1;
    }
    this.st.reload(this.searchForm.value);
  }

}

