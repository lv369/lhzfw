import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { GaxxComponent } from '../dpsj/gaxx/gaxx.component';
import { JjxxComponent } from '../dpsj/jjxx/jjxx.component';
import { PatxxComponent } from '../dpsj/patxx/patxx.component';
import { XsajComponent } from '../dpsj/xsaj/xsaj.component';
import { BjxxComponent } from '../dpsj/bjxx/bjxx.component';


const routes: Routes = [
  { path: 'gaxx', component: GaxxComponent },
  { path: 'jjxx', component: JjxxComponent },
  { path: 'patxx', component: PatxxComponent },
  { path: 'xsaj', component: XsajComponent },
  { path: 'bjxx', component: BjxxComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DpsjRoutingModule { }
