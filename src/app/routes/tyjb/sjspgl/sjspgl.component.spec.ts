import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { TyjbSjspglComponent } from './sjspgl.component';

describe('TyjbSjspglComponent', () => {
  let component: TyjbSjspglComponent;
  let fixture: ComponentFixture<TyjbSjspglComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TyjbSjspglComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TyjbSjspglComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
