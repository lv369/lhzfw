import { Component, OnInit, ViewChild, Input, Output, EventEmitter } from '@angular/core';
import { HttpService, SupDic, GridComponent } from 'lbf';
import { SFComponent, SFSchema } from '@delon/form';
import { STColumn, STColumnTag, STColumnBadge } from '@delon/abc';
import { NzMessageService } from 'ng-zorro-antd';
import { TyekouSjspComponent } from '@shared/components/tyjb/tyekou_sjsp';

@Component({
  selector: 'app-tyjb-sjspgl',
  templateUrl: './sjspgl.component.html',
  styleUrls: ['./sjspgl.less'],
})
export class TyjbSjspglComponent implements OnInit {
  addSchema: SFSchema
  querySchema: SFSchema = {
    properties: {
     
      ZTBH: { type: 'string', title: '主体编号' },
      ZTMC: { type: 'string', title: '主体名称' },
      KSSJ: { 
        type: 'string', 
        title: '开始时间',
        ui: { widget: 'date' } 
      },
      JSSJ: { 
        type: 'string', 
        title: '结束时间',
        ui: { widget: 'date' } 
      },
      AYW009: {
        type: 'number',
        title:'业务流水号'
      }
    },
    ui:{
      grid:{
        span:6
      }
    }
  }
  

  // 主体对象类别渲染标签 AYW004
  TAG: STColumnTag = {
    '1': { text: '确认申请主体', color: 'magenta' },
    '2': { text: '申请表单完成', color: 'blue' },
    '4': { text: '待受理', color: 'cyan' },
    '5': { text: '作废', color: 'red' },
    '6': { text: '受理表单完成', color: 'lime' },
    '7': { text: '待调解', color: 'orange' },
    '8': { text: '不予受理', color: 'red' },
    '9': { text: '调解成功', color: 'green' },
    '10': { text: '调解失败', color: 'red' },
    '11': { text: '事项已转交', color: 'purple' }
  };

  // 信息来源
  xxTAG: STColumnTag = {
    '1': { text: '事项退回', color: 'green' },
    '2': { text: '其他平台', color: 'magenta' },
    '3': { text: '乡镇街道', color: 'blue' }
  };
/*
  1	1	确认申请主体
10	3	调解失败
11	3	事项已转交
2	1	表单已确认
4	1	申请成功
5	1	作废办结
6	2	表单已确认
7	2	受理通过
8	2	不予受理
9	3	调解成功
*/

  BADGE: STColumnBadge = {
    '1': { text: '单位', color: 'success' },
    '2': { text: '个人', color: 'processing' },
  };

  curParamId = -1;
  // 列表
  grColumns: STColumn[] = [    
    { title: '流水号', index: 'AYW009',width:70 },
    { title: '信息来源', index: 'SXLY',width:100,type: 'tag', tag: this.xxTAG},
    { title: '指派部门', index: 'BMMC',width:100},
    { title: '纠纷事项', index: 'AYW001',dic:'AYW001',width:100},
    { title: '当前状态', index: 'AYW034',type: 'tag', tag: this.TAG,width:100},
    { title: '最近操作时间', index: 'AAE035',width:100},   
    { title: '对象主体', index: 'ZTLX', type: 'badge', badge: this.BADGE,width:100},    
    { title: '主体编号', index: 'ZTBH',width:100 },
    { title: '主体名称', index: 'ZTMC',width:100 },    
    { title: '乡镇街道', index: 'CAA001',dic:'CAA001',width:100 },
    { title: '村社区', index: 'AAE025',dic:'AAE025',width:100 },  
    { title: '详细地址', index: 'ADDR',width:100 },  
    { title: '手机号码', index: 'PHONE',width:100 },      
    {
      title: '操作区',
      fixed: 'right',
      width: 80,
      buttons: [
        {
          text: record=>{
            if(record.SHZT==='1'){
              return '已处理'
            }
            else{
              return '事项审核'
            }
            
          },
          click: (record: any) => {
            this.queryInfo.AYW009 = record.AYW009;
          },
          iif: record => record.SHZT === '0',
          iifBehavior: 'disabled',
        },
      ],
    },
  ];
  // 详情弹出框
  @ViewChild('queryInfo', { static: false }) queryInfo: TyekouSjspComponent;
  
  
 
  @Output() clickItem = new EventEmitter<string>();


  // 列表
  @ViewChild('grGrid', { static: false }) grGrid: GridComponent;
  // 查询表单
  @ViewChild('queryForm', { static: false }) queryForm: SFComponent;
  
  
  // 人员列表查询
  doQuery(pi1=false) {
    if(pi1){
      this.grGrid.pi=1;
    }
    this.grGrid.reload(this.queryForm.value);
  }

    


 
  
  constructor(
    private lbservice: HttpService,
    private supDic: SupDic,
    public msgSrv: NzMessageService,
  ) {
    
  }

  ngOnInit() { 
    console.log('-------------ngOnInit111----------')
    this.todoInit();    
  }

  _onReuseInit(){
    console.log('-------------_onReuseInit----------')
    this.todoInit();
  }

  todoInit(){
    // 去获取是否有传递参数
    this.lbservice.lbservice('PUBLIC_INIT',{}).then(resdata=>{
      if(resdata.code<1)
      {
        this.msgSrv.error(resdata.errmsg);
      }
      else
      {
        console.log('-------------init----------')
        console.log(resdata)
        // 判断是否有初始化数据
        if(resdata.message.AYW009!==undefined){
          this.curParamId = resdata.message.AYW039;
          this.queryForm.setValue('/AYW009',resdata.message.AYW009)
          this.doQuery();  
        }
        // 如果没有参数传递，默认打开首页
        else{
          if(this.curParamId<0){
            this.doQuery();    
          }
                 
        }
      }
      
   })
  }


}