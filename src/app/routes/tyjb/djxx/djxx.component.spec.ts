import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { TyjbDjxxComponent } from './djxx.component';

describe('TyjbDjxxComponent', () => {
  let component: TyjbDjxxComponent;
  let fixture: ComponentFixture<TyjbDjxxComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TyjbDjxxComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TyjbDjxxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
