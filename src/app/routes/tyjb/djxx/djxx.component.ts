import {
  Component,
  Input,
  Output,
  OnInit,
  ViewChild,
  ChangeDetectionStrategy,
  EventEmitter,
  OnChanges,
} from '@angular/core';
import { _HttpClient, ModalHelper } from '@delon/theme';
import { STColumn, STComponent } from '@delon/abc/table';
import { SFSchema } from '@delon/form';
import { TransferChange, TransferItem } from 'ng-zorro-antd/transfer';
import { NzTreeComponent } from 'ng-zorro-antd/tree';
import { NzTreeNode, NzFormatEmitEvent, NzMessageService } from 'ng-zorro-antd';
import { GridComponent, PopdetailComponent, HttpService, SupDic } from 'lbf';
import { ReuseTabService } from '@delon/abc/reuse-tab';
import { NzModalService } from 'ng-zorro-antd/modal';
import { LbRepComponent } from '@shared/components/lb-rep/lb-rep.component';

@Component({
  selector: 'app-tyjb-djxx',
  templateUrl: './djxx.component.html',
})
export class TyjbDjxxComponent implements OnInit,OnChanges {
  @Input() dad069: number;
  @Input() dad017: number;
  @Input() dad075: number;

  treeData: any;
  value: string;
  inputValue: string;
  array = []
  pararray = []
  nodes  = []

  

  constructor(private http: _HttpClient, private modal: ModalHelper,
    private lbservice: HttpService,
    private reuseTabService: ReuseTabService,
    private modalService: NzModalService,
    public msgSrv: NzMessageService,) {

  }

  ngOnInit() {
    
  }
  ngOnChanges(change){
    
    if (change.dad069.currentValue!==null){
      this.query();
    }
    
  }

  add() {
    // this.modal
    //   .createStatic(FormEditComponent, { i: { id: 0 } })
    //   .subscribe(() => this.st.reload());
  }

  query(){
    
    this.lbservice.lbservice("ZFYY_QUERY",{para:{DAD069:this.dad069}}).then(resdata=>{
      
      if (resdata.code === 0) {
        
      } else {
        this.nodes = resdata.message.list;
      }
    })
  }


  openFolder(data: NzTreeNode | NzFormatEmitEvent): void {
    // do something if u want
    let origin = {}
    if (data instanceof NzTreeNode) {
      data.isExpanded = !data.isExpanded;
      origin = data.origin;
    } else {
      const node = data.node;
     origin = data.node.origin;
      if (node) {
        node.isExpanded = !node.isExpanded;
      }
    }

    
    const tnodes = this.nodes;
    if (origin.hasOwnProperty('children')) {
      // 获取选中的父节点
      if(origin['checked']){
        this.pararray.push(origin['title'])
      }
      if(!origin['checked']){
      this.pararray =  this.pararray.filter(value => value !== origin['title'])
      }
    } else {
      // 将获取选中的子节点存放到input里面
      this.inputValue = '';
      console.log(origin)
      
      if(origin['checked']){
        // 选中的放入数组中
        this.array.push(origin['title'])
      }
      if(!origin['checked']){
        // 没选中的从数组中剔除
      this.array =  this.array.filter(value => value !== origin['title'])
      }
      if (data instanceof NzTreeNode) {

      }else{
        this.array.map((v, i) => {
        this.inputValue += i + 1 + '. ' + v + '\r\n';
      });
      }
     
    }

  }

  okClick() {
    
    this.lbservice.lbservice("ZFYY_ADD",{para:{DAD069:this.dad069,ARRA:this.pararray,
      INPUT:this.inputValue,DAD075:this.dad075,DAD017:this.dad017}}).then(resdata=>{
      
      if (resdata.code === 0) {
        
      } else {
        this.nodes = resdata.message.list;
        // this.initSxDataSbcl();
      }
    })
    // this.reuseTabService.close('/tyjb/djxx');

    const modal = this.modalService.create({
      nzTitle: '打印',
      nzContent: LbRepComponent,
      nzComponentParams: {
        repid: '23',
        inpara: { dad069: this.dad069 },
      },
      nzWidth: 880,
    });

  }
  cancleClick() {
    this.reuseTabService.close('/tyjb/djxx')
  }


}
