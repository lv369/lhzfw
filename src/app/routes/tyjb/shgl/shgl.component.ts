import { Component, OnInit, ViewChild, Input, Output, EventEmitter } from '@angular/core';
import { HttpService, SupDic, GridComponent } from 'lbf';
import { SFComponent, SFSchema } from '@delon/form';
import { STColumn, STColumnTag, STColumnBadge } from '@delon/abc';
import { NzMessageService, NzTreeNode, NzFormatEmitEvent } from 'ng-zorro-antd';
import { TyekouQueryComponent } from '@shared/components/tyjb/tyekou_query';



@Component({
  selector: 'app-tyjb-shgl',
  templateUrl: './shgl.component.html',
  styleUrls: ['./shgl.component.less'],
})
export class TyjbShglComponent implements OnInit {
  isCallBack = false; // 事项召回窗口是否显示
  addSchema: SFSchema
  dataAYW009 = 0;
  querySchema: SFSchema = {
    properties: {

      ZTBH: { type: 'string', title: '主体编号' },
      ZTMC: { type: 'string', title: '主体名称' },
      KSSJ: {
        type: 'string',
        title: '开始时间',
        ui: { widget: 'date' }
      },
      JSSJ: {
        type: 'string',
        title: '结束时间',
        ui: { widget: 'date' }
      },
      AYW034: {
        type: 'string',
        title: '当前状态',
        enum: this.supDic.getSFDic('AYW034'),
        ui: {
          widget: 'select',
          allowClear: true,
        },
      },
      AYW009: {
        type: 'number',
        title: '业务流水号',
        ui: { widgetWidth: 200 },
      }
    },
    ui: {
      spanLabelFixed: 100,
      grid: {
        span: 6
      }
    }
  }

  CallBackSchema: SFSchema = {
    properties: {
      BMID: {
        type: 'string',
        title: '分中心或部门',
        enum: [],
        ui: {
          widget: 'tree-select',
          dropdownStyle: {
            height: '200px'
          },
          grid: {
            span: 24
          },
          expandChange: (e: NzFormatEmitEvent) => this.getSdTree(e)
        }
      },

    },
    ui: {
      spanLabelFixed: 100,
      grid: {
        span: 24,
      },
    },
    required: ['BMID']
  }; // 召回编辑框

  // CallBackSchema: SFSchema; // 召回编辑框
  // 主体对象类别渲染标签 AYW004
  TAG: STColumnTag = {
    '1': { text: '确认申请主体', color: 'magenta' },
    '2': { text: '申请表单完成', color: 'blue' },
    '4': { text: '待受理', color: 'cyan' },
    '5': { text: '作废', color: 'red' },
    '6': { text: '受理表单完成', color: 'lime' },
    '7': { text: '待办理', color: 'orange' },
    '8': { text: '不予受理', color: 'red' },
    '9': { text: '办理成功', color: 'green' },
    '10': { text: '办理失败', color: 'red' },
    '11': { text: '事项已转交', color: 'purple' }
  };
  /*
    1	1	确认申请主体
  10	3	调解失败
  11	3	事项已转交
  2	1	表单已确认
  4	1	申请成功
  5	1	作废办结
  6	2	表单已确认
  7	2	受理通过
  8	2	不予受理
  9	3	调解成功
  */

  BADGE: STColumnBadge = {
    '1': { text: '单位', color: 'success' },
    '2': { text: '个人', color: 'processing' },
  };

  curParamId = -1;
  // 列表
  grColumns: STColumn[] = [
    { title: '流水号', index: 'AYW009', width: 70 },
    { title: '指派中心', index: 'ZXMC', width: 100 },
    { title: '事项大类', index: 'AYW120', dic: 'AYW120', width: 100 },
    { title: '指派部门', index: 'BMMC', width: 100 },
    { title: '纠纷事项', index: 'AYW001', dic: 'AYW001', width: 100 },
    { title: '当前状态', index: 'AYW034', type: 'tag', tag: this.TAG, width: 100 },
    { title: '最近操作时间', index: 'AAE035', width: 100 },
    { title: '对象主体', index: 'ZTLX', type: 'badge', badge: this.BADGE, width: 100 },
    { title: '主体编号', index: 'ZTBH', width: 100 },
    { title: '主体名称', index: 'ZTMC', width: 100 },
    /*  
    { title: '乡镇街道', index: 'CAA001',dic:'CAA001',width:100 },
    { title: '村社区', index: 'AAE025',dic:'AAE025',width:100 },  
    { title: '详细地址', index: 'ADDR',width:100 },  */
    { title: '手机号码', index: 'PHONE', width: 100 },
    {
      title: '操作区',
      fixed: 'right',
      width: 80,
      buttons: [
        {
          text: '查看详情',
          click: (record: any) => {
            this.queryInfo.AYW009 = record.AYW009;
          },
        },
        {
          text: '事项召回',
          click: (record: any) => {
            this.OpenWindow(record);
          },
          // 权限控制，
          iif: record => record.SFBJ === 0 && record.ZXMC !== '未指派' && record.ROLETYPE !== '4' && record.ROLETYPE !== '3',
        },
      ],
    },
  ];
  // 详情弹出框
  @ViewChild('queryInfo', { static: false }) queryInfo: TyekouQueryComponent;

  // 业务表单
  @ViewChild('sfCallBack', { static: false }) sfCallBack: SFComponent;

  @Output() clickItem = new EventEmitter<string>();


  // 列表
  @ViewChild('grGrid', { static: false }) grGrid: GridComponent;
  // 查询表单
  @ViewChild('queryForm', { static: false }) queryForm: SFComponent;


  // 人员列表查询
  doQuery(pi1 = false) {
    if (pi1) {
      this.grGrid.pi = 1;
    }
    this.grGrid.reload(this.queryForm.value);
  }
  dataBM: any; //中心部门
  // 打开事项召回窗口
  OpenWindow(record: any) {
    console.log('-------open-----------')
    console.log(record.BMLIST)
    this.dataBM = JSON.parse(record.BMLIST);
    this.dataAYW009 = record.AYW009;
    this.CallBackSchema = {
      properties: {
        BMID: {
          type: 'string',
          title: '分中心或部门',
          enum: this.dataBM.message.list,
          ui: {
            widget: 'tree-select',
            dropdownStyle: {
              height: '200px'
            },
            grid: {
              span: 24
            },
            expandChange: (e: NzFormatEmitEvent) => this.getSdTree(e)
          }
        },

      },
      ui: {
        spanLabelFixed: 100,
        grid: {
          span: 24,
        },
      },
      required: ['BMID']
    }

    this.isCallBack = true;
  }

  node: NzTreeNode;
  getSdTree(e: NzFormatEmitEvent) {
    this.node = e.node;
    this.lbservice.lbservice('BM_TREESELECT', { key: e.node.origin.key }).then(resdata => {
      if (resdata.code < 1) {
        this.msgSrv.error(resdata.errmsg);
      }
      else {
        if (this.node.getChildren().length === 0 && this.node.isExpanded) {
          this.node.addChildren(resdata.message.list);
        }


      }
    })
  }

  // 关闭事项召回窗口
  CloseWindow() {
    this.isCallBack = false;
  }

  // 事项召回保存
  SaveCallBack() {
    if (!this.sfCallBack.valid) {
      this.msgSrv.error('重新指派的分中心或部门不能为空!');

      return;
    }
    // 调用表单保存过程
    this.lbservice.lbservice('TYJB_SXZH', { AYW009: this.dataAYW009, para: this.sfCallBack.value }).then(resdata => {
      if (resdata.code < 1) {
        this.msgSrv.error(resdata.errmsg);
      } else {

        this.msgSrv.success('事项召回操作成功！');
        this.CloseWindow();
        this.doQuery()

      }
    })

  }

  constructor(
    private lbservice: HttpService,
    private supDic: SupDic,
    public msgSrv: NzMessageService,
  ) {

  }

  ngOnInit() {
    // this.todoInit();
  }

  _onReuseInit() {
    // this.todoInit();
  }

  todoInit() {
    // 去获取是否有传递参数
    this.lbservice.lbservice('PUBLIC_INIT', {}).then(resdata => {
      if (resdata.code < 1) {
        this.msgSrv.error(resdata.errmsg);
      }
      else {
        // 判断是否有初始化数据
        if (resdata.message.AYW009 !== undefined) {
          this.curParamId = resdata.message.AYW039;
          this.queryForm.setValue('/AYW009', resdata.message.AYW009)
          this.doQuery();
        }
        // 如果没有参数传递，默认打开首页
        else {
          if (this.curParamId < 0) {
            this.doQuery();
          }

        }
      }

    })
  }


}