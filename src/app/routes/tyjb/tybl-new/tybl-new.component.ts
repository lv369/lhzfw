import { Component, OnInit, ViewChild } from '@angular/core';
import { TyekouTyblComponent } from '@shared/components/tyjb/tyekou_tybl';
import { Subscription } from 'rxjs';
import { RefService } from '@core/lb/RefService';
@Component({
  selector: 'app-tyjb-tybl-new',
  templateUrl: './tybl-new.component.html',
})
export class TyjbTyblNewComponent implements OnInit {
  
  @ViewChild('tybl',{static: false}) tybl: TyekouTyblComponent;
 
  private refSub: Subscription;
  constructor(
    private ref: RefService,
  ) {


  }
  
  ngOnInit() {     
    
  }

  ngAfterViewInit(){
    this.refSub = this.ref.getRef('shgl').subscribe(msg => {
      console.log(msg);
      this.tybl.showInitList(msg.message);
    })
  }

  ngOnDestroy(): void {
    this.refSub.unsubscribe();
  }


}
