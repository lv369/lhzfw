import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { TyjbTyblNewComponent } from './tybl-new.component';

describe('TyjbTyblNewComponent', () => {
  let component: TyjbTyblNewComponent;
  let fixture: ComponentFixture<TyjbTyblNewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TyjbTyblNewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TyjbTyblNewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
