import { NgModule } from '@angular/core';
import { SharedModule } from '@shared';
import { TyjbRoutingModule } from './tyjb-routing.module';
import { TyjbTysjComponent } from './tysj/tysj.component';
import { TyjbSxclComponent } from './sxcl/sxcl.component';
import { TyjbDjxxComponent } from './djxx/djxx.component';
import { TyjbTyslComponent } from './tysl/tysl.component';
import { TyjbSlymComponent } from './slym/slym.component';
import { TyjbShglComponent } from './shgl/shgl.component';
import { TyjbShymComponent } from './shym/shym.component';
import { TyjbBjymComponent } from './bjym/bjym.component';
import { TyjbTycjComponent } from './tycj/tycj.component';
import { TyjbCjprComponent } from './cjpr/cjpr.component';
//打印通过弹出框需要的应用

import { TyjbTysjNewComponent } from './tysj-new/tysj-new.component';
import { TyjbTyslNewComponent } from './tysl-new/tysl-new.component';
import { TyjbTyblNewComponent } from './tybl-new/tybl-new.component';
import { TyjbBmxtyyComponent } from './bmxtyy/bmxtyy.component';
import { TyjbTysjCkslComponent } from './tysj-cksl/tysj-cksl.component';
import { TyjbTysjQtComponent } from './tysj-qt/tysj-qt.component';
import { TyjbTysjXzComponent } from './tysj-xz/tysj-xz.component';
import { TyjbTysj12345Component } from './tysj-12345/tysj-12345.component';
import { TyjbSjspglComponent } from './sjspgl/sjspgl.component';
import { TyjbXtyyryzpComponent } from './xtyyryzp/xtyyryzp.component';
import { TyjbXtycxComponent } from './xtycx/xtycx.component';
import { TyjbSfqryaComponent } from './sfqrya/sfqrya.component';
import { TyjbSfqrtjComponent } from './sfqrtj/sfqrtj.component';
import { DjtComponent } from './djt/djt.component';
import { TyjbTysjXzptComponent } from './tysj-xzpt/tysj-xzpt.component';

const COMPONENTS = [
  TyjbTysjComponent,
  TyjbSxclComponent,
  TyjbDjxxComponent,
  TyjbTyslComponent,
  TyjbSlymComponent,
  TyjbShglComponent,
  TyjbShymComponent,
  TyjbBjymComponent,
  TyjbTycjComponent,
  TyjbCjprComponent,
  TyjbTysjNewComponent,
  TyjbTyslNewComponent,
  TyjbTyblNewComponent,
  TyjbBmxtyyComponent,
  TyjbTysjCkslComponent,
  TyjbTysjQtComponent,
  TyjbTysjXzComponent,
  TyjbTysj12345Component,
  TyjbSjspglComponent,
  TyjbXtyyryzpComponent,
  TyjbXtycxComponent,
  TyjbSfqryaComponent,
  TyjbSfqrtjComponent,
  DjtComponent,
  TyjbTysjXzptComponent]

const COMPONENTS_NOROUNT = [];

@NgModule({
  imports: [SharedModule, TyjbRoutingModule,],
  declarations: [...COMPONENTS, ...COMPONENTS_NOROUNT],
  entryComponents: COMPONENTS_NOROUNT,
  providers: [],
  //

})
export class TyjbModule { }

