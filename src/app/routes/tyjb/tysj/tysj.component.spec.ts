import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { TyjbTysjComponent } from './tysj.component';

describe('TyjbTysjComponent', () => {
  let component: TyjbTysjComponent;
  let fixture: ComponentFixture<TyjbTysjComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TyjbTysjComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TyjbTysjComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
