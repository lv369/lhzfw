import { Component, OnInit, ViewChild, Injector } from '@angular/core';
import { _HttpClient, ModalHelper } from '@delon/theme';
import { STColumn, STChange, STPage, STComponent } from '@delon/abc';
import { SFSchema,SFTreeSelectWidgetSchema, SFSchemaEnum, SFComponent, SFRadioWidgetSchema } from '@delon/form';
import { GridComponent, PopdetailComponent, HttpService, SupDic } from 'lbf';
import { NzMessageService } from 'ng-zorro-antd';
import { of } from 'rxjs';
import { delay } from 'rxjs/operators';
import { throwIfAlreadyLoaded } from '@core';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { Router } from '@angular/router';
import {ZskService} from '../zskservice/zsk.service'




@Component({
  selector: 'app-tyjb-tysj',
  templateUrl: './tysj.component.html',
  styleUrls: ['./tysj.component.less']
})

export class TyjbTysjComponent implements OnInit {
 
  dataset = [];
  jbxx =[];
  dad032 :any;
  grdj = "primary";
  dwdj = "default";
  biaot = "";
  isVisible = false;
  isAddVisible = false;
  isSqVisible = false;
  isSlVisible = false;
  nodes: any=[]
  params = {sname: 'JBXX_QUERY',queryparas : {}}
  @ViewChild('st', { static: false }) st: GridComponent;
  @ViewChild('sf', { static: false }) sf: SFComponent;
  @ViewChild('sf1', { static: false }) sf1: SFComponent;
  @ViewChild('xzxx', { static: false }) xzxx: SFComponent;
  
  

  searchValue = ''

  Schema: SFSchema = {
    properties: {
      
      dad031: {
        type: 'string',
        title: '事项名称'

      }
    }
  }

  

  

  xzslSchema: SFSchema = {
    properties: {
      AAC022: {type: 'string',title: '证件类型'},
      AAC002: {type: 'string',title: '社会保障号'},
      AAC003: {type: 'string',title: '姓名'},
      AAC006: {type: 'string',title: '出生日期',format: 'date'},
      AAC004: {type: 'string',title: '性别'},
      AAC005: {
        type: 'string',
        title: '民族',
        enum: this.supdic.getSFDic('AAC005'),
        ui: {
          widget: 'select',
          allowClear: true
        }
      },
      AAC009: {type: 'string',title: '户籍性质'},
      AAC010: {type: 'string',title: '户籍地址',ui: 'cascader'} ,
      EAE033: {type: 'string',title: '手机号码'}
    },ui: {
      spanLabelFixed: 100,
      grid: {
        span: 12
      }
    },
    required:["AAC022","AAC002","AAC003","AAC004","AAC006"]
  }
  
  
 

  constructor(
    private modal: ModalHelper,
    public msgSrv: NzMessageService,
    private lbservice: HttpService,
    private supdic: SupDic,
    private injector: Injector,
    private router:Router,
    private zskService:ZskService,
  ) { }

  ngOnInit() {
    

  }

  // tslint:disable-next-line: use-lifecycle-interface
  ngAfterViewInit() {
    this.gr();
  }
   
  // 查询个人事项
  gr(){
    
    this.dad032 = ''
    this.dad032 = '2'
    this.grdj = "primary";
    this.dwdj = "default";
    this.lbservice.lbservice("TYSL_SXZL",{para:{test:this.sf.value, dad032: '2' }}).then(resdata=>{
      this.dataset=resdata.message.list;
      
    });
  }
  
  // 查询单位事项
  dw(){
    
    this.dwdj = "primary";
    this.grdj = "default";
    this.dad032 = ''
    this.dad032 = '1'
    this.lbservice.lbservice("TYSL_SXZL",{para:{ test:this.sf.value,dad032: '1' }}).then(resdata=>{
      this.dataset=resdata.message.list;
    });

  }

  
  searchsx(){
    this.lbservice.lbservice("TYSL_SXZL",{para:{test:this.sf.value,dad032:this.dad032}}).then(resdata=>{
      this.dataset=resdata.message.list;
    });
  }

  
  // tslint:disable-next-line: member-ordering
  dad017;
  // tslint:disable-next-line: member-ordering
  dad031;
  sxdad011;
  // 打开人员查询页面
  tysl(dad031,dad017,dad011) {
    this.sxdad011=dad011;
    this.dad017 = dad017;
    this.dad031 = dad031;
    // console.log(dad017)
    // this.isVisible = true;
    // this.biaot = "【" + dad031 + "】人员查询";
    this.goTo();
  }
  
  

 

  
  
 

  private goTo() {
    // etTimeout(() => this.injector.get(Router).navigateByUrl(url));
    // console.log("aaaaaaa")
    this.zskService.setDAD011(this.sxdad011)
    setTimeout(() => {
      this.router.navigate(['/tyjb/sxcl'],{queryParams:{sxdad011:this.sxdad011,dad017:this.dad017,dad031:this.dad031,ympage:"1",dad032:this.dad032}});
    }, 500);
    
  }

 

}
