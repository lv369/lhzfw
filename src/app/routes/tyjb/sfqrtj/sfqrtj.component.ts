import { Component, OnInit, ViewChild } from '@angular/core';
import { _HttpClient, ModalHelper } from '@delon/theme';
import { STColumn, STComponent, STColumnTag } from '@delon/abc/table';
import { SFSchema, SFComponent } from '@delon/form';
import { GridComponent, SupDic } from 'lbf';
import { TyekouQueryComponent } from '@shared/components/tyjb/tyekou_query';

@Component({
  selector: 'app-tyjb-sfqrtj',
  templateUrl: './sfqrtj.component.html',
})
export class TyjbSfqrtjComponent implements OnInit {
  params = { sname: 'query_listsftj', form: {} };
  schema: SFSchema = {
    properties: {
       OLDLSH: {
        type: 'number',
        title: '原业务流水号',
        ui:{ 
          widgetWidth:'200'
        }
      },
      AYW009: {
        type: 'number',
        title: '新业务流水号',
        ui:{ 
          widgetWidth:'200'
        }
      },

      SJRQ: {
        type: 'string',
        title:'经办时间',
  
        ui: { widget: 'date', end: 'SLRQ',format:'YYYY/MM/DD'  },
      },
      SLRQ: {
        type: 'string',
        ui: { widget: 'date', end: 'SLRQ',format:'YYYY/MM/DD'  },
      },
     
     
      CZY: {
        type: 'string',
        title: '操作员',    
      },

      SLBM:{
        title:'所属部门',
        type:'string',
        enum: this.supDic.getSFDic('SBM'),
       
        ui: {
          widget: 'select',
          allowClear: true,
        },
     },


    },ui:{
       grid:{span:8},
       spanLabelFixed:150
    }
  };
  @ViewChild('st', { static: false }) st: GridComponent;
  @ViewChild('sf', { static: false }) sf: SFComponent;
  // 详情弹出框
  @ViewChild('queryInfo', { static: false }) queryInfo: TyekouQueryComponent;
  
  TAG: STColumnTag = {
    1: { text: '单位', color: 'blue' },
    2: { text: '个人', color: 'blue' },
  };
  TAG1: STColumnTag = {
    0: { text: '未办结', color: 'red' },
    1: { text: '已办结', color: 'green' },
   
  };

  TAG2: STColumnTag = {
    '1': { text: '确认申请主体', color: 'magenta' },
    '2': { text: '申请表单完成', color: 'blue' },
    '4': { text: '待受理', color: 'cyan' },
    '5': { text: '作废', color: 'red' },
    '6': { text: '受理表单完成', color: 'lime' },
    '7': { text: '待办理', color: 'orange' },
    '8': { text: '不予受理', color: 'red' },
    '9': { text: '办理成功', color: 'green' },
    '10': { text: '办理失败', color: 'red' },
    '11': { text: '事项已转交', color: 'purple' }
  };
  columns: STColumn[] = [
    { title: '原业务流水号', index: 'OLDLSH',className:'text-center' },
    { title: '新业务流水号',  index: 'AYW009',className:'text-center' },
    { title: '申请类型', index:'ZTLX', 
    tag:this.TAG, type:'tag',className:'text-center',
    filter: {
      menus: [{ text: '自然人', value: '2' }, { text: '单位,企业', value: '1' }],
      multiple: true,
    },
  },

  { title: '办结状态', index: 'BJZT',type:'tag',tag:this.TAG1,className:'text-center',
  filter: {
  menus: [{ text: '已办结', value: '1' }, { text: '未办结', value: '0' }],
  multiple: true,
 
  },

},
{ title: '主题名称',index: 'ZTMC',className:'text-center'},
{ title: '所属分中心', index: 'SLFZX',dic:'ZX', className:'text-center'},
{ title: '所属部门', index: 'SLBM',dic:'SBM', className:'text-center',},
{ title: '当前状态', index: 'AYW034',type:'tag', tag:this.TAG2},
    { title: '事项大类', index: 'SXDL' ,className:'text-center'},
    { title: '事项细类',  index: 'SXXL',className:'text-center' },
    { title: '经办人',  index: 'CZY' ,className:'text-center'},
    { title: '经办时间',  index: 'CZQR',className:'text-center' },
    { title: '来访人数', index: 'AYW016',className:'text-center',
     dic:'AYW016'
  
  },
    {
      title: '操作区',
      fixed: 'right',
      width: 80,
      buttons: [
        {
          text: '查看详情',
          click: (record: any) => {
            this.queryInfo.AYW009 = record.AYW009;
          },
        },
      ]}
  ];

  constructor(private http: _HttpClient, private modal: ModalHelper,private supDic: SupDic) { }

  ngOnInit() { }



  getdata() {
    this.st.reload(this.sf.value);
  
  }

}
