import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { TyjbSfqrtjComponent } from './sfqrtj.component';

describe('TyjbSfqrtjComponent', () => {
  let component: TyjbSfqrtjComponent;
  let fixture: ComponentFixture<TyjbSfqrtjComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TyjbSfqrtjComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TyjbSfqrtjComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
