import { Component, OnInit, ViewChild } from '@angular/core';
import { SFSchema, SFComponent } from '@delon/form';
import { STColumn, STColumnTag } from '@delon/abc';
import { GridComponent, SupDic, HttpService } from 'lbf';
import { NzMessageService } from 'ng-zorro-antd';
import { TyekouQueryComponent } from '@shared/components/tyjb/tyekou_query';
import { CacheService } from '@delon/cache';
import { RefService } from '@core/lb/RefService';
import { Subscription } from 'rxjs';


@Component({
  selector: 'app-tyjb-xtyyryzp',
  templateUrl: './xtyyryzp.component.html',
})
export class TyjbXtyyryzpComponent implements OnInit {
  params = { sname: 'TYJB_XTRY_QUERY', queryparas: {} };
  isRyzpVisible = false;
  dataAYW043 = 0;
  searchSchema: SFSchema = {
    properties: {
      AYW009: {
        title: '业务流水号',
        type: 'number'
      },

      KSSJ: {
        type: 'string',
        title: '邀约开始时间',
        ui: { widget: 'date' }
      },
      JSSJ: {
        type: 'string',
        title: '邀约结束时间',
        ui: { widget: 'date' }
      },

    }
  }



  columns: STColumn[] = [

    {
      title: '业务流水号',
      buttons: [
        {
          text: (record) => { return record.AYW009 },
          click: (record) => {
            this.queryInfo.AYW009 = record.AYW009;
          }
        }
      ]
    },
    { title: '发起分中心', index: 'FSZX', dic: 'ZX' },
    { title: '中心大类', index: 'AYW120', dic: 'AYW120' },
    { title: '发起部门', index: 'FSBM', dic: 'BMMC' },
    { title: '纠纷事项', index: 'AYW001', dic: 'AYW001' },
    { title: '邀约时间', index: 'SQSJ' },
    { title: '接受邀约人', index: 'AYW024', dic: 'USERID' },
    { title: '接受时间', index: 'JSSJ' },
    {
      title: '操作区',
      buttons: [

        {
          text: record => {
            if (record.AYW023 === '2') {
              return '已指派'
            }
            else {
              return '人员指派'
            }

          },
          click: (record) => {
            this.dataAYW043 = record.AYW043;
            this.openWindow();
          },
          iif: record => record.AYW023 === '1',
          iifBehavior: 'disabled',
        }
      ]
    }
  ]

  zprySchema: SFSchema = {
    properties: {
      AYW001: {
        type: 'string',
        title: '指派人员',
        enum: this.supdic.getDic('CASCADER_CZY'),
        ui: {
          widget: 'cascader',

          grid: {
            span: 24
          }
        }
      }
    },
    ui: {
      spanLabelFixed: 100,
      grid: {
        span: 24,
      },
    },
    required: ['AYW001', 'AYW059']
  }
  curParamId: number = -1;
  @ViewChild('st', { static: false }) st: GridComponent;
  @ViewChild('sf', { static: false }) sf: SFComponent;
  // 人员指派名单
  @ViewChild('zpryForm', { static: false }) zpryForm: SFComponent;
  // 详情弹出框
  @ViewChild('queryInfo', { static: false }) queryInfo: TyekouQueryComponent;
  private refSub: Subscription;
  constructor(private msgSrv: NzMessageService, private ref: RefService, private supdic: SupDic, private httpService: HttpService, public srv: CacheService) { }

  ngOnInit() {
    console.log('-------------ngOnInit111----------')
    this.todoInit();
  }

  _onReuseInit() {
    console.log('-------------_onReuseInit----------')
    this.todoInit();
  }

  ngAfterViewInit() {
    /*
    this.refSub = this.ref.getRef('shgl').subscribe(msg => {
      console.log(msg);
      this.sf.setValue('/AYW009',msg.message.AYW009)
      this.query();  
    })
    */
  }

  doSave() {
    if (!this.zpryForm.valid) {
      this.msgSrv.error('请选择指派的人员！');
      return;
    }
    this.httpService.lbservice('BMYY_RYZP', { AYW043: this.dataAYW043, para: this.zpryForm.value }).then(resdata => {
      if (resdata.code < 1) {
        this.msgSrv.error(resdata.errmsg);
      }
      else {
        this.closeWindow();
        this.query();
        this.msgSrv.success('指派成功！');
      }

    })
  }

  ngOnDestroy(): void {
    this.refSub.unsubscribe();
  }

  todoInit() {
    // 去获取是否有传递参数
    this.httpService.lbservice('PUBLIC_INIT', {}).then(resdata => {
      if (resdata.code < 1) {
        this.msgSrv.error(resdata.errmsg);
      }
      else {
        console.log('-------------init----------')
        console.log(resdata)
        // 判断是否有初始化数据
        if (resdata.message.AYW009 !== undefined) {
          this.curParamId = resdata.message.AYW039;
          this.sf.setValue('/AYW009', resdata.message.AYW009)
          this.query();
        }
        // 如果没有参数传递，默认打开首页
        else {
          if (this.curParamId < 0) {
            this.query();
          }

        }
      }

    })
  }

  closeWindow() {
    this.isRyzpVisible = false;
  }

  openWindow() {
    this.isRyzpVisible = true;
  }

  query() {
    this.st.reload(this.sf.value);
  }

  // 事项状态切换
  changeSxState(dataDGB010: string, dataAYW043: number, dataAYW023: string) {
    this.httpService.lbservice("TYJB_YY_DO", { DGB010: dataDGB010, AYW043: dataAYW043, AYW023: dataAYW023 }).then(resdata => {
      if (resdata.code < 1) {
        this.msgSrv.error(resdata.errmsg);
      }
      else {
        this.msgSrv.success('邀约处理成功');
        this.st.st.reload();
      }

    });
  }




}

