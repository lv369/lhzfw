import { Component, OnInit, ViewChild, EventEmitter } from '@angular/core';
import { _HttpClient, ModalHelper } from '@delon/theme';
import { STColumn, STComponent, STChange, STPage } from '@delon/abc/table';
import { SFSchema,SFTreeSelectWidgetSchema, SFSchemaEnum, SFComponent, SFRadioWidgetSchema } from '@delon/form';
import { GridComponent, PopdetailComponent, HttpService, SupDic } from 'lbf';
import { ActivatedRoute } from '@angular/router';
import { NzMessageService, NzModalService } from 'ng-zorro-antd';
// tslint:disable-next-line: no-duplicate-imports
import { Router } from '@angular/router';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { of, config } from 'rxjs';
import { delay } from 'rxjs/operators';
// tslint:disable-next-line: use-path-mapping
import {ReuseTabService} from '@delon/abc/reuse-tab';
import { LbWjfwComponent } from '@shared/components/lb_wjfw/lbwjfw.component';

@Component({
  selector: 'app-tyjb-shym',
  templateUrl: './shym.component.html',
})
export class TyjbShymComponent implements OnInit {
  params = {sname: 'BJXX_QUERY',queryparas : {}}
  tabs = ["办件信息", "材料影像"];
  dataset = []
  dad069=null
  dad017=null
  dae001=null
  dad028=null
  paras :any;
  Sname :FileSname = {save:'',query:'DZCL_QUERY',del:''}
  slSchema:SFSchema = {
    properties: {
      DAD095: {
        type: 'string',
        title: '',
        enum: [{ label: '通过', value: '1' }, 
        { label: '不通过', value: '0' }],
        // tslint:disable-next-line: no-object-literal-type-assertion
        ui: {
          widget: 'radio',
         // asyncData: () => of().pipe(delay(100)),
          change: console.log,
          grid: {
            span: 24
          }
        } as SFRadioWidgetSchema,
        default: '1',
      },
      DAD098: {type: 'string',title: '不通过原因',ui:{visibleIf: { DAD095: [ "0"] },spanLabelFixed: 80,grid:{span: 24}}},
    },ui:{grid:{span:24}}
  };
  bjxxSchema: SFSchema = {
    properties: {
      DAD031: {
        type: 'string',
        title: '申报事项',
        ui:{grid:{span: 24}}
      },
      DAD069: {
        type: 'string',
        title: '申报编号',
        ui:{grid:{span: 24}}
      },
      DAD072: {
        type: 'string',
        title: '申报途径',
        ui:{grid:{span: 12}}
      },
      AAE036: {
        type: 'string',
        title: '申报时间',
        format: 'datetime',
        ui:{grid:{span: 12}}
      },
      DAD075: {
        type: 'string',
        title: '申报对象名称',
        ui:{grid:{span: 12}}
      },
      DAD070: {
        type: 'string',
        title: '申报对象证件号',
        ui:{grid:{span: 12}}
      },
      DAD113: {
        type: 'string',
        title: '联系人名称',
        ui:{grid:{span: 6}}
      },
      DAD114: {
        type: 'string',
        title: '联系人证件号',
        ui:{grid:{span: 12}}
      },
      DAD115: {
        type: 'string',
        title: '联系人手机号',
        ui:{grid:{span: 6}}
      },
      DAE001: {
        type: 'string',
        title: '子项编码',
        ui:{hidden:true, grid:{span: 12}}
      },
      DAD090: {
        type: 'string',
        title: '环节编码',
        ui:{hidden:true, grid:{span: 12}}
      },
      DAD017: {
        type: 'string',
        title: '事项编码',
        ui:{hidden:true, grid:{span: 12}}
      },
    },ui:{
      spanLabelFixed: 100,
      grid:{span: 6}
    }
  };
  
  tabMode='bjxx';
  @ViewChild('st', { static: false }) st: STComponent;
  @ViewChild('sf', { static: false }) sf: SFComponent;
  @ViewChild('slxx', { static: false }) slxx: SFComponent;

  @ViewChild('wjfw', { static: false }) wjfw: LbWjfwComponent;
  
  clsmcolumns: STColumn[] = [
    { title: '序号', index: 'RN', width: '10%' },
    { title: '材料名称', index: 'DAD029', width: '50%' },
    { title: '历史材料数', index: 'CZE010', width: '15%' },
    { title: '收件未扫描', index: 'CZE012', type: 'checkbox', width: '5%' },
    { title: '当前材料数', index: 'CZE011', width: '10%' },

  ];
  page: STPage = {
    show: false,
  };

  constructor(private route:ActivatedRoute,
    private http: _HttpClient, 
    private modal: ModalHelper,
    private lbservice: HttpService,
    public msgSrv: NzMessageService,
    private router:Router,
    private supDic:SupDic,
    private reuseTabService:ReuseTabService,
    ) { 
      this.reuseTabService.title = '办理';
    }

  ngOnInit() {
    
    this.route.queryParams.subscribe(param=>{
      // tslint:disable-next-line: no-string-literal
      // tslint:disable-next-line: radix
      this.dad069= parseInt(param['dad069']);
      this.dad017= parseInt(param['dad017']);
      this.dae001= parseInt(param['dae001']);
      this.paras = {DAD069:this.dad069};
      
    });

    
   }

   
   selectTab(str:string){
    this.tabMode=str;
    if(str==='sxjd'){
      
      //this.handleClick();
    }
   }
  // tslint:disable-next-line: use-lifecycle-interface
  ngAfterViewInit(){
    
    this.bjxx_query();
    setTimeout(() => {
      this.cl_query();
    }, 500);
  }

  
  add() {
    // this.modal
    //   .createStatic(FormEditComponent, { i: { id: 0 } })
    //   .subscribe(() => this.st.reload());
  }

  bjxx_query(){
    this.lbservice.lbservice("BJXX_QUERY",{para:{DAD069:this.dad069,DAD017:this.dad017,DAE001:this.dae001}}).then(resdata=>{
      if (resdata.code < 1) {
        this.msgSrv.error(resdata.errmsg);
      } else {
      this.dataset=resdata.message.list;
      console.log(this.dataset);
      this.sf.setValue('/DAD031',this.dataset[0].DAD031);
      this.sf.setValue('/DAD069',this.dataset[0].DAD069);
      this.sf.setValue('/DAD072',this.dataset[0].DAD072);
      this.sf.setValue('/AAE036',this.dataset[0].AAE036);
      this.sf.setValue('/DAD075',this.dataset[0].DAD075);
      this.sf.setValue('/DAD070',this.dataset[0].DAD070);
      this.sf.setValue('/DAD113',this.dataset[0].DAD113);
      this.sf.setValue('/DAD114',this.dataset[0].DAD114);
      this.sf.setValue('/DAD115',this.dataset[0].DAD115);
      this.sf.setValue('/DAD090',this.dataset[0].DAD090);
      this.sf.setValue('/DAE001',this.dataset[0].DAE001);
      this.sf.setValue('/DAD017',this.dataset[0].DAD017);
      }

    });
  }

  sl_insert(){
    this.lbservice.lbservice("SL_INSERT",{para:{DAD069:this.dad069,sf:this.sf.value,slxx:this.slxx.value,DAD017:this.dad017}}).then(resdata=>{
      if (resdata.code < 1) {
        this.msgSrv.error(resdata.errmsg);
      } else {
        this.msgSrv.info('保存成功');
        this.reuseTabService.replace('/tyjb/shgl');
      }

    });
  }


  cl_query() {
    console.log("cl_query=========")
    this.lbservice
      .lbservice('CLSM_QUERY', {
        para: { DAD069: this.dad069, DAD017: this.dad017 ,FLAG:'2' },
      })
      .then(resdata => {
        if (resdata.code === 0) {
          this.msgSrv.error(resdata.errmsg);
        } else {
          this.dataset = resdata.message.list;
        }
      });
  }

  _click(e: STChange) {
    this.dad028 = e.click.item.DAD028;
    this.dae001 = e.click.item.DAE001;
    console.log(e)
    this.paras = {DAD069:this.dad069,DAD028:this.dad028,DAD017:this.dad017,DAE001:this.dae001};
    
  }

  addOne(){

  }

 

}

interface FileSname {
  /**
   * 文件上传
   */
  save?: string;
  /**
   * 文件预览
   */
  query: string;
  /**
   * 文件删除
   */
  del?: string;
}
