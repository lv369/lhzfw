import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { TyjbShymComponent } from './shym.component';

describe('TyjbShymComponent', () => {
  let component: TyjbShymComponent;
  let fixture: ComponentFixture<TyjbShymComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TyjbShymComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TyjbShymComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
