import { Component, OnInit, ViewChild } from '@angular/core';
import { _HttpClient } from '@delon/theme';
import { ActivatedRoute } from '@angular/router';
import { NzMessageService, NzModalService } from 'ng-zorro-antd';
import { HttpService, SupDic } from 'lbf';
import { PrintComponent } from '@shared/components/tyjb/lb_print';
import { SqListComponent } from '@shared/components/tyjb/lb_sqlist';
import { UserSelectComponent } from '@shared/components/tyjb/lb_user_select';
import { SFSchema, SFComponent } from '@delon/form';
import { TyekouQueryComponent } from '@shared/components/tyjb/tyekou_query';
import { GrComponent } from '@shared/components/tyjb/lb_gr';
import { DwComponent } from '@shared/components/tyjb/lb_dw';
import { SjFormComponent } from '@shared/components/tyjb/sj_form';

@Component({
  selector: 'app-tyjb-tysj-qt',
  templateUrl: './tysj-qt.component.html',
  styleUrls: ['./tysj-qt.less'],
})
export class TyjbTysjQtComponent implements OnInit {
  topTitle = '事项信息录入';
  curStep = 0; // 当前步骤 0 首页 1 人员选择 2 单位选择 3 人员表单 4 单位表单 5 受理成功 6 不予受理 7 收件箱
  lastStep = 0; // 已完成的步骤

  // 控制页面显示 隐藏
  hidden_List = true;
  hidden_SxList = false; // 显示事项清单
  hidden_Gr = true; // 人员选择
  hidden_Dw = true; // 单位选择
  hidden_SjForm = true; // 申请表单
  hidden_Print = true; // 打印界面
  hidden_userSelect = true; // 用户选择


  // 按钮组
  isBtnDelete = false; // 作废按钮不显示
  isBtnHome = false; // 事项清单首页按钮不显示
  isBtnNext = false; // 下一步按钮不显示
  isBtnBack = false; // 上一步按钮不显示
  isBtnCur = false; // 返回当前经办环节
  isBtnStop = false; //搁置按钮
  isBtnList = true; // 显示列表

  isDelVisible = false; // 作废弹出框是否显示

  curMode: number = 0; // 0 操作模式 0 为默认操作 1 为待办事项查看模式

  styleParam = {}

  // sqData:any // 申请人数据
  // infoData:any // 纠纷明细数据


  dataAYW001 = ''; // 事项编码
  dataAYW002 = ''; // 事项名称
  dataAYW004 = ''; // 事项主体类型
  dataBMLX = ''; // 部门类型 1 中心 2 部门
  dataAYW009: number; // 业务流水号

  // 个人信息
  @ViewChild('gr', { static: false }) gr: GrComponent;
  // 单位信息
  @ViewChild('dw', { static: false }) dw: DwComponent;
  // 表单信息
  @ViewChild('sjForm', { static: false }) sjForm: SjFormComponent;
  // 表单信息
  @ViewChild('print', { static: false }) print: PrintComponent;
  // 待办
  @ViewChild('sqlist', { static: false }) sqlist: SqListComponent;
  // 用户选择
  @ViewChild('userSelect', { static: false }) userSelect: UserSelectComponent;
  // 作废表单
  @ViewChild('delInfoForm', { static: false }) delInfoForm: SFComponent;
  // 详情弹出框
  @ViewChild('queryInfo', { static: false }) queryInfo: TyekouQueryComponent;

  // 作废表单
  deleteSchema: SFSchema = {
    properties: {

      ZFYY: {
        type: 'string',
        title: '作废原因',
        ui: {
          widget: 'textarea',
          autosize: { minRows: 2, maxRows: 6 },

          grid: {
            span: 24
          }
        }
      }

    },
    ui: {
      spanLabelFixed: 100,
      grid: {
        span: 24,
      },
    },
    required: ['ZFYY']
  }


  constructor(
    private route: ActivatedRoute,
    private lbservice: HttpService,
    public msgSrv: NzMessageService,
    private supdic: SupDic
  ) { }

  ngOnInit() {
    this.styleParam = { height: (window.innerHeight - 180) + 'px' }
    this.initSd();
  }
  /********************************************* */
  /********************初始化配置**************** */
  /********************************************* */
  // 初始化属地级联数据
  initSd() {
    this.lbservice.lbservice('PUBLIC_JL', { key: 'DGB030' }).then(resdata => {
      if (resdata.code < 1) {
        this.msgSrv.error(resdata.errmsg);
      }
      else {

      }
    })
  }

  // 隐藏所有功能，所有按钮失效
  hiddenAll() {
    this.hidden_SxList = true; // 首页不显示
    this.hidden_Gr = true; // 个人信息选择
    this.hidden_Dw = true;
    this.hidden_SjForm = true;
    this.hidden_Print = true;
    this.hidden_List = true;
    this.hidden_userSelect = true;

    this.isBtnDelete = false; // 作废按钮不显示
    this.isBtnHome = false; // 事项清单首页按钮不显示
    this.isBtnNext = false; // 下一步按钮不显示
    this.isBtnBack = false; // 上一步按钮不显示
    this.isBtnCur = false; // 返回当前经办环节
    this.isBtnStop = false; //搁置按钮
    this.isBtnList = true;

    // 如果有业务流水号，则显示搁置按钮
    if (this.dataAYW009 !== undefined) {
      this.isBtnStop = true; // 搁置按钮显示
      this.isBtnDelete = true; //作废按钮显示
    }
  }

  /********************************************* */
  /********************按钮组事件**************** */
  /********************************************* */
  // 导航栏按钮单击事件
  btnClick(flag: string) {
    // 首页
    if (flag === 'home') {
      this.showHome();
    }

    /********************************/
    /***********显示当前*************/
    /********************************/
    else if (flag === 'cur') {
      this.showCur();
    }

    /********************************/
    /*************作废***************/
    /********************************/
    else if (flag === 'delete') {
      this.isDelVisible = true;
      console.log('del');
    }
    /********************************/
    /*************我的办件***************/
    /********************************/
    else if (flag === 'list') {
      // this.dataAYW009 = undefined;
      // this.lastStep = 0
      this.showSqList()
    }
    // 搁置
    else if (flag === 'stop') {
      this.dataAYW009 = undefined;
      this.lastStep = 0
      this.showHome()
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////

    /********************************/
    /************下一步**************/
    /********************************/
    else if (flag === 'next') {
      // 调用表单保存过程
      this.lbservice.lbservice('TYJB_GRFORM_ADD', { AYW009: this.dataAYW009, INFO: this.sjForm.mdjfValue }).then(resdata => {
        if (resdata.code < 1) {
          this.msgSrv.error(resdata.errmsg);
        } else {
          // 定位最终步骤，是否是中途返回的
          if (this.lastStep < 6) {
            this.lastStep = 6;
          }
          this.msgSrv.success('申请登记完成')
          this.dataAYW009 = undefined; // 流水号清空
          this.lastStep = 0;
          this.curMode = 0;
          // 显示用户选择 
          // this.showUserSelect();    
          this.showHome();
        }
      })
    }
    else {

    }

  }

  // 首页事项清单列表单击事件
  clickItem(item: any) {

    // 事项变量赋值
    this.dataAYW001 = item.BMID;
    this.dataAYW002 = item.BMMC;
    this.dataAYW004 = item.AYW004;
    this.dataBMLX = item.BMLX;

    this.hiddenAll();
    // 调用新增服务
    this.lbservice.lbservice('TYJB_SQ_INIT', { TYPE: '1', ZTBH: item.AAB001, AYW009: this.dataAYW009, AYW001: this.dataAYW001, BMLX: this.dataBMLX, SXLY: '2' }).then(resdata => {
      if (resdata.code < 1) {
        this.msgSrv.error(resdata.errmsg);
      } else {
        if (this.dataAYW009 === undefined) {
          this.dataAYW009 = resdata.AYW009 // 返回流水号赋值
        }
        // 将申请表单的业务流水号置空，重新取表单信息
        this.sjForm.AYW009 = undefined;
        // 定位最终步骤，是否是中途返回的
        if (this.lastStep < 4) {
          this.lastStep = 4;
        }
        this.curMode = 0;
        // 显示单位类型申请表单 
        this.showSjForm();
      }
    })
  }

  // 待办事项
  DBSX_Click(item: any) {

    //  判断要跳转的页面
    if (item.AYW034 === '1') {

      // 单位表单填写
      if (item.ZTLX === '1') {
        this.lastStep = 4;

      }
      // 个人表单填写
      else {
        this.lastStep = 3;


      }
    }

    else {
      this.queryInfo.AYW009 = item.AYW009;

      return;
    }

    this.dataAYW009 = item.AYW009;
    this.dataAYW001 = item.AYW001;
    this.dataAYW002 = this.supdic.getdicLabel('AYW001', item.AYW001);
    this.dataAYW004 = item.ZTLX;
    // this.infoData = JSON.parse(item.INFO).message;
    // this.sqData = JSON.parse(item.PARA).message;
    this.curMode = 1;

    this.showCur();

  }


  // 操作员指定
  UserSet(item: any) {
    // 调用新增服务
    this.lbservice.lbservice('TYJB_USER_SET', item).then(resdata => {
      if (resdata.code < 1) {
        this.msgSrv.error(resdata.errmsg);
      } else {
        this.msgSrv.success('申请登记完成')
        this.dataAYW009 = undefined; // 流水号清空
        this.lastStep = 0;
        this.curMode = 0;
        // 返回首页
        this.showHome();
      }
    })

  }

  /********************************************* */
  /******************页面显示切换**************** */
  /********************************************* */

  showCur() {
    // 个人信息
    if (this.lastStep === 1) {
      this.showGr();
    }
    // 单位信息
    else if (this.lastStep === 2) {
      this.showDw();
    }
    // 个人表单信息
    else if (this.lastStep === 3) {
      this.showSjForm();
    }
    // 单位表单信息
    else if (this.lastStep === 4) {
      this.showSjForm();
    }
    // 打印页面
    /*
    else if(this.lastStep===5)
    {
      this.showPrint();
    }
    */
    // 用户选择
    else if (this.lastStep === 6) {
      // this.showUserSelect();
      this.showHome();
    }
    else {
    }
  }

  // 0 显示首页
  showHome() {
    this.topTitle = '事项信息录入';
    this.hiddenAll();
    this.hidden_SxList = false; // 首页显示
    this.curStep = 0;
    if (this.lastStep > 0) {
      this.isBtnCur = true; // 返回当前经办环节
    }

  }

  showSqList() {
    this.topTitle = '我的办件清单';
    this.hiddenAll();
    this.hidden_List = false; // 显示
    this.isBtnHome = true;
    if (this.dataAYW009 !== undefined) {
      this.isBtnCur = true;
    }
    this.isBtnList = false;
    this.sqlist.InitGrid = 0;
    this.curStep = 0;
  }

  // 1 显示个人信息选择页
  showGr() {
    this.topTitle = '[人员选择]';
    this.hiddenAll();
    this.curStep = 1;
    if (this.lastStep > 1) {
      this.isBtnCur = true; // 返回当前经办环节
    }
    // 首页
    this.isBtnHome = true;

    // 个人信息 不隐藏
    this.hidden_Gr = false;
  }


  // 5 单位信息选择
  showDw() {
    this.hiddenAll();

    this.topTitle = '[单位选择]';

    this.curStep = 2;

    // 首页显示
    this.isBtnHome = true;
    if (this.lastStep > 2) {
      this.isBtnCur = true;
    }

    // 单位信息 显示
    this.hidden_Dw = false;
  }

  // 显示申请表单
  showSjForm() {

    this.topTitle = '[申请信息采集]';
    this.curStep = 4;
    this.hiddenAll();

    // 如果是正常办理模式，则显示上一步、下一步按钮
    this.isBtnBack = true;

    this.isBtnHome = true;
    if (this.lastStep > 4) {
      this.isBtnCur = true;
    }
    this.isBtnNext = true;

    // 如果表单业务流水号为空，或是与当前业务流水号不一致，则更新
    if (this.sjForm.AYW009 === undefined || this.sjForm.AYW009 !== this.dataAYW009) {
      this.sjForm.AYW009 = this.dataAYW009; // 业务流水号赋值，根据业务流水号，重新刷新页面
    }

    this.hidden_SjForm = false;

  }

  // 显示打印界面
  /*
  showPrint(){
    this.topTitle = '[申请回执单打印]--'+this.dataAYW002+''; 
    this.curStep = 5;
    this.hiddenAll();
    // 首页、上一步、下一步、搁置、作废
    
    // 如果是正常办理模式，则显示上一步、下一步按钮
    this.isBtnHome = true;      
    if(this.lastStep>5){
      this.isBtnCur = true;
    }
    this.isBtnBack = true;
    this.isBtnNext = true;
    this.print.REPID = '19';
    this.print.QueryPara = { dad010: '164' }
    this.hidden_Print = false;
  }
  */
  // 显示用户选择界面
  /*
  showUserSelect(){
    this.topTitle = '[受理人员指派]--'+this.dataAYW002+''; 
    this.curStep = 6;
    this.hiddenAll();
    // 首页、上一步、下一步、搁置、作废
    
    // 如果是正常办理模式，则显示上一步、下一步按钮
    this.isBtnBack = true;      
    this.isBtnHome = true;
    this.userSelect.AYW009 = this.dataAYW009;
    this.userSelect.AYW001 = this.dataAYW001;
  
    this.hidden_userSelect = false;
  }
  */
  // 打开弹出框
  openWindow(flag: string) {
    if (flag === "del") {
      this.isDelVisible = true;
    }
  }

  // 关闭弹出框
  closeWindow(flag: string) {
    if (flag === "del") {
      this.isDelVisible = false;
    }
  }

  save(flag: string) {
    if (flag === "del") {
      if (!this.delInfoForm.valid) {
        this.msgSrv.error('作废原因不能为空！');
        return;
      }

      // 调用作废服务
      this.lbservice.lbservice('TYJB_SQ_DEL', { AYW009: this.dataAYW009, para: this.delInfoForm.value }).then(resdata => {
        if (resdata.code < 1) {
          this.msgSrv.error(resdata.errmsg);
        } else {
          this.msgSrv.success('事项作废成功')
          this.delInfoForm.reset();
          this.isDelVisible = false;
          this.dataAYW009 = undefined; // 流水号清空
          this.lastStep = 0;
          this.curMode = 0;

          // 返回首页
          this.showHome();
        }
      })
    }
  }

}
