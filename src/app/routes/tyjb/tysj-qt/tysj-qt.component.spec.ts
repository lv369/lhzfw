import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { TyjbTysjQtComponent } from './tysj-qt.component';

describe('TyjbTysjQtComponent', () => {
  let component: TyjbTysjQtComponent;
  let fixture: ComponentFixture<TyjbTysjQtComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TyjbTysjQtComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TyjbTysjQtComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
