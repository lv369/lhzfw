import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { TyjbTysjNewComponent } from './tysj-new.component';

describe('TyjbTysjNewComponent', () => {
  let component: TyjbTysjNewComponent;
  let fixture: ComponentFixture<TyjbTysjNewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TyjbTysjNewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TyjbTysjNewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
