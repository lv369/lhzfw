import { Component, OnInit, ViewChild } from '@angular/core';
import { SFSchema, SFComponent } from '@delon/form';
import { STColumn, STColumnTag } from '@delon/abc';
import { GridComponent, SupDic, HttpService } from 'lbf';
import { NzMessageService } from 'ng-zorro-antd';
import { TyekouQueryComponent } from '@shared/components/tyjb/tyekou_query';
import { CacheService } from '@delon/cache';
import { RefService } from '@core/lb/RefService';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-tyjb-bmxtyy',
  templateUrl: './bmxtyy.component.html',
})
export class TyjbBmxtyyComponent implements OnInit {

  params = { sname: 'TYJB_BMXT_QUERY', queryparas: {} };

  searchSchema: SFSchema = {
    properties: {
      AYW009: {
        title: '业务流水号',
        type: 'number'
      },
      AYW023: {
        title: '邀约状态',
        type: 'string',
        enum: [{ label: '未处理', value: '0' }, { label: '已审核', value: '1' }, { label: '已接受', value: '2' }],
        ui: {
          widget: 'radio'
        }
      },
      KSSJ: {
        type: 'string',
        title: '邀约开始时间',
        ui: { widget: 'date' }
      },
      JSSJ: {
        type: 'string',
        title: '邀约结束时间',
        ui: { widget: 'date' }
      },

    }
  }



  columns: STColumn[] = [
    {
      title: '审核操作',
      index: 'AYW023',
      render: 'custom'
    },
    
    {
      title: '业务流水号',
      buttons: [
        {
          text: (record)=>{ return record.AYW009 },
          click: (record) => {
            this.queryInfo.AYW009 = record.AYW009;
          }
        }
      ]
    },
    { title: '邀约状态', index: 'AYW023', dic: 'AYW023' },
    { title: '接受时间', index: 'JSSJ' },
    /*{ title: '业务流水号', index: 'AYW009', type: 'number' },*/
    { title: '纠纷事项', index: 'AYW001', dic: 'AYW001' },
    { title: '发起部门', index: 'FSBM', dic: 'DGB040' },
    { title: '邀约时间', index: 'SQSJ' },
    { title: '接受邀约人', index: 'AYW024', dic: 'USERID' }
  ]
  curParamId: number = -1;
  @ViewChild('st', { static: false }) st: GridComponent;
  @ViewChild('sf', { static: false }) sf: SFComponent;
  // 详情弹出框
  @ViewChild('queryInfo', { static: false }) queryInfo: TyekouQueryComponent;
  private refSub: Subscription;
  constructor(private msgSrv: NzMessageService, private ref: RefService, private supdic: SupDic, private httpService: HttpService, public srv: CacheService) { }

  ngOnInit() {

  }

  _onReuseInit() {

  }

  ngAfterViewInit() {
    this.refSub = this.ref.getRef('shgl').subscribe(msg => {
      console.log(msg);
      this.sf.setValue('/AYW009', msg.message.AYW009)
      this.query();
    })
  }

  ngOnDestroy(): void {
    this.refSub.unsubscribe();
  }

  todoInit() {
    // 去获取是否有传递参数
    this.httpService.lbservice('PUBLIC_INIT', {}).then(resdata => {
      if (resdata.code < 1) {
        this.msgSrv.error(resdata.errmsg);
      }
      else {
        console.log(resdata)
        // 判断是否有初始化数据
        if (resdata.message.AYW009 !== undefined) {
          this.curParamId = resdata.message.AYW039;
          this.sf.setValue('/AYW009', resdata.message.AYW009)
          this.query();
        }
        // 如果没有参数传递，默认打开首页
        else {
          if (this.curParamId < 0) {
            this.query();
          }
        }
      }
    })
  }

  query() {
    this.st.reload(this.sf.value);
  }

  // 事项状态切换
  changeSxState(dataDGB010: string, dataAYW043: number, dataAYW023: string) {
    this.httpService.lbservice("TYJB_YY_DO", { DGB010: dataDGB010, AYW043: dataAYW043, AYW023: dataAYW023 }).then(resdata => {
      if (resdata.code < 1) {
        this.msgSrv.error(resdata.errmsg);
      }
      else {
        this.msgSrv.success('邀约处理成功');
        this.st.st.reload();
      }
    });
  }

}

