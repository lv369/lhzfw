import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { TyjbBmxtyyComponent } from './bmxtyy.component';

describe('TyjbBmxtyyComponent', () => {
  let component: TyjbBmxtyyComponent;
  let fixture: ComponentFixture<TyjbBmxtyyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TyjbBmxtyyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TyjbBmxtyyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
