import { Component, OnInit, ViewChild } from '@angular/core';
import { _HttpClient, ModalHelper } from '@delon/theme';
import { STColumn, STChange, STPage, STComponent } from '@delon/abc';
import {
  SFSchema,
  SFTreeSelectWidgetSchema,
  SFSchemaEnum,
  SFComponent,
  SFRadioWidgetSchema,
} from '@delon/form';
import { GridComponent, PopdetailComponent, HttpService, SupDic } from 'lbf';
import { ActivatedRoute } from '@angular/router';
import { NzMessageService } from 'ng-zorro-antd';
// tslint:disable-next-line: no-duplicate-imports
import { Router } from '@angular/router';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { of, config } from 'rxjs';
import { delay } from 'rxjs/operators';
// tslint:disable-next-line: use-path-mapping

@Component({
  selector: 'app-tyjb-tysl',
  templateUrl: './tysl.component.html',
  styleUrls: ['./tysl.component.less'],
})
export class TyjbTyslComponent implements OnInit {
  dslsxnum;
  dataset = [];
  isVisible = false;
  queryFormData:any = {}

  searchSchema: SFSchema = {
    properties: {
      DAD075: {
        type: 'string',
        title: '申报者名称',
      },
      DAD070: {
        type: 'string',
        title: '申报者证件号',
      },
      DAD069: {
        type: 'string',
        title: '申报号',
      },
      DAD031: {
        type: 'string',
        title: '事项名称',
        ui: { sapn: 8 },
      },
      DAD090: {
        type: 'string',
        title: '事项名称',
        ui: { hidden: true, sapn: 8 },
        default: '2',
      },
    },
    ui: {
      spanLabelFixed: 100,
      grid: {
        span: 8,
      },
    },
  };
  @ViewChild('st', { static: false }) st: GridComponent;
  @ViewChild('sf', { static: false }) sf: SFComponent;
  columns: STColumn[] = [
    { title: '申报号', index: 'DAD069', type: 'number' },
    { title: '事项名称', index: 'DAD031' },
    { title: '申报者名称', index: 'DAD075' },
    { title: '申报时间', type: 'date', index: 'AAE036' },
    {
      title: '操作区',
      width: '50px',
      buttons: [
        {
          text: '受理',
          click: (record: any) => this.tysl(record),
        },
      ],
    },
  ];

  constructor(
    private http: _HttpClient,
    private modal: ModalHelper,
    private lbservice: HttpService,
    public msgSrv: NzMessageService,
    private router: Router,
    private supDic: SupDic,
    private routerinfo: ActivatedRoute,
  ) {}

  stpage: STPage = {
    total: true,
    show: true, // 显示分页
    front: false, // 关闭前端分页，true是前端分页，false后端控制分页
  };
  stchange: STChange = {
    type: 'pi',
    pi: 1,
    ps: 10,
    total: 0,
  };

  ngOnInit() {
    if (
      this.routerinfo.snapshot.queryParams &&
      this.routerinfo.snapshot.queryParams.id
    ) {
      // this.routerinfo.snapshot.queryParams.id;
      this.queryFormData.DAD069 = this.routerinfo.snapshot.queryParams.id;
    }
  }
  _onReuseInit() {
    if (
      this.routerinfo.snapshot.queryParams &&
      this.routerinfo.snapshot.queryParams.id
    ) {
      // this.routerinfo.snapshot.queryParams.id;
      this.queryFormData.DAD069 = this.routerinfo.snapshot.queryParams.id;
      this.sf.reset();
    }
    this.dslsxnum_query();
  }

  // tslint:disable-next-line: use-lifecycle-interface
  ngAfterViewInit() {
    this.dslsxnum_query();
  }

  add() {
    // this.modal
    //   .createStatic(FormEditComponent, { i: { id: 0 } })
    //   .subscribe(() => this.st.reload());
  }

  dslsxnum_query() {
    this.st.reload(this.sf.value);

  }

  tysl(id) {
    setTimeout(() => {
      this.router.navigate(['/tyjb/slym'], {
        queryParams: { dad069: id.DAD069, dad017: id.DAD017 ,dad001:id.DAE001},
      });
    }, 500);
  }

  handleCancel() {
    this.isVisible = false;
  }
}
