import { Component, OnInit } from '@angular/core';
import { _HttpClient } from '@delon/theme';
import { ActivatedRoute } from '@angular/router';
import { NzMessageService } from 'ng-zorro-antd';
import { ReuseTabService } from '@delon/abc';

@Component({
  selector: 'app-tyjb-sxcl',
  templateUrl: './sxcl.component.html',
  styleUrls: ['./sxcl.component.less'],
})
export class TyjbSxclComponent implements OnInit {
 
  gotoUrl:string = ''; 
  constructor(
    private route: ActivatedRoute,
    public msgSrv: NzMessageService,
    private reuseTabSrv: ReuseTabService,
  ) {
    
  }

  
  ngOnInit() {
    this.route.queryParams.subscribe(param => {
      
      this.gotoUrl = param['R'];
      this.reuseTabSrv.replace(this.gotoUrl);
      
    });

    
  }

  
}