import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { TyjbSxclComponent } from './sxcl.component';

describe('TyjbSxclComponent', () => {
  let component: TyjbSxclComponent;
  let fixture: ComponentFixture<TyjbSxclComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TyjbSxclComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TyjbSxclComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
