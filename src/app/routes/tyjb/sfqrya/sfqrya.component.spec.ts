import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { TyjbSfqryaComponent } from './sfqrya.component';

describe('TyjbSfqryaComponent', () => {
  let component: TyjbSfqryaComponent;
  let fixture: ComponentFixture<TyjbSfqryaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TyjbSfqryaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TyjbSfqryaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
