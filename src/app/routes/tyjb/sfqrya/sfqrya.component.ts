import { Component, OnInit, ViewChild, Output, EventEmitter } from '@angular/core';
import { _HttpClient, ModalHelper } from '@delon/theme';
import { STColumn, STComponent, STColumnBadge, STColumnTag } from '@delon/abc/table';
import { SFSchema, SFComponent } from '@delon/form';
import { HttpService, SupDic, GridComponent } from 'lbf';
import { TyekouQueryComponent } from '@shared/components/tyjb/tyekou_query';
import { NzMessageService } from 'ng-zorro-antd';

@Component({
  selector: 'app-tyjb-sfqrya',
  templateUrl: './sfqrya.component.html',
})
export class TyjbSfqryaComponent implements OnInit {
  
  addSchema: SFSchema
  querySchema: SFSchema = {
    properties: {

      ZTBH: { type: 'string', title: '主体编号' },
      ZTMC: { type: 'string', title: '主体名称' },
      KSSJ: {
        type: 'string',
        title: '开始时间',
        ui: { widget: 'date' }
      },
      JSSJ: {
        type: 'string',
        title: '结束时间',
        ui: { widget: 'date' }
      },
    }
  }


  // 主体对象类别渲染标签 AYW004
  TAG: STColumnTag = {
    '92': { text: '办理成功', color: 'green' },
    '99': { text: '待司法中心确认', color: 'purple' },
  };

  BADGE: STColumnBadge = {
    '1': { text: '单位', color: 'success' },
    '2': { text: '个人', color: 'processing' },
  };


  // 列表
  grColumns: STColumn[] = [
    { title: '流水号', index: 'AYW009', width: 70 },
    { title: '纠纷事项', index: 'AYW001', dic: 'AYW001', width: 100 },
    { title: '当前状态', index: 'AYW034', type: 'tag', tag: this.TAG, width: 100 },
    { title: '最近操作时间', index: 'AAE035', width: 100 },
    { title: '对象主体', index: 'ZTLX', type: 'badge', badge: this.BADGE, width: 100 },
    { title: '主体编号', index: 'ZTBH', width: 100 },
    { title: '主体名称', index: 'ZTMC', width: 100 },
    { title: '乡镇街道', index: 'CAA001', dic: 'CAA001', width: 100 },
    { title: '村社区', index: 'AAE025', dic: 'AAE025', width: 100 },
    { title: '详细地址', index: 'ADDR', width: 100 },
    { title: '手机号码', index: 'PHONE', width: 100 },
    
    {
      title: '操作区',
      fixed: 'right',
      width: 80, 
      buttons: [
  
      
        {
          text: '查看详情',
          click: (record: any) => {
            this.queryInfo.AYW009 = record.AYW009;
          },
        },

        // {
        //   text: record => {
        //     if (record.SQSHJG !== '2') {
        //       return '已审核'
        //     }
        //     else {
        //       return '审核'
        //     }
        //   },
        //   click: (record) => this.sh(record),
        //   iif: record => record.SQSHJG === '2',
        //   iifBehavior: 'disabled',
        // }

      ],
    },
  ];

  // 详情弹出框
  @ViewChild('queryInfo', { static: false }) queryInfo: TyekouQueryComponent;
  // 列表
  @ViewChild('grGrid', { static: false }) grGrid: GridComponent;
  // 查询表单
  @ViewChild('queryForm', { static: false }) queryForm: SFComponent;


  @Output() set InitGrid(value: number) {
    this.doQuery();
  }

  @Output() clickItem = new EventEmitter<string>();


  @Output() set Where(_where: any) {
    this.grGrid.reload(_where);
  }

  // 人员列表查询
  doQuery(pi1 = false) {
    if (pi1) {
      this.grGrid.pi = 1;
    }
    this.grGrid.reload(this.queryForm.value);
  }


  constructor(
    private lbservice: HttpService,
    private supDic: SupDic,
    public msgSrv: NzMessageService,
  ) {

  }

  ngOnInit() {

  }


  

}
