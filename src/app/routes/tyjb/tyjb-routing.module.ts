import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TyjbTysjComponent } from './tysj/tysj.component';
import { TyjbSxclComponent } from './sxcl/sxcl.component';
import { TyjbDjxxComponent } from './djxx/djxx.component';
import { TyjbTyslComponent } from './tysl/tysl.component';
import { TyjbSlymComponent } from './slym/slym.component';
import { TyjbShglComponent } from './shgl/shgl.component';
import { TyjbShymComponent } from './shym/shym.component';
import { TyjbBjymComponent } from './bjym/bjym.component';
import { TyjbTycjComponent } from './tycj/tycj.component';
import { TyjbCjprComponent } from './cjpr/cjpr.component';
import { TyjbTysjNewComponent } from './tysj-new/tysj-new.component';
import { TyjbTyslNewComponent } from './tysl-new/tysl-new.component';
import { TyjbTyblNewComponent } from './tybl-new/tybl-new.component';
import { TyjbBmxtyyComponent } from './bmxtyy/bmxtyy.component';
import { TyjbTysjCkslComponent } from './tysj-cksl/tysj-cksl.component';
import { TyjbTysjQtComponent } from './tysj-qt/tysj-qt.component';
import { TyjbTysjXzComponent } from './tysj-xz/tysj-xz.component';
import { TyjbTysj12345Component } from './tysj-12345/tysj-12345.component';
import { TyjbSjspglComponent } from './sjspgl/sjspgl.component';
import { TyjbXtyyryzpComponent } from './xtyyryzp/xtyyryzp.component';
import { TyjbXtycxComponent } from './xtycx/xtycx.component';
import { TyjbSfqryaComponent } from './sfqrya/sfqrya.component';
import { TyjbSfqrtjComponent } from './sfqrtj/sfqrtj.component';
import { DjtComponent } from './djt/djt.component';
import { TyjbTysjXzptComponent } from './tysj-xzpt/tysj-xzpt.component';


const routes: Routes = [

  { path: 'tysj', component: TyjbTysjComponent },
  { path: 'sxcl', component: TyjbSxclComponent },
  { path: 'djxx', component: TyjbDjxxComponent },
  { path: 'tysl', component: TyjbTyslComponent },
  { path: 'slym', component: TyjbSlymComponent },
  { path: 'shgl', component: TyjbShglComponent },
  { path: 'shym', component: TyjbShymComponent },
  { path: 'bjym', component: TyjbBjymComponent },
  { path: 'tycj', component: TyjbTycjComponent },
  { path: 'cjpr', component: TyjbCjprComponent },
  { path: 'tysj_new', component: TyjbTysjNewComponent },
  { path: 'tysl-new', component: TyjbTyslNewComponent },
  { path: 'tybl-new', component: TyjbTyblNewComponent },
  { path: 'bmxtyy', component: TyjbBmxtyyComponent },
  { path: 'tysj-cksl', component: TyjbTysjCkslComponent },
  { path: 'tysj-qt', component: TyjbTysjQtComponent },
  { path: 'tysj-xz', component: TyjbTysjXzComponent },
  { path: 'tysj-12345', component: TyjbTysj12345Component },
  { path: 'tysj-xzpt', component: TyjbTysjXzptComponent },
  { path: 'sjspgl', component: TyjbSjspglComponent },
  { path: 'xtyyryzp', component: TyjbXtyyryzpComponent },
  { path: 'xtycx', component: TyjbXtycxComponent },
  { path: 'sfqrya', component: TyjbSfqryaComponent },
  { path: 'sfqrtj', component: TyjbSfqrtjComponent },
  { path: 'djt', component: DjtComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TyjbRoutingModule { }
