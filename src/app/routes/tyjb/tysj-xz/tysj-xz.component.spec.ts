import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { TyjbTysjXzComponent } from './tysj-xz.component';

describe('TyjbTysjXzComponent', () => {
  let component: TyjbTysjXzComponent;
  let fixture: ComponentFixture<TyjbTysjXzComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TyjbTysjXzComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TyjbTysjXzComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
