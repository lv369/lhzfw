import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { TyjbSlymComponent } from './slym.component';

describe('TyjbSlymComponent', () => {
  let component: TyjbSlymComponent;
  let fixture: ComponentFixture<TyjbSlymComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TyjbSlymComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TyjbSlymComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
