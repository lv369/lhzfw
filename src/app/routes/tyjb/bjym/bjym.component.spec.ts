import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { TyjbBjymComponent } from './bjym.component';

describe('TyjbBjymComponent', () => {
  let component: TyjbBjymComponent;
  let fixture: ComponentFixture<TyjbBjymComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TyjbBjymComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TyjbBjymComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
