import { Injectable } from '@angular/core';
import { HttpService, SupDic } from 'lbf';

@Injectable({
  providedIn: 'root',
})
export class ZskService {
  private DAD011;
  private DAD017;
  constructor(
    private lbs:HttpService 
    ,private supdic: SupDic,) { }

  setDAD011(para){
    this.DAD011=para;
  }
  getDAD011(dad017=undefined){
    if(this.DAD011===undefined && dad017!==undefined){
      this.lbs.lbservice("ZSKWH_QSXJBXX",{para:{DAD017:dad017}}).then(resdata=>{
        if(resdata.code>0){
          this.DAD011=resdata.message.list[0].DAD011;
        }
      });
    }
    return this.DAD011;
  }


  setDAD017(para){
    this.DAD017=para;
  }
  getDAD017(){
    return this.DAD017;
  }



  /*****************************MAP************************************* */
  DAD065Map=new Map();
  initDAD065Map(){
    this.supdic.getSFDic('DAD065').forEach(e=>{
      this.DAD065Map.set(e['value'],e['label']);
    });
  }

  getDAD065Val(para){
    if(para===undefined){
      return '';
    }
    if(this.DAD065Map.size===0){
      this.initDAD065Map();
    }
    return this.DAD065Map.get(para[0]);
  }

  DAD063Map=new Map();
  initDAD063Map(){
    this.supdic.getSFDic('DAD063').forEach(e=>{
      this.DAD063Map.set(e['value'],e['label']);
    });
  }
  getDAD063Val(para){
    if(para===undefined){
      return '';
    }
    if(this.DAD063Map.size===0){
      this.initDAD063Map();
    }
    return this.DAD063Map.get(para);
  }

DAD012Map=new Map();
  initDAD012Map(){
    this.supdic.getSFDic('DAD012').forEach(e=>{
      this.DAD012Map.set(e['value'],e['label']);
    });
  }
  getDAD012Val(para){
    if(para===undefined){
      return '';
    }
    if(this.DAD012Map.size===0){
      this.initDAD012Map();
    }
    return this.DAD012Map.get(para);
  }

  DAD170Map=new Map();
  initDAD170Map(){
    this.supdic.getSFDic('DAD170').forEach(e=>{
      this.DAD170Map.set(e['value'],e['label']);
    });
  }
  getDAD170Val(para){
    if(para===undefined){
      return '';
    }
    if(this.DAD170Map.size===0){
      this.initDAD170Map();
    }
    return this.DAD170Map.get(para);
  }

  DAD105Map=new Map();
  initDAD105Map(){
    this.supdic.getSFDic('DAD105').forEach(e=>{
      this.DAD105Map.set(e['value'],e['label']);
    });
  }
  getDAD105Val(para){
    if(para===undefined){
      return '';
    }
    if(this.DAD105Map.size===0){
      this.initDAD105Map();
    }
    return this.DAD105Map.get(para);
  }

  DAC002Map=new Map();
  initDAC002Map(){
    this.supdic.getSFDic('DAC002').forEach(e=>{
      this.DAC002Map.set(e['value'],e['label']);
    });
  }
  getDAC002Val(para){
    if(para===undefined){
      return '';
    }
    if(this.DAC002Map.size===0){
      this.initDAC002Map();
    }
    return this.DAC002Map.get(para);
  }

  DAC003Map=new Map();
  initDAC003Map(){
    this.supdic.getSFDic('DAC003').forEach(e=>{
      this.DAC003Map.set(e['value'],e['label']);
    });
  }
  getDAC003Val(para){
    if(para===undefined){
      return '';
    }
    if(this.DAC003Map.size===0){
      this.initDAC003Map();
    }
    return this.DAC003Map.get(para);
  }

  DAD034Map=new Map();
  initDAD034Map(){
    this.supdic.getSFDic('DAD034').forEach(e=>{
      this.DAD034Map.set(e['value'],e['label']);
    });
  }
  getDAD034Val(para){
    if(para===undefined){
      return '';
    }
    if(this.DAD034Map.size===0){
      this.initDAD034Map();
    }
    return this.DAD034Map.get(para);
  }

  DAD080Map=new Map();
  initDAD080Map(){
    this.supdic.getSFDic('DAD080').forEach(e=>{
      this.DAD080Map.set(e['value'],e['label']);
    });
  }
  getDAD080Val(para){
    if(para===undefined){
      return '';
    }
    if(this.DAD080Map.size===0){
      this.initDAD080Map();
    }
    return this.DAD080Map.get(para);
  }

  DAD082Map=new Map();
  initDAD082Map(){
    this.supdic.getSFDic('DAD082').forEach(e=>{
      this.DAD082Map.set(e['value'],e['label']);
    });
  }
  getDAD082Val(para){
    if(para===undefined){
      return '';
    }
    if(this.DAD082Map.size===0){
      this.initDAD082Map();
    }
    return this.DAD082Map.get(para);
  }

}