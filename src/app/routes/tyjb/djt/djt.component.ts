import { Component, TemplateRef, OnInit, ViewChild } from '@angular/core';
import { _HttpClient } from '@delon/theme';
import { STColumn, STColumnTag } from '@delon/abc';
import { SFSchema, SFComponent } from '@delon/form';
import { SupDic, HttpService, GridComponent } from 'lbf';
import { NzMessageService, NzTreeComponent } from 'ng-zorro-antd';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-djt',
  templateUrl: './djt.component.html',
  styleUrls: ['./djt.component.less']
})
export class DjtComponent implements OnInit {
  addFormData = {}; // 新增弹出框表单数据
  modiFormData = {}; // 修改弹出框表单数据
  queryFormData = {}; // 查询区表单数据
  showAddWindow = false; // 是否显示新增弹出框
  showModiWindow = false; // 是否显示修改弹出框
  showWindow = false; //是否显示详情弹出框
  apd: any = {};
  dataAAC001 = 0;
  @ViewChild('extra', { static: true }) private extra: TemplateRef<void>;
  // 事项列表
  @ViewChild('st', { static: true }) st: GridComponent;
  // 事项新增表单
  @ViewChild('addWindow', { static: false }) addWindow: SFComponent;
  // 事项修改表单
  @ViewChild('modiWindow', { static: false }) modiWindow: SFComponent;
  // 查询区表单
  @ViewChild('searchForm', { static: false }) searchForm: SFComponent;
  // 详情弹出框
  @ViewChild('queryInfo', { static: false }) queryInfo: SFComponent;

  // 查询区
  searchSchema: SFSchema = {
    properties: {
      AAC003: {
        type: 'string',
        title: '姓名'
      },
      AAC008: {
        type: 'string',
        title: '人员类别',
        enum: this.dic.getSFDic('AAC008'),
        ui: {
          widget: 'select',
          allowClear: true,
        }
      },
    },
    ui: {
      spanLabelFixed: 100,
      grid: {
        span: 6
      }
    }
  };

  // 新增表单
  addSchema = {
    properties: {
      AAC003: {
        type: 'string',
        title: '姓名'
      },
      AAC002: {
        type: 'string',
        title: '身份证号'
      },
      AAE005: {
        type: 'string',
        title: '电话号码'
      },
      AAC008: {
        type: 'string',
        title: '人员类别',
        enum: this.dic.getSFDic('AAC008'),
        ui: {
          widget: 'select',
          allowClear: true,
        }
      },
      JJSM: {
        type: 'string',
        title: '简介说明',
        ui: {
          widget: 'textarea',
          autosize: { minRows: 2, maxRows: 6 },
          grid: {
            span: 24
          }
        }
      },
      AAE013: {
        type: 'string',
        title: '备注',
        ui: {
          widget: 'textarea',
          autosize: { minRows: 2, maxRows: 6 },
          grid: {
            span: 24
          }
        }
      },
      PIC: {
        type: 'string',
        title: '上传照片',
        enum: [
          {
            uid: -1,
            name: 'xxx.png',
            status: 'done',
            url: 'https://zos.alipayobjects.com/rmsportal/jkjgkEfvpUPVyRjUImniVslZfWPnJuuZ.png',
            response: {
              resource_id: 1,
            },
          },
        ],
        ui: {
          widget: 'upload',
          action: '/upload',
          resReName: 'resource_id',
          urlReName: 'url',
        }
      },
    },
    ui: {
      spanLabelFixed: 90,
      grid: {
        span: 12
      }
    },
    required: ['AAC003', 'AAE005', 'AAC008', 'JJSM']
  };

  // 修改表单
  updateSchema = {
    properties: {
      AAC001: {
        type: 'string',
        title: '个人编号',
        ui:
        {
          hidden: true
        }
      },
      AAC003: {
        type: 'string',
        title: '姓名',
        readOnly: true
      },
      AAE005: {
        type: 'string',
        title: '电话号码'
      },
      AAC008: {
        type: 'string',
        title: '人员类别',
        enum: this.dic.getSFDic('AAC008'),
        readOnly: true,
        ui: {
          widget: 'select',
          allowClear: true,
        }
      },
      AAE013: {
        type: 'string',
        title: '备注',
        ui: {
          widget: 'textarea',
          autosize: { minRows: 2, maxRows: 6 },
          grid: {
            span: 24
          }
        }
      },
    },
    ui: {
      spanLabelFixed: 90,
      grid: {
        span: 12
      }
    },
    required: ['AAC003', 'AAE005', 'AAC008']
  };


  QuerySchema: SFSchema = {
    properties: {
      PIC: {
        type: 'string',
        title: '照片',
        ui: {
          widget: 'textarea',
          autosize: { minRows: 4, maxRows: 6 },
          grid: {
            span: 12
          }
        }
      },
      AAC003: {
        type: 'string',
        title: '姓名',
      },
      AAE005: {
        type: 'string',
        title: '电话号码',
      },
      JJSM: {
        type: 'string',
        title: '简介说明',
        ui: {
          widget: 'textarea',
          autosize: { minRows: 2, maxRows: 6 },
          grid: {
            span: 24
          }
        }
      },
      AAE013: {
        type: 'string',
        title: '备注',
        ui: {
          widget: 'textarea',
          autosize: { minRows: 2, maxRows: 6 },
          grid: {
            span: 24
          }
        }
      },
    },
    ui: {
      spanLabelFixed: 90,
      grid: {
        span: 12
      }
    },
  };

  gridData = [];

  // 事项列表生成串
  columns: STColumn[] = [
    { title: '个人编号', index: 'AAC001', hidden: true },
    { title: '姓名', index: 'AAC003' },
    { title: '电话号码', index: 'AAE005' },
    { title: '人员类别', index: 'AAC008', dic: 'AAC008' },
    { title: '备注', index: 'AAE013' },
    {
      title: '操作区',
      fixed: 'right',
      width: 80, buttons: [
        {
          text: '查看详情',
          click: (record: any) => {
            this.dataAAC001 = record.AAC001;
            this.openWindow('query');
          },
        },
        {
          text: '修改',
          icon: 'edit',
          click: (record: any) => {
            // 给修改编辑框赋值
            this.modiWindow.setValue('/AAC001', (record.AAC001));
            this.modiWindow.setValue('/AAC003', (record.AAC003));
            this.modiWindow.setValue('/AAE005', (record.AAE005));
            this.modiWindow.setValue('/AAC008', (record.AAC008));
            this.modiWindow.setValue('/AAE013', (record.AAE013));
            // 打开修改编辑框
            this.openWindow('modi');
          }
        },
      ]
    },
  ];

  constructor(
    private lbservice: HttpService,
    private http: HttpClient,
    private dic: SupDic,
    private lbs: HttpService,
    public msgSrv: NzMessageService,
  ) { }

  ngOnInit() {

  }

  ngAfterViewInit() {
    this.query();
  }

  // 打开弹出框 根据传入标签进行处理
  openWindow(flag: string) {
    // 新增
    if (flag === 'add') {
      this.showAddWindow = true;
    }
    // 修改
    else if (flag === 'modi') {
      this.showModiWindow = true;
    }
    else if (flag === 'query') {
      this.lbservice.lbservice('DJT_QUERY', { AAC001: this.dataAAC001 }).then(resdata => {
        if (resdata.code < 1) {
          this.msgSrv.error(resdata.errmsg);
        } else {
          this.queryInfo.setValue('/AAC003', resdata.message.list[0].AAC003);
          this.queryInfo.setValue('/AAE005', resdata.message.list[0].AAE005);
          this.queryInfo.setValue('/AAE013', resdata.message.list[0].AAE013);
          this.queryInfo.setValue('/JJSM', resdata.message.list[0].JJSM);
          this.queryInfo.setValue('/PIC', resdata.message.list[0].PCITURE);
          this.showWindow = true;
        }
      })
    }
    // 主管领导设置
    else {

    }
  }

  // 关闭弹出框 根据传入标签进行处理
  closeWindow(flag: string) {
    // 新增
    if (flag === 'add') {
      this.showAddWindow = false;
    }
    // 修改
    else if (flag === 'modi') {
      this.showModiWindow = false;
    }
    else if (flag === 'query') {
      this.showWindow = false;
    }
    else {

    }
  }

  // 新增保存事件
  addSave() {
    // 必输项校验
    if (!this.addWindow.valid) {
      this.msgSrv.error('必填项不能为空！');
      return;
    }
    // 调用新增服务
    this.lbs.lbservice('DJT_add', { para: this.addWindow.value }).then(resdata => {
      if (resdata.code < 1) {
        this.msgSrv.error(resdata.errmsg);
      }
      else {
        // 关闭新增框
        this.closeWindow('add');
        // 新增框内容情况
        this.addFormData = {};
        this.msgSrv.success('信息填报成功');
        // 成功后，重新加载列表
        this.query();
      }

    })

  }

  // 修改保存事件
  modiSave() {
    // 必输项校验
    if (!this.modiWindow.valid) {
      this.msgSrv.error('必填项不能为空！');
      return;
    }

    // 调用事项修改服
    this.lbs.lbservice('DJT_update', { para: this.modiWindow.value }).then(resdata => {
      if (resdata.code < 1) {
        this.msgSrv.error(resdata.errmsg);
      }
      else {
        // 关闭修改框
        this.closeWindow('modi');
        // 将修改框清空
        this.modiFormData = {};
        this.msgSrv.success('信息修改成功');
        // 成功后，重新加载列表
        this.query();
      }
    })
  }

  // 事项信息查询
  query(pi1 = false) {

    if (pi1) {
      this.st.pi = 1;
    }
    this.st.reload(this.searchForm.value);
  }

}

