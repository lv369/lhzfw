import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { TyjbTyslNewComponent } from './tysl-new.component';

describe('TyjbTyslNewComponent', () => {
  let component: TyjbTyslNewComponent;
  let fixture: ComponentFixture<TyjbTyslNewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TyjbTyslNewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TyjbTyslNewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
