import { Component, OnInit, ViewChild } from '@angular/core';
import { TyekouTyslComponent } from '@shared/components/tyjb/tyekou_tysl';
import { RefService } from '@core/lb/RefService';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-tyjb-tysl-new',
  templateUrl: './tysl-new.component.html',
})
export class TyjbTyslNewComponent implements OnInit {

  @ViewChild('tysl', { static: false }) tysl: TyekouTyslComponent;

  private refSub: Subscription;
  constructor(
    private ref: RefService,
  ) {


  }

  ngOnInit() {
    // this.tysl.showSqList();


  }

  ngAfterViewInit() {
    this.refSub = this.ref.getRef('shgl').subscribe(msg => {
      console.log(msg);
      this.tysl.showInitList(msg.message);
    })
  }

  ngOnDestroy(): void {
    this.refSub.unsubscribe();
    console.log(this.refSub.closed);
  }


}
