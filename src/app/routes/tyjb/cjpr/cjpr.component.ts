import { Component, OnInit, ViewChild } from '@angular/core';
import { _HttpClient, ModalHelper } from '@delon/theme';
import { STColumn, STComponent } from '@delon/abc/table';
import { SFSchema, SFRadioWidgetSchema, SFComponent } from '@delon/form';
import { GridComponent, HttpService } from 'lbf';
import { Router, ActivatedRoute } from '@angular/router';
import { NzMessageService } from 'ng-zorro-antd';
import { RepserviceService } from '@core/lb/repservice.service';
import { SafeResourceUrl, DomSanitizer } from '@angular/platform-browser';
import { ReuseTabService } from '@delon/abc/reuse-tab';

@Component({
  selector: 'app-tyjb-cjpr',
  templateUrl: './cjpr.component.html',
})
export class TyjbCjprComponent implements OnInit {
  formdata: {};
  dad069=0;
  dad017;
  dae001=0;
  show='1';
  dataset= []
  printshow = ''
  inpara :any;
  dah001

  
  @ViewChild('st', { static: false }) st: GridComponent;
  @ViewChild('sbxx', { static: false }) sbxx: SFComponent;
  params = { sname: 'PZML_QUERY', queryparas: { DAD069:this.dad069,DAE001:this.dae001 } };
  columns: STColumn[] = [
    { title: '凭证名称', index: 'DAH002' },
    { title: '报表ID', index: 'REPID' },
    { title: '报表参数', index: 'INPARA' },
    {
      title: '操作区',
      buttons: [
        {
          text :'打印',
          click: (record: any) => this.print(record),
        },
      ],
    }
  ];

  cjxxSchema: SFSchema = {
    properties: {
      DAD065: {
        type: 'string',
        title: '',
        enum: [
          { label: '自取', value: '1' },
          { label: '邮寄', value: '2' },
          { label: '短信', value: '3' },
          { label: '无需送达', value: '9' },
        ],
        // tslint:disable-next-line: no-object-literal-type-assertion
        ui: {
          widget: 'radio',
          // asyncData: () => of().pipe(delay(100)),
          change: console.log,
          grid: {
            span: 24,
          },
        } as SFRadioWidgetSchema,
        default: '1',
      },
      DAD088: {
        type: 'string',
        title: '快递单号',
        ui: { visibleIf: { DAD065: ['2'] } },
      },
      DAD132: {
        type: 'string',
        title: '联系人电话',
        ui: { visibleIf: { DAD065: ['3'] } },
      },
      DAD069: {
        type: 'string',
        title: '申报号',
        ui:{hidden:true},
      },
      DAE001: {
        type: 'string',
        title: '子事项编码',
        ui:{hidden:true},
      },
      DAD017: {
        type: 'string',
        title: '事项编码',
        ui:{hidden:true},
      },
      DAH001: {
        type: 'string',
        title: '凭证编码',
        ui:{hidden:true},
      },

    },
    ui: {
      spanLabelFixed: 100,
      grid: {
        span: 16,
      },
    },


  };




  constructor(private http: _HttpClient, private modal: ModalHelper,
    private route: ActivatedRoute,
    private lbservice: HttpService,
    public msgSrv: NzMessageService,
    private sanitizer: DomSanitizer,
    private reuseTabService: ReuseTabService,
    private repservice: RepserviceService,) { 
      this.reuseTabService.title = '出件';
    }
    ifrmeee: SafeResourceUrl;

  ngOnInit() { 
    this.route.queryParams.subscribe(param=>{
      // tslint:disable-next-line: no-string-literal
      // tslint:disable-next-line: radix
      this.dad069= parseInt(param['dad069']);
      this.dad017= parseInt(param['dad017']);
      this.dae001= parseInt(param['dae001']);
      
    });
  }
  // tslint:disable-next-line: use-lifecycle-interface
  ngAfterViewInit() {
    this.query();
  }

  add() {
    // this.modal
    //   .createStatic(FormEditComponent, { i: { id: 0 } })
    //   .subscribe(() => this.st.reload());
  }
  query(){
    this.lbservice
      .lbservice('CJXX_QUERY', { para: { DAD069: this.dad069 } })
      .then(resdata => {
        if (resdata.code < 1) {
          this.msgSrv.error(resdata.errmsg);
        } else {
          this.dataset = resdata.message.list;
          console.log(this.dataset);
          this.sbxx.setValue('/DAD065', this.dataset[0].DAD065);
          this.sbxx.setValue('/DAD088', this.dataset[0].DAD088);
          this.sbxx.setValue('/DAD132', this.dataset[0].DAD132);
        }
      });
  }

  cj_add(){
    this.sbxx.setValue("/DAD069",this.dad069);
    this.sbxx.setValue("/DAE001",this.dae001);
    this.sbxx.setValue("/DAD017",this.dad017);
    /*
    this.lbservice
      .lbservice('CJXX_ADD', { para: this.sbxx.value  })
      .then(resdata => {
        if (resdata.code < 1) {
          this.msgSrv.error(resdata.errmsg);
        } else {
          this.dataset = resdata.message.list;
          
        }
      });
      */
    console.log(this.sbxx.value)
    this.st.reload(this.sbxx.value);
    this.formdata = this.sbxx.value;
    this.show= '';
    
  }
  print(record){
    this.dah001 = record.DAH001
    
    this.lbservice
      .lbservice('JFPZ_ADD', { para: {SBXX:this.formdata,DAH001:this.dah001}})
      .then(resdata => {
        if (resdata.code < 1) {
          this.msgSrv.error(resdata.errmsg);
        } else {
          this.dataset = resdata.message.list;
          this.printshow = '1';
          this.inpara = JSON.parse(record.INPARA)
          this.repservice.getRep(record.REPID,this.inpara).then(resdata => {
            if (resdata != null) {
              this.ifrmeee = this.sanitizer.bypassSecurityTrustResourceUrl(
                String(resdata),
              );
            } else {
              alert('不存在报表');
            }
          });
        }
      });


    
  }

}
