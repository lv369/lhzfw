import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { TyjbCjprComponent } from './cjpr.component';

describe('TyjbCjprComponent', () => {
  let component: TyjbCjprComponent;
  let fixture: ComponentFixture<TyjbCjprComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TyjbCjprComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TyjbCjprComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
