import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { TyjbTysjCkslComponent } from './tysj-cksl.component';

describe('TyjbTysjCkslComponent', () => {
  let component: TyjbTysjCkslComponent;
  let fixture: ComponentFixture<TyjbTysjCkslComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TyjbTysjCkslComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TyjbTysjCkslComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
