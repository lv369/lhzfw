import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { TyjbTycjComponent } from './tycj.component';

describe('TyjbTycjComponent', () => {
  let component: TyjbTycjComponent;
  let fixture: ComponentFixture<TyjbTycjComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TyjbTycjComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TyjbTycjComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
