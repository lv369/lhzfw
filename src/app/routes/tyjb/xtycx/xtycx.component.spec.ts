import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { TyjbXtycxComponent } from './xtycx.component';

describe('TyjbXtycxComponent', () => {
  let component: TyjbXtycxComponent;
  let fixture: ComponentFixture<TyjbXtycxComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TyjbXtycxComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TyjbXtycxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
