import { Component, OnInit, ViewChild, EventEmitter, Output } from '@angular/core';
import { _HttpClient, ModalHelper } from '@delon/theme';
import { STColumn, STComponent, STColumnTag, STColumnBadge } from '@delon/abc/table';
import { SFSchema, SFComponent } from '@delon/form';
import { NzMessageService } from 'ng-zorro-antd';
import { HttpService, SupDic, GridComponent } from 'lbf';
import { TyekouQueryComponent } from '@shared/components/tyjb/tyekou_query';

@Component({
  selector: 'app-tyjb-xtycx',
  templateUrl: './xtycx.component.html',
})
export class TyjbXtycxComponent implements OnInit {
  addSchema: SFSchema
  isDoBj = false; // 办结弹出框
  dataAYW043:number = 0;
  querySchema: SFSchema = {
    properties: {
     
      ZTBH: { type: 'string', title: '主体编号' },
      ZTMC: { type: 'string', title: '主体名称' },
      AYW009: {
        type: 'number',
        title:'业务流水号',
        ui: { widgetWidth: 230 }
      }
    },
    ui:{
      grid:{
        span:6
      }
    }
  }
  

  // 主体对象类别渲染标签 AYW004
  TAG: STColumnTag = {
    '1': { text: '确认申请主体', color: 'magenta' },
    '2': { text: '申请表单完成', color: 'blue' },
    // '3': { text: '待调解', color: 'orange' },
    '4': { text: '待受理', color: 'cyan' },
    '5': { text: '作废', color: 'red' },
    '6': { text: '受理表单完成', color: 'lime' },
    '7': { text: '待调解', color: 'orange' },
    '8': { text: '不予受理', color: 'red' },
    '9': { text: '调解成功', color: 'green' },
    '10': { text: '调解失败', color: 'red' },
    '11': { text: '事项已转交', color: 'purple' }
  };
/*
  1	1	确认申请主体
10	3	调解失败
11	3	事项已转交
2	1	表单已确认
4	1	申请成功
5	1	作废办结
6	2	表单已确认
7	2	受理通过
8	2	不予受理
9	3	调解成功
*/

  BADGE: STColumnBadge = {
    '1': { text: '单位', color: 'success' },
    '2': { text: '个人', color: 'processing' },
  };

  curParamId = -1;

  bjSchema:SFSchema = {
    properties: {
      AYW043: {
        type: 'number',
        title: '主键',
        
        ui: {
          hidden: true,
          grid: {
            span: 24
          }
        },

      },
      AAE013: {
        type: 'string',
        title: '办结说明',
        ui: {
          widget: 'textarea',
          autosize: { minRows: 2, maxRows: 6 },

          grid: {
            span: 24
          }
        }
      },
    },
    ui: {
      spanLabelFixed: 100,
      grid: {
        span: 24,
      },
    },
    required: ['AAE013']
  
}


  // 列表
  grColumns: STColumn[] = [    
    // { title: '流水号', index: 'AYW009',width:70 },
    // { title: '协同请求接受时间', index: 'AYW022',width:100},   
    // { title: '审核时间', index: 'AYW131',width:100 },    
    // { title: '结果说明', index: 'AYW029',width:100 },

    { title: '流水号', index: 'AYW009',width:70 },
    { title: '协同请求接受时间', index: 'AYW022',width:100}, 
    { title: '纠纷事项', index: 'AYW001',dic:'AYW001',width:100},
    { title: '当前状态', index: 'AYW034',type: 'tag', tag: this.TAG,width:100},
    { title: '最近操作时间', index: 'AAE035',width:100},   
    { title: '对象主体', index: 'ZTLX', type: 'badge', badge: this.BADGE,width:100},    
    { title: '主体编号', index: 'ZTBH',width:100 },
    { title: '主体名称', index: 'ZTMC',width:100 },    
    { title: '乡镇街道', index: 'CAA001',dic:'CAA001',width:100 },
    { title: '村社区', index: 'AAE025',dic:'AAE025',width:100 },  
    { title: '详细地址', index: 'ADDR',width:100 },  
    { title: '手机号码', index: 'PHONE',width:100 },      
    {
      title: '操作区',
      fixed: 'right',
      width: 80,
      buttons: [
        {
          text: '查看详情',
          click: (record: any) => {
            this.queryInfo.AYW009 = record.AYW009;
          },
        },
        {
          text: record=>{
            if(record.AYW023==='4'){
              return '已办结'
            }
            else{
              return '我要办结'
            }
            
          },
          click: (record) => {
             // this.bjForm.reset();
             // this.bjForm.setValue('/AYW043',record.AYW043);
             this.dataAYW043 = record.AYW043;
             this.openWindow();
          },
          iif: record => record.AYW023 !== '4',
          iifBehavior: 'disabled',
        }
      ],
    },
  ];
  // 详情弹出框
  @ViewChild('queryInfo', { static: false }) queryInfo: TyekouQueryComponent;
  
  
 
  @Output() clickItem = new EventEmitter<string>();


  // 列表
  @ViewChild('grGrid', { static: false }) grGrid: GridComponent;
  // 查询表单
  @ViewChild('queryForm', { static: false }) queryForm: SFComponent;
   // 办结表单
   @ViewChild('bjForm', { static: false }) bjForm: SFComponent;
  
  // 人员列表查询
  doQuery(pi1=false) {
    if(pi1){
      this.grGrid.pi=1;
    }
    this.grGrid.reload(this.queryForm.value);
  }

    


 
  
  constructor(
    private lbservice: HttpService,
    private supDic: SupDic,
    public msgSrv: NzMessageService,
  ) {
    
  }

  closeWindow(){
    this.isDoBj = false;
  }

  openWindow(){
    this.isDoBj = true;
  }

  doBJ(){
    if(!this.bjForm.valid){
      this.msgSrv.error('办结说明不能为空！');
      return;
    }

    this.lbservice.lbservice('XTBJ_SAVE',{para:this.bjForm.value,AYW043:this.dataAYW043}).then(resdata=>{
      if(resdata.code<1)
      {
        this.msgSrv.error(resdata.errmsg);
      }
      else
      {
        this.msgSrv.success('办结成功');
        this.closeWindow();
        this.doQuery();  
      }
      
   })
  }

  ngOnInit() { 
    this.todoInit();    
  }

  _onReuseInit(){
    this.todoInit();
  }

  todoInit(){
    // 去获取是否有传递参数
    this.lbservice.lbservice('PUBLIC_INIT',{}).then(resdata=>{
      if(resdata.code<1)
      {
        this.msgSrv.error(resdata.errmsg);
      }
      else
      {
        // 判断是否有初始化数据
        if(resdata.message.AYW009!==undefined){
          this.curParamId = resdata.message.AYW039;
          this.queryForm.setValue('/AYW009',resdata.message.AYW009)
          this.doQuery();  
        }
        // 如果没有参数传递，默认打开首页
        else{
          if(this.curParamId<0){
            this.doQuery();    
          }
                 
        }
      }
      
   })
  }


}
