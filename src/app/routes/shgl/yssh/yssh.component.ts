import { Component, OnInit, ViewChild } from '@angular/core';
import { SFSchema, SFComponent } from '@delon/form';
import { STColumn, STColumnTag } from '@delon/abc';
import { GridComponent,SupDic,HttpService } from 'lbf';
import { NzMessageService } from 'ng-zorro-antd';
import { TyekouQueryComponent } from '@shared/components/tyjb/tyekou_query';
import { RefService } from '@core/lb/RefService';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-yssh',
  templateUrl: './yssh.component.html',
  styleUrls: ['./yssh.component.less']
})
export class YsshComponent implements OnInit {
  // 详情弹出框
  @ViewChild('queryInfo', { static: false }) queryInfo: TyekouQueryComponent; 
  params = {sname: 'SHGL_YSCX', queryparas: {}};
  searchSchema:SFSchema = {
    properties: {
      AYW009: {
        title:'业务流水号',
        type:'number'
      },
      AYW057: {
        title:'审核意见',
        type:'string',
        enum: [{ label: '未审核', value: '2' },{ label: '同意', value: '1' }, { label: '不同意', value: '0' }],
        ui: {
          widget: 'radio'
        }
      },
      KSSJ: { 
        type: 'string', 
        title: '申请开始时间',
        ui: { widget: 'date' } 
      },
      JSSJ: { 
        type: 'string', 
        title: '申请结束时间',
        ui: { widget: 'date' } 
      },
      
    }
  }

  // 审核弹框相关
  isVisible = false;
  shFormData = {};
  shSchema: SFSchema ={
    properties:{
      AYW053: {
        title:'主键',
        type:'number',
        ui: {
          hidden: true
        }
      },
      AYW009: {
        title: '业务流水号',
        type: 'number',
        ui: {hidden: true}
      },
      AYW054: {
        title: '延长时限',
        type: 'number'
      },
      AYW006: {
        title: '时限单位',
        type:'string'
      },
      AYW055: {
        title: '申请时间',
        type:'string',
        readOnly: true
      },
      AAE011:{
        title: '申请人',
        type:'string',
        enum: this.supdic.getSFDic('USERID'),
        ui: {
          widget: 'select',
          
        },
        readOnly: true
      },
      AYW059:{
        title: '申请原因说明',
        type: 'string',
        readOnly: true,
        ui: {
          widget: 'textarea',
          autosize: { minRows: 3, maxRows: 10 },
          
            grid: {
              span:24
            
          }
        }
      },
      AYW056: {
        title: '审核时间', 
        type:'string',
        readOnly: true,
        ui: {
          grid: {
            span:24
          }
        }
      },
      AYW057: {
        title:'审核意见',
        type:'string',
        enum: [ { label: '同意', value: '1' }, { label: '不同意', value: '0' }],
        ui: {
          widget: 'radio',
          grid: {
            span:24
          }
        }
      },
      AYW058:{
        type: 'string',
        title: '审核意见说明',
        ui: {
          widget: 'textarea',
          autosize: { minRows: 3, maxRows: 10 },
          
            grid: {
              span:24
            
          }
        }
      }
    },
    ui: {
      spanLabelFixed: 100,
      grid: {
        span: 12,
      },
    },
    required: ['AYW057','AYW054','AYW006']
  }

  TAG: STColumnTag = {
    '2': { text: '未审核', color: 'orange' },
    '1': { text: '同意', color: 'green' },
    '0': { text: '不同意', color: 'red' }
  };

  columns:STColumn[] = [
    {title: '业务流水号',index: 'AYW009',type:'number'},
    { title: '审核意见', index: 'AYW057',type: 'tag', tag: this.TAG},
    {title: '延长时限',type: 'number',index:'AYW054'},
    {title: '时限单位',index: 'AYW006'},
    {title:'申请时间',index:'AYW055'},
    {title: '申请人',index:'AAE011',dic:'USERID'},
    {title:'申请原因',index:'AYW059'},
    {title:'审核时间',index:'AYW056'},
    {
      title: '操作区',
      buttons: [
        {
          text:'事项详情',
          click: (record) => {
            this.queryInfo.AYW009 = record.AYW009;
          }
        },
        {
          text:'审核',
          click: (record) => this.sh(record)
        }
      ]
    }
  ]
  
  curParamId = -1;
  @ViewChild('st',{static: false}) st: GridComponent;
  @ViewChild('sf',{static:false}) sf:SFComponent;
  @ViewChild('shsf',{static: false}) shsf:SFComponent;
  private refSub: Subscription;
  constructor(private msgSrv:NzMessageService,private ref: RefService,private supdic: SupDic,private httpService: HttpService) { }

  ngOnInit() { 
    console.log('-------------ngOnInit111----------')
    // this.todoInit();    
  }

  _onReuseInit(){
    console.log('-------------_onReuseInit----------')
    // this.todoInit();
  }

  ngAfterViewInit(){
    this.refSub = this.ref.getRef('shgl').subscribe(msg => {
      console.log(msg);
      this.sf.setValue('/AYW009',msg.message.AYW009)
      this.query();  
    })
  }

  ngOnDestroy(): void {
    this.refSub.unsubscribe();
  }

  todoInit(){
    // 去获取是否有传递参数
    this.httpService.lbservice('PUBLIC_INIT',{}).then(resdata=>{
      if(resdata.code<1)
      {
        this.msgSrv.error(resdata.errmsg);
      }
      else
      {
        console.log('-------------init----------')
        console.log(resdata)
        // 判断是否有初始化数据
        if(resdata.message.AYW009!==undefined){
          this.curParamId = resdata.message.AYW039;
          this.sf.setValue('/AYW009',resdata.message.AYW009)
          this.query();  
        }
        // 如果没有参数传递，默认打开首页
        else{
          if(this.curParamId<0){
            this.query();    
          }
                 
        }
      }
      
   })
  }


  query(){
    this.st.reload(this.sf.value);
  }

  sh(record){
    console.log(record)
    this.shFormData = record;
    this.isVisible = true
  }

  save(value){

    // 必输项校验
    if(!this.shsf.valid){
      this.msgSrv.error("表单必输项不能为空");
      return;
    }
    
    this.httpService.lbservice('SHGL_YSSHTG',{para: value}).then(res => {
      if(res.code > 0){
        this.msgSrv.info("审核成功");
        this.isVisible = false;
        this.query();
      }
      else{
        this.msgSrv.error(res.errmsg);
      }
    })
  }

  handleCancel(){
    this.isVisible = false;
  }

}
