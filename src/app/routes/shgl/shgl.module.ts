import { NgModule } from '@angular/core';
import { SharedModule } from '@shared';
import { ShglRoutingModule } from './shgl-routing.module';
import { YsshComponent } from './yssh/yssh.component';
import { QbshComponent } from './qbsh/qbsh.component';
import { GhbmshComponent } from './ghbmsh/ghbmsh.component';
import { ShglBbdyComponent } from './bbdy/bbdy.component';

const COMPONENTS = [
  ShglBbdyComponent];
const COMPONENTS_NOROUNT = [];

@NgModule({
  imports: [
    SharedModule,
    ShglRoutingModule
  ],
  declarations: [
    ...COMPONENTS,
    ...COMPONENTS_NOROUNT,
    YsshComponent,
    QbshComponent,
    GhbmshComponent
  ],
  entryComponents: COMPONENTS_NOROUNT
})
export class ShglModule { }
