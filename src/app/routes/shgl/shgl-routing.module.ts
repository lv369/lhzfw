import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { YsshComponent } from '../shgl/yssh/yssh.component';
import { QbshComponent } from '../shgl/qbsh/qbsh.component';
import { GhbmshComponent } from '../shgl/ghbmsh/ghbmsh.component';
import { ShglBbdyComponent } from './bbdy/bbdy.component';

const routes: Routes = [
  { path: 'yssh', component: YsshComponent },
  { path: 'qbsh', component: QbshComponent },
  { path: 'ghbm', component: GhbmshComponent },
  { path: 'bbdy', component: ShglBbdyComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ShglRoutingModule { }
