import { Component, OnInit, ViewChild } from '@angular/core';
import { SFSchema, SFComponent } from '@delon/form';
import { STColumn, STColumnTag } from '@delon/abc';
import { GridComponent, SupDic, HttpService } from 'lbf';
import { NzMessageService } from 'ng-zorro-antd';
import { TyekouQueryComponent } from '@shared/components/tyjb/tyekou_query';
import { RefService } from '@core/lb/RefService';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-yssh',
  templateUrl: './qbsh.component.html',
  styleUrls: ['./qbsh.component.less']
})
export class QbshComponent implements OnInit {
  // 详情弹出框
  @ViewChild('queryInfo', { static: false }) queryInfo: TyekouQueryComponent;
  params = { sname: 'QBSH_QSHXX', queryparas: {} };
  searchSchema: SFSchema = {
    properties: {
      AYW009: {
        title: '业务流水号',
        type: 'number',
        ui: { widgetWidth: 200 }
      },
      SQSHJG: {
        title: '审核结果',
        type: 'string',
        enum: [{ label: '未审核', value: '2' }, { label: '同意', value: '1' }, { label: '不同意', value: '0' }],
        ui: {
          widget: 'radio'
        }
      },
      SQZT: {
        type: 'string',
        title: '申请类别',
        enum: this.supdic.getSFDic('SHLB'),
        ui: {
          widget: 'select',
          allowClear: true,
          width: 260,
        }
      },
      KSSJ: {
        type: 'string',
        title: '申请发起时间',
        ui: { widget: 'date' }
      },
      JSSJ: {
        type: 'string',
        title: '申请结束时间',
        ui: { widget: 'date' }
      },
    },
    ui: {
      grid: {
        span: 8,
      }
    }
  }

  // 审核弹框相关
  isVisible = false;
  shFormData = {};
  shSchema: SFSchema = {
    properties: {
      AYW053: {
        title: '主键',
        type: 'number',
        ui: {
          hidden: true
        }
      },
      AYW009: {
        title: '业务流水号',
        type: 'number',
        ui: { hidden: true }
      },
      AYW054: {
        title: '延长时限',
        type: 'number'
      },
      AYW006: {
        title: '时限单位',
        type: 'string'
      },
      AYW055: {
        title: '申请时间',
        type: 'string',
        readOnly: true
      },
      AAE011: {
        title: '申请人',
        type: 'string',
        enum: this.supdic.getSFDic('USERID'),
        ui: {
          widget: 'select',

        },
        readOnly: true
      },
      AYW059: {
        title: '申请原因说明',
        type: 'string',
        readOnly: true,
        ui: {
          widget: 'textarea',
          autosize: { minRows: 3, maxRows: 10 },

          grid: {
            span: 24

          }
        }
      },
      AYW056: {
        title: '审核时间',
        type: 'string',
        readOnly: true,
        ui: {
          grid: {
            span: 24
          }
        }
      },
      AYW057: {
        title: '审核意见',
        type: 'string',
        enum: [{ label: '同意', value: '1' }, { label: '不同意', value: '0' }],
        ui: {
          widget: 'radio',
          grid: {
            span: 24
          }
        }
      },
      AYW058: {
        type: 'string',
        title: '审核意见说明',
        ui: {
          widget: 'textarea',
          autosize: { minRows: 3, maxRows: 10 },

          grid: {
            span: 24

          }
        }
      }
    },
    ui: {
      spanLabelFixed: 100,
      grid: {
        span: 12,
      },
    },
    required: ['AYW057', 'AYW054', 'AYW006']
  }


  TAG: STColumnTag = {
    '4': { text: '已办结', color: 'green' },
    '3': { text: '已接受', color: 'green' },
    '2': { text: '未审核', color: 'orange' },
    '1': { text: '同意', color: 'green' },
    '0': { text: '不同意', color: 'red' }
  };

  SqlbTAG: STColumnTag = {
    '2': { text: '转交', color: 'purple' },
    '1': { text: '邀约', color: 'magenta' },
    '3': { text: '延时', color: 'cyan' }
  };

  columns: STColumn[] = [
    { title: '审核意见', index: 'SQSHJG', type: 'tag', tag: this.TAG },
    { title: '业务流水号', index: 'AYW009' },
    { title: '申请类别', index: 'SQZT', type: 'tag', tag: this.SqlbTAG },
    { title: '申请时间', index: 'SQFQSJ' },
    { title: '申请人', index: 'AYW030', dic: 'USERID' },
    { title: '申请信息', index: 'SQXX', render: 'SQXX' },
    { title: '审核时间', index: 'SHSJ' },
    { title: '审核人', index: 'SHR', dic: 'USERID' },
    {
      title: '操作区',
      buttons: [
        {
          text: '事项详情',
          click: (record) => {
            this.queryInfo.AYW009 = record.AYW009;
          }
        },
        {
          text: record => {
            if (record.SQSHJG !== '2') {
              return '已审核'
            }
            else {
              return '审核'
            }
          },
          click: (record) => this.sh(record),
          iif: record => record.SQSHJG === '2',
          iifBehavior: 'disabled',
        }
      ]
    }
  ]

  curParamId = -1;
  @ViewChild('st', { static: false }) st: GridComponent;
  @ViewChild('sf', { static: false }) sf: SFComponent;
  @ViewChild('shsf', { static: false }) shsf: SFComponent;
  private refSub: Subscription;
  constructor(private msgSrv: NzMessageService, private ref: RefService, private supdic: SupDic, private httpService: HttpService) { }

  ngOnInit() {
    console.log('-------------ngOnInit111----------')
    // this.todoInit();    
  }

  _onReuseInit() {
    console.log('-------------_onReuseInit----------')
    // this.todoInit();
  }

  ngAfterViewInit() {
    this.refSub = this.ref.getRef('shgl').subscribe(msg => {
      console.log(msg);
      this.sf.setValue('/AYW009', msg.message.AYW009)
      this.query();
    })
  }

  ngOnDestroy(): void {
    this.refSub.unsubscribe();
  }

  todoInit() {
    // 去获取是否有传递参数
    this.httpService.lbservice('PUBLIC_INIT', {}).then(resdata => {
      if (resdata.code < 1) {
        this.msgSrv.error(resdata.errmsg);
      }
      else {
        console.log('-------------init----------')
        console.log(resdata)
        // 判断是否有初始化数据
        if (resdata.message.AYW009 !== undefined) {
          this.curParamId = resdata.message.AYW039;
          this.sf.setValue('/AYW009', resdata.message.AYW009)
          this.query();
        }
        // 如果没有参数传递，默认打开首页
        else {
          if (this.curParamId < 0) {
            this.query();
          }

        }
      }

    })
  }


  query() {
    this.st.reload(this.sf.value);
  }

  sh(record) {
    // this.shFormData = record;
    if (record.SQZT === '1') {
      this.initYySchema(record);
      this.nowSqzt = '1';
    } else if (record.SQZT === '2') {
      this.initZjShSchema(record);
      this.nowSqzt = '2';
    } else if (record.SQZT === '3') {
      this.initYsShSchema(record);
      this.nowSqzt = '3';
    }
    this.isVisible = true
  }
  nowSqzt = '';
  save(value) {

    // 必输项校验
    if (!this.shsf.valid) {
      this.msgSrv.error("表单必输项不能为空");
      return;
    }
    let sname = '';
    if (this.nowSqzt === '2') {
      sname = 'SHGL_GHBMSHTG';
    } else if (this.nowSqzt === '3') {
      sname = 'SHGL_YSSHTG';
    } else if (this.nowSqzt === '1') {
      console.log(this.shsf.value);
      this.yySave(this.shsf.value.DGB010, this.shsf.value.AYW043, this.shsf.value.AYW023);
      return;
    }
    this.httpService.lbservice(sname, { para: value }).then(res => {
      if (res.code > 0) {
        this.msgSrv.info("审核成功");
        this.isVisible = false;
        this.query();
      }
      else {
        this.msgSrv.error(res.errmsg);
      }
    })
  }

  handleCancel() {
    this.isVisible = false;
  }
  // 邀约受理
  yySave(dataDGB010: string, dataAYW043: number, dataAYW023: string) {
    console.log(dataAYW023);
    this.httpService.lbservice("TYJB_YY_DO", { DGB010: dataDGB010, AYW043: dataAYW043, AYW023: dataAYW023 }).then(resdata => {
      this.isVisible = false;
      if (resdata.code < 1) {
        this.msgSrv.error(resdata.errmsg);
      }
      else {
        this.msgSrv.success('邀约处理成功');
        this.st.st.reload();
      }
    });
  }

  /* *审核弹窗  */

  initYsShSchema(para) {
    const qtxx = JSON.parse(para.QTXX);
    this.shSchema = {
      properties: {
        AYW053: {
          title: '主键',
          type: 'number',
          default: para.PK,
          ui: {
            hidden: true
          }
        },
        AYW009: {
          title: '业务流水号',
          type: 'number',
          default: para.AYW009,
          ui: { hidden: true }
        },
        AYW054: {
          title: '延长时限',
          type: 'number',
          default: qtxx.AYW054,
        },
        AYW006: {
          title: '时限单位',
          type: 'string',
          enum: this.supdic.getSFDic('AYW006'),
          default: qtxx.AYW006,
        },
        AYW055: {
          title: '申请时间',
          type: 'string',
          readOnly: true,
          default: para.SQFQSJ,
        },
        AAE011: {
          title: '申请人',
          type: 'string',
          default: para.AYW030,
          enum: this.supdic.getSFDic('USERID'),
          ui: {
            widget: 'select',
          },
          readOnly: true
        },
        AYW059: {
          title: '申请原因说明',
          type: 'string',
          default: qtxx.AYW059,
          readOnly: true,
          ui: {
            widget: 'textarea',
            autosize: { minRows: 3, maxRows: 10 },
            grid: {
              span: 24
            }
          }
        },
        AYW056: {
          title: '审核时间',
          type: 'string',
          readOnly: true,
          ui: {
            grid: {
              span: 24
            }
          }
        },
        AYW057: {
          title: '审核意见',
          type: 'string',
          enum: [{ label: '同意', value: '1' }, { label: '不同意', value: '0' }],
          ui: {
            widget: 'radio',
            grid: {
              span: 24
            }
          }
        },
        AYW058: {
          type: 'string',
          title: '审核意见说明',
          ui: {
            widget: 'textarea',
            autosize: { minRows: 3, maxRows: 10 },

            grid: {
              span: 24

            }
          }
        }
      },
      ui: {
        spanLabelFixed: 100,
        grid: {
          span: 12,
        },
      },
      required: ['AYW057', 'AYW054', 'AYW006']
    }
  }

  initZjShSchema(para) {
    const qtxx = JSON.parse(para.QTXX);
    console.log(qtxx);
    this.shSchema = {
      properties: {
        AYW053: {
          title: '主键',
          type: 'number',
          default: para.PK,
          ui: {
            hidden: true
          }
        },
        AYW009: {
          title: '业务流水号',
          type: 'number',
          default: para.AYW009,
          readOnly: true
        },
        AYW001: {
          title: '转交事项',
          type: 'string',
          //enum: this.supdic.getSFDic('AYW001'),
          default: qtxx.AYW001,
          readOnly: true
        },
        DGB010: {
          title: '转交分中心',
          type: 'string',
          default: qtxx.DGB010,
          enum: this.supdic.getSFDic('DGB020'),
          ui: {
            widget: 'select',

          },
          readOnly: true
        },
        AYW055: {
          title: '申请时间',
          type: 'string',
          default: para.SQFQSJ,
          readOnly: true,
        },
        AAE011: {
          title: '申请人',
          type: 'string',
          default: para.AYW030,
          enum: this.supdic.getSFDic('USERID'),
          ui: {
            widget: 'select',

          },
          readOnly: true
        },
        AYW059: {
          title: '申请原因说明',
          type: 'string',
          default: qtxx.AYW059,
          readOnly: true,
          ui: {
            widget: 'textarea',
            autosize: { minRows: 3, maxRows: 10 },
            grid: {
              span: 24
            }
          }
        },
        AYW056: {
          title: '审核时间',
          type: 'string',
          readOnly: true,
          ui: {
            grid: {
              span: 24
            }
          }
        },
        AYW057: {
          title: '审核意见',
          type: 'string',
          enum: [{ label: '同意', value: '1' }, { label: '不同意', value: '0' }],
          ui: {
            widget: 'radio',
            grid: {
              span: 24
            }
          }
        },
        AYW058: {
          type: 'string',
          title: '审核意见说明',
          ui: {
            widget: 'textarea',
            autosize: { minRows: 3, maxRows: 10 },

            grid: {
              span: 24

            }
          }
        }
      },
      ui: {
        spanLabelFixed: 100,
        grid: {
          span: 12,
        },
      },
      required: ['AYW057']
    }
  }

  initYySchema(para) {
    const qtxx = JSON.parse(para.QTXX);
    this.shSchema = {
      properties: {
        AYW043: {
          title: '主键',
          type: 'number',
          default: para.PK,
          ui: {
            hidden: true
          }
        },
        AYW009: {
          title: '业务流水号',
          type: 'number',
          default: para.AYW009,
          readOnly: true
        },
        DGB010: {
          title: '邀约中心',
          type: 'string',
          default: qtxx.DGB010,
          enum: this.supdic.getSFDic('BMMC'),
          ui: {
            widget: 'select',

          },
          readOnly: true
        },
        AYW021: {
          title: '申请时间',
          type: 'string',
          default: para.SQFQSJ,
          readOnly: true,
        },
        AYW030: {
          title: '申请人',
          type: 'string',
          default: para.AYW030,
          enum: this.supdic.getSFDic('USERID'),
          ui: {
            widget: 'select',

          },
          readOnly: true
        },
        AYW023: {
          title: '审核意见',
          type: 'string',
          enum: [{ label: '同意', value: '1' }, { label: '不同意', value: '0' }],
          ui: {
            widget: 'radio',
            grid: {
              span: 24
            }
          }
        },
      },
      ui: {
        spanLabelFixed: 100,
        grid: {
          span: 12,
        },
      },
      required: ['AYW023']
    }
  }


}
