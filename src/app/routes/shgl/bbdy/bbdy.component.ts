import { Component, OnInit, ViewChild } from '@angular/core';
import { _HttpClient, ModalHelper } from '@delon/theme';
import { STColumn, STComponent, STColumnTag } from '@delon/abc/table';
import { SFSchema, SFComponent, SFDateWidgetSchema } from '@delon/form';
import { GridComponent, SupDic } from 'lbf';
import { TyekouQueryComponent } from '@shared/components/tyjb/tyekou_query';

@Component({
  selector: 'app-shgl-bbdy',
  templateUrl: './bbdy.component.html',
})
export class ShglBbdyComponent implements OnInit {

 
  params = { sname: 'query_list', form: {} };
  schema: SFSchema = {
    properties: {
      AYW009: {
        type: 'string',
        title: '流水号'
      },
      ZTMC: {
        type: 'string',
        title: '主题名称'
      },
      SJRQ: {
        type: 'string',
        title:'收件时间',
  
        ui: { widget: 'date', end: 'SLRQ',format:'YYYY/MM/DD'  },
      },
      SLRQ: {
        type: 'string',
        ui: { widget: 'date', end: 'SLRQ',format:'YYYY/MM/DD'  },
      },
      BLRQ: {
        type: 'string',
        title:'办结时间',
      
        ui: { widget: 'date', end: 'BJRQ', format:'YYYY/MM/DD' },
      },
      BJRQ: {
        type: 'string',
        ui: { widget: 'date', end: 'BJRQ',format:'YYYYMMDD' },
      },
      SLBM:{
         title:'所属部门',
         type:'string',
         enum: this.supDic.getSFDic('MYBM'),
        
         ui: {
           widget: 'select',
           allowClear: true,
         },
      },
      TNAME:{
        title:'操作员',
        type:'string'
      }


    },ui:{
       grid:{span:8},
       spanLabelFixed:150
    }
  };
  @ViewChild('st', { static: false }) st: GridComponent;
  @ViewChild('sf', { static: false }) sf: SFComponent;
    // 详情弹出框
    @ViewChild('queryInfo', { static: false }) queryInfo: TyekouQueryComponent;
  TAG: STColumnTag = {
    1: { text: '单位', color: 'blue' },
    2: { text: '个人', color: 'blue' },
  };
  TAG1: STColumnTag = {
    0: { text: '未办结', color: 'red' },
    1: { text: '已办结', color: 'green' },
   
  };

  TAG2: STColumnTag = {
    '1': { text: '确认申请主体', color: 'magenta' },
    '2': { text: '申请表单完成', color: 'blue' },
    '4': { text: '待受理', color: 'cyan' },
    '5': { text: '作废', color: 'red' },
    '6': { text: '受理表单完成', color: 'lime' },
    '7': { text: '待办理', color: 'orange' },
    '8': { text: '不予受理', color: 'red' },
    '9': { text: '办理成功', color: 'green' },
    '10': { text: '办理失败', color: 'red' },
    '11': { text: '事项已转交', color: 'purple' }
  };

 

  columns: STColumn[] = [
    { title: '流水号', index: 'AYW009' },
    { title: '申请类型', index:'ZTLX', 
    // badge: {
    //   1: { text: 'Processing', color: 'processing' },
    //   2: { text: 'Warning', color: 'warning' },
    // },
    tag:this.TAG, type:'tag',className:'text-center',
    filter: {
      menus: [{ text: '自然人', value: '2' }, { text: '单位,企业', value: '1' }],
      multiple: true,
    },
  },
    { title: '办结状态', index: 'BJZT',type:'tag',tag:this.TAG1,className:'text-center',
    filter: {
    menus: [{ text: '已办结', value: '1' }, { text: '未办结', value: '0' }],
    multiple: true,
   
    },

  },
    { title: '主题名称',index: 'ZTMC',className:'text-center'},
    { title: '所属分中心', index: 'SLFZX',dic:'ZX', className:'text-center'},
    { title: '所属部门', index: 'SLBM',dic:'DGB040', className:'text-center',},
    { title: '收件时间', index: 'SJRQ',type:'date',className:'text-center'},
    { title: '受理时间', index: 'SLRQ',type:'date'},
    { title: '办理时间', index: 'BLRQ',type:'date'},
    { title: '当前状态', index: 'AYW034',type:'tag', tag:this.TAG2},
    { title: '办结时间', index: 'BJRQ',type:'date',},
     { title: '来访人数', index: 'AYW016',className:'text-center',
     dic:'AYW016'
     // type:'badge',
    // badge: {
    //   1: { text: '1', color: 'default' },
    //   2:{ text: '2~5',color: 'default' },
    //    3:{ text: '6~10', color: 'default'  },
    //    4:{ text: '11~20',color: 'default' },
    //   5: { text: '21~50', color: 'default' },
    //   6: { text: '51~100',  color: 'default' },
    //   7:{ text: '100以上',  color: 'default' }
    // },
    // filter: {
    //   menus: [{ text: '已办结', value: '1' }, { text: '未办结', value: '0' }],
    //   multiple: true,
     
    //   },
  
  },
    { title: '操作员', index: 'TNAME',className:'text-center'},

    {
      title: '操作区',
      fixed: 'right',
      width: 80,
      buttons: [
        {
          text: '查看详情',
          click: (record: any) => {
            this.queryInfo.AYW009 = record.AYW009;
          },
        },
      ]}
   

  ];

  constructor(private http: _HttpClient, private modal: ModalHelper, private supDic: SupDic,) { }


  ngOnInit() { }

  getdata() {
    this.st.reload(this.sf.value);
  
  }

}
