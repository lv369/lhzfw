import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ShglBbdyComponent } from './bbdy.component';

describe('ShglBbdyComponent', () => {
  let component: ShglBbdyComponent;
  let fixture: ComponentFixture<ShglBbdyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShglBbdyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShglBbdyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
