import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { _HttpClient, ModalHelper } from '@delon/theme';
import { STColumnTag, STColumn, STComponent, STColumnBadge } from '@delon/abc';
import { GridComponent, SupDic, HttpService } from 'lbf';
import { SFComponent, SFSchema } from '@delon/form';
import { NzMessageService } from 'ng-zorro-antd';
import {JgyjService} from '../jgyjservice/jgyj.service'
import { Subscription } from 'rxjs';
import { RefService } from '@core/lb/RefService';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-jgyj-sxyjzj',
  templateUrl: './sxyjzj.component.html',
  providers: [DatePipe],
})

export class JgyjSxyjzjComponent implements OnInit ,OnDestroy{
  queryFormData = {};  
  @ViewChild('st', { static: true }) st: GridComponent;
  @ViewChild('searchForm', { static: false }) searchForm: SFComponent;

  TAG: STColumnTag = {
    1: { text: '已完成', color: 'green' },
    0: { text: '未完成', color: 'red' },
  };

  searchSchema: SFSchema = {
    properties: {
      JGZ004: {
        type: 'string',
        title: '预警类型',
        enum: this.dic.getSFDic('JGZ004'),
        ui: {
          widget: 'select',
          allowClear:true,
        }
      },
      AYW025: {
        type: 'string',
        title: '环节',
        enum: this.dic.getSFDic('AYW025'),
        ui: {
          widget: 'select',
          allowClear:true,
        }
      },
      AYW002: {
        type: 'string',
        title: '事项名称',
      },
      AAE036:{
        type: 'string',
        title:'最后警告时间',
        ui: { widget: 'date' },
      },
      end: {
        type: 'string',
        title:'警告结束时间',
        ui: { widget: 'date'},
      },
    },
    ui: {
      spanLabelFixed: 100,
      grid: {
        span: 8
      }
    }
  };

  gridData = [];
  columns: STColumn[] = [
    { title: '环节状态', index: 'YJZ004' ,type:'tag',tag:this.TAG }, 
    { title: '业务流水号', index: 'AYW009'}, 
    // { title: '预警类型', index: 'JGZ004' ,render:'JG02Tag'},
    { title: '对应事项', index: 'AYW002' ,dic: 'DAD017' },
    { title: '对应环节', index: 'AYW025' ,dic: 'AYW025' },
    { title: '开始时间', index: 'AYW027' },
    { title: '应完成时间', index: '' ,format:(item,col,idx)=>{return this.getEndDate(item.AAE037,item.YJZ003)} },  
    { title: '操作员', index: 'AYW030' ,dic: 'USERID' },  
    { title: '最后预警时间', index: 'AAE036' ,type:'date' },  
    { title: '操作区', index: '' ,render:"gotoURL",fixed:'right'},  
  ];
  private refSub: Subscription;
  getEndDate(AAE037,YJZ003){
    const date=new Date(AAE037);
    date.setTime(date.getTime()-YJZ003*60*1000);
     return this.datePipe.transform(date,'yyyy-MM-dd HH:mm:ss')
    }

  gotoURL(V_AYW009,V_AYW025){
    const obj={AYW009:V_AYW009};
    let goURL='';
    let rece='';
    if(V_AYW025==='2'){// 受理
      goURL='/tyjb/tysl-new';
      rece='tysl';
    }else if(V_AYW025==='3'){// 办理
      goURL='/tyjb/tybl-new';
      rece='tybl';
    }else{
      this.msgSrv.error('此环节暂不支持跳转');
      return;
    }
    this.ref.sendRef({receive:'shgl',message:obj,sender:'message',createTime:new Date(),isGoto:true,url:goURL})
  }

  add(){
   


    // 办理
    this.lbs.lbservice('sxyj_sxyj', {  }).then(resdata => {
      // console.log(this.addWindow.value);
      if (resdata.code < 1) {
        this.msgSrv.error(resdata.errmsg);
      }
      else {
        this.msgSrv.success('刷新成功');
        this.query();
      }
    })  

     // 受理
    //  this.lbs.lbservice('sxyj_sxyjsj', {  }).then(resdata => {
    //   // console.log(this.addWindow.value);
    //   if (resdata.code < 1) {
    //     this.msgSrv.error(resdata.errmsg);
    //   }
    //   else {
    //     this.msgSrv.success('刷新成功');
    //     this.query();
    //   }
    // })  
   
  }
  
  query(pi1=false,pin=1) {
    if(this.searchForm.value.AAE036!==undefined&&this.searchForm.value.AAE036.length>0&&this.searchForm.value.AAE036[0]==='I'){
      this.searchForm.value.AAE036='';
    }
    if(this.searchForm.value.end!==undefined&&this.searchForm.value.end.length>0&&this.searchForm.value.end[0]==='I'){
      this.searchForm.value.end='';
    }
    if(this.searchForm.value.AAE036!==undefined&&this.searchForm.value.AAE036.length>10){
      this.searchForm.value.AAE036=this.searchForm.value.AAE036.substring(0,10);
    }
    if(this.searchForm.value.end!==undefined&&this.searchForm.value.end.length>10){
      this.searchForm.value.end=this.searchForm.value.end.substring(0,10);
    }
    if(pi1){
      this.st.pi=pin;
    }
    this.st.reload(this.searchForm.value);
   


  }

  constructor(private http: _HttpClient,
    private dic: SupDic,
    private lbs: HttpService,
    public msgSrv: NzMessageService,
    private jgService:JgyjService,
    private ref: RefService,
    private datePipe:DatePipe
    ) { }

  ngOnInit() { 
  }

  // tslint:disable-next-line: use-lifecycle-interface
  ngAfterViewInit() {
    this.refSub = this.ref.getRef('shgl').subscribe(msg => {
      const obj=msg.message;
      this.st.reload(obj);
    })
    // this.query();
  }

  ngOnDestroy(): void {
    this.refSub.unsubscribe();
  }

  toDate(para){
    let str:string="";
    if(para<0){
      str='- '+str;
    }
    let day=0;
    let hour=0;
    let min=0;
    if(Math.abs(para)>(60*24)){
      day=Math.floor(para/(60*24));
      para=para%(60*24);
    }
    if(Math.abs(para)>(60)){
      hour=Math.floor(para/60);
      para=para%60;
    }
    min=para;
    // 拼接字符串
    if(day!==0){
      day=Math.abs(day);
      str=str+day+' 天 ';
    }
    if(hour!==0){
      hour=Math.abs(hour);
      str=str+hour+' 小时 ';
    }
    if(min!==0){
      min=Math.abs(min);
      str=str+min+' 分'
    }
    return str;
  }


}
