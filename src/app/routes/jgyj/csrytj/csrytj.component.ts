import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { _HttpClient, ModalHelper } from '@delon/theme';
import { STColumn, STComponent, STColumnTag } from '@delon/abc/table';
import { SFSchema, SFComponent } from '@delon/form';
import { Chart } from '@antv/g2';
import { GridComponent, HttpService } from 'lbf';
import { NzMessageService } from 'ng-zorro-antd';

@Component({
  selector: 'app-jgyj-csrytj',
  templateUrl: './csrytj.component.html',
  styleUrls:['./csrytj.component.less']
})
export class JgyjCsrytjComponent implements OnInit {

  rankingListData: any[];
  constructor(private http: _HttpClient,
    private modal: ModalHelper,
    private lbs: HttpService,
    public msgSrv: NzMessageService) { }
  queryFormData = {};
  private chart: any;
  public num = '0';


  sname = {
    query: 'staffRank_query', // 超时人员查询1
    detail:'staff_query'// 超时人员查询2
  }
  data = [
    { item: '事例一', count: 40, percent: 0.4 },
    { item: '事例二', count: 21, percent: 0.21 },
    { item: '事例三', count: 17, percent: 0.17 },
    { item: '事例四', count: 13, percent: 0.13 },
    { item: '事例五', count: 9, percent: 0.09 },
  ];;

  @ViewChild('pie', { static: false }) pie: ElementRef;
  @ViewChild('st', { static: true }) st: GridComponent;


  TAG: STColumnTag = {
    1: { text: '正常', color: 'green' },
    2: { text: '将到期', color: 'orange' },
    3: { text: '黄牌', color: 'yellow' },
    4: { text: '红牌', color: 'red' },
  };

  gridData = [];
  columns: STColumn[] = [
    { title: '中心', index: 'DGB021' },
    { title: '部门', index: 'DGB041' },
    { title: '岗位', index: 'RNAME' },
    { title: '预警事件序号', index: 'YJZ001' },
    { title: '预警类型', index: 'JGZ004',type:'tag',tag:this.TAG },
    { title: '超限时长', index: 'YJZ003' ,format:(item,col,idx)=>{return this.toDate(item.YJZ003)} },
    { title: '对应事项', index: 'DAD031' },
    { title: '对应环节', index: 'AYW025',dic: 'AYW025' },
    { title: '办理时限', index: 'DAD101' ,format:(item,col,idx)=>{return this.toDate(item.DAD101)} },
    { title: '环节操作员', index: 'NAME' },
  ];  
  csxmzb: number;
  ln_num = 0;
 

  ngOnInit() {


    this.lbs.lbservice(this.sname.query, {}).then(res => {
      if (res.code > 0) {
        if (res.message.list.length === 0) return
        const arr = res.message.list
        const piedata = arr.map(value => {
          const temp = {
            item: value.NAME,
            count: value.COUNT
          }
          return temp;
        })
        const total = piedata.reduce((prev, cur) => prev + cur.count, 0)
        this.data = piedata.map(item => { item.percent = (item.count / total); return item })
        this.rankingListData = arr.map(item => {
          const temp = {
            title: item.NAME,
            total: item.COUNT,
            id: item.ID
          }
   
          return temp;
        })
        setTimeout(() => {
          this.pieInit();
        }, 100);
      } else {
        this.msgSrv.error(res.errmsg)
      }
    })
  }





  pieInit() {

    const container = this.pie.nativeElement
    const chart = new G2.Chart({
      container,
      autoFit: true,
      height: 290,
    });

    chart.source(this.data, {
      percent: {
        formatter: val => {
          val = (val * 100).toFixed(0) + '%';
          return val;
        }
      }
    });
    chart.coord('theta', {
      radius: 0.75
    });
    chart.tooltip({
      showTitle: false,
      itemTpl: '<li><span style="background-color:{color};" class="g2-tooltip-marker"></span>{name}: {value}</li>'
    });
    chart.intervalStack()
      .position('percent')
      .color('item')
      .label('percent', {
        formatter: (val, item) => {
          return item.point.item + ': ' + val;
        }
      })
      .tooltip('item*percent', (item, percent) => {
        percent = (percent * 100).toFixed(0) + '%';
        return {
          name: item,
          value: percent
        };
      })
      .style({
        lineWidth: 1,
        stroke: '#fff'
      });
    chart.render();
  }

  handleClick(id:string) {
    const para = {
      AYW030:id
    }
    this.st.reload(para)
  }
  
  toDate(para){
    let str:string="";
    if(para<0){
      str='- '+str;
    }
    let day=0;
    let hour=0;
    let min=0;
    if(Math.abs(para)>(60*24)){
      day=Math.floor(para/(60*24));
      para=para%(60*24);
    }
    if(Math.abs(para)>(60)){
      hour=Math.floor(para/60);
      para=para%60;
    }
    min=para;
    // 拼接字符串
    if(day!==0){
      day=Math.abs(day);
      str=str+day+' 天 ';
    }
    if(hour!==0){
      hour=Math.abs(hour);
      str=str+hour+' 小时 ';
    }
    if(min!==0){
      min=Math.abs(min);
      str=str+min+' 分'
    }
    return str;
  }
}
