import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { JgyjTbtjComponent } from './tbtj.component';

describe('JgyjTbtjComponent', () => {
  let component: JgyjTbtjComponent;
  let fixture: ComponentFixture<JgyjTbtjComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JgyjTbtjComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JgyjTbtjComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
