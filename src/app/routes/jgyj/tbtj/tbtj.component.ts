﻿//  import { Component, OnInit, ChangeDetectionStrategy, ViewChild, ElementRef, Input, AfterViewInit } from '@angular/core';
import { Component, OnInit, ViewChild, NgModule, ChangeDetectionStrategy, AfterViewInit, ElementRef, Input, ViewChildren } from '@angular/core';
import { _HttpClient, ModalHelper } from '@delon/theme';
import { STColumnTag, STColumn, STComponent, STColumnBadge } from '@delon/abc';
import { GridComponent, SupDic, HttpService } from 'lbf';
import { SFComponent, SFSchema, SFButton } from '@delon/form';
import { NzMessageService } from 'ng-zorro-antd';
import { DelonChartModule } from '@delon/chart';
import { Chart, Util } from '@antv/g2';
import { randomBytes } from 'crypto';

@Component({
  selector: 'app-jgyj-tbtj',
  templateUrl: './tbtj.component.html',
})
export class JgyjTbtjComponent implements OnInit {
  private chart: any;
  public num ='0';
  @ViewChild('chart1', { static: false })  private node1: ElementRef;  
  @ViewChild('chart', { static: false })  private node: ElementRef;  
  data=[];
  val: string;
 
  ngOnInit() { }

  // tslint:disable-next-line: use-lifecycle-interface
  ngAfterViewInit() {
    //  Called after ngAfterContentInit when the component's view has been initialized. Applies to components only.
    //  Add 'implements AfterViewInit' to the class.
    this.chart1render();
  }

  public rechart(){  
    this.linechart();    
  }

  public chart1render() 
  {     
    const { node1 } = this;
    const container = node1.nativeElement as HTMLElement;

    const data = [
      { genre: 'Sports', sold: 275 },
      { genre: 'Strategy', sold: 115 },
      { genre: 'Action', sold: 120 },
      { genre: 'Shooter', sold: 350 },
      { genre: 'Other', sold: 150 }
    ]; 
    // G2 对数据源格式的要求，仅仅是 JSON 数组，数组的每个元素是一个标准 JSON 对象。
    // Step 1: 创建 Chart 对象 
    const chart = (this.chart=new G2.Chart({
      container, // 指定图表容器 ID
      width : 600, // 指定图表宽度
      height : 300 // 指定图表高度
    }))

    // Step 2: 载入数据源
    chart.source(data);
    chart.tooltip({
      showTitle: false,
    });
    chart.axis(true);  //  带坐标轴
    // Step 3：创建图形语法，绘制柱状图，由 genre 和 sold 两个属性决定图形位置，genre 映射至 x 轴，sold 映射至 y 轴
    //  chart.interval().position('genre*sold').color('genre')
    // Step 4: 渲染图表    
    chart
    .interval()
    .position('genre*sold')
    .color('genre')    
    //  .tooltip('name')
    .style({
      stroke: '#bfbfbf',
      lineWidth: 0.5,
      fill: '#ff2400',
      globalAlpha: 0.5, // 透明度
      cursor: 'pointer', // 设置鼠标手势
      padding: [ 80, 70, 80, 70 ]
    })
    .select({
      // 设置是否允许选中以及选中样式
      mode: 'single', // 多选还是单选
      style: {
        fill: '#ff2400', // 选中的样式
      },
    }).label('sold');

    chart.render();

    // tslint:disable-next-line: unnecessary-bind
    chart.on('plotclick', function(ev: { shape: any; }) {
      const shape = ev.shape;
      if (!shape || !shape.name) {
        return false;
      }
      if (shape.get('selected')) {
        // console.log(shape.name);        
        this.rechart()
      }
    }.bind(this)
    );
  }  

  public getrandom(): number {
    return Math.random() *30;
  }

  normalchart() {
    if(this.num === '0'){ 
      const { node } = this;
      const container = node.nativeElement as HTMLElement;  
      const data = [
        { Xaxis: 'a', Yaxis: 275 },
        { Xaxis: 'b', Yaxis: 115 },
        { Xaxis: 'c', Yaxis: 120 },
        { Xaxis: 'd', Yaxis: 350 },
        { Xaxis: 'e', Yaxis: 150 }
      ]; // G2 对数据源格式的要求，仅仅是 JSON 数组，数组的每个元素是一个标准 JSON 对象。
      // Step 1: 创建 Chart 对象
      const chart = (this.chart=new G2.Chart({
        container, // 指定图表容器 ID
        width : 600, // 指定图表宽度
        height : 300, // 指定图表高度
        padding: [ 80, 70, 80, 70 ]
      }))
  
      // Step 2: 载入数据源
      chart.source(data);
      // Step 3：创建图形语法，绘制柱状图，由 genre 和 sold 两个属性决定图形位置，genre 映射至 x 轴，sold 映射至 y 轴
      chart.interval().position('Xaxis*Yaxis').color('Xaxis').label('Yaxis');
      // chart.transpose('');
      // chart.coord('theta').transpose('true');
      // Step 4: 渲染图表
      chart.render();
      this.num = '1';
    }
  }

  piechart(){
    if(this.num === '0'){ 
      const { node } = this;
      const container = node.nativeElement as HTMLElement;
      this.data = [
        { item: '事例一', count: 40, percent: 0.4 },
        { item: '事例二', count: 21, percent: 0.21 },
        { item: '事例三', count: 17, percent: 0.17 },
        { item: '事例四', count: 13, percent: 0.13 },
        { item: '事例五', count: 9, percent: 0.09 },
      ];      
      const chart = (this.chart=new G2.Chart({
        container, // 指定图表容器 ID
        // autoFit: true,        
        width : 500, // 指定图表宽度
        height : 500, // 指定图表高度
        padding: [ 80, 70, 80, 70 ]
      }));
      chart.coord('theta', {
        radius: 0.8,
        innerRadius: 0.5,
      });
      // this.chart.source(this.data); 
      chart.source(this.data);

      /*chart.scale('percent', {
        formatter: (val) => {
          val = val * 100 + '%';
          return val;
        },
      });*/
      
      chart.tooltip({
        showTitle: false,
        showMarkers: false,
      });

      /*(chart
        .interval()
        .position('percent')
        .color('item')
        .label('percent', {
          content: (data) => {
            return `${data.item}: ${data.percent * 100}%`;
          },
        })
        .adjust('stack');*/

      chart.intervalStack().position('count').color('item');
      // chart.interaction('element-active');
      this.chart.coord('theta', {
        radius: 1 // 设置饼图的大小
      }).transpose();
      chart.render();
      this.num = '1';
    }
  }

  linechart(){
    if(this.num === '0'){ 
      const { node } = this;
      const container = node.nativeElement as HTMLElement;
      this.data = [
        { year: '1991', value: 3 },
        { year: '1992', value: 4 },
        { year: '1993', value: 3.5 },
        { year: '1994', value: 5 },
        { year: '1995', value: 4.9 },
        { year: '1996', value: 6 },
        { year: '1997', value: 7 },
        { year: '1998', value: 9 },
        { year: '1999', value: 13 },
      ];
      const chart = (this.chart=new G2.Chart({
        container, // 指定图表容器 ID
        // autoFit: true,        
        width : 1200, // 指定图表宽度
        height : 500, // 指定图表高度
        padding: [ 80, 70, 80, 70 ]
      }));  
      chart.source(this.data);

      chart.scale({
        year: {
          range: [0, 1],
        },
        value: {
          min: 0,
          nice: true,
        },
      });
      
      chart.tooltip({ 
        showCrosshairs: true, // 展示 Tooltip 辅助线
        shared: true,
      }); 

      chart.line().position('year*value').label('value');
      chart.point().position('year*value');
      chart.render();
      this.num = '1';
    }
  }
  
}
