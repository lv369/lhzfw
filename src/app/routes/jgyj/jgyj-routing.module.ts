import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { JgyjJggzComponent } from './jggz/jggz.component';
import { JgyjSxyjComponent } from './sxyj/sxyj.component';
import { JgyjCssxtjComponent } from './cssxtj/cssxtj.component';
import { JgyjCsrytjComponent } from './csrytj/csrytj.component';
import { JgyjTbtjComponent } from './tbtj/tbtj.component';
import {JgyjSxyjzjComponent} from './szyjzj/sxyjzj.component';
import {JgyjSxjbComponent} from './sxjb/sxjb.component';
import {JgyjSxjbzjComponent} from './sxjbzj/sxjbzj.component';

const routes: Routes = [

  { path: 'jggz', component: JgyjJggzComponent },
  { path: 'sxyj', component: JgyjSxyjComponent },
  { path: 'cssxtj', component: JgyjCssxtjComponent },
  { path: 'csrytj', component: JgyjCsrytjComponent },
  { path: 'sxyjzj', component: JgyjSxyjzjComponent },
  { path: 'sxjb', component: JgyjSxjbComponent },
  { path: 'sxjbzj', component: JgyjSxjbzjComponent},
  { path: 'tbtj', component: JgyjTbtjComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class JgyjRoutingModule { }
