import { Injectable } from '@angular/core';
import { HttpService, SupDic } from 'lbf';
import { STColumnTag } from '@delon/abc';

@Injectable({
  providedIn: 'root',
})
export class JgyjService {
  constructor(
    private lbs:HttpService 
    ,private supdic: SupDic,
  ){ }


  /*****************************MAP************************************* */
  JGZ004TagMap=new Map();
  initJGZ004TagMap(){
    this.lbs.lbservice("YJWH_QYJBQ",{}).then(resdata=>{
      if(resdata.code>0){
        resdata.message.list.forEach(item=>{
          const obj={name:item.JGZ005,color:item.COLOR};
          this.JGZ004TagMap.set(item.JGZ004,obj);
        });
      }
    });
  }
  refreshJAZ004TagMap(){
    this.JGZ004TagMap=new Map();
    this.initJGZ004TagMap();
  }
  getJGZ004TagName(para){
    if(this.JGZ004TagMap.size===0){
      this.initJGZ004TagMap();
    }
    const obj=this.JGZ004TagMap.get(para);

    return obj.name;
  }
  getJGZ004TagColor(para){
    if(this.JGZ004TagMap.size===0){
      this.initJGZ004TagMap();
    }
    const obj=this.JGZ004TagMap.get(para);
    return obj.color;
  }
  getJGZ004TagMap(){
    if(this.JGZ004TagMap.size===0){
      this.initJGZ004TagMap();
    }
    return this.JGZ004TagMap;
  }


}