import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { JgyjJggzComponent } from './jggz.component';

describe('JgyjJggzComponent', () => {
  let component: JgyjJggzComponent;
  let fixture: ComponentFixture<JgyjJggzComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JgyjJggzComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JgyjJggzComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
