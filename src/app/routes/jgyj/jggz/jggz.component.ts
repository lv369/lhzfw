import { Component, OnInit, ViewChild } from '@angular/core';
import { _HttpClient, ModalHelper } from '@delon/theme';
import { STColumnTag, STColumn, STComponent, STColumnBadge } from '@delon/abc';
import { GridComponent, SupDic, HttpService } from 'lbf';
import { SFComponent, SFSchema } from '@delon/form';
import { NzMessageService } from 'ng-zorro-antd';
import { JgyjService } from '../jgyjservice/jgyj.service';

@Component({
  selector: 'app-jgyj-jggz',
  templateUrl: './jggz.component.html',
  styleUrls: ['./jggz.less']
})

export class JgyjJggzComponent implements OnInit {
  addFormData = {};
  modiFormData = {};
  queryFormData = {}; 
  colorList=[{label:'pink',value:'pink'},{label:'red',value:'red'},{label:'yellow',value:'yellow'},{label:'orange',value:'orange'}
  ,{label:'cyan',value:'cyan'},{label:'green',value:'green'},{label:'blue',value:'blue'},{label:'purple',value:'purple'}
  ,{label:'geekblue',value:'geekblue'},{label:'magenta',value:'magenta'},{label:'volcano',value:'volcano'},{label:'gold',value:'gold'},{label:'lime',value:'lime'}
]
  showAddWindow = false; 
  showModiWindow = false;
  @ViewChild('st', { static: true }) st: GridComponent;
  @ViewChild('addWindow', { static: false }) addWindow: SFComponent;
  @ViewChild('modiWindow', { static: false }) modiWindow: SFComponent;
  @ViewChild('searchForm', { static: false }) searchForm: SFComponent;

  TAG: STColumnTag = {
    1: { text: '是', color: 'green' },
    0: { text: '否', color: 'red' },
  };

  searchSchema: SFSchema = {
    properties: {
      JGZ001: {
        type: 'string',
        title: '规则启用标志',
        enum: this.dic.getSFDic('YESNO'),
        ui: {
          widget: 'select',
          allowClear:true,
        }
      },
      JGZ005: {
        type: 'string',
        title: '规则名称',
      },
      AYW025: {
        type: 'string',
        title: '对应环节',
        enum:this.dic.getSFDic('AYW025'),
        ui:{
          widget:'select',
          allowClear:true,
        }
      },
    },
    ui: {
      spanLabelFixed: 100,
      grid: {
        span: 8
      }
    }
  };

  gridData = [];
  columns: STColumn[] = [
    {
      title: '规则启用标志',
      index: 'JGZ001',
      render: 'custom'
    },
    { title: '类型名称', index: 'JGZ005' },
    { title: '对应环节', index: 'AYW025',dic:'AYW025'},
    { title: '时限(分)', index: 'JGZ003' },
    { title: '备注', index: 'AAE013' },
    {
      title: '操作区', buttons: [
        {
          text: '修改',
          icon: 'edit',
          click: (record: any) => this.modi(record),
        },
        {
          text: '删除',
          icon: 'delete',
          click: (record: any) => this.del(record),
          pop: true,
          popTitle: '确认要删除该规则吗？',
        },
      ]
    },
  ];

  addSchema = {
    properties: {
      JGZ005: {
        type: 'string',
        title: '类型名称'
      },
      JGZ003: {
        type: 'number',
        title: '时限(分)'
      },
      AYW025: {
        type: 'number',
        title: '对应环节',
        enum:this.dic.getSFDic('AYW025'),
        default:'3',
        ui:{
          widget:'select',
        }
      },
      AAE013: {
        type: 'string',
        title: '备注',
        ui:{
          widget:'textarea',
          autosize:{ minRows: 1, maxRows: 6 },
        }
      },
      COLOR:{
        type:'string',
        title:'特征颜色',
        default:'yellow',
        ui:{
          widget:'colorSelect',
          selectList:this.colorList,
        }
      }
    },
    ui: {
      spanLabelFixed: 80,
      grid: {
        span: 24
      }
    },
    required: [ 'JGZ003', 'JGZ005']
  }

  modiSchema:any = {
    properties: {
      JGZ005: {
        type: 'string',
        title: '类型名称',
        default:'',
      },
      JGZ003: {
        type: 'number',
        title: '时限(分)',
        default:'',
      },
      AYW025: {
        type: 'number',
        title: '对应环节',
        enum:this.dic.getSFDic('AYW025'),
        default:'3',
        ui:{
          widget:'select',
        }
      },
      AAE013: {
        type: 'string',
        title: '备注',
        default:'',
        ui:{
          widget:'textarea',
          autosize:{ minRows: 1, maxRows: 6 },
        }
      },
      JGZ004: {
        type: 'number',
        title: '',
        default:'',
        ui:{
          hidden:true,
        }
      },
      COLOR:{
        type:'string',
        title:'特征颜色',
        default:'yellow',
        ui:{
          widget:'custom',
        }
      }
    },
    ui: {
      spanLabelFixed: 80,
      grid: {
        span: 24
      }
    },
    required: ['JGZ003', 'JGZ005']
  }

  initModiSchema(para){
    this.modiSchema = {
      properties: {
        JGZ005: {
          type: 'string',
          title: '类型名称',
          default:para.JGZ005,
        },
        AYW025: {
          type: 'number',
          title: '对应环节',
          enum:this.dic.getSFDic('AYW025'),
          default:para.AYW025, 
          ui:{
            widget:'select',
          }
        },
        JGZ003: {
          type: 'number',
          title: '时限(分)',
          default:para.JGZ003,
        },
        AAE013: {
          type: 'string',
          title: '备注',
          default:para.AAE013,
          ui:{
            widget:'textarea',
            autosize:{ minRows: 1, maxRows: 6 },
          }
        },
        JGZ004: {
          type: 'number',
          title: '',
          default:para.JGZ004,
          ui:{
            hidden:true,
          }
        },
        COLOR:{
          type:'string',
          title:'特征颜色',
          default:para.COLOR,
          ui:{
            widget:'colorSelect',
            selectList:this.colorList,
          }
        }
      },
      ui: {
        spanLabelFixed: 80,
        grid: {
          span: 24
        }
      },
      required: ['JGZ003', 'JGZ005']
    }
  }


  constructor(private http: _HttpClient,
    private dic: SupDic,
    private lbs: HttpService,
    public msgSrv: NzMessageService,
    public jgService:JgyjService
    ) { }
  ngOnInit() { }

  query(pi1=false,pin=1) {
    if(pi1){
      this.st.pi=pin;
    }
    this.st.reload(this.searchForm.value);
  }

  add() {
    this.showAddWindow = true;
  }

  handleCancel() {
    this.showAddWindow = false;
  }

  handleOk() {
    this.addWindow.value.AYW025=this.addWindow.value.AYW025+'';
    if (!this.addWindow.valid) {
      this.msgSrv.error('必填项不能为空！');
      return;
    }

    this.lbs.lbservice('jgyj_gzxz', { para: this.addWindow.value }).then(resdata => {
      if (resdata.code < 1) {
        this.msgSrv.error(resdata.errmsg);
      }
      else {
        this.showAddWindow = false;
        this.addFormData = {};
        this.msgSrv.success('事项新增成功');
        this.jgService.refreshJAZ004TagMap();
        this.query();
      }

    })
  }

  modi(rowData: any) {
    this.initModiSchema(rowData);
    // this.modiFormData = rowData;
    this.showModiWindow = true;
  }
  modiHandleCancel() {
    this.showModiWindow = false;
  }

  modiHandleOk() {
    this.modiWindow.value.AYW025=this.modiWindow.value.AYW025+'';
    if (!this.modiWindow.valid) {
      this.msgSrv.error('必填项不能为空！');
      return; 
    }

    this.lbs.lbservice('jgyj_gzxg', { para: this.modiWindow.value }).then(resdata => {
      console.log(this.modiWindow.value);
      if (resdata.code < 1) {
        this.msgSrv.error(resdata.errmsg);
      }
      else {
        this.showModiWindow = false;
        this.modiFormData = {};
        this.msgSrv.success('规则修改成功');
        this.jgService.refreshJAZ004TagMap();
        this.query();
      }
    })
  }

  del(rowData: any) {
    this.lbs.lbservice("jgyj_gzsc", { para: { JGZ004: rowData.JGZ004 } }).then(resdata => {
      if (resdata.code < 1) {
        this.msgSrv.error(resdata.errmsg);
      }
      else {
        // this.showAddWindow = false;
        // this.addFormData = {};
        this.msgSrv.success('规则删除成功');
        this.jgService.refreshJAZ004TagMap();
        this.query();
      }
    });
  }

  // tslint:disable-next-line: use-lifecycle-interface
  ngAfterViewInit() {
    this.query();
  }

  changeSxState(index:number) {
    const item=this.st.data[index];
    let para001=item.JGZ001;
    // tslint:disable-next-line: prefer-conditional-expression
    if(para001==='0'){
      para001='1';
    }else{
      para001='0';
    }
    console.log(item);
    this.lbs.lbservice("jgyj_sxqt", { para: { JGZ004: item.JGZ004,JGZ001: para001 } }).then(resdata => {
      if(resdata.code>0){
        item.JGZ001=para001;
        this.jgService.refreshJAZ004TagMap();
      }
      else{
        this.msgSrv.error(resdata.errmsg);
      }

    });
  }
}
