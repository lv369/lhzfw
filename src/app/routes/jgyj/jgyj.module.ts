import { NgModule } from '@angular/core';
import { SharedModule } from '@shared';
import { JgyjRoutingModule } from './jgyj-routing.module';
import { JgyjJggzComponent } from './jggz/jggz.component';
import { JgyjSxyjComponent } from './sxyj/sxyj.component';
import { JgyjCssxtjComponent } from './cssxtj/cssxtj.component';
import { JgyjCsrytjComponent } from './csrytj/csrytj.component';
import { JgyjTbtjComponent } from './tbtj/tbtj.component';
import {JgyjSxyjzjComponent} from './szyjzj/sxyjzj.component';
import {JgyjSxjbComponent} from './sxjb/sxjb.component';
import {JgyjSxjbzjComponent} from './sxjbzj/sxjbzj.component';

const COMPONENTS = [
  JgyjJggzComponent,
  JgyjSxyjComponent,
  JgyjCssxtjComponent,
  JgyjCsrytjComponent,
  JgyjSxyjzjComponent,
  JgyjTbtjComponent,
  JgyjSxjbComponent,
  JgyjSxjbzjComponent];
const COMPONENTS_NOROUNT = [];

@NgModule({
  imports: [
    SharedModule,
    JgyjRoutingModule
  ],
  declarations: [
    ...COMPONENTS,
    ...COMPONENTS_NOROUNT
  ],
  entryComponents: COMPONENTS_NOROUNT
})
export class JgyjModule { }
