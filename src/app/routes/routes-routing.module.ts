import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { environment } from '@env/environment';
// layout
import { LayoutProComponent } from '@brand';
import { LayoutPassportComponent } from '../layout/passport/passport.component';
// dashboard pages
import { DashboardComponent } from './dashboard/dashboard.component';
// passport pages
import { UserLoginComponent } from './passport/login/login.component';
import { UserLockComponent } from './passport/lock/lock.component';
// single pages
import { CallbackComponent } from './callback/callback.component';
import { DashboardWorkplaceComponent } from './dashboard/workplace/workplace.component';
import { Workplace2Component } from './dashboard/workplace2/workplace2.component';

const routes: Routes = [
  {
    path: '',
    component: LayoutProComponent,
    children: [
      { path: '', redirectTo: 'dashboard/workplace', pathMatch: 'full' },
      { path: 'dashboard', redirectTo: 'dashboard/workplace', pathMatch: 'full' },
      { path: 'dashboard/monitor', component: DashboardComponent },
      { path: 'dashboard/workplace', component: DashboardWorkplaceComponent },
      { path: 'dashboard/workplace2', component: Workplace2Component },
      // Exception
      {
        path: 'exception',
        loadChildren: './exception/exception.module#ExceptionModule',
      },

      { path: 'xtgl', loadChildren: './xtgl/xtgl.module#XtglModule' },
      { path: 'xtgn', loadChildren: './xtgn/xtgn.module#XtgnModule' },
      { path: 'ywtx', loadChildren: './ywtx/ywtx.module#YwtxModule' },
      { path: 'tyjb', loadChildren: './tyjb/tyjb.module#TyjbModule' },
      { path: 'xxgl', loadChildren: './xxgl/xxgl.module#XxglModule' },
      { path: 'shgl', loadChildren: './shgl/shgl.module#ShglModule' },
      { path: 'jgyj', loadChildren: './jgyj/jgyj.module#JgyjModule' },
      { path: 'dpsj', loadChildren: './dpsj/dpsj.module#DpsjModule' },
    ],
  },
  // passport
  {
    path: 'passport',
    component: LayoutPassportComponent,
    children: [
      {
        path: 'login',
        component: UserLoginComponent,
        data: { title: '登录' },
      },
      {
        path: 'lock',
        component: UserLockComponent,
        data: { title: '锁屏', },
      },
    ],
  },
  // 单页不包裹Layout
  { path: 'callback/:type', component: CallbackComponent },
  { path: '**', redirectTo: 'exception/404' },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, {
      useHash: environment.useHash,
      // NOTICE: If you use `reuse-tab` component and turn on keepingScroll you can set to `disabled`
      // Pls refer to https://ng-alain.com/components/reuse-tab
      scrollPositionRestoration: 'top',
    }),
  ],
  exports: [RouterModule],
})
export class RouteRoutingModule { }
