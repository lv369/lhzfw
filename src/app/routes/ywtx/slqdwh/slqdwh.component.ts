import { Component, OnInit, ViewChild, EventEmitter ,Input, TemplateRef } from '@angular/core';
import { _HttpClient, ModalHelper } from '@delon/theme';
import { STColumn, STComponent, STPage, STChange, STColumnFilterMenu } from '@delon/abc';
import { SFSchema,SFComponent } from '@delon/form';
import { GridComponent, PopdetailComponent, HttpService, SupDic } from 'lbf';
import { NzMessageService } from 'ng-zorro-antd';
import { Router } from '@angular/router';

@Component({
  selector: 'app-ywtx-slqdwh',
  templateUrl: './slqdwh.component.html',
})
export class YwtxSlqdwhComponent implements OnInit {
  params = {slqdSname:'SLQDWH_QSLQD',form:{}};
     // 展示
    @ViewChild('slqdSF',{static:false}) slqdSF: SFComponent;
    @ViewChild('slqdST',{static:false}) slqdST: GridComponent;
    slqdSchema: SFSchema={properties:{}};
    slqdColumns: STColumn[] = [
      { title: '渠道编码', index: 'DAD107'},
      { title: '渠道名称', index: 'DAD108'},
      { title: '渠道地址', index: 'DAD109'},
      { title: '联系电话', index: 'AAE005'},
      { title: '工作时间', index: 'DAD106'},
      { title: '线下微信自主机', index: 'DAD112'},
      { title: '对应机构', index: 'DGB020',dic:'DGB020'},
      { title: '状态', index: 'AAE100',render:'custom'},
      { title: '操作区',
          buttons:[
           { 
             text: '修改',
             icon: 'edit',
             click: (record: any) => this.slqdUpdate(record),
           },
         ]},
       ];
       // 新建
      @ViewChild('slqdAddSF',{static:false}) slqdAddSF: SFComponent;
      slqdAddVisible=false;
      slqdAddSchema: SFSchema={properties:{}};

      // 新建
      @ViewChild('slqdUpSF',{static:false}) slqdUpSF: SFComponent;
      slqdUpVisible=false;
      slqdUpSchema: SFSchema={properties:{}};





/*------------------------------------------------------------------------------------------------------------------------------------------------- */
constructor(
  public msgSrv: NzMessageService,
  private lbservice: HttpService,
  private supdic: SupDic,
) { 
}

  ngOnInit() { 
    this.params={
      slqdSname:'SLQDWH_QSLQD',
      form:{},
    }
    this.initJgTreeDataset();
  }


  checkValueNull(e:string){
    if(e==null||e.length===0){
      return true;
    }
    return false;
  }

  checkValueNumber(sch:string,tar:string,src:string,title:string){
    if(this.checkValueNull(this[sch].value[src])){
      return true;
    }
    this[sch].value[tar]=Number(this[sch].value[src]);
    if(title!=='none' && isNaN(this[sch].value[tar])){
      this.msgSrv.error(title+'只允许为数字');
      return false;
    }
    return true;
  }

  /**************************查询***************************** */
  initslqdSchema(){
    this.slqdSchema= {
      properties: {
        DAD107_S:{
          type:'string',
          title:'渠道编号',
        },
        DAD108:{
          type:'string',
          title:'渠道名称',
        },
        DGB020:{
          type:'string',
          title:'机构',
          enum:this.jgTreeDataset,
          ui:{
            widget: 'tree-select',
          }
        },
        btn:{
          type:'string',
          title:'',
          ui:{
            widget:'custom',
          }
        }
      },
      ui: {
        spanLabelFixed: 100,
        grid: {span: 8 }
      },
    };
  }
  
  slqdQuery(pi1=false){
    if(pi1){
      this.slqdST.pi=1;
    }
    this.checkValueNumber('slqdSF','DAD107','DAD107_S','none');
    this.slqdST.reload(this.slqdSF.value);
  }


  /**************************新建*************************** */
  slqdAdd(){
    this.slqdAddVisible=true;
    this.initslqdAddSchema();
  }

  slqdAddCancel(){
    this.slqdAddVisible=false;
  }

  slqdAddOk(){
    if(this.checkValueNull(this.slqdAddSF.value.DAD108)){
      this.msgSrv.error('渠道名称不可为空');
      return;
    }
    this.lbservice.lbservice("SLQDWH_ASLQD",{para:this.slqdAddSF.value}).then(resdata=>{
      if(resdata.code>0){
        this. slqdQuery();
        this.slqdAddVisible=false;
        this.msgSrv.success('添加成功');
      }else{
        this.msgSrv.error(resdata.errmsg);
      }
    });
  }

  initslqdAddSchema(){
    this.slqdAddSchema= {
      properties: {
        DAD108:{
            type:'string',
            title:'渠道名称'
        },
        DAD109:{
            type:'string',
            title:'渠道地址'
        },
        AAE005:{
            type:'string',
            title:'联系电话'
        },
        DAD106:{
            type:'string',
            title:'工作时间'
        },
        AAE100:{
            type:'string',
            title:'状态',
            enum:this.supdic.getSFDic('AAE100'),
            default:'1',
            ui:{
              widget:'select',
            }
        },
        DAD112:{
            type:'string',
            title:'线下自助机'
        },
        DAD110:{
            type:'string',
            title:'gps坐标'
        },
        DAD111:{
            type:'string',
            title:'百度坐标'
        },
        DGB020:{
            type:'string',
            title:'对应机构',
            enum:this.jgTreeDataset,
          ui:{
            widget: 'tree-select',
          }
        }
    },
      ui: {
        spanLabelFixed: 100,
        grid: {span: 12 }
      },
      required:["DAD108"]
    };
  }

  /***************************修改****************************** */
  slqdUpdate(rec:any){
    this.initslqdUpSchema(rec);
    this.slqdUpVisible=true;
  }

  slqdUpCancel(){
    this.slqdUpVisible=false;
  }

  slqdUpOk(){
    if(this.checkValueNull(this.slqdUpSF.value.DAD108)){
      this.msgSrv.error('渠道名称不可为空');
      return;
    }
    this.lbservice.lbservice("SLQDWH_USLQD",{para:this.slqdUpSF.value}).then(resdata=>{
      if(resdata.code>0){
        this. slqdQuery();
        this.slqdUpVisible=false;
        this.msgSrv.success('修改成功');
      }else{
        this.msgSrv.error(resdata.errmsg);
      }
    });
  }

  initslqdUpSchema(rec:any){
    this.slqdUpSchema= {
      properties: {
        DAD107:{
          type:'string',
          title:'渠道ID',
          default:rec.DAD107,
          ui:{
            hidden:true,
          }
      },
        DAD108:{
            type:'string',
            title:'渠道名称',
            default:rec.DAD108,
        },
        DAD109:{
            type:'string',
            title:'渠道地址',
            default:rec.DAD109,
        },
        AAE005:{
            type:'string',
            title:'联系电话',
            default:rec.AAE005,
        },
        DAD106:{
            type:'string',
            title:'工作时间',
            default:rec.DAD106,
        },
        DAD112:{
            type:'string',
            title:'线下自助机',
            default:rec.DAD112,
        },
        DAD110:{
            type:'string',
            title:'gps坐标',
            default:rec.DAD110,
        },
        DAD111:{
            type:'string',
            title:'百度坐标',
            default:rec.DAD111,
        },
        DGB020:{
            type:'string',
            title:'对应机构',
            default:rec.DGB020,
            enum:this.jgTreeDataset,
          ui:{
            widget: 'tree-select',
          }
        }
    },
      ui: {
        spanLabelFixed: 100,
        grid: {span: 12 }
      },
      required:["DAD108"]
    };
  }

  changeSlqdState(index:number){
    let rec: any = this.slqdST.data[index];
    if (rec.AAE100 === '0') {
      rec.AAE100 = '1';
    } else if (rec.AAE100 === '1') {
      rec.AAE100 = '0';
    }
    this.lbservice.lbservice('SLQDWH_USLQD_CS', { para: rec }).then(resdata => {
      if (resdata.code >0) {

      }else{
        if (rec.AAE100 === '0') {
          rec.AAE100 = '1';
        } else if (rec.AAE100 === '1') {
          rec.AAE100 = '0';
        }
      }
      this.slqdST.data[index].AAE100 = rec.AAE100;
    });
  }

  /***************机构树********** */
jgTreeDataset=[];
initJgTreeDataset(){
  this.lbservice.lbservice("SELECT_QJGSOFQX",{}).then(resdata=>{
    this.jgTreeDataset=resdata.message.list;
    this.initslqdSchema();
  })
}
    
}
