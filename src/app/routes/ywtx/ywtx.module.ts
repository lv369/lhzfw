import { NgModule } from '@angular/core';
import { SharedModule } from '@shared';
import { YwtxRoutingModule } from './ywtx-routing.module';
import { YwtxZskwhComponent } from './zskwh/zskwh.component';
import { YwtxSlqdwhComponent } from './slqdwh/slqdwh.component';
import { YwtxClwhComponent } from './clwh/clwh.component';
import { YwtxCltjwhComponent } from './cltjwh/cltjwh.component';
import { YwtxSxbdwhComponent } from './sxbdwh/sxbdwh.component';
import { SxtjwhComponent } from './sxtjwh/sxtjwh.component';
import { YwtxSxclwhComponent} from './sxclwh/sxclwh.component';
import { SxlbwhComponent } from './sxlbwh/sxlbwh.component';
import { YwtxHjrywhComponent } from './hjrywh/hjrywh.component';
import { YwtxZxsxwhComponent } from './zxsxwh/zxsxwh.component'

const COMPONENTS = [
  YwtxZskwhComponent,
  YwtxSlqdwhComponent,
  YwtxClwhComponent,
  YwtxCltjwhComponent,
  YwtxSxbdwhComponent,
  SxtjwhComponent,
  YwtxSxclwhComponent,
  SxlbwhComponent,
  YwtxHjrywhComponent,
  YwtxZxsxwhComponent];
const COMPONENTS_NOROUNT = [];

@NgModule({
  imports: [
    SharedModule,
    YwtxRoutingModule,
  ],
  declarations: [
    ...COMPONENTS,
    ...COMPONENTS_NOROUNT,


  ],
  entryComponents: COMPONENTS_NOROUNT
})
export class YwtxModule { }
