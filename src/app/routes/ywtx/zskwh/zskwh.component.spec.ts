import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { YwtxZskwhComponent } from './zskwh.component';

describe('YwtxZskwhComponent', () => {
  let component: YwtxZskwhComponent;
  let fixture: ComponentFixture<YwtxZskwhComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ YwtxZskwhComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(YwtxZskwhComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
