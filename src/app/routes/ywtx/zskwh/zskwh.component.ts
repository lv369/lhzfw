import { Component, OnInit, ViewChild } from '@angular/core';
import { _HttpClient } from '@delon/theme';
import { STColumn, STColumnTag } from '@delon/abc';
import { SFSchema, SFComponent } from '@delon/form';
import { SupDic, HttpService, GridComponent } from 'lbf';
import { NzMessageService, NzTreeComponent } from 'ng-zorro-antd';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-ywtx-zskwh',
  templateUrl: './zskwh.component.html',
  styleUrls: ['./zskwh.less']
})
export class YwtxZskwhComponent implements OnInit {
  addFormData = {}; // 新增弹出框表单数据
  modiFormData = {}; // 修改弹出框表单数据
  queryFormData = {}; // 查询区表单数据
  showAddWindow = false; // 是否显示新增弹出框
  showModiWindow = false; // 是否显示修改弹出框
  showSetWindow = false; // 是否显示主管领导设置弹出框
  apd: any = {};
  userNodes: any // 人员树数据
  curAYW001 = 0 // 领导设置时，当前的事项编码

  // 事项列表
  @ViewChild('st', { static: true }) st: GridComponent;
  // 事项新增表单
  @ViewChild('addWindow', { static: false }) addWindow: SFComponent;
  // 事项修改表单
  @ViewChild('modiWindow', { static: false }) modiWindow: SFComponent;
  // 查询区表单
  @ViewChild('searchForm', { static: false }) searchForm: SFComponent;
  // 人员树
  @ViewChild('userTree', { static: false }) userTree: NzTreeComponent;

  // 主体对象类别渲染标签 AYW004
  TAG: STColumnTag = {
    1: { text: '企业、组织', color: 'green' },
    2: { text: '个人', color: 'red' },
    3: { text: '不限', color: 'blue' },
  };

  // 查询区生成串
  searchSchema: SFSchema = {
    properties: {
      AYW002: {
        type: 'string',
        title: '事项名称'
      },
      DGB010: {
        type: 'string',
        title: '所属部门',
        enum: this.dic.getSFDic('MYSXBM'),
        ui: {
          widget: 'select',
          allowClear: true,
        }
      },
    },
    ui: {
      spanLabelFixed: 100,
      grid: {
        span: 6
      }
    }

  };

  // 事项新增（修改）表单生成串
  addSchema = {
    properties: {
      AYW001: {
        type: 'string',
        title: '事项主键',
        ui:
        {
          hidden: true
        }

      },
      AYW003: {
        type: 'string',
        title: '事项编码',
        ui: {
          grid: {
            span: 24
          }
        }
      },
      AYW002: {
        type: 'string',
        title: '事项名称',
        ui: {
          grid: {
            span: 24
          }
        }
      },

      DGB010: {
        type: 'string',
        title: '所属部门',
        enum: this.dic.getSFDic('MYSXBM'),
        ui: {
          widget: 'select',
          grid: {
            span: 24
          }
        }
      },
      AYW005: {
        type: 'number',
        title: '承诺时限'
      },
      AYW006: {
        type: 'string',
        title: '时限单位',
        enum: this.dic.getSFDic('AYW006'),
        ui: {
          widget: 'select',
        }
      },
    },
    ui: {
      spanLabelFixed: 90,
      grid: {
        span: 12
      }
    },
    required: ['AYW002', 'AYW003', 'AYW004', 'AYW005', 'AYW006', 'DGB010']
  }
  gridData = [];

  // 事项列表生成串
  columns: STColumn[] = [
    {
      title: '事项状态',
      index: 'AYW008',
      render: 'custom'
    },

    { title: '事项编码', index: 'AYW003' },
    { title: '事项名称', index: 'AYW002' },
    { title: '所属部门', index: 'DGB010', dic: 'MYSXBM' },
    { title: '承诺时限', index: 'AYW005' },
    { title: '时限单位', index: 'AYW006', dic: 'AYW006' },
    {
      title: '操作区', buttons: [
        {
          text: '修改',
          icon: 'edit',
          click: (record: any) => {
            // 给修改编辑框赋值
            this.modiWindow.setValue('/AYW001', (record.AYW001));
            this.modiWindow.setValue('/AYW002', (record.AYW002));
            this.modiWindow.setValue('/AYW003', (record.AYW003));
            this.modiWindow.setValue('/AYW005', (record.AYW005));
            this.modiWindow.setValue('/AYW006', (record.AYW006));
            this.modiWindow.setValue('/DGB010', (record.DGB010));
            // 打开修改编辑框
            this.openWindow('modi');
          }
        },
        {
          text: '删除',
          icon: 'delete',
          click: (record: any) => this.delSave(record),
          pop: '确定要删除该事项吗？',
        },

      ]
    },
  ];

  constructor(private http: HttpClient,
    private dic: SupDic,
    private lbs: HttpService,
    public msgSrv: NzMessageService,
  ) { }

  ngOnInit() {

  }

  ngAfterViewInit() {
    this.query();
  }

  // 打开弹出框 根据传入标签进行处理
  openWindow(flag: string) {
    // 新增
    if (flag === 'add') {
      this.showAddWindow = true;
    }
    // 修改
    else if (flag === 'modi') {
      this.showModiWindow = true;
    }
    // 主管领导设置
    else {
      this.showSetWindow = true;
    }
  }

  // 关闭弹出框 根据传入标签进行处理
  closeWindow(flag: string) {
    // 新增
    if (flag === 'add') {
      this.showAddWindow = false;
    }
    // 修改
    else if (flag === 'modi') {
      this.showModiWindow = false;
    }
    // 主管领导设置
    else {
      this.showSetWindow = false;
    }
  }

  // 事项新增保存事件
  addSave() {
    // 必输项校验
    if (!this.addWindow.valid) {
      this.msgSrv.error('必填项不能为空！');
      return;
    }

    // 调用事项新增服务
    this.lbs.lbservice('YWPZ_SX_ADD', { para: this.addWindow.value }).then(resdata => {
      console.log(this.addWindow.value);
      if (resdata.code < 1) {
        this.msgSrv.error(resdata.errmsg);
      }
      else {
        // 关闭新增框
        this.closeWindow('add');
        // 新增框内容情况
        this.addFormData = {};
        this.msgSrv.success('事项新增成功');
        // 成功后，重新加载列表
        this.query();
      }

    })

  }

  // 事项修改保存事件
  modiSave() {
    // 必输项校验
    if (!this.modiWindow.valid) {
      this.msgSrv.error('必填项不能为空！');
      return;
    }

    // 调用事项修改服
    this.lbs.lbservice('YWPZ_SX_MODI', { para: this.modiWindow.value }).then(resdata => {
      console.log(this.modiWindow.value);
      if (resdata.code < 1) {
        this.msgSrv.error(resdata.errmsg);
      }
      else {
        // 关闭修改框
        this.closeWindow('modi');
        // 将修改框清空
        this.modiFormData = {};
        this.msgSrv.success('事项修改成功');
        // 成功后，重新加载列表
        this.query();
      }

    })

  }

  // 事项信息查询
  query(pi1 = false) {

    if (pi1) {
      this.st.pi = 1;
    }
    this.st.reload(this.searchForm.value);
  }

  // 事项状态切换

  changeSxState(sxbm: number) {
    this.lbs.lbservice("YWPZ_SXZT_MODI", { para: { AYW001: sxbm } }).then(resdata => {
      if (resdata.code < 1) {
        this.msgSrv.error(resdata.errmsg);
      }

    });
  }

  // 事项删除保存事件
  delSave(rowData: any) {
    // 调用事项删除服务
    this.lbs.lbservice("YWPZ_SX_DEL", { para: { AYW001: rowData.AYW001 } }).then(resdata => {
      if (resdata.code < 1) {
        this.msgSrv.error(resdata.errmsg);
      }
      else {
        this.msgSrv.success('事项删除成功');
        // 删除成功，重新加载
        this.query(true);
      }
    });
  }

  // 初始化部门人员树
  initBmUserTree(dataAYW001: number, dataDGB010: string) {
    this.curAYW001 = dataAYW001;
    this.lbs.lbservice('YWPZ_SX_USER_TREE', { para: { AYW001: dataAYW001, DGB010: dataDGB010 } }).then(result => {
      if (result.code < 1) {
        this.msgSrv.error(result.errmsg)
      } else {
        this.userNodes = result.message.list;
        console.log(this.userNodes)
        this.openWindow('set');
        // this.initGw();
      }
    })
  }

  // 主管领导设置保存事件
  setSave() {
    // 将选中节点值放入数组中
    const userList = [];
    for (let users of this.userTree.getCheckedNodeList()) {
      userList.push(users.key)
    }

    // 主管设置领导服务
    console.log(this.userTree.getCheckedNodeList());
    this.lbs.lbservice("YWPZ_SXUSER_SET", { para: { AYW001: this.curAYW001, USERS: userList } }).then(resdata => {
      if (resdata.code < 1) {
        this.msgSrv.error(resdata.errmsg);
      }
      else {
        this.closeWindow('set')
        this.msgSrv.success('设置成功');
        // 删除成功，重新加载
        this.query(true);
      }
    });
  }

}
