import { Component, OnInit , ViewChild ,TemplateRef } from '@angular/core';
import { _HttpClient, ModalHelper } from '@delon/theme';
import { NzLayoutModule } from 'ng-zorro-antd/layout';
import { NzButtonModule, NzButtonComponent  } from 'ng-zorro-antd/button';
import { NzFormatEmitEvent  } from 'ng-zorro-antd/core';
import { NzMessageService, NzModalRef, NzModalService } from 'ng-zorro-antd';
import { FormBuilder, FormControl, FormGroup  } from '@angular/forms';
import { DelonFormModule,SFSchema , SFComponent , WidgetRegistry, SFSchemaEnumType } from '@delon/form';
import { HttpService, SupDic, GridComponent } from 'lbf';
import { Subject } from 'rxjs';
import { STColumn, STColumnTag, STColumnBadge } from '@delon/abc';

@Component({
  selector: 'app-ywtx-sxclwh',
  templateUrl: './sxclwh.component.html',
  styleUrls: ['./sxclwh.component.less']
})
export class YwtxSxclwhComponent implements OnInit {
  searchValue = ''
  nodes = [];
  isAddDisabled = true;
  treeData;
  isVisible = false;
  isClVisible = false;
  isUpVisible = false;
  modalTitle = '新增材料';
  modalClTitle = '材料列表';
  param = {sname: 'CLWH_CLMLCX',queryparas: {}};
  upData;
  style = {height: (window.outerHeight-330)+'px',overflow: 'auto'}

  showSchema: SFSchema = {
    properties:{
      DAD017:{
        type: 'string',
        title: '事项主键',
        ui:
        {
          hidden: true
        }        
      },
      DAD004:{
        type: 'string',
        title: '权力编码',
        ui:{
          widget:'text',
          grid:{
            span:8
          }
        }
      },
      DAD031:{
        type: 'string',
        title: '事项名称',
        ui:{
          widget:'text',
          grid:{
            span:16
          }
        }
      },
      DAD032: {
        type: 'string',
        title: '对象主体',
        enum: this.supdic.getSFDic('DAD032'),
        readOnly: true,
        ui: {
          widget: 'radio',
          grid:{
            span:8
          }
        }
      },
      DGB010: {
        type: 'string',
        title: '所属部门',
        ui:{
          widget:'text',
          grid:{
            span:8
          }
        }
      },
      DAD011: {
        type: 'string',
        title: '事项类型',
        ui:{
          widget:'text',
          grid:{
            span:8
          }
        }
      },      
    },
    ui:{
      spanLabelFixed: 90,
      grid:{
        span:24
      }
    }
  };

  addSchema: SFSchema = {
    properties: {
      DAD017: {
        type: 'number',
        title: '事项编码',
        enum: this.supdic.getSFDic('DAD017'),
        ui: {
          widget: 'select',
          grid: {
            span: 12
          }
        },
        readOnly: true,
      },
      custom: {
        type: 'number',
        title: '材料编号',
        ui: {
          widget: 'custom',
          grid: {
            span: 6
          }
        }      
      },
      DAD029:{
        type: 'string',
        title: '材料名称',
        ui: {
          grid: {
            span: 6
          }
        }
      },
      DAD105:{
        type: 'string',
        title: '材料形式',
        enum: this.supdic.getSFDic('DAD034'),
        ui: {          
          widget: 'select',
          grid: {
            span: 6
          }
        }
      },
      DAD120: {
        type: 'string',
        title: '纸质材料份数',
        ui: {
          grid: {
            span: 6
          }
        }
      },
      DAD121: {
        type: 'string',
        title: '材料必要性',
        enum: this.supdic.getSFDic('DAC002'),
        ui: {
          widget: 'select',
          grid: {
            span: 6
          }
        }
      },
      DAD122: {
        type: 'string',
        title: '材料类型',
        enum: this.supdic.getSFDic('DAC003'),
        ui: {
          widget: 'select',
          grid: {
            span: 6
          }
        }
      },
      DAD123: {
        type: 'string',
        title: '纸质材料规格',
        ui: {
          grid: {
            span: 6
          }
        }
      },
      DAD124: {
        type: 'string',
        title: '受理标准',
        ui: {
          grid: {
            span: 6
          }
        }
      },
      DAD125: {
        type: 'string',
        title: '填报须知',
        ui: {
          grid: {
            span: 12
          }
        }
      },
      DAD126: {
        type: 'string',
        title: '提供材料依据',
        ui: {
          grid: {
            span: 24
          }
        }
      },
      SEQ: {
        type: 'number',
        title: '材料顺序',
        ui: {
          grid: {
            span: 6
          }
        }
      },
      AAE013: {
        type: 'string',
        title: '备注',
        ui: {
          grid: {
            span: 12
          }
        }
      },
    },
    ui: {
      spanLabelFixed: 90,
      grid: {
        span: 24
      }
    },
    required: ['DAD017','SEQ','DAD028']
  }

  upSchema: SFSchema = {
    properties: {
      DAD017: {
        type: 'number',
        title: '事项编码',
        enum: this.supdic.getSFDic('DAD017'),
        ui: {
          widget: 'select',
          grid: {
            span: 12
          }
        },
        readOnly: true,
      },
      DAD028: {
        type: 'number',
        title: '材料编码',
        ui: {
          grid: {
            span: 6
          }
        },
        readOnly: true
      },
      DAD029: {
        type: 'number',
        title: '材料名称',
        ui: {
          grid: {
            span: 6
          }
        },
        readOnly: true
      }, 
      DAD105:{
        type: 'string',
        title: '材料形式',
        enum: this.supdic.getSFDic('DAD034'),
        ui: {          
          widget: 'select',
          grid: {
            span: 6
          }
        }
      },
      DAD120: {
        type: 'string',
        title: '纸质材料份数',
        ui: {
          grid: {
            span: 6
          }
        }
      },
      DAD121: {
        type: 'string',
        title: '材料必要性',
        enum: this.supdic.getSFDic('DAC002'),
        ui: {
          widget: 'select',
          grid: {
            span: 6
          }
        }
      },
      DAD122: {
        type: 'string',
        title: '材料类型',
        enum: this.supdic.getSFDic('DAC003'),
        ui: {
          widget: 'select',
          grid: {
            span: 6
          }
        }
      },
      DAD123: {
        type: 'string',
        title: '纸质材料规格',
        ui: {
          grid: {
            span: 6
          }
        },
      },
      DAD124: {
        type: 'string',
        title: '受理标准',
        ui: {
          grid: {
            span: 12
          }
        },
      },
      DAD125: {
        type: 'string',
        title: '填报须知',
        ui: {
          grid: {
            span: 12
          }
        },
      },
      DAD126: {
        type: 'string',
        title: '提供材料依据',
        ui: {
          grid: {
            span: 24
          }
        }
      },     
      SEQ: {
        type: 'number',
        title: '顺序',
        ui: {
          grid: {
            span: 6
          }
        },
      }, 
      AAE013: {
        type: 'string',
        title: '备注',
        ui: {
          grid: {
            span: 12
          }
        },
      },
    },
    ui: {
      spanLabelFixed: 90,
      grid: {
        span: 24
      }
    },
    required: ['SEQ']
  }
  
  searchSchema: SFSchema = {
    properties: {
      DAD130: {
        type: 'string',
        title: '材料标签',
        enum: this.supdic.getSFDic('CLBQ'),
        ui: {
          widget: 'select',
          mode: 'tags',
          width: 400,
          allowClear: true
        }
      }
    }
  }
  columns: STColumn[] = [
    // { title: '事项编码', index: 'DAD017', dic: 'DAD017' },    
    { title: '材料编码', index: 'DAD028' },
    { title: '材料名称', index: 'DAD029' },
    { title: '材料形式', index: 'DAD105', dic: 'DAD034' },
    { title: '纸质材料份数', index: 'DAD120' },
    { title: '材料必要性', index: 'DAD121', dic: 'DAC002' },
    { title: '材料类型', index: 'DAD122', dic: 'DAC003' },
    { title: '纸质材料规格', index: 'DAD123' },
    { title: '受理标准', index: 'DAD124' },
    { title: '填报须知', index: 'DAD125' },
    { title: '提供材料依据', index: 'DAD126' },
    { title: '备注', index: 'AAE013' },
    // { title: '渠道来源', index: 'DAD080' ,dic:'DAD080'},
    // { title: '渠道说明', index: 'DAD081' },
    // { title: '是否共享', index: 'DAD082'  ,dic:'DAD082'},
    { title: '顺序', index: 'SEQ' },
    {
      title: '操作区',fixed:'right',width:100,
      buttons: [
        {
          text: '修改',
          click: (record) => this.update(record)
        },
        {
          text: '删除',
          click: (record) => this.del(record),
          pop: true,
          popTitle: '是否删除？'
        },
        {
          text: '标签维护',
          click: (record) => this.showBqUpdate(record)
        }
      ]
    }
  ]

  ClColumns: STColumn[] = [
    { title: '材料编码', index: 'DAD028'},// ,dic: 'DAH001'},
    { title: '材料名称', index: 'DAD029'},
    { title: '渠道来源', index: 'DAD080',dic:'DAD080'},
    { title: '来源渠道说明', index: 'DAD081'},
    { title: '是否共享', index: 'DAD082',dic:'DAD082'},
    {
      title: '操作区',
      buttons: [
          {
            text: '添加',
            click: (record) => this.tjcl(record)
          }
      ]
    }
  ]

  @ViewChild('clst',{static:false}) clst: GridComponent;
  @ViewChild('showSf',{static:false}) showSf: SFComponent;
  @ViewChild('addSf',{static:false}) addSf: SFComponent;
  @ViewChild('upSf',{static:false}) upSf: SFComponent;
  @ViewChild('st',{static: false}) st: GridComponent;
  @ViewChild('sf',{static:false}) sf: SFComponent;
  constructor(private httpservice: HttpService,
    private supdic: SupDic,
    private msgSrv: NzMessageService) { }

  ngOnInit() { 
    this.initDeptTree();
  }

  // 初始化机构树
  initDeptTree(){
    this.httpservice.lbservice('BDWH_TREE',{}).then(res=>{
      this.nodes = res.message.list;
    });
 }

  // 功能树单击事件
  nzEvent(event: NzFormatEmitEvent): void {
    this.isAddDisabled = true
    this.showSf.refreshSchema();
    this.treeData = event.node.origin;
    // console.log(this.addSf);
    // console.log(event);
    if(this.treeData.isLeaf)
    {
        // 编辑框表单赋值
        this.showSf.setValue('/DAD017',this.treeData.key);
        this.showSf.setValue('/DAD031',this.treeData.title);
        this.showSf.setValue('/DAD004',this.treeData.DAD004);
        this.showSf.setValue('/DGB010',this.treeData.DGB010);
        this.showSf.setValue('/DAD032',this.treeData.DAD032);
        this.showSf.setValue('/DAD011',this.treeData.DAD011);
        this.isAddDisabled = false;
        this.st.reload({DAD017: this.treeData.key})
    }    
  }

  add(){
    this.addSf.refreshSchema();
    this.addSf.setValue('/DAD017',this.treeData.key);
    this.isVisible = true
  }

  tjcl(rec){
      this.addSf.setValue('/custom',rec.DAD028);
      this.addSf.setValue('/DAD029',rec.DAD029);
      this.addSf.setValue('/DAD105',rec.DAD034);
      this.addSf.setValue('/DAD125',rec.DAD066);
      this.addSf.setValue('/DAD124',rec.DAD085);
      this.isClVisible = false;
  }

  query(){
    this.clst.reload(this.sf.value)
  }

  CLAdd(){
    this.sf.refreshSchema();
    this.clst.reload({});
    this.isClVisible = true;
    // opener.reload();
    // tslint:disable-next-line: no-unused-expression
  }

  handleAddCancel(){
    this.isVisible = false
  }

  handleAddOk(){
    if (!this.addSf.valid) {
      this.msgSrv.error('必填项不能为空！');
      return;
    }
    this.isVisible = false    
    this.httpservice.lbservice('CLWH_ADD',{para: this.addSf.value}).then(resdata => {
      if(resdata.code<1)
      {
         this.msgSrv.error(resdata.errmsg);
      }
      else
      {
        this.st.reload({DAD017: this.treeData.key})
        this.msgSrv.success('添加成功');     
      }
    })
    this.st.reload({DAD017: this.treeData.key})
    // this.st.st.reload();
    // this.st.reload({DAD017: this.treeData.key})
    
  }

  handleClCancel(){
    this.isClVisible = false;
  }

  handleUpCancel(){
    this.isUpVisible = false;
  }

  saveUp(){
    if (!this.upSf.valid) {
      this.msgSrv.error('必填项不能为空！');
      return;
    }
    this.isUpVisible = false;
    this.httpservice.lbservice('CLWH_MODI',{para: this.upSf.value}).then(resdata => {
      if(resdata.code<1)
      {
         this.msgSrv.error(resdata.errmsg);
      }
      else
      {
        this.msgSrv.success('修改成功');
        // this.st.reload();
        this.st.reload({DAD017: this.treeData.key})
      }
    })
  }

  update(rec){
    this.upData = rec;
    this.isUpVisible = true;    
  }

  del(rec){
    this.httpservice.lbservice('CLWH_DEL',{DAD017: rec.DAD017,DAD028: rec.DAD028}).then(resdata => {
      if(resdata.code<1)
      {
         this.msgSrv.error(resdata.errmsg);
      }
      else
      {
        this.msgSrv.success('删除成功');
        this.st.reload({DAD017: this.treeData.key})
        // this.st.reload();
      }
    })
  }

  /****************************事项材料标签维护**************************** */
  // tslint:disable-next-line: member-ordering
  bqUpdateVisible=false;
  bqUpdateSchema: SFSchema={properties:{}}


  defaultCheckedKeys=[];
  bqNodes=[];
  bqRec;
  handleBqCancel(){
    this.bqUpdateVisible=false;
  }
  handleBqOk(){
    const res=this.getCheckedNodes();
    this.httpservice.lbservice('SXCLWH_UCLBQS',{para:{DAD017: this.bqRec.DAD017,DAD028:this.bqRec.DAD028,DAD128:res}}).then(resdata => {
      if(resdata.code>0){
        this.msgSrv.success('保存成功');
        this.bqUpdateVisible=false;
      }else{
        this.msgSrv.error(resdata.errmsg);
      }
    })
  }

  showBqUpdate(rec:any){
    this.bqUpdateVisible=true;
    this.bqRec=rec;
    this.httpservice.lbservice('SXCLWH_QCLBQS',{para:{DAD017: rec.DAD017,DAD028:rec.DAD028}}).then(resdata => {
      if(resdata.code>0){
        if(resdata.message.list[0].bqNodes!==null){
          this.bqNodes=resdata.message.list[0].bqNodes;
        }
        if(resdata.message.list[0].checkedNodes!==null){
          this.defaultCheckedKeys=JSON.parse(resdata.message.list[0].checkedNodes);
        }
      }else{
        this.msgSrv.error(resdata.errmsg);
      }
    })
  }

  getCheckedNodes(){
    const DAD128=[];
    this.bqNodes.forEach(item=>{
      item.children.forEach(cl=>{
        if(cl.checked===true){
          DAD128.push(cl.key);
        }
      });
    });
    return JSON.stringify(DAD128);
  }

}
