import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { YwtxSxclwhComponent } from './sxclwh.component';

describe('YwtxSxclwhComponent', () => {
  let component: YwtxSxclwhComponent;
  let fixture: ComponentFixture<YwtxSxclwhComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ YwtxSxclwhComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(YwtxSxclwhComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
