import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { YwtxZxsxwhComponent } from './zxsxwh.component';

describe('YwtxZxsxwhComponent', () => {
  let component: YwtxZxsxwhComponent;
  let fixture: ComponentFixture<YwtxZxsxwhComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ YwtxZxsxwhComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(YwtxZxsxwhComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
