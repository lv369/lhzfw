import { Component, OnInit, ViewChild } from '@angular/core';
import { _HttpClient, ModalHelper } from '@delon/theme';
import { STColumn, STComponent } from '@delon/abc/table';
import { SFSchema, SFComponent } from '@delon/form';
import { GridComponent, HttpService, SupDic } from 'lbf';
import { NzMessageService } from 'ng-zorro-antd';

@Component({
  selector: 'app-ywtx-zxsxwh',
  templateUrl: './zxsxwh.component.html',
})
export class YwtxZxsxwhComponent implements OnInit {
  addFormData = {}; // 新增弹出框表单数据
  modiFormData = {}; // 修改弹出框表单数据
  queryFormData = {}; // 查询区表单数据
  showAddWindow = false; // 是否显示新增弹出框
  showModiWindow = false; // 是否显示修改弹出框
  searchSchema:SFSchema={
    properties: {
      DGB020: {
        type: 'string',
        title: '中心编码',
        enum: this.dic.getSFDic('DGB020'),
        ui: {
          widget: 'select',
          allowClear:true,
        }
      
      },
      AYW120:{
         type:'string',
         title:'事项大类',
         enum: this.dic.getSFDic('AYW120'),
         ui: {
           widget: 'select',
           allowClear:true,
         }
      } 

    },
    ui:{
      spanLabelFixed:100,
      grid:{span:6}
    }

  }


  // 添加 and 修改
  addSchema: SFSchema = {
    properties: {
      SEQ:{
        type:'number',
        title:'顺序',
        ui:{hidden:true}
      },
      DGB020: {
        type: 'string',
        title: '中心编码',
        enum: this.dic.getSFDic('DGB020'),
        ui: {
          widget: 'select',
          allowClear:true,
        }
      
      },
      AYW120:{
         type:'string',
         title:'大类名称',
         enum: this.dic.getSFDic('AYW120'),
         ui: {
           widget: 'select',
           allowClear:true,
         }
      } 
    },ui:{
          spanLabelFixed:80,
          grid:{span:24}
    },required:['AYW120','DGB020']
  };



  
   // 查询区表单
  @ViewChild('searchForm', { static: false }) searchForm: SFComponent;
  // 事项列表
  @ViewChild('st', { static: true }) st: GridComponent;
  // 事项新增表单
  @ViewChild('addWindow', { static: false }) addWindow: SFComponent;
    // 事项修改表单
    @ViewChild('modiWindow', { static: false }) modiWindow: SFComponent;

// 事项列表生成串
columns: STColumn[] = [
  // {
  //   title: '事项状态',
  //   index: 'AYW008',
  //   render: 'custom'
  // },
  { title: '中心编码', index: 'DGB021',dic:'DGB020'},
  { title: '大类名称', index: 'AYW120',dic:'AYW120'},

  {
    title: '操作区', buttons: [
      {
        text: '修改',
        icon: 'edit',
        click: (record: any) => {
          // 给修改编辑框赋值
          this.modiFormData = record;
          // 打开修改编辑框
          this.openWindow('modi');
        }
      },
      {
        text: '删除',
        icon: 'delete',
        click: (record: any) => this.delSave(record),
        pop: '确定要删除该事项吗？',
      },
      
    ]
  },
];


  constructor(private http: _HttpClient, 
    private modal: ModalHelper,
    private lbs :HttpService,
    public msgSrv: NzMessageService,
    private dic: SupDic,
    ) { }

  ngOnInit() { }

  // 弹出框nzOnOk绑定 
  addSave(){
     // 必输项校验
     if (!this.addWindow.valid) {
      this.msgSrv.error('必填项不能为空！');
      return;
    }
     // 调用事项新增服务
     this.lbs.lbservice('YWPZ_ADD', { para: this.addWindow.value }).then(resdata => {
      console.log(this.addWindow.value);
      if (resdata.code < 1) {
        this.msgSrv.error(resdata.errmsg);
      }
      else {
        // 关闭新增框
        this.closeWindow('add');
        // 新增框内容情况
        this.addFormData = {};
        this.msgSrv.success('事项新增成功');
        // 成功后，重新加载列表
        this.query();
      }

    })
   
  }
// 弹出框nzOnCancel绑定
  closeWindow(flag :string){
    if(flag==='add'){ this.showAddWindow=false}
    if(flag==='modi'){this.showModiWindow=false}
  }
 


    query(pi1=false) {

      if(pi1){
        this.st.pi=1;
      }
  

      this.st.reload(this.searchForm.value);
    }

  // 删除
    delSave(record:string){
      this.lbs.lbservice("SHIX_DEL", { para: record }).then(resdata => {
        if (resdata.code < 1) {
          this.msgSrv.error(resdata.errmsg);
        }
        else {
          this.msgSrv.success('事项删除成功');
          // 删除成功，重新加载
          this.query(true);
        }
      });

    }


 // 新增和修改弹出框
  openWindow(flag:string) {
    // 新增
    if(flag==='add'){
      this.showAddWindow = true;
    }
    // 修改
    else if(flag==='modi'){
      this.showModiWindow = true;
    }
  
  }


 // 事项修改保存事件
 modiSave() {
  // 必输项校验
  if (!this.modiWindow.valid) {
    this.msgSrv.error('必填项不能为空！');
    return;
  }

  // 调用事项修改服务
  this.lbs.lbservice('YWPZ_SX_upte', { para: this.modiWindow.value }).then(resdata => {
    console.log(this.modiWindow.value);
    if (resdata.code < 1) {
      this.msgSrv.error(resdata.errmsg);
    }
    else {
      // 关闭修改框
      this.closeWindow('modi');
      // 将修改框清空
      this.modiFormData = {};
      this.msgSrv.success('事项修改成功');
      // 成功后，重新加载列表
      this.query();
    }

  })

}


 // 事项状态切换
 changeSxState(sxbm: string) {
  this.lbs.lbservice("YWPZ_BG", { para: { DGB020: sxbm } }).then(resdata => {
    if (resdata.code < 1) {
      this.msgSrv.error(resdata.errmsg);
    }

  });
}


}
