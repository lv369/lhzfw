import { Component, OnInit, ViewChild } from '@angular/core';
import { SFSchema, SFComponent } from '@delon/form';
import { STColumn } from '@delon/abc';
import { GridComponent, HttpService, SupDic } from 'lbf';
import { NzMessageService, NzFormatEmitEvent } from 'ng-zorro-antd';

@Component({
  selector: 'app-sxlbwh',
  templateUrl: './sxlbwh.component.html',
  styleUrls: ['./sxlbwh.component.less']
})
export class SxlbwhComponent implements OnInit {
  searchValue = ''
  nodes = [];
  isAddDisabled = true;
  treeData;
  isVisible = false;
  isClVisible = false;
  isUpVisible = false;
  modalTitle = '新增材料';
  modalPzTite = '材料列表';
  param = {sname: 'CLWH_CLMLCX',queryparas: {}}
  sname = {
    add:'LBGL_SXCONDITIONADD',
    del:'LBGL_SXCONDITIONDEL'
  }
  upData;
  style = {height: (window.outerHeight-330)+'px',overflow: 'auto'}

  showSchema: SFSchema = {
    properties:{
      DAD017:{
        type: 'string',
        title: '事项主键',
        ui:
        {
          hidden: true
        }        
      },
      DAD004:{
        type: 'string',
        title: '权力编码',
        ui:{
          widget:'text',
          grid:{
            span:8
          }
        }
      },
      DAD031:{
        type: 'string',
        title: '事项名称',
        ui:{
          widget:'text',
          grid:{
            span:16
          }
        }
      },
      DAD032: {
        type: 'string',
        title: '对象主体',
        enum: this.supdic.getSFDic('DAD032'),
        readOnly: true,
        ui: {
          widget: 'radio',
          grid:{
            span:8
          }
        }
      },
      DGB010: {
        type: 'string',
        title: '所属部门',
        ui:{
          widget:'text',
          grid:{
            span:8
          }
        }
      },
      DAD011: {
        type: 'string',
        title: '事项类型',
        ui:{
          widget:'text',
          grid:{
            span:8
          }
        }
      },      
    },
    ui:{
      spanLabelFixed: 90,
      grid:{
        span:24
      }
    }
  };

  addSchema: SFSchema = {
    properties: {
      DAD017: {
        type: 'number',
        title: '事项编码',
        enum: this.supdic.getSFDic('DAD017'),
        ui: {
          widget: 'select',
          grid: {
            span: 12
          }
        },
        readOnly: true,
      },
      DAD086: {
        type: 'number',
        title: '条件',
        enum: this.supdic.getSFDic('DAD086'),
        ui: {
          widget: 'select',
          grid: {
            span: 6
          }
        }      
      },
      SEQ: {
        type: 'number',
        title: '顺序',
        ui: {
         
          grid: {
            span: 6
          }
        }      
      },

    },
    ui: {
      spanLabelFixed: 90,
      grid: {
        span: 24
      }
    },
    required: ['DAD017','SEQ','DAD086']
  }

  upSchema: SFSchema = {
    properties: {
      DAD017: {
        type: 'number',
        title: '事项编码',
        enum: this.supdic.getSFDic('DAD017'),
        ui: {
          widget: 'select',
          grid: {
            span: 12
          }
        },
        readOnly: true,
      },
      DAD028: {
        type: 'number',
        title: '材料编码',
        ui: {
          grid: {
            span: 6
          }
        },
        readOnly: true
      },      
  
    },
    ui: {
      spanLabelFixed: 90,
      grid: {
        span: 24
      }
    },
    required: ['AAE013']
  }

  columns: STColumn[] = [
    { title: '事项名称', index: 'DAD017', dic: 'DAD017' },
    { title: '条件名称', index: 'DAD087'},
    { title: '条件ID', index: 'DAD086' },
    { title: '顺序', index: 'SEQ' },
    {
      title: '操作区',fixed:'right',width:100,
      buttons: [
        {
          text: '删除',

          click: (record) => this.del(record),
          pop: true,
          popTitle: '是否删除？'
        }
      ]
    }
  ]

  ClColumns: STColumn[] = [
    { title: '材料编码', index: 'DAD028'},// ,dic: 'DAH001'},
    { title: '材料名称', index: 'DAD029'},
    { title: '受理渠道', index: 'DAD030',dic:'DAD030'},
    { title: '材料形式', index: 'DAD034',dic: 'DAD034'},
    { title: '共享范围', index: 'DAD035',dic:'DAD035'},
    { title: '受理标准', index: 'DAD085'},
    { title: '填报须知', index: 'DAD066'},
    { title: '材料下载', index: 'DAD067'},
    {
      title: '操作区',
      buttons: [
          {
            text: '添加',
            click: (record) => this.tjpz(record)
          }
      ]
    }
  ]

  @ViewChild('showSf',{static:false}) showSf: SFComponent;
  @ViewChild('addSf',{static:false}) addSf: SFComponent;
  @ViewChild('upSf',{static:false}) upSf: SFComponent;
  @ViewChild('st',{static: false}) st: GridComponent;
  constructor(private httpservice: HttpService,
    private supdic: SupDic,
    private msgSrv: NzMessageService) { }

  ngOnInit() { 
    this.initDeptTree();
  }

  // 初始化机构树
  initDeptTree(){
    this.httpservice.lbservice('BDWH_TREE',{}).then(res=>{
      this.nodes = res.message.list;
    });
 }

  // 功能树单击事件
  nzEvent(event: NzFormatEmitEvent): void {
    this.isAddDisabled = true
    this.showSf.refreshSchema();
    this.treeData = event.node.origin;
    // console.log(this.addSf);

    
    // console.log(event);
    if(this.treeData.isLeaf)
    {
        // 编辑框表单赋值
        this.showSf.setValue('/DAD017',this.treeData.key);
        this.showSf.setValue('/DAD031',this.treeData.title);
        this.showSf.setValue('/DAD004',this.treeData.DAD004);
        this.showSf.setValue('/DGB010',this.treeData.DGB010);
        this.showSf.setValue('/DAD032',this.treeData.DAD032);
        this.showSf.setValue('/DAD011',this.treeData.DAD011);
        this.isAddDisabled = false;
        this.st.reload({DAD017: this.treeData.key})
    }    
  }

  add(){
    this.addSf.refreshSchema();
    this.addSf.setValue('/DAD017',this.treeData.key);
    this.isVisible = true
  }

  tjpz(rec){
      this.addSf.setValue('/custom',rec.DAD028);
      this.addSf.setValue('/DAD029',rec.DAD029);
      this.addSf.setValue('/DAD105',rec.DAD034);
      this.addSf.setValue('/DAD125',rec.DAD066);
      this.addSf.setValue('/DAD124',rec.DAD085);
      this.isClVisible = false;
  }

  CLAdd(){
    this.isClVisible = true
  }

  handleAddCancel(){
    this.isVisible = false
  }

  handleAddOk(){
    this.isVisible = false    
    this.httpservice.lbservice(this.sname.add,{para: this.addSf.value}).then(resdata => {
      if(resdata.code<1)
      {
         this.msgSrv.error(resdata.errmsg);
      }
      else
      {
        this.msgSrv.success('添加成功');     
      }
    })
    // this.st.reload({DAD017: this.treeData.key})
    this.st.st.reload();
  }

  handlePzCancel(){
    this.isClVisible = false;
  }

  handleUpCancel(){
    this.isUpVisible = false;
  }

  saveUp(){
    this.isUpVisible = false;
    this.httpservice.lbservice('CLWH_MODI',{para: this.upSf.value}).then(resdata => {
      if(resdata.code<1)
      {
         this.msgSrv.error(resdata.errmsg);
      }
      else
      {
        this.msgSrv.success('修改成功');
        this.st.st.reload();
      }
    })
  }

  update(rec){
    this.upData = rec;
    this.isUpVisible = true;    
  }

  del(rec){
    const para = {
      DAD017: rec.DAD017,
      DAD086: rec.DAD086
    }
    this.httpservice.lbservice(this.sname.del,{para}).then(resdata => {
      if(resdata.code<1)
      {
         this.msgSrv.error(resdata.errmsg);
      }
      else
      {
        this.msgSrv.success('删除成功');
        this.st.st.reload();
      }
    })
  }
}
