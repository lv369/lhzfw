import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { YwtxSxbdwhComponent } from './sxbdwh.component';

describe('YwtxSxbdwhComponent', () => {
  let component: YwtxSxbdwhComponent;
  let fixture: ComponentFixture<YwtxSxbdwhComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ YwtxSxbdwhComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(YwtxSxbdwhComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
