import { Component, OnInit , ViewChild ,TemplateRef, EventEmitter } from '@angular/core';
import { _HttpClient, ModalHelper } from '@delon/theme';
import { NzLayoutModule } from 'ng-zorro-antd/layout';
import { NzButtonModule, NzButtonComponent  } from 'ng-zorro-antd/button';
import { NzFormatEmitEvent  } from 'ng-zorro-antd/core';
import { NzMessageService, NzModalRef, NzModalService } from 'ng-zorro-antd';
import { FormBuilder, FormControl, FormGroup  } from '@angular/forms';
import { DelonFormModule,SFSchema , SFComponent , WidgetRegistry, SFSchemaEnumType } from '@delon/form';
import { HttpService, SupDic, GridComponent } from 'lbf';
import { Subject } from 'rxjs';
import { STColumn, STColumnTag, STColumnBadge } from '@delon/abc';


@Component({
  selector: 'app-ywtx-sxbdwh',
  templateUrl: './sxbdwh.component.html',
  styleUrls: ['./sxbdwh.less']
})
export class YwtxSxbdwhComponent implements OnInit {
  style = {height: (window.outerHeight-330)+'px',overflow: 'auto'}
  showSchema: SFSchema = {
    properties:{
      DAD017:{
        type: 'string',
        title: '事项主键',
        ui:
        {
          hidden: true
        }
        
      },
      DAD004:{
        type: 'string',
        title: '权力编码',
        ui:{
          widget:'text',
          grid:{
            span:8
          }
        }
      },
      DAD031:{
        type: 'string',
        title: '事项名称',
        ui:{
          widget:'text',
          grid:{
            span:16
          }
        }
      },
      DAD032: {
        type: 'string',
        title: '对象主体',
        enum: this.supDic.getSFDic('DAD032'),
        readOnly: true,
        ui: {
          widget: 'radio',
          grid:{
            span:8
          }
        }
      },      
      
      DGB010: {
        type: 'string',
        title: '所属部门',
        ui:{
          widget:'text',
          grid:{
            span:8
          }
        }
      },
      DAD011: {
        type: 'string',
        title: '事项类型',
        ui:{
          widget:'text',
          grid:{
            span:8
          }
        }
      },
      
    },
    ui:{
      spanLabelFixed: 90,
      grid:{
        span:24
      }
    }
  };

  BADGE: STColumnBadge = {
    0: { text: '否', color: 'success' },
    1: { text: '是', color: 'processing' },
  }; 
  
  
  TAG: STColumnTag = {
    0: { text: '0', color: 'purple' },
    1: { text: '1', color: 'green' },
    2: { text: '2', color: 'red' },
    3: { text: '3', color: 'blue' },
    4: { text: '4', color: 'orange' },
    5: { text: '5', color: 'cyan' },
    6: { text: '6', color: 'geekblue' },
    7: { text: '7', color: 'volcano' },
    8: { text: '8', color: 'magenta' },
    9: { text: '9', color: 'lime' },
  };


  columns: STColumn[] = [
    { title: '顺序',  index: 'DAD160' ,type: 'tag', tag: this.TAG },
    { title: '字段编码', index: 'DAD153' },
    { title: '字段名称',  index: 'DAD154' },
    /*{ title: '数据标准',  index: 'DAD155' },*/
    { title: '控件类型', index: 'DAD156',dic: 'DAD156'  },
    { title: '必输项',  index: 'DAD157' ,type: 'yn',yn:{truth:'1'} },
    { title: '默认值',  index: 'DAD200' },
    { title: '跨列数',  index: 'DAD158',type:'number' },
    { title: '跨行数',  index: 'DAD159',type:'number' },
    { title: '操作区', buttons:[
      { 
        text: '修改' ,
        icon : 'edit',
        click: (record: any) => this.modi(record),
      },
      { 
        text: '移除',
        icon: 'delete',
        click: (record: any) => this.del(record),
        pop: true,
        popTitle: '确认要将该表单项移除吗？',
      },
    ]},
  ];

  addSchema:SFSchema = {
    properties:{
      DAD017:{
        type: 'string',
        title: '事项主键',
        ui:
        {
          hidden: true
        }      
      },
      DAD153:{
        type: 'string',
        title: '字段编码',
        ui:{
          grid:{
            span: 12
          }
        }
      },
      DAD154:{
        type: 'string',
        title: '字段名称',
        ui:{
          grid:{
            span: 12
          }
        }
      },
      /*
      DAD155:{
        type: 'string',
        title: '数据标准',
        ui:{
          grid:{
            span: 12
          }
        }
      },
      */
      DAD156:{
        type: 'string',
        title: '控件类型',
        enum: this.supDic.getSFDic('DAD156'),
        ui:{
          widget: 'select',
          grid:{
            span: 12
          }
        }
      },
      DAD200:{
        type: 'string',
        title: '默认值',
        ui:{
          grid:{
            span: 12
          }
        }
      },
      DAD160:{
        type: 'number',
        title: '顺序',
        default: 1
      },
      
      
      DAD158:{
        type: 'number',
        title: '跨列数',
        default: 1
      },
      DAD159:{
        type: 'number',
        title: '跨行数',
        default: 1
      },
      DAD157: {
        type: 'string',
        title: '必输项',
        enum: [{ label: '是', value: '1' }, { label: '否', value: '0' }],
        ui: {
          widget: 'radio',
        
          grid:{
            span:24
          }
        },
        default: '1'
      },
      
    },
    required:[ "DAD153" ,"DAD154","DAD155","DAD156","DAD157","DAD158","DAD159","DAD160" ],
    ui:{
      spanLabelFixed:80,
      grid:{
        gutter:20,
        span:12
      }
    }
  };
  
  modiSchema:SFSchema =  {
    properties:{
      DAD017:{
        type: 'string',
        title: '事项主键',
        ui:
        {
          hidden: true
        }      
      },
      DAD153:{
        type: 'string',
        title: '字段编码',
        readOnly: true,
        ui:{
          grid:{
            span: 12
          }
        }
      },
      DAD154:{
        type: 'string',
        title: '字段名称',
        ui:{
          grid:{
            span: 12
          }
        }
      },
      /*
      DAD155:{
        type: 'string',
        title: '数据标准',
        ui:{
          grid:{
            span: 12
          }
        }
      },
      */
      DAD156:{
        type: 'string',
        title: '控件类型',
        enum: this.supDic.getSFDic('DAD156'),
        ui:{
          widget: 'select',
          grid:{
            span: 12
          }
        }
      },
      DAD200:{
        type: 'string',
        title: '默认值',
        ui:{
          grid:{
            span: 12
          }
        }
      },
      DAD160:{
        type: 'number',
        title: '顺序',
        default: 1
      },
      
      
      DAD158:{
        type: 'number',
        title: '跨列数',
        default: 1
      },
      DAD159:{
        type: 'number',
        title: '跨行数',
        default: 1
      },
      DAD157: {
        type: 'string',
        title: '必输项',
        enum: [{ label: '是', value: '1' }, { label: '否', value: '0' }],
        ui: {
          widget: 'radio',
         
          grid:{
            span:24
          }
        },
        default: '1'
      },
      
    },
    required:[ "DAD153" ,"DAD154","DAD155","DAD156","DAD157","DAD158","DAD159","DAD160" ],
    ui:{
      spanLabelFixed:80,
      grid:{
        gutter:20,
        span:12
      }
    }
  };
  searchValue = '';
  gutter = 24;
  nodes:any = [];
  treeData:any;
  showModiWindow = false;
  isAddDisabled = true;
  isCopyDisabled = true;
  formData ={};
  modiFormData = {};
  isVisible = false;
  modalTitle = '添加表单项';
  curDAD017 = 0;
  

  @ViewChild('showSf',{static:false}) showSf: SFComponent;
  @ViewChild('addWindow',{static:false}) addWindow: SFComponent;
  @ViewChild('modiWindow',{static:false}) modiWindow: SFComponent;
  @ViewChild('btnSave',{static:false}) btnSave: NzButtonComponent;
  @ViewChild('st', { static: true }) st: GridComponent;
  
  constructor(private modal:ModalHelper,
    private lbs:HttpService ,
    private supDic:SupDic ,
    public msgSrv: NzMessageService) {
   }

  
  // 初始化机构树
  initDeptTree(){
     this.lbs.lbservice('BDWH_TREE',{}).then(res=>{
       console.log(res.message.list);
       this.nodes = res.message.list;
     });
  }


  
  // 功能树单击事件
  nzEvent(event: NzFormatEmitEvent): void {
    this.treeData = event.node.origin;
    // console.log(this.addSf);
    // console.log(event);
    if(this.treeData.isLeaf)
    {
        this.curDAD017 = this.treeData.key;
        // 编辑框表单赋值
        this.showSf.setValue('/DAD017',this.treeData.key);
        this.showSf.setValue('/DAD031',this.treeData.title);
        this.showSf.setValue('/DAD004',this.treeData.DAD004);
        this.showSf.setValue('/DGB010',this.treeData.DGB010);
        this.showSf.setValue('/DAD032',this.treeData.DAD032);
        this.showSf.setValue('/DAD011',this.treeData.DAD011);
        // console.log(this.sf.getValue('/SEQ'));
        // 如果是一件事，则 子项表单提取 按钮 显示
        if(this.treeData.SXLB==='1')
        {
          this.isAddDisabled = true;
        }
        else
        {
          this.isAddDisabled = false;
        }
        // this.isAddDisabled = false;
        this.query();
    }
    
  }
  
  copy(){
    this.lbs.lbservice("BDWH_COPY",{para:{ DAD017:this.treeData.key}}).then(resdata=>{
      if(resdata.code<1)
      {
         this.msgSrv.error(resdata.errmsg);
      }
      else
      {
        this.msgSrv.success(resdata.message);
        console.log(resdata);
        this.query();
      }
   });
    
  }

  add(){
    this.formData = { DAD017:this.treeData.key }
    this.isVisible = true;
  }

  query(pi1=false){
    if(pi1){
      this.st.pi=1;
    }
    this.st.reload({DAD017: this.curDAD017 });
  }
 

  modi(rowData:any){
    this.showModiWindow = true;
    this.modalTitle = '表单项修改';
    this.modiFormData = rowData;
  }

  del(rowData:any){
     this.lbs.lbservice("BDWH_DEL",{para:{ DAD017:rowData.DAD017,DAD153:rowData.DAD153}}).then(resdata=>{
        if(resdata.code<1)
        {
           this.msgSrv.error(resdata.errmsg);
        }
        else
        {
          this.msgSrv.success('表单项移除成功');
          this.query();
        }
     });
  }

  handleCancel(){
    this.isVisible = false;
    this.formData = {};
  }

  handleOk()
  {
    if(!this.addWindow.valid)
    {
        this.msgSrv.error('必填项不能为空！');
        return;
    }

    this.lbs.lbservice('BDWH_ADD',{para:this.addWindow.value}).then(resdata=>{
      console.log(this.addWindow.value);
       if(resdata.code<1)
       {
         this.msgSrv.error(resdata.errmsg);
       }
       else
       {
        this.isVisible = false;
        this.formData = {};
        this.msgSrv.success('表单项添加成功');
        this.query();
       }

    });
  }

  modiHandleCancel(){
    this.showModiWindow = false;
  }

  modiHandleOk(){
    
    if(!this.modiWindow.valid)
    {
        this.msgSrv.error('必填项不能为空！');
        return;
    }

    this.lbs.lbservice('BDWH_MODI',{para:this.modiWindow.value}).then(resdata=>{
      console.log(this.modiWindow.value);
       if(resdata.code<1)
       {
         this.msgSrv.error(resdata.errmsg);
       }
       else
       {
        this.showModiWindow = false;
        this.modiFormData = {};
        this.msgSrv.success('表单项修改成功');
        this.query();
       }

    })
    
  }


  ngOnInit() { 
    this.initDeptTree();    
  }

  


  

  
}
