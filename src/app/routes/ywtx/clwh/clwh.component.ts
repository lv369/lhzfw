
import { Component, OnInit, ViewChild, EventEmitter ,Input, TemplateRef } from '@angular/core';
import { _HttpClient, ModalHelper } from '@delon/theme';
import { STColumn, STComponent, STPage, STChange, STColumnFilterMenu } from '@delon/abc';
import { SFSchema,SFComponent, FormProperty, PropertyGroup } from '@delon/form';
import { GridComponent, PopdetailComponent, HttpService, SupDic } from 'lbf';
import { NzMessageService } from 'ng-zorro-antd';
import { Router } from '@angular/router';

@Component({
  selector: 'app-ywtx-clwh',
  templateUrl: './clwh.component.html',
})
export class YwtxClwhComponent implements OnInit {
      params = {clSname:'CLWH_QCL',form:{}};
       // 展示
      @ViewChild('clSF',{static:false}) clSF: SFComponent;
      @ViewChild('clST',{static:false}) clST: GridComponent;
      clSchema: SFSchema={properties:{}};
      clColumns: STColumn[] = [
        {title:'材料ID',index:'DAD028',width:100},
        {title:'材料名称',index:'DAD029',width:100},
        {title:'提交方式',index:'DAD030',dic:'DAD030',width:100},
        {title:'材料形式',index:'DAD034',dic:'DAD034',width:100},
        {title:'材料范围',index:'DAD035',dic:'DAD035',width:100},
        {title:'材料样本',index:'DAD043',width:100},
        {title:'受理标准',index:'DAD085',dic:'DAD085',width:100},
        {title:'填报须知',index:'DAD066',width:100},
        {title:'材料下载',index:'DAD067',width:100},
        {title:'A4文字说明',index:'DAD068',width:100},
        { title: '操作区',fixed:'right',width:100,
            buttons:[
             { 
               text: '修改',
               icon: 'edit',
               click: (record: any) => this.clUpdate(record),
             },
             { 
              text: '删除',
              icon: 'delete',
              click: (record: any) => this.clDelete(record),
              pop:true,
              popTitle:'确定删除该材料?',
            },
           ]},
         ];
         // 新建
        @ViewChild('clAddSF',{static:false}) clAddSF: SFComponent;
        clAddVisible=false;
        clAddSchema: SFSchema={properties:{}};
  
        // 修改
        @ViewChild('clUpSF',{static:false}) clUpSF: SFComponent;
        clUpVisible=false;
        clUpSchema: SFSchema={properties:{}};
  
  
  
  
  
  /*------------------------------------------------------------------------------------------------------------------------------------------------- */
  constructor(
    public msgSrv: NzMessageService,
    private lbservice: HttpService,
    private supdic: SupDic,
  ) { 
  }
  
    ngOnInit() { 
      this.params={
        clSname:'CLWH_QCL',
        form:{},
      }
      this.initclSchema();
    }
  
  
    checkValueNull(e:string){
      if(e==null||e.length===0){
        return true;
      }
      return false;
    }
  
    checkValueNumber(sch:string,tar:string,src:string,title:string){
      if(this.checkValueNull(this[sch].value[src])){
        return true;
      }
      this[sch].value[tar]=Number(this[sch].value[src]);
      if(title!=='none' && isNaN(this[sch].value[tar])){
        this.msgSrv.error(title+'只允许为数字');
        return false;
      }
      return true;
    }
  
    /**************************查询***************************** */
    initclSchema(){
      this.clSchema= {
        properties: {
          DAD028_S:{
            type:'string',
            title:'材料编号',
          },
          DAD029:{
            type:'string',
            title:'材料名称',
          },
          btn:{
            type:'string',
            title:'',
            ui:{
              widget:'custom',
            }
          }
        },
        ui: {
          spanLabelFixed: 100,
          grid: {span: 8 }
        },
      };
    }
    
    clQuery(pi1=false){
      if(pi1){
        this.clST.pi=1;
      }
      this.checkValueNumber('clSF','DAD028','DAD028_S','none');
      this.clST.reload(this.clSF.value);
    }
  
  
    /**************************新建*************************** */
    clAdd(){
      this.clAddVisible=true;
      this.initclAddSchema();
    }
  
    clAddCancel(){
      this.clAddVisible=false;
    }
  
    clAddOk(){
      if(this.checkValueNull(this.clAddSF.value.DAD029)){
        this.msgSrv.error('材料名称不可为空');
        return;
      }
      this.lbservice.lbservice("CLWH_ACL",{para:this.clAddSF.value}).then(resdata=>{
        if(resdata.code>0){
          this. clQuery();
          this.clAddVisible=false;
          this.msgSrv.success('添加成功');
        }else{
          this.msgSrv.error(resdata.errmsg);
        }
      });
    }
  
    initclAddSchema(){
      this.clAddSchema= {
        properties: {
        DAD029:{
            type:'string',
            title:'材料名称'
        },
        DAD030:{
            type:'string',
            title:'提交方式',
            enum:this.supdic.getSFDic('DAD030'),
            ui:{
              widget:'select',
            }
        },
        DAD034:{
            type:'string',
            title:'材料形式',
            enum:this.supdic.getSFDic('DAD034'),
            ui:{
              widget:'select',
            }
        },
        DAD035:{
            type:'string',
            title:'材料范围'
        },
        DAD043:{
            type:'string',
            title:'材料样本'
        },
        DAD085:{
            type:'string',
            title:'受理标准',
            enum:this.supdic.getSFDic('DAD085'),
            ui:{
              widget:'select',
              allowClear:true,
            }
        },
        DAD066:{
            type:'string',
            title:'填报须知'
        },
        DAD067:{
            type:'string',
            title:'材料下载'
        },
        DAD068:{
            type:'string',
            title:'A4文字说明',
            ui:{
              widget:'textarea',
              autosize:{ minRows: 2, maxRows: 6 },
              grid: {span: 24 }
            }
        }
      },
        ui: {
          spanLabelFixed: 100,
          grid: {span: 12 }
        },
        required:["DAD029"]
      };
    }
  
    /***************************修改****************************** */
    clUpdate(rec:any){
      this.initclUpSchema(rec);
      this.clUpVisible=true;
    }
  
    clUpCancel(){
      this.clUpVisible=false;
    }
  
    clUpOk(){
      if(this.checkValueNull(this.clUpSF.value.DAD029)){
        this.msgSrv.error('材料名称不可为空');
        return;
      }
      this.lbservice.lbservice("CLWH_UCL",{para:this.clUpSF.value}).then(resdata=>{
        if(resdata.code>0){
          this. clQuery();
          this.clUpVisible=false;
          this.msgSrv.success('修改成功');
        }else{
          this.msgSrv.error(resdata.errmsg);
        }
      });
    }
  
    initclUpSchema(rec:any){
      this.clUpSchema= {
        properties: {
          DAD028:{
            type:'string',
            title:'',
            default:rec.DAD028,
            ui:{
              hidden:true,
            }
        },
          DAD029:{
            type:'string',
            title:'材料名称',
            default:rec.DAD029,
        },
        DAD030:{
            type:'string',
            title:'提交方式',
            default:rec.DAD030,
            enum:this.supdic.getSFDic('DAD030'),
            ui:{
              widget:'select',
            }
        },
        DAD034:{
            type:'string',
            title:'材料形式',
            default:rec.DAD034,
            enum:this.supdic.getSFDic('DAD034'),
            ui:{
              widget:'select',
            }
        },
        DAD035:{
            type:'string',
            title:'材料范围',
            default:rec.DAD035,
        },
        DAD043:{
            type:'string',
            title:'材料样本',
            default:rec.DAD043,
        },
        DAD085:{
            type:'string',
            title:'受理标准',
            default:rec.DAD085,
            enum:this.supdic.getSFDic('DAD085'),
            ui:{
              widget:'select',
              allowClear:true,
            }
        },
        DAD066:{
            type:'string',
            title:'填报须知',
            default:rec.DAD066,
        },
        DAD067:{
            type:'string',
            title:'材料下载',
            default:rec.DAD067,
        },
        DAD068:{
            type:'string',
            title:'A4文字说明',
            default:rec.DAD068,
            ui:{
              widget:'textarea',
              autosize:{ minRows: 2, maxRows: 6 },
              grid: {span: 24 }
            }
        }
      },
        ui: {
          spanLabelFixed: 100,
          grid: {span: 12 },
        },
        required:["DAD029"]
      };
    }

    /***************************删除****************************** */
    clDelete(rec:any){
      this.lbservice.lbservice("CLWH_DCL",{para:{DAD028:rec.DAD028}}).then(resdata=>{
        if(resdata.code>0){
          this.msgSrv.success('删除成功');
          this.clQuery();
        }else{
          this.msgSrv.error(resdata.errmsg);
        }
      });
    }
  
      
  }
  
