import { Component, OnInit , ViewChild ,TemplateRef } from '@angular/core';
import { _HttpClient, ModalHelper } from '@delon/theme';
import { NzLayoutModule } from 'ng-zorro-antd/layout';
import { NzButtonModule, NzButtonComponent  } from 'ng-zorro-antd/button';
import { NzFormatEmitEvent  } from 'ng-zorro-antd/core';
import { NzMessageService, NzModalRef, NzModalService } from 'ng-zorro-antd';
import { FormBuilder, FormControl, FormGroup  } from '@angular/forms';
import { DelonFormModule,SFSchema , SFComponent , WidgetRegistry, SFSchemaEnumType, SFTextareaWidgetSchema, SFRadioWidgetSchema } from '@delon/form';
import { HttpService, SupDic, GridComponent } from 'lbf';
import { Subject } from 'rxjs';
import { STColumn, STColumnTag, STColumnBadge, STComponent } from '@delon/abc';

@Component({
  selector: 'app-ywtx-hjrywh',
  templateUrl: './hjrywh.component.html',
  styleUrls: ['./hjrywh.component.less']
})
export class YwtxHjrywhComponent implements OnInit {
  searchValue = ''
  nodes = [];
  treeData;
  hjlist: [];
  params = {sname: 'YWTX_YHQUERY',queryparas: {}}
  style = {height: (window.outerHeight-330)+'px',overflow: 'auto'}

  showSchema: SFSchema = {
    properties: {
      HJMC: {
        type: 'string',
        enum: this.hjlist,
        title: '环节名称',
        ui: {
          widget: 'radio',
          change: (value) =>this.hjchange(value)
        }
    }
  }
}

  yhColumns: STColumn[] = [
    {title: '所属机构',index: 'DGB020',dic: 'DGB020'},
    {title: '所属科室',index: 'DGB040',dic: 'DGB040'},
    {title: '所属岗位',index: 'ROLEID',dic: 'ROLEID'},
    {title: '登录名',index: 'UNAME'},
    {title: '用户名',index: 'NAME'},
    {title: '手机号码',index: 'PHONE'},
  ]


  @ViewChild('showsf',{static: false}) showsf: SFComponent;
  @ViewChild('st',{static: false}) st: GridComponent;
  constructor(private httpservice: HttpService,
    private supdic: SupDic,
    private msgSrv: NzMessageService) { }

  ngOnInit() { 
    this.initDeptTree();
  }

  // 初始化机构树
  initDeptTree(){
    this.httpservice.lbservice('BDWH_TREE',{}).then(res=>{
      console.log(res.message.list);
      this.nodes = res.message.list;
    });
 }

  // 功能树单击事件
  nzEvent(event: NzFormatEmitEvent): void {
    if(event.node.origin !== this.treeData){
      this.st.reload({});
    }
    this.treeData  = event.node.origin;
    this.httpservice.lbservice('YWTX_HJQUERY',{DAD017: this.treeData.DAD017}).then(res => {
      if(res.code > 0){
         this.hjlist = res.message.list;
         this.initHj(this.hjlist);
      }
    })
  }

  initHj(hjlist){
    if(hjlist.length > 0){
      this.showSchema = {
        properties: {
          HJMC: {
            type: 'string',
            title: '环节名称',
            enum: hjlist,
            ui: {
              widget: 'radio',
              change: (value) =>this.hjchange(value)
            },
            default: hjlist[0].value
          }
        },
        ui: {
          spanLabelFixed: 100,
          grid: {
            span: 8
          }
        }
      }
  
      this.st.reload({DAD017: this.treeData.DAD017,DAD090: hjlist[0].value})
    }else{
      this.showSchema = {
        properties: {
          HJMC: {
            type: 'string',
            title: '环节名称',
            enum: hjlist,
            ui: {
              widget: 'radio',
              change: (value) =>this.hjchange(value)
            }
          }
        },
        ui: {
          spanLabelFixed: 100,
          grid: {
            span: 8
          }
        }
      }
    }
      
  }

  hjchange(data){
    console.log(data)
    this.st.reload({DAD017: this.treeData.DAD017,DAD090: data})
  }

  log(data){
    console.log(data);
    this.st.reload({DAD017: this.treeData.DAD017,HJLIST: data})
  }
}
