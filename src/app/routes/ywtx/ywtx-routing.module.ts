import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { YwtxZskwhComponent } from './zskwh/zskwh.component';
import { YwtxSlqdwhComponent } from './slqdwh/slqdwh.component';
import { YwtxClwhComponent } from './clwh/clwh.component';
import { YwtxCltjwhComponent } from './cltjwh/cltjwh.component';
import { YwtxSxbdwhComponent } from './sxbdwh/sxbdwh.component';
import { YwtxSxclwhComponent} from './sxclwh/sxclwh.component'
import { SxlbwhComponent } from './sxlbwh/sxlbwh.component';
import { YwtxHjrywhComponent } from './hjrywh/hjrywh.component';
import { YwtxZxsxwhComponent } from './zxsxwh/zxsxwh.component';

const routes: Routes = [

  { path: 'zskwh', component: YwtxZskwhComponent },
  { path: 'slqdwh', component: YwtxSlqdwhComponent },
  { path: 'clwh', component: YwtxClwhComponent },
  { path: 'cltjwh', component: YwtxCltjwhComponent },
  { path: 'sxbdwh', component: YwtxSxbdwhComponent },
  { path: 'sxclwh', component: YwtxSxclwhComponent },
  { path: 'sxlbwh', component: SxlbwhComponent }
,
  { path: 'hjrywh', component: YwtxHjrywhComponent },
  { path: 'zxsxwh', component: YwtxZxsxwhComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class YwtxRoutingModule { }
