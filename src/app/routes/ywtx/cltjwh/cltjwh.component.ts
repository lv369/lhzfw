import { Component, OnInit, ViewChild, EventEmitter ,Input, TemplateRef } from '@angular/core';
import { _HttpClient, ModalHelper } from '@delon/theme';
import { STColumn, STComponent, STPage, STChange, STColumnFilterMenu } from '@delon/abc';
import { SFSchema,SFComponent, FormProperty, PropertyGroup } from '@delon/form';
import { GridComponent, PopdetailComponent, HttpService, SupDic } from 'lbf';
import { NzMessageService } from 'ng-zorro-antd';
import { Router } from '@angular/router';

@Component({
  selector: 'app-ywtx-cltjwh',
  styleUrls:['./cltjwh.component.less'],
  templateUrl: './cltjwh.component.html',
})
export class YwtxCltjwhComponent implements OnInit {
        params = {cltjSname:'CLWH_QCL',form:{}};
         // 展示
        @ViewChild('cltjSF',{static:false}) cltjSF: SFComponent;
        @ViewChild('cltjST',{static:false}) cltjST: GridComponent;
        cltjSchema: SFSchema={properties:{}};
        listDataset=[];
        listPage:{
          pageIndex:1,
          pageSize:8,
          pageTotal:0,
        }
           // 新建
          @ViewChild('cltjAddSF',{static:false}) cltjAddSF: SFComponent;
          cltjAddVisible=false;
          cltjAddSchema: SFSchema={properties:{}};
    
          // 修改
          @ViewChild('cltjUpSF',{static:false}) cltjUpSF: SFComponent;
          cltjUpVisible=false;
          cltjUpSchema: SFSchema={properties:{}};
    
    
    
    
    
    /*------------------------------------------------------------------------------------------------------------------------------------------------- */
    constructor(
      public msgSrv: NzMessageService,
      private lbservice: HttpService,
      private supdic: SupDic,
    ) { 
    }
    
      ngOnInit() { 
        this.listPage={
          pageIndex:1,
          pageSize:8,
          pageTotal:0,
        }
        this.initcltjSchema();
        this.initListDataset();
      }

      initListDataset(){
        this.lbservice.lbservice("CLTJWH_QCLTJ",{para:this.getInitPara()
          ,page:{PAGESIZE:this.listPage.pageSize,PAGEIDX:this.listPage.pageIndex}}).then(
          resdata=>{
            if(resdata.code>0){
              this.listDataset=resdata.message.list;
              this.listPage.pageTotal=resdata.page.COUNT;
              this.listDataset.forEach(item=>{
                const tjList=JSON.parse(item.DAA003);
                item.tjList=tjList;
              });
            }else{
              this.msgSrv.error(resdata.errmsg);
            }
          }
        );
      }

      getInitPara(){
        if(this.cltjSF===undefined){
          return {};
        }
        return this.cltjSF.value;
      }
    
    
      checkListNull(list,itemName){
        list.forEach(item=>{
          if(this.checkValueNull(item[itemName])){
            return true;
          }
        });
        return false;
      }

      checkValueNull(e:string){
        if(e==null||e.length===0){
          return true;
        }
        return false;
      }
    
      checkValueNumber(sch:string,tar:string,src:string,title:string){
        if(this.checkValueNull(this[sch].value[src])){
          return true;
        }
        this[sch].value[tar]=Number(this[sch].value[src]);
        if(title!=='none' && isNaN(this[sch].value[tar])){
          this.msgSrv.error(title+'只允许为数字');
          return false;
        }
        return true;
      }

    
      /**************************查询***************************** */
      pageChange(pi){
        this.listPage.pageIndex=pi;
        this.cltjQuery();
      }
      initcltjSchema(){
        this.cltjSchema= {
          properties: {
            DAC004:{
              type:'string',
              title:'条件编号',
            },
            DAA002:{
              type:'string',
              title:'条件类别',
            },
            DAD017:{
              type:'string',
              title:'所属事项',
              ui:{
                widget:'custom',
              }
            }
          },
          ui: {
            spanLabelFixed: 100,
            grid: {span: 8 }
          },
        };
      }
      
      cltjQuery(){
        // this.checkValueNumber('cltjSF','DAD028','DAD028_S','none');
        this.cltjSF.value.DAD017=this.qSelectOpt;
        this.initListDataset();
      }
    
    
      /**************************新建*************************** */
      cltjAdd(){
        this.cltjAddVisible=true;
        this.initcltjAddSchema();
      }
    
      cltjAddCancel(){
        this.cltjAddVisible=false;
      }
    
      cltjAddOk(){
        this.cltjAddSF.value.DAD017=this.selectOpt || undefined;
        if(this.checkValueNull(this.cltjAddSF.value.DAD017)){
          this.msgSrv.error('事项不可为空');
          return;
        }
        if(this.checkValueNull(this.cltjAddSF.value.DAA002)){
          this.msgSrv.error('条件类别不可为空');
          return;
        }
        this.cltjAddSF.value.DAA003=JSON.stringify(this.cltjAddSF.value.tjList);
        this.lbservice.lbservice("CLTJWH_ACLTJ",{para:this.cltjAddSF.value}).then(resdata=>{
          if(resdata.code>0){
            this. cltjQuery();
            this.cltjAddVisible=false;
            this.msgSrv.success('添加成功');
            this.selectOpt='';
            this.displayTips=true;
            this.AD04List=[];
          }else{
            this.msgSrv.error(resdata.errmsg);
          }
        });
      }
    
      initcltjAddSchema(){
        this.cltjAddSchema= {
          properties: {
            DAD017:{
              type:'string',
              title:'事项编号',
              ui:{
                widget:'custom',
              }
            },
            DAA002:{
              type:'string',
              title:'条件类别'
              
            },
            tjList: {
              type: 'array',
              title: '条件',
              items: {
                type: 'object',
                properties: {
                  tjName: {
                    type: 'string',
                    title: '条件名称',
                  },
                },
                required: ['tjName'],
              },
              ui: { 
                spanLabel: 5,
                grid: { 
                  span: 24,
                  arraySpan: 24 
                } 
              } ,
            },
        },
          ui: {
            spanLabelFixed: 80,
            grid: {span: 24 }
          },
          required:['DAD017',"DAD002"]
        };
      }
    
      /***************************修改****************************** */
      cltjUpdate(rec:any){
        this.initcltjUpSchema(rec);
        this.cltjUpVisible=true;
      }
    
      cltjUpCancel(){
        this.cltjUpVisible=false;
      }
    
      cltjUpOk(){
        this.cltjUpSF.value.DAD017=this.uSelectOpt || undefined;
        if(this.checkValueNull(this.cltjUpSF.value.DAD017)){
          this.msgSrv.error('事项不可为空');
          return;
        }
        if(this.checkValueNull(this.cltjUpSF.value.DAA002)){
          this.msgSrv.error('条件类别不可为空');
          return;
        }
        this.cltjUpSF.value.DAA003=JSON.stringify(this.cltjUpSF.value.tjList);
        this.lbservice.lbservice("CLTJWH_UCLTJ",{para:this.cltjUpSF.value}).then(resdata=>{
          if(resdata.code>0){
            this. cltjQuery();
            this.cltjUpVisible=false;
            this.msgSrv.success('添加成功');
            this.uSelectOpt='';
            this.udisplayTips=true;
            this.uAD04List=[];
          }else{
            this.msgSrv.error(resdata.errmsg);
          }
        });
      }
    
      initcltjUpSchema(rec:any){
        this.uinitAD04List(rec.DAD017+'',true);
        this.cltjUpSchema= {
          properties: {
            DAC004:{
              type:'string',
              title:'条件ID',
              default:rec.DAC004,
              ui:{
                hidden:true,
              }
            },
            DAD017:{
              type:'string',
              title:'事项编号',
              default:rec.DAD017,
              ui:{
                widget:'custom',
              }
            },
            DAA002:{
              type:'string',
              title:'条件类别',
              default:rec.DAA002,
            },
            tjList: {
              type: 'array',
              title: '条件',
              default:rec.tjList,
              items: {
                type: 'object',
                properties: {
                  tjName: {
                    type: 'string',
                    title: '条件名称',
                  },
                },
                required: ['tjName'],
              },
              ui: { 
                spanLabel: 5,
                grid: { 
                  span: 24,
                  arraySpan: 24 
                } 
              } ,
            },
          },
          ui: {
            spanLabelFixed: 80,
            grid: {span: 24 }
          },
          required:['DAD017',"DAD002"]
        };
      }
  
      /***************************删除****************************** */
      cltjDelete(rec:any){
        this.lbservice.lbservice("CLTJWH_DCLTJ",{para:{DAC004:rec.DAC004}}).then(resdata=>{
          if(resdata.code>0){
            this.msgSrv.success('删除成功');
            this.cltjQuery();
          }else{
            this.msgSrv.error(resdata.errmsg);
          }
        });
      }

      /**************************ADD查询选择********************************* */
  AD04List=[];
  selectOpt: string;
  displayTips = true;

  onSearch(value: string): void {
    if (value && value.length > 0) {
      this.initAD04List(value);
      this.displayTips = false;
    } else {
      this.AD04List = [];
      this.displayTips = true;
    }
  }

  initAD04List(val:any){
    this.lbservice.lbservice("CLTJWH_ASXOPT",{para:{PARSTR:val}}).then(resdata=>{
      if(resdata.code>0){
        this.AD04List=resdata.message.list;
      }else{
        this.msgSrv.error(resdata.errmsg);
      }
    });
  }

      /**************************Q查询选择********************************* */
      qAD04List=[];
      qSelectOpt: string;
      qdisplayTips = true;
    
      qonSearch(value: string): void {
        if (value && value.length > 0) {
          this.qinitAD04List(value);
          this.qdisplayTips = false;
        } else {
          this.qAD04List = [];
          this.qdisplayTips = true;
        }
      }
    
      qinitAD04List(val:any){
        this.lbservice.lbservice("CLTJWH_ASXOPT",{para:{PARSTR:val}}).then(resdata=>{
          if(resdata.code>0){
            this.qAD04List=resdata.message.list;
          }else{
            this.msgSrv.error(resdata.errmsg);
          }
        });
      }

            /**************************Q查询选择********************************* */
            uAD04List=[];
            basicList=[];
            uSelectOpt;
            udisplayTips = true;
          
            uonSearch(value: string,first:boolean): void {
              if (value && value.length > 0) {
                this.uinitAD04List(value,first);
                this.udisplayTips = false;
              } else {
                this.uAD04List = [];
                this.udisplayTips = true;
              }
            }
          
            uinitAD04List(val:any,first:boolean){
              this.lbservice.lbservice("CLTJWH_ASXOPT",{para:{PARSTR:val}}).then(resdata=>{
                if(resdata.code>0){
                  this.uAD04List=resdata.message.list;
                  if(first){
                    this.uSelectOpt=Number(val);
                    // this.basicList=this.uAD04List;
                  }
                }else{
                  this.msgSrv.error(resdata.errmsg);
                }
              });
            }

      /****************************详情*************************** */
      cltjShowVisible=false;
      showItem;
      cltjShowCancel(){
        this.cltjShowVisible=false;
      }
      show(val){
        this.showItem=val;
        this.lbservice.lbservice("CLTJWH_ASXOPT",{para:{PARSTR:val.DAA017}}).then(resdata=>{
          if(resdata.code>0){
            resdata.message.list.forEach(item=>{
              if(item.DAD017===this.showItem.DAD017){
                this.showItem.DAD017_S=item.DAD031;
                return;
              }
            })
          }else{
            this.msgSrv.error(resdata.errmsg);
          }
        });
        this.cltjShowVisible=true;
      }


        
    }
    
  
