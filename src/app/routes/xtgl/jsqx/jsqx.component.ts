import { Component, OnInit ,ViewChild} from '@angular/core';
import { _HttpClient, ModalHelper } from '@delon/theme';
import { STColumn, STComponent, STPage, STChange } from '@delon/abc';
import { SFSchema, SFComponent } from '@delon/form';
import { GridComponent, PopdetailComponent, HttpService, SupDic } from 'lbf';
import { NzMessageService, NzFormatEmitEvent ,NzModalService} from 'ng-zorro-antd';

@Component({
  selector: 'app-xtgl-jsqx',
  templateUrl: './jsqx.component.html',
})
export class XtglJsqxComponent implements OnInit {
  constructor( private modal: ModalHelper,
    public msgSrv: NzMessageService,
    private lbservice: HttpService,
    private supdic: SupDic,
    private modalService: NzModalService
    ) { }

  nodes: any[];
 

  params = {sname: 'xtgl_jsgncx',form: {}}
  item: any;
  title: any;
  roleid: any;
  columns: STColumn[] = [
    {title: '所属业务',index: 'BID',format: (item,data,index)=>this.ywfz(item,data,index)},
    {title: '功能名称',index: 'FNAME'},
    { title: '操作区', render:'custom'},
  ]

  searchSchema: SFSchema = {
    properties: {
      sq: {
        type: 'number',
        title: '是否授权',
        enum: this.supdic.getSFDic('SQ'),
        ui: {
          widget: 'select',
          allowClear: true,
          width: 150
         }
      }
    }
  }

@ViewChild('st',{static:false}) st: GridComponent;
@ViewChild('sf',{static:false}) sf: SFComponent;

  ngOnInit() {
    this.lbservice
  .lbservice('xtgl_jscx', {})
  .then(resdata => {
    if(resdata.code<1){
        this.msgSrv.error(resdata.errmsg);
    }else{
      this.nodes = resdata.message.list;
    }
  })
 }

query(value){
  console.log(value)
    this.params.form = {roleid: this.roleid,value};
    this.st.reload(this.params.form);
}
reset(value){
  this.sf.reset();
  this.st.reload({roleid: this.roleid});
}

 ywfz(item,data,index){
    return item.BNAME;
 }
 nzEvent(item): void {
  
  if(item.eventName === 'expand'){
    if(item.keys.length===0){
      this.title = ''
      this.st.reload();
    }
  }
  if(item.eventName === 'click'){
     console.log(item)
    if(item.node.origin.isLeaf === false){
      console.log("return")
      return;
    }
      this.title = item.node.origin.title + '功能列表';
      console.log(item)
      this.roleid = item.node.origin.key;
      this.params.form = {roleid: this.roleid}
      this.st.reload(this.params.form);
  }
}

changeState(rec){
  console.log(rec)
  if(rec.STATE === 0){
    rec.STATE = 1
  }else if(rec.STATE === 1){
    rec.STATE = 0
  }
  this.lbservice.lbservice('xtgl_jsgnsq',{roleid: this.roleid,fid: rec.FID,state: rec.STATE}).then(resdata=>{
    if (resdata.code < 1) {
      this.msgSrv.error(resdata.errmsg);
    }else{
      this.msgSrv.success("授权成功");
      
    }
 });
}
}
