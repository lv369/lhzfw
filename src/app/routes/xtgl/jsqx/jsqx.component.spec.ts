import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { XtglJsqxComponent } from './jsqx.component';

describe('XtglJsqxComponent', () => {
  let component: XtglJsqxComponent;
  let fixture: ComponentFixture<XtglJsqxComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ XtglJsqxComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(XtglJsqxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
