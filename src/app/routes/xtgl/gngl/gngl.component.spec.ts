import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { XtglGnglComponent } from './gngl.component';

describe('XtglGnglComponent', () => {
  let component: XtglGnglComponent;
  let fixture: ComponentFixture<XtglGnglComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ XtglGnglComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(XtglGnglComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
