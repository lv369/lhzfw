import { Component, OnInit ,ViewChild, ɵConsole} from '@angular/core';
import { _HttpClient, ModalHelper } from '@delon/theme';
import { STColumn, STComponent, STChange, STPage } from '@delon/abc';
import { SFSchema, SFComponent } from '@delon/form';
import { GridComponent, PopdetailComponent, HttpService, SupDic } from 'lbf';
import { NzMessageService, NzModalComponent,NzTreeComponent } from 'ng-zorro-antd';
import { Router } from '@angular/router';


@Component({
  selector: 'app-xtgl-gngl',
  templateUrl: './gngl.component.html',
})
export class XtglGnglComponent implements OnInit {
// datas = 'MohrssExInterface/gridsvr';
params = { sname: "FR_qfunlist", form: {} };

isVisible = false;
isUpVisible = false;
isSqVisible = false;
roles: any = []
value: any=['1']
gnarr: any=[]
fid: number;
nodes: any=[]
gnsqNodes: any=[]
data: any;
isTrue: any = false;

updateSchema: SFSchema =  {
  properties: {
    FID: {
      type: 'number',
      title: '功能ID',
      ui: {
        hidden: true
      }
    },
    FNAME: {
      type: 'string',
      title: '功能名称'
    },
    LINK: {
      type: 'string',
      title: '功能路径'
    },
    BID: {
      type: 'number',
      title: '所属业务',
      enum: this.supdic.getSFDic('BID'),
      ui:{
        widget: 'select'
      }
    },
    SEQ: {
      type: 'number',
      title: '顺序'
    }
  },
  required: [ "FNAME", "BID","LINK","SEQ"],
  ui: {
    spanLabelFixed: 100,
    grid: {
      span: 24
    }
  }
}


newSchema: SFSchema = {
  properties: {
    bid: {
      type: 'string',
      title: '所属业务',
      enum: this.supdic.getSFDic('BID'),
      readOnly: true,
      ui: {
           widget: 'select',
           allowClear: true,
           
          }
    },
    fname: {
      type: 'string',
      title: '功能名称'
    },
    link: {
      type: 'string',
      title: '功能路径'
    },
    
    seq: {
      type: 'number',
      title: '顺序',
      default: 0
    }
  },
  required: [ "fname", "fstate","bid","link","seq"],
  ui: {
    spanLabelFixed: 100,
    grid: {
      span: 24
    }
  }
}

searchSchema: SFSchema = {
  properties: {
    BID: {
      type: 'number',
      title: '所属业务',
      enum: this.supdic.getSFDic('BID'),
      ui: {
        widget: 'select',
        allowClear: true
      }
    },
    FNAME: {
      type: 'string',
      title: '功能名称',
    },
  }  ,
  ui: {
    spanLabelFixed: 80,
    grid: {
      span: 6
    }
  }
};

@ViewChild('st',{static:false}) st: GridComponent;
@ViewChild('sf',{static:false}) sf: SFComponent;
@ViewChild('sf1',{static:false}) sf1: SFComponent;
@ViewChild('sf2',{static:false}) sf2: SFComponent;
@ViewChild('tree',{static:false}) tree: NzTreeComponent;
columns: STColumn[] = [
  { title: '所属业务', index:'BID',dic: 'BID'},
  { title: '功能名称', index: 'FNAME'},
  { title: '功能路径', index: 'LINK' },
  { title: '顺序', index: 'SEQ',type:'number' },
  { title: '功能状态', render: 'custom' },
  
  {
    title: '操作区',
    buttons: [
      {
        text: '删除',
        click: (record: any) => this.delfun(record),
        pop: true,
        popTitle: '确认删除该功能？',
      },
      {
        text: '修改',
        click: (record: any) => this.updatefun(record),
      }
    ]
  }
];

isConfirmLoading = false;

gnmc: any;
gnlj: any;
item: any;
modiFormData = {}
constructor(
  private modal: ModalHelper,
  public msgSrv: NzMessageService,
  private lbservice: HttpService,
  private supdic: SupDic,
) { }

ngOnInit() {

  this.lbservice
  .lbservice('xtgl_cdcx', {})
  .then(resdata => {
    if (resdata.code < 1) {
      this.msgSrv.error(resdata.errmsg);
    } else {
      this.nodes = resdata.message.list;
    }
  });
}

nzEvent(item){
  this.tree.getCheckedNodeList();
  if(item.eventName === 'expand'){
    console.log(item)
  }
  if(item.eventName === 'click'){
    // console.log(item)
    if(item.node.key !==9999){
      this.item = item;
      this.sf.setValue('/bid',item.node.key);
    }

    
    // console.log(item.node.key)
  }
}

changeState(rec){
  if(rec.AAE100 === '0'){
    rec.AAE100 = '1'
  }else if(rec.AAE100 === '1'){
    rec.AAE100 = '0'
  }
   this.lbservice.lbservice('gngl_gnztupdate',{para: {'fid':rec.FID,'aae100':rec.AAE100}})
  .then(resdata=>{
    if (resdata.code < 1) {
      this.msgSrv.error(resdata.errmsg);
    }else{
      this.msgSrv.success("功能状态修改成功");
      
    }
});
}

ywfz(item,data,index){
  return item.BNAME;
}

gnEvent(item){
  if(item.eventName === 'expand'){
    console.log(item)
     if(item.node.children === []){
        item.node.origin.isLeaf= true
     }
  }
  if(item.eventName === 'check'){
    this.value = item.keys;
  }
}

query(){
  this.st.pi = 1;
  this.st.reload(this.sf2.value);
}

/*
cz(){
  this.sf2.reset();
  this.st.reload();
}
*/
delfun(record: any){
  console.log(record)
  this.isTrue = false;
  
  if(record.AAE100 === '1'){
    this.msgSrv.error("功能处于正常状态，不能删除");
    return;
 }

    this.lbservice
    .lbservice('FR_qfundel', { para: { FID: record.FID } })
    .then(resdata => {
      this.isConfirmLoading = false;
      if (resdata.code < 1) {
        this.msgSrv.error(resdata.errmsg);
      } else {
        this.msgSrv.info('注销成功！');
        this.st.reload();
      }
    });
}

updatefun(rec: any){
  console.log(rec);
  //this.sf1.formData = rec;
  this.modiFormData = rec;
  
  this.isUpVisible = true;
}

add() {
  // this.modal
  //   .createStatic(FormEditComponent, { i: { id: 0 } })
  //   .subscribe(() => this.st.reload());

  this.isVisible = true;
}


log(val){
  console.log(val);

  this.value = val;
}

handleCancel() {
  this.isSqVisible = false;
  console.log(this.value)
  this.isVisible = false
  this.isUpVisible = false
}
handleOk(){
  this.isSqVisible= false;
  console.log(this.value)
    this.lbservice
      .lbservice('gngl_gnsq', {para:{ROLES: this.value,FID:this.fid }})
      .then(resdata => {
        if (resdata.code < 1) {
          this.msgSrv.error(resdata.errmsg);
        } else {
          this.msgSrv.info('授权成功');
        }
      });
}

handleOkAdd(){
  this.isVisible = false
  
  this.lbservice.lbservice('FR_funadd', {para: this.sf.value})
  .then(resdata => {
    if (resdata.code < 1) {
      this.msgSrv.error(resdata.errmsg);
    } else {
      this.msgSrv.info('新增成功');
      this.st.st.reload();
      // this.reload();
    }
  });
}

handleOkUp(){
  this.isUpVisible = false
  this.lbservice
  .lbservice('FR_funupdate', {para: this.sf1.value})
  .then(resdata => {
    if (resdata.code < 1) {
      this.msgSrv.error(resdata.errmsg);
    } else {
      this.msgSrv.info('修改成功');
      this.st.st.reload();
    }
  });
}
}