import { Component, OnInit, ViewChild, SimpleChanges } from '@angular/core';
import { _HttpClient, ModalHelper } from '@delon/theme';
import { STColumn, STComponent, STChange } from '@delon/abc';
import { SFSchema,SFComponent } from '@delon/form';
import { GridComponent, PopdetailComponent, HttpService, SupDic } from 'lbf';
import { NzMessageService } from 'ng-zorro-antd';
import { TestBed } from '@angular/core/testing';


@Component({
  selector: 'app-xtgl-fwsq',
  templateUrl: './fwsq.component.html',
})
export class XtglFwsqComponent implements OnInit {
  params = { sname: "FR_fwsqlist", form: {} };

  isVisible = false;
  chVisible = false;

  formData = {};

  isConfirmLoading = false;

  constructor(
    private modal: ModalHelper,
    public msgSrv: NzMessageService,
    private lbservice: HttpService,
    private supdic: SupDic,
  ) { }
  
  ngOnInit() { 
   this.initDataset();
   this.initData();
  }

 
  export() {

   // this.st.export({ filename: 'test.xlsx', sheetname: 'sheet1' });

  }

  //新建服务相关
  add() {
    this.isVisible = true;
  }

  newSchema: SFSchema = {
    properties: {
      xtid: {
        type: 'string',
        title: '系统ID',
      },
      fwid: {
        type: 'string',
        title: '服务ID',
      },
    },
    required: ["xtid", "fwid"]
  }

  save(val: any, resdata: any) {

    //  if(resdata.code>0){
    //    this.st.reload();
    //  }
  }
   
//查询框相关
searchSchema: SFSchema = {
  properties: {
    fwid: {
      type: 'string',
      title: '服务ID',
      ui:{
        width:200,
      }
    },
    fwmc: {
      type: 'string',
      title: '服务名称',
      ui: {
        width: 200,
      }
    },
    dyrkmc: {
      type: 'string',
      title: '入口名称',
      ui: {
        width: 200,
      }
    },
  },
};


query(){
  //this.st.reload(this.sf.value);
  this.stchange.pi=1;
  this.stReload();
}

/********************************************************展示框相关**************************************************/
@ViewChild('st',{static:false}) st: STComponent;
@ViewChild('pop',{static:false}) pop: SFComponent;
columns: STColumn[] = [
  { title: '服务ID', index: 'CZE028', statistical: 'count'  },
  { title: '服务名称', index: 'CZE030',filter: {type: 'keyword',icon:{type:'filter',theme:'fill'}},},
  { title: '调用入口名称', index: 'CZE029' ,filter: {type: 'keyword',icon:{type:'filter',theme:'fill'}},},
  { title: '操作区', index: 'ZT' ,render:'custom',filter: {menus:[{text:'未授权',value:'0'},{text:'已授权',value:'1'}],icon:{type:'filter',theme:'fill'}},},
];
// ,filter: {menus:[{text:'未授权',value:'0'},{text:'已授权',value:'1'}]},
  //st分页相关
  stpage = {
    total: true,
    show: true,//显示分页
    front: false //关闭前端分页，true是前端分页，false后端控制分页
  };
  stchange:STChange={
    type:"pi",
    pi:1,
    ps:10,
    total:0,
  }
  dataset=[];

initDataset(){
  this.lbservice.lbservice('FR_fwsqlist',{'CZE020':this.nowSystem,page:{'PAGESIZE':this.stchange.ps,'PAGEIDX':this.stchange.pi}}).then(resdata=>{
    this.dataset=resdata.message.list;
   this.stchange.total=resdata.page.COUNT;
  });
}

filterPara:any={};
pageChange(para:any){
  if(para.type=="pi"){
   this.lbservice.lbservice('FR_fwsqlist',{'CZE020':this.nowSystem,para:this.filterPara,page:{'PAGESIZE':this.stchange.ps,'PAGEIDX':para.pi}}).then(resdata=>{
     if(resdata.code==1){
       this.dataset=resdata.message.list;
       this.stchange.pi=para.pi;
     }
  });
  }else if(para.type=="filter"){
    if(para.filter.filter.type=='keyword'){
      this.filterPara[para.filter.filter.key]=para.filter.filter.menus[0].value;
    }else if(para.filter.filter.type=='default'){
      const arr=[];
      para.filter.filter.menus.forEach(ele=>{
        if(ele.checked){
          arr.push(ele.value);
        }
      });
      this.filterPara[para.filter.filter.key]=arr;
    }
    this.stchange.pi=1;
    this.stReload();
  }
}
changeFwState(index:any){
  const rec:any=this.st.data[index];
  if(rec.ZT==0){       //授权
    this.lbservice.lbservice("FR_fwsqadd",{para:{"xtid":this.nowSystem,"fwid":rec.CZE028}}).then(resdata=>{
      if(resdata.code>0){
        this.msgSrv.success("授权成功");
        this.dataset[index].ZT=1;
      }else{
        this.msgSrv.error(resdata.errmsg);
      }
    });
  }else{      //取消授权
    this.lbservice.lbservice("FR_fwsqdelete",{para:{"CZE020":this.nowSystem,"CZE028":rec.CZE028}}).then(resdata=>{
      if(resdata.code>0){
        this.msgSrv.success("取消授权成功");
        this.dataset[index].ZT=0;
      }else{
        this.msgSrv.error(resdata.errmsg);
      }
    });
  }
}
stReload(){
  this.lbservice.lbservice('FR_fwsqlist',{'CZE020':this.nowSystem,para:this.filterPara,page:{'PAGESIZE':this.stchange.ps,'PAGEIDX':this.stchange.pi}}).then(resdata=>{
    this.dataset=resdata.message.list;
   this.stchange.total=resdata.page.COUNT;
  });
}



//删除相关
@ViewChild('sf',{static:false}) sf: SFComponent;

sqdelete(rec:any){
  this.lbservice.lbservice('FR_fwsqdelete',rec).then((resdata:any)=>{
    this.st.reload(this.sf.value);
  });
}


delczy(rec:any){}


 //更改相关
 changeSchema: SFSchema={
    properties: {
      xtid: {
        type: 'string',
        title: '系统ID',
      },
      fwid: {
        type: 'string',
        title: '服务ID',
      },
      fwmc: {
        type: 'string',
        title: '服务名称',
      },
      fwzt: {
        type: 'string',
        title: '服务状态',
      }
    },
    required: ["xtid", "fwid"]
  };

updateService(rec:any){
  this.changeSchema={
    properties: {
      xtid: {
        type: 'string',
        title: '系统ID',
      },
      fwid: {
        type: 'string',
        title: '服务ID',
      },
      fwmc: {
        type: 'string',
        title: '服务名称',
        readOnly:true,
      },
      fwzt: {
        type: 'string',
        title: '服务状态',
        readOnly:true,
      }
    },
    required: ["xtid", "fwid"]
  };
  this.chVisible=true;
} 


//@ViewChild('sf',{static:false}) sf: SFComponent;

handleCancel() {
  this.isVisible = false;
}

handleOk(){
  this.lbservice.lbservice("FR_fwsqadd",{para:this.pop.value}).then(resdata=>{
    if(resdata.code>0){
      this.msgSrv.success("授权成功");
    }else{
      this.msgSrv.error(resdata.errmsg);
    }
    this.isVisible=false;
    this.st.reload(this.sf.value);
  });
}


/***********************************************系统列表******************************************************* */
data=[];
nowSystem=1;

initData(){
  this.lbservice.lbservice("xtgl_qxtlist",{}).then(resdata=>{
    this.data=resdata.message.list;
    this.data.forEach(ele=>{
      this.tabs.push({'name':ele.CZE021,'content':ele.CZE020});
    })
  });
}

xtChange(){
  this.stchange.pi=1;
  this.lbservice.lbservice('FR_fwsqlist',{'CZE020':this.nowSystem,para:this.filterPara,page:{'PAGESIZE':this.stchange.ps,'PAGEIDX':this.stchange.pi}}).then(resdata=>{
    this.dataset=resdata.message.list;
   this.stchange.total=resdata.page.COUNT;
  });
}


/****************************************************************************************************************** */

tabs: Array<{ name: string; content: string }> = [];

/* tslint:disable-next-line:no-any */
log(args: any): void {
  this.nowSystem=Number(args.content);
  this.xtChange();
}






}
