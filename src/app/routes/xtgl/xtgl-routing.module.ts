import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { XtglPwdChangeComponent } from './pwd-change/pwd-change.component';
import { XtglFwzcComponent } from './fwzc/fwzc.component';
import { XtglFwsqComponent } from './fwsq/fwsq.component';
import {XtglGnglComponent} from './gngl/gngl.component';
import { XtglCdglComponent } from './cdgl/cdgl.component';
import { XtglThemeComponent } from './theme/theme.component';
import { XtglThemeDetailComponent } from './theme/detail/detail.component';
import { XtglJsqxComponent } from './jsqx/jsqx.component';
import { RepglComponent } from './repgl/repgl.component';


const routes: Routes = [

  { path: 'pwdChange', component: XtglPwdChangeComponent ,data: { title: '修改密码',}, },
  { path: 'gngl', component: XtglGnglComponent},

  { path: 'fwzc', component: XtglFwzcComponent },
  { path: 'fwsq', component: XtglFwsqComponent },
  { path: 'gngl', component: XtglGnglComponent },
  { path: 'cdgl', component: XtglCdglComponent },
  { path: 'theme', component: XtglThemeComponent },
  { path: 'detail', component: XtglThemeDetailComponent },
  { path: 'repgl', component: RepglComponent },
  { path: 'jsqx', component: XtglJsqxComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
 
})
export class XtglRoutingModule { }
