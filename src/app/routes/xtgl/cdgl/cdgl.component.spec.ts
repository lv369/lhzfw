import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { XtglCdglComponent } from './cdgl.component';

describe('XtglCdglComponent', () => {
  let component: XtglCdglComponent;
  let fixture: ComponentFixture<XtglCdglComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ XtglCdglComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(XtglCdglComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
