import { Component, OnInit ,ViewChild} from '@angular/core';
import { _HttpClient, ModalHelper } from '@delon/theme';
import { STColumn, STComponent, STPage, STChange } from '@delon/abc';
import { SFSchema, SFComponent } from '@delon/form';
import { GridComponent, PopdetailComponent, HttpService, SupDic } from 'lbf';
import { NzMessageService, NzFormatEmitEvent ,NzModalService} from 'ng-zorro-antd';

@Component({
  selector: 'app-xtgl-cdgl',
  templateUrl: './cdgl.component.html',
})
export class XtglCdglComponent implements OnInit {
  constructor(
    private modal: ModalHelper,
    public msgSrv: NzMessageService,
    private lbservice: HttpService,
    private supdic: SupDic,
    private modalService: NzModalService
  ) { }
  
  nodes: any[] = []
  dataSet: any = []
  isAddDisabled = true
  isSaveDisabled = true
  isDelDisabled = true
  confirmText = '删除'
  params = {sname: 'gngl_gncx',form: {}}

  item: any ;
  isVisible = false

  editSchema:SFSchema = {
    properties: {
      BID: {
        type: 'number',
        title: '菜单ID',
        ui: {
          hidden: true
        }
      },
      PBID:{
        type: 'string',
        title: '上级ID',
        readOnly: true
      },
      FBID:{
        type: 'number',
        title: '上级ID',
        ui: {
          hidden: true
        }
      },
      BNAME: {
        type: 'string',
        title: '菜单名称',
        ui: {
          width: 300
        }
      },
      ICON: {
        type: 'string',
        title: '图标',
        ui: {
          width: 300
        }
      },
      SEQ: {
        type: 'number',
        title: '顺序',
        ui: {
          width: 300
        }
      }
    },
    ui: {
      spanLabelFixed: 100,
      grid: {
        span: 12
      }
    }
  }

  newSchema: SFSchema = {
    properties: {
      PBID:{
        type: 'string',
        title: '上级ID',
        readOnly: true,
        ui: {
          width: 300
        }
      },
      FBID:{
        type: 'number',
        title: '上级ID',
        ui: {
          hidden: true
        }
      },
      BNAME: {
        type: 'string',
        title: '菜单名称',
        ui: {
          width: 300
        }
      },
      ICON:{
        type: 'string',
        title: '图标',
        ui: {
          width: 300,
        }
      },
      SEQ: {
        type: 'number',
        title: '顺序',
        ui: {
          width: 300
        }
      }
    },
    required: [ "BNAME", "ICON","SEQ"],
    ui: {
      spanLabelFixed: 100,
      grid: {
        span: 24
      }
    }
    }
  columns: STColumn[] = [
    {title: '功能编号',index: 'FID'},
    {title: '功能名称',index: 'FNAME'},
    {
      title: '操作区',
      buttons: [
        {
          text: '删除',
          click: (record: any) => this.delfun(record),
          pop: true,
          popTitle: '确认删除该功能？',
        }
      ]
    }
  ]

   @ViewChild('st',{static:false}) st: GridComponent;
   @ViewChild('sf',{static:false}) sf: SFComponent;
   @ViewChild('editsf',{static:false}) editsf: SFComponent;
  ngOnInit() {
    this.initCdTree();
  }

  delfun(record: any){
    console.log(record)
    
  
      this.lbservice
      .lbservice('FR_qfundel', { para: { FID: record.FID } })
      .then(resdata => {
       
        if (resdata.code < 1) {
          this.msgSrv.error(resdata.errmsg);
        } else {
          this.msgSrv.info('功能删除成功！');
          this.st.reload();
        }
      });
  }

  initCdTree(){
    this.lbservice
        .lbservice('xtgl_cdcx', { })
        .then(resdata => {
          if (resdata.code < 1) {
            this.msgSrv.error(resdata.errmsg);
          } else {
            this.nodes = resdata.message.list;
          }
        });
  }
  
  nzEvent(item): void {

    this.isAddDisabled = true;
    this.isDelDisabled = true;
    this.isSaveDisabled = true
    if(item.eventName === 'click'){
      console.log(item)
      this.item = item.node.origin
      const treeData = item.node.origin;
      
      this.editsf.setValue('/BID',treeData.key);
      if(item.node.parentNode === null){
        this.editsf.setValue('/PBID',null);
        this.editsf.setValue('/FBID',null);
      }else{
        this.editsf.setValue('/PBID',item.node.parentNode.origin.title);
        this.editsf.setValue('/FBID',treeData.fbid);
      }
      
      this.confirmText = '是否确认删除【'+treeData.title+'】菜单';
      this.editsf.setValue('/BNAME',treeData.title);
      this.editsf.setValue('/ICON',treeData.icon);
      this.editsf.setValue('/SEQ',treeData.seq);
      
      
        this.params.form = {BID: item.node.origin.key}
        this.st.reload(this.params.form);

        this.isAddDisabled = false;
        if(treeData.fbid === 9998){
          this.isDelDisabled = true;
        }else{
          this.isDelDisabled = false;
        }
        this.isSaveDisabled = true;
    }
  }


  formChanged(event){
    this.isSaveDisabled = false;
  }

  add(){
    console.log(this.item)

    this.sf.setValue('/PBID',this.item.title);
    this.sf.setValue('/FBID',this.item.key);
    this.sf.setValue('/ICON',this.item.icon)
    this.isVisible = true
    
  }

  handleCancel(){
    this.isVisible = false
  }

  handleOk(){
    this.isVisible = false
    this.lbservice
          .lbservice('cdgl_cdxz', {para: this.sf.value})
          .then(resdata => {
            if (resdata.code < 1) {
              this.msgSrv.error(resdata.errmsg);
            } else {
              this.msgSrv.info("菜单新增成功");
               this.initCdTree();   
            }

          });
   
  }
  delete(){
        this.lbservice
        .lbservice('cdgl_cdsc', {para: {BID: this.item.key}})
        .then(resdata => {
          if (resdata.code < 1) {
            this.msgSrv.error(resdata.errmsg);
          } else {
            this.msgSrv.info("菜单删除成功");
            this.initCdTree();
          }
        });
       
     
    
  }
  update(){
    this.lbservice.lbservice('cdgl_cdxg',{para: this.editsf.value}).then(resdata =>{
      if (resdata.code < 1) {
                this.msgSrv.error(resdata.errmsg);
              } else {
                this.msgSrv.info("修改成功");
                this.initCdTree();
              }
    })
    // console.log(this.sjcdValue);
    // this.modalService.confirm({
    //   nzTitle: "是否保存修改",
    //   nzOkText: "确定",
    //   nzCancelText: "取消",
    //   nzOnOk: () => {
    //     this.lbservice
    //     .lbservice('cdgl_cdxg', {para: {BID: this.item.node.origin.key,BNAME:this.cdValue,ICON: this.icon,
    //       SEQ: this.seq}})
    //     .then(resdata => {
    //       if (resdata.code < 1) {
    //         this.msgSrv.error(resdata.errmsg);
    //       } else {
    //         this.msgSrv.info("修改成功");
    //         this.lbservice
    //         .lbservice('xtgl_cdcx', {})
    //         .then(resdata => {
    //           if (resdata.code < 1) {
    //             this.msgSrv.error(resdata.errmsg);
    //           } else {
    //             this.nodes = resdata.message.list;
    //           }
    //         });
    //       }
    //     });
    //   }
    // })
    
  }

  confirm(){
    this.delete();
  }

  cancel(){

  }
}
