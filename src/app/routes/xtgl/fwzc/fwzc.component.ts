import { Component, OnInit, ViewChild, EventEmitter } from '@angular/core';
import { _HttpClient, ModalHelper } from '@delon/theme';
import { STColumn, STChange, STComponent } from '@delon/abc';
import { SFSchema, SFComponent } from '@delon/form';
import { GridComponent, PopdetailComponent, HttpService, SupDic } from 'lbf';
import { NzMessageService } from 'ng-zorro-antd';

@Component({
  selector: 'app-xtgl-fwzc',
  templateUrl: './fwzc.component.html',
})
export class XtglFwzcComponent implements OnInit {
  params = { sname: 'FR_fwzclist', form: {} };

  isVisible = false;
  chVisible = false;

  formData = {};

  isConfirmLoading = false;
  @ViewChild('st', { static: false }) st: GridComponent;
  @ViewChild('pop', { static: false }) pop: SFComponent;
  @ViewChild('sf',{static:false}) sf: SFComponent;
  @ViewChild('changePop', { static: false }) changePop: SFComponent;

  columns: STColumn[] = [
    { title: '服务ID', index: 'CZE028' },
    {
      title: '调用入口名称',
      index: 'CZE029',
    },
    {
      title: '服务名称',
      index: 'CZE030',
    },
    { title: '过程名称', index: 'CZE031' },
    {
      title: '服务状态',
      index: 'CZE032',
      render: 'custom',
    },
    { title: '是否登录', index: 'CZE038', render: 'login' },
    {
      title: '操作区',
      buttons: [
        {
          text: '更改',
          click: (record: any) => this.updateService(record),
        },
        {
          text: '删除',
          click: (record: any) => this.delete(record),
          pop: true,
          popTitle: '确定删除该任务？',
        },
        {
          text: '授权',
          click: (record: any) => this.sq(record),
        },
      ],
    },
  ];
    filterPara: any = {};
      // 服务授权
      isSqVisible = false;
      fwsqlist = [];
      nowFW = 0;
      cklist = [];
    changeSchema: SFSchema = {
      properties: {},
      required: ['dyrkmc', 'fwmc', 'fwnr', 'fwzt', 'sfdl'],
    };

  // st分页相关
  stpage = {
    total: true,
    show: true, // 显示分页
    front: false, // 关闭前端分页，true是前端分页，false后端控制分页
  };
  stchange: STChange = {
    type: 'pi',
    pi: 1,
    ps: 10,
    total: 0,
  };
  dataset = [];

    // 查询框相关
    searchSchema: SFSchema = {
      properties: {
        CZE029: {
          type: 'string',
          title: '入口名称',
        },
        CZE032: {
          type: 'string',
          title: '服务状态',
          enum: [{label:'停用',value:'0'},{label:'启用',value:'1'}],
          ui: {
            width: 300,
            widget: 'select',
            allowClear: true,
          },
        },
        CZE030: {
          type: 'string',
          title: '服务名称',
        },
        btn: {
          type: 'string',
          title: '',
          ui:{
            spanLabelFixed:0,
            widget:'custom',
            grid:{
              span:3,
            }
          }
        },
      },
      ui:{
        grid:{span:7}
      }
    };

    newSchema: SFSchema = {
      properties: {
        dyrkmc: {
          type: 'string',
          title: '调用入口名称',
        },
        fwmc: {
          type: 'string',
          title: '服务名称',
        },
        fwnr: {
          type: 'string',
          title: '调用过程',
        },
        sfdl: {
          type: 'string',
          title: '是否需要登录',
          enum: ['需要登录', '不需要登录'],
          default: '需要登录',
          ui: {
            widget: 'radio',
          },
        },
      },
      required: ['dyrkmc', 'fwmc', 'fwnr', 'sfdl'],
    };


  constructor(
    private modal: ModalHelper,
    public msgSrv: NzMessageService,
    private lbservice: HttpService,
    private supdic: SupDic,
  ) {}

  ngOnInit() {
    this.initDataset();
  }

  export() {}

  // 新建服务相关
  add() {
    this.isVisible = true;
  }

  handleCancel() {
    this.isVisible = false;
  }

  handleOk() {
    this.lbservice
      .lbservice('FR_fwzcadd', { para: this.pop.value })
      .then(resdata => {
        if (resdata.code > 0) {
          this.msgSrv.success('新建成功');
        } else {
          this.msgSrv.error(resdata.errmsg);
        }
        this.isVisible = false;
        this.st.reload();
      });
  }



  save(val: any, resdata: any) {
    //  if(resdata.code>0){
    //    this.st.reload();
    //  }
  }



  query(pi1=false) {
    // this.stchange.pi = 1;
    // this.stReload();
    if(pi1){
      this.st.pi=1;
    }
    this.st.reload(this.sf.value)
  }

  /**************************************************展示框相关******************************************************/
  

  initDataset() {
    this.lbservice
      .lbservice('FR_fwzclist', {
        page: { PAGESIZE: this.stchange.ps, PAGEIDX: this.stchange.pi },
      })
      .then(resdata => {
        this.dataset = resdata.message.list;
        this.stchange.total = resdata.page.COUNT;
      });
  }


  pageChange(para: any) {
    if (para.type === 'pi') {
      this.lbservice
        .lbservice('FR_fwzclist', {
          para: this.filterPara,
          page: { PAGESIZE: this.stchange.ps, PAGEIDX: para.pi },
        })
        .then(resdata => {
          if (resdata.code === 1) {
            this.dataset = resdata.message.list;
            this.stchange.pi = para.pi;
          }
        });
    } else if (para.type === 'filter') {
      if (para.filter.filter.type === 'keyword') {
        this.filterPara[para.filter.filter.key] =
          para.filter.filter.menus[0].value;
      } else if (para.filter.filter.type === 'default') {
        const arr = [];
        para.filter.filter.menus.forEach(ele => {
          if (ele.checked) {
            arr.push(ele.value);
          }
        });
        this.filterPara[para.filter.filter.key] = arr;
      }
      this.stchange.pi = 1;
      this.stReload();
    }
  }
  changeFwState(index: any) {
    const rec: any = this.st.data[index];
    if (rec.CZE032 === '0') {
      rec.CZE032 = '1';
    } else if (rec.CZE032 === '1') {
      rec.CZE032 = '0';
    }
    this.lbservice.lbservice('FR_fwzcupdate', { para: rec }).then(resdata => {
      if (resdata.code === 1) {
        this.st.data[index].CZE032 = rec.CZE032;
      }
    });
  }
  changeLgState(index: any) {
    const rec: any = this.st.data[index];
    if (rec.CZE038 === '0') {
      rec.CZE038 = '1';
    } else if (rec.CZE038 === '1') {
      rec.CZE038 = '0';
    }
    this.lbservice.lbservice('FR_fwzcupdate', { para: rec }).then(resdata => {
      if (resdata.code === 1) {
        this.st.data[index].CZE038 = rec.CZE038;
      }
    });
  }

  stReload() {
    this.query();
    // this.lbservice
    //   .lbservice('FR_fwzclist', {
    //     para: this.filterPara,
    //     page: { PAGESIZE: this.stchange.ps, PAGEIDX: this.stchange.pi },
    //   })
    //   .then(resdata => {
    //     this.dataset = resdata.message.list;
    //     this.stchange.total = resdata.page.COUNT;
    //   });
  }

  update(val: any, resdata: any) {
    this.chVisible = true;
  }

  delete(rec: any) {
    this.lbservice
      .lbservice('fwzc_fwdelete', { para: { CZE028: rec.CZE028 } })
      .then(resdata => {
        if (resdata.code > 0) {
          this.msgSrv.success('删除成功');
        } else {
          this.msgSrv.error(resdata.errmsg);
        }
        this.query(true);
      });
  }


  sq(rec: any) {
    this.lbservice
      .lbservice('fwgl_fwzcsqlist', { CZE028: rec.CZE028 })
      .then(resdata => {
        this.nowFW = rec.CZE028;
        this.fwsqlist = resdata.message.list;
        this.fwsqlist.forEach(ele => {
          if (ele.CZE028 !== 0) {
            this.cklist.push(ele.CZE020);
          }
        });
        console.log(this.cklist);
        this.isSqVisible = true;
      });
  }
  log(eve: any) {
    this.cklist = eve;
  }
  fwsqhandleCancel() {
    this.cklist = [];
    this.isSqVisible = false;
  }

  fwsqhandleOk() {
    this.lbservice
      .lbservice('fwgl_fwzcsq', { CZE028: this.nowFW, XTLIST: this.cklist })
      .then(resdata => {
        if (resdata.code > 0) {
          this.msgSrv.success('授权成功');
        } else {
          this.msgSrv.error(resdata.errmsg);
        }
        this.cklist = [];
        this.isSqVisible = false;
      });
  }
  /******************************************************更改相关*****************************************************/


  updateService(rec: any) {
    this.changeSchema = {
      properties: {
        CZE028: {
          type: 'string',
          title: '服务ID',
          readOnly: true,
          default: rec.CZE028,
        },
        CZE029: {
          type: 'string',
          title: '调用入口名称',
          default: rec.CZE029,
        },
        CZE030: {
          type: 'string',
          title: '服务名称',
          default: rec.CZE030,
        },
        CZE031: {
          type: 'string',
          title: '调用过程',
          default: rec.CZE031,
        },
      },
      required: ['CZE028', 'CZE029', 'CZE030', 'CZE031'],
    };
    this.chVisible = true;
  }

  fwuphandleCancel() {
    this.chVisible = false;
  }

  fwuphandleOk() {
    this.lbservice
      .lbservice('FR_fwzcupdate', { para: this.changePop.value })
      .then(resdata => {
        if (resdata.code > 0) {
          this.msgSrv.success('更改成功');
        } else {
          this.msgSrv.error(resdata.errmsg);
        }
        this.chVisible = false;
        this.stReload();
      });
  }
}
