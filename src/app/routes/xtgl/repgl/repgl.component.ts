import { Component, OnInit, ViewChild } from '@angular/core';
import { STColumn, STComponent } from '@delon/abc';
import { SFComponent, SFSchema } from '@delon/form';
import { NzMessageService } from 'ng-zorro-antd/message';
import { UploadFile } from 'ng-zorro-antd/upload';
//引入HttpClient
import { HttpRequest, HttpClient, HttpResponse } from '@angular/common/http';
import { filter } from 'rxjs/operators';

import { RepserviceService } from '@core/lb/repservice.service';
import { Rep01Component } from '../rep01/rep01.component';
import { SafeResourceUrl, DomSanitizer } from '@angular/platform-browser';

import { NzModalService } from 'ng-zorro-antd/modal';
import { LbRepComponent } from '@shared/components/lb-rep/lb-rep.component';
import { HttpService } from 'lbf';

@Component({
  selector: 'app-repgl',
  templateUrl: './repgl.component.html',
  styleUrls: ['./repgl.component.less'],
})
export class RepglComponent implements OnInit {
  isVisible = false;
  isVisible1 = false;

  params = { sname: 'query_rep01', form: {} };

  roleid: string;

  fileList: UploadFile[] = [];
  uploading = false;

  @ViewChild('sf', { static: false }) sf: SFComponent;
  @ViewChild('st', { static: false }) st: STComponent;
  @ViewChild('pop', { static: false }) pop: SFComponent;

  columns: STColumn[] = [
    { title: '报表id', index: 'REPID' },
    // { title: '报表版本号', index: 'REPVER', type: 'number' },
    // { title: '参数模板', index: 'REPPAR' },
    { title: '报表名称', index: 'REPNAME' },
    { title: '报表地址', index: 'REPURL' },
    { title: '操作员', index: 'AAE011' },
    { title: '操作日期', index: 'AAE036'},
   
    {
      title: '操作区',
      buttons: [
        {
          text: '更改',
          click: (record: any) => this.updateService(record),
        },
      ],
    },
  ];

  schema: SFSchema = {
    properties: {
      REPID: {
        type: 'string',
        title: '报表id',
      },
      REPNAME: {
        type: 'string',
        title: '报表名称',
      },
    },
    ui: {
      grid: {
        span: 6,
      },
    },
  };

  newSchema: SFSchema = {
    properties: {
      REPID: {
        type: 'string',
        title: '报表id',
        readOnly: true,
      },
      REPNAME: {
        type: 'string',
        title: '报表名称',
      },
      REPURL: {
        type: 'string',
        title: '报表地址',
      },
    },
    required: ['DAD017', 'DAD090', 'REPURL', 'REPNAME'],
  };

  constructor(
    private http: HttpClient,
    private msg: NzMessageService,
    private sanitizer: DomSanitizer,
    private repservice: RepserviceService,
    // private lbRepComponent: LbRepComponent,
    private modalService: NzModalService,
    private lbservice: HttpService,
  ) {}

  updateService(rec: any) {
    this.pop.formData = rec;
    this.isVisible = true;
    this.pop.reset();
  }

  nzOnOk(){
    if (!this.pop.valid) {
      this.msg.error('必填项不能为空！');
      return;
    }
    this.lbservice.lbservice('rep_insert', this.pop.value).then(resdata => {
      if (resdata.code < 1) {
        this.msg.error(resdata.errmsg);
      } else {
        this.pop.formData = null;
        this.pop.reset();
        this.msg.success('successfully.');
        this.isVisible = false;
        this.getdata();
        this.isVisible=false
      }
    });

   
  }

  handleCancel() {
    this.isVisible = false;
    this.isVisible1 = false;
  }
  /*
  url1: any;
  repid = '17';
  inpara = { roleid: '1' };
  ifrmeee: SafeResourceUrl;
  */

  print() {
    /* this.repservice.getRep01(this.repid, this.inpara).then(resdata => {
      console.log('resdata=============' + resdata);
      this.url1 = resdata;
      console.log('url1111111111========' + this.url1);
      if (this.url1 != null) {
        // document.getElementById('div1').style.display = 'block';
        this.ifrmeee = this.sanitizer.bypassSecurityTrustResourceUrl(this.url1);
        this.isVisible1 = true;
      } else {
        alert('不存在对应报表');
      }
    });
*/
    /*
    this.url1 = this.repservice.getRep(this.repid, this.inpara);

    console.log('url1111111111========' + this.url1);
    if (this.url1 != null) {
      // document.getElementById('div1').style.display = 'block';
      this.ifrmeee = this.sanitizer.bypassSecurityTrustResourceUrl(this.url1);
      this.isVisible1 = true;
    } else {
      alert('不存在对应报表');
    }
*/

    const modal = this.modalService.create({
      nzTitle: '打印',
      nzContent: LbRepComponent,
      nzComponentParams: {
        repid: '19',
        inpara: { dad010: '164' },
      },
      nzWidth: 880,
    });
  }

  //新建服务相关
  add() {
    this.pop.formData = null;
    this.pop.reset();
    this.isVisible = true;
  }

  getdata() {

    this.st.reload(this.sf.value);
  }

  beforeUpload = (file: UploadFile): boolean => {
    this.fileList = this.fileList.concat(file);

    return false;
  };

  handleUpload(): void {
    const formData = new FormData();
    formData.append('detall', JSON.stringify(this.pop.value));
    this.fileList.forEach((file: any) => {
      formData.append('files[]', file);
    });
    this.uploading = true;
    // You can use any AJAX library you like

    const req = new HttpRequest('POST', '/rep/service', formData, {});
    this.http
      .request(req)
      .pipe(filter(e => e instanceof HttpResponse))
      .subscribe(
        data => {
          this.uploading = false;
          console.log('data---->' + JSON.stringify(data['body']));
          this.pop.formData = data['body'];
          this.pop.reset();
          this.fileList = [];
          this.msg.success('upload successfully.');
        },
        data => {
          this.uploading = false;
          console.log('data---->' + JSON.stringify(data));
          this.msg.error('upload failed.');
        },
      );
  }

  ngOnInit() {}
}
