import { NgModule } from '@angular/core';
import { SharedModule } from '@shared';
import { XtglRoutingModule } from './xtgl-routing.module';
import { XtglPwdChangeComponent } from './pwd-change/pwd-change.component';
import { XtglFwzcComponent } from './fwzc/fwzc.component';
import { XtglFwsqComponent } from './fwsq/fwsq.component';
import { XtglGnglComponent } from './gngl/gngl.component';
import { XtglCdglComponent } from './cdgl/cdgl.component';
import { XtglThemeComponent } from './theme/theme.component';
import { XtglThemeDetailComponent } from './theme/detail/detail.component';
import { XtglJsqxComponent } from './jsqx/jsqx.component';
import { RepglComponent } from './repgl/repgl.component';
import { HttpClientModule } from '@angular/common/http';
import { Rep01Component } from './rep01/rep01.component';

const COMPONENTS = [
  XtglPwdChangeComponent,
  XtglFwzcComponent,
  XtglFwsqComponent,
  XtglGnglComponent,
  XtglCdglComponent,
  XtglThemeComponent,
  XtglThemeDetailComponent,
  XtglJsqxComponent,
  RepglComponent,
  Rep01Component,];
const COMPONENTS_NOROUNT = []; //打印弹出框需要

@NgModule({
  imports: [SharedModule, XtglRoutingModule, HttpClientModule], //声明HttpClientModule
  declarations: [...COMPONENTS, ...COMPONENTS_NOROUNT],
  entryComponents: COMPONENTS_NOROUNT,
})
export class XtglModule {}
