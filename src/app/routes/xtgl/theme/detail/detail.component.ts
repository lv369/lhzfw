import { Component, OnInit, Input } from '@angular/core';
import { _HttpClient } from '@delon/theme';
import { NzModalRef } from 'ng-zorro-antd';
import { HttpService } from 'lbf';
import { STColumn } from '@delon/abc';

@Component({
  selector: 'app-xtgl-theme-detail',
  templateUrl: './detail.component.html',
})
export class XtglThemeDetailComponent implements OnInit {
  @Input()
  record:any;

  param:any={
    sname:"",
    form: {},
  }
  theme:any = this.record;
  dataset:any[] =[{
    DAA020:'1',
    DAA021:'2',
    DAA022:'3'
  }]
  columns:STColumn[]=[
    {title:'指标ID',index:'DAA020'},
    {title:'指标编码',index:'DAA021'},
    {title:'指标明称',index:'DAA022'},
  ]

  constructor(private modal: NzModalRef,
              private lbservice: HttpService) { }

  ngOnInit() {
      console.log('record :', this.record);
      this.theme=this.record
   }

   

}
