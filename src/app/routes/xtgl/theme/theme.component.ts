import { Component, OnInit, ViewChildren, ViewChild } from '@angular/core';
import { HttpService, GridComponent, PopdetailComponent, SupDic } from 'lbf';
import { STColumn, STPage, STComponent, STData } from '@delon/abc';
import { SFSchema, SFComponent } from '@delon/form';
import { of } from 'rxjs';
import { NzMessageService } from 'ng-zorro-antd';
import { compareAsc } from 'date-fns';
import { XtglThemeDetailComponent } from './detail/detail.component';
import { deepMergeKey } from '@delon/util';
import { LbCronComponent } from '@shared/components/lb-cron/lb-cron.component';


@Component({
  selector: 'app-xtgl-theme',
  templateUrl: './theme.component.html',
})
export class XtglThemeComponent implements OnInit {

  dataset = []
  isVisible = false
  test = "hhh"
  params = {
    sname: "FR_querytheme",
    form: {},
    savesname: "FR_addtheme",
    upadate: "FR_updatetheme"
  }
  stpage: STPage = {
    total: true,
    show: true,// 显示分页
    front: false // 关闭前端分页，true是前端分页，false后端控制分页
  };
  constructor(private lbservice: HttpService,
    private message: NzMessageService,
    private supdic: SupDic) {
  }

  @ViewChild('ccpop',{static:false}) ccpop: SFComponent;
  @ViewChild('st', { static: false }) st: STComponent;
  @ViewChild('pop', { static: false }) pop: SFComponent;
  //新增内容
  newSchema: SFSchema = {
    properties: {
      'DAA029': {
        type: 'string',
        title: '主题名称',
      },
      // 'DAE001': {
      //   type: 'string',
      //   title: '主题分类',
      // },
      'DAA030': {
        type: 'string',
        title: '主题颜色',
        ui:{
          widget:'custom',
        }
      }
    },
    required: ['DAA029', 'DAE001', 'DAA030']
  }
  //表格数据
  columns: STColumn[] = [
    {
      title: '操作', index: 'op', render: 'custom'

    },
    {
      title: '标签ID', index: 'DAA028',
      sort: { compare: (a, b) => Number(a.DAA028 - b.DAA028) },

    },
    { title: '标签名称', index: 'DAA029' },
    { title: '标签颜色', index: 'DAA030',render:'colorSelect' },
    {
      title: '主题状态', index: 'AAE100',
      type: "tag", tag: {
        1: { text: "启用", color: "green" },
        0: { text: "停用", color: "red" },
      },
      filter: {
        menus: [{ text: "正常", value: "1" }, { text: "禁用", value: "0" }],
        fn: (filter, record) => record.AAE100 === filter.value,
        multiple: false,
      }
    },
    {
      title:'操作区',
      buttons:[
        {
          text:'查看',
          type:'modal',
          modal:{component:XtglThemeDetailComponent,},
          click:(record,modal)=>{

          }
        }
      ]
    }
  ]

  yslist=[];
  ngOnInit() {
    this.initSearch();
    this.lbservice.lbservice("zbgl_qyszd",{}).then(resdata=>{
      this.yslist=resdata.message.list;
      this.yslist.forEach(ele=>{
        ele.value=ele.VALUE;
        ele.label=ele.LABEL;
      });
    });
    this.querydata()
    
  }


  add() {
    this.isVisible = true;
    this.newColor='';
    // 监控改变刷新表单
    // this.pop.isVisibleChange.subscribe(
    //   () => this.querydata())
  }

  querydata() {
    this.lbservice.lbservice(this.params.sname, { para: {} }).then(
      (resdata) => {
        if (resdata.code > 0) {
          this.dataset = resdata.message.list;
          this.st.reload()
        }
      }
    )
  }


  ztAddhandleCancel() {
    this.isVisible = false;
  }
  
  ztAddhandleOk(){
    this.lbservice.lbservice("FR_addtheme",{'para':{"DAA029":this.pop.value.DAA029,"DAA030":this.newColor}}).then(resdata=>{
      if(resdata.code>0){
        this.message.success('更改成功');
        this.querydata();
        this.isVisible = false;
      }else{
        this.message.error(resdata.errmsg);
      }
    });
  }


  changeState(item) {
    const rec: any =JSON.parse(JSON.stringify(item)) 

    this.lbservice.lbservice(this.params.upadate, { para: { DAA028: rec.DAA028, AAE100: rec.AAE100 } }).then(
      (resdata) => {
        if (resdata.code > 0) {
          // 修改一行数据
          item.AAE100 = rec.AAE100 === '0' ? '1' : '0'
          
		     // tslint:disable-next-line: prefer-conditional-expression
		     if(item.AAE100 ==='1')
          item._values[5] = {text: "正常", org: "1", color: "green"}
          else
          item._values[5] = {text: "禁用", org: "0", color: "red"}
          this.st.cd()          
          this.message.success('调用服务成功！');
        }
        else {
          this.message.error('调用服务失败！');
        }
      }
    )
  }
  // setRow(index: number, item: STData): this {
  //   this.st._data[index] = deepMergeKey( this.st._data[index], false, item);
  //   this.st._data =  this.st.dataSource.optimizeData({ columns:  this.st._columns, result:  this.st._data, rowClassName: this.st.rowClassName });
  //   this.st.cdr.detectChanges();
  //   return this;
  // }

  colorChange(eve:any,idx:number){
    idx=(this.st.pi-1)*10+idx;
    this.initupdateSchema(this.st.data[idx]);
    this.nowColor=this.st.data[idx].DAA030;
    this.chVisible=true;
  }

  chVisible=false;
  changeSchema: SFSchema = {properties: {},}
  formData={};
  nowColor='';
  newColor='';

  handleCancel() {
    this.chVisible = false;
  }
  
  handleOk(){
    this.lbservice.lbservice("ztgl_ztysupdate",{"DAA028":this.ccpop.value.DAA028,"DAA030":this.nowColor}).then(resdata=>{
      if(resdata.code>0){
        this.message.success('更改成功');
        this.querydata();
        this.chVisible=false;
      }else{
        this.message.error(resdata.errmsg);
      }
    });
  }

  initupdateSchema(rec:any){
    const temp:SFSchema = {
      properties: {
        DAA028:{
          type:'string',
          title:'编码',
          default:rec.DAA028,
          ui:{
            hidden:true,
          }
        },
        DAA029:{
          type:'string',
          title:'名称',
          default:rec.DAA029,
          readOnly:true,
        },
        DAA030:{
          type:'string',
          title:'颜色',
          default:rec.DAA030,
          ui:{
            widget:'custom',
          }
        }
      }

  }
    this.changeSchema=temp;
  }

/********************************************查询********************************************************* */
searchSchema: SFSchema={properties:{}}
initSearch(){
    this.searchSchema = {
      properties: {
        DAA028:{
          type:'string',
          title:'标签ID',
        },
        DAA029:{
          type:'string',
          title:'标签名称',
        },
        AAE100:{
          type:'string',
          title:'标签状态',
          enum:this.supdic.getSFDic('zbzt'),
          ui:{
            widget:'select',
          }
        }
    },
    ui: {
      width:200,
      grid: {span: 5 }
    },
  }
}

@ViewChild('sf',{static:false}) sf: SFComponent;
query(){
  console.log(this.cron.cron);
  this.lbservice.lbservice(this.params.sname, { para: this.sf.value }).then(
    (resdata) => {
      if (resdata.code > 0) {
        this.dataset = resdata.message.list;
        this.st.reload()
      }
    }
  )
}

@ViewChild('cron',{static:false}) cron : LbCronComponent;
getValue(ev){
  console.log(ev);
}
}
