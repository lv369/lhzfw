import { Component, OnInit, ViewChild } from '@angular/core';
import { _HttpClient } from '@delon/theme';
import { SFSchema, SFComponent } from '@delon/form';
import { NzMessageService } from 'ng-zorro-antd';
import { HttpService, ServiceInfo } from 'lbf';


@Component({
  selector: 'app-xtgl-pwd-change',
  templateUrl: './pwd-change.component.html',
})
export class XtglPwdChangeComponent implements OnInit {

  constructor(private lbservice: HttpService,
    private msgsvr: NzMessageService,
    private serviceInfo: ServiceInfo, ) { }

  @ViewChild('sf', { static: false }) sf: SFComponent;

  schema: SFSchema = {
    properties: {
      oldpwd: {
        type: 'string',
        title: '当前密码',
        ui: {
          type: "password",
        }
      },
      newpwd: {
        type: 'string',
        title: '新密码',
        minLength: 6,
        maxLength: 30,
        ui: {
          type: "password",

        }
      },
      cnfpwd: {
        type: 'string',
        title: '确认密码',
        minLength: 6,
        maxLength: 30,
        ui: {
          type: "password",

        }
      }
    },
    required: ["oldpwd", "newpwd", "cnfpwd"]
  };


  ngOnInit() {

  }

  submit(value: any) {

    if (!value.oldpwd) {

      this.msgsvr.error('请输入当前密码');

      return;
    }

    if (!value.newpwd) {

      this.msgsvr.error('请输入新密码');

      return;
    }


    if (!value.cnfpwd) {

      this.msgsvr.error('请输入确认密码');

      return;
    }

    if (value.newpwd !== value.cnfpwd) {
      this.msgsvr.error('确认密码与新密码不一致');

      return;
    }


    this.lbservice.lbservice('FR_pwdChang', { para: { oldpwd: this.serviceInfo.pwdmd5(value.oldpwd), newpwd: this.serviceInfo.pwdmd5(value.newpwd) } }).then(resdata => {
      if (resdata.code < 1) {
        this.msgsvr.error(resdata.errmsg);
      } else {
        this.msgsvr.success('密码修改成功！');
      }

    })

  }

}
