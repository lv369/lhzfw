import { Component, OnInit } from '@angular/core';
import { SafeResourceUrl, DomSanitizer } from '@angular/platform-browser';
import { RepserviceService } from '@core/lb/repservice.service';

@Component({
  selector: 'app-rep01',
  templateUrl: './rep01.component.html',
  styleUrls: ['./rep01.component.less'],
})
export class Rep01Component implements OnInit {
  constructor(
    private sanitizer: DomSanitizer,
    private repservice: RepserviceService,
  ) {}

  repid: string;
  inpara: string;
  label01: string;

  url = 'http://60.191.106.236:81/WebReport/ReportServer?reportlet=test01.cpt';
  url1: any;
  ifrmeee: SafeResourceUrl;

  ngOnInit() {
    //this.ifrmeee = this.sanitizer.bypassSecurityTrustResourceUrl(this.url);

    this.repservice.getRep(this.repid, this.inpara).then(resdata => {
      this.url1 = resdata;
      if (this.url1 != null) {
        document.getElementById('div1').style.display = 'block';
        this.ifrmeee = this.sanitizer.bypassSecurityTrustResourceUrl(this.url1);
      } else {
        this.label01 = '不存在对应报表';
        this.ifrmeee = this.sanitizer.bypassSecurityTrustResourceUrl(this.url);
        document.getElementById('div2').style.display = 'block';
      }
    });
  }

  ngAfterViewInit() {}
}
