import { NgModule } from '@angular/core';

import { SharedModule } from '@shared';
import { RouteRoutingModule } from './routes-routing.module';
// dashboard pages
import { DashboardComponent } from './dashboard/dashboard.component';
// passport pages
import { UserLoginComponent } from './passport/login/login.component';
import { UserLockComponent } from './passport/lock/lock.component';
// single pages
import { CallbackComponent } from './callback/callback.component';
import { DashboardWorkplaceComponent } from './dashboard/workplace/workplace.component';
import { Workplace2Component } from './dashboard/workplace2/workplace2.component';
import { LbRepComponent } from '@shared/components/lb-rep/lb-rep.component';


const COMPONENTS = [
  DashboardComponent,
  DashboardWorkplaceComponent,
  UserLoginComponent,
  UserLockComponent,
  CallbackComponent,
];
const COMPONENTS_NOROUNT = [LbRepComponent];

@NgModule({
  imports: [SharedModule, RouteRoutingModule],
  declarations: [...COMPONENTS, ...COMPONENTS_NOROUNT, Workplace2Component],
  entryComponents: COMPONENTS_NOROUNT,
  providers: [],
})
export class RoutesModule { }
