import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { XxglRytjComponent } from './rytj.component';

describe('XxglRytjComponent', () => {
  let component: XxglRytjComponent;
  let fixture: ComponentFixture<XxglRytjComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ XxglRytjComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(XxglRytjComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
