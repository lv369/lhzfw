import { Component, OnInit, ViewChild } from '@angular/core';
import { _HttpClient, ModalHelper } from '@delon/theme';
import { STColumn, STComponent } from '@delon/abc/table';
import { SFSchema, SFComponent } from '@delon/form';
import { GridComponent, HttpService, SupDic } from 'lbf';
import { PrintComponent } from '@shared/components/tyjb/lb_print';
import { NzMessageService } from 'ng-zorro-antd';

@Component({
  selector: 'app-xxgl-rytj',
  templateUrl: './rytj.component.html',
})
export class XxglRytjComponent implements OnInit {
  queryPara: any;
  isShowPrint = false; // 打印窗口是否显示
  hidden_Print = true; // 隐藏打印框
  formData :any ={};
  title='打印请指定中心,否则打印六大中心！';
  params = { sname: 'quer_rytj', form: {} };
  schema: SFSchema = {
    properties: {
      FZXCODE: {
        type: 'string',
        title: '中心名称',
        enum: this.supdic.getSFDic('DGB020'),
     
        ui: { widget: 'select', allowClear: true, }


      },
      KSRQ: {
        type: 'string',
        title: '窗口收件时间',

        ui: { widget: 'date', end: 'JSRQ', format: 'YYYYMMDD' },
      },
      JSRQ: {
        type: 'string',
        ui: { widget: 'date', end: 'JSRQ', format: 'YYYYMMDD' },
      },

    }, ui: {
      grid: { span: 8 },
      spanLabelFixed: 150,
    
    },required:['FZXCODE']
  };
  @ViewChild('st', { static: false }) st: GridComponent;
  @ViewChild('sf', { static: false }) sf: SFComponent;
  @ViewChild('print', { static: false }) print: PrintComponent;
  columns: STColumn[] = [
    { title: '中心名字', index: 'SLFZX', className: 'text-center' },
    { title: '部门名字', index: 'SLBM', className: 'text-center' },
    { title: '人员名字', index: 'TNAME', className: 'text-center' },
    { title: '待流转数量', index: 'DLZ', className: 'text-center' },
    { title: '待办理数量', index: 'DBL', className: 'text-center' },
    { title: '办结成功数量', index: 'BJCG', className: 'text-center' },
    { title: '办结失败数量', index: 'BJSB', className: 'text-center' },
    { title: '事项转交数量', index: 'SXZJ', className: 'text-center' },
    { title: '司法确认数量', index: 'SFQR', className: 'text-center' },


  ];

  constructor(private http: _HttpClient,
    private supdic: SupDic, private httpService: HttpService, private modal: ModalHelper, private supDic: SupDic, private msgSrv: NzMessageService) { }


  ngOnInit() { }

 
  getdata() {
    this.st.reload(this.sf.value);


  }


  doPrint() {
 
       




    if (this.sf.value.FZXCODE === undefined && this.sf.value.KSRQ !== '' && this.sf.value.KSRQ !== '') {
      this.print.QueryPara = { FZXCODE: '8', KSRQ: this.sf.value.KSRQ, JSRQ: this.sf.value.JSRQ }
    } else if (this.sf.value.FZXCODE !== undefined && this.sf.value.KSRQ === '' && this.sf.value.KSRQ === '') {
      this.print.QueryPara = { FZXCODE: this.sf.value.FZXCODE, KSRQ: '19700101', JSRQ: '20220101' }
    } else if (this.sf.value.FZXCODE === undefined && this.sf.value.KSRQ === '' && this.sf.value.KSRQ === '') {
      
      
      // if (!this.sf.valid) {
      //   return this.msgSrv.error("没有填写必填项")
  
      // }

      this.print.QueryPara = { FZXCODE: '8', KSRQ: '19700101', JSRQ: '20220101' }
    }
    else {
      this.print.QueryPara = { FZXCODE: this.sf.value.FZXCODE, KSRQ: this.sf.value.KSRQ, JSRQ: this.sf.value.JSRQ }
    }
    this.print.RepId = '23';
    this.isShowPrint = true;
    this.hidden_Print = false;
  }



  CloseWindow() {
    this.isShowPrint = false;
    this.hidden_Print = true;
  }

}
