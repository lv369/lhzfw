import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { XxglBmtjComponent } from './bmtj.component';

describe('XxglBmtjComponent', () => {
  let component: XxglBmtjComponent;
  let fixture: ComponentFixture<XxglBmtjComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ XxglBmtjComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(XxglBmtjComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
