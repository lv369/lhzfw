import { Component, OnInit, ViewChild } from '@angular/core';
import { _HttpClient, ModalHelper } from '@delon/theme';
import { STColumn, STComponent } from '@delon/abc/table';
import { SFSchema, SFComponent } from '@delon/form';
import { GridComponent, HttpService, SupDic } from 'lbf';
import { NzMessageService } from 'ng-zorro-antd';
import { PrintComponent } from '@shared/components/tyjb/lb_print';

@Component({
  selector: 'app-xxgl-bmtj',
  templateUrl: './bmtj.component.html',
})
export class XxglBmtjComponent implements OnInit {
  queryPara: any;
  isShowPrint = false; // 打印窗口是否显示
  hidden_Print = true; // 隐藏打印框
  params = { sname: 'quer_bmsjtj', form: {} };
  schema: SFSchema = {
    properties: {
      FZXCODE: {
        type: 'string',
        title: '中心名称',
        enum: this.supdic.getSFDic('DGB020'),
        ui:{widget:'select',  allowClear: true,}
      },
      // SLBM: {
      //   type: 'string',
      //   title: '部门名称',
      //   enum: this.supdic.getSFDic('BM'),
      //   ui:{widget:'select',  allowClear: true,}
      // },
      KSRQ: {
        type: 'string',
        title: '窗口收件时间',
        format: 'YYYYMMDD',
        ui: { widget: 'date', end: 'JSRQ', format: 'YYYYMMDD' },
      },
      JSRQ: {
        type: 'string',
        format: 'YYYYMMDD',
        ui: { widget: 'date', end: 'JSRQ', format: 'YYYYMMDD' },
      },
     
    },ui:{
       grid:{span:8},
       spanLabelFixed:150
    },required:['FZXCODE']
  };
  @ViewChild('st', { static: false }) st: GridComponent;
  @ViewChild('sf', { static: false }) sf: SFComponent;
  @ViewChild('print', { static: false }) print: PrintComponent;
  columns: STColumn[] = [
    { title: '中心名字', index: 'SLFZX', className:'text-center'},
    { title: '部门名字', index: 'SLBM',className:'text-center' },
    { title: '待流转数量', index: 'DLZ', className:'text-center' },
    { title: '待办理数量', index: 'DBL', className:'text-center' },
    { title: '办结成功数量', index: 'BJCG' , className:'text-center'},
    { title: '办结失败数量', index: 'BJSB' , className:'text-center'},
    { title: '事项转交数量', index: 'SXZJ' , className:'text-center'},
    { title: '司法确认数量', index: 'SFQR', className:'text-center' },
   

  ];

  constructor(private http: _HttpClient,
    private supdic: SupDic, private httpService: HttpService,private modal: ModalHelper, private supDic: SupDic,private msgSrv:NzMessageService) { }


  ngOnInit() { }

  // add(){
            
  //   this.httpService.lbservice('list_bmsjtj',{}).then(resdata=>{
  //     if(resdata.code<1)
  //     {
  //       this.msgSrv.error(resdata.errmsg);
  //     }
      
  //  })
  // }

  getdata() {
    this.st.reload(this.sf.value);
  
  }

 
  doPrint() {
    if (this.sf.value.FZXCODE === undefined && this.sf.value.KSRQ !== '' && this.sf.value.KSRQ !== '') {
      this.print.QueryPara = { FZXCODE: '8', KSRQ: this.sf.value.KSRQ, JSRQ: this.sf.value.JSRQ }
    } else if (this.sf.value.FZXCODE !== undefined && this.sf.value.KSRQ === '' && this.sf.value.KSRQ === '') {
      this.print.QueryPara = { FZXCODE: this.sf.value.FZXCODE, KSRQ: '19700101', JSRQ: '20220101' }
    } else if(this.sf.value.FZXCODE ===undefined && this.sf.value.KSRQ === '' && this.sf.value.KSRQ === ''){
      this.print.QueryPara = { FZXCODE: '8', KSRQ: '19700101', JSRQ: '20220101' }
    }    
    else{
      this.print.QueryPara = { FZXCODE: this.sf.value.FZXCODE, KSRQ: this.sf.value.KSRQ, JSRQ: this.sf.value.JSRQ }
    }
    this.print.RepId = '22';
    this.isShowPrint = true;
    this.hidden_Print = false;
  }



  CloseWindow() { 
    this.isShowPrint = false;
    this.hidden_Print = true;
  }

}