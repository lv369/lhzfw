import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { XxglZxtjComponent } from './zxtj.component';

describe('XxglZxtjComponent', () => {
  let component: XxglZxtjComponent;
  let fixture: ComponentFixture<XxglZxtjComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ XxglZxtjComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(XxglZxtjComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
