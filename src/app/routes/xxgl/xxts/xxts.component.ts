import { Component, OnInit, TemplateRef, ViewChild, AfterViewInit, Input, Output, EventEmitter } from '@angular/core';
import { _HttpClient } from '@delon/theme';
import { NzNotificationService,NzMessageService,NzConfigService} from 'ng-zorro-antd';
import { fixEndTimeOfRange } from '@delon/util';
import {ServiceInfo,HttpService} from 'lbf';
import {Router,ActivatedRoute} from '@angular/router';
import { setInterval } from 'timers';
import { ReuseTabService } from '@delon/abc';

@Component({
  selector: 'app-tyjb-xxts',
  templateUrl: './xxts.component.html',
})
export class TyjbXxtsComponent implements OnInit,AfterViewInit {
  @Output() voted = new EventEmitter();

 @Input() FR_MESSAGE = {XXID: 4,XTYPE: '1',XTITLE: '办理通知',
                 XCONTENT:'美国约翰斯·霍普金斯大学发布的实时统计数据显示，截至北京时间4月23日7时38分，'
                 + '全球累计确诊新冠肺炎病例2623415例，死亡183027例，美国新冠肺炎确诊病例约84万例。'
                 + ' 美国2月初已出现新冠病毒死亡病例，据美国有线电视新闻网报道，2月6日与17日在加利福尼亚州北部圣克拉拉县出现的两例死亡病例，'+
                 +'目前被发现是美国境内最早死于新冠病毒感染的病例。据《洛杉矶时报》22日报道，20日，美国加利福尼亚州的法医透露，根据尸检报告显示，目前发现的全美最早一个新冠肺炎死亡病例于2月6日就出现了。'
                 +'此前，美国疾控中心确认的首例死亡病例出现时间为2月29日，提前了近一个月。同时，因为早期检测不到位，有很多的死亡患者未被确诊，'
                 +'疫情暴发时间可能在去年12月。',
                 SHOWTYPE: '1',FSR:'1',FSSJ: new Date().toLocaleDateString()+" "+
                 new Date().getHours()+":"+
                 new Date().getMinutes()+":"+
                 new Date().getSeconds()}

  //  FR_MESSAGE = {XXID: 5,XTYPE: '2',XTITLE: '业务提醒',
  //                XCONTENT:'/xtgn/sdgl',
  //                SHOWTYPE: '2',FSR:'1',FSSJ: new Date()}

  color = this.FR_MESSAGE.XTYPE==='1'?'#2db7f5':this.FR_MESSAGE.XTYPE==='2'?'#fa8c16':'#f50' ;
  style={color: this.color}

  XCONTENT = ''

  @ViewChild('template',{static: false}) template: TemplateRef<{}>;
  constructor(private notification: NzNotificationService,private router: Router,
      private service: ServiceInfo,private httpService: HttpService,private msgSrv: NzMessageService,
      private reuseTabSrv: ReuseTabService,
      private configSrv:NzConfigService) {
        this.configSrv.set('notification',{nzPlacement: 'bottomRight'})
      }
  

  ngOnInit() {
     // this.saveMessage();

    
     
  
    
   }

   ngAfterViewInit(): void {
     
    this.openNotification(this.template);

  }

   saveMessage(){
     
     this.httpService.lbservice('XXGL_SAVE',{MESSAGE: this.FR_MESSAGE});
   }

  openNotification(template): void {
    
    if(this.FR_MESSAGE.XTYPE==='1'){
      this.XCONTENT = this.FR_MESSAGE.XCONTENT.length>100? `${this.FR_MESSAGE.XCONTENT.slice(0,100)}...`:this.FR_MESSAGE.XCONTENT;
    }   
    else{
      console.log('-------ccc-------------')
      this.XCONTENT = JSON.parse(this.FR_MESSAGE.XCONTENT).MSG;
      console.log(this.XCONTENT)
    }
    
   
    this.notification.template(template);
    console.log('-------ddd-------------')
   
  }
  // 点击查看
  click(){
    // 如果消息类型是1 通知，则跳转到收件箱，并显示消息内容
    
    if(this.FR_MESSAGE.XTYPE==='1'){
      this.FR_MESSAGE['JSR'] = this.service.user.aac003;
      this.voted.emit(this.FR_MESSAGE);
    }
    // 如果是提醒和警报
    else{
      //{URL:'',MSG:'',PARA:{},STEP:'1',INFO:'收件提醒'}
      const msgInfo:any = JSON.parse(this.FR_MESSAGE.XCONTENT)
      this.httpService.lbservice('PUBLIC_PARA',msgInfo).then(resdata=>{
        if(resdata.code<1)
        {
          this.msgSrv.error(resdata.errmsg);
        }
        else
        {
          console.log('----------replace---------------')
          this.reuseTabSrv.replace(msgInfo.URL);
          // this.router.navigate([msgInfo.URL])
          this.notification.remove()
        }      
      })
    }    
  }
}