import { Component, OnInit, Input, Output, EventEmitter, OnChanges } from '@angular/core';
import { _HttpClient } from '@delon/theme';
import {Router,ActivatedRoute} from '@angular/router';
import {HttpService,SupDic, ServiceInfo} from 'lbf';
import { ReuseTabService } from '@delon/abc';
import { NzMessageService } from 'ng-zorro-antd';
import { RefService } from '@core/lb/RefService';

@Component({
  selector: 'app-tyjb-xxxq',
  templateUrl: './xxxq.component.html',
})
export class TyjbXxxqComponent implements OnInit,OnChanges {

  @Input() xxDetail:any = {}
  @Output() voted = new EventEmitter();
  FSR;
  JSR;
  
  @Output() ViewMsg = new EventEmitter<string>();

  constructor(private http: _HttpClient,private router: ActivatedRoute,private supdic: SupDic,private msgSrv: NzMessageService,
              private httpService: HttpService,private reuseTabSrv: ReuseTabService,private routers: Router,
              private service: ServiceInfo,
              private ref:RefService,) { }

  ngOnChanges(changes: import("@angular/core").SimpleChanges): void {
    if(changes.xxDetail.currentValue !== undefined){
      this.FSR = this.supdic.getdicLabel('USERID',this.xxDetail['FSR']);
    }
  }

  ngOnInit() { 
  
    this.JSR = this.service.user.aac003;
 
  }

  /*
    goURL 跳转地址
    para 携带参数
  */
  gotoUrl(goURL:string/*,CUR_XXID:number*/,para:string){

    // 隐藏抽屉 外放方法
    this.ViewMsg.emit(goURL);
    this.ref.sendRef({receive:'shgl',message:JSON.parse(para),sender:'message',createTime:new Date(),isGoto:true,url:goURL})
/*
    this.httpService.lbservice('PUBLIC_PARA',{XXID:CUR_XXID}).then(resdata=>{
      if(resdata.code<1)
      {
        this.msgSrv.error(resdata.errmsg);
      }
      else
      {
        // 隐藏抽屉 外放方法
        this.ViewMsg.emit(goURL);
        
        // 没办法，暂时到中转页面去转悠一下       
        // this.routers.navigate(['/tyjb/sxcl'],{queryParams:{R:goURL}})
        this.ref.sendRef({receive:'shgl',message:{R:goURL},sender:'message',createTime:new Date(),isGoto:true,url:goURL})
        
       
      }      
    })
    */
    
  }

}
