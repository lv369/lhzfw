import { Component, OnInit, ViewChild, Output, EventEmitter } from '@angular/core';
import { _HttpClient } from '@delon/theme';
import { SFSchema, SFComponent } from '@delon/form';
import { NzMessageService } from 'ng-zorro-antd';
import {HttpService, GridComponent} from 'lbf';
import { STColumn } from '@delon/abc';
import {ReuseTabService} from '@delon/abc/reuse-tab';

@Component({
  selector: 'app-xtgl-xxfs',
  templateUrl: './xxfs.component.html'
})
export class XtglXxfsComponent implements OnInit {

  value=''
  condition= null

  // 消息发送维护父组件  
  xxfsFlag;
  @Output() voted = new EventEmitter<boolean>();

  isTsVisible = false;
  editSchema: SFSchema = {
    properties: {
      XTITLE: {
        type: 'string',
        title: '标题',

      },
      XCONTENT: {
        type: 'string',
        title: '正文',
        ui: {
          widget: 'textarea',
          autosize: { minRows: 21}
        }
      }
    },
    ui: {
      spanLabelFixed:50,
      grid: {
        span: 24
      }
    }
  }
  
  @ViewChild('st',{static:false}) st: GridComponent;
  @ViewChild('sf',{static:false}) sf: SFComponent;
  constructor(private http: _HttpClient,private msgSrv: NzMessageService,private httpService: HttpService,
    private reuseService: ReuseTabService) { }

  ngOnInit() { 
    
  }

  send(){
    // if(this.yhList.length === 0 ){
    //   this.msgSrv.error('请填写收件人之后再发送');
    //   return;
    // }
    if(this.sf.value.XTITLE === undefined || this.sf.value.XTITLE ===''){
     this.isTsVisible = true
    }else{
      this.confirm();
    }
  }

  formChange(event){
    if(event.hasOwnProperty('XTITLE')){
      this.condition = event.XTITLE;
      if(this.condition === ''){
        this.condition = null;
      }
    }
  }

  confirm(){
    this.isTsVisible = false;
    if(this.sf.value.XTITLE === undefined || this.sf.value.XTITLE ==='') this.sf.value.XTITLE= ' ';

    if(this.sf.value.XCONTENT === undefined || this.sf.value.XCONTENT ==='') this.sf.value.XCONTENT = ' ';

    this.httpService.lbservice('XXGL_XXFS',{MESSAGE: this.sf.value}).then(res => {
      if(res.code > 0 ) {
        this.msgSrv.success('消息新建成功');
        this.xxfsFlag = false;
        this.voted.emit(this.xxfsFlag);
      };
    })
  }

  cancel(){
    this.isTsVisible = false;
  }

  close(){
    this.xxfsFlag = false;
    this.voted.emit(this.xxfsFlag);
  }
}
