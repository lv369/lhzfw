import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {XtglXxfsComponent} from './xxfs/xxfs.component';
import { TyjbXxtsComponent } from './xxts/xxts.component';
import { TyjbXxxqComponent } from './xxxq/xxxq.component';
import { XxglXxfswhComponent } from './xxfswh/xxfswh.component';
import { XxglCesshiComponent } from './cesshi/cesshi.component';
import { XxglZxtjComponent } from './zxtj/zxtj.component';
import { XxglBmtjComponent } from './bmtj/bmtj.component';
import { XxglRytjComponent } from './rytj/rytj.component';

const routes: Routes = [
  {path:'xxfs',component:XtglXxfsComponent},
  { path: 'xxts', component: TyjbXxtsComponent },
  { path: 'xxxq', component: TyjbXxxqComponent }

,
  { path: 'xxfswh', component: XxglXxfswhComponent },
  { path: 'cesshi', component: XxglCesshiComponent },
  { path: 'zxtj', component: XxglZxtjComponent },
  { path: 'bmtj', component: XxglBmtjComponent },
  { path: 'rytj', component: XxglRytjComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class XxglRoutingModule { }
