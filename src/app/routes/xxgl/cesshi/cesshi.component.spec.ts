import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { XxglCesshiComponent } from './cesshi.component';

describe('XxglCesshiComponent', () => {
  let component: XxglCesshiComponent;
  let fixture: ComponentFixture<XxglCesshiComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ XxglCesshiComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(XxglCesshiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
