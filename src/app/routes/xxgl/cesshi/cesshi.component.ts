import { Component, OnInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import { _HttpClient, ModalHelper } from '@delon/theme';
import { STColumn, STComponent } from '@delon/abc/table';
import { SFSchema } from '@delon/form';
import { Chart } from '@antv/g2';
import { G2PieComponent } from '@delon/chart';

@Component({
  selector: 'app-xxgl-cesshi',
  templateUrl: './cesshi.component.html',
})
export class XxglCesshiComponent implements OnInit {
   data :any={};

  // @ViewChild('st', { static: false }) st: STComponent;
  // columns: STColumn[] = [
  //   { title: '编号', index: 'no' },
  //   { title: '调用次数', type: 'number', index: 'callNo' },
  //   { title: '头像', type: 'img', width: '50px', index: 'avatar' },
  //   { title: '时间', type: 'date', index: 'updatedAt' },
  //   {
  //     title: '',
  //     buttons: [
  //       // { text: '查看', click: (item: any) => `/form/${item.id}` },
  //       // { text: '编辑', type: 'static', component: FormEditComponent, click: 'reload' },
  //     ]
  //   }
  // ];


  @ViewChild('pie',{static:false}) pie: G2PieComponent;

  constructor(private http: _HttpClient, private modal: ModalHelper,private cdr: ChangeDetectorRef,) { }


  
  


  ngOnInit() {
     this.initUserInfo();
   }

   initUserInfo(){
   
     const data1 = [
      { year: '1951 年', sales: 38 },
      { year: '1952 年', sales: 52 },
      { year: '1956 年', sales: 61 },
      { year: '1957 年', sales: 145 },
      { year: '1958 年', sales: 48 },
      { year: '1959 年', sales: 38 },
      { year: '1960 年', sales: 38 },
      { year: '1962 年', sales: 38 },
    ];

    this.data= data1;
    this.pie.colors = ['#5ad8a6','#1890ff'];

        this.cdr.markForCheck();
      

  }







}
