import { NgModule } from '@angular/core';
import { SharedModule } from '@shared';
import { XxglRoutingModule } from './xxgl-routing.module';
import {XtglXxfsComponent} from './xxfs/xxfs.component';
import { TyjbXxtsComponent } from './xxts/xxts.component';
import { TyjbXxxqComponent } from './xxxq/xxxq.component';
import { XxglXxfswhComponent } from './xxfswh/xxfswh.component';
import { XxglCesshiComponent } from './cesshi/cesshi.component';
import { XxglZxtjComponent } from './zxtj/zxtj.component';
import { XxglBmtjComponent } from './bmtj/bmtj.component';
import { XxglRytjComponent } from './rytj/rytj.component';

const COMPONENTS = [
  XtglXxfsComponent,
  TyjbXxtsComponent,
  TyjbXxxqComponent,
  XxglXxfswhComponent,
  XxglCesshiComponent,
  XxglZxtjComponent,
  XxglBmtjComponent,
  XxglRytjComponent];
const COMPONENTS_NOROUNT = [];

@NgModule({
  imports: [
    SharedModule,
    XxglRoutingModule
  ],
  exports: [
    XtglXxfsComponent,
  TyjbXxtsComponent,
  TyjbXxxqComponent,
  XxglXxfswhComponent
  ],
  declarations: [
    ...COMPONENTS,
    ...COMPONENTS_NOROUNT
  ],
  entryComponents: COMPONENTS_NOROUNT
})
export class XxglModule { }
