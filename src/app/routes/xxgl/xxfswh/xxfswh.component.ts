import { Component, OnInit, ViewChild } from '@angular/core';
import { _HttpClient } from '@delon/theme';
import { SFSchema, SFComponent, SFDateWidgetSchema } from '@delon/form';
import { STColumn,ReuseTabService } from '@delon/abc';
import { GridComponent,HttpService,SupDic } from 'lbf';
import {Router} from '@angular/router';
import {NzMessageService } from 'ng-zorro-antd';
import { element } from 'protractor';


@Component({
  selector: 'app-xxgl-xxfswh',
  templateUrl: './xxfswh.component.html',
  styleUrls: ['./xxfswh.less']
})
export class XxglXxfswhComponent implements OnInit {

  param = {sname: 'XXGL_XXFSCX',queryparas: {}}
  yhparams = {sname: 'xxgl_yhcx',queryparas: {}}
  yfsparams = {sname: 'XXGL_FSRYCX',queryparas: {}}
  styleParam: { height: string; overflow: string; }; // 消息发送人员选择左侧界面滚动显示

  searchValue = ''
  nodes = []

  schema:SFSchema = {
    properties: {
      START: {
        type: 'string',
        title: '开始时间',
        ui: {
          widget: 'date'
        }
      },
      END: {
        type: 'string',
        title: '结束时间',
        ui: {
          widget: 'date'
        }
      },
      XTYPE: {
        type: 'string',
        title: '消息类别',
        enum: this.supdic.getSFDic('XTYPE'),
        ui: {
          widget: 'select',
          allowClear: true,
          width: 250
        }
      }
    }
  }

  /* ------------------------   消息发送  ------------------*/
  isVisible = false; 
  isXqVisible = false;
  isFsVisible =false;
  treeData:any;
  yhList = [] // 已选人员
  fsyhList = [] // 已发送人员
  yxflag= false; // 判断已选人员是否重复标志
  fsflag = false; // 判断发送人员是否重复
  xxInfo = {}; // 详细详情
  message; // 选择人员发送消息
  FSR;
  JSR;


  columns:STColumn[] = [
    {title: '消息类别',index: 'XTYPE',format: (record)=> this.supdic.getdicLabel('XTYPE',record.XTYPE)},
    {title: '标题',type:'link',index: 'XTITLE',click: (record) => this.xxDetail(record),
     format: (record) => record.XTITLE === ' '?'(无主题)':record.XTITLE},
     {title:'发送人数',index: 'COUNT'},
     {title: '已读人数',index: 'YDCOUNT'},
    {title: '时间',index: 'FSSJ'},
    {
      title: '操作区',
      buttons: [
        {
          text: '发送',
          click: (record) => this.send(record)
        },
        {
          text: '删除',
          click: (record) =>this.xxDel(record),
          pop: true,
          popTitle: '是否删除此条信息？'
        }
      ]
    }
  ]

  yhColumns: STColumn[] = [
    {title: '登录名',index: 'UNAME'},
    {title: '用户名',index: 'NAME'},
    {title: '手机号码',index: 'PHONE'},
    {
      title: '操作区',
      buttons: [
        {
          text: '添加',
          click: (record) => this.yhAdd(record)
        }
      ]
    }
  ]

  yfsColumns: STColumn[] = [
    {title: '接受状态',index: 'JSZT',type: 'tag',
    tag: {
      '1': { text: '已读', color: '#2db7f5' },
      '0': { text: '未读', color: 'orange'}
    }},
    {title: '登录名',index: 'UNAME'},
    {title: '用户名',index: 'NAME'},
    {title: '手机号码',index: 'PHONE'},
  ]

  @ViewChild('st',{static: false}) st: GridComponent;
  @ViewChild('fsst',{static: false}) fsst: GridComponent;
  @ViewChild('yfsst',{static: false}) yfsst: GridComponent;
  @ViewChild('sf',{static: false}) sf: SFComponent;
  constructor(private http: _HttpClient,private httpService: HttpService,
    private route: Router,private msgSrv: NzMessageService,
    private reuseService: ReuseTabService,private supdic: SupDic) { }

  ngOnInit() { 
    this.styleParam = {height: (window.innerHeight-175)+'px',overflow: 'auto'}
    this.initGwTree();
  }

  click(){
    this.isXqVisible = false;
    this.route.navigateByUrl(this.xxInfo['XCONTENT']);
  }

  xxDetail(record){
    this.st.reload();
    this.JSR = "";
    let tJsr = "";
    console.log(record);
    this.xxInfo = record;
    this.FSR = this.supdic.getdicLabel('USERID',this.xxInfo['FSR']);
    this.httpService.lbservice('XXGL_FSRYCX',{para:{XXID: record.XXID}}).then(res => {
      if(res.code > 0){
         const users = res.message.list;
         users.forEach(ele => {
         tJsr = tJsr + ele.label+"；";
        })
        this.JSR = tJsr;
        if(res.message.list.length > 0) this.xxInfo['JSZT'] = "投递成功"
        else this.xxInfo['JSZT'] = '未发送'
      }
    })
    this.isXqVisible = true;
    
  }

  // 信息发送
  send(record){
    this.st.reload();
    this.yfsst.reload({COLUMN: 'true',XXID:record.XXID});
    this.yhList = []
    this.fsyhList = []
    this.message = record;
    this.httpService.lbservice('XXGL_FSRYCX',{para:{XXID: record.XXID}}).then(res => {
      if(res.code >0 ){
        this.fsyhList = res.message.list;
      }
    })

    this.isFsVisible = true;
  }

  handleFsCancel(){
    this.isFsVisible = false;
  }

  saveAdd(){
    console.log(this.yhList)
    console.log(this.message);
    const uids = []
    this.yhList.forEach(ele => {
      uids.push(ele.value)
    })
    this.httpService.lbservice('XXGL_FSRY',{YHLIST: uids,MESSAGE: this.message}).then(res => {
      if(res.code >0){
        this.msgSrv.info('消息发送成功');
        this.st.reload();
      }
    })
    this.isFsVisible = false;
  }

  // 初始化岗位树
  initGwTree(){
    this.httpService.lbservice('xxgl_gwlist',{}).then(res=>{
      console.log(res);
      this.nodes = res.message.list;
    });
 }

  nzEvent(event){
    this.treeData = event.node.origin;
    console.log(this.treeData)
    if(this.treeData.hasOwnProperty('dgb010')){
      this.fsst.reload({DGB010: this.treeData.dgb010})
    }
    if(this.treeData.hasOwnProperty('dgb020')){
      this.fsst.reload({DGB020: this.treeData.dgb020})
    }
    if(this.treeData.hasOwnProperty('dgb040')){
      this.fsst.reload({DGB040: this.treeData.dgb040})
    }
    if(this.treeData.hasOwnProperty('roleid')){
      this.fsst.reload({ROLEID: this.treeData.roleid})
    }
  }
 

  yhAdd(record){
     // 判断人员是否重复标志
     this.yxflag = false;
     this.fsflag = false;
    console.log(record)
    const yh = {label: record.NAME,value: record.USERID};

    // 判断人员是否已被发送
    if(this.fsyhList.length !== 0){
      this.fsyhList.forEach(element => {
        if(element.value === yh.value){
          this.fsflag = true
        }
      })
    }


    if(this.fsflag){
      this.msgSrv.info(yh.label+'已发送')
    }else{
      // 判断人员是否已被选择
      if(this.yhList.length === 0 ) {
        this.yhList.push(yh);
      }else{
        
        this.yhList.forEach(element => {
          if(element.value === yh.value){
            this.yxflag = true;
          }
        });
  
        if(this.yxflag === false){
          this.yhList.push(yh);
        }else{
          this.msgSrv.info(yh.label+'已被添加')
        }
      }  
    }
    console.log(this.yhList)
  }

  handleClose(item){
    console.log(item)
    this.yhList = this.yhList.filter(data => data.value!==item.value)
    console.log(this.yhList)
  }

  // 信息删除
  xxDel(record){
    this.httpService.lbservice('XXGL_XXFSSC',{XXID: record.XXID}).then(res =>{
      if(res.code >0){
        this.msgSrv.success('删除成功');
        this.st.pi = 1;
        this.st.reload({}); 
      }  
      else this.msgSrv.error(res.error);
    } )
  }

  add(){
    this.isVisible = true
  }

  handleCancel(){
    this.isVisible = false;
  }

  handleXqCancel(){
    this.isXqVisible = false;
  }

  query(){
   this.st.reload(this.sf.value);
  }

  onVoted(xxfsFlag: boolean){
    this.isVisible = xxfsFlag;
    this.st.reload({})
  }

  _onReuseInit(){
    this.st.reload({})
  }
}
