import { Component, OnInit, ViewChild } from '@angular/core';
import { _HttpClient, ModalHelper } from '@delon/theme';
import { STColumn, STComponent } from '@delon/abc';
import { GridComponent, PopdetailComponent, HttpService, SupDic } from 'lbf';
import { SFSchema, SFComponent } from '@delon/form';
import { NzMessageService } from 'ng-zorro-antd';

@Component({
  selector: 'app-xtgn-clsy',
  templateUrl: './clsy.component.html',
  styleUrls: ['./clsy.component.less']
})
export class XtgnClsyComponent implements OnInit {

  params = {sname: 'JCPM_clsypm',form: {jsny: {SYEAR: new Date().getFullYear()+"",SMONTH: "0"+(new Date().getMonth()+1)}}}

  searchSchema: SFSchema = {
    properties: {
      jsny: {
        type: 'string',
        title: '结算年月',
        format: 'month',
        default: new Date().getFullYear()+"-0"+(new Date().getMonth()+1)
      },
      AKB001: {
        type: 'string',
        title: '机构编号'
      },
      AKC052: {
        type: 'string',
        title: '医院材料名称'
      },
      AKC053: {
        type: 'string',
        title: '中心编码'
      }
    }
  }

  columns: STColumn[] = [
    { title: '机构编号',index: 'AKB001'},
    { title: '机构名称',index: 'AKB002'},
    { title: '所属医共体',index: 'AKE500',dic: 'AKE500'},
    { title: '材料编码',index: 'AKC053'},
    { title: '医院材料名称',index: 'AKC052'},
    { title: '中心材料名称',index: 'AKE036'},
    { title: '数量',index: 'AKC056',type: 'number'},
    { title: '最大单价',index: 'AKC055',type: 'currency'},
    { title: '金额',index: 'AKC046',type: 'currency'},
    { title: '可报金额',index: 'AKC048',type: 'currency'},
  ]

  @ViewChild('st', { static: false }) st: GridComponent;
  @ViewChild('sf', { static: false }) sf: SFComponent;

  constructor(private modal: ModalHelper,
    public msgSrv: NzMessageService,
    private lbservice: HttpService,
    private supdic: SupDic) { }

  ngOnInit() { }

  search(){
    console.log(this.sf.value)
    if(this.sf.value.hasOwnProperty('jsny') && this.sf.value.jsny !== ""){
      const time = {SYEAR: this.sf.value.jsny.split("-")[0],SMONTH: this.sf.value.jsny.split("-")[1]}
      this.sf.value.jsny = time;
      console.log(this.sf.value)
    }
    this.st.reload(this.sf.value)
  }

  reset(){
    this.st.reload(this.params.form);
    this.sf.refreshSchema();
  }

}
