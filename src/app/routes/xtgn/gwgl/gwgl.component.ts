import { Component, OnInit, ViewChild} from '@angular/core';
import { _HttpClient, ModalHelper } from '@delon/theme';
import { STColumn, STComponent, STColumnButton } from '@delon/abc';
import { SFSchema, SFComponent } from '@delon/form';
import { GridComponent, HttpService, SupDic} from 'lbf';
import { NzMessageService, NzButtonComponent, NzDropdownMenuComponent,NzContextMenuService, NzFormatEmitEvent, NzTreeComponent} from 'ng-zorro-antd';

@Component({
  selector: 'app-xtgn-gwgl',
  templateUrl: './gwgl.component.html',
  styleUrls: ['./gwgl.less']
})
export class XtgnGwglComponent implements OnInit {
  searchValue = ''
  searchSxValue = ''
  nodes:any = [];
  cdnodes:any = [];
  sxnodes: any=[];
  treeData:any; // 当前所选节点
  yjData: any; // 右击所选节点
  fzData: any;// 复制所选节点
  cdtreeData = [];
  sxtreeData= [];
  dgb040: any;
  dataDGB041 = '';
  style ={height: (window.outerHeight-330)+'px',overflow: 'auto'}

   // 功能、业务复制
   fzFlag = 0;
   gndisabled = true;
   ywdisabled = true;
   bothdisabled = true;
   copydisabled = false;

  isSaveDisabled = true;
  isAddDisabled = true;
  isDelDisabled = true;
  isXtDisabled = true;
  isYwDisabled = true;
  isAddUserDisabled = true;
  formData ={};
  isVisible = false;
  isCdVisible = false;
  isYwVisible = false;
  modalTitle = '创建下属岗位';
  modalCdTitle = '设置系统权限';
  modalYwTitle = '设置业务权限';
  params = {sname: 'gwgl_yhcx',queryparas: {}}
  isYhVisible = false;

  editSchema: SFSchema = {
    properties: {
      ROLEID: {
        type: 'string',
        title: '岗位编码',
        readOnly: true,
        
      },
      DGB040: {
        type: 'string',
        title: '所属部门',
        ui: {
          hidden: true
        },
      },
      KS: {
        type: 'string',
        title: '所属部门',
        readOnly: true
      },
      RNAME: {
        type: 'string',
        title: '岗位名称',
      },
      RTYPE:{
        type:'string',
        title:'岗位类型',
        enum: this.supdic.getSFDic('RTYPE'),
        ui: {
          widget: 'select',
          allowClear: true
        }
      }
    },
   ui: {
     spanLabelFixed: 100,
     grid: {
       span: 12
     }
   },
   required: ["RNAME","RTYPE"]
  }

  newYhSchema: SFSchema = {
    properties: {
      UNAME: {
        type: 'string',
        title: '登录名'
      },
      NAME: {
        type: 'string',
        title: '用户名'
      },
      PHONE: {
        type: 'string',
        title: '手机号码',
        format:'mobile'
      },
      ROLEID: {
        type: 'string',
        title: '岗位',
        ui:{
          hidden:true
        }
      }
    },
    required: ['UNAME','NAME','PHONE'],
    ui: {
      spanLabelFixed:80,
      grid:{
        gutter:20,
        span:24
      }
    }
  }

  addSchema: SFSchema = {
    properties: {
      DGB040: {
        type: 'string',
        title: '所属部门',        
        ui: {
          hidden: true
        }
      },
      KS: {
        type: 'string',
        title: '所属部门',
        readOnly: true
      },
      RNAME: {
        type: 'string',
        title: '岗位名称'
      },
      RTYPE:{
        type:'string',
        title:'岗位类型',
        enum: this.supdic.getSFDic('RTYPE'),
        ui: {
          widget: 'select',
          allowClear: true
        }
      }
    },
    required:["DGB040","RNAME","RTYPE"],
    ui:{
      spanLabelFixed:80,
      grid:{
        gutter:20,
        span:24
      }
    }
  }

  yhColumns: STColumn[] = [
    
    {title: '登录名',index: 'UNAME'},
    {title: '用户名',index: 'NAME'},
    {title: '手机号码',index: 'PHONE'},
    {title: '状态',index: 'STATUS',type: 'yn',yn:{truth:'1'}}
  ]
  
  @ViewChild('sf',{static:false}) sf: SFComponent;
  @ViewChild('addSf',{static:false}) addSf: SFComponent;
  @ViewChild('st',{static:false}) st: GridComponent;
  @ViewChild('yhsf', { static: false }) yhsf: SFComponent;
  @ViewChild('tree',{static:false}) tree:NzTreeComponent;
  @ViewChild('cdtree',{static:false}) cdtree:NzTreeComponent;
  
  constructor(private lbservice: HttpService,
    private msgSrv: NzMessageService ,
    private supdic: SupDic,private nzContextMenuService: NzContextMenuService
    ) { }



  ngOnInit() { 
    this.initGwTree();
  }

  // 初始化岗位树
  initGwTree(){
    this.lbservice.lbservice('gwgl_gwlist',{}).then(res=>{


      this.nodes = res.message.list;


      console.log('**********************');
console.log( this.nodes );
console.log('*******************');
    });
 }

 initCdTree(){
  this.lbservice.lbservice('gwgl_cdlist',{ROLEID: this.treeData.roleid}).then(res=>{
    console.log(res.message.list);
    this.cdnodes = res.message.list;
  });
}

addYh(){
  this.isYhVisible = true;
}

handleYhOk(){

  const phone = this.yhsf.value.PHONE;
  this.isYhVisible = false;
  
  if((/^1[3456789]\d{9}$/.test(phone))){
    this.lbservice.lbservice("yhgl_yhxz",{para: this.yhsf.value}).then(result => {
      if (result.code < 1) {
        this.msgSrv.error(result.errmsg);
    }else{
        this.msgSrv.success("用户新增成功");
        this.st.reload({ROLEID: this.params.queryparas});
    }
   })
  }else{
    this.msgSrv.error('新增失败，电话格式错误');
  }
 
}

handleYhCancel(){
  this.isYhVisible = false;
}

initSxTree(){
  this.lbservice.lbservice('gwgl_sxlist',{ROLEID: this.treeData.roleid}).then(res=>{
    console.log(res.message.list);
    this.sxnodes = res.message.list;
  });
}
  nzEvent(event){
    this.cdtreeData= []
    this.sxtreeData = []
    this.isSaveDisabled = true;
    this.isDelDisabled = true;
    this.isAddDisabled = true;
    this.isAddUserDisabled = true;
    this.isXtDisabled = true;
    this.isYwDisabled = true;
    this.sf.refreshSchema();
    this.treeData = event.node.origin;


    if(this.treeData.hasOwnProperty("roleid")){
      // 编辑框表单赋值
      this.yhsf.setValue('/ROLEID',this.treeData.roleid);
      if(this.treeData.roleid !== '0' && this.treeData.roleid !== '1'){
        this.dataDGB041 = event.node.parentNode.origin.dgb041;
        this.dgb040 = event.node.parentNode.origin.dgb040;
        this.sf.setValue('/DGB040',this.dgb040);
        this.sf.setValue('/KS',this.dataDGB041);
        this.sf.setValue('/RNAME',this.treeData.rname);
        this.sf.setValue('/ROLEID',this.treeData.roleid);
        this.sf.setValue('/RTYPE',this.treeData.rtype);
       
        this.isSaveDisabled = true;
        this.isXtDisabled = false;
        this.isYwDisabled = false;
        this.isDelDisabled = false;
      }   

      if(this.treeData.roleid === '1'){
        this.isXtDisabled = false;
        this.isYwDisabled = false;
      }
      
      this.params.queryparas =this.treeData.roleid;
      console.log(this.params.queryparas)
      this.st.reload({ROLEID: this.params.queryparas});
        this.isAddUserDisabled = false;

    }else{ 

      this.st.reload({});
    }

    if(this.treeData.hasOwnProperty("dgb040")){
      this.isAddDisabled = false;
      this.dgb040 = event.node.origin.dgb040;
      this.dataDGB041 = event.node.origin.dgb041;
    }
}

nzCdEvent(event){
  this.cdtreeData = event.keys;
}

nzSxEvent(event){
   console.log(event)
  // const testArr = [1,2,3,4,5];
  //    const start = testArr.indexOf(2);
  //   testArr.splice(start,1);
  //   console.log(testArr)
  if(event.eventName === 'check'){
  const node = event.node.origin;
  const elem ={AYW001: node.AYW001,AYW025: node.key} 
  if(node.checked === true){
    this.sxtreeData.push(elem);
    console.log(this.sxtreeData)
  }
  if(node.checked === false){
   this.sxtreeData =  this.deleteItem(this.sxtreeData,elem);
   console.log(this.sxtreeData);
  }
  }
}

deleteItem(array,item) {
    if(typeof(item.AYW025)==='string'){
      console.log(array);
      console.log(item);
      const index = 
      array.findIndex(text => text.AYW001 === item.AYW001 && text.AYW025 === item.AYW025);
      console.log(index);
      if(index !== -1){
        array.splice(index, 1);
      }
      return array;
    }else{
      return array.filter(ele => ele.AYW001 !== item.AYW001);
    }
  } 

  formChanged(event){
    this.isSaveDisabled = false;  
   
}

  add(){
    this.isVisible = true;
    this.addSf.setValue('/DGB040',this.dgb040);
    this.addSf.setValue('/KS',this.dataDGB041);

    console.log(this.dataDGB041)
  }

  addXt(){
    this.initCdTree();
    this.lbservice.lbservice('gwgl_initCdData',{ROLEID: this.treeData.roleid}).then(res =>{
      res.message.list.forEach((item,index,arr)=> {
        const fid = item.fid;
        this.cdtreeData.push(fid);
    })
    })
    console.log(this.cdtreeData)
    this.isCdVisible = true
  }

  addYw(){
    this.initSxTree();
    this.lbservice.lbservice('gwgl_initSxData',{ROLEID: this.treeData.roleid}).then(res =>{
      this.sxtreeData = res.message.list;
    })
    this.isYwVisible = true;
  }
  save(){
    if(!this.sf.valid)
    {
        this.msgSrv.error('内容校验不通过，请确认必输项是否填写（或格式是否正确）！');
        return;
    }
    this.lbservice.lbservice('gwgl_gwxg',{para:this.sf.value}).then(resdata=>{
      console.log(this.sf.value);
       if(resdata.code<1)
       {
         this.msgSrv.error(resdata.errmsg);
       }
       else
       {
        this.msgSrv.success('修改成功');
        this.isSaveDisabled = true;
        this.initGwTree();
       }
       
    })
    
  }

  del(){
    this.lbservice.lbservice("gwgl_gwsc",{ROLEID: this.sf.value.ROLEID}).then(result => {
      if (result.code < 1) {
        this.msgSrv.error(result.errmsg);
        
       }else{
        this.msgSrv.success("删除成功");
        this.initGwTree();
        this.sf.reset();
      }
      
    })
  }

  handleCancel(){
    this.isVisible = false;
    this.isCdVisible = false;
    this.isYwVisible = false;
    this.formData = {};
  }

  handleOkCd(){
    let data = []
    console.log(this.cdtree.getTreeNodes())
  this.cdtree.getTreeNodes()[0].children.forEach(ele1=> {
    ele1.children.forEach(ele2 => {
      let item = ele2.origin.key;
      if(ele2.isChecked){
        data.push(item)
      }
    })
  })
    this.lbservice.lbservice('gwgl_xtqx',{FIDS: data,ROLEID: this.treeData.roleid}).then(result =>{
      if (result.code < 1) {
        this.msgSrv.error(result.errmsg);
      }else{
        this.msgSrv.success("设置系统权限成功");
      }
    })
    this.isCdVisible = false;
  }

  handleOkYw(){
    let data = []
    console.log(this.tree.getTreeNodes())
  this.tree.getTreeNodes().forEach(ele1=> {
    ele1.children.forEach(ele2 => {
      let item = {AYW001: ele2.origin['AYW001'],AYW025: ele2.origin['key']};
      if(ele2.isChecked){
        data.push(item)
      }
    })
  })
    console.log(this.sxtreeData)
    this.lbservice.lbservice('gwgl_ywqx',{SXLIST: data,ROLEID: this.treeData.roleid}).then(result =>{
      if (result.code < 1) {  
        this.msgSrv.error(result.errmsg);
      }else{
        this.msgSrv.success("设置业务权限成功");
      }
    })
    this.isYwVisible = false;
  }
    

  saveAdd(data){
    this.lbservice.lbservice("gwgl_gwxz",{para: data}).then(result => {
      if (result.code < 1) {
        this.msgSrv.error(result.errmsg);
   }else{
        this.msgSrv.success("新增成功");
        this.initGwTree();
   }
   this.isVisible = false;
    })
  }

  confirm(){
    this.del();
  }

  cancel(){

  }

  _onReuseInit() {
    this.initGwTree();
  }

  /*----------------- 复制操作  ------------------*/
  contextMenu(event: NzFormatEmitEvent, menu: NzDropdownMenuComponent): void {
     
    console.log(event)
    this.yjData = event.node.origin;
     if(this.treeData.key === this.yjData.key){
      this.nzContextMenuService.create(event.event, menu);
    }
    
  }

  cdfz(){
    this.fzFlag = 1;
    // 右击角色和粘贴角色一致 允许粘贴
      // 粘贴角色和复制角色一致
      if(this.treeData.key === this.fzData.key){
        this.msgSrv.error('功能权限已存在')
      }else{
        this.lbservice.lbservice('gwgl_qxfz',{FZFLAG: this.fzFlag,
          ROLEID: this.fzData.key,TROLEID: this.treeData.key}).then(res =>{
            if(res.code >0 ) {
              this.msgSrv.info('功能权限粘贴成功')
            }
          })
      }
    
  }

  ywfz(){
    this.fzFlag = 2;
      if(this.treeData.key === this.fzData.key){
        this.msgSrv.error('业务权限已存在')
      }else{
        this.lbservice.lbservice('gwgl_qxfz',{FZFLAG: this.fzFlag,
          ROLEID: this.fzData.key,TROLEID: this.treeData.key}).then(res =>{
            if(res.code >0 ) {
              this.msgSrv.info('业务权限粘贴成功')
            }
          })
      }
  }

  bothfz(){
    this.fzFlag = 3;
      if(this.treeData.key === this.fzData.key){
        this.msgSrv.error('功能、业务权限已存在')
      }else{
        this.lbservice.lbservice('gwgl_qxfz',{FZFLAG: this.fzFlag,
          ROLEID: this.fzData.key,TROLEID: this.treeData.key}).then(res =>{
            if(res.code >0 ) {
              this.msgSrv.info('功能、业务权限粘贴成功')
            }
          })
      }
  }

  copy(){
      this.gndisabled = false;
      this.ywdisabled = false;
      this.bothdisabled = false;
      this.fzData = this.yjData;
      this.msgSrv.info('复制成功');
  }
 
}
