import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { XtgnJzmxcxComponent } from './jzmxcx/jzmxcx.component';
import { XtgnClsyComponent } from './clsy/clsy.component';
import { XtgnJgglComponent } from './jggl/jggl.component';
import { XtgnSdglComponent } from './sdgl/sdgl.component';
import { XtgnKsglComponent } from './ksgl/ksgl.component';
import { XtgnGwglComponent } from './gwgl/gwgl.component';
import { XtgnYhglComponent } from './yhgl/yhgl.component';
import { XtgnTysjglComponent } from './tysjgl/tysjgl.component';
import { LbglComponent } from './lbgl/lbgl.component';

const routes: Routes = [

  { path: 'jzmxcx', component: XtgnJzmxcxComponent },
  { path: 'clsy', component: XtgnClsyComponent },
 { path: 'jggl', component: XtgnJgglComponent },
 { path: 'sdgl', component: XtgnSdglComponent },
  { path: 'ksgl', component: XtgnKsglComponent },
 { path: 'gwgl', component: XtgnGwglComponent },
 { path: 'yhgl', component: XtgnYhglComponent },
  { path: 'tysjgl', component: XtgnTysjglComponent },
  { path: 'lbgl', component: LbglComponent },];
 
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class XtgnRoutingModule { }
