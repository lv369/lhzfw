import { Component, OnInit, ViewChild } from '@angular/core';
import { _HttpClient, ModalHelper } from '@delon/theme';
import { STColumn } from '@delon/abc';
import { SFSchema, SFComponent } from '@delon/form';
import { GridComponent, HttpService, SupDic } from 'lbf';
import { NzMessageService, NzButtonComponent, NzFormatEmitEvent } from 'ng-zorro-antd';


@Component({
  selector: 'app-lbgl',
  templateUrl: './lbgl.component.html',
  styleUrls: ['./lbgl.component.less']
})
export class LbglComponent implements OnInit {
  searchValue = ''
  searchSxValue = ''
  nodes: any = [];
  cdnodes: any = [];
  sxnodes: any = [];
  treeData: any;
  cdtreeData: any;
  sxtreeData = [];
  dgb040: any;
  DAD086
  style = {height: (window.outerHeight-330)+'px',overflow: 'auto'}

  isSaveDisabled = true;
  isAddDisabled = true;
  isDelDisabled = true;
  isXtDisabled = true;
  isYwDisabled = true;
  isAddUserDisabled = true;
  formData = {};
  isVisible = false;
  isCdVisible = false;
  isYwVisible = false;



  sname = {
    label: 'LBGL_LABELQUERY',
    conditionAdd: 'LBGL_CONDITIONADD',
    labelAdd: 'LBGL_LABELADD',
    labelDel:'LBGL_LABELDEL',
    condtitionEdit:'LBGL_CONDITIONEDIT',
    condtitionDel:'LBGL_CONDITIONDEL'
  }
  isYhVisible = false;

  editSchema: SFSchema = {
    properties: {
      DAD086: {
        type: 'string',
        title: '条件ID',
        readOnly: true,

      },
      DAD087: {
        type: 'string',
        title: '条件名称',
      },
    },
    ui: {
      spanLabelFixed: 100,
      grid: {
        span: 12
      }
    },
    required: ["DAD087"]
  }

  newYhSchema: SFSchema = {
    properties: {
      DAD087: {
        type: 'string',
        title: '条件名称'
      },
    },
    required: ['DAD087'],
    ui: {
      spanLabelFixed: 80,
      grid: {
        gutter: 20,
        span: 24
      }
    }
  }

  addSchema: SFSchema = {
    properties: {
      DAD086: {
        type: 'string',
        title: '标签名称',
        ui: {
          hidden: true
        }
      },
      DAD100: {
        type: 'string',
        title: '标签名称'
      },
    },
    required: ["DAD100"],
    ui: {
      spanLabelFixed: 80,
      grid: {
        gutter: 20,
        span: 24
      }
    }
  }

  yhColumns: STColumn[] = [

    { title: '标签ID', index: 'DAD099' },
    { title: '标签名称', index: 'DAD100' },
    {
      title:'操作区',index:'operator',
      buttons:[

        { 
          text: '删除',
          icon: 'delete',
          click: (record: any) => this.del(record),
          pop: true,
          popTitle: '确认要将该表单项移除吗？',
        },
      ]
    }
  ]

  @ViewChild('sf', { static: false }) sf: SFComponent;
  @ViewChild('addSf', { static: false }) addSf: SFComponent;
  @ViewChild('st', { static: false }) st: GridComponent;
  @ViewChild('yhsf', { static: false }) yhsf: SFComponent;

  constructor(private lbservice: HttpService,
    private msgSrv: NzMessageService,
    private supdic: SupDic
  ) { }



  ngOnInit() {
    this.initGwTree();
  }

  // 初始化岗位树
  initGwTree() {
    this.lbservice.lbservice('LBGL_TREE', {}).then(res => {
      console.log(res);
      this.nodes = res.message.list;
    });
  }


  handleYhOk() {
    this.lbservice.lbservice(this.sname.conditionAdd, { para: this.yhsf.value }).then(result => {
      if (result.code < 1) {
        this.msgSrv.error(result.errmsg);
      } else {
        this.msgSrv.success("条件新增成功");
        this.initGwTree()
        this.isYhVisible = false;
      }
    })

  }

  handleYhCancel() {
    this.isYhVisible = false;
  }


  nzEvent(event) {
    this.sxtreeData = []


    this.sf.refreshSchema();
    this.treeData = event.node.origin;
    console.log(event)
    this.DAD086 = this.treeData.key
    this.sf.setValue('/DAD086', this.treeData.key)
    this.sf.setValue('/DAD087', this.treeData.title)
    this.st.reload({ DAD086: this.treeData.key })
  }





  add() {
    this.isVisible = true;
  }
  addYh() {
    this.isYhVisible = true;
  }



  save() {
    if (!this.sf.valid) {
      this.msgSrv.error('必填项不能为空！');
      return;
    }
    this.lbservice.lbservice(this.sname.condtitionEdit, { para: this.sf.value }).then(resdata => {
      console.log(this.sf.value);
      if (resdata.code < 1) {
        this.msgSrv.error(resdata.errmsg);
      }
      else {
        this.msgSrv.success('修改成功');
        this.isSaveDisabled = true;
        this.initGwTree();
      }

    })

  }

  del(record) {
    this.lbservice.lbservice(this.sname.labelDel, {para:{DAD099:record.DAD099}  }).then(result => {
      if (result.code < 1) {
        this.msgSrv.error(result.errmsg);
      } else {
        this.msgSrv.success("删除成功");
        this.st.reload({ DAD086: this.DAD086 })
      }
    })
  }

  handleCancel() {
    this.isVisible = false;
    this.isCdVisible = false;
    this.isYwVisible = false;
    this.formData = {};
  }



  saveAdd(data) {
    data = {
      ...data,
      DAD086: this.DAD086
    }
    this.lbservice.lbservice(this.sname.labelAdd, { para: data }).then(result => {
      if (result.code < 1) {
        this.msgSrv.error(result.errmsg);
      } else {
        this.msgSrv.success("新增成功");

          this.st.reload({ DAD086: this.DAD086 })
      }
      this.isVisible = false;
    })
  }

  confirm() {
    this.lbservice.lbservice(this.sname.condtitionDel, {para:{DAD086:this.DAD086}  }).then(result => {
      if (result.code < 1) {
        this.msgSrv.error(result.errmsg);
      } else {
        this.msgSrv.success("删除成功");
        this.sf.reset()
        this.DAD086 = null
        this.st.reload()
        this.initGwTree();
      }
    })
  }

  cancel() {

  }


}
