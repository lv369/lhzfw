import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { XtgnJzmxcxComponent } from './jzmxcx.component';

describe('XtgnJzmxcxComponent', () => {
  let component: XtgnJzmxcxComponent;
  let fixture: ComponentFixture<XtgnJzmxcxComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ XtgnJzmxcxComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(XtgnJzmxcxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
