import { Component, OnInit, ChangeDetectionStrategy, ViewChild, ElementRef, Input, AfterViewInit } from '@angular/core';


@Component({
  selector: 'cx-stack-pie',
  templateUrl: './cx-stack-pie.component.html',
  styleUrls: ['./cx-stack-pie.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CxStackPieComponent implements OnInit ,AfterViewInit{
  private chart: any;


  @Input() height = 500;
  @Input() data = [
    { value: 251, type: '大事例一', name: '子事例一' },
    { value: 1048, type: '大事例一', name: '子事例二' },
    { value: 610, type: '大事例二', name: '子事例三' },
    { value: 434, type: '大事例二', name: '子事例四' },
    { value: 335, type: '大事例三', name: '子事例五' },
    { value: 250, type: '大事例三', name: '子事例六' }
  ];
  @ViewChild('container', { static: false })  private node: ElementRef;
  constructor() {


  }

  render() {
    const { node } = this;
    const data =  this.data;
    const container = node.nativeElement as HTMLElement;
    const { DataView } = DataSet;

    const dv = new DataView();
    dv.source(data).transform({
      type: 'percent',
      field: 'value',
      dimension: 'type',
      as: 'percent'
    });
    const chart = (this.chart = new G2.Chart({
      container,
      forceFit: true,
      height: this.getHeight,
      padding: 0
    }))

    chart.source(dv, {
      percent: {
        formatter: val => {
          val = (val * 100).toFixed(2) + '%';
          return val;
        }
      }
    });
    chart.coord('theta', {
      radius: 0.5
    });
    chart.tooltip({
      showTitle: false
    });
    chart.legend(false);
    chart.intervalStack()
      .position('percent')
      .color('type')
      .label('type', {
        textStyle:
        { 
          rotate:-7,
         fontSize:20
          },
        offset:-30,
      })
      .tooltip('type*percent', (item, percent) => {
        percent = (percent * 100).toFixed(2) + '%';
        return {
          name: item,
          value: percent
        };
      })
      .select(false)
      .style({
        lineWidth: 1,
        stroke: '#fff'
      });

    const outterView = chart.view();
    const dv1 = new DataView();
    dv1.source(data).transform({
      type: 'percent',
      field: 'value',
      dimension: 'name',
      as: 'percent'
    });
    outterView.source(dv1, {
      percent: {
        formatter: val => {
          val = (val * 100).toFixed(2) + '%';
          return val;
        }
      }
    });
    outterView.coord('theta', {
      innerRadius: 0.5 / 0.75,
      radius: 0.75
    });
    outterView.intervalStack()
      .position('percent')
      .color('name', ['#BAE7FF', '#7FC9FE', '#71E3E3', '#ABF5F5', '#8EE0A1', '#BAF5C4'])
      .label('name')
      .tooltip('name*percent', (item, percent) => {
        percent = (percent * 100).toFixed(2) + '%';
        return {
          name: item,
          value: percent
        };
      })
      .select(false)
      .style({
        lineWidth: 1,
        stroke: '#fff'
      });

    chart.render();
  }
  ngOnInit() {
    //this.render();
  }
  ngAfterViewInit(): void {
    //Called after ngAfterContentInit when the component's view has been initialized. Applies to components only.
    //Add 'implements AfterViewInit' to the class.
    this.render();
  }


  public get getHeight(): number {
    return this.height;
  }

}
