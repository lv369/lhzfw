import { Component, OnInit, ViewChild, AfterContentInit, AfterContentChecked, AfterViewInit } from '@angular/core';
import { Router } from '@angular/router';
import { STColumn } from '@delon/abc';
import { SFComponent, SFSchema, SFTreeSelectWidgetSchema } from '@delon/form';
import { GridComponent, HttpService, SupDic } from 'lbf';
import { NzMessageService } from 'ng-zorro-antd';
import { of } from 'rxjs';
import { delay } from 'rxjs/operators';

@Component({
  selector: 'app-xtgn-yhgl',
  templateUrl: './yhgl.component.html',
})
export class XtgnYhglComponent implements OnInit{
  
  isUpVisible = false;
  isGwVisible = false;
  nodes = [];
  gwnodes = [];
  userid : string ;
  gwlist: any; 
  modalGwTitle = '设置岗位';
  params = {sname: 'yhgl_yhss', queryparas: {}};
  modiFormData = {};
  dropdownStyle= {height: "300px",width: "250px"}

  searchSchema: SFSchema = {
    properties: {
      NAME: {
        type: 'string',
        title: '用户名',
        ui:{
          grid:{
            span:6
          }
        }
      },
      DGB010: {
        type: 'string',
        title: '部门',
        enum: this.supdic.getSFDic('DGB010'),
        ui: {
          widget: 'select',
          allowClear: true,
          width:130,
          grid:{
            span:6
          }
        }
      },
      ROLEID: {
        type: 'string',
        title: '岗位',
        ui: {
          widget: 'tree-select',
          checkable: true,
          multiple: true,
          defaultExpandAll:true,
          showLine:true,
          dropdownStyle:{
            height:'400px',
          },
          width: 550,
          grid:{
            span:12
          }
        }
      }
    },
    ui:{
      grid:{
        span:6
      }
    }
  };
 

  

  updateYhSchema: SFSchema = {
    properties: {
      USERID: {
        type: 'string',
        title: '用户ID',
        ui :{
          hidden: true
        }
      },
      UNAME: {
        type: 'string',
        title: '登录名'
      },
      NAME: {
        type: 'string',
        title: '用户名'
      },
      PHONE: {
        type: 'string',
        title: '手机号码'
      }
    },
    required: ['UNAME','NAME','PHONE'],
    ui: {
      spanLabelFixed:80,
      grid:{
        gutter:20,
        span:24
      }
    }
  }

  @ViewChild('st', { static: false }) st: GridComponent;
  @ViewChild('sf', { static: false }) sf: SFComponent;
  
  @ViewChild('yhUpsf', { static: false }) yhUpsf: SFComponent;
  yhColumns: STColumn[] = [
    {title: '状态',index: 'STATUS',render: 'custom'},
    {title: '登录名',index: 'UNAME'},
    {title: '用户名',index: 'NAME'},
    {title: '手机号码',index: 'PHONE'},
    {title: '开始时间',index: 'SDATE',type: 'date'},
    {
      title: '岗位',
      buttons :[
        {
          text: '岗位设置',
          click: (record) => this.gwsz(record)
        }
      ]
    },
    {
      title: '操作区',
      buttons: [
        {
          text: '修改',
          click: (record) => this.update(record)
        },
        {
          text: '删除',
          pop: true,
          popTitle: '是否确认删除?',
          click: (record) => this.del(record)
        }
      ]
    }
  ]

  constructor(private lbservice: HttpService,
    private supdic :SupDic,
    private msgSrv: NzMessageService,
    private route: Router
    ) { }


   initGw(){
    this.searchSchema = {
      properties: {
        NAME: {
          type: 'string',
          title: '用户名',
          ui:{
            grid:{
              span:6
            }
          }
        },
        DGB010: {
          type: 'string',
          title: '部门',
          enum: this.supdic.getSFDic('DGB040'),
          ui: {
            widget: 'select',
            allowClear: true,
            width:150,
            grid:{
              span:6
            }
          }
        },
        ROLEID: {
          type: 'string',
          title: '岗位',
          enum: this.gwnodes,
          ui: {
            widget: 'tree-select',
            checkable: true,
            multiple: true,
            defaultExpandAll:true,
            showLine:true,
            allowClear: true,
            dropdownStyle:{
              height:'400px',
            },
            width: 550,
            grid:{
              span:12
            }
          }
        }
      },
      ui:{
        grid:{
          span:6
        }
      }
    };
   }

  ngOnInit() { 
    
    this.initGwDxTree();
    
    
  }



  query(){
    this.st.pi = 1;
    this.st.reload(this.sf.value);
    console.log('*******************');
    console.log(this.sf.value);
    console.log('**************');
    
  }

  changeState(index:any){
    const rec:any=this.st.data[index];
    if(rec.STATUS === '0'){
      rec.STATUS = '1'
    }else if(rec.STATUS === '1'){
      rec.STATUS = '0'
    }
     this.lbservice.lbservice('yhgl_yhztupdate',{'USERID':rec.USERID,'STATUS':rec.STATUS})
    .then(resdata=>{
      if (resdata.code < 1) {
        this.msgSrv.error(resdata.errmsg);
      }else{
        this.msgSrv.success("用户状态修改成功");
        this.st.data[index].STATUS = rec.STATUS;
        // this.st.reload();
      }
  });
 
  }

  update(record){
    
    this.modiFormData = record;
    
    // this.yhUpsf.formData = record;
    this.isUpVisible = true;
    console.log(record);
  }

  del(record) {
    this.lbservice.lbservice('yhgl_yhsc',{USERID: record.USERID}).then(result =>{
      if(result.code<1){
        this.msgSrv.error(result.errmsg);
      }else{
        this.msgSrv.success('删除成功');
      }
    })
    this.st.reload();
  }

  gwsz(record){
    this.userid = record.USERID;
    this.initGwDxTree();
    
    this.isGwVisible = true;
    // this.sf.refreshSchema();
  }
  
  initGwDxTree(){
    this.lbservice.lbservice('gwgl_gwlist',{para: {YH: '1',USERID: this.userid}}).then(result =>{
      if(result.code<1){
        this.msgSrv.error(result.errmsg)
      }else{
        this.gwnodes = result.message.list;
        this.initGw();
      }
    })
  }

  nzEvent(data){
    this.gwlist = data.keys;
  }

  handleOkGw(){
    this.lbservice.lbservice('yhgl_szgw',{GWLIST: this.gwlist,USERID: this.userid}).then(result =>{
      if (result.code < 1) {
        this.msgSrv.error(result.errmsg);
      }else{
        this.msgSrv.success("设置岗位成功");
        this.isGwVisible = false;
      }
    })
    
  }

  handleCancel(){
    
    this.isUpVisible = false;
    this.isGwVisible = false;
  }
  
  

  saveUp(data){
    this.lbservice.lbservice('yhgl_yhxg',{para: data}).then(result =>{
      if(result.code<1){
        this.msgSrv.error('修改失败');
      }else{
        this.msgSrv.success('修改成功');
        this.st.st.reload();
        this.isUpVisible = false;
      }
    })
    
  }

  onChange(event){
    console.log(event)
    this.sf.value.ROLEID = event;
  }

}
