import { Component, OnInit, ViewChild} from '@angular/core';
import { _HttpClient, ModalHelper } from '@delon/theme';
import { STColumn, STComponent, STColumnButton } from '@delon/abc';
import { SFSchema, SFComponent, SFTreeSelectWidgetSchema, SFSchemaEnumType, SFTextareaWidgetSchema } from '@delon/form';
import { GridComponent, HttpService, SupDic} from 'lbf';
import { NzMessageService, NzFormatEmitEvent, NzTreeNode} from 'ng-zorro-antd';
import { of } from 'rxjs';
import { delay } from 'rxjs/operators';
import {Router} from '@angular/router';

@Component({
  selector: 'app-xtgn-jggl',
  templateUrl: './jggl.component.html',
  styleUrls: ['./jggl.less']
})
export class XtgnJgglComponent implements OnInit {
  searchValue = ''
  deptid: any;
  nodes:any = [];
  enums:SFSchemaEnumType[];
  enums1:SFSchemaEnumType[];
  treeData:any;
  isSaveDisabled = true;
  isAddDisabled = true;
  isDelDisabled = true;
  formData ={};
  isVisible = false;
  isSdVisible = false;
  modalTitle = '新增机构';
  params = {sname: 'jggl_kscx',queryparas: {}}
  sdParam = {DGB030: '',DGB031: ''}
  style = {height: (window.outerHeight-330)+'px',overflow: 'auto'}
   
  editSchema: SFSchema = {
    properties: {
      DGB020: {
        type: 'string',
        title: '中心编码',
        readOnly: true,
        
      },
      /*
      DGB010: {
        type: 'string',
        title: '所属部门',
        readOnly: true,
        ui: {
          widget: 'select',
        },
        enum: this.supdic.getSFDic("DGB010")
      },
      */
     /*
      DGB030: {
        type: 'string',
        title: '属地编码',         
        ui: {
          hidden: true
        } 
      },
      
      DGB031: {
        type: 'string',
        title: '属地',
        readOnly: true
      },
      */
      DGB021: {
        type: 'string',
        title: '中心名称',
      },
      /*
      AAE005: {
        type: 'string',
        title: '联系方式',
        
      },
      AAE006: {
        type: 'string',
        title: '机构地址',
      }
      */
    },
   ui: {
     spanLabelFixed: 100,
     grid: {
       span: 12
     }
   },
   required: ["DGB021"/*,"AAE005","AAE006"*/]
  }

  addSchema: SFSchema = {
    properties: {
      DGB021: {
        type: 'string',
        title: '中心名称',
      },
      /*
      DGB010: {
        type: 'string',
        title: '所属部门',
        ui:{
         widget: 'select',
        },
        enum: this.supdic.getSFDic('DGB010')
      },      
      DGB030:{
        type: 'string',
        title: '属地',        
        enum: [{ title: '临海市', key: '331082000000' }],        
        ui:{
          widget:'tree-select',
          dropdownStyle: {
            height: '200px'
          },
          expandChange: (e: NzFormatEmitEvent)=>this.getSdTree(e)
        }
      },
      AAE005: {
        type: 'string',
        title: '联系方式',
        
      },
      
      AAE006: {
        type: 'string',
        title: '机构地址',        
      }*/
    },
    required:[ "DGB021" /*,"DGB030","DGB010","AAE005","AAE006"*/],
    ui:{
      spanLabelFixed:80,
      grid:{
        gutter:20,
        span:24
      }
    }
  }
  node:NzTreeNode;
  ksColumns: STColumn[] = [
    {title: '序号',index: 'no',type:'no'},
    {title: '部门名称',index: 'DGB041'},
    {title: '下属岗位数量',index: 'GWS',type:'number'},
    { title: '操作区', buttons:[
      { 
        text: '删除',
        icon: 'delete',
        click: (record: any) => this.delKs(record),
        pop: true,
        popTitle: '确认要将该科室删除吗？',
      },
    ]},
  ]
  
  @ViewChild('sf',{static:false}) sf: SFComponent;
  @ViewChild('addSf',{static:false}) addSf: SFComponent;
  @ViewChild('st',{static:false}) st: GridComponent;
  
  constructor(private lbservice: HttpService,
    private msgSrv: NzMessageService ,
    private supdic: SupDic,
    private route: Router
    ) { }



  ngOnInit() { 
    this.initJgTree();
    this.isSaveDisabled = true;
    this.isAddDisabled = false;
    console.log('jggl')
  }

  
  getSdTree(e: NzFormatEmitEvent){
    this.node = e.node;
    this.lbservice.lbservice('sd_selecttree',{key:e.node.origin.key}).then(resdata=>{
       if(resdata.code<1)
       {
         this.msgSrv.error(resdata.errmsg);
       }
       else
       {
        if(this.node.getChildren().length === 0 && this.node.isExpanded)
        {
          this.node.addChildren(resdata.message.list);
        }
        
        
       }
    })
  }

  // 初始化机构树
  initJgTree(){
    this.lbservice.lbservice('jggl_jglist',{}).then(res=>{
      console.log(res);
      this.nodes = res.message.list;
      // this.addSchema.properties.DGB030.enum = [{ title: res.DGB031 , key: res.DGB030 }];
    });
 }

 

  nzEvent(event){
    this.isAddDisabled = false;
    this.isSaveDisabled = true;
    this.isDelDisabled = true;
    // this.sf.refreshSchema();
    this.treeData = event.node.origin;
    // console.log(event)

    if(this.treeData.hasOwnProperty("dgb020")){
        this.sf.setValue('/DGB020', this.treeData.dgb020);
        this.sf.setValue('/DGB021', this.treeData.dgb021);
        /*
        this.sf.setValue('/DGB010', this.treeData.dgb010);
        this.sf.setValue('/DGB030', this.treeData.dgb030);
        this.sf.setValue('/DGB031', this.treeData.dgb031);
        
        this.sf.setValue('/AAE005', this.treeData.aae005);
        this.sf.setValue('/AAE006', this.treeData.aae006);
        */
        this.params.queryparas =  {DGB020: this.treeData.dgb020};

        // console.log(this.params.queryparas)
        this.st.reload(this.params.queryparas);
        this.isDelDisabled = false;
        
      }else{
        this.st.reload({});
      }
  }

  

  formChanged(event){
    this.isSaveDisabled = false;  
   
}

  add(){
    this.isVisible = true;
     this.addSf.refreshSchema();
  }

  save(){
    if(!this.sf.valid)
    {
        this.msgSrv.error('必填项不能为空！');
        return;
    }
    /*
    if(/^([0-9]{3,4}-)?[0-9]{7,8}$/.test(this.sf.value.AAE005) || /^1[3456789]\d{9}$/.test(this.sf.value.AAE005)){
     
    }else{
      this.msgSrv.error('电话格式错误');
      return;
    }
*/
    this.lbservice.lbservice('jggl_jgxg',{para:this.sf.value}).then(resdata=>{
      console.log(this.sf.value);
       if(resdata.code<1)
       {
         this.msgSrv.error(resdata.errmsg);
       }
       else
       {
        this.msgSrv.success('修改成功');
        this.isSaveDisabled = true;
        setTimeout(() => {
          this.initJgTree();
        }, 500);
       }
    })
    
  }

  delKs(record:any){
    this.lbservice.lbservice("ksgl_kssc",{DGB040: record.DGB040}).then(result => {
      if (result.code < 1) {
        this.msgSrv.error(result.errmsg);
       }else{
        this.st.reload(this.params.queryparas);
        this.msgSrv.success("删除成功");
      }
    })
  }

  del(){
    this.lbservice.lbservice("jggl_jgsc",{DGB020: this.sf.value.DGB020,DGB010: this.sf.value.DGB010}).then(result => {
      if (result.code < 1) {
        this.msgSrv.error(result.errmsg);
       }else{
        this.msgSrv.success("删除成功");
        this.sf.reset();
      }
      setTimeout(() => {
        this.initJgTree();
      }, 500);
      // this.initJgTree();
    })
  }

  handleCancel(){
    this.isVisible = false;
    this.formData = {};
  }

  handleSdCancel(){
    this.isSdVisible = false;
  }

  saveAdd(data){
   /*
    if(/^([0-9]{3,4}-)?[0-9]{7,8}$/.test(this.sf.value.AAE005) || /^1[3456789]\d{9}$/.test(this.sf.value.AAE005)){
     
    }else{
      this.msgSrv.error('电话格式错误');
      return;
    }
    */
    const DGB030 = this.sdParam.DGB030;
    data.custom = DGB030;
    this.lbservice.lbservice("jggl_jgxz",{para: data}).then(result => {
      if (result.code < 1) {
        this.msgSrv.error(result.errmsg);
   }else{
        this.msgSrv.success("新增成功");
        setTimeout(() => {
          this.initJgTree();
        }, 500);
       // this.initJgTree();
   }
   this.isVisible = false;
    })
  }

  click(){
      this.isSdVisible = true;
  }

  

  confirm(){
    this.del();
  }

  cancel(){

  }

  _onReuseInit() {
    console.log('_onReuseInit');
  }

}
