import { Component, OnInit, ViewChild} from '@angular/core';
import { _HttpClient, ModalHelper } from '@delon/theme';
import { STColumn, STComponent, STColumnButton } from '@delon/abc';
import { SFSchema, SFComponent } from '@delon/form';
import { GridComponent, HttpService, SupDic} from 'lbf';
import { NzMessageService} from 'ng-zorro-antd';

@Component({
  selector: 'app-xtgn-ksgl',
  templateUrl: './ksgl.component.html',
  styleUrls: ['./ksgl.less']
})
export class XtgnKsglComponent implements OnInit {
  searchValue = ''
  nodes:any = [];
  treeData:any;
  isSaveDisabled = true;
  isAddDisabled = true;
  isDelDisabled = true;
  formData ={};
  isVisible = false;
  modalTitle = '创建下属部门';
  params = {sname: 'ksgl_gwcx',queryparas: {}}
  styleParam = {}
  editSchema: SFSchema = {
    properties: {
      DGB040: {
        type: 'string',
        title: '部门编码',
        readOnly: true
      },
      DGB020: {
        type: 'string',
        title: '所属中心',
        ui: {
         hidden: true
        }, 
      },
      SSZX: {
        type: 'string',
        title: '所属中心',
        readOnly: true
      },
      DGB041: {
        type: 'string',
        title: '部门名称',
      },
      SEQ:{
         type:'number',
         title:'排序',
        
      },
      DGB052:{
        type:'string',
        title:'部门类型',
        enum: this.supdic.getSFDic('DGB052'),
        ui: {
          widget: 'select',
        }
      }
    },
   ui: {
     spanLabelFixed: 100,
     grid: {
       span: 12
     }
   },
   required: ['DGB041','SEQ','DGB052']
  }

  addSchema: SFSchema = {
    properties: {
      DGB020: {
        type: 'string',
        title: '所属中心',        
        ui: {
          hidden: true
        },        
      },
      JG: {
        type: 'string',
        title: '所属中心',
        readOnly: true,
      },
      DGB041: {
        type: 'string',
        title: '部门名称'
      },
      SEQ:{
        type:'number',
        title:'排序',
        default:0
     },
     DGB052:{
       type:'string',
       title:'部门类型',
       enum: this.supdic.getSFDic('DGB052'),
       ui: {
         widget: 'select',
       },
       default:'1'
     }
    },
    required:[ "DGB041" ,"ORGANID","DEPMTYPE","DGB052","SEQ"],
    ui:{
      spanLabelFixed:80,
      
      grid:{
        gutter:20,
        span:24
      }
    }
  }

  gwColumns: STColumn[] = [
    {title: '岗位编码',index: 'ROLEID'},
    {title: '岗位名称',index: 'RNAME'},
    {title: '岗位用户数量',index: 'GWS',type:'number'},
    { title: '操作区', buttons:[
      { 
        text: '删除',
        icon: 'delete',
        click: (record: any) => this.delGw(record),
        pop: true,
        popTitle: '确认要将该岗位删除吗？',
      },
    ]},
    
  ]
  
  @ViewChild('sf',{static:false}) sf: SFComponent;
  @ViewChild('addSf',{static:false}) addSf: SFComponent;
  @ViewChild('st',{static:false}) st: GridComponent;
  
  constructor(private lbservice: HttpService,
    private msgSrv: NzMessageService ,
    private supdic: SupDic
    ) { }



  ngOnInit() { 
    this.initKsTree();
    console.log(window.innerHeight);
    this.styleParam = {height: (window.innerHeight-220)+'px',overflow: 'auto'}
  }

  // 初始化科室树
  initKsTree(){
    this.lbservice.lbservice('ksgl_kslist',{}).then(res=>{
  
      this.nodes = res.message.list;

  

    });
 }

  nzEvent(event){
    this.isSaveDisabled = true;
    this.isDelDisabled = true;
    this.isAddDisabled = true;
     this.sf.refreshSchema();
    this.treeData = event.node.origin;
 
    if(this.treeData.hasOwnProperty("dgb040")){
      // 编辑框表单赋值
        this.sf.setValue('/DGB040',this.treeData.dgb040);
        this.sf.setValue('/DGB041',this.treeData.dgb041);
        this.sf.setValue('/DGB020',event.node.parentNode.origin.dgb020);
        this.sf.setValue('/SEQ',this.treeData.seq);
        this.sf.setValue('/DGB052',this.treeData.DGB052);
        this.sf.setValue('/SSZX',event.node.parentNode.origin.dgb021);
        this.params.queryparas =this.treeData.dgb040;
        this.st.reload({DGB040: this.params.queryparas});
        this.isSaveDisabled = true;
        this.isDelDisabled = false;
    }else{
      this.st.reload({});
    }

    if(this.treeData.hasOwnProperty("dgb020")){
      this.isAddDisabled = false;
    }
}

  formChanged(event){
    this.isSaveDisabled = false;  
   
}

delGw(record:any){
  
  this.lbservice.lbservice("gwgl_gwsc",{ROLEID: record.ROLEID}).then(result => {
    if (result.code < 1) {
      this.msgSrv.error(result.errmsg);
     }else{
      // this.st.reload(this.params.queryparas);
      this.st.reload({DGB040: this.params.queryparas});
      this.msgSrv.success("删除成功");
    }
  })
}

  add(){
    this.isVisible = true;
    this.addSf.setValue('/DGB020',this.treeData.dgb020);
    this.addSf.setValue('/JG',this.treeData.title);
    console.log(window.innerHeight);
  }

  save(){
    if(!this.sf.valid)
    {
        this.msgSrv.error('必填项不能为空！');
        return;
    }
    this.lbservice.lbservice('ksgl_ksxg',{para:this.sf.value}).then(resdata=>{
      console.log(this.sf.value);
       if(resdata.code<1)
       {
         this.msgSrv.error(resdata.errmsg);
       }
       else
       {
        this.msgSrv.success('修改成功');
        this.isSaveDisabled = true;
        setTimeout(() => {
          this.initKsTree();
        }, 500);
       }
       
    })
    
  }

  del(){
    this.lbservice.lbservice("ksgl_kssc",{DGB040: this.treeData.dgb040}).then(result => {
      if (result.code < 1) {
        this.msgSrv.error(result.errmsg);
       }else{
        this.msgSrv.success("删除成功");
      }
      setTimeout(() => {
        this.initKsTree();
        this.sf.reset();
      }, 500);
    })
  }

  handleCancel(){
    this.isVisible = false;
    this.formData = {};
  }

  saveAdd(data){
    this.lbservice.lbservice("ksgl_ksxz",{para: data}).then(result => {
      if (result.code < 1) {
        this.msgSrv.error(result.errmsg);
   }else{
        this.msgSrv.success("新增成功");
        setTimeout(() => {
          this.initKsTree();
        }, 500);
   }
   this.isVisible = false;
    })
  }

  confirm(){
    this.del();
  }

  cancel(){

  }

  _onReuseInit() {
    this.initKsTree();
  }
}
