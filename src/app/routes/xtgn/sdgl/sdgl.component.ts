import { Component, OnInit, ViewChild, Inject, Injectable, Injector} from '@angular/core';
import { _HttpClient, ModalHelper } from '@delon/theme';
import { STColumn, STComponent, STColumnButton } from '@delon/abc';
import { SFSchema, SFComponent } from '@delon/form';
import { GridComponent, HttpService, SupDic} from 'lbf';
import { NzMessageService, NzTreeNodeOptions} from 'ng-zorro-antd';
import {Router} from '@angular/router';
import {ReuseTabService} from '@delon/abc/reuse-tab';

@Component({
  selector: 'app-xtgn-sdgl',
  templateUrl: './sdgl.component.html',
  styleUrls: ['./sdgl.less']
})

export class XtgnSdglComponent implements OnInit {
  searchValue = ''
   nodes:any[] = [];
   styleParam = {}
  treeData:any;
  isSaveDisabled = true;
  isAddDisabled = true;
  isDelDisabled = true;
  formData ={};
  isVisible = false;
  modalTitle = '创建下级属地';
   
  editSchema: SFSchema = {
    properties: {
      DGB033: {
        type: 'string',
        title: '上级属地',
        readOnly: true,
        ui: {
          widget: 'select',
          width: 300
        },
        enum: this.supdic.getSFDic("DGB030")
      },
      DGB032: {
        type: 'string',
        title: '属地类型',
        readOnly: true,
        enum:this.supdic.getSFDic('DTYPE'),
        ui:{
          widget:"select",
          width: 300
        }
      },
      DGB030: {
        type: 'string',
        title: '属地编码',
        ui: {
          hidden : true
        }
      },
      DGB031: {
        type: 'string',
        title: '属地名称',
        ui: {
          width: 300
        }
      },
      
      DGB050: {
        type: 'string',
        title: '行政编码',
        ui: {
          width: 300
        }
      }
    },
   ui: {
     spanLabelFixed: 100,
     grid: {
       span: 12
     }
   },
   required: ["DGB031"]
  }

  addSchema: SFSchema = {
    properties: {
      DGB033: {
        type: 'string',
        title: '上级属地',
        readOnly:true,
        enum:this.supdic.getSFDic('DGB030'),
        ui:{
          widget:"select",
          grid:{
            span:24
          }
        }
      },
      DGB032: {
        type: 'string',
        title: '属地类型',
        readOnly: true,
        enum:this.supdic.getSFDic('DTYPE'),
        ui:{
          widget:"select",
          grid:{
            span:24
          }
        }
      },
      DGB031: {
        type: 'string',
        title: '属地名称',
        ui:{
          grid:{
            span:24
          }
        }
      },
      DGB050: {
        type: 'string',
        title: '行政编码',
        ui: {
          grid:{
            span:24
          }
        }
      }
    },
    required:[ "DGB031" ,"DGB032"],
    ui:{
      spanLabelFixed:80,
      grid:{
        gutter:20,
        span:12
      }
    }
  }

  

  
  @ViewChild('sf',{static:false}) sf: SFComponent;
  @ViewChild('addSf',{static:false}) addSf: SFComponent;
  
  constructor(private lbservice: HttpService,
    private msgSrv: NzMessageService ,
    private supdic: SupDic,
    private route: Router,
    private reuseTabService: ReuseTabService
    ) { }




  ngOnInit() { 
    this.styleParam = {height: (window.innerHeight-175)+'px',overflow: 'auto'}
    this.initSdTree(0);  
}

  // 初始化属地数 
  initSdTree(dicFlag:number){
    this.lbservice.lbservice('sdgl_sdlist',{ DIC:dicFlag }).then(res=>{
      // console.log(res);
       // this.nodes.isLoading = true;
      this.nodes = res.message.tree;
      

      // 如果是新增或修改，字典同步一下，删除和初始化的时候不用同步
      if(dicFlag===1)
      {
        this.editSchema.properties.DGB033.enum = res.message.dic;
        this.addSchema.properties.DGB033.enum = res.message.dic;
      }
      
    });
 }

  nzEvent(event){
    this.isSaveDisabled = true;
    this.isAddDisabled = true;
    this.isDelDisabled = true;
    this.treeData = event.node.origin;
    // console.log(event.node.parentNode)

    // 编辑框表单赋值
    this.sf.setValue('/DGB030',this.treeData.key);
    this.sf.setValue('/DGB031',this.treeData.title);
    this.sf.setValue('/DGB033',this.treeData.DGB033);
    this.sf.setValue('/DGB032',this.treeData.DGB032);
    this.sf.setValue('/DGB050',this.treeData.DGB050);

    this.isSaveDisabled = true;

    // 如果是村级的，则不能新增下级属地
   if(this.treeData.DGB032 !== '5'){
     this.isAddDisabled = false
   }
    this.isDelDisabled = false;
  }

  formChanged(event){
    this.isSaveDisabled = false;  
   
}

  add(){
    this.formData = { DGB033:this.treeData.key,DGB032: this.treeData.DTYPE }
    this.isVisible = true;
  }

  save(){
    if(!this.sf.valid)
    {
        this.msgSrv.error('必填项不能为空！');
        return;
    }
    this.lbservice.lbservice('sdgl_sdxg',{para:this.sf.value}).then(resdata=>{
      console.log(this.sf.value);
       if(resdata.code<1)
       {
         this.msgSrv.error(resdata.errmsg);
       }
       else
       {
        this.isSaveDisabled = true;

        // this.addSchema.properties.DGB030.enum = this.addSchema.properties.DGB030.enum.push({"label":"aaa","value":"aaaa"});
        this.msgSrv.success('修改成功');
        
        setTimeout(() => {
          this.initSdTree(1);
        }, 500);
        
       }
    })
    
  }

  del(){
    this.lbservice.lbservice("sdgl_sdsc",{DGB030: this.sf.value.DGB030}).then(result => {
      if (result.code < 1) {
        this.msgSrv.error(result.errmsg);
       }else{
        this.msgSrv.success("删除成功");
        this.sf.reset();
      }
      
      setTimeout(() => {
        this.initSdTree(0);
      }, 500);
      
    })
  }

  handleCancel(){
    this.isVisible = false;
    this.formData = {};
  }

  saveAdd(data){
    this.lbservice.lbservice("sdgl_sdxz",{para: data}).then(result => {
      if (result.code < 1) {
        this.msgSrv.error(result.errmsg);
   }else{
        this.msgSrv.success("新增成功");
        
        
        setTimeout(() => {
          this.initSdTree(1);
        }, 500);
        
        // this.initSdTree();
   }
   this.isVisible = false;
    })
  }

  confirm(){
    this.del();
  }

  cancel(){

  }

  _onReuseInit(){
    console.log(345)
  }

}
