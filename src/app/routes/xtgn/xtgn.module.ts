import { NgModule } from '@angular/core';
import { SharedModule } from '@shared';
import { XtgnRoutingModule } from './xtgn-routing.module';
import { XtgnJzmxcxComponent } from './jzmxcx/jzmxcx.component';
import { CxStackPieComponent } from './jzmxcx/components/cx-stack-pie/cx-stack-pie.component';
import { XtgnClsyComponent } from './clsy/clsy.component';
import { XtgnTysjglComponent } from './tysjgl/tysjgl.component';
import { XtgnJgglComponent } from './jggl/jggl.component';
import { XtgnSdglComponent } from './sdgl/sdgl.component';
import { XtgnGwglComponent } from './gwgl/gwgl.component';
import { XtgnKsglComponent } from './ksgl/ksgl.component';
import { XtgnYhglComponent } from './yhgl/yhgl.component';
import { LbglComponent } from './lbgl/lbgl.component';

const COMPONENTS = [
  XtgnJzmxcxComponent,
  XtgnClsyComponent,
  XtgnJgglComponent,
  XtgnSdglComponent,
  XtgnKsglComponent,
  XtgnGwglComponent,
  XtgnYhglComponent,
  XtgnTysjglComponent,
  LbglComponent];
const COMPONENTS_NOROUNT = [
  ];

@NgModule({
  imports: [
    SharedModule,
    XtgnRoutingModule,
  
  ],
  declarations: [
    ...COMPONENTS,
    ...COMPONENTS_NOROUNT,
    CxStackPieComponent,

  ],
  entryComponents: COMPONENTS_NOROUNT
})
export class XtgnModule { }
