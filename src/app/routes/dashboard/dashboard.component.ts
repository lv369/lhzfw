import { Component, ChangeDetectionStrategy, OnInit, Inject, ChangeDetectorRef, ViewChild, ElementRef } from '@angular/core';
import { NzMessageService } from 'ng-zorro-antd';
import { HttpService } from 'lbf';
import { G2CardComponent } from '@delon/chart';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
  host: {
    '[class.d-block]': 'true'
  }
})
export class DashboardComponent implements OnInit {

  ygt =[{'id':'A','name':'医共体A'},{'id':'B','name':'医共体B'}];
  xz =[{'id':'20','name':'职工医保'},{'id':'25','name':'城乡医保'}];

  ygtpara = {AKE500:'A'};
  xzpara ={AAE140:'20'};

  constructor( ) {

  }



  ngOnInit(): void {

  }

  selectJkt(event){
   this.ygtpara ={AKE500:this.ygt[event].id};
    
  }

  selectXz(event){
    this.xzpara ={AAE140:this.xz[event].id};
  }
}
