import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
// import { Calendar } from '@antv/g2plot';


import { HttpClient } from '@angular/common/http';
@Component({
  selector: 'app-workplace2',
  templateUrl: './workplace2.component.html',
  styleUrls: ['./workplace2.component.less']
})
export class Workplace2Component implements OnInit {


  @ViewChild('container',{static:false}) container:ElementRef;
  constructor(private http: HttpClient) { }


  items=[    { label: 'Apple', value: 'Apple', checked: true },
  
  { label: 'Orange', value: 'Orange' }]
  ngOnInit() {
  //  G2.Shape.registerShape('polygon', 'boundary-polygon', {
  //   draw(cfg, container) {
  //     if (!G2.Util.isEmpty(cfg.points)) {
  //       const attrs = {
  //         stroke: '#fff',
  //         lineWidth: 1,
  //         fill: cfg.color,
  //         fillOpacity: cfg.opacity,
  //         path:'',
  //       };
  //       const points = cfg.points;
  //       const path = [
  //         [ 'M', points[0].x, points[0].y ],
  //         [ 'L', points[1].x, points[1].y ],
  //         [ 'L', points[2].x, points[2].y ],
  //         [ 'L', points[3].x, points[3].y ],
  //         [ 'Z' ]
  //       ];
  //       attrs.path = this.parsePath(path);
  //       const polygon = container.addShape('path', {
  //         attrs
  //       });
  
  //       if (cfg.origin._origin.lastWeek) {
  //         const linePath = [
  //           [ 'M', points[2].x, points[2].y ],
  //           [ 'L', points[3].x, points[3].y ]
  //         ];
  //         // 最后一周的多边形添加右侧边框
  //         container.addShape('path', {
  //           zIndex: 1,
  //           attrs: {
  //             path: this.parsePath(linePath),
  //             lineWidth: 1,
  //             stroke: '#404040'
  //           }
  //         });
  //         if (cfg.origin._origin.lastDay) {
  //           container.addShape('path', {
  //             zIndex: 1,
  //             attrs: {
  //               path: this.parsePath([
  //                 [ 'M', points[1].x, points[1].y ],
  //                 [ 'L', points[2].x, points[2].y ]
  //               ]),
  //               lineWidth: 1,
  //               stroke: '#404040'
  //             }
  //           });
  //         }
  //       }
  //       container.sort();
  //       return polygon;
  //     }
  //   }
  // });
  }

  ngAfterViewInit() {
    //  Called after ngAfterContentInit when the component's view has been initialized. Applies to components only.
    //  Add 'implements AfterViewInit' to the class.
    this.chartData();
  }

  
  chartData(){
    // this.http.get('/data/github-commit').subscribe(res=>{
    //    const data = res
    //    const chart = new G2.Chart({
    //     container: 'container',
    //     forceFit: true,
    //     height: 300,
       
    //   });
    //   chart.source(data, {
    //     day: {
    //       type: 'cat',
    //       values: [ '星期日', '星期一', '星期二', '星期三', '星期四', '星期五', '星期六' ]
    //     },
    //     week: {
    //       type: 'cat'
    //     },
    //     commits: {
    //       sync: true
    //     }
    //   });
  
    //   chart.axis('week', {
    //     position: 'top',
    //     tickLine: null,
    //     line: null,
    //     label: {
    //       offset: 12,
    //       textStyle: {
    //         fontSize: 12,
    //         fill: '#666',
    //         textBaseline: 'top'
    //       },
    //       formatter: val => {
    //         if (val === '2') {
    //           return '五月';
    //         } else if (val === '6') {
    //           return '六月';
    //         } else if (val === '10') {
    //           return '七月';
    //         } else if (val === '15') {
    //           return '八月';
    //         } else if (val === '19') {
    //           return '九月';
    //         } else if (val === '24') {
    //           return '十月';
    //         }
    //         return '';
    //       }
    //     }
    //   });
    //   chart.axis('day', {
    //     grid: null
    //   });
    //   chart.legend(false);
    //   chart.tooltip({
    //     title: 'date'
    //   });
    //   chart.coord().reflect('y');
    //   chart.polygon().position('week*day*date')
    //     .color('commits', '#BAE7FF-#1890FF-#0050B3')
    //     .shape('boundary-polygon');
    //   chart.on('point:click',ev=>{
    //       console.log(1)
    //     })
    //     chart.on('polygon:click',ev=>{
    //       console.log(ev.data._origin)
    //     })
    //   chart.render();
    // })

  // g2plot
  //   this.http.get('/data/github-commit').subscribe(res=>{
  //      const data = res
  //     const calendar = new Calendar(this.container.nativeElement, {
     
   
  //       width: 650,
  //       height: 400,
  //       data,
  //       dateField: 'date',
  //       valueField: 'commits',
  //       dateRange: ['2017-05-01', '2017-10-31'],
  //       colors: '#BAE7FF-#1890FF-#0050B3',
  //       padding: 'auto',
  //       xAxis: {
  //         title: {
  //           text: '月份',
  //         },
  //       },
  //       months:['一月', '二月', '三月', '四月', '五月', '六月', '七月', '八月', '九月', '十月', '十一月', '十二月'],
  //       yAxis: {
  //         title: null,
  //       },
  //       label: {
  //         visible: true,
  //       },
  //     });
  
  //     calendar.render();
  //   });
  
  // }
 
  }

  ok(value){
    console.log(value)
  }
}
