import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  ViewChild,
} from '@angular/core';
import { zip } from 'rxjs';
import { NzMessageService, NzModalService } from 'ng-zorro-antd';
import { _HttpClient } from '@delon/theme';
import { ServiceInfo, HttpService } from 'lbf';
import { Router } from '@angular/router';
import { WsMessageService } from '@core/lb/MessageService';
import { RefService } from '@core/lb/RefService';
import { G2PieComponent } from '@delon/chart';

@Component({
  selector: 'app-dashboard-workplace',
  templateUrl: './workplace.component.html',
  styleUrls: ['./workplace.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DashboardWorkplaceComponent implements OnInit {
  asd =150
  titleList:any = [] // 首页汇总数统计
  msgList:any = [] // 动态列表
  total: string = '';
  total2: string = ''; // 月办件数量
  total4: string = ''; // 总预警件数量
  total5: string = ''; // 预警月办件数量
  countList:any = [];
  countList2:any = [];
  conclude2 :any={};// 月办结量
  conclude :any={} // 办结量

  user: any = {};
  notice: any[] = [];
  activities: any[] = [];
  links: any[] = [];
  radarData: any[];
  loading = true;
  dataset = [];
  dsl;
  dsh;
  dbj;
  dcj;
  ywc;
  jrywc;

  isVisible = false;
  nodes;
  treeData;

  linkEidtStatus = false;


  // region: mock data

  members = [];
  // endregion
  @ViewChild('pie',{static:false}) pie: G2PieComponent;
  @ViewChild('pie1',{static:false}) pie1: G2PieComponent;
  @ViewChild('pie2',{static:false}) pie2: G2PieComponent;
  @ViewChild('pie3',{static:false}) pie3: G2PieComponent;
  
  constructor(
    public msg: NzMessageService,
    private msgWs:WsMessageService,
    private modal: NzModalService,
    private cdr: ChangeDetectorRef,
    private sinfo: ServiceInfo,
    private lbservice: HttpService,
    public msgSrv: NzMessageService,
    private route: Router,
    private ref:RefService,
  ) {
    this.user = sinfo.user;
    console.log(this.user);
  }

  ngOnInit() {
    // 根据令牌是否为空，判断是否要执行
    if(this.sinfo.LOGTOKEN != undefined && this.sinfo.LOGTOKEN.length>0){
        this.initCdTree();
       this.initUserInfo();
    }


    /**
     * 获取最近6个待办事项
     *   id 申请编码
     *   title 事项名称
     *   logo 图标，分几种考虑： 1、大类展现 社保 医保 公安 民政
     *                          2、分阶段展现  收件 受理 初审 复审 办结 出件 6个图标
     *   description 描述，可以考虑 XXX 申请 XXX事项，当前XXX状态
     *   updatedAt   最后一次更新时间
     *   member      所属部门，该事项当前归哪个部门（或者说哪个操作员）？
     *   href        快速跳转到对应的办理模块，ID 会传递过去
     *   tagName     提示，类似今天到期 明天到期 还是两天到期 已经到期
     *   tagColor    对应的颜色
     */





    /**
     * 获取最近10个动态
     *   id 申请编码
     *   name 操作人姓名
     *   title 事项名称
     *   logo 图标，分几种考虑： 1、大类展现 社保 医保 公安 民政
     *                          2、分阶段展现  收件 受理 初审 复审 办结 出件 6个图标
     *   text 描述   申请了XX事项，受理了XX事项 可以考虑格式化成一下内容：
     *               动作: 申请 收件 受理 初审 复审 办结 出件
     *               申请人名称：  事项的申请人姓名，比如 张三 李四
     *               事项名称： 办理的事项名称
     *   updatedAt   最后一次更新时间
     *   href        快速跳转到对应的办理模块，ID 会传递过去
     */

/*
    if(this.sinfo.LOGTOKEN != undefined && this.sinfo.LOGTOKEN.length>0){
      this.sysl_query();
    }
    */
    this.loading = false;
  }

  showMore(){
    this.msgWs.showNotify(true)    
  }

  // 根据用户ID，获取首页初始化信息
  initUserInfo(){
    this.lbservice.lbservice('PUBLIC_INDEX_INIT', { para: {} }).then(resdata => {
      if (resdata.code < 1) {
        this.msgSrv.error(resdata.errmsg);
      } else {

        this.titleList = resdata.message.TITLE;
        this.msgList = resdata.message.MSG;
        this.total = resdata.message.TOTAL;
        this.total2= resdata.message.TOTAL2;
        this.total4= resdata.message.TOTAL4;
        this.total5= resdata.message.TOTAL5;
       this.conclude=resdata.message.BJ;
       this.conclude2=resdata.message.YBJ;
        this.countList = resdata.message.PIE;
        this.countList2 = resdata.message.PIE2;
        this.pie.colors = ['#5ad8a6','#1890ff'];
        this.pie1.colors = ['#5ad8a6','#1890ff'];
        this.pie2.colors =  ['#f6bd16','#e86452'];
        this.pie3.colors = ['#f6bd16','#e86452'];
        this.cdr.markForCheck();
      }
    })
  }

  clickMsg(item:any){
    this.ref.sendRef({receive:'shgl',message:JSON.parse(item.PARA),sender:'message',createTime:new Date(),isGoto:true,url:item.URL})
  }


  sysl_query() {
    // 数量查询  
    this.lbservice.lbservice('SYSL_QUERY', { para: {} }).then(resdata => {
      if (resdata.code < 1) {
        this.msgSrv.error(resdata.errmsg);
      } else {
        this.dataset = resdata.message.list;
        this.dsl = this.dataset[0].DSL;
        this.dsh = this.dataset[0].DSH;
        this.ywc = this.dataset[0].YWC;
        this.jrywc = this.dataset[0].JRYWC;
        this.dbj = this.dataset[0].DBJ;
        this.dcj = this.dataset[0].DCJ;
        this.cdr.markForCheck();
      }
    });

    // 进行中事项查询
    this.lbservice.lbservice('JXZSX_QUERY', { para: { FLAG: '1' } }).then(resdata => {
      if (resdata.code < 1) {
        this.msgSrv.error(resdata.errmsg);
      } else {
        this.notice = resdata.message.list;
        this.cdr.markForCheck();
      }
    });
    // 已办理事项环节查询
    this.lbservice.lbservice('JXZSX_QUERY', { para: { FLAG: '2' } }).then(resdata => {
      if (resdata.code < 1) {
        this.msgSrv.error(resdata.errmsg);
      } else {
        this.activities = resdata.message.list;
        this.cdr.markForCheck();
      }
    });
    // 快捷菜单查询
    this.lbservice.lbservice('KJCD_QUERY', {}).then(resdata => {
      if (resdata.code < 1) {
        this.msgSrv.error(resdata.errmsg);
      } else {
        this.links = resdata.message.list;
        this.cdr.markForCheck();
      }
    });

  }

  add() {

    this.isVisible = true;
  }

  initCdTree() {
    this.lbservice.lbservice('gwgl_cdlist', {}).then(res => {
      console.log(res.message.list);
      this.nodes = res.message.list;
    });
  }

  handleCancel() {
    this.isVisible = false;
  }

  nzEvent(event) {



    this.treeData = event.node.origin;
    if (this.treeData.isLeaf) {
      this.lbservice.lbservice('KJCD_ADD', { FID: this.treeData.key }).then(resdata => {
        if (resdata.code < 1) {
          this.msgSrv.error(resdata.errmsg);
        } else {
          this.msgSrv.success('添加成功')
          this.links = resdata.message.list;
          this.cdr.markForCheck();
        }
      })
      this.isVisible = false;
    }
  }


  _onReuseInit(): void {
    console.log('---_onReuseInit----')
    this.initUserInfo()
    // this.sysl_query();
  }


  linkClick(item) {

    if (this.linkEidtStatus) {

      this.modal.confirm(
        {
          nzTitle: '是否删除?',
          nzContent: `是否删除功能[${item.TITLE}]的快捷链接?`,
          nzOnOk: () => {
            this.delLink(item);
          }
        })

    } else {
      this.route.navigateByUrl(item.HREF);
    }


  }

  delLink(item) {

    this.msg.info(`等待删除${item.FID}`)
    // todo 调用数据库删除
    this.lbservice.lbservice('KJCD_DEL', { FID: item.FID }).then(resdata => {
      if (resdata.code < 1) {
        this.msgSrv.error(resdata.errmsg);
      } else {
        this.msgSrv.success('删除成功')
        this.links = resdata.message.list;
        this.cdr.markForCheck();
      }
    })
  }

}
