import { Component, OnInit, Inject, ChangeDetectionStrategy, Optional } from '@angular/core';
import { Router } from '@angular/router';
import { SettingsService, ModalHelper } from '@delon/theme';
import { DA_SERVICE_TOKEN, ITokenService } from '@delon/auth';
import { ServiceInfo, SupDic, HttpService } from 'lbf';
import { NzMessageService, NzModalService } from 'ng-zorro-antd';
import { ReuseTabService } from '@delon/abc';


@Component({
  selector: 'layout-pro-user',
  templateUrl: 'user.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class LayoutProWidgetUserComponent implements OnInit {
  constructor(
    public settings: SettingsService,
    private router: Router,
    private sinfo: ServiceInfo,
    @Inject(DA_SERVICE_TOKEN) private tokenService: ITokenService,
    private supdic: SupDic,
    private lbservice: HttpService,
    private message: NzMessageService,
    private modal: NzModalService,
    @Optional()
    @Inject(ReuseTabService)
    private reuseTabService: ReuseTabService,
  ) { }


  user: any;

  roles = []

  // 修改密码部分

  isVisible = false;
  isOkLoading = false;
  toRname: string;

  ngOnInit(): void {

    this.user = this.sinfo.user;
    this.user.rname = this.supdic.getdicLabel("ROLEID", this.user.roleid);
    if(this.sinfo.LOGTOKEN != undefined && this.sinfo.LOGTOKEN.length>0){
    this.lbservice.lbservice('FR_getuserrole', {}).then(resdata => {
      if (resdata.code >= 1) {
        this.roles = resdata.message.list.map((r: any) => {
          r.roleid = r.ROLEID;
          r.rname = r.RNAME;
          return r;
        });
      } else {
        // this.message.error('获取角色列表失败！');
      }

      console.log(this.user);

    })
  }

  }

  logout() {

    this.lbservice.lbservice("fr_logout", {}).then(resdata => {

      this.tokenService.clear();
      this.sinfo.clearToken();
      this.reuseTabService.clear();
      this.router.navigateByUrl(this.tokenService.login_url);

    })


  }


  handleCancel() {

    this.isVisible = false;
  }

  handleOk() {

    this.isOkLoading = true;
  }

  changedPwd() {


  }

  changeRole(role: any) {

    if (role.roleid === this.user.roleid) return;

    this.toRname = role.rname;

    this.modal.confirm({
      nzTitle: '确认切换角色?',
      nzContent: '是否将角色从[' + this.user.rname + ']切换到[' + this.toRname + ']',
      nzOnOk: () => this.doChangRole(role)
    });


  }


  doChangRole(role: any) {

    this.lbservice.changeRole(role.roleid).then(resdata => {

      if (resdata.code < 1) {
        this.message.error(resdata.errmsg);
        return;
      }

      this.sinfo.LOGTOKEN = resdata.message.LOGINTOKEN;
      this.sinfo.saveToken(resdata.message.LOGINTOKEN);
      this.sinfo.setUser({ aac003: resdata.message.AAC003, name: this.user.name, dept: resdata.message.DEPT, usertype: resdata.message.USERTYPE, roleid: resdata.message.USERTYPE, ks: resdata.message.KS });

      let c = localStorage.getItem('usercache');
      if (!c) {
        c = "{}";
      }

      const usercache = JSON.parse(c);
      if (!usercache[this.sinfo.user.name]) {
        usercache[this.sinfo.user.name] = { li: [], cdr: [] };
      }

      usercache[this.sinfo.user.name].cdr.push(new Date());
      usercache[this.sinfo.user.name].roleid = resdata.message.ROLEID;
      localStorage.setItem('usercache', JSON.stringify(usercache));

      setTimeout(() => {
        location.reload();
      }, 500);
    })
  }


}