import { Component, ChangeDetectionStrategy, ChangeDetectorRef, Output, EventEmitter, OnInit, ViewChild, Input, OnChanges } from '@angular/core';
import * as distanceInWordsToNow from 'date-fns/distance_in_words_to_now';
import { NzMessageService } from 'ng-zorro-antd';
import { NoticeItem, NoticeIconList, STColumn } from '@delon/abc';
import {Router,ActivatedRoute} from '@angular/router';
import { WsMessageService } from '@core/lb/MessageService';
import {HttpService,GridComponent,SupDic, ServiceInfo} from 'lbf';
import { SFSchema, SFComponent } from '@delon/form';
import { Observable } from 'rxjs';

/**
 * 菜单通知
 */
@Component({
  selector: 'layout-pro-notify',
  templateUrl: './notify.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class LayoutProWidgetNotifyComponent implements OnInit,OnChanges{

  @Input() MESSAGE;


  isVisible = false
  data: NoticeItem[] = [
    
  ];
  count = 0;
  loading = false;

  /* ------------------  信息详情  ------------------------*/
  xxDetail = {}; // 消息数据
  flag = false; // 是否显示消息详情
  title = ''; // 消息标题
  serSchema :SFSchema = {
    properties: {
      START: {
        type: 'string',
        title: '开始时间',
        ui: {
          widget: 'date'
        }
      },
      END: {
        type: 'string',
        title: '结束时间',
        ui: {
          widget: 'date'
        }
      },
      JSZT: {
        type: 'string',
        title: '接受状态',
        enum: this.supdic.getSFDic('JSZT'),
        ui: {
          widget: 'select',
          width: 250,
          allowClear: true
        }
      }
    },
    ui: {
      grid: {
        span: 8
      }
    }
  }
  
  /*-------------------  新增消息  ------------------------*/
  param = {sname: 'XXGL_QUERY',xttzParam: {XTYPE: '1'}, ywtxParam: {XTYPE: '2'} ,jbParam: {XTYPE: '3'}}

  columns:STColumn[] = [
    {title: '状态',render: 'custom' },
    {title: '发件人',index: 'FSR',format: record => this.supdic.getdicLabel('USERID',record.FSR)},
    {title: '标题',type:'link',index: 'XTITLE',click: (record) => this.xxxq(record),width:400,
    format: (record) => record.XTITLE === ' '?'(无主题)':record.XTITLE},
    {title: '发送时间',index: 'FSSJ'}
  ]
    
  
@ViewChild('xttz',{static: false}) xttz: GridComponent;
@ViewChild('ywtx',{static: false}) ywtx: GridComponent;
@ViewChild('jb',{static: false}) jb: GridComponent;
@ViewChild('sf',{static: false}) sf: SFComponent;
  constructor(private msg: NzMessageService, private cdr: ChangeDetectorRef,private supdic: SupDic,
    private msgWs:WsMessageService,
    public sinfo: ServiceInfo,
    private router: Router,private httpService: HttpService,) {}

    // 消息发送点击数据展示
  ngOnChanges(changes: import("@angular/core").SimpleChanges): void {
    console.log(changes)
    this.xxwdcx();
    

    if(changes.MESSAGE.currentValue !== undefined && changes.MESSAGE.currentValue !== 'false'){
      if(changes.MESSAGE.currentValue.XTYPE==='1'){
        this.click();
        
      }
      this.xxxq(changes.MESSAGE.currentValue);
      // 消息未读查询
      this.xxwdcx();
    }

  }

  ngOnInit(): void {

    // this.msgWs.getMessage().subscribe(message=>{
    //   console.log(message);
    // })
    if(this.sinfo.LOGTOKEN != undefined && this.sinfo.LOGTOKEN.length>0){
      this.xxwdcx();
      console.log('----setNotify1------')
      this.msgWs.setNotify(this);
    }
  }

  

  /**
   * 消息未读查询
   */
  xxwdcx(){
    
    this.httpService.lbservice('XXGL_XXWDCX',{}).then(res => {
      if(res.code > 0 ){
        
        this.count = res.page.COUNT;
        console.log('------消息未读查询-'+this.count+'---');
      }
    })
  }

  updateNoticeData(notices: NoticeIconList[]): NoticeItem[] {
    const data = this.data.slice();
    data.forEach(i => (i.list = []));

    notices.forEach(item => {
      const newItem = { ...item };
      if (newItem.datetime)
        newItem.datetime = distanceInWordsToNow(item.datetime, {
          locale: (window as any).__locale__,
        });
      if (newItem.extra && newItem.status) {
        newItem.color = {
          todo: undefined,
          processing: 'blue',
          urgent: 'red',
          doing: 'gold',
        }[newItem.status];
      }
      data.find(w => w.title === newItem.type).list.push(newItem);
    });
    return data;
  }

  loadData() {
    if (this.loading) return;
    this.loading = true;
    setTimeout(() => {
      this.data = this.updateNoticeData([
      ]);

      this.loading = false;

      this.cdr.detectChanges();
    }, 1000);
  }

  clear(type: string) {
    this.msg.success(`清空了 ${type}`);
  }

  select(res: any) {
    this.msg.success(`点击了 ${res.title} 的 ${res.item.title}`);
  }

  click(){
    console.log(123)
    this.xttz.reload();
    this.ywtx.reload();
    this.jb.reload();
    this.flag = false;
    this.isVisible = true
    // this.msgWs.showMessageBox = true;
  }

  close(){
    this.isVisible = false;
    // this.msgWs.showMessageBox = false;
  
  }

  query(){
    let temp = this.sf.value;
    temp.XTYPE = '1'
    this.xttz.reload(temp);
    temp.XTYPE = '2'
    this.ywtx.reload(temp);
    temp.XTYPE = '3'
    this.jb.reload(temp);
  }

  xxxq(record){ 
    console.log(record)
    this.xxDetail = record;
    this.flag = true;
    this.httpService.lbservice('XXGL_ZTUP',{XXID: record.XXID,YDSJ: new Date().toLocaleDateString()}).then(res =>{
      if(res.code > 0){
        if(record['XTYPE'] === '1') {this.xttz.reload({ XTYPE: "1"});}
        if(record['XTYPE'] === '2') {this.ywtx.reload({ XTYPE: "2"});}
        if(record['XTYPE'] === '3') {this.jb.reload({XTYPE: "3"});}
        console.log('-------消息状态更改----------')
        this.xxwdcx();
      }
    });
  }

  onVoted(event){
    this.isVisible = false;
    // this.msgWs.showMessageBox = false;
  }

  
}
