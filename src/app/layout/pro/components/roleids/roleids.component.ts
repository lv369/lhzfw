import {
  Component,
  ChangeDetectionStrategy,
  Inject,
  Input,
} from '@angular/core';
import { SettingsService, ALAIN_I18N_TOKEN } from '@delon/theme';
import { ServiceInfo } from 'lbf';

@Component({
  selector: 'pro-roleids',
  templateUrl: './roleids.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class RoleidsComponent {
  

  @Input() placement = 'bottomRight';
  @Input() btnClass = 'alain-pro__header-item';
  @Input() btnIconClass = 'alain-pro__header-item-icon';

  constructor(
    public settings: SettingsService,
    private sinfo:ServiceInfo,
  ) {
     this.user=sinfo.user;
     console.log(this.user);
  }

  user:any={};
  roles =[{roleid:'1',rname:'管理员'},{roleid:'2',rname:'中心业务'},{roleid:'2',rname:'科长'}]

  change(lang: string) {
    // const spinEl = this.doc.createElement('div');
    // spinEl.setAttribute(
    //   'class',
    //   `page-loading ant-spin ant-spin-lg ant-spin-spinning`,
    // );
    // spinEl.innerHTML = `<span class="ant-spin-dot ant-spin-dot-spin"><i></i><i></i><i></i><i></i></span>`;
    // this.doc.body.appendChild(spinEl);

    // this.i18n.use(lang);
    // this.settings.setLayout('lang', lang);
    // setTimeout(() => this.doc.location.reload());
  }
}
