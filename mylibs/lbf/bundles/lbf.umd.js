(function (global, factory) {
    typeof exports === 'object' && typeof module !== 'undefined' ? factory(exports, require('@angular/core'), require('rxjs'), require('@angular/common/http'), require('rxjs/operators'), require('ts-md5/dist/md5'), require('ng-zorro-antd'), require('@delon/theme'), require('@delon/abc'), require('@delon/chart'), require('@delon/acl'), require('@delon/util'), require('@delon/auth'), require('@delon/form'), require('@delon/cache'), require('@delon/abc/xlsx'), require('@angular/common'), require('@angular/router')) :
    typeof define === 'function' && define.amd ? define('lbf', ['exports', '@angular/core', 'rxjs', '@angular/common/http', 'rxjs/operators', 'ts-md5/dist/md5', 'ng-zorro-antd', '@delon/theme', '@delon/abc', '@delon/chart', '@delon/acl', '@delon/util', '@delon/auth', '@delon/form', '@delon/cache', '@delon/abc/xlsx', '@angular/common', '@angular/router'], factory) :
    (global = global || self, factory(global.lbf = {}, global.ng.core, global.rxjs, global.ng.common.http, global.rxjs.operators, global.md5, global.ngZorroAntd, global.theme, global.abc, global.chart, global.acl, global.util, global.auth, global.form, global.cache, global.xlsx, global.ng.common, global.ng.router));
}(this, (function (exports, core, rxjs, http, operators, md5, ngZorroAntd, theme, abc, chart, acl, util, auth, form, cache, xlsx, common, router) { 'use strict';

    /*! *****************************************************************************
    Copyright (c) Microsoft Corporation. All rights reserved.
    Licensed under the Apache License, Version 2.0 (the "License"); you may not use
    this file except in compliance with the License. You may obtain a copy of the
    License at http://www.apache.org/licenses/LICENSE-2.0

    THIS CODE IS PROVIDED ON AN *AS IS* BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
    KIND, EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION ANY IMPLIED
    WARRANTIES OR CONDITIONS OF TITLE, FITNESS FOR A PARTICULAR PURPOSE,
    MERCHANTABLITY OR NON-INFRINGEMENT.

    See the Apache Version 2.0 License for specific language governing permissions
    and limitations under the License.
    ***************************************************************************** */
    /* global Reflect, Promise */

    var extendStatics = function(d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };

    function __extends(d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    }

    var __assign = function() {
        __assign = Object.assign || function __assign(t) {
            for (var s, i = 1, n = arguments.length; i < n; i++) {
                s = arguments[i];
                for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p)) t[p] = s[p];
            }
            return t;
        };
        return __assign.apply(this, arguments);
    };

    function __rest(s, e) {
        var t = {};
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
            t[p] = s[p];
        if (s != null && typeof Object.getOwnPropertySymbols === "function")
            for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
                if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
                    t[p[i]] = s[p[i]];
            }
        return t;
    }

    function __decorate(decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    }

    function __param(paramIndex, decorator) {
        return function (target, key) { decorator(target, key, paramIndex); }
    }

    function __metadata(metadataKey, metadataValue) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(metadataKey, metadataValue);
    }

    function __awaiter(thisArg, _arguments, P, generator) {
        function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
        return new (P || (P = Promise))(function (resolve, reject) {
            function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
            function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
            function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
            step((generator = generator.apply(thisArg, _arguments || [])).next());
        });
    }

    function __generator(thisArg, body) {
        var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
        return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
        function verb(n) { return function (v) { return step([n, v]); }; }
        function step(op) {
            if (f) throw new TypeError("Generator is already executing.");
            while (_) try {
                if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
                if (y = 0, t) op = [op[0] & 2, t.value];
                switch (op[0]) {
                    case 0: case 1: t = op; break;
                    case 4: _.label++; return { value: op[1], done: false };
                    case 5: _.label++; y = op[1]; op = [0]; continue;
                    case 7: op = _.ops.pop(); _.trys.pop(); continue;
                    default:
                        if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                        if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                        if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                        if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                        if (t[2]) _.ops.pop();
                        _.trys.pop(); continue;
                }
                op = body.call(thisArg, _);
            } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
            if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
        }
    }

    function __exportStar(m, exports) {
        for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
    }

    function __values(o) {
        var s = typeof Symbol === "function" && Symbol.iterator, m = s && o[s], i = 0;
        if (m) return m.call(o);
        if (o && typeof o.length === "number") return {
            next: function () {
                if (o && i >= o.length) o = void 0;
                return { value: o && o[i++], done: !o };
            }
        };
        throw new TypeError(s ? "Object is not iterable." : "Symbol.iterator is not defined.");
    }

    function __read(o, n) {
        var m = typeof Symbol === "function" && o[Symbol.iterator];
        if (!m) return o;
        var i = m.call(o), r, ar = [], e;
        try {
            while ((n === void 0 || n-- > 0) && !(r = i.next()).done) ar.push(r.value);
        }
        catch (error) { e = { error: error }; }
        finally {
            try {
                if (r && !r.done && (m = i["return"])) m.call(i);
            }
            finally { if (e) throw e.error; }
        }
        return ar;
    }

    function __spread() {
        for (var ar = [], i = 0; i < arguments.length; i++)
            ar = ar.concat(__read(arguments[i]));
        return ar;
    }

    function __spreadArrays() {
        for (var s = 0, i = 0, il = arguments.length; i < il; i++) s += arguments[i].length;
        for (var r = Array(s), k = 0, i = 0; i < il; i++)
            for (var a = arguments[i], j = 0, jl = a.length; j < jl; j++, k++)
                r[k] = a[j];
        return r;
    };

    function __await(v) {
        return this instanceof __await ? (this.v = v, this) : new __await(v);
    }

    function __asyncGenerator(thisArg, _arguments, generator) {
        if (!Symbol.asyncIterator) throw new TypeError("Symbol.asyncIterator is not defined.");
        var g = generator.apply(thisArg, _arguments || []), i, q = [];
        return i = {}, verb("next"), verb("throw"), verb("return"), i[Symbol.asyncIterator] = function () { return this; }, i;
        function verb(n) { if (g[n]) i[n] = function (v) { return new Promise(function (a, b) { q.push([n, v, a, b]) > 1 || resume(n, v); }); }; }
        function resume(n, v) { try { step(g[n](v)); } catch (e) { settle(q[0][3], e); } }
        function step(r) { r.value instanceof __await ? Promise.resolve(r.value.v).then(fulfill, reject) : settle(q[0][2], r); }
        function fulfill(value) { resume("next", value); }
        function reject(value) { resume("throw", value); }
        function settle(f, v) { if (f(v), q.shift(), q.length) resume(q[0][0], q[0][1]); }
    }

    function __asyncDelegator(o) {
        var i, p;
        return i = {}, verb("next"), verb("throw", function (e) { throw e; }), verb("return"), i[Symbol.iterator] = function () { return this; }, i;
        function verb(n, f) { i[n] = o[n] ? function (v) { return (p = !p) ? { value: __await(o[n](v)), done: n === "return" } : f ? f(v) : v; } : f; }
    }

    function __asyncValues(o) {
        if (!Symbol.asyncIterator) throw new TypeError("Symbol.asyncIterator is not defined.");
        var m = o[Symbol.asyncIterator], i;
        return m ? m.call(o) : (o = typeof __values === "function" ? __values(o) : o[Symbol.iterator](), i = {}, verb("next"), verb("throw"), verb("return"), i[Symbol.asyncIterator] = function () { return this; }, i);
        function verb(n) { i[n] = o[n] && function (v) { return new Promise(function (resolve, reject) { v = o[n](v), settle(resolve, reject, v.done, v.value); }); }; }
        function settle(resolve, reject, d, v) { Promise.resolve(v).then(function(v) { resolve({ value: v, done: d }); }, reject); }
    }

    function __makeTemplateObject(cooked, raw) {
        if (Object.defineProperty) { Object.defineProperty(cooked, "raw", { value: raw }); } else { cooked.raw = raw; }
        return cooked;
    };

    function __importStar(mod) {
        if (mod && mod.__esModule) return mod;
        var result = {};
        if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
        result.default = mod;
        return result;
    }

    function __importDefault(mod) {
        return (mod && mod.__esModule) ? mod : { default: mod };
    }

    function __classPrivateFieldGet(receiver, privateMap) {
        if (!privateMap.has(receiver)) {
            throw new TypeError("attempted to get private field on non-instance");
        }
        return privateMap.get(receiver);
    }

    function __classPrivateFieldSet(receiver, privateMap, value) {
        if (!privateMap.has(receiver)) {
            throw new TypeError("attempted to set private field on non-instance");
        }
        privateMap.set(receiver, value);
        return value;
    }

    /**
     * @fileoverview added by tsickle
     * Generated from: lib/lbf.component.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var LbfComponent = /** @class */ (function () {
        function LbfComponent() {
        }
        /**
         * @return {?}
         */
        LbfComponent.prototype.ngOnInit = /**
         * @return {?}
         */
        function () {
        };
        LbfComponent.decorators = [
            { type: core.Component, args: [{
                        selector: 'lb-lbf',
                        template: "\n    <p>\n      lbf works!\n    </p>\n  "
                    }] }
        ];
        /** @nocollapse */
        LbfComponent.ctorParameters = function () { return []; };
        return LbfComponent;
    }());

    /**
     * @fileoverview added by tsickle
     * Generated from: lib/ServiceConfig.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var ServiceInfo = /** @class */ (function () {
        function ServiceInfo() {
            this.APPID = "TEST_APPID";
            this.APPKEY = "TEST_APPKEY";
            this.PWDFLAG = "H5CS3";
            this.LOGTOKEN = "";
            this.VERSION = "20190730";
            this.APPINFO = {
                "name": "测试",
                "description": ""
            };
            this.APPSUFFIX = this.APPINFO.name;
            this.user = { aac003: "", name: "", roleid: "", dept: "", avatar: "./assets/tmp/img/avatar.jpg" };
            this.baseUrl = "";
            this.molssUrl = "mohrss/chbx";
            this.uploadurl = "mohrss/chbx/upload";
            this._adminRole = [];
        }
        /**
         * @param {?} adminRole
         * @return {?}
         */
        ServiceInfo.prototype.setAdminRole = /**
         * @param {?} adminRole
         * @return {?}
         */
        function (adminRole) {
            this._adminRole = adminRole;
        };
        Object.defineProperty(ServiceInfo.prototype, "isAdmin", {
            get: /**
             * @return {?}
             */
            function () {
                if (!this._adminRole || !this.user || !this.user.roleid)
                    return false;
                return this._adminRole.includes(this.user.roleid);
            },
            enumerable: true,
            configurable: true
        });
        /**
         * @param {?} sname
         * @param {?} timestamp
         * @param {?} para
         * @return {?}
         */
        ServiceInfo.prototype.getSign = /**
         * @param {?} sname
         * @param {?} timestamp
         * @param {?} para
         * @return {?}
         */
        function (sname, timestamp, para) {
            /** @type {?} */
            var hashstr = this.APPKEY + para + timestamp + sname + this.LOGTOKEN;
            return md5.Md5.hashStr(hashstr).toString();
        };
        /**
         * @param {?} info
         * @return {?}
         */
        ServiceInfo.prototype.setSinfo = /**
         * @param {?} info
         * @return {?}
         */
        function (info) {
            this.APPID = info.APPID;
            this.APPKEY = info.APPKEY;
            this.VERSION = info.VERSION;
            this.APPINFO.name = info.APPNAME;
            this.APPSUFFIX = this.APPINFO.name;
        };
        /**
         * @param {?} user
         * @return {?}
         */
        ServiceInfo.prototype.setUser = /**
         * @param {?} user
         * @return {?}
         */
        function (user) {
            this.user = __assign({}, this.user, user);
            // this.user.name = user.name;
            // this.user.aac003 = user.aac003;
            this.user.roleid = user.usertype;
            // this.user.dept = user.dept;
            sessionStorage.setItem("userinfo", JSON.stringify(this.user));
        };
        /**
         * @return {?}
         */
        ServiceInfo.prototype.loaduser = /**
         * @return {?}
         */
        function () {
            /** @type {?} */
            var userinfo = sessionStorage.getItem("userinfo");
            try {
                /** @type {?} */
                var u = JSON.parse(userinfo);
                this.user = __assign({}, this.user, u);
                // this.user.name = u.name;
                // this.user.aac003 = u.aac003;
                // this.user.roleid = u.roleid;
                // this.user.dept = u.dept;
            }
            catch (ex) {
            }
        };
        /**
         * @return {?}
         */
        ServiceInfo.prototype.getTimestamp = /**
         * @return {?}
         */
        function () {
            /** @type {?} */
            var d = new Date();
            return Date.UTC(d.getFullYear(), d.getMonth(), d.getDate(), d.getHours(), d.getMinutes(), d.getSeconds(), d.getMilliseconds());
        };
        /**
         * @param {?} token
         * @return {?}
         */
        ServiceInfo.prototype.saveToken = /**
         * @param {?} token
         * @return {?}
         */
        function (token) {
            sessionStorage.setItem("logintoken", token);
        };
        /**
         * @return {?}
         */
        ServiceInfo.prototype.getToken = /**
         * @return {?}
         */
        function () {
            this.LOGTOKEN = sessionStorage.getItem("logintoken");
            if (this.LOGTOKEN == null)
                this.LOGTOKEN = "";
        };
        /**
         * @return {?}
         */
        ServiceInfo.prototype.clearToken = /**
         * @return {?}
         */
        function () {
            sessionStorage.setItem("logintoken", "");
        };
        /**
         * @param {?} md5Str
         * @return {?}
         */
        ServiceInfo.prototype.md5 = /**
         * @param {?} md5Str
         * @return {?}
         */
        function (md5Str) {
            return md5.Md5.hashStr(md5Str).toString();
        };
        /**
         * @param {?} pwd
         * @return {?}
         */
        ServiceInfo.prototype.pwdmd5 = /**
         * @param {?} pwd
         * @return {?}
         */
        function (pwd) {
            return this.md5(pwd + this.PWDFLAG);
        };
        ServiceInfo.decorators = [
            { type: core.Injectable, args: [{
                        providedIn: 'root'
                    },] }
        ];
        /** @nocollapse */
        ServiceInfo.ctorParameters = function () { return []; };
        /** @nocollapse */ ServiceInfo.ngInjectableDef = core.ɵɵdefineInjectable({ factory: function ServiceInfo_Factory() { return new ServiceInfo(); }, token: ServiceInfo, providedIn: "root" });
        return ServiceInfo;
    }());
    if (false) {
        /** @type {?} */
        ServiceInfo.prototype.APPID;
        /**
         * @type {?}
         * @private
         */
        ServiceInfo.prototype.APPKEY;
        /**
         * @type {?}
         * @private
         */
        ServiceInfo.prototype.PWDFLAG;
        /** @type {?} */
        ServiceInfo.prototype.LOGTOKEN;
        /** @type {?} */
        ServiceInfo.prototype.VERSION;
        /** @type {?} */
        ServiceInfo.prototype.APPINFO;
        /** @type {?} */
        ServiceInfo.prototype.APPSUFFIX;
        /** @type {?} */
        ServiceInfo.prototype.user;
        /** @type {?} */
        ServiceInfo.prototype.baseUrl;
        /** @type {?} */
        ServiceInfo.prototype.molssUrl;
        /** @type {?} */
        ServiceInfo.prototype.uploadurl;
        /** @type {?} */
        ServiceInfo.prototype._adminRole;
    }

    /**
     * @fileoverview added by tsickle
     * Generated from: lib/lblibs/Lbjk.service.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var HttpService = /** @class */ (function () {
        function HttpService(httpClient, serviceInfo) {
            this.httpClient = httpClient;
            this.serviceInfo = serviceInfo;
        }
        /**
         * 登录服务
         * @param username 用户名
         * @param password 用户密码
         * @returns 登录结果 Promise<any>
         */
        /**
         * 登录服务
         * @param {?} username 用户名
         * @param {?} password 用户密码
         * @return {?} 登录结果 Promise<any>
         */
        HttpService.prototype.login = /**
         * 登录服务
         * @param {?} username 用户名
         * @param {?} password 用户密码
         * @return {?} 登录结果 Promise<any>
         */
        function (username, password) {
            var _this = this;
            /** @type {?} */
            var c = localStorage.getItem('usercache');
            if (!c) {
                c = "{}";
            }
            /** @type {?} */
            var usercache = JSON.parse(c);
            /** @type {?} */
            var ROLEID = null;
            if (usercache && usercache[username]) {
                ROLEID = usercache[username].roleid;
            }
            return new Promise((/**
             * @param {?} resolve
             * @param {?} _
             * @return {?}
             */
            function (resolve, _) {
                _this.lbservice("FR_login", { "para": { "USERID": username, "PWD": _this.serviceInfo.pwdmd5(password), ROLEID: ROLEID } }, 500, true).then((/**
                 * @param {?} resdata
                 * @return {?}
                 */
                function (resdata) {
                    // 设置登录token
                    if (resdata.code > 0) {
                        _this.serviceInfo.LOGTOKEN = resdata.message.LOGINTOKEN;
                        _this.serviceInfo.saveToken(resdata.message.LOGINTOKEN);
                        _this.serviceInfo.setUser({ aac003: resdata.message.NAME, name: username, dept: resdata.message.DEPT, roleid: resdata.message.ROLEID });
                        if (!usercache) {
                            usercache = {};
                        }
                        if (!usercache[username]) {
                            usercache[username] = { li: [], cdr: [] };
                        }
                        usercache[username].li.push(new Date());
                        usercache[username].roleid = resdata.message.ROLEID;
                        localStorage.setItem('usercache', JSON.stringify(usercache));
                    }
                    resolve(resdata);
                }));
            }));
        };
        /**
         * @param {?} roleid
         * @return {?}
         */
        HttpService.prototype.changeRole = /**
         * @param {?} roleid
         * @return {?}
         */
        function (roleid) {
            var _this = this;
            /** @type {?} */
            var c = localStorage.getItem('usercache');
            if (!c) {
                c = "{}";
            }
            /** @type {?} */
            var usercache = JSON.parse(c);
            return new Promise((/**
             * @param {?} resolve
             * @param {?} _
             * @return {?}
             */
            function (resolve, _) {
                _this.lbservice("FR_cdr", { "para": { ROLEID: roleid } }).then((/**
                 * @param {?} resdata
                 * @return {?}
                 */
                function (resdata) {
                    // 设置登录token
                    if (resdata.code > 0) {
                        _this.serviceInfo.LOGTOKEN = resdata.message.LOGINTOKEN;
                        _this.serviceInfo.user.roleid = roleid;
                        if (!usercache) {
                            usercache = {};
                        }
                        if (!usercache[_this.serviceInfo.user.name]) {
                            usercache[_this.serviceInfo.user.name] = { li: [], cdr: [] };
                        }
                        usercache[_this.serviceInfo.user.name].cdr.push(new Date());
                        usercache[_this.serviceInfo.user.name].roleid = resdata.message.ROLEID;
                        localStorage.setItem('usercache', JSON.stringify(usercache));
                    }
                    resolve(resdata);
                }));
            }));
        };
        /**
         * @param {?} sname
         * @param {?} para
         * @param {?=} debounce
         * @param {?=} anonymous
         * @return {?}
         */
        HttpService.prototype.lbservice2 = /**
         * @param {?} sname
         * @param {?} para
         * @param {?=} debounce
         * @param {?=} anonymous
         * @return {?}
         */
        function (sname, para, debounce, anonymous) {
            if (debounce === void 0) { debounce = 0; }
            if (anonymous === void 0) { anonymous = false; }
            /** @type {?} */
            var httpOptions = {
                headers: new http.HttpHeaders({
                    'SNAME': sname
                })
            };
            /** @type {?} */
            var url = this.serviceInfo.molssUrl;
            if (anonymous) {
                url = url + '?_allow_anonymous=true';
            }
            return this.httpClient.post(url, para, httpOptions);
        };
        /**
         *  访问服务端
         * @param sname 服务名
         * @param para 入参
         * @param debounce 防抖设置，默认0
         * @param anonymous 是否匿名访问，默认否
         */
        /**
         *  访问服务端
         * @param {?} sname 服务名
         * @param {?} para 入参
         * @param {?=} debounce 防抖设置，默认0
         * @param {?=} anonymous 是否匿名访问，默认否
         * @return {?}
         */
        HttpService.prototype.lbservice = /**
         *  访问服务端
         * @param {?} sname 服务名
         * @param {?} para 入参
         * @param {?=} debounce 防抖设置，默认0
         * @param {?=} anonymous 是否匿名访问，默认否
         * @return {?}
         */
        function (sname, para, debounce, anonymous) {
            var _this = this;
            if (debounce === void 0) { debounce = 0; }
            if (anonymous === void 0) { anonymous = false; }
            /** @type {?} */
            var httpOptions = {
                headers: new http.HttpHeaders({
                    'SNAME': sname
                })
            };
            /** @type {?} */
            var url = this.serviceInfo.molssUrl;
            if (anonymous) {
                url = url + '?_allow_anonymous=true';
            }
            return new Promise((/**
             * @param {?} resolve
             * @param {?} _
             * @return {?}
             */
            function (resolve, _) {
                /** @type {?} */
                var result = { code: 0, errmsg: "请求失败", msg: {} };
                _this.httpClient.post(url, para, httpOptions)
                    .pipe(operators.debounceTime(debounce), operators.distinctUntilChanged(), 
                // 接收其他拦截器后产生的异常消息
                operators.catchError((/**
                 * @param {?} error
                 * @return {?}
                 */
                function (error) {
                    console.log("调用失败", error);
                    resolve({ code: -100, errmsg: "请求失败" });
                    return error;
                })))
                    .subscribe((/**
                 * @param {?} resdata
                 * @return {?}
                 */
                function (resdata) {
                    // setting language data
                    // console.log(resdata)
                    result = resdata;
                }), (/**
                 * @return {?}
                 */
                function () { }), (/**
                 * @return {?}
                 */
                function () { resolve(result); }));
            }));
        };
        /**
         * 文件上传类服务
         *   只支持单文件上传
         * @param sname 服务名
         * @param item 文件上传信息
         * @param para 参数
         */
        /**
         * 文件上传类服务
         *   只支持单文件上传
         * @param {?} sname 服务名
         * @param {?} item 文件上传信息
         * @param {?} para 参数
         * @return {?}
         */
        HttpService.prototype.lbfileupload = /**
         * 文件上传类服务
         *   只支持单文件上传
         * @param {?} sname 服务名
         * @param {?} item 文件上传信息
         * @param {?} para 参数
         * @return {?}
         */
        function (sname, item, para) {
            /** @type {?} */
            var formData = new FormData();
            formData.append('file', (/** @type {?} */ (item.file)));
            formData.append('paras', JSON.stringify({ para: para }));
            /** @type {?} */
            var req = new http.HttpRequest('POST', item.action ? item.action : this.serviceInfo.uploadurl, formData, {
                headers: new http.HttpHeaders({
                    'SNAME': sname
                }),
                reportProgress: true,
                withCredentials: true
            });
            // 始终返回一个 `Subscription` 对象，nz-upload 会在适当时机自动取消订阅
            return this.httpClient.request(req).subscribe((/**
             * @param {?} event
             * @return {?}
             */
            function (event) {
                if (event.type === http.HttpEventType.UploadProgress) {
                    if (event.total > 0) {
                        // tslint:disable-next-line:no-any
                        ((/** @type {?} */ (event))).percent = event.loaded / event.total * 100;
                    }
                    // 处理上传进度条，必须指定 `percent` 属性来表示进度
                    item.onProgress(event, item.file);
                }
                else if (event instanceof http.HttpResponse) {
                    // 处理成功
                    item.onSuccess(event.body, item.file, event);
                }
            }), (/**
             * @param {?} err
             * @return {?}
             */
            function (err) {
                // 处理失败
                item.onError(err, item.file);
            }));
        };
        HttpService.decorators = [
            { type: core.Injectable, args: [{ providedIn: 'root', },] }
        ];
        /** @nocollapse */
        HttpService.ctorParameters = function () { return [
            { type: http.HttpClient },
            { type: ServiceInfo }
        ]; };
        /** @nocollapse */ HttpService.ngInjectableDef = core.ɵɵdefineInjectable({ factory: function HttpService_Factory() { return new HttpService(core.ɵɵinject(http.HttpClient), core.ɵɵinject(ServiceInfo)); }, token: HttpService, providedIn: "root" });
        return HttpService;
    }());
    if (false) {
        /**
         * @type {?}
         * @private
         */
        HttpService.prototype.httpClient;
        /**
         * @type {?}
         * @private
         */
        HttpService.prototype.serviceInfo;
    }

    /**
     * @fileoverview added by tsickle
     * Generated from: lib/lblibs/SupDic.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var SupDic = /** @class */ (function () {
        function SupDic(lbservice, httpclient) {
            this.lbservice = lbservice;
            this.httpclient = httpclient;
            this.dicCache = new Object();
            this.ka010203 = {};
        }
        /**
         * 获取字典信息,返回数组
         *  @param dicName 字典值
         *  @returns 字典信息，未取到返回[]
         */
        /**
         * 获取字典信息,返回数组
         * @param {?} dicName 字典值
         * @return {?} 字典信息，未取到返回[]
         */
        SupDic.prototype.getDic = /**
         * 获取字典信息,返回数组
         * @param {?} dicName 字典值
         * @return {?} 字典信息，未取到返回[]
         */
        function (dicName) {
            if (this.dicCache && this.dicCache.hasOwnProperty(dicName)) {
                return this.dicCache[dicName].diclist;
            }
            return [];
        };
        /**
         * 获取字典信息,返回object
         *  @param dicName 字典值
         *  @returns 字典信息
         */
        /**
         * 获取字典信息,返回object
         * @param {?} dicName 字典值
         * @return {?} 字典信息
         */
        SupDic.prototype.getDicObj = /**
         * 获取字典信息,返回object
         * @param {?} dicName 字典值
         * @return {?} 字典信息
         */
        function (dicName) {
            if (this.dicCache && this.dicCache.hasOwnProperty(dicName)) {
                return this.dicCache[dicName].dicobj;
            }
        };
        /**
         * 获取指定字典指定值对应的标签值
         * @param key 字典名称
         * @param val 值
         */
        /**
         * 获取指定字典指定值对应的标签值
         * @param {?} key 字典名称
         * @param {?} val 值
         * @return {?}
         */
        SupDic.prototype.getdicLabel = /**
         * 获取指定字典指定值对应的标签值
         * @param {?} key 字典名称
         * @param {?} val 值
         * @return {?}
         */
        function (key, val) {
            /** @type {?} */
            var dic = this.getDicObj(key);
            if (dic && dic.hasOwnProperty(val)) {
                return dic[val].CNAME;
            }
            return val;
        };
        /**
         * 异步方式获取字典信息，返回SF的字典数组
         *   始终从数据库中获取，获取的字典将会写入到缓存中
         * @param dicName 字典名称
         */
        /**
         * 异步方式获取字典信息，返回SF的字典数组
         *   始终从数据库中获取，获取的字典将会写入到缓存中
         * @param {?} dicName 字典名称
         * @return {?}
         */
        SupDic.prototype.getSFDicAsync = /**
         * 异步方式获取字典信息，返回SF的字典数组
         *   始终从数据库中获取，获取的字典将会写入到缓存中
         * @param {?} dicName 字典名称
         * @return {?}
         */
        function (dicName) {
            // const dic = this.getSFDic(dicName);
            // if (dic.length > 0) {
            //   return of(dic);
            // }
            // const dic = this.getSFDic(dicName);
            // if (dic.length > 0) {
            //   return of(dic);
            // }
            /** @type {?} */
            var aaa = this.lbservice;
            /** @type {?} */
            var dicCache = this.dicCache;
            return new rxjs.Observable((/**
             * @param {?} observer
             * @return {?}
             */
            function subscribe(observer) {
                // FR_GetDIC
                aaa.lbservice('FR_GetAllDIC', {}).then((/**
                 * @param {?} resdata
                 * @return {?}
                 */
                function (resdata) {
                    var e_1, _a;
                    if (resdata.code > 0) {
                        /** @type {?} */
                        var dicobj = new Object();
                        /** @type {?} */
                        var dicarr = resdata.message.list;
                        /** @type {?} */
                        var reslist = [];
                        try {
                            for (var dicarr_1 = __values(dicarr), dicarr_1_1 = dicarr_1.next(); !dicarr_1_1.done; dicarr_1_1 = dicarr_1.next()) {
                                var el = dicarr_1_1.value;
                                el.text = el.CNAME;
                                el.value = el.CCODE;
                                dicobj[el.CCODE] = el;
                                reslist.push(el);
                            }
                        }
                        catch (e_1_1) { e_1 = { error: e_1_1 }; }
                        finally {
                            try {
                                if (dicarr_1_1 && !dicarr_1_1.done && (_a = dicarr_1.return)) _a.call(dicarr_1);
                            }
                            finally { if (e_1) throw e_1.error; }
                        }
                        dicCache[dicName] = { diclist: dicarr, dicobj: dicobj };
                        observer.next(reslist);
                        observer.complete();
                    }
                }));
            }));
        };
        /**
         * 获取SF格式的字典信息
         * @param dicName 字典值
         */
        /**
         * 获取SF格式的字典信息
         * @param {?} dicName 字典值
         * @return {?}
         */
        SupDic.prototype.getSFDic = /**
         * 获取SF格式的字典信息
         * @param {?} dicName 字典值
         * @return {?}
         */
        function (dicName) {
            var e_2, _a;
            /** @type {?} */
            var reslist = [];
            /** @type {?} */
            var diclist = this.getDic(dicName);
            try {
                for (var diclist_1 = __values(diclist), diclist_1_1 = diclist_1.next(); !diclist_1_1.done; diclist_1_1 = diclist_1.next()) {
                    var dic = diclist_1_1.value;
                    reslist.push({ label: dic.CNAME, value: dic.CCODE });
                }
            }
            catch (e_2_1) { e_2 = { error: e_2_1 }; }
            finally {
                try {
                    if (diclist_1_1 && !diclist_1_1.done && (_a = diclist_1.return)) _a.call(diclist_1);
                }
                finally { if (e_2) throw e_2.error; }
            }
            return reslist;
        };
        /**
         * 加载所有字典
         */
        /**
         * 加载所有字典
         * @return {?}
         */
        SupDic.prototype.loadAllDic = /**
         * 加载所有字典
         * @return {?}
         */
        function () {
            return __awaiter(this, void 0, void 0, function () {
                var _this = this;
                return __generator(this, function (_a) {
                    // FR_GetAllDIC
                    return [2 /*return*/, new Promise((/**
                         * @param {?} resolve
                         * @param {?} _
                         * @return {?}
                         */
                        function (resolve, _) {
                            _this.lbservice.lbservice('FR_GetAllDIC', {}).then((/**
                             * @param {?} resdata
                             * @return {?}
                             */
                            function (resdata) {
                                var e_3, _a;
                                if (resdata.code > 0) {
                                    // this.dicCache=resdata.message;
                                    for (var d in resdata.message) {
                                        if (resdata.message.hasOwnProperty(d)) {
                                            /** @type {?} */
                                            var a = new Object();
                                            /** @type {?} */
                                            var dicarr = resdata.message[d];
                                            try {
                                                for (var dicarr_2 = (e_3 = void 0, __values(dicarr)), dicarr_2_1 = dicarr_2.next(); !dicarr_2_1.done; dicarr_2_1 = dicarr_2.next()) {
                                                    var el = dicarr_2_1.value;
                                                    el.text = el.CNAME;
                                                    el.value = el.CCODE;
                                                    a[el.CCODE] = el;
                                                }
                                            }
                                            catch (e_3_1) { e_3 = { error: e_3_1 }; }
                                            finally {
                                                try {
                                                    if (dicarr_2_1 && !dicarr_2_1.done && (_a = dicarr_2.return)) _a.call(dicarr_2);
                                                }
                                                finally { if (e_3) throw e_3.error; }
                                            }
                                            /** @type {?} */
                                            var dic = { diclist: dicarr, dicobj: a };
                                            _this.dicCache[d] = dic;
                                        }
                                    }
                                }
                                resolve();
                            }));
                        }))];
                });
            });
        };
        SupDic.decorators = [
            { type: core.Injectable, args: [{ providedIn: 'root' },] }
        ];
        /** @nocollapse */
        SupDic.ctorParameters = function () { return [
            { type: HttpService },
            { type: http.HttpClient }
        ]; };
        /** @nocollapse */ SupDic.ngInjectableDef = core.ɵɵdefineInjectable({ factory: function SupDic_Factory() { return new SupDic(core.ɵɵinject(HttpService), core.ɵɵinject(http.HttpClient)); }, token: SupDic, providedIn: "root" });
        return SupDic;
    }());
    if (false) {
        /**
         * @type {?}
         * @private
         */
        SupDic.prototype.dicCache;
        /** @type {?} */
        SupDic.prototype.ka010203;
        /**
         * @type {?}
         * @private
         */
        SupDic.prototype.lbservice;
        /**
         * @type {?}
         * @private
         */
        SupDic.prototype.httpclient;
    }

    /**
     * @fileoverview added by tsickle
     * Generated from: lib/lblibs/DicPipe.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    /*
    * 字典显示管道
    *   10|dicpipe:aae140
    */
    var DicPipe = /** @class */ (function () {
        function DicPipe(supdic) {
            this.supdic = supdic;
        }
        /**
         * @param {?} value
         * @param {?} dicname
         * @return {?}
         */
        DicPipe.prototype.transform = /**
         * @param {?} value
         * @param {?} dicname
         * @return {?}
         */
        function (value, dicname) {
            return value ? dicname ? this.supdic.getdicLabel(dicname.toUpperCase(), value) : value : value;
        };
        DicPipe.decorators = [
            { type: core.Pipe, args: [{ name: 'dicpipe' },] }
        ];
        /** @nocollapse */
        DicPipe.ctorParameters = function () { return [
            { type: SupDic }
        ]; };
        return DicPipe;
    }());
    if (false) {
        /**
         * @type {?}
         * @private
         */
        DicPipe.prototype.supdic;
    }

    /**
     * @fileoverview added by tsickle
     * Generated from: lib/lblibs/SupExcel.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var SupExcel = /** @class */ (function () {
        function SupExcel(lbservice, xlsx) {
            this.lbservice = lbservice;
            this.xlsx = xlsx;
        }
        /**
         * 导出excel
         * @param columns 字段信息
         * @param sname 查询服务名
         * @param para 查询服务参数
         * @param total 总条数
         * @param onetime 单次请求数，默认20
         */
        /**
         * 导出excel
         * @param {?} columns 字段信息
         * @param {?} sname 查询服务名
         * @param {?} para 查询服务参数
         * @param {?} total 总条数
         * @param {?=} onetime 单次请求数，默认20
         * @return {?}
         */
        SupExcel.prototype.export = /**
         * 导出excel
         * @param {?} columns 字段信息
         * @param {?} sname 查询服务名
         * @param {?} para 查询服务参数
         * @param {?} total 总条数
         * @param {?=} onetime 单次请求数，默认20
         * @return {?}
         */
        function (columns, sname, para, total, onetime) {
            var _this = this;
            if (onetime === void 0) { onetime = 20; }
            /** @type {?} */
            var data = [columns.map((/**
                 * @param {?} i
                 * @return {?}
                 */
                function (i) { return i.title; }))];
            /** @type {?} */
            var pageCount = Math.ceil(total / 100);
            // 请求信息
            /** @type {?} */
            var reqtime = Array(pageCount).fill({}).map((/**
             * @param {?} _item
             * @param {?} idx
             * @return {?}
             */
            function (_item, idx) {
                return {
                    PAGEIDX: idx,
                    PAGESIZE: 100,
                };
            }));
            /** @type {?} */
            var obs = [];
            // 把20次的请求合一起
            reqtime.forEach((/**
             * @param {?} page
             * @return {?}
             */
            function (page) {
                obs.push(_this.lbservice.lbservice2(sname, { para: para, page: page }));
            }));
            /** @type {?} */
            var allcall = rxjs.forkJoin(obs);
            return new Promise((/**
             * @param {?} resolve
             * @param {?} _
             * @return {?}
             */
            function (resolve, _) {
                allcall.subscribe((/**
                 * @param {?} results
                 * @return {?}
                 */
                function (results) {
                    results.forEach((/**
                     * @param {?} item
                     * @return {?}
                     */
                    function (item) {
                        if (item.code > 0) {
                            item.message.list.forEach((/**
                             * @param {?} i
                             * @return {?}
                             */
                            function (i) {
                                return data.push(columns.map((/**
                                 * @param {?} c
                                 * @return {?}
                                 */
                                function (c) { return i[(/** @type {?} */ (c.index))]; })));
                            }));
                        }
                    }));
                    _this.xlsx.export({
                        sheets: [
                            {
                                data: data,
                                name: 'sheet1',
                            },
                        ],
                    }).then((/**
                     * @return {?}
                     */
                    function () {
                        resolve(null);
                    }));
                }));
            }));
        };
        SupExcel.decorators = [
            { type: core.Injectable, args: [{ providedIn: 'root' },] }
        ];
        /** @nocollapse */
        SupExcel.ctorParameters = function () { return [
            { type: HttpService },
            { type: abc.XlsxService }
        ]; };
        /** @nocollapse */ SupExcel.ngInjectableDef = core.ɵɵdefineInjectable({ factory: function SupExcel_Factory() { return new SupExcel(core.ɵɵinject(HttpService), core.ɵɵinject(xlsx.XlsxService)); }, token: SupExcel, providedIn: "root" });
        return SupExcel;
    }());
    if (false) {
        /**
         * @type {?}
         * @private
         */
        SupExcel.prototype.lbservice;
        /**
         * @type {?}
         * @private
         */
        SupExcel.prototype.xlsx;
    }

    /**
     * @fileoverview added by tsickle
     * Generated from: lib/components/grid/LbColumn.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var LbRowSource = /** @class */ (function () {
        function LbRowSource() {
            this.titles = {};
            this.rows = {};
        }
        /**
         * @param {?} type
         * @param {?} path
         * @param {?} ref
         * @return {?}
         */
        LbRowSource.prototype.add = /**
         * @param {?} type
         * @param {?} path
         * @param {?} ref
         * @return {?}
         */
        function (type, path, ref) {
            this[type === 'title' ? 'titles' : 'rows'][path] = ref;
        };
        /**
         * @param {?} path
         * @return {?}
         */
        LbRowSource.prototype.getTitle = /**
         * @param {?} path
         * @return {?}
         */
        function (path) {
            return this.titles[path];
        };
        /**
         * @param {?} path
         * @return {?}
         */
        LbRowSource.prototype.getRow = /**
         * @param {?} path
         * @return {?}
         */
        function (path) {
            return this.rows[path];
        };
        LbRowSource.decorators = [
            { type: core.Injectable }
        ];
        /** @nocollapse */
        LbRowSource.ctorParameters = function () { return []; };
        return LbRowSource;
    }());
    if (false) {
        /**
         * @type {?}
         * @private
         */
        LbRowSource.prototype.titles;
        /**
         * @type {?}
         * @private
         */
        LbRowSource.prototype.rows;
    }
    var LbRowDirective = /** @class */ (function () {
        function LbRowDirective(ref, source) {
            this.ref = ref;
            this.source = source;
            console.log('lb-row constructor');
        }
        /**
         * @return {?}
         */
        LbRowDirective.prototype.ngOnInit = /**
         * @return {?}
         */
        function () {
            this.source.add(this.type, this.id, this.ref);
            console.log(this.id, this.ref);
        };
        LbRowDirective.decorators = [
            { type: core.Directive, args: [{ selector: '[lb-row]' },] }
        ];
        /** @nocollapse */
        LbRowDirective.ctorParameters = function () { return [
            { type: core.TemplateRef },
            { type: LbRowSource, decorators: [{ type: core.Host }] }
        ]; };
        LbRowDirective.propDecorators = {
            id: [{ type: core.Input, args: ['lb-row',] }],
            type: [{ type: core.Input }]
        };
        return LbRowDirective;
    }());
    if (false) {
        /** @type {?} */
        LbRowDirective.prototype.id;
        /** @type {?} */
        LbRowDirective.prototype.type;
        /**
         * @type {?}
         * @private
         */
        LbRowDirective.prototype.ref;
        /**
         * @type {?}
         * @private
         */
        LbRowDirective.prototype.source;
    }

    /**
     * @fileoverview added by tsickle
     * Generated from: lib/components/grid/grid.component.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var GridComponent = /** @class */ (function () {
        function GridComponent(supdic, supexcel, sinfo, lbSource) {
            this.supdic = supdic;
            this.supexcel = supexcel;
            this.sinfo = sinfo;
            this.lbSource = lbSource;
            this.grhxurl = ['/ybjg/grhx'];
            this.isGrhx = true;
            /**
             * 每页数量，当设置为 0 表示不分页，默认：10
             */
            this.ps = 10;
            this.datas = 'MohrssExInterface/gridsvr';
            this.statisticals = [];
            this._statcal = {};
            // 是否加载中
            this.loading = false;
            this.loadingChange = new core.EventEmitter();
            // 服务ID
            this._sname = '';
            this.reqparas = { params: { sname: this._sname, form: this._queryparas, stacal: this._statcal }, method: 'post', allInBody: true };
            this.process = (/**
             * @param {?} data
             * @return {?}
             */
            function (data) {
                return data;
            });
        }
        Object.defineProperty(GridComponent.prototype, "data", {
            /**
             * 表格数值
             */
            get: /**
             * 表格数值
             * @return {?}
             */
            function () {
                return this.st._data;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(GridComponent.prototype, "innerSt", {
            get: /**
             * @return {?}
             */
            function () {
                return this.st;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(GridComponent.prototype, "columns", {
            get: /**
             * @return {?}
             */
            function () {
                return this._columns;
            },
            set: /**
             * @param {?} columns
             * @return {?}
             */
            function (columns) {
                var _this = this;
                // 统计类信息
                this.statisticals = [];
                columns.forEach((/**
                 * @param {?} col
                 * @return {?}
                 */
                function (col) {
                    if (col.render) {
                        col.render2 = col.render;
                        col.render = '';
                    }
                    else {
                        if (_this.isGrhx && col.index === 'AAC003' && _this.sinfo.isAdmin) {
                            col.render = 'grhx';
                        }
                    }
                    if (col.renderTitle) {
                        col.renderTitle2 = col.renderTitle;
                        col.renderTitle = '';
                    }
                    /** @type {?} */
                    var key = col.index;
                    if (key instanceof Array) {
                        key = key[0];
                    }
                    col.key = key;
                    // 添加字典信息
                    if (col.dic) {
                        col.format = (/**
                         * @param {?} a
                         * @param {?} b
                         * @return {?}
                         */
                        function (a, b) { return _this.supdic.getdicLabel(b.dic, a[b.key]); });
                        col.filter = { menus: _this.supdic.getDic(col.dic) };
                    }
                    // 添加统计类型信息
                    /** @type {?} */
                    var val = col.statistical;
                    if (!val) {
                        _this.statisticals.push({ key: key, show: false, type: "" });
                        _this._statcal[key] = "";
                        return;
                    }
                    /** @type {?} */
                    var item = __assign({ digits: 2, currency: null }, (typeof val === 'string'
                        ? { type: (/** @type {?} */ (val)) }
                        : ((/** @type {?} */ (val)))));
                    /** @type {?} */
                    var stcal = item.type;
                    item.digits = stcal === "count" ? 0 : 2;
                    item.type = _this.customStatistical;
                    item.currency = false;
                    col.statistical = item;
                    _this.statisticals.push({ key: key, show: true, type: stcal });
                    _this._statcal[key] = stcal;
                }));
                this._columns = columns;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(GridComponent.prototype, "sname", {
            set: /**
             * @param {?} sname
             * @return {?}
             */
            function (sname) {
                this._sname = sname;
                this.reqparas.params.sname = this._sname;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(GridComponent.prototype, "queryparas", {
            // 查询参数
            get: 
            // 查询参数
            /**
             * @return {?}
             */
            function () {
                return this._queryparas;
            },
            set: /**
             * @param {?} value
             * @return {?}
             */
            function (value) {
                this._queryparas = value;
                this.reqparas.params.form = this._queryparas;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(GridComponent.prototype, "loadingIndicator", {
            /**
             * 加载指示符
             */
            set: /**
             * 加载指示符
             * @param {?} loadingIndicator
             * @return {?}
             */
            function (loadingIndicator) {
                this.st.loadingIndicator = loadingIndicator;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(GridComponent.prototype, "loadingDelay", {
            /**
             * 延迟显示加载效果的时间（防止闪烁）
             */
            set: /**
             * 延迟显示加载效果的时间（防止闪烁）
             * @param {?} loadingDelay
             * @return {?}
             */
            function (loadingDelay) {
                this.st.loadingDelay = loadingDelay;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(GridComponent.prototype, "pi", {
            get: /**
             * @return {?}
             */
            function () {
                return this.st.pi;
            },
            /**
             * 当前页码
             */
            set: /**
             * 当前页码
             * @param {?} pi
             * @return {?}
             */
            function (pi) {
                this.st.pi = pi;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(GridComponent.prototype, "page", {
            get: /**
             * @return {?}
             */
            function () {
                return this.st.page;
            },
            /**
             *  分页器配置
             */
            set: /**
             *  分页器配置
             * @param {?} page
             * @return {?}
             */
            function (page) {
                this.st.page = page;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(GridComponent.prototype, "noResult", {
            get: /**
             * @return {?}
             */
            function () {
                return this.st.noResult;
            },
            /**
             * 无数据时显示内容
             */
            set: /**
             * 无数据时显示内容
             * @param {?} noResult
             * @return {?}
             */
            function (noResult) {
                this.st.noResult = noResult;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(GridComponent.prototype, "bordered", {
            get: /**
             * @return {?}
             */
            function () {
                return this.st.bordered;
            },
            /**
             * 是否显示边框
             */
            set: /**
             * 是否显示边框
             * @param {?} bordered
             * @return {?}
             */
            function (bordered) {
                this.st.bordered = bordered;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(GridComponent.prototype, "header", {
            /**
             * 表格标题
             */
            set: /**
             * 表格标题
             * @param {?} header
             * @return {?}
             */
            function (header) {
                this.st.header = header;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(GridComponent.prototype, "footer", {
            /**
             * 表格底部
             */
            set: /**
             * 表格底部
             * @param {?} footer
             * @return {?}
             */
            function (footer) {
                this.st.footer = footer;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(GridComponent.prototype, "bodyHeader", {
            /**
             * 表格顶部额外内容，一般用于添加合计行
             */
            set: /**
             * 表格顶部额外内容，一般用于添加合计行
             * @param {?} bodyHeader
             * @return {?}
             */
            function (bodyHeader) {
                this.st.bodyHeader = bodyHeader;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(GridComponent.prototype, "expandRowByClick", {
            set: /**
             * @param {?} expandRowByClick
             * @return {?}
             */
            function (expandRowByClick) {
                this.st.expandRowByClick = expandRowByClick;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(GridComponent.prototype, "expandAccordion", {
            set: /**
             * @param {?} expandAccordion
             * @return {?}
             */
            function (expandAccordion) {
                this.st.expandAccordion = expandAccordion;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(GridComponent.prototype, "expand", {
            set: /**
             * @param {?} expand
             * @return {?}
             */
            function (expand) {
                this.st.expand = expand;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(GridComponent.prototype, "filteredData", {
            get: /**
             * @return {?}
             */
            function () {
                return this.st.filteredData;
            },
            enumerable: true,
            configurable: true
        });
        /**
         * @return {?}
         */
        GridComponent.prototype.ngOnInit = /**
         * @return {?}
         */
        function () {
            var _this = this;
            setTimeout((/**
             * @return {?}
             */
            function () {
                _this.columns.forEach((/**
                 * @param {?} col
                 * @return {?}
                 */
                function (col) {
                    _this.restoreRender(col);
                }));
                _this.st.resetColumns();
            }), 100);
        };
        /**
         * @param {?=} options
         * @return {?}
         */
        GridComponent.prototype.resetColumns = /**
         * @param {?=} options
         * @return {?}
         */
        function (options) {
            this.st.resetColumns(options);
        };
        /**
         * 移除行
         * @param data STData | STData[] | number
         */
        /**
         * 移除行
         * @param {?} data STData | STData[] | number
         * @return {?}
         */
        GridComponent.prototype.removeRow = /**
         * 移除行
         * @param {?} data STData | STData[] | number
         * @return {?}
         */
        function (data) {
            this.st.removeRow(data);
        };
        /**
         * 清空所有数据
         */
        /**
         * 清空所有数据
         * @return {?}
         */
        GridComponent.prototype.clear = /**
         * 清空所有数据
         * @return {?}
         */
        function () {
            this.st.clear();
        };
        /**
         * @param {?=} queryparas
         * @return {?}
         */
        GridComponent.prototype.reload = /**
         * @param {?=} queryparas
         * @return {?}
         */
        function (queryparas) {
            this.st.loading = true;
            if (queryparas)
                this.st.req.params.form = queryparas;
            this.st.reload();
            this.st.cd();
        };
        /**
         * @param {?} index
         * @param {?} data
         * @return {?}
         */
        GridComponent.prototype.setRow = /**
         * @param {?} index
         * @param {?} data
         * @return {?}
         */
        function (index, data) {
            this.st.setRow(index, data);
        };
        /**
         * @param {?} event
         * @return {?}
         */
        GridComponent.prototype.reset = /**
         * @param {?} event
         * @return {?}
         */
        function (event) {
            this.loading = true;
            this.loadingChange.emit(true);
            this.st.reset(event);
            this.st.cd();
        };
        /**
         * @private
         * @param {?} values
         * @param {?} col
         * @param {?} list
         * @param {?=} rawData
         * @return {?}
         */
        GridComponent.prototype.customStatistical = /**
         * @private
         * @param {?} values
         * @param {?} col
         * @param {?} list
         * @param {?=} rawData
         * @return {?}
         */
        function (values, col, list, rawData) {
            if (rawData.code < 1)
                return { value: "", text: "" };
            if (rawData.page.statistical) {
                /** @type {?} */
                var key = col.index;
                if (key instanceof Array) {
                    key = key[0];
                }
                if (rawData.page.statistical[key]) {
                    return { value: rawData.page.statistical[key], text: "" };
                }
                return { value: "", text: "" };
            }
            return { value: "", text: "" };
        };
        // 导出excel
        // 导出excel
        /**
         * @param {?=} opt
         * @return {?}
         */
        GridComponent.prototype.export = 
        // 导出excel
        /**
         * @param {?=} opt
         * @return {?}
         */
        function (opt) {
            /** @type {?} */
            var data = this.st._data;
            this.st.export(data, opt);
        };
        /**
         * @return {?}
         */
        GridComponent.prototype.exportAll = /**
         * @return {?}
         */
        function () {
            var _this = this;
            this.loading = true;
            this.supexcel.export(this.columns, this._sname, this.st.req.params.form, this.st.total).then((/**
             * @return {?}
             */
            function () {
                _this.loading = false;
            }));
        };
        /**
         * @private
         * @param {?} item
         * @return {?}
         */
        GridComponent.prototype.restoreRender = /**
         * @private
         * @param {?} item
         * @return {?}
         */
        function (item) {
            if (item.renderTitle2) {
                item.__renderTitle = this.lbSource.getTitle(item.renderTitle2);
            }
            if (item.render2) {
                item.__render = this.lbSource.getRow(item.render2);
            }
        };
        GridComponent.decorators = [
            { type: core.Component, args: [{
                        selector: 'lb-grid',
                        template: "<st #st [data]=\"datas\" [req]=\"reqparas\" [loading]=\"loading\" [body]=\"bodyTpl\" [scroll]=\"scroll\" multiSort [ps]=\"ps\"\n    [res]=\"{reName :{list:'message.list',total:'page.COUNT',process: dataProcess}}\" [columns]=\"columns\">\n\n    <ng-template st-row=\"grhx\" let-item let-c=\"column\">\n        <a [routerLink]=\"grhxurl\" [queryParams]=\"item\">{{item[c.indexKey]}}</a>\n    </ng-template>\n\n    <ng-template #bodyTpl let-s>\n        <tr class=\"bg-grey-lighter\">\n            <ng-container *ngFor=\"let statis of statisticals\">\n                <td>\n                    <div *ngIf=\"statis.show&&s[statis.key]\">\n                        {{ s[statis.key].text}}\n                    </div>\n                </td>\n            </ng-container>\n        </tr>\n    </ng-template>\n</st>",
                        providers: [LbRowSource],
                        styles: [""]
                    }] }
        ];
        /** @nocollapse */
        GridComponent.ctorParameters = function () { return [
            { type: SupDic },
            { type: SupExcel },
            { type: ServiceInfo },
            { type: LbRowSource }
        ]; };
        GridComponent.propDecorators = {
            grhxurl: [{ type: core.Input }],
            isGrhx: [{ type: core.Input }],
            columns: [{ type: core.Input, args: ['columns',] }],
            sname: [{ type: core.Input, args: ['sname',] }],
            queryparas: [{ type: core.Input }],
            ps: [{ type: core.Input }],
            loading: [{ type: core.Input }],
            loadingChange: [{ type: core.Output }],
            loadingIndicator: [{ type: core.Input }],
            loadingDelay: [{ type: core.Input }],
            scroll: [{ type: core.Input }],
            pi: [{ type: core.Input }],
            noResult: [{ type: core.Input }],
            bordered: [{ type: core.Input }],
            header: [{ type: core.Input }],
            footer: [{ type: core.Input }],
            bodyHeader: [{ type: core.Input }],
            expandRowByClick: [{ type: core.Input }],
            expandAccordion: [{ type: core.Input }],
            expand: [{ type: core.Input }],
            st: [{ type: core.ViewChild, args: ['st', { static: true },] }]
        };
        __decorate([
            ngZorroAntd.InputBoolean(),
            __metadata("design:type", Object)
        ], GridComponent.prototype, "isGrhx", void 0);
        __decorate([
            ngZorroAntd.InputNumber(),
            __metadata("design:type", Number),
            __metadata("design:paramtypes", [Number])
        ], GridComponent.prototype, "loadingDelay", null);
        __decorate([
            ngZorroAntd.InputBoolean(),
            __metadata("design:type", Boolean),
            __metadata("design:paramtypes", [Boolean])
        ], GridComponent.prototype, "bordered", null);
        __decorate([
            ngZorroAntd.InputBoolean(),
            __metadata("design:type", Boolean),
            __metadata("design:paramtypes", [Boolean])
        ], GridComponent.prototype, "expandRowByClick", null);
        __decorate([
            ngZorroAntd.InputBoolean(),
            __metadata("design:type", Boolean),
            __metadata("design:paramtypes", [Boolean])
        ], GridComponent.prototype, "expandAccordion", null);
        __decorate([
            ngZorroAntd.InputBoolean(),
            __metadata("design:type", core.TemplateRef),
            __metadata("design:paramtypes", [core.TemplateRef])
        ], GridComponent.prototype, "expand", null);
        return GridComponent;
    }());
    if (false) {
        /** @type {?} */
        GridComponent.prototype.grhxurl;
        /** @type {?} */
        GridComponent.prototype.isGrhx;
        /**
         * 每页数量，当设置为 0 表示不分页，默认：10
         * @type {?}
         */
        GridComponent.prototype.ps;
        /** @type {?} */
        GridComponent.prototype.datas;
        /** @type {?} */
        GridComponent.prototype.statisticals;
        /** @type {?} */
        GridComponent.prototype._statcal;
        /** @type {?} */
        GridComponent.prototype.loading;
        /** @type {?} */
        GridComponent.prototype.loadingChange;
        /**
         * 横向或纵向支持滚动，也可用于指定滚动区域的宽高度：{ x: "300px", y: "300px" }
         * @type {?}
         */
        GridComponent.prototype.scroll;
        /** @type {?} */
        GridComponent.prototype._columns;
        /** @type {?} */
        GridComponent.prototype._sname;
        /** @type {?} */
        GridComponent.prototype._queryparas;
        /** @type {?} */
        GridComponent.prototype.st;
        /** @type {?} */
        GridComponent.prototype.reqparas;
        /** @type {?} */
        GridComponent.prototype.dataProcess;
        /**
         * @type {?}
         * @private
         */
        GridComponent.prototype.process;
        /**
         * @type {?}
         * @private
         */
        GridComponent.prototype.supdic;
        /**
         * @type {?}
         * @private
         */
        GridComponent.prototype.supexcel;
        /**
         * @type {?}
         * @private
         */
        GridComponent.prototype.sinfo;
        /**
         * @type {?}
         * @private
         */
        GridComponent.prototype.lbSource;
    }

    /**
     * @fileoverview added by tsickle
     * Generated from: lib/components/img-upload/img-upload.component.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var ImgUploadComponent = /** @class */ (function () {
        function ImgUploadComponent(msgsvr, lbservice, modalService) {
            var _this = this;
            this.msgsvr = msgsvr;
            this.lbservice = lbservice;
            this.modalService = modalService;
            /**
             * 图表列表
             */
            this.fileList = [];
            this._previewImage = '';
            this._previewVisible = false;
            /**
             * 上传按钮是否显示
             */
            this.nzShowButton = true;
            /**
             * 预览按钮以及删除按钮
             */
            this.showUploadList = {
                showPreviewIcon: true,
                showRemoveIcon: true,
                hidePreviewIconInNonImage: true
            };
            /**
             * 上传事件
             */
            this.upload = (/**
             * @param {?} item
             * @return {?}
             */
            function (item) { return rxjs.Subscription; });
            /**
             * 删除事件
             */
            this.del = (/**
             * @param {?} file
             * @return {?}
             */
            function (file) { return new Promise((/**
             * @return {?}
             */
            function () { })); });
            this._handlePreview = (/**
             * @param {?} file
             * @return {?}
             */
            function (file) {
                _this._previewImage = file.url || file.thumbUrl;
                _this._previewVisible = true;
            });
            /**
             * 图片上传
             */
            this._uploadimg = (/**
             * @param {?} item
             * @return {?}
             */
            function (item) {
                if (_this.upload) {
                    return _this.upload(item);
                }
            });
            /**
             * 移除文件
             */
            this._removeIMG = (/**
             * @param {?} file
             * @return {?}
             */
            function (file) {
                return new rxjs.Observable((/**
                 * @param {?} obs
                 * @return {?}
                 */
                function (obs) {
                    _this.modalService.confirm({
                        nzTitle: '删除确认',
                        nzContent: '<b style="color: red;">确认要删除这张图片么？</b>',
                        nzOkText: '确定',
                        nzOkType: 'danger',
                        nzOnOk: (/**
                         * @return {?}
                         */
                        function () {
                            if (_this.del) {
                                _this.del(file).then((/**
                                 * @param {?} resdata
                                 * @return {?}
                                 */
                                function (resdata) {
                                    if (resdata.code < 1) {
                                        _this.msgsvr.error("图片删除失败:" + resdata.errmsg);
                                        obs.next(false);
                                    }
                                    else {
                                        _this.msgsvr.success('图片删除成功！');
                                        obs.next(true);
                                    }
                                }));
                            }
                        }),
                        nzCancelText: '取消',
                        nzOnCancel: (/**
                         * @return {?}
                         */
                        function () { return obs.next(false); })
                    });
                }));
            });
            this._beforeUpload = (/**
             * @param {?} file
             * @return {?}
             */
            function (file) {
                return new rxjs.Observable((/**
                 * @param {?} observer
                 * @return {?}
                 */
                function (observer) {
                    /** @type {?} */
                    var imgtype = ["image/png", "image/jpeg", "image/gif", "image/bmp"];
                    //  const isImg = file.type === 'image/jpeg';
                    if (!imgtype.includes(file.type)) {
                        _this.msgsvr.error('只能上传图片格式!');
                        observer.complete();
                        return;
                    }
                    observer.next(true);
                    observer.complete();
                }));
            });
        }
        /**
         * @return {?}
         */
        ImgUploadComponent.prototype.ngOnInit = /**
         * @return {?}
         */
        function () {
        };
        /**
         * 上传成功后处理
         * @param info  上传返回信息
         */
        /**
         * 上传成功后处理
         * @param {?} info  上传返回信息
         * @return {?}
         */
        ImgUploadComponent.prototype._handleChange = /**
         * 上传成功后处理
         * @param {?} info  上传返回信息
         * @return {?}
         */
        function (info) {
            /** @type {?} */
            var fileList = info.fileList;
            // 2. read from response and show file link
            if (info.file.response) {
                if (info.file.response.code < 1) {
                    this.msgsvr.error("文件上传失败:" + info.file.response.errmsg);
                }
                else {
                    info.file.ftoken = info.file.response.message.ftoken;
                }
                info.file.url = info.file.response.url;
            }
            // 3. filter successfully uploaded files according to response from server
            this.fileList = fileList.filter((/**
             * @param {?} item
             * @return {?}
             */
            function (item) {
                if (item.response) {
                    if (item.response.code > 0) {
                        item.ftoken = item.response.message.ftoken;
                        return true;
                    }
                    return false;
                }
                return true;
            }));
        };
        ImgUploadComponent.decorators = [
            { type: core.Component, args: [{
                        selector: 'lb-img',
                        template: "<div>\n    <nz-upload nzListType=\"picture-card\" [(nzFileList)]=\"fileList\" [nzCustomRequest]=\"_uploadimg\"\n        [nzRemove]=\"_removeIMG\" [nzBeforeUpload]=\"_beforeUpload\" (nzChange)=\"_handleChange($event)\"\n        [nzShowButton]=\"nzShowButton\" [nzSize]=\"10*1024\" [nzShowUploadList]=\"showUploadList\"\n        [nzPreview]=\"_handlePreview\">\n        <div class=\"ant-upload-text\">\u4E0A\u4F20\u56FE\u7247</div>\n    </nz-upload>\n    <nz-modal [nzVisible]=\"_previewVisible\" [nzContent]=\"modalContent\" [nzFooter]=\"null\"\n        (nzOnCancel)=\"_previewVisible=false\">\n        <ng-template #modalContent>\n            <img [src]=\"_previewImage\" [ngStyle]=\"{ 'width': '100%' }\" />\n        </ng-template>\n    </nz-modal>\n</div>",
                        styles: [""]
                    }] }
        ];
        /** @nocollapse */
        ImgUploadComponent.ctorParameters = function () { return [
            { type: ngZorroAntd.NzMessageService },
            { type: HttpService },
            { type: ngZorroAntd.NzModalService }
        ]; };
        ImgUploadComponent.propDecorators = {
            fileList: [{ type: core.Input }],
            nzShowButton: [{ type: core.Input }],
            showUploadList: [{ type: core.Input }],
            upload: [{ type: core.Input }],
            del: [{ type: core.Input }]
        };
        return ImgUploadComponent;
    }());
    if (false) {
        /**
         * 图表列表
         * @type {?}
         */
        ImgUploadComponent.prototype.fileList;
        /** @type {?} */
        ImgUploadComponent.prototype._previewImage;
        /** @type {?} */
        ImgUploadComponent.prototype._previewVisible;
        /**
         * 上传按钮是否显示
         * @type {?}
         */
        ImgUploadComponent.prototype.nzShowButton;
        /**
         * 预览按钮以及删除按钮
         * @type {?}
         */
        ImgUploadComponent.prototype.showUploadList;
        /**
         * 上传事件
         * @type {?}
         */
        ImgUploadComponent.prototype.upload;
        /**
         * 删除事件
         * @type {?}
         */
        ImgUploadComponent.prototype.del;
        /** @type {?} */
        ImgUploadComponent.prototype._handlePreview;
        /**
         * 图片上传
         * @type {?}
         */
        ImgUploadComponent.prototype._uploadimg;
        /**
         * 移除文件
         * @type {?}
         */
        ImgUploadComponent.prototype._removeIMG;
        /** @type {?} */
        ImgUploadComponent.prototype._beforeUpload;
        /**
         * @type {?}
         * @private
         */
        ImgUploadComponent.prototype.msgsvr;
        /**
         * @type {?}
         * @private
         */
        ImgUploadComponent.prototype.lbservice;
        /**
         * @type {?}
         * @private
         */
        ImgUploadComponent.prototype.modalService;
    }

    /**
     * @fileoverview added by tsickle
     * Generated from: lib/components/popdetail/popdetail.component.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var PopdetailComponent = /** @class */ (function () {
        function PopdetailComponent(msgSrv, lbservice) {
            this.msgSrv = msgSrv;
            this.lbservice = lbservice;
            // 是否弹出
            this.isVisible = false;
            this.isVisibleChange = new core.EventEmitter();
            // 弹出框标题
            this.title = "明细框";
            // 字段信息
            this.schema = {};
            // 默认值
            this.formData = {};
            // 是否加载中
            this.loading = false;
            this.loadingChange = new core.EventEmitter();
            // 保存服务
            this.saveSname = '';
            // formchang事件
            this.formChange = new core.EventEmitter();
        }
        /**
         * @return {?}
         */
        PopdetailComponent.prototype.ngOnInit = /**
         * @return {?}
         */
        function () {
        };
        /**
         * @return {?}
         */
        PopdetailComponent.prototype.handleCancel = /**
         * @return {?}
         */
        function () {
            this.isVisible = false;
            this.isVisibleChange.emit(this.isVisible);
        };
        /**
         * @return {?}
         */
        PopdetailComponent.prototype.show = /**
         * @return {?}
         */
        function () {
            this.isVisible = true;
        };
        /**
         * @return {?}
         */
        PopdetailComponent.prototype.hidde = /**
         * @return {?}
         */
        function () {
            this.isVisible = true;
        };
        /**
         * @return {?}
         */
        PopdetailComponent.prototype.reset = /**
         * @return {?}
         */
        function () {
            this.sf.reset();
        };
        /**
         * @return {?}
         */
        PopdetailComponent.prototype.value = /**
         * @return {?}
         */
        function () {
            return this.sf.value;
        };
        // path 采用 / 来分隔，例如：/user/name
        // path 采用 / 来分隔，例如：/user/name
        /**
         * @param {?} path
         * @return {?}
         */
        PopdetailComponent.prototype.getValue = 
        // path 采用 / 来分隔，例如：/user/name
        /**
         * @param {?} path
         * @return {?}
         */
        function (path) {
            return this.sf.getValue(path);
        };
        /**
         * @param {?} path
         * @param {?} value
         * @return {?}
         */
        PopdetailComponent.prototype.setValue = /**
         * @param {?} path
         * @param {?} value
         * @return {?}
         */
        function (path, value) {
            this.sf.setValue(path, value);
        };
        /**
         * @return {?}
         */
        PopdetailComponent.prototype.validator = /**
         * @return {?}
         */
        function () {
            this.sf.validator();
        };
        /**
         * @param {?=} newSchema
         * @param {?=} newUI
         * @return {?}
         */
        PopdetailComponent.prototype.refreshSchema = /**
         * @param {?=} newSchema
         * @param {?=} newUI
         * @return {?}
         */
        function (newSchema, newUI) {
            this.sf.refreshSchema(newSchema, newUI);
        };
        /**
         * @return {?}
         */
        PopdetailComponent.prototype.getsf = /**
         * @return {?}
         */
        function () {
            return this.sf;
        };
        /**
         * @param {?} val
         * @return {?}
         */
        PopdetailComponent.prototype.save = /**
         * @param {?} val
         * @return {?}
         */
        function (val) {
            var _this = this;
            this.loading = true;
            this.loadingChange.emit(this.loading);
            if (this.saveSname && this.saveSname !== '') {
                this.lbservice.lbservice(this.saveSname, { para: val }, 500).then((/**
                 * @param {?} resdata
                 * @return {?}
                 */
                function (resdata) {
                    _this.loading = false;
                    _this.loadingChange.emit(_this.loading);
                    if (resdata.code < 1) {
                        _this.msgSrv.error(resdata.errmsg);
                        return;
                    }
                    if (_this.saveFu && typeof _this.saveFu === 'function') {
                        _this.saveFu(val, resdata);
                    }
                    _this.isVisible = false;
                    _this.isVisibleChange.emit(_this.isVisible);
                    _this.reset();
                    _this.msgSrv.info("成功！");
                }));
            }
            else {
                if (this.saveFu && typeof this.saveFu === 'function') {
                    this.saveFu(val, {});
                }
            }
        };
        /**
         * @param {?} value
         * @return {?}
         */
        PopdetailComponent.prototype.change = /**
         * @param {?} value
         * @return {?}
         */
        function (value) {
            this.formChange.emit(value);
        };
        PopdetailComponent.decorators = [
            { type: core.Component, args: [{
                        selector: 'lb-pop',
                        template: "<nz-modal [(nzVisible)]=\"isVisible\" [nzTitle]=\"title\" nzWidth=\"80%\" (nzOnCancel)=\"handleCancel()\"\n    (nzOnOk)=\"save(sf.value)\" [nzOkLoading]=\"loading\">\n\n    <sf #sf [schema]=\"schema\" [formData]=\"formData\" (formChange)=\"change($event)\" button=\"none\">\n\n    </sf>\n\n</nz-modal>",
                        styles: [""]
                    }] }
        ];
        /** @nocollapse */
        PopdetailComponent.ctorParameters = function () { return [
            { type: ngZorroAntd.NzMessageService },
            { type: HttpService }
        ]; };
        PopdetailComponent.propDecorators = {
            sf: [{ type: core.ViewChild, args: ['sf', { static: true },] }],
            isVisible: [{ type: core.Input }],
            isVisibleChange: [{ type: core.Output }],
            title: [{ type: core.Input }],
            schema: [{ type: core.Input }],
            formData: [{ type: core.Input }],
            loading: [{ type: core.Input }],
            loadingChange: [{ type: core.Output }],
            saveSname: [{ type: core.Input }],
            saveFu: [{ type: core.Input }],
            formChange: [{ type: core.Output }]
        };
        return PopdetailComponent;
    }());
    if (false) {
        /** @type {?} */
        PopdetailComponent.prototype.sf;
        /** @type {?} */
        PopdetailComponent.prototype.isVisible;
        /** @type {?} */
        PopdetailComponent.prototype.isVisibleChange;
        /** @type {?} */
        PopdetailComponent.prototype.title;
        /** @type {?} */
        PopdetailComponent.prototype.schema;
        /** @type {?} */
        PopdetailComponent.prototype.formData;
        /** @type {?} */
        PopdetailComponent.prototype.loading;
        /** @type {?} */
        PopdetailComponent.prototype.loadingChange;
        /** @type {?} */
        PopdetailComponent.prototype.saveSname;
        /** @type {?} */
        PopdetailComponent.prototype.saveFu;
        /** @type {?} */
        PopdetailComponent.prototype.formChange;
        /** @type {?} */
        PopdetailComponent.prototype.msgSrv;
        /**
         * @type {?}
         * @private
         */
        PopdetailComponent.prototype.lbservice;
    }

    /**
     * @fileoverview added by tsickle
     * Generated from: lib/lbf.module.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    /** @type {?} */
    var LbfComponents = [LbfComponent, GridComponent, DicPipe, ImgUploadComponent, PopdetailComponent, LbRowDirective];
    var LbfModule = /** @class */ (function () {
        function LbfModule() {
        }
        LbfModule.decorators = [
            { type: core.NgModule, args: [{
                        declarations: __spread(LbfComponents),
                        imports: [
                            common.CommonModule,
                            ngZorroAntd.NgZorroAntdModule,
                            router.RouterModule,
                            theme.AlainThemeModule.forRoot(),
                            abc.DelonABCModule,
                            chart.DelonChartModule,
                            acl.DelonACLModule,
                            cache.DelonCacheModule,
                            util.DelonUtilModule,
                            auth.DelonAuthModule,
                            form.DelonFormModule.forRoot(),
                        ],
                        exports: __spread(LbfComponents)
                    },] }
        ];
        return LbfModule;
    }());

    /**
     * @fileoverview added by tsickle
     * Generated from: lib/lbf.service.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var LbfService = /** @class */ (function () {
        function LbfService() {
        }
        LbfService.decorators = [
            { type: core.Injectable, args: [{
                        providedIn: 'root'
                    },] }
        ];
        /** @nocollapse */
        LbfService.ctorParameters = function () { return []; };
        /** @nocollapse */ LbfService.ngInjectableDef = core.ɵɵdefineInjectable({ factory: function LbfService_Factory() { return new LbfService(); }, token: LbfService, providedIn: "root" });
        return LbfService;
    }());

    /**
     * @fileoverview added by tsickle
     * Generated from: lib/lblibs/gird.interceptor.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var GridInterceptor = /** @class */ (function () {
        function GridInterceptor(injector) {
            this.injector = injector;
        }
        /**
         * @param {?} req
         * @param {?} next
         * @return {?}
         */
        GridInterceptor.prototype.intercept = /**
         * @param {?} req
         * @param {?} next
         * @return {?}
         */
        function (req, next) {
            var e_1, _a;
            var _this = this;
            // 统一加上服务端前缀
            /** @type {?} */
            var url = req.url;
            if (url !== "MohrssExInterface/gridsvr") {
                return next.handle(req);
            }
            url = this.sinfo.molssUrl;
            /** @type {?} */
            var body = req.body;
            /** @type {?} */
            var sname = body.sname;
            /** @type {?} */
            var sorts = {};
            /** @type {?} */
            var filters = {};
            /** @type {?} */
            var stacal = body.stacal;
            for (var p in body) {
                if (!"sname,pi,ps,sort,form,stacal,".includes(p + ",")) {
                    /** @type {?} */
                    var v = body[p];
                    filters[p] = v;
                }
            }
            /** @type {?} */
            var sortstr = body.sort;
            if (sortstr) {
                // AAE070.descend-AKC258.descend
                /** @type {?} */
                var ss = sortstr.split("-");
                try {
                    for (var ss_1 = __values(ss), ss_1_1 = ss_1.next(); !ss_1_1.done; ss_1_1 = ss_1.next()) {
                        var s = ss_1_1.value;
                        /** @type {?} */
                        var sort = s.split(".");
                        if (sort.length > 1 && sort[0] && sort[1]) {
                            sorts[sort[0]] = sort[1];
                        }
                    }
                }
                catch (e_1_1) { e_1 = { error: e_1_1 }; }
                finally {
                    try {
                        if (ss_1_1 && !ss_1_1.done && (_a = ss_1.return)) _a.call(ss_1);
                    }
                    finally { if (e_1) throw e_1.error; }
                }
            }
            /** @type {?} */
            var para = body.form;
            /** @type {?} */
            var newbody = {
                para: para,
                page: { PAGESIZE: body.ps, PAGEIDX: body.pi, SORT: sorts, FILTER: filters, STACAL: stacal },
            };
            /** @type {?} */
            var headers = req.headers.set("SNAME", sname);
            /** @type {?} */
            var newReq = req.clone({
                url: url, headers: headers, body: newbody
            });
            return next.handle(newReq).pipe(operators.mergeMap((/**
             * @param {?} event
             * @return {?}
             */
            function (event) {
                if (event instanceof http.HttpResponseBase) {
                    _this.handleData(event);
                }
                return rxjs.of(event);
            })));
        };
        Object.defineProperty(GridInterceptor.prototype, "sinfo", {
            get: /**
             * @return {?}
             */
            function () {
                return this.injector.get(ServiceInfo);
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(GridInterceptor.prototype, "msg", {
            get: /**
             * @return {?}
             */
            function () {
                return this.injector.get(ngZorroAntd.NzMessageService);
            },
            enumerable: true,
            configurable: true
        });
        /**
         * @private
         * @param {?} ev
         * @return {?}
         */
        GridInterceptor.prototype.handleData = /**
         * @private
         * @param {?} ev
         * @return {?}
         */
        function (ev) {
            if (ev instanceof http.HttpResponse) {
                /** @type {?} */
                var body = ev.body;
                if (body) {
                    if (body.code < 1) {
                        this.msg.error(body.errmsg);
                    }
                    else {
                        // 添加序号
                        /** @type {?} */
                        var list = body.message.list;
                        if (list) {
                            list.map((/**
                             * @param {?} row
                             * @param {?} index
                             * @return {?}
                             */
                            function (row, index) { return row._idx = index; }));
                        }
                    }
                }
            }
            return rxjs.of(ev);
        };
        GridInterceptor.decorators = [
            { type: core.Injectable }
        ];
        /** @nocollapse */
        GridInterceptor.ctorParameters = function () { return [
            { type: core.Injector }
        ]; };
        return GridInterceptor;
    }());
    if (false) {
        /**
         * @type {?}
         * @private
         */
        GridInterceptor.prototype.injector;
    }

    /**
     * @fileoverview added by tsickle
     * Generated from: lib/lblibs/lbfw.interceptor.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    /** @type {?} */
    var CODEMESSAGE = {
        200: '服务器成功返回请求的数据。',
        201: '新建或修改数据成功。',
        202: '一个请求已经进入后台排队（异步任务）。',
        204: '删除数据成功。',
        400: '发出的请求有错误，服务器没有进行新建或修改数据的操作。',
        401: '用户未登录或者登录已过期。',
        403: '用户得到授权，但是访问是被禁止的。',
        404: '发出的请求针对的是不存在的记录，服务器没有进行操作。',
        406: '请求的格式不可得。',
        410: '请求的资源被永久删除，且不会再得到的。',
        422: '当创建一个对象时，发生一个验证错误。',
        500: '服务器发生错误，请检查服务器。',
        502: '网关错误。',
        503: '服务不可用，服务器暂时过载或维护。',
        504: '网关超时。',
    };
    /**
     * 默认HTTP拦截器，其注册细节见 `app.module.ts`
     */
    var LbfwInterceptor = /** @class */ (function () {
        function LbfwInterceptor(injector) {
            this.injector = injector;
        }
        Object.defineProperty(LbfwInterceptor.prototype, "msg", {
            get: /**
             * @return {?}
             */
            function () {
                return this.injector.get(ngZorroAntd.NzMessageService);
            },
            enumerable: true,
            configurable: true
        });
        /**
         * @private
         * @param {?} url
         * @return {?}
         */
        LbfwInterceptor.prototype.goTo = /**
         * @private
         * @param {?} url
         * @return {?}
         */
        function (url) {
            var _this = this;
            setTimeout((/**
             * @return {?}
             */
            function () { return _this.injector.get(router.Router).navigateByUrl(url); }));
        };
        Object.defineProperty(LbfwInterceptor.prototype, "sinfo", {
            get: /**
             * @return {?}
             */
            function () {
                return this.injector.get(ServiceInfo);
            },
            enumerable: true,
            configurable: true
        });
        /**
         * @private
         * @param {?} ev
         * @return {?}
         */
        LbfwInterceptor.prototype.checkStatus = /**
         * @private
         * @param {?} ev
         * @return {?}
         */
        function (ev) {
            if (ev.status >= 200 && ev.status < 300)
                return;
            /** @type {?} */
            var errortext = CODEMESSAGE[ev.status] || ev.statusText;
            this.injector.get(ngZorroAntd.NzNotificationService).error("\u8BF7\u6C42\u9519\u8BEF " + ev.status, errortext);
        };
        /**
         * @private
         * @param {?} ev
         * @return {?}
         */
        LbfwInterceptor.prototype.handleData = /**
         * @private
         * @param {?} ev
         * @return {?}
         */
        function (ev) {
            // 可能会因为 `throw` 导出无法执行 `_HttpClient` 的 `end()` 操作
            if (ev.status > 0) {
                this.injector.get(theme._HttpClient).end();
            }
            this.checkStatus(ev);
            // 业务处理：一些通用操作
            switch (ev.status) {
                case 200:
                    // 处理未登录的错误 {code: -322, errmsg: "未登录或者登陆已经过期！"}
                    if (ev instanceof http.HttpResponse) {
                        /** @type {?} */
                        var body = ev.body;
                        if (body && body.code === -322) {
                            this.msg.error("未登录或者登陆已经过期！");
                            // 继续抛出错误中断后续所有 Pipe、subscribe 操作，因此：
                            // this.http.get('/').subscribe() 并不会触发
                            this.goTo('/passport/login');
                            break;
                        }
                        else {
                            // 重新修改 `body` 内容为 `response` 内容，对于绝大多数场景已经无须再关心业务状态码
                            // 或者依然保持完整的格式
                            return rxjs.of(ev);
                        }
                    }
                    break;
                case 401: // 未登录状态码
                    // 请求错误 401: https://preview.pro.ant.design/api/401 用户没有权限（令牌、用户名、密码错误）。
                    ((/** @type {?} */ (this.injector.get(auth.DA_SERVICE_TOKEN)))).clear();
                    this.goTo('/passport/login');
                    break;
                case 403:
                case 404:
                case 500:
                    this.goTo("/exception/" + ev.status);
                    break;
                default:
                    if (ev instanceof http.HttpErrorResponse) {
                        console.warn('未可知错误，大部分是由于后端不支持CORS或无效配置引起', ev);
                        return rxjs.throwError(ev);
                    }
                    break;
            }
            return rxjs.of(ev);
        };
        /**
         * @param {?} req
         * @param {?} next
         * @return {?}
         */
        LbfwInterceptor.prototype.intercept = /**
         * @param {?} req
         * @param {?} next
         * @return {?}
         */
        function (req, next) {
            var _this = this;
            // 统一加上服务端前缀
            /** @type {?} */
            var url = this.sinfo.baseUrl + req.url;
            // if (!url.startsWith('https://') && !url.startsWith('http://')) {
            //   url = environment.SERVER_URL + url;
            // }
            // 加上统一的请求参数
            /** @type {?} */
            var sname = req.headers.get("SNAME");
            /** @type {?} */
            var para = JSON.stringify(req.body);
            /** @type {?} */
            var appid = this.sinfo.APPID;
            /** @type {?} */
            var timestamp = this.sinfo.getTimestamp().toString();
            /** @type {?} */
            var sign = this.sinfo.getSign(sname, timestamp, para);
            /** @type {?} */
            var token = this.sinfo.LOGTOKEN;
            /** @type {?} */
            var version = this.sinfo.VERSION;
            /** @type {?} */
            var headers = req.headers.set("APPID", appid).set("TIMESTAMP", timestamp).set("SIGN", sign).set("LOGINTOKEN", token).set("VERSION", version);
            /** @type {?} */
            var newReq = req.clone({
                url: url, headers: headers
            });
            return next.handle(newReq).pipe(operators.mergeMap((/**
             * @param {?} event
             * @return {?}
             */
            function (event) {
                // 允许统一对请求错误处理
                if (event instanceof http.HttpResponseBase)
                    return _this.handleData(event);
                // 若一切都正常，则后续操作
                return rxjs.of(event);
            })), operators.catchError((/**
             * @param {?} err
             * @return {?}
             */
            function (err) { return _this.handleData(err); })));
        };
        LbfwInterceptor.decorators = [
            { type: core.Injectable }
        ];
        /** @nocollapse */
        LbfwInterceptor.ctorParameters = function () { return [
            { type: core.Injector }
        ]; };
        return LbfwInterceptor;
    }());
    if (false) {
        /**
         * @type {?}
         * @private
         */
        LbfwInterceptor.prototype.injector;
    }

    exports.DicPipe = DicPipe;
    exports.GridComponent = GridComponent;
    exports.GridInterceptor = GridInterceptor;
    exports.HttpService = HttpService;
    exports.ImgUploadComponent = ImgUploadComponent;
    exports.LbRowDirective = LbRowDirective;
    exports.LbRowSource = LbRowSource;
    exports.LbfComponent = LbfComponent;
    exports.LbfModule = LbfModule;
    exports.LbfService = LbfService;
    exports.LbfwInterceptor = LbfwInterceptor;
    exports.PopdetailComponent = PopdetailComponent;
    exports.ServiceInfo = ServiceInfo;
    exports.SupDic = SupDic;
    exports.SupExcel = SupExcel;

    Object.defineProperty(exports, '__esModule', { value: true });

})));
//# sourceMappingURL=lbf.umd.js.map
