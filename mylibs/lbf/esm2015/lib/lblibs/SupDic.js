/**
 * @fileoverview added by tsickle
 * Generated from: lib/lblibs/SupDic.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { HttpService } from './Lbjk.service';
import * as i0 from "@angular/core";
import * as i1 from "./Lbjk.service";
import * as i2 from "@angular/common/http";
export class SupDic {
    /**
     * @param {?} lbservice
     * @param {?} httpclient
     */
    constructor(lbservice, httpclient) {
        this.lbservice = lbservice;
        this.httpclient = httpclient;
        this.dicCache = new Object();
        this.ka010203 = {};
    }
    /**
     * 获取字典信息,返回数组
     * @param {?} dicName 字典值
     * @return {?} 字典信息，未取到返回[]
     */
    getDic(dicName) {
        if (this.dicCache && this.dicCache.hasOwnProperty(dicName)) {
            return this.dicCache[dicName].diclist;
        }
        return [];
    }
    /**
     * 获取字典信息,返回object
     * @param {?} dicName 字典值
     * @return {?} 字典信息
     */
    getDicObj(dicName) {
        if (this.dicCache && this.dicCache.hasOwnProperty(dicName)) {
            return this.dicCache[dicName].dicobj;
        }
    }
    /**
     * 获取指定字典指定值对应的标签值
     * @param {?} key 字典名称
     * @param {?} val 值
     * @return {?}
     */
    getdicLabel(key, val) {
        /** @type {?} */
        const dic = this.getDicObj(key);
        if (dic && dic.hasOwnProperty(val)) {
            return dic[val].CNAME;
        }
        return val;
    }
    /**
     * 异步方式获取字典信息，返回SF的字典数组
     *   始终从数据库中获取，获取的字典将会写入到缓存中
     * @param {?} dicName 字典名称
     * @return {?}
     */
    getSFDicAsync(dicName) {
        // const dic = this.getSFDic(dicName);
        // if (dic.length > 0) {
        //   return of(dic);
        // }
        // const dic = this.getSFDic(dicName);
        // if (dic.length > 0) {
        //   return of(dic);
        // }
        /** @type {?} */
        const aaa = this.lbservice;
        /** @type {?} */
        const dicCache = this.dicCache;
        return new Observable((/**
         * @param {?} observer
         * @return {?}
         */
        function subscribe(observer) {
            // FR_GetDIC
            aaa.lbservice('FR_GetAllDIC', {}).then((/**
             * @param {?} resdata
             * @return {?}
             */
            resdata => {
                if (resdata.code > 0) {
                    /** @type {?} */
                    const dicobj = new Object();
                    /** @type {?} */
                    const dicarr = resdata.message.list;
                    /** @type {?} */
                    const reslist = [];
                    for (const el of dicarr) {
                        el.text = el.CNAME;
                        el.value = el.CCODE;
                        dicobj[el.CCODE] = el;
                        reslist.push(el);
                    }
                    dicCache[dicName] = { diclist: dicarr, dicobj };
                    observer.next(reslist);
                    observer.complete();
                }
            }));
        }));
    }
    /**
     * 获取SF格式的字典信息
     * @param {?} dicName 字典值
     * @return {?}
     */
    getSFDic(dicName) {
        /** @type {?} */
        const reslist = [];
        /** @type {?} */
        const diclist = this.getDic(dicName);
        for (const dic of diclist) {
            reslist.push({ label: dic.CNAME, value: dic.CCODE });
        }
        return reslist;
    }
    /**
     * 加载所有字典
     * @return {?}
     */
    loadAllDic() {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            // FR_GetAllDIC
            return new Promise((/**
             * @param {?} resolve
             * @param {?} _
             * @return {?}
             */
            (resolve, _) => {
                this.lbservice.lbservice('FR_GetAllDIC', {}).then((/**
                 * @param {?} resdata
                 * @return {?}
                 */
                resdata => {
                    if (resdata.code > 0) {
                        // this.dicCache=resdata.message;
                        for (const d in resdata.message) {
                            if (resdata.message.hasOwnProperty(d)) {
                                /** @type {?} */
                                const a = new Object();
                                /** @type {?} */
                                const dicarr = resdata.message[d];
                                for (const el of dicarr) {
                                    el.text = el.CNAME;
                                    el.value = el.CCODE;
                                    a[el.CCODE] = el;
                                }
                                /** @type {?} */
                                const dic = { diclist: dicarr, dicobj: a };
                                this.dicCache[d] = dic;
                            }
                        }
                    }
                    resolve();
                }));
            }));
        });
    }
}
SupDic.decorators = [
    { type: Injectable, args: [{ providedIn: 'root' },] }
];
/** @nocollapse */
SupDic.ctorParameters = () => [
    { type: HttpService },
    { type: HttpClient }
];
/** @nocollapse */ SupDic.ngInjectableDef = i0.ɵɵdefineInjectable({ factory: function SupDic_Factory() { return new SupDic(i0.ɵɵinject(i1.HttpService), i0.ɵɵinject(i2.HttpClient)); }, token: SupDic, providedIn: "root" });
if (false) {
    /**
     * @type {?}
     * @private
     */
    SupDic.prototype.dicCache;
    /** @type {?} */
    SupDic.prototype.ka010203;
    /**
     * @type {?}
     * @private
     */
    SupDic.prototype.lbservice;
    /**
     * @type {?}
     * @private
     */
    SupDic.prototype.httpclient;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiU3VwRGljLmpzIiwic291cmNlUm9vdCI6Im5nOi8vbGJmLyIsInNvdXJjZXMiOlsibGliL2xibGlicy9TdXBEaWMudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7O0FBQUEsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUUzQyxPQUFPLEVBQUUsVUFBVSxFQUFrQixNQUFNLE1BQU0sQ0FBQztBQUNsRCxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sc0JBQXNCLENBQUM7QUFDbEQsT0FBTyxFQUFFLFdBQVcsRUFBRSxNQUFNLGdCQUFnQixDQUFDOzs7O0FBRzdDLE1BQU0sT0FBTyxNQUFNOzs7OztJQUNqQixZQUFvQixTQUFzQixFQUFVLFVBQXNCO1FBQXRELGNBQVMsR0FBVCxTQUFTLENBQWE7UUFBVSxlQUFVLEdBQVYsVUFBVSxDQUFZO1FBQ2xFLGFBQVEsR0FBUSxJQUFJLE1BQU0sRUFBRSxDQUFDO1FBQzlCLGFBQVEsR0FBUSxFQUFFLENBQUM7SUFGb0QsQ0FBQzs7Ozs7O0lBUy9FLE1BQU0sQ0FBQyxPQUFlO1FBQ3BCLElBQUksSUFBSSxDQUFDLFFBQVEsSUFBSSxJQUFJLENBQUMsUUFBUSxDQUFDLGNBQWMsQ0FBQyxPQUFPLENBQUMsRUFBRTtZQUMxRCxPQUFPLElBQUksQ0FBQyxRQUFRLENBQUMsT0FBTyxDQUFDLENBQUMsT0FBTyxDQUFDO1NBQ3ZDO1FBRUQsT0FBTyxFQUFFLENBQUM7SUFDWixDQUFDOzs7Ozs7SUFPRCxTQUFTLENBQUMsT0FBZTtRQUN2QixJQUFJLElBQUksQ0FBQyxRQUFRLElBQUksSUFBSSxDQUFDLFFBQVEsQ0FBQyxjQUFjLENBQUMsT0FBTyxDQUFDLEVBQUU7WUFDMUQsT0FBTyxJQUFJLENBQUMsUUFBUSxDQUFDLE9BQU8sQ0FBQyxDQUFDLE1BQU0sQ0FBQztTQUN0QztJQUNILENBQUM7Ozs7Ozs7SUFPRCxXQUFXLENBQUMsR0FBVyxFQUFFLEdBQVc7O2NBQzVCLEdBQUcsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQztRQUMvQixJQUFJLEdBQUcsSUFBSSxHQUFHLENBQUMsY0FBYyxDQUFDLEdBQUcsQ0FBQyxFQUFFO1lBQ2xDLE9BQU8sR0FBRyxDQUFDLEdBQUcsQ0FBQyxDQUFDLEtBQUssQ0FBQztTQUN2QjtRQUVELE9BQU8sR0FBRyxDQUFDO0lBQ2IsQ0FBQzs7Ozs7OztJQVFELGFBQWEsQ0FBQyxPQUFlO1FBRTNCLHNDQUFzQztRQUN0Qyx3QkFBd0I7UUFDeEIsb0JBQW9CO1FBQ3BCLElBQUk7Ozs7OztjQUVFLEdBQUcsR0FBRyxJQUFJLENBQUMsU0FBUzs7Y0FDcEIsUUFBUSxHQUFHLElBQUksQ0FBQyxRQUFRO1FBRTlCLE9BQU8sSUFBSSxVQUFVOzs7O1FBQXFCLFNBQVMsU0FBUyxDQUFDLFFBQVE7WUFFbkUsWUFBWTtZQUNaLEdBQUcsQ0FBQyxTQUFTLENBQUMsY0FBYyxFQUFFLEVBQUUsQ0FBQyxDQUFDLElBQUk7Ozs7WUFBQyxPQUFPLENBQUMsRUFBRTtnQkFFL0MsSUFBSSxPQUFPLENBQUMsSUFBSSxHQUFHLENBQUMsRUFBRTs7MEJBRWQsTUFBTSxHQUFRLElBQUksTUFBTSxFQUFFOzswQkFDMUIsTUFBTSxHQUFVLE9BQU8sQ0FBQyxPQUFPLENBQUMsSUFBSTs7MEJBQ3BDLE9BQU8sR0FBdUIsRUFBRTtvQkFDdEMsS0FBSyxNQUFNLEVBQUUsSUFBSSxNQUFNLEVBQUU7d0JBQ3ZCLEVBQUUsQ0FBQyxJQUFJLEdBQUcsRUFBRSxDQUFDLEtBQUssQ0FBQzt3QkFDbkIsRUFBRSxDQUFDLEtBQUssR0FBRyxFQUFFLENBQUMsS0FBSyxDQUFDO3dCQUNwQixNQUFNLENBQUMsRUFBRSxDQUFDLEtBQUssQ0FBQyxHQUFHLEVBQUUsQ0FBQzt3QkFDdEIsT0FBTyxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBQztxQkFDbEI7b0JBRUQsUUFBUSxDQUFDLE9BQU8sQ0FBQyxHQUFHLEVBQUUsT0FBTyxFQUFFLE1BQU0sRUFBRSxNQUFNLEVBQUUsQ0FBQztvQkFDaEQsUUFBUSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQztvQkFDdkIsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO2lCQUVyQjtZQUNILENBQUMsRUFBQyxDQUFDO1FBRUwsQ0FBQyxFQUFDLENBQUM7SUFDTCxDQUFDOzs7Ozs7SUFNRCxRQUFRLENBQUMsT0FBZTs7Y0FHaEIsT0FBTyxHQUF1QixFQUFFOztjQUVoQyxPQUFPLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxPQUFPLENBQUM7UUFDcEMsS0FBSyxNQUFNLEdBQUcsSUFBSSxPQUFPLEVBQUU7WUFDekIsT0FBTyxDQUFDLElBQUksQ0FBQyxFQUFFLEtBQUssRUFBRSxHQUFHLENBQUMsS0FBSyxFQUFFLEtBQUssRUFBRSxHQUFHLENBQUMsS0FBSyxFQUFFLENBQUMsQ0FBQztTQUN0RDtRQUdELE9BQU8sT0FBTyxDQUFDO0lBQ2pCLENBQUM7Ozs7O0lBS0ssVUFBVTs7WUFDZCxlQUFlO1lBRWYsT0FBTyxJQUFJLE9BQU87Ozs7O1lBQUMsQ0FBQyxPQUFPLEVBQUUsQ0FBQyxFQUFFLEVBQUU7Z0JBRWhDLElBQUksQ0FBQyxTQUFTLENBQUMsU0FBUyxDQUFDLGNBQWMsRUFBRSxFQUFFLENBQUMsQ0FBQyxJQUFJOzs7O2dCQUFDLE9BQU8sQ0FBQyxFQUFFO29CQUUxRCxJQUFJLE9BQU8sQ0FBQyxJQUFJLEdBQUcsQ0FBQyxFQUFFO3dCQUNwQixpQ0FBaUM7d0JBRWpDLEtBQUssTUFBTSxDQUFDLElBQUksT0FBTyxDQUFDLE9BQU8sRUFBRTs0QkFDL0IsSUFBSSxPQUFPLENBQUMsT0FBTyxDQUFDLGNBQWMsQ0FBQyxDQUFDLENBQUMsRUFBRTs7c0NBRS9CLENBQUMsR0FBUSxJQUFJLE1BQU0sRUFBRTs7c0NBQ3JCLE1BQU0sR0FBVSxPQUFPLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQztnQ0FDeEMsS0FBSyxNQUFNLEVBQUUsSUFBSSxNQUFNLEVBQUU7b0NBQ3ZCLEVBQUUsQ0FBQyxJQUFJLEdBQUcsRUFBRSxDQUFDLEtBQUssQ0FBQztvQ0FDbkIsRUFBRSxDQUFDLEtBQUssR0FBRyxFQUFFLENBQUMsS0FBSyxDQUFDO29DQUNwQixDQUFDLENBQUMsRUFBRSxDQUFDLEtBQUssQ0FBQyxHQUFHLEVBQUUsQ0FBQztpQ0FDbEI7O3NDQUVLLEdBQUcsR0FBRyxFQUFFLE9BQU8sRUFBRSxNQUFNLEVBQUUsTUFBTSxFQUFFLENBQUMsRUFBRTtnQ0FFMUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsR0FBRyxHQUFHLENBQUM7NkJBQ3hCO3lCQUNGO3FCQUVGO29CQUVELE9BQU8sRUFBRSxDQUFDO2dCQUNaLENBQUMsRUFBQyxDQUFDO1lBRUwsQ0FBQyxFQUFDLENBQUM7UUFFTCxDQUFDO0tBQUE7OztZQTlJRixVQUFVLFNBQUMsRUFBRSxVQUFVLEVBQUUsTUFBTSxFQUFFOzs7O1lBRnpCLFdBQVc7WUFEWCxVQUFVOzs7Ozs7OztJQU1qQiwwQkFBcUM7O0lBQ3JDLDBCQUEwQjs7Ozs7SUFGZCwyQkFBOEI7Ozs7O0lBQUUsNEJBQThCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSW5qZWN0YWJsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBTRlNjaGVtYUVudW1UeXBlIH0gZnJvbSAnQGRlbG9uL2Zvcm0nO1xyXG5pbXBvcnQgeyBPYnNlcnZhYmxlLCBvZiwgb2JzZXJ2YWJsZSB9IGZyb20gJ3J4anMnO1xyXG5pbXBvcnQgeyBIdHRwQ2xpZW50IH0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uL2h0dHAnO1xyXG5pbXBvcnQgeyBIdHRwU2VydmljZSB9IGZyb20gJy4vTGJqay5zZXJ2aWNlJztcclxuXHJcbkBJbmplY3RhYmxlKHsgcHJvdmlkZWRJbjogJ3Jvb3QnIH0pXHJcbmV4cG9ydCBjbGFzcyBTdXBEaWMge1xyXG4gIGNvbnN0cnVjdG9yKHByaXZhdGUgbGJzZXJ2aWNlOiBIdHRwU2VydmljZSwgcHJpdmF0ZSBodHRwY2xpZW50OiBIdHRwQ2xpZW50KSB7IH1cclxuICBwcml2YXRlIGRpY0NhY2hlOiBhbnkgPSBuZXcgT2JqZWN0KCk7XHJcbiAgcHVibGljIGthMDEwMjAzOiBhbnkgPSB7fTtcclxuXHJcbiAgLyoqXHJcbiAgICog6I635Y+W5a2X5YW45L+h5oGvLOi/lOWbnuaVsOe7hFxyXG4gICAqICBAcGFyYW0gZGljTmFtZSDlrZflhbjlgLxcclxuICAgKiAgQHJldHVybnMg5a2X5YW45L+h5oGv77yM5pyq5Y+W5Yiw6L+U5ZueW11cclxuICAgKi9cclxuICBnZXREaWMoZGljTmFtZTogc3RyaW5nKSB7XHJcbiAgICBpZiAodGhpcy5kaWNDYWNoZSAmJiB0aGlzLmRpY0NhY2hlLmhhc093blByb3BlcnR5KGRpY05hbWUpKSB7XHJcbiAgICAgIHJldHVybiB0aGlzLmRpY0NhY2hlW2RpY05hbWVdLmRpY2xpc3Q7XHJcbiAgICB9XHJcblxyXG4gICAgcmV0dXJuIFtdO1xyXG4gIH1cclxuXHJcbiAgLyoqXHJcbiAgICog6I635Y+W5a2X5YW45L+h5oGvLOi/lOWbnm9iamVjdFxyXG4gICAqICBAcGFyYW0gZGljTmFtZSDlrZflhbjlgLxcclxuICAgKiAgQHJldHVybnMg5a2X5YW45L+h5oGvXHJcbiAgICovXHJcbiAgZ2V0RGljT2JqKGRpY05hbWU6IHN0cmluZykge1xyXG4gICAgaWYgKHRoaXMuZGljQ2FjaGUgJiYgdGhpcy5kaWNDYWNoZS5oYXNPd25Qcm9wZXJ0eShkaWNOYW1lKSkge1xyXG4gICAgICByZXR1cm4gdGhpcy5kaWNDYWNoZVtkaWNOYW1lXS5kaWNvYmo7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICAvKipcclxuICAgKiDojrflj5bmjIflrprlrZflhbjmjIflrprlgLzlr7nlupTnmoTmoIfnrb7lgLxcclxuICAgKiBAcGFyYW0ga2V5IOWtl+WFuOWQjeensFxyXG4gICAqIEBwYXJhbSB2YWwg5YC8XHJcbiAgICovXHJcbiAgZ2V0ZGljTGFiZWwoa2V5OiBzdHJpbmcsIHZhbDogc3RyaW5nKSB7XHJcbiAgICBjb25zdCBkaWMgPSB0aGlzLmdldERpY09iaihrZXkpO1xyXG4gICAgaWYgKGRpYyAmJiBkaWMuaGFzT3duUHJvcGVydHkodmFsKSkge1xyXG4gICAgICByZXR1cm4gZGljW3ZhbF0uQ05BTUU7XHJcbiAgICB9XHJcblxyXG4gICAgcmV0dXJuIHZhbDtcclxuICB9XHJcblxyXG5cclxuICAvKipcclxuICAgKiDlvILmraXmlrnlvI/ojrflj5blrZflhbjkv6Hmga/vvIzov5Tlm55TRueahOWtl+WFuOaVsOe7hFxyXG4gICAqICAg5aeL57uI5LuO5pWw5o2u5bqT5Lit6I635Y+W77yM6I635Y+W55qE5a2X5YW45bCG5Lya5YaZ5YWl5Yiw57yT5a2Y5LitXHJcbiAgICogQHBhcmFtIGRpY05hbWUg5a2X5YW45ZCN56ewXHJcbiAgICovXHJcbiAgZ2V0U0ZEaWNBc3luYyhkaWNOYW1lOiBzdHJpbmcpOiBPYnNlcnZhYmxlPFNGU2NoZW1hRW51bVR5cGVbXT4ge1xyXG5cclxuICAgIC8vIGNvbnN0IGRpYyA9IHRoaXMuZ2V0U0ZEaWMoZGljTmFtZSk7XHJcbiAgICAvLyBpZiAoZGljLmxlbmd0aCA+IDApIHtcclxuICAgIC8vICAgcmV0dXJuIG9mKGRpYyk7XHJcbiAgICAvLyB9XHJcblxyXG4gICAgY29uc3QgYWFhID0gdGhpcy5sYnNlcnZpY2U7XHJcbiAgICBjb25zdCBkaWNDYWNoZSA9IHRoaXMuZGljQ2FjaGU7XHJcblxyXG4gICAgcmV0dXJuIG5ldyBPYnNlcnZhYmxlPFNGU2NoZW1hRW51bVR5cGVbXT4oZnVuY3Rpb24gc3Vic2NyaWJlKG9ic2VydmVyKSB7XHJcblxyXG4gICAgICAvLyBGUl9HZXRESUNcclxuICAgICAgYWFhLmxic2VydmljZSgnRlJfR2V0QWxsRElDJywge30pLnRoZW4ocmVzZGF0YSA9PiB7XHJcblxyXG4gICAgICAgIGlmIChyZXNkYXRhLmNvZGUgPiAwKSB7XHJcblxyXG4gICAgICAgICAgY29uc3QgZGljb2JqOiBhbnkgPSBuZXcgT2JqZWN0KCk7XHJcbiAgICAgICAgICBjb25zdCBkaWNhcnI6IGFueVtdID0gcmVzZGF0YS5tZXNzYWdlLmxpc3Q7XHJcbiAgICAgICAgICBjb25zdCByZXNsaXN0OiBTRlNjaGVtYUVudW1UeXBlW10gPSBbXTtcclxuICAgICAgICAgIGZvciAoY29uc3QgZWwgb2YgZGljYXJyKSB7XHJcbiAgICAgICAgICAgIGVsLnRleHQgPSBlbC5DTkFNRTtcclxuICAgICAgICAgICAgZWwudmFsdWUgPSBlbC5DQ09ERTtcclxuICAgICAgICAgICAgZGljb2JqW2VsLkNDT0RFXSA9IGVsO1xyXG4gICAgICAgICAgICByZXNsaXN0LnB1c2goZWwpO1xyXG4gICAgICAgICAgfVxyXG5cclxuICAgICAgICAgIGRpY0NhY2hlW2RpY05hbWVdID0geyBkaWNsaXN0OiBkaWNhcnIsIGRpY29iaiB9O1xyXG4gICAgICAgICAgb2JzZXJ2ZXIubmV4dChyZXNsaXN0KTtcclxuICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcblxyXG4gICAgICAgIH1cclxuICAgICAgfSk7XHJcblxyXG4gICAgfSk7XHJcbiAgfVxyXG5cclxuICAvKipcclxuICAgKiDojrflj5ZTRuagvOW8j+eahOWtl+WFuOS/oeaBr1xyXG4gICAqIEBwYXJhbSBkaWNOYW1lIOWtl+WFuOWAvFxyXG4gICAqL1xyXG4gIGdldFNGRGljKGRpY05hbWU6IHN0cmluZyk6IFNGU2NoZW1hRW51bVR5cGVbXSB7XHJcblxyXG5cclxuICAgIGNvbnN0IHJlc2xpc3Q6IFNGU2NoZW1hRW51bVR5cGVbXSA9IFtdO1xyXG5cclxuICAgIGNvbnN0IGRpY2xpc3QgPSB0aGlzLmdldERpYyhkaWNOYW1lKTtcclxuICAgIGZvciAoY29uc3QgZGljIG9mIGRpY2xpc3QpIHtcclxuICAgICAgcmVzbGlzdC5wdXNoKHsgbGFiZWw6IGRpYy5DTkFNRSwgdmFsdWU6IGRpYy5DQ09ERSB9KTtcclxuICAgIH1cclxuXHJcblxyXG4gICAgcmV0dXJuIHJlc2xpc3Q7XHJcbiAgfVxyXG5cclxuICAvKipcclxuICAgKiDliqDovb3miYDmnInlrZflhbhcclxuICAgKi9cclxuICBhc3luYyBsb2FkQWxsRGljKCk6IFByb21pc2U8dm9pZD4ge1xyXG4gICAgLy8gRlJfR2V0QWxsRElDXHJcblxyXG4gICAgcmV0dXJuIG5ldyBQcm9taXNlKChyZXNvbHZlLCBfKSA9PiB7XHJcblxyXG4gICAgICB0aGlzLmxic2VydmljZS5sYnNlcnZpY2UoJ0ZSX0dldEFsbERJQycsIHt9KS50aGVuKHJlc2RhdGEgPT4ge1xyXG5cclxuICAgICAgICBpZiAocmVzZGF0YS5jb2RlID4gMCkge1xyXG4gICAgICAgICAgLy8gdGhpcy5kaWNDYWNoZT1yZXNkYXRhLm1lc3NhZ2U7XHJcblxyXG4gICAgICAgICAgZm9yIChjb25zdCBkIGluIHJlc2RhdGEubWVzc2FnZSkge1xyXG4gICAgICAgICAgICBpZiAocmVzZGF0YS5tZXNzYWdlLmhhc093blByb3BlcnR5KGQpKSB7XHJcblxyXG4gICAgICAgICAgICAgIGNvbnN0IGE6IGFueSA9IG5ldyBPYmplY3QoKTtcclxuICAgICAgICAgICAgICBjb25zdCBkaWNhcnI6IGFueVtdID0gcmVzZGF0YS5tZXNzYWdlW2RdO1xyXG4gICAgICAgICAgICAgIGZvciAoY29uc3QgZWwgb2YgZGljYXJyKSB7XHJcbiAgICAgICAgICAgICAgICBlbC50ZXh0ID0gZWwuQ05BTUU7XHJcbiAgICAgICAgICAgICAgICBlbC52YWx1ZSA9IGVsLkNDT0RFO1xyXG4gICAgICAgICAgICAgICAgYVtlbC5DQ09ERV0gPSBlbDtcclxuICAgICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAgIGNvbnN0IGRpYyA9IHsgZGljbGlzdDogZGljYXJyLCBkaWNvYmo6IGEgfTtcclxuXHJcbiAgICAgICAgICAgICAgdGhpcy5kaWNDYWNoZVtkXSA9IGRpYztcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgfVxyXG5cclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIHJlc29sdmUoKTtcclxuICAgICAgfSk7XHJcblxyXG4gICAgfSk7XHJcblxyXG4gIH1cclxufVxyXG4iXX0=