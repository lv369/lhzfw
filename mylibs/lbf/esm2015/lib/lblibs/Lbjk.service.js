/**
 * @fileoverview added by tsickle
 * Generated from: lib/lblibs/Lbjk.service.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpEventType, HttpResponse, HttpRequest } from '@angular/common/http';
import { catchError, debounceTime, distinctUntilChanged } from 'rxjs/operators';
import { ServiceInfo } from '../ServiceConfig';
import * as i0 from "@angular/core";
import * as i1 from "@angular/common/http";
import * as i2 from "../ServiceConfig";
export class HttpService {
    /**
     * @param {?} httpClient
     * @param {?} serviceInfo
     */
    constructor(httpClient, serviceInfo) {
        this.httpClient = httpClient;
        this.serviceInfo = serviceInfo;
    }
    /**
     * 登录服务
     * @param {?} username 用户名
     * @param {?} password 用户密码
     * @return {?} 登录结果 Promise<any>
     */
    login(username, password) {
        /** @type {?} */
        let c = localStorage.getItem('usercache');
        if (!c) {
            c = "{}";
        }
        /** @type {?} */
        let usercache = JSON.parse(c);
        /** @type {?} */
        let ROLEID = null;
        if (usercache && usercache[username]) {
            ROLEID = usercache[username].roleid;
        }
        return new Promise((/**
         * @param {?} resolve
         * @param {?} _
         * @return {?}
         */
        (resolve, _) => {
            this.lbservice("FR_login", { "para": { "USERID": username, "PWD": this.serviceInfo.pwdmd5(password), ROLEID } }, 500, true).then((/**
             * @param {?} resdata
             * @return {?}
             */
            (resdata) => {
                // 设置登录token
                if (resdata.code > 0) {
                    this.serviceInfo.LOGTOKEN = resdata.message.LOGINTOKEN;
                    this.serviceInfo.saveToken(resdata.message.LOGINTOKEN);
                    this.serviceInfo.setUser({ aac003: resdata.message.NAME, name: username, dept: resdata.message.DEPT, roleid: resdata.message.ROLEID });
                    if (!usercache) {
                        usercache = {};
                    }
                    if (!usercache[username]) {
                        usercache[username] = { li: [], cdr: [] };
                    }
                    usercache[username].li.push(new Date());
                    usercache[username].roleid = resdata.message.ROLEID;
                    localStorage.setItem('usercache', JSON.stringify(usercache));
                }
                resolve(resdata);
            }));
        }));
    }
    /**
     * @param {?} roleid
     * @return {?}
     */
    changeRole(roleid) {
        /** @type {?} */
        let c = localStorage.getItem('usercache');
        if (!c) {
            c = "{}";
        }
        /** @type {?} */
        let usercache = JSON.parse(c);
        return new Promise((/**
         * @param {?} resolve
         * @param {?} _
         * @return {?}
         */
        (resolve, _) => {
            this.lbservice("FR_cdr", { "para": { ROLEID: roleid } }).then((/**
             * @param {?} resdata
             * @return {?}
             */
            (resdata) => {
                // 设置登录token
                if (resdata.code > 0) {
                    this.serviceInfo.LOGTOKEN = resdata.message.LOGINTOKEN;
                    this.serviceInfo.user.roleid = roleid;
                    if (!usercache) {
                        usercache = {};
                    }
                    if (!usercache[this.serviceInfo.user.name]) {
                        usercache[this.serviceInfo.user.name] = { li: [], cdr: [] };
                    }
                    usercache[this.serviceInfo.user.name].cdr.push(new Date());
                    usercache[this.serviceInfo.user.name].roleid = resdata.message.ROLEID;
                    localStorage.setItem('usercache', JSON.stringify(usercache));
                }
                resolve(resdata);
            }));
        }));
    }
    /**
     * @param {?} sname
     * @param {?} para
     * @param {?=} debounce
     * @param {?=} anonymous
     * @return {?}
     */
    lbservice2(sname, para, debounce = 0, anonymous = false) {
        /** @type {?} */
        const httpOptions = {
            headers: new HttpHeaders({
                'SNAME': sname
            })
        };
        /** @type {?} */
        let url = this.serviceInfo.molssUrl;
        if (anonymous) {
            url = url + '?_allow_anonymous=true';
        }
        return this.httpClient.post(url, para, httpOptions);
    }
    /**
     *  访问服务端
     * @param {?} sname 服务名
     * @param {?} para 入参
     * @param {?=} debounce 防抖设置，默认0
     * @param {?=} anonymous 是否匿名访问，默认否
     * @return {?}
     */
    lbservice(sname, para, debounce = 0, anonymous = false) {
        /** @type {?} */
        const httpOptions = {
            headers: new HttpHeaders({
                'SNAME': sname
            })
        };
        /** @type {?} */
        let url = this.serviceInfo.molssUrl;
        if (anonymous) {
            url = url + '?_allow_anonymous=true';
        }
        return new Promise((/**
         * @param {?} resolve
         * @param {?} _
         * @return {?}
         */
        (resolve, _) => {
            /** @type {?} */
            let result = { code: 0, errmsg: "请求失败", msg: {} };
            this.httpClient.post(url, para, httpOptions)
                .pipe(debounceTime(debounce), distinctUntilChanged(), 
            // 接收其他拦截器后产生的异常消息
            catchError((/**
             * @param {?} error
             * @return {?}
             */
            (error) => {
                console.log("调用失败", error);
                resolve({ code: -100, errmsg: "请求失败" });
                return error;
            })))
                .subscribe((/**
             * @param {?} resdata
             * @return {?}
             */
            (resdata) => {
                // setting language data
                // console.log(resdata)
                result = resdata;
            }), (/**
             * @return {?}
             */
            () => { }), (/**
             * @return {?}
             */
            () => { resolve(result); }));
        }));
    }
    /**
     * 文件上传类服务
     *   只支持单文件上传
     * @param {?} sname 服务名
     * @param {?} item 文件上传信息
     * @param {?} para 参数
     * @return {?}
     */
    lbfileupload(sname, item, para) {
        /** @type {?} */
        const formData = new FormData();
        formData.append('file', (/** @type {?} */ (item.file)));
        formData.append('paras', JSON.stringify({ para }));
        /** @type {?} */
        const req = new HttpRequest('POST', item.action ? item.action : this.serviceInfo.uploadurl, formData, {
            headers: new HttpHeaders({
                'SNAME': sname
            }),
            reportProgress: true,
            withCredentials: true
        });
        // 始终返回一个 `Subscription` 对象，nz-upload 会在适当时机自动取消订阅
        return this.httpClient.request(req).subscribe((/**
         * @param {?} event
         * @return {?}
         */
        (event) => {
            if (event.type === HttpEventType.UploadProgress) {
                if (event.total > 0) {
                    // tslint:disable-next-line:no-any
                    ((/** @type {?} */ (event))).percent = event.loaded / event.total * 100;
                }
                // 处理上传进度条，必须指定 `percent` 属性来表示进度
                item.onProgress(event, item.file);
            }
            else if (event instanceof HttpResponse) {
                // 处理成功
                item.onSuccess(event.body, item.file, event);
            }
        }), (/**
         * @param {?} err
         * @return {?}
         */
        (err) => {
            // 处理失败
            item.onError(err, item.file);
        }));
    }
}
HttpService.decorators = [
    { type: Injectable, args: [{ providedIn: 'root', },] }
];
/** @nocollapse */
HttpService.ctorParameters = () => [
    { type: HttpClient },
    { type: ServiceInfo }
];
/** @nocollapse */ HttpService.ngInjectableDef = i0.ɵɵdefineInjectable({ factory: function HttpService_Factory() { return new HttpService(i0.ɵɵinject(i1.HttpClient), i0.ɵɵinject(i2.ServiceInfo)); }, token: HttpService, providedIn: "root" });
if (false) {
    /**
     * @type {?}
     * @private
     */
    HttpService.prototype.httpClient;
    /**
     * @type {?}
     * @private
     */
    HttpService.prototype.serviceInfo;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiTGJqay5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vbGJmLyIsInNvdXJjZXMiOlsibGliL2xibGlicy9MYmprLnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7QUFBQSxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQzNDLE9BQU8sRUFBRSxVQUFVLEVBQUUsV0FBVyxFQUFhLGFBQWEsRUFBRSxZQUFZLEVBQUUsV0FBVyxFQUFFLE1BQU0sc0JBQXNCLENBQUM7QUFDcEgsT0FBTyxFQUFFLFVBQVUsRUFBRSxZQUFZLEVBQUUsb0JBQW9CLEVBQUUsTUFBTSxnQkFBZ0IsQ0FBQztBQUdoRixPQUFPLEVBQUUsV0FBVyxFQUFFLE1BQU0sa0JBQWtCLENBQUM7Ozs7QUFLL0MsTUFBTSxPQUFPLFdBQVc7Ozs7O0lBR3BCLFlBQ1ksVUFBc0IsRUFDdEIsV0FBd0I7UUFEeEIsZUFBVSxHQUFWLFVBQVUsQ0FBWTtRQUN0QixnQkFBVyxHQUFYLFdBQVcsQ0FBYTtJQUVwQyxDQUFDOzs7Ozs7O0lBUUQsS0FBSyxDQUFDLFFBQWdCLEVBQUUsUUFBZ0I7O1lBRWhDLENBQUMsR0FBRyxZQUFZLENBQUMsT0FBTyxDQUFDLFdBQVcsQ0FBQztRQUN6QyxJQUFJLENBQUMsQ0FBQyxFQUFFO1lBQ0osQ0FBQyxHQUFHLElBQUksQ0FBQztTQUNaOztZQUVHLFNBQVMsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQzs7WUFDekIsTUFBTSxHQUFHLElBQUk7UUFDakIsSUFBSSxTQUFTLElBQUksU0FBUyxDQUFDLFFBQVEsQ0FBQyxFQUFFO1lBQ2xDLE1BQU0sR0FBRyxTQUFTLENBQUMsUUFBUSxDQUFDLENBQUMsTUFBTSxDQUFDO1NBQ3ZDO1FBRUQsT0FBTyxJQUFJLE9BQU87Ozs7O1FBQUMsQ0FBQyxPQUFPLEVBQUUsQ0FBQyxFQUFFLEVBQUU7WUFFOUIsSUFBSSxDQUFDLFNBQVMsQ0FBQyxVQUFVLEVBQUUsRUFBRSxNQUFNLEVBQUUsRUFBRSxRQUFRLEVBQUUsUUFBUSxFQUFFLEtBQUssRUFBRSxJQUFJLENBQUMsV0FBVyxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsRUFBRSxNQUFNLEVBQUUsRUFBRSxFQUFFLEdBQUcsRUFBRSxJQUFJLENBQUMsQ0FBQyxJQUFJOzs7O1lBQUMsQ0FBQyxPQUFPLEVBQUUsRUFBRTtnQkFFekksWUFBWTtnQkFDWixJQUFJLE9BQU8sQ0FBQyxJQUFJLEdBQUcsQ0FBQyxFQUFFO29CQUNsQixJQUFJLENBQUMsV0FBVyxDQUFDLFFBQVEsR0FBRyxPQUFPLENBQUMsT0FBTyxDQUFDLFVBQVUsQ0FBQztvQkFDdkQsSUFBSSxDQUFDLFdBQVcsQ0FBQyxTQUFTLENBQUMsT0FBTyxDQUFDLE9BQU8sQ0FBQyxVQUFVLENBQUMsQ0FBQztvQkFDdkQsSUFBSSxDQUFDLFdBQVcsQ0FBQyxPQUFPLENBQUMsRUFBRSxNQUFNLEVBQUUsT0FBTyxDQUFDLE9BQU8sQ0FBQyxJQUFJLEVBQUUsSUFBSSxFQUFFLFFBQVEsRUFBRSxJQUFJLEVBQUUsT0FBTyxDQUFDLE9BQU8sQ0FBQyxJQUFJLEVBQUUsTUFBTSxFQUFFLE9BQU8sQ0FBQyxPQUFPLENBQUMsTUFBTSxFQUFFLENBQUMsQ0FBQztvQkFDdkksSUFBSSxDQUFDLFNBQVMsRUFBRTt3QkFDWixTQUFTLEdBQUcsRUFBRSxDQUFDO3FCQUNsQjtvQkFDRCxJQUFJLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQyxFQUFFO3dCQUN0QixTQUFTLENBQUMsUUFBUSxDQUFDLEdBQUcsRUFBRSxFQUFFLEVBQUUsRUFBRSxFQUFFLEdBQUcsRUFBRSxFQUFFLEVBQUUsQ0FBQztxQkFDN0M7b0JBRUQsU0FBUyxDQUFDLFFBQVEsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUMsSUFBSSxJQUFJLEVBQUUsQ0FBQyxDQUFDO29CQUN4QyxTQUFTLENBQUMsUUFBUSxDQUFDLENBQUMsTUFBTSxHQUFHLE9BQU8sQ0FBQyxPQUFPLENBQUMsTUFBTSxDQUFDO29CQUNwRCxZQUFZLENBQUMsT0FBTyxDQUFDLFdBQVcsRUFBRSxJQUFJLENBQUMsU0FBUyxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUM7aUJBQ2hFO2dCQUVELE9BQU8sQ0FBQyxPQUFPLENBQUMsQ0FBQztZQUVyQixDQUFDLEVBQUMsQ0FBQTtRQUVOLENBQUMsRUFBQyxDQUFBO0lBQ04sQ0FBQzs7Ozs7SUFFRCxVQUFVLENBQUMsTUFBYzs7WUFFakIsQ0FBQyxHQUFHLFlBQVksQ0FBQyxPQUFPLENBQUMsV0FBVyxDQUFDO1FBQ3pDLElBQUksQ0FBQyxDQUFDLEVBQUU7WUFDSixDQUFDLEdBQUcsSUFBSSxDQUFDO1NBQ1o7O1lBRUcsU0FBUyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDO1FBRzdCLE9BQU8sSUFBSSxPQUFPOzs7OztRQUFDLENBQUMsT0FBTyxFQUFFLENBQUMsRUFBRSxFQUFFO1lBRTlCLElBQUksQ0FBQyxTQUFTLENBQUMsUUFBUSxFQUFFLEVBQUUsTUFBTSxFQUFFLEVBQUUsTUFBTSxFQUFFLE1BQU0sRUFBRSxFQUFFLENBQUMsQ0FBQyxJQUFJOzs7O1lBQUMsQ0FBQyxPQUFPLEVBQUUsRUFBRTtnQkFFdEUsWUFBWTtnQkFDWixJQUFJLE9BQU8sQ0FBQyxJQUFJLEdBQUcsQ0FBQyxFQUFFO29CQUNsQixJQUFJLENBQUMsV0FBVyxDQUFDLFFBQVEsR0FBRyxPQUFPLENBQUMsT0FBTyxDQUFDLFVBQVUsQ0FBQztvQkFDdkQsSUFBSSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsTUFBTSxHQUFHLE1BQU0sQ0FBQztvQkFDdEMsSUFBSSxDQUFDLFNBQVMsRUFBRTt3QkFDWixTQUFTLEdBQUcsRUFBRSxDQUFDO3FCQUNsQjtvQkFDRCxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxFQUFFO3dCQUN4QyxTQUFTLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsRUFBRSxFQUFFLEVBQUUsRUFBRSxFQUFFLEdBQUcsRUFBRSxFQUFFLEVBQUUsQ0FBQztxQkFDL0Q7b0JBRUQsU0FBUyxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsSUFBSSxJQUFJLEVBQUUsQ0FBQyxDQUFDO29CQUMzRCxTQUFTLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsTUFBTSxHQUFHLE9BQU8sQ0FBQyxPQUFPLENBQUMsTUFBTSxDQUFDO29CQUN0RSxZQUFZLENBQUMsT0FBTyxDQUFDLFdBQVcsRUFBRSxJQUFJLENBQUMsU0FBUyxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUM7aUJBQ2hFO2dCQUVELE9BQU8sQ0FBQyxPQUFPLENBQUMsQ0FBQztZQUVyQixDQUFDLEVBQUMsQ0FBQTtRQUVOLENBQUMsRUFBQyxDQUFBO0lBQ04sQ0FBQzs7Ozs7Ozs7SUFFRCxVQUFVLENBQUMsS0FBYSxFQUFFLElBQVEsRUFBRSxXQUFtQixDQUFDLEVBQUUsWUFBcUIsS0FBSzs7Y0FDMUUsV0FBVyxHQUFHO1lBQ2hCLE9BQU8sRUFBRSxJQUFJLFdBQVcsQ0FBQztnQkFDckIsT0FBTyxFQUFFLEtBQUs7YUFDakIsQ0FBQztTQUNMOztZQUVHLEdBQUcsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLFFBQVE7UUFDbkMsSUFBSSxTQUFTLEVBQUU7WUFDWCxHQUFHLEdBQUcsR0FBRyxHQUFHLHdCQUF3QixDQUFDO1NBQ3hDO1FBRUQsT0FBTyxJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxHQUFHLEVBQUUsSUFBSSxFQUFFLFdBQVcsQ0FBQyxDQUFDO0lBRXhELENBQUM7Ozs7Ozs7OztJQVVELFNBQVMsQ0FBQyxLQUFhLEVBQUUsSUFBUSxFQUFFLFdBQW1CLENBQUMsRUFBRSxZQUFxQixLQUFLOztjQUV6RSxXQUFXLEdBQUc7WUFDaEIsT0FBTyxFQUFFLElBQUksV0FBVyxDQUFDO2dCQUNyQixPQUFPLEVBQUUsS0FBSzthQUNqQixDQUFDO1NBQ0w7O1lBRUcsR0FBRyxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsUUFBUTtRQUNuQyxJQUFJLFNBQVMsRUFBRTtZQUNYLEdBQUcsR0FBRyxHQUFHLEdBQUcsd0JBQXdCLENBQUM7U0FDeEM7UUFFRCxPQUFPLElBQUksT0FBTzs7Ozs7UUFBQyxDQUFDLE9BQU8sRUFBRSxDQUFDLEVBQUUsRUFBRTs7Z0JBRTFCLE1BQU0sR0FBUSxFQUFFLElBQUksRUFBRSxDQUFDLEVBQUUsTUFBTSxFQUFFLE1BQU0sRUFBRSxHQUFHLEVBQUUsRUFBRSxFQUFFO1lBRXRELElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLEdBQUcsRUFBRSxJQUFJLEVBQUUsV0FBVyxDQUFDO2lCQUN2QyxJQUFJLENBQ0QsWUFBWSxDQUFDLFFBQVEsQ0FBQyxFQUN0QixvQkFBb0IsRUFBRTtZQUN0QixrQkFBa0I7WUFDbEIsVUFBVTs7OztZQUFDLENBQUMsS0FBSyxFQUFFLEVBQUU7Z0JBQ2pCLE9BQU8sQ0FBQyxHQUFHLENBQUMsTUFBTSxFQUFFLEtBQUssQ0FBQyxDQUFBO2dCQUMxQixPQUFPLENBQUMsRUFBRSxJQUFJLEVBQUUsQ0FBQyxHQUFHLEVBQUUsTUFBTSxFQUFFLE1BQU0sRUFBRSxDQUFDLENBQUM7Z0JBQ3hDLE9BQU8sS0FBSyxDQUFDO1lBQ2pCLENBQUMsRUFBQyxDQUNMO2lCQUNBLFNBQVM7Ozs7WUFDTixDQUFDLE9BQU8sRUFBRSxFQUFFO2dCQUNSLHdCQUF3QjtnQkFDeEIsdUJBQXVCO2dCQUN2QixNQUFNLEdBQUcsT0FBTyxDQUFDO1lBRXJCLENBQUM7OztZQUNELEdBQUcsRUFBRSxHQUFHLENBQUM7OztZQUNULEdBQUcsRUFBRSxHQUFHLE9BQU8sQ0FBQyxNQUFNLENBQUMsQ0FBQSxDQUFDLENBQUMsRUFDNUIsQ0FBQztRQUNWLENBQUMsRUFBQyxDQUFDO0lBQ1AsQ0FBQzs7Ozs7Ozs7O0lBU0QsWUFBWSxDQUFDLEtBQWEsRUFBRSxJQUFtQixFQUFFLElBQVk7O2NBR25ELFFBQVEsR0FBRyxJQUFJLFFBQVEsRUFBRTtRQUMvQixRQUFRLENBQUMsTUFBTSxDQUFDLE1BQU0sRUFBRSxtQkFBQSxJQUFJLENBQUMsSUFBSSxFQUFPLENBQUMsQ0FBQztRQUMxQyxRQUFRLENBQUMsTUFBTSxDQUFDLE9BQU8sRUFBRSxJQUFJLENBQUMsU0FBUyxDQUFDLEVBQUUsSUFBSSxFQUFFLENBQUMsQ0FBQyxDQUFDOztjQUc3QyxHQUFHLEdBQUcsSUFBSSxXQUFXLENBQUMsTUFBTSxFQUFFLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsU0FBUyxFQUFFLFFBQVEsRUFBRTtZQUNsRyxPQUFPLEVBQUUsSUFBSSxXQUFXLENBQUM7Z0JBQ3JCLE9BQU8sRUFBRSxLQUFLO2FBQ2pCLENBQUM7WUFDRixjQUFjLEVBQUUsSUFBSTtZQUNwQixlQUFlLEVBQUUsSUFBSTtTQUN4QixDQUFDO1FBRUYsa0RBQWtEO1FBQ2xELE9BQU8sSUFBSSxDQUFDLFVBQVUsQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLENBQUMsU0FBUzs7OztRQUFDLENBQUMsS0FBb0IsRUFBRSxFQUFFO1lBQ25FLElBQUksS0FBSyxDQUFDLElBQUksS0FBSyxhQUFhLENBQUMsY0FBYyxFQUFFO2dCQUM3QyxJQUFJLEtBQUssQ0FBQyxLQUFLLEdBQUcsQ0FBQyxFQUFFO29CQUNqQixrQ0FBa0M7b0JBQ2xDLENBQUMsbUJBQUEsS0FBSyxFQUFPLENBQUMsQ0FBQyxPQUFPLEdBQUcsS0FBSyxDQUFDLE1BQU0sR0FBRyxLQUFLLENBQUMsS0FBSyxHQUFHLEdBQUcsQ0FBQztpQkFDN0Q7Z0JBQ0QsaUNBQWlDO2dCQUNqQyxJQUFJLENBQUMsVUFBVSxDQUFDLEtBQUssRUFBRSxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7YUFDckM7aUJBQU0sSUFBSSxLQUFLLFlBQVksWUFBWSxFQUFFO2dCQUN0QyxPQUFPO2dCQUNQLElBQUksQ0FBQyxTQUFTLENBQUMsS0FBSyxDQUFDLElBQUksRUFBRSxJQUFJLENBQUMsSUFBSSxFQUFFLEtBQUssQ0FBQyxDQUFDO2FBQ2hEO1FBQ0wsQ0FBQzs7OztRQUFFLENBQUMsR0FBRyxFQUFFLEVBQUU7WUFDUCxPQUFPO1lBQ1AsSUFBSSxDQUFDLE9BQU8sQ0FBQyxHQUFHLEVBQUUsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQ2pDLENBQUMsRUFBQyxDQUFDO0lBQ1AsQ0FBQzs7O1lBdk1KLFVBQVUsU0FBQyxFQUFFLFVBQVUsRUFBRSxNQUFNLEdBQUc7Ozs7WUFSMUIsVUFBVTtZQUlWLFdBQVc7Ozs7Ozs7O0lBU1osaUNBQThCOzs7OztJQUM5QixrQ0FBZ0MiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBJbmplY3RhYmxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IEh0dHBDbGllbnQsIEh0dHBIZWFkZXJzLCBIdHRwRXZlbnQsIEh0dHBFdmVudFR5cGUsIEh0dHBSZXNwb25zZSwgSHR0cFJlcXVlc3QgfSBmcm9tICdAYW5ndWxhci9jb21tb24vaHR0cCc7XHJcbmltcG9ydCB7IGNhdGNoRXJyb3IsIGRlYm91bmNlVGltZSwgZGlzdGluY3RVbnRpbENoYW5nZWQgfSBmcm9tICdyeGpzL29wZXJhdG9ycyc7XHJcbmltcG9ydCB7IFVwbG9hZFhIUkFyZ3MgfSBmcm9tICduZy16b3Jyby1hbnRkJztcclxuaW1wb3J0IHsgU3Vic2NyaXB0aW9uLCBPYnNlcnZhYmxlIH0gZnJvbSAncnhqcyc7XHJcbmltcG9ydCB7IFNlcnZpY2VJbmZvIH0gZnJvbSAnLi4vU2VydmljZUNvbmZpZyc7XHJcblxyXG5cclxuXHJcbkBJbmplY3RhYmxlKHsgcHJvdmlkZWRJbjogJ3Jvb3QnLCB9KVxyXG5leHBvcnQgY2xhc3MgSHR0cFNlcnZpY2Uge1xyXG5cclxuXHJcbiAgICBjb25zdHJ1Y3RvcihcclxuICAgICAgICBwcml2YXRlIGh0dHBDbGllbnQ6IEh0dHBDbGllbnQsXHJcbiAgICAgICAgcHJpdmF0ZSBzZXJ2aWNlSW5mbzogU2VydmljZUluZm8sXHJcbiAgICApIHtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIOeZu+W9leacjeWKoVxyXG4gICAgICogQHBhcmFtIHVzZXJuYW1lIOeUqOaIt+WQjVxyXG4gICAgICogQHBhcmFtIHBhc3N3b3JkIOeUqOaIt+WvhueggVxyXG4gICAgICogQHJldHVybnMg55m75b2V57uT5p6cIFByb21pc2U8YW55PlxyXG4gICAgICovXHJcbiAgICBsb2dpbih1c2VybmFtZTogc3RyaW5nLCBwYXNzd29yZDogc3RyaW5nKTogUHJvbWlzZTxhbnk+IHtcclxuXHJcbiAgICAgICAgbGV0IGMgPSBsb2NhbFN0b3JhZ2UuZ2V0SXRlbSgndXNlcmNhY2hlJyk7XHJcbiAgICAgICAgaWYgKCFjKSB7XHJcbiAgICAgICAgICAgIGMgPSBcInt9XCI7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBsZXQgdXNlcmNhY2hlID0gSlNPTi5wYXJzZShjKTtcclxuICAgICAgICBsZXQgUk9MRUlEID0gbnVsbDtcclxuICAgICAgICBpZiAodXNlcmNhY2hlICYmIHVzZXJjYWNoZVt1c2VybmFtZV0pIHtcclxuICAgICAgICAgICAgUk9MRUlEID0gdXNlcmNhY2hlW3VzZXJuYW1lXS5yb2xlaWQ7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICByZXR1cm4gbmV3IFByb21pc2UoKHJlc29sdmUsIF8pID0+IHtcclxuXHJcbiAgICAgICAgICAgIHRoaXMubGJzZXJ2aWNlKFwiRlJfbG9naW5cIiwgeyBcInBhcmFcIjogeyBcIlVTRVJJRFwiOiB1c2VybmFtZSwgXCJQV0RcIjogdGhpcy5zZXJ2aWNlSW5mby5wd2RtZDUocGFzc3dvcmQpLCBST0xFSUQgfSB9LCA1MDAsIHRydWUpLnRoZW4oKHJlc2RhdGEpID0+IHtcclxuXHJcbiAgICAgICAgICAgICAgICAvLyDorr7nva7nmbvlvZV0b2tlblxyXG4gICAgICAgICAgICAgICAgaWYgKHJlc2RhdGEuY29kZSA+IDApIHtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLnNlcnZpY2VJbmZvLkxPR1RPS0VOID0gcmVzZGF0YS5tZXNzYWdlLkxPR0lOVE9LRU47XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5zZXJ2aWNlSW5mby5zYXZlVG9rZW4ocmVzZGF0YS5tZXNzYWdlLkxPR0lOVE9LRU4pO1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuc2VydmljZUluZm8uc2V0VXNlcih7IGFhYzAwMzogcmVzZGF0YS5tZXNzYWdlLk5BTUUsIG5hbWU6IHVzZXJuYW1lLCBkZXB0OiByZXNkYXRhLm1lc3NhZ2UuREVQVCwgcm9sZWlkOiByZXNkYXRhLm1lc3NhZ2UuUk9MRUlEIH0pO1xyXG4gICAgICAgICAgICAgICAgICAgIGlmICghdXNlcmNhY2hlKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHVzZXJjYWNoZSA9IHt9O1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICBpZiAoIXVzZXJjYWNoZVt1c2VybmFtZV0pIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdXNlcmNhY2hlW3VzZXJuYW1lXSA9IHsgbGk6IFtdLCBjZHI6IFtdIH07XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgICAgICAgICB1c2VyY2FjaGVbdXNlcm5hbWVdLmxpLnB1c2gobmV3IERhdGUoKSk7XHJcbiAgICAgICAgICAgICAgICAgICAgdXNlcmNhY2hlW3VzZXJuYW1lXS5yb2xlaWQgPSByZXNkYXRhLm1lc3NhZ2UuUk9MRUlEO1xyXG4gICAgICAgICAgICAgICAgICAgIGxvY2FsU3RvcmFnZS5zZXRJdGVtKCd1c2VyY2FjaGUnLCBKU09OLnN0cmluZ2lmeSh1c2VyY2FjaGUpKTtcclxuICAgICAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgICAgICByZXNvbHZlKHJlc2RhdGEpO1xyXG5cclxuICAgICAgICAgICAgfSlcclxuXHJcbiAgICAgICAgfSlcclxuICAgIH1cclxuXHJcbiAgICBjaGFuZ2VSb2xlKHJvbGVpZDogc3RyaW5nKTogUHJvbWlzZTxhbnk+IHtcclxuXHJcbiAgICAgICAgbGV0IGMgPSBsb2NhbFN0b3JhZ2UuZ2V0SXRlbSgndXNlcmNhY2hlJyk7XHJcbiAgICAgICAgaWYgKCFjKSB7XHJcbiAgICAgICAgICAgIGMgPSBcInt9XCI7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBsZXQgdXNlcmNhY2hlID0gSlNPTi5wYXJzZShjKTtcclxuXHJcblxyXG4gICAgICAgIHJldHVybiBuZXcgUHJvbWlzZSgocmVzb2x2ZSwgXykgPT4ge1xyXG5cclxuICAgICAgICAgICAgdGhpcy5sYnNlcnZpY2UoXCJGUl9jZHJcIiwgeyBcInBhcmFcIjogeyBST0xFSUQ6IHJvbGVpZCB9IH0pLnRoZW4oKHJlc2RhdGEpID0+IHtcclxuXHJcbiAgICAgICAgICAgICAgICAvLyDorr7nva7nmbvlvZV0b2tlblxyXG4gICAgICAgICAgICAgICAgaWYgKHJlc2RhdGEuY29kZSA+IDApIHtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLnNlcnZpY2VJbmZvLkxPR1RPS0VOID0gcmVzZGF0YS5tZXNzYWdlLkxPR0lOVE9LRU47XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5zZXJ2aWNlSW5mby51c2VyLnJvbGVpZCA9IHJvbGVpZDtcclxuICAgICAgICAgICAgICAgICAgICBpZiAoIXVzZXJjYWNoZSkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB1c2VyY2FjaGUgPSB7fTtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKCF1c2VyY2FjaGVbdGhpcy5zZXJ2aWNlSW5mby51c2VyLm5hbWVdKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHVzZXJjYWNoZVt0aGlzLnNlcnZpY2VJbmZvLnVzZXIubmFtZV0gPSB7IGxpOiBbXSwgY2RyOiBbXSB9O1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgICAgICAgICAgdXNlcmNhY2hlW3RoaXMuc2VydmljZUluZm8udXNlci5uYW1lXS5jZHIucHVzaChuZXcgRGF0ZSgpKTtcclxuICAgICAgICAgICAgICAgICAgICB1c2VyY2FjaGVbdGhpcy5zZXJ2aWNlSW5mby51c2VyLm5hbWVdLnJvbGVpZCA9IHJlc2RhdGEubWVzc2FnZS5ST0xFSUQ7XHJcbiAgICAgICAgICAgICAgICAgICAgbG9jYWxTdG9yYWdlLnNldEl0ZW0oJ3VzZXJjYWNoZScsIEpTT04uc3RyaW5naWZ5KHVzZXJjYWNoZSkpO1xyXG4gICAgICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgICAgIHJlc29sdmUocmVzZGF0YSk7XHJcblxyXG4gICAgICAgICAgICB9KVxyXG5cclxuICAgICAgICB9KVxyXG4gICAgfVxyXG5cclxuICAgIGxic2VydmljZTIoc25hbWU6IHN0cmluZywgcGFyYToge30sIGRlYm91bmNlOiBudW1iZXIgPSAwLCBhbm9ueW1vdXM6IGJvb2xlYW4gPSBmYWxzZSk6IE9ic2VydmFibGU8e30+IHtcclxuICAgICAgICBjb25zdCBodHRwT3B0aW9ucyA9IHtcclxuICAgICAgICAgICAgaGVhZGVyczogbmV3IEh0dHBIZWFkZXJzKHtcclxuICAgICAgICAgICAgICAgICdTTkFNRSc6IHNuYW1lXHJcbiAgICAgICAgICAgIH0pXHJcbiAgICAgICAgfTtcclxuXHJcbiAgICAgICAgbGV0IHVybCA9IHRoaXMuc2VydmljZUluZm8ubW9sc3NVcmw7XHJcbiAgICAgICAgaWYgKGFub255bW91cykge1xyXG4gICAgICAgICAgICB1cmwgPSB1cmwgKyAnP19hbGxvd19hbm9ueW1vdXM9dHJ1ZSc7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICByZXR1cm4gdGhpcy5odHRwQ2xpZW50LnBvc3QodXJsLCBwYXJhLCBodHRwT3B0aW9ucyk7XHJcblxyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogIOiuv+mXruacjeWKoeerr1xyXG4gICAgICogQHBhcmFtIHNuYW1lIOacjeWKoeWQjVxyXG4gICAgICogQHBhcmFtIHBhcmEg5YWl5Y+CXHJcbiAgICAgKiBAcGFyYW0gZGVib3VuY2Ug6Ziy5oqW6K6+572u77yM6buY6K6kMFxyXG4gICAgICogQHBhcmFtIGFub255bW91cyDmmK/lkKbljL/lkI3orr/pl67vvIzpu5jorqTlkKZcclxuICAgICAqL1xyXG5cclxuICAgIGxic2VydmljZShzbmFtZTogc3RyaW5nLCBwYXJhOiB7fSwgZGVib3VuY2U6IG51bWJlciA9IDAsIGFub255bW91czogYm9vbGVhbiA9IGZhbHNlKTogUHJvbWlzZTxhbnk+IHtcclxuXHJcbiAgICAgICAgY29uc3QgaHR0cE9wdGlvbnMgPSB7XHJcbiAgICAgICAgICAgIGhlYWRlcnM6IG5ldyBIdHRwSGVhZGVycyh7XHJcbiAgICAgICAgICAgICAgICAnU05BTUUnOiBzbmFtZVxyXG4gICAgICAgICAgICB9KVxyXG4gICAgICAgIH07XHJcblxyXG4gICAgICAgIGxldCB1cmwgPSB0aGlzLnNlcnZpY2VJbmZvLm1vbHNzVXJsO1xyXG4gICAgICAgIGlmIChhbm9ueW1vdXMpIHtcclxuICAgICAgICAgICAgdXJsID0gdXJsICsgJz9fYWxsb3dfYW5vbnltb3VzPXRydWUnO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgcmV0dXJuIG5ldyBQcm9taXNlKChyZXNvbHZlLCBfKSA9PiB7XHJcblxyXG4gICAgICAgICAgICBsZXQgcmVzdWx0OiBhbnkgPSB7IGNvZGU6IDAsIGVycm1zZzogXCLor7fmsYLlpLHotKVcIiwgbXNnOiB7fSB9O1xyXG5cclxuICAgICAgICAgICAgdGhpcy5odHRwQ2xpZW50LnBvc3QodXJsLCBwYXJhLCBodHRwT3B0aW9ucylcclxuICAgICAgICAgICAgICAgIC5waXBlKFxyXG4gICAgICAgICAgICAgICAgICAgIGRlYm91bmNlVGltZShkZWJvdW5jZSksXHJcbiAgICAgICAgICAgICAgICAgICAgZGlzdGluY3RVbnRpbENoYW5nZWQoKSxcclxuICAgICAgICAgICAgICAgICAgICAvLyDmjqXmlLblhbbku5bmi6bmiKrlmajlkI7kuqfnlJ/nmoTlvILluLjmtojmga9cclxuICAgICAgICAgICAgICAgICAgICBjYXRjaEVycm9yKChlcnJvcikgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyhcIuiwg+eUqOWksei0pVwiLCBlcnJvcilcclxuICAgICAgICAgICAgICAgICAgICAgICAgcmVzb2x2ZSh7IGNvZGU6IC0xMDAsIGVycm1zZzogXCLor7fmsYLlpLHotKVcIiB9KTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIGVycm9yO1xyXG4gICAgICAgICAgICAgICAgICAgIH0pLFxyXG4gICAgICAgICAgICAgICAgKVxyXG4gICAgICAgICAgICAgICAgLnN1YnNjcmliZShcclxuICAgICAgICAgICAgICAgICAgICAocmVzZGF0YSkgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAvLyBzZXR0aW5nIGxhbmd1YWdlIGRhdGFcclxuICAgICAgICAgICAgICAgICAgICAgICAgLy8gY29uc29sZS5sb2cocmVzZGF0YSlcclxuICAgICAgICAgICAgICAgICAgICAgICAgcmVzdWx0ID0gcmVzZGF0YTtcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgICAgICAgICAoKSA9PiB7IH0sXHJcbiAgICAgICAgICAgICAgICAgICAgKCkgPT4geyByZXNvbHZlKHJlc3VsdCkgfSxcclxuICAgICAgICAgICAgICAgICk7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiDmlofku7bkuIrkvKDnsbvmnI3liqFcclxuICAgICAqICAg5Y+q5pSv5oyB5Y2V5paH5Lu25LiK5LygXHJcbiAgICAgKiBAcGFyYW0gc25hbWUg5pyN5Yqh5ZCNXHJcbiAgICAgKiBAcGFyYW0gaXRlbSDmlofku7bkuIrkvKDkv6Hmga9cclxuICAgICAqIEBwYXJhbSBwYXJhIOWPguaVsFxyXG4gICAgICovXHJcbiAgICBsYmZpbGV1cGxvYWQoc25hbWU6IHN0cmluZywgaXRlbTogVXBsb2FkWEhSQXJncywgcGFyYTogb2JqZWN0KTogU3Vic2NyaXB0aW9uIHtcclxuXHJcblxyXG4gICAgICAgIGNvbnN0IGZvcm1EYXRhID0gbmV3IEZvcm1EYXRhKCk7XHJcbiAgICAgICAgZm9ybURhdGEuYXBwZW5kKCdmaWxlJywgaXRlbS5maWxlIGFzIGFueSk7XHJcbiAgICAgICAgZm9ybURhdGEuYXBwZW5kKCdwYXJhcycsIEpTT04uc3RyaW5naWZ5KHsgcGFyYSB9KSk7XHJcblxyXG5cclxuICAgICAgICBjb25zdCByZXEgPSBuZXcgSHR0cFJlcXVlc3QoJ1BPU1QnLCBpdGVtLmFjdGlvbiA/IGl0ZW0uYWN0aW9uIDogdGhpcy5zZXJ2aWNlSW5mby51cGxvYWR1cmwsIGZvcm1EYXRhLCB7XHJcbiAgICAgICAgICAgIGhlYWRlcnM6IG5ldyBIdHRwSGVhZGVycyh7XHJcbiAgICAgICAgICAgICAgICAnU05BTUUnOiBzbmFtZVxyXG4gICAgICAgICAgICB9KSxcclxuICAgICAgICAgICAgcmVwb3J0UHJvZ3Jlc3M6IHRydWUsXHJcbiAgICAgICAgICAgIHdpdGhDcmVkZW50aWFsczogdHJ1ZVxyXG4gICAgICAgIH0pO1xyXG5cclxuICAgICAgICAvLyDlp4vnu4jov5Tlm57kuIDkuKogYFN1YnNjcmlwdGlvbmAg5a+56LGh77yMbnotdXBsb2FkIOS8muWcqOmAguW9k+aXtuacuuiHquWKqOWPlua2iOiuoumYhVxyXG4gICAgICAgIHJldHVybiB0aGlzLmh0dHBDbGllbnQucmVxdWVzdChyZXEpLnN1YnNjcmliZSgoZXZlbnQ6IEh0dHBFdmVudDx7fT4pID0+IHtcclxuICAgICAgICAgICAgaWYgKGV2ZW50LnR5cGUgPT09IEh0dHBFdmVudFR5cGUuVXBsb2FkUHJvZ3Jlc3MpIHtcclxuICAgICAgICAgICAgICAgIGlmIChldmVudC50b3RhbCA+IDApIHtcclxuICAgICAgICAgICAgICAgICAgICAvLyB0c2xpbnQ6ZGlzYWJsZS1uZXh0LWxpbmU6bm8tYW55XHJcbiAgICAgICAgICAgICAgICAgICAgKGV2ZW50IGFzIGFueSkucGVyY2VudCA9IGV2ZW50LmxvYWRlZCAvIGV2ZW50LnRvdGFsICogMTAwO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgLy8g5aSE55CG5LiK5Lyg6L+b5bqm5p2h77yM5b+F6aG75oyH5a6aIGBwZXJjZW50YCDlsZ7mgKfmnaXooajnpLrov5vluqZcclxuICAgICAgICAgICAgICAgIGl0ZW0ub25Qcm9ncmVzcyhldmVudCwgaXRlbS5maWxlKTtcclxuICAgICAgICAgICAgfSBlbHNlIGlmIChldmVudCBpbnN0YW5jZW9mIEh0dHBSZXNwb25zZSkge1xyXG4gICAgICAgICAgICAgICAgLy8g5aSE55CG5oiQ5YqfXHJcbiAgICAgICAgICAgICAgICBpdGVtLm9uU3VjY2VzcyhldmVudC5ib2R5LCBpdGVtLmZpbGUsIGV2ZW50KTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0sIChlcnIpID0+IHtcclxuICAgICAgICAgICAgLy8g5aSE55CG5aSx6LSlXHJcbiAgICAgICAgICAgIGl0ZW0ub25FcnJvcihlcnIsIGl0ZW0uZmlsZSk7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG59Il19