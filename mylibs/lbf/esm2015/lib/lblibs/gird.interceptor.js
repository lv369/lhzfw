/**
 * @fileoverview added by tsickle
 * Generated from: lib/lblibs/gird.interceptor.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Injectable, Injector } from '@angular/core';
import { HttpResponse, HttpResponseBase, } from '@angular/common/http';
import { of } from 'rxjs';
import { mergeMap } from 'rxjs/operators';
import { NzMessageService } from 'ng-zorro-antd';
import { ServiceInfo } from '../ServiceConfig';
export class GridInterceptor {
    /**
     * @param {?} injector
     */
    constructor(injector) {
        this.injector = injector;
    }
    /**
     * @param {?} req
     * @param {?} next
     * @return {?}
     */
    intercept(req, next) {
        // 统一加上服务端前缀
        /** @type {?} */
        let url = req.url;
        if (url !== "MohrssExInterface/gridsvr") {
            return next.handle(req);
        }
        url = this.sinfo.molssUrl;
        /** @type {?} */
        const body = req.body;
        /** @type {?} */
        const sname = body.sname;
        /** @type {?} */
        const sorts = {};
        /** @type {?} */
        const filters = {};
        /** @type {?} */
        const stacal = body.stacal;
        for (const p in body) {
            if (!"sname,pi,ps,sort,form,stacal,".includes(p + ",")) {
                /** @type {?} */
                const v = body[p];
                filters[p] = v;
            }
        }
        /** @type {?} */
        const sortstr = body.sort;
        if (sortstr) {
            // AAE070.descend-AKC258.descend
            /** @type {?} */
            const ss = sortstr.split("-");
            for (const s of ss) {
                /** @type {?} */
                const sort = s.split(".");
                if (sort.length > 1 && sort[0] && sort[1]) {
                    sorts[sort[0]] = sort[1];
                }
            }
        }
        /** @type {?} */
        const para = body.form;
        /** @type {?} */
        const newbody = {
            para,
            page: { PAGESIZE: body.ps, PAGEIDX: body.pi, SORT: sorts, FILTER: filters, STACAL: stacal },
        };
        /** @type {?} */
        const headers = req.headers.set("SNAME", sname);
        /** @type {?} */
        const newReq = req.clone({
            url, headers, body: newbody
        });
        return next.handle(newReq).pipe(mergeMap((/**
         * @param {?} event
         * @return {?}
         */
        (event) => {
            if (event instanceof HttpResponseBase) {
                this.handleData(event);
            }
            return of(event);
        })));
    }
    /**
     * @return {?}
     */
    get sinfo() {
        return this.injector.get(ServiceInfo);
    }
    /**
     * @return {?}
     */
    get msg() {
        return this.injector.get(NzMessageService);
    }
    /**
     * @private
     * @param {?} ev
     * @return {?}
     */
    handleData(ev) {
        if (ev instanceof HttpResponse) {
            /** @type {?} */
            const body = ev.body;
            if (body) {
                if (body.code < 1) {
                    this.msg.error(body.errmsg);
                }
                else {
                    // 添加序号
                    /** @type {?} */
                    const list = body.message.list;
                    if (list) {
                        list.map((/**
                         * @param {?} row
                         * @param {?} index
                         * @return {?}
                         */
                        (row, index) => row._idx = index));
                    }
                }
            }
        }
        return of(ev);
    }
}
GridInterceptor.decorators = [
    { type: Injectable }
];
/** @nocollapse */
GridInterceptor.ctorParameters = () => [
    { type: Injector }
];
if (false) {
    /**
     * @type {?}
     * @private
     */
    GridInterceptor.prototype.injector;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZ2lyZC5pbnRlcmNlcHRvci5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2xiZi8iLCJzb3VyY2VzIjpbImxpYi9sYmxpYnMvZ2lyZC5pbnRlcmNlcHRvci50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7OztBQUFBLE9BQU8sRUFBRSxVQUFVLEVBQUUsUUFBUSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQ3JELE9BQU8sRUFPTCxZQUFZLEVBRVosZ0JBQWdCLEdBQ2pCLE1BQU0sc0JBQXNCLENBQUM7QUFDOUIsT0FBTyxFQUFjLEVBQUUsRUFBRSxNQUFNLE1BQU0sQ0FBQztBQUV0QyxPQUFPLEVBQUUsUUFBUSxFQUFFLE1BQU0sZ0JBQWdCLENBQUM7QUFDMUMsT0FBTyxFQUFFLGdCQUFnQixFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQ2pELE9BQU8sRUFBRSxXQUFXLEVBQUUsTUFBTSxrQkFBa0IsQ0FBQztBQUcvQyxNQUFNLE9BQU8sZUFBZTs7OztJQUMxQixZQUFvQixRQUFrQjtRQUFsQixhQUFRLEdBQVIsUUFBUSxDQUFVO0lBQUksQ0FBQzs7Ozs7O0lBRzNDLFNBQVMsQ0FDUCxHQUFxQixFQUNyQixJQUFpQjs7O1lBU2IsR0FBRyxHQUFHLEdBQUcsQ0FBQyxHQUFHO1FBQ2pCLElBQUksR0FBRyxLQUFLLDJCQUEyQixFQUFFO1lBQ3ZDLE9BQU8sSUFBSSxDQUFDLE1BQU0sQ0FBQyxHQUFHLENBQUMsQ0FBQztTQUN6QjtRQUVELEdBQUcsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQzs7Y0FFcEIsSUFBSSxHQUFHLEdBQUcsQ0FBQyxJQUFJOztjQUNmLEtBQUssR0FBRyxJQUFJLENBQUMsS0FBSzs7Y0FDbEIsS0FBSyxHQUFHLEVBQUU7O2NBQ1YsT0FBTyxHQUFHLEVBQUU7O2NBQ1osTUFBTSxHQUFHLElBQUksQ0FBQyxNQUFNO1FBRTFCLEtBQUssTUFBTSxDQUFDLElBQUksSUFBSSxFQUFFO1lBRXBCLElBQUksQ0FBQywrQkFBK0IsQ0FBQyxRQUFRLENBQUMsQ0FBQyxHQUFHLEdBQUcsQ0FBQyxFQUFFOztzQkFFaEQsQ0FBQyxHQUFHLElBQUksQ0FBQyxDQUFDLENBQUM7Z0JBQ2pCLE9BQU8sQ0FBQyxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUM7YUFDaEI7U0FFRjs7Y0FDSyxPQUFPLEdBQVcsSUFBSSxDQUFDLElBQUk7UUFHakMsSUFBSSxPQUFPLEVBQUU7OztrQkFFTCxFQUFFLEdBQUcsT0FBTyxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUM7WUFDN0IsS0FBSyxNQUFNLENBQUMsSUFBSSxFQUFFLEVBQUU7O3NCQUVaLElBQUksR0FBRyxDQUFDLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQztnQkFDekIsSUFBSSxJQUFJLENBQUMsTUFBTSxHQUFHLENBQUMsSUFBSSxJQUFJLENBQUMsQ0FBQyxDQUFDLElBQUksSUFBSSxDQUFDLENBQUMsQ0FBQyxFQUFFO29CQUN6QyxLQUFLLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLEdBQUcsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDO2lCQUMxQjthQUNGO1NBQ0Y7O2NBRUssSUFBSSxHQUFHLElBQUksQ0FBQyxJQUFJOztjQUVoQixPQUFPLEdBQUc7WUFDZCxJQUFJO1lBQ0osSUFBSSxFQUFFLEVBQUUsUUFBUSxFQUFFLElBQUksQ0FBQyxFQUFFLEVBQUUsT0FBTyxFQUFFLElBQUksQ0FBQyxFQUFFLEVBQUUsSUFBSSxFQUFFLEtBQUssRUFBRSxNQUFNLEVBQUUsT0FBTyxFQUFFLE1BQU0sRUFBRSxNQUFNLEVBQUU7U0FDNUY7O2NBR0ssT0FBTyxHQUFHLEdBQUcsQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLE9BQU8sRUFBRSxLQUFLLENBQUM7O2NBRXpDLE1BQU0sR0FBRyxHQUFHLENBQUMsS0FBSyxDQUFDO1lBQ3ZCLEdBQUcsRUFBRSxPQUFPLEVBQUUsSUFBSSxFQUFFLE9BQU87U0FDNUIsQ0FBQztRQUVGLE9BQU8sSUFBSSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsQ0FBQyxJQUFJLENBQUMsUUFBUTs7OztRQUFDLENBQUMsS0FBVSxFQUFFLEVBQUU7WUFDdEQsSUFBSSxLQUFLLFlBQVksZ0JBQWdCLEVBQUU7Z0JBQ3JDLElBQUksQ0FBQyxVQUFVLENBQUMsS0FBSyxDQUFDLENBQUM7YUFDeEI7WUFDRCxPQUFPLEVBQUUsQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUVuQixDQUFDLEVBQUMsQ0FBQyxDQUFDO0lBQ04sQ0FBQzs7OztJQUdELElBQUksS0FBSztRQUNQLE9BQU8sSUFBSSxDQUFDLFFBQVEsQ0FBQyxHQUFHLENBQUMsV0FBVyxDQUFDLENBQUM7SUFDeEMsQ0FBQzs7OztJQUNELElBQUksR0FBRztRQUNMLE9BQU8sSUFBSSxDQUFDLFFBQVEsQ0FBQyxHQUFHLENBQUMsZ0JBQWdCLENBQUMsQ0FBQztJQUM3QyxDQUFDOzs7Ozs7SUFFTyxVQUFVLENBQUMsRUFBb0I7UUFFckMsSUFBSSxFQUFFLFlBQVksWUFBWSxFQUFFOztrQkFFeEIsSUFBSSxHQUFRLEVBQUUsQ0FBQyxJQUFJO1lBQ3pCLElBQUksSUFBSSxFQUFFO2dCQUNSLElBQUksSUFBSSxDQUFDLElBQUksR0FBRyxDQUFDLEVBQUU7b0JBQ2pCLElBQUksQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQztpQkFDN0I7cUJBQ0k7OzswQkFHRyxJQUFJLEdBQVUsSUFBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJO29CQUNyQyxJQUFJLElBQUksRUFBRTt3QkFDUixJQUFJLENBQUMsR0FBRzs7Ozs7d0JBQUMsQ0FBQyxHQUFRLEVBQUUsS0FBYSxFQUFFLEVBQUUsQ0FBQyxHQUFHLENBQUMsSUFBSSxHQUFHLEtBQUssRUFBQyxDQUFDO3FCQUN6RDtpQkFHRjthQUNGO1NBSUY7UUFHRCxPQUFPLEVBQUUsQ0FBQyxFQUFFLENBQUMsQ0FBQztJQUVoQixDQUFDOzs7WUFoSEYsVUFBVTs7OztZQWxCVSxRQUFROzs7Ozs7O0lBb0JmLG1DQUEwQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEluamVjdGFibGUsIEluamVjdG9yIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7XHJcbiAgSHR0cEludGVyY2VwdG9yLFxyXG4gIEh0dHBSZXF1ZXN0LFxyXG4gIEh0dHBIYW5kbGVyLFxyXG4gIEh0dHBTZW50RXZlbnQsXHJcbiAgSHR0cEhlYWRlclJlc3BvbnNlLFxyXG4gIEh0dHBQcm9ncmVzc0V2ZW50LFxyXG4gIEh0dHBSZXNwb25zZSxcclxuICBIdHRwVXNlckV2ZW50LFxyXG4gIEh0dHBSZXNwb25zZUJhc2UsXHJcbn0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uL2h0dHAnO1xyXG5pbXBvcnQgeyBPYnNlcnZhYmxlLCBvZiB9IGZyb20gJ3J4anMnO1xyXG5pbXBvcnQgeyBfSHR0cENsaWVudCB9IGZyb20gJ0BkZWxvbi90aGVtZSc7XHJcbmltcG9ydCB7IG1lcmdlTWFwIH0gZnJvbSAncnhqcy9vcGVyYXRvcnMnO1xyXG5pbXBvcnQgeyBOek1lc3NhZ2VTZXJ2aWNlIH0gZnJvbSAnbmctem9ycm8tYW50ZCc7XHJcbmltcG9ydCB7IFNlcnZpY2VJbmZvIH0gZnJvbSAnLi4vU2VydmljZUNvbmZpZyc7XHJcblxyXG5ASW5qZWN0YWJsZSgpXHJcbmV4cG9ydCBjbGFzcyBHcmlkSW50ZXJjZXB0b3IgaW1wbGVtZW50cyBIdHRwSW50ZXJjZXB0b3Ige1xyXG4gIGNvbnN0cnVjdG9yKHByaXZhdGUgaW5qZWN0b3I6IEluamVjdG9yKSB7IH1cclxuXHJcblxyXG4gIGludGVyY2VwdChcclxuICAgIHJlcTogSHR0cFJlcXVlc3Q8YW55PixcclxuICAgIG5leHQ6IEh0dHBIYW5kbGVyLFxyXG4gICk6IE9ic2VydmFibGU8XHJcbiAgICB8IEh0dHBTZW50RXZlbnRcclxuICAgIHwgSHR0cEhlYWRlclJlc3BvbnNlXHJcbiAgICB8IEh0dHBQcm9ncmVzc0V2ZW50XHJcbiAgICB8IEh0dHBSZXNwb25zZTxhbnk+XHJcbiAgICB8IEh0dHBVc2VyRXZlbnQ8YW55PlxyXG4gID4ge1xyXG4gICAgLy8g57uf5LiA5Yqg5LiK5pyN5Yqh56uv5YmN57yAXHJcbiAgICBsZXQgdXJsID0gcmVxLnVybDtcclxuICAgIGlmICh1cmwgIT09IFwiTW9ocnNzRXhJbnRlcmZhY2UvZ3JpZHN2clwiKSB7XHJcbiAgICAgIHJldHVybiBuZXh0LmhhbmRsZShyZXEpO1xyXG4gICAgfVxyXG5cclxuICAgIHVybCA9IHRoaXMuc2luZm8ubW9sc3NVcmw7XHJcblxyXG4gICAgY29uc3QgYm9keSA9IHJlcS5ib2R5O1xyXG4gICAgY29uc3Qgc25hbWUgPSBib2R5LnNuYW1lO1xyXG4gICAgY29uc3Qgc29ydHMgPSB7fTtcclxuICAgIGNvbnN0IGZpbHRlcnMgPSB7fTtcclxuICAgIGNvbnN0IHN0YWNhbCA9IGJvZHkuc3RhY2FsO1xyXG5cclxuICAgIGZvciAoY29uc3QgcCBpbiBib2R5KSB7XHJcblxyXG4gICAgICBpZiAoIVwic25hbWUscGkscHMsc29ydCxmb3JtLHN0YWNhbCxcIi5pbmNsdWRlcyhwICsgXCIsXCIpKSB7XHJcblxyXG4gICAgICAgIGNvbnN0IHYgPSBib2R5W3BdO1xyXG4gICAgICAgIGZpbHRlcnNbcF0gPSB2O1xyXG4gICAgICB9XHJcblxyXG4gICAgfVxyXG4gICAgY29uc3Qgc29ydHN0cjogc3RyaW5nID0gYm9keS5zb3J0O1xyXG5cclxuXHJcbiAgICBpZiAoc29ydHN0cikge1xyXG4gICAgICAvLyBBQUUwNzAuZGVzY2VuZC1BS0MyNTguZGVzY2VuZFxyXG4gICAgICBjb25zdCBzcyA9IHNvcnRzdHIuc3BsaXQoXCItXCIpO1xyXG4gICAgICBmb3IgKGNvbnN0IHMgb2Ygc3MpIHtcclxuXHJcbiAgICAgICAgY29uc3Qgc29ydCA9IHMuc3BsaXQoXCIuXCIpO1xyXG4gICAgICAgIGlmIChzb3J0Lmxlbmd0aCA+IDEgJiYgc29ydFswXSAmJiBzb3J0WzFdKSB7XHJcbiAgICAgICAgICBzb3J0c1tzb3J0WzBdXSA9IHNvcnRbMV07XHJcbiAgICAgICAgfVxyXG4gICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgY29uc3QgcGFyYSA9IGJvZHkuZm9ybTtcclxuXHJcbiAgICBjb25zdCBuZXdib2R5ID0ge1xyXG4gICAgICBwYXJhLFxyXG4gICAgICBwYWdlOiB7IFBBR0VTSVpFOiBib2R5LnBzLCBQQUdFSURYOiBib2R5LnBpLCBTT1JUOiBzb3J0cywgRklMVEVSOiBmaWx0ZXJzLCBTVEFDQUw6IHN0YWNhbCB9LFxyXG4gICAgfVxyXG5cclxuXHJcbiAgICBjb25zdCBoZWFkZXJzID0gcmVxLmhlYWRlcnMuc2V0KFwiU05BTUVcIiwgc25hbWUpO1xyXG5cclxuICAgIGNvbnN0IG5ld1JlcSA9IHJlcS5jbG9uZSh7XHJcbiAgICAgIHVybCwgaGVhZGVycywgYm9keTogbmV3Ym9keVxyXG4gICAgfSk7XHJcblxyXG4gICAgcmV0dXJuIG5leHQuaGFuZGxlKG5ld1JlcSkucGlwZShtZXJnZU1hcCgoZXZlbnQ6IGFueSkgPT4ge1xyXG4gICAgICBpZiAoZXZlbnQgaW5zdGFuY2VvZiBIdHRwUmVzcG9uc2VCYXNlKSB7XHJcbiAgICAgICAgdGhpcy5oYW5kbGVEYXRhKGV2ZW50KTtcclxuICAgICAgfVxyXG4gICAgICByZXR1cm4gb2YoZXZlbnQpO1xyXG5cclxuICAgIH0pKTtcclxuICB9XHJcblxyXG5cclxuICBnZXQgc2luZm8oKTogU2VydmljZUluZm8ge1xyXG4gICAgcmV0dXJuIHRoaXMuaW5qZWN0b3IuZ2V0KFNlcnZpY2VJbmZvKTtcclxuICB9XHJcbiAgZ2V0IG1zZygpOiBOek1lc3NhZ2VTZXJ2aWNlIHtcclxuICAgIHJldHVybiB0aGlzLmluamVjdG9yLmdldChOek1lc3NhZ2VTZXJ2aWNlKTtcclxuICB9XHJcblxyXG4gIHByaXZhdGUgaGFuZGxlRGF0YShldjogSHR0cFJlc3BvbnNlQmFzZSk6IE9ic2VydmFibGU8YW55PiB7XHJcblxyXG4gICAgaWYgKGV2IGluc3RhbmNlb2YgSHR0cFJlc3BvbnNlKSB7XHJcblxyXG4gICAgICBjb25zdCBib2R5OiBhbnkgPSBldi5ib2R5O1xyXG4gICAgICBpZiAoYm9keSkge1xyXG4gICAgICAgIGlmIChib2R5LmNvZGUgPCAxKSB7XHJcbiAgICAgICAgICB0aGlzLm1zZy5lcnJvcihib2R5LmVycm1zZyk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGVsc2Uge1xyXG4gICAgICAgICAgLy8g5re75Yqg5bqP5Y+3XHJcblxyXG4gICAgICAgICAgY29uc3QgbGlzdDogYW55W10gPSBib2R5Lm1lc3NhZ2UubGlzdDtcclxuICAgICAgICAgIGlmIChsaXN0KSB7XHJcbiAgICAgICAgICAgIGxpc3QubWFwKChyb3c6IGFueSwgaW5kZXg6IG51bWJlcikgPT4gcm93Ll9pZHggPSBpbmRleCk7XHJcbiAgICAgICAgICB9XHJcblxyXG5cclxuICAgICAgICB9XHJcbiAgICAgIH1cclxuXHJcblxyXG5cclxuICAgIH1cclxuXHJcblxyXG4gICAgcmV0dXJuIG9mKGV2KTtcclxuXHJcbiAgfVxyXG5cclxuXHJcbn0iXX0=