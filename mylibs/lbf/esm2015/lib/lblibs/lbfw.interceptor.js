/**
 * @fileoverview added by tsickle
 * Generated from: lib/lblibs/lbfw.interceptor.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Injectable, Injector } from '@angular/core';
import { Router } from '@angular/router';
import { HttpErrorResponse, HttpResponseBase, HttpResponse } from '@angular/common/http';
import { of, throwError } from 'rxjs';
import { mergeMap, catchError } from 'rxjs/operators';
import { NzMessageService, NzNotificationService } from 'ng-zorro-antd';
import { _HttpClient } from '@delon/theme';
import { DA_SERVICE_TOKEN } from '@delon/auth';
import { ServiceInfo } from '../ServiceConfig';
/** @type {?} */
const CODEMESSAGE = {
    200: '服务器成功返回请求的数据。',
    201: '新建或修改数据成功。',
    202: '一个请求已经进入后台排队（异步任务）。',
    204: '删除数据成功。',
    400: '发出的请求有错误，服务器没有进行新建或修改数据的操作。',
    401: '用户未登录或者登录已过期。',
    403: '用户得到授权，但是访问是被禁止的。',
    404: '发出的请求针对的是不存在的记录，服务器没有进行操作。',
    406: '请求的格式不可得。',
    410: '请求的资源被永久删除，且不会再得到的。',
    422: '当创建一个对象时，发生一个验证错误。',
    500: '服务器发生错误，请检查服务器。',
    502: '网关错误。',
    503: '服务不可用，服务器暂时过载或维护。',
    504: '网关超时。',
};
/**
 * 默认HTTP拦截器，其注册细节见 `app.module.ts`
 */
export class LbfwInterceptor {
    /**
     * @param {?} injector
     */
    constructor(injector) {
        this.injector = injector;
    }
    /**
     * @return {?}
     */
    get msg() {
        return this.injector.get(NzMessageService);
    }
    /**
     * @private
     * @param {?} url
     * @return {?}
     */
    goTo(url) {
        setTimeout((/**
         * @return {?}
         */
        () => this.injector.get(Router).navigateByUrl(url)));
    }
    /**
     * @return {?}
     */
    get sinfo() {
        return this.injector.get(ServiceInfo);
    }
    /**
     * @private
     * @param {?} ev
     * @return {?}
     */
    checkStatus(ev) {
        if (ev.status >= 200 && ev.status < 300)
            return;
        /** @type {?} */
        const errortext = CODEMESSAGE[ev.status] || ev.statusText;
        this.injector.get(NzNotificationService).error(`请求错误 ${ev.status}`, errortext);
    }
    /**
     * @private
     * @param {?} ev
     * @return {?}
     */
    handleData(ev) {
        // 可能会因为 `throw` 导出无法执行 `_HttpClient` 的 `end()` 操作
        if (ev.status > 0) {
            this.injector.get(_HttpClient).end();
        }
        this.checkStatus(ev);
        // 业务处理：一些通用操作
        switch (ev.status) {
            case 200:
                // 处理未登录的错误 {code: -322, errmsg: "未登录或者登陆已经过期！"}
                if (ev instanceof HttpResponse) {
                    /** @type {?} */
                    const body = ev.body;
                    if (body && body.code === -322) {
                        this.msg.error("未登录或者登陆已经过期！");
                        // 继续抛出错误中断后续所有 Pipe、subscribe 操作，因此：
                        // this.http.get('/').subscribe() 并不会触发
                        this.goTo('/passport/login');
                        break;
                    }
                    else {
                        // 重新修改 `body` 内容为 `response` 内容，对于绝大多数场景已经无须再关心业务状态码
                        // 或者依然保持完整的格式
                        return of(ev);
                    }
                }
                break;
            case 401: // 未登录状态码
                // 请求错误 401: https://preview.pro.ant.design/api/401 用户没有权限（令牌、用户名、密码错误）。
                ((/** @type {?} */ (this.injector.get(DA_SERVICE_TOKEN)))).clear();
                this.goTo('/passport/login');
                break;
            case 403:
            case 404:
            case 500:
                this.goTo(`/exception/${ev.status}`);
                break;
            default:
                if (ev instanceof HttpErrorResponse) {
                    console.warn('未可知错误，大部分是由于后端不支持CORS或无效配置引起', ev);
                    return throwError(ev);
                }
                break;
        }
        return of(ev);
    }
    /**
     * @param {?} req
     * @param {?} next
     * @return {?}
     */
    intercept(req, next) {
        // 统一加上服务端前缀
        /** @type {?} */
        const url = this.sinfo.baseUrl + req.url;
        // if (!url.startsWith('https://') && !url.startsWith('http://')) {
        //   url = environment.SERVER_URL + url;
        // }
        // 加上统一的请求参数
        /** @type {?} */
        const sname = req.headers.get("SNAME");
        /** @type {?} */
        const para = JSON.stringify(req.body);
        /** @type {?} */
        const appid = this.sinfo.APPID;
        /** @type {?} */
        const timestamp = this.sinfo.getTimestamp().toString();
        /** @type {?} */
        const sign = this.sinfo.getSign(sname, timestamp, para);
        /** @type {?} */
        const token = this.sinfo.LOGTOKEN;
        /** @type {?} */
        const version = this.sinfo.VERSION;
        /** @type {?} */
        const headers = req.headers.set("APPID", appid).set("TIMESTAMP", timestamp).set("SIGN", sign).set("LOGINTOKEN", token).set("VERSION", version);
        /** @type {?} */
        const newReq = req.clone({
            url, headers
        });
        return next.handle(newReq).pipe(mergeMap((/**
         * @param {?} event
         * @return {?}
         */
        (event) => {
            // 允许统一对请求错误处理
            if (event instanceof HttpResponseBase)
                return this.handleData(event);
            // 若一切都正常，则后续操作
            return of(event);
        })), catchError((/**
         * @param {?} err
         * @return {?}
         */
        (err) => this.handleData(err))));
    }
}
LbfwInterceptor.decorators = [
    { type: Injectable }
];
/** @nocollapse */
LbfwInterceptor.ctorParameters = () => [
    { type: Injector }
];
if (false) {
    /**
     * @type {?}
     * @private
     */
    LbfwInterceptor.prototype.injector;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibGJmdy5pbnRlcmNlcHRvci5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2xiZi8iLCJzb3VyY2VzIjpbImxpYi9sYmxpYnMvbGJmdy5pbnRlcmNlcHRvci50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7OztBQUFBLE9BQU8sRUFBRSxVQUFVLEVBQUUsUUFBUSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQ3JELE9BQU8sRUFBRSxNQUFNLEVBQUUsTUFBTSxpQkFBaUIsQ0FBQztBQUN6QyxPQUFPLEVBQTZDLGlCQUFpQixFQUFhLGdCQUFnQixFQUFFLFlBQVksRUFBRSxNQUFNLHNCQUFzQixDQUFDO0FBQy9JLE9BQU8sRUFBYyxFQUFFLEVBQUUsVUFBVSxFQUFFLE1BQU0sTUFBTSxDQUFDO0FBQ2xELE9BQU8sRUFBRSxRQUFRLEVBQUUsVUFBVSxFQUFFLE1BQU0sZ0JBQWdCLENBQUM7QUFDdEQsT0FBTyxFQUFFLGdCQUFnQixFQUFFLHFCQUFxQixFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQ3hFLE9BQU8sRUFBRSxXQUFXLEVBQUUsTUFBTSxjQUFjLENBQUM7QUFDM0MsT0FBTyxFQUFFLGdCQUFnQixFQUFpQixNQUFNLGFBQWEsQ0FBQztBQUM5RCxPQUFPLEVBQUUsV0FBVyxFQUFFLE1BQU0sa0JBQWtCLENBQUM7O01BR3pDLFdBQVcsR0FBRztJQUNsQixHQUFHLEVBQUUsZUFBZTtJQUNwQixHQUFHLEVBQUUsWUFBWTtJQUNqQixHQUFHLEVBQUUscUJBQXFCO0lBQzFCLEdBQUcsRUFBRSxTQUFTO0lBQ2QsR0FBRyxFQUFFLDZCQUE2QjtJQUNsQyxHQUFHLEVBQUUsZUFBZTtJQUNwQixHQUFHLEVBQUUsbUJBQW1CO0lBQ3hCLEdBQUcsRUFBRSw0QkFBNEI7SUFDakMsR0FBRyxFQUFFLFdBQVc7SUFDaEIsR0FBRyxFQUFFLHFCQUFxQjtJQUMxQixHQUFHLEVBQUUsb0JBQW9CO0lBQ3pCLEdBQUcsRUFBRSxpQkFBaUI7SUFDdEIsR0FBRyxFQUFFLE9BQU87SUFDWixHQUFHLEVBQUUsbUJBQW1CO0lBQ3hCLEdBQUcsRUFBRSxPQUFPO0NBQ2I7Ozs7QUFNRCxNQUFNLE9BQU8sZUFBZTs7OztJQUMxQixZQUFvQixRQUFrQjtRQUFsQixhQUFRLEdBQVIsUUFBUSxDQUFVO0lBQUksQ0FBQzs7OztJQUUzQyxJQUFJLEdBQUc7UUFDTCxPQUFPLElBQUksQ0FBQyxRQUFRLENBQUMsR0FBRyxDQUFDLGdCQUFnQixDQUFDLENBQUM7SUFDN0MsQ0FBQzs7Ozs7O0lBRU8sSUFBSSxDQUFDLEdBQVc7UUFDdEIsVUFBVTs7O1FBQUMsR0FBRyxFQUFFLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxHQUFHLENBQUMsTUFBTSxDQUFDLENBQUMsYUFBYSxDQUFDLEdBQUcsQ0FBQyxFQUFDLENBQUM7SUFDakUsQ0FBQzs7OztJQUdELElBQUksS0FBSztRQUNQLE9BQU8sSUFBSSxDQUFDLFFBQVEsQ0FBQyxHQUFHLENBQUMsV0FBVyxDQUFDLENBQUM7SUFDeEMsQ0FBQzs7Ozs7O0lBR08sV0FBVyxDQUFDLEVBQW9CO1FBQ3RDLElBQUksRUFBRSxDQUFDLE1BQU0sSUFBSSxHQUFHLElBQUksRUFBRSxDQUFDLE1BQU0sR0FBRyxHQUFHO1lBQUUsT0FBTzs7Y0FFMUMsU0FBUyxHQUFHLFdBQVcsQ0FBQyxFQUFFLENBQUMsTUFBTSxDQUFDLElBQUksRUFBRSxDQUFDLFVBQVU7UUFDekQsSUFBSSxDQUFDLFFBQVEsQ0FBQyxHQUFHLENBQUMscUJBQXFCLENBQUMsQ0FBQyxLQUFLLENBQzVDLFFBQVEsRUFBRSxDQUFDLE1BQU0sRUFBRSxFQUNuQixTQUFTLENBQ1YsQ0FBQztJQUNKLENBQUM7Ozs7OztJQUVPLFVBQVUsQ0FBQyxFQUFvQjtRQUVyQyxrREFBa0Q7UUFDbEQsSUFBSSxFQUFFLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRTtZQUNqQixJQUFJLENBQUMsUUFBUSxDQUFDLEdBQUcsQ0FBQyxXQUFXLENBQUMsQ0FBQyxHQUFHLEVBQUUsQ0FBQztTQUN0QztRQUNELElBQUksQ0FBQyxXQUFXLENBQUMsRUFBRSxDQUFDLENBQUM7UUFDckIsY0FBYztRQUNkLFFBQVEsRUFBRSxDQUFDLE1BQU0sRUFBRTtZQUNqQixLQUFLLEdBQUc7Z0JBRU4sZ0RBQWdEO2dCQUNoRCxJQUFJLEVBQUUsWUFBWSxZQUFZLEVBQUU7OzBCQUV4QixJQUFJLEdBQVEsRUFBRSxDQUFDLElBQUk7b0JBQ3pCLElBQUksSUFBSSxJQUFJLElBQUksQ0FBQyxJQUFJLEtBQUssQ0FBQyxHQUFHLEVBQUU7d0JBQzlCLElBQUksQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDLGNBQWMsQ0FBQyxDQUFDO3dCQUMvQixxQ0FBcUM7d0JBQ3JDLHVDQUF1Qzt3QkFDdkMsSUFBSSxDQUFDLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDO3dCQUM3QixNQUFNO3FCQUNQO3lCQUFNO3dCQUNMLHFEQUFxRDt3QkFDckQsY0FBYzt3QkFDZCxPQUFPLEVBQUUsQ0FBQyxFQUFFLENBQUMsQ0FBQztxQkFDZjtpQkFDRjtnQkFFRCxNQUFNO1lBQ1IsS0FBSyxHQUFHLEVBQUUsU0FBUztnQkFDakIsd0VBQXdFO2dCQUN4RSxDQUFDLG1CQUFBLElBQUksQ0FBQyxRQUFRLENBQUMsR0FBRyxDQUFDLGdCQUFnQixDQUFDLEVBQWlCLENBQUMsQ0FBQyxLQUFLLEVBQUUsQ0FBQztnQkFDL0QsSUFBSSxDQUFDLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDO2dCQUM3QixNQUFNO1lBQ1IsS0FBSyxHQUFHLENBQUM7WUFDVCxLQUFLLEdBQUcsQ0FBQztZQUNULEtBQUssR0FBRztnQkFDTixJQUFJLENBQUMsSUFBSSxDQUFDLGNBQWMsRUFBRSxDQUFDLE1BQU0sRUFBRSxDQUFDLENBQUM7Z0JBQ3JDLE1BQU07WUFDUjtnQkFDRSxJQUFJLEVBQUUsWUFBWSxpQkFBaUIsRUFBRTtvQkFDbkMsT0FBTyxDQUFDLElBQUksQ0FBQyw4QkFBOEIsRUFBRSxFQUFFLENBQUMsQ0FBQztvQkFDakQsT0FBTyxVQUFVLENBQUMsRUFBRSxDQUFDLENBQUM7aUJBQ3ZCO2dCQUNELE1BQU07U0FDVDtRQUNELE9BQU8sRUFBRSxDQUFDLEVBQUUsQ0FBQyxDQUFDO0lBQ2hCLENBQUM7Ozs7OztJQUVELFNBQVMsQ0FBQyxHQUFxQixFQUFFLElBQWlCOzs7Y0FFMUMsR0FBRyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsT0FBTyxHQUFHLEdBQUcsQ0FBQyxHQUFHOzs7Ozs7Y0FNbEMsS0FBSyxHQUFHLEdBQUcsQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLE9BQU8sQ0FBQzs7Y0FDaEMsSUFBSSxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQzs7Y0FDL0IsS0FBSyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsS0FBSzs7Y0FDeEIsU0FBUyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsWUFBWSxFQUFFLENBQUMsUUFBUSxFQUFFOztjQUNoRCxJQUFJLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsS0FBSyxFQUFFLFNBQVMsRUFBRSxJQUFJLENBQUM7O2NBQ2pELEtBQUssR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLFFBQVE7O2NBQzNCLE9BQU8sR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLE9BQU87O2NBRTVCLE9BQU8sR0FBRyxHQUFHLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxPQUFPLEVBQUUsS0FBSyxDQUFDLENBQUMsR0FBRyxDQUFDLFdBQVcsRUFBRSxTQUFTLENBQUMsQ0FBQyxHQUFHLENBQUMsTUFBTSxFQUFFLElBQUksQ0FBQyxDQUFDLEdBQUcsQ0FBQyxZQUFZLEVBQUUsS0FBSyxDQUFDLENBQUMsR0FBRyxDQUFDLFNBQVMsRUFBRSxPQUFPLENBQUM7O2NBR3hJLE1BQU0sR0FBRyxHQUFHLENBQUMsS0FBSyxDQUFDO1lBQ3ZCLEdBQUcsRUFBRSxPQUFPO1NBQ2IsQ0FBQztRQUdGLE9BQU8sSUFBSSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsQ0FBQyxJQUFJLENBQzdCLFFBQVE7Ozs7UUFBQyxDQUFDLEtBQVUsRUFBRSxFQUFFO1lBQ3RCLGNBQWM7WUFDZCxJQUFJLEtBQUssWUFBWSxnQkFBZ0I7Z0JBQ25DLE9BQU8sSUFBSSxDQUFDLFVBQVUsQ0FBQyxLQUFLLENBQUMsQ0FBQztZQUNoQyxlQUFlO1lBQ2YsT0FBTyxFQUFFLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDbkIsQ0FBQyxFQUFDLEVBQ0YsVUFBVTs7OztRQUFDLENBQUMsR0FBc0IsRUFBRSxFQUFFLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxHQUFHLENBQUMsRUFBQyxDQUM3RCxDQUFDO0lBQ0osQ0FBQzs7O1lBL0dGLFVBQVU7Ozs7WUFoQ1UsUUFBUTs7Ozs7OztJQWtDZixtQ0FBMEIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBJbmplY3RhYmxlLCBJbmplY3RvciB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBSb3V0ZXIgfSBmcm9tICdAYW5ndWxhci9yb3V0ZXInO1xyXG5pbXBvcnQgeyBIdHRwSW50ZXJjZXB0b3IsIEh0dHBSZXF1ZXN0LCBIdHRwSGFuZGxlciwgSHR0cEVycm9yUmVzcG9uc2UsIEh0dHBFdmVudCwgSHR0cFJlc3BvbnNlQmFzZSwgSHR0cFJlc3BvbnNlIH0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uL2h0dHAnO1xyXG5pbXBvcnQgeyBPYnNlcnZhYmxlLCBvZiwgdGhyb3dFcnJvciB9IGZyb20gJ3J4anMnO1xyXG5pbXBvcnQgeyBtZXJnZU1hcCwgY2F0Y2hFcnJvciB9IGZyb20gJ3J4anMvb3BlcmF0b3JzJztcclxuaW1wb3J0IHsgTnpNZXNzYWdlU2VydmljZSwgTnpOb3RpZmljYXRpb25TZXJ2aWNlIH0gZnJvbSAnbmctem9ycm8tYW50ZCc7XHJcbmltcG9ydCB7IF9IdHRwQ2xpZW50IH0gZnJvbSAnQGRlbG9uL3RoZW1lJztcclxuaW1wb3J0IHsgREFfU0VSVklDRV9UT0tFTiwgSVRva2VuU2VydmljZSB9IGZyb20gJ0BkZWxvbi9hdXRoJztcclxuaW1wb3J0IHsgU2VydmljZUluZm8gfSBmcm9tICcuLi9TZXJ2aWNlQ29uZmlnJztcclxuXHJcblxyXG5jb25zdCBDT0RFTUVTU0FHRSA9IHtcclxuICAyMDA6ICfmnI3liqHlmajmiJDlip/ov5Tlm57or7fmsYLnmoTmlbDmja7jgIInLFxyXG4gIDIwMTogJ+aWsOW7uuaIluS/ruaUueaVsOaNruaIkOWKn+OAgicsXHJcbiAgMjAyOiAn5LiA5Liq6K+35rGC5bey57uP6L+b5YWl5ZCO5Y+w5o6S6Zif77yI5byC5q2l5Lu75Yqh77yJ44CCJyxcclxuICAyMDQ6ICfliKDpmaTmlbDmja7miJDlip/jgIInLFxyXG4gIDQwMDogJ+WPkeWHuueahOivt+axguaciemUmeivr++8jOacjeWKoeWZqOayoeaciei/m+ihjOaWsOW7uuaIluS/ruaUueaVsOaNrueahOaTjeS9nOOAgicsXHJcbiAgNDAxOiAn55So5oi35pyq55m75b2V5oiW6ICF55m75b2V5bey6L+H5pyf44CCJyxcclxuICA0MDM6ICfnlKjmiLflvpfliLDmjojmnYPvvIzkvYbmmK/orr/pl67mmK/ooqvnpoHmraLnmoTjgIInLFxyXG4gIDQwNDogJ+WPkeWHuueahOivt+axgumSiOWvueeahOaYr+S4jeWtmOWcqOeahOiusOW9le+8jOacjeWKoeWZqOayoeaciei/m+ihjOaTjeS9nOOAgicsXHJcbiAgNDA2OiAn6K+35rGC55qE5qC85byP5LiN5Y+v5b6X44CCJyxcclxuICA0MTA6ICfor7fmsYLnmoTotYTmupDooqvmsLjkuYXliKDpmaTvvIzkuJTkuI3kvJrlho3lvpfliLDnmoTjgIInLFxyXG4gIDQyMjogJ+W9k+WIm+W7uuS4gOS4quWvueixoeaXtu+8jOWPkeeUn+S4gOS4qumqjOivgemUmeivr+OAgicsXHJcbiAgNTAwOiAn5pyN5Yqh5Zmo5Y+R55Sf6ZSZ6K+v77yM6K+35qOA5p+l5pyN5Yqh5Zmo44CCJyxcclxuICA1MDI6ICfnvZHlhbPplJnor6/jgIInLFxyXG4gIDUwMzogJ+acjeWKoeS4jeWPr+eUqO+8jOacjeWKoeWZqOaaguaXtui/h+i9veaIlue7tOaKpOOAgicsXHJcbiAgNTA0OiAn572R5YWz6LaF5pe244CCJyxcclxufTtcclxuXHJcbi8qKlxyXG4gKiDpu5jorqRIVFRQ5oum5oiq5Zmo77yM5YW25rOo5YaM57uG6IqC6KeBIGBhcHAubW9kdWxlLnRzYFxyXG4gKi9cclxuQEluamVjdGFibGUoKVxyXG5leHBvcnQgY2xhc3MgTGJmd0ludGVyY2VwdG9yIGltcGxlbWVudHMgSHR0cEludGVyY2VwdG9yIHtcclxuICBjb25zdHJ1Y3Rvcihwcml2YXRlIGluamVjdG9yOiBJbmplY3RvcikgeyB9XHJcblxyXG4gIGdldCBtc2coKTogTnpNZXNzYWdlU2VydmljZSB7XHJcbiAgICByZXR1cm4gdGhpcy5pbmplY3Rvci5nZXQoTnpNZXNzYWdlU2VydmljZSk7XHJcbiAgfVxyXG5cclxuICBwcml2YXRlIGdvVG8odXJsOiBzdHJpbmcpIHtcclxuICAgIHNldFRpbWVvdXQoKCkgPT4gdGhpcy5pbmplY3Rvci5nZXQoUm91dGVyKS5uYXZpZ2F0ZUJ5VXJsKHVybCkpO1xyXG4gIH1cclxuXHJcblxyXG4gIGdldCBzaW5mbygpOiBTZXJ2aWNlSW5mbyB7XHJcbiAgICByZXR1cm4gdGhpcy5pbmplY3Rvci5nZXQoU2VydmljZUluZm8pO1xyXG4gIH1cclxuXHJcblxyXG4gIHByaXZhdGUgY2hlY2tTdGF0dXMoZXY6IEh0dHBSZXNwb25zZUJhc2UpIHtcclxuICAgIGlmIChldi5zdGF0dXMgPj0gMjAwICYmIGV2LnN0YXR1cyA8IDMwMCkgcmV0dXJuO1xyXG5cclxuICAgIGNvbnN0IGVycm9ydGV4dCA9IENPREVNRVNTQUdFW2V2LnN0YXR1c10gfHwgZXYuc3RhdHVzVGV4dDtcclxuICAgIHRoaXMuaW5qZWN0b3IuZ2V0KE56Tm90aWZpY2F0aW9uU2VydmljZSkuZXJyb3IoXHJcbiAgICAgIGDor7fmsYLplJnor68gJHtldi5zdGF0dXN9YCxcclxuICAgICAgZXJyb3J0ZXh0XHJcbiAgICApO1xyXG4gIH1cclxuXHJcbiAgcHJpdmF0ZSBoYW5kbGVEYXRhKGV2OiBIdHRwUmVzcG9uc2VCYXNlKTogT2JzZXJ2YWJsZTxhbnk+IHtcclxuXHJcbiAgICAvLyDlj6/og73kvJrlm6DkuLogYHRocm93YCDlr7zlh7rml6Dms5XmiafooYwgYF9IdHRwQ2xpZW50YCDnmoQgYGVuZCgpYCDmk43kvZxcclxuICAgIGlmIChldi5zdGF0dXMgPiAwKSB7XHJcbiAgICAgIHRoaXMuaW5qZWN0b3IuZ2V0KF9IdHRwQ2xpZW50KS5lbmQoKTtcclxuICAgIH1cclxuICAgIHRoaXMuY2hlY2tTdGF0dXMoZXYpO1xyXG4gICAgLy8g5Lia5Yqh5aSE55CG77ya5LiA5Lqb6YCa55So5pON5L2cXHJcbiAgICBzd2l0Y2ggKGV2LnN0YXR1cykge1xyXG4gICAgICBjYXNlIDIwMDpcclxuXHJcbiAgICAgICAgLy8g5aSE55CG5pyq55m75b2V55qE6ZSZ6K+vIHtjb2RlOiAtMzIyLCBlcnJtc2c6IFwi5pyq55m75b2V5oiW6ICF55m76ZmG5bey57uP6L+H5pyf77yBXCJ9XHJcbiAgICAgICAgaWYgKGV2IGluc3RhbmNlb2YgSHR0cFJlc3BvbnNlKSB7XHJcblxyXG4gICAgICAgICAgY29uc3QgYm9keTogYW55ID0gZXYuYm9keTtcclxuICAgICAgICAgIGlmIChib2R5ICYmIGJvZHkuY29kZSA9PT0gLTMyMikge1xyXG4gICAgICAgICAgICB0aGlzLm1zZy5lcnJvcihcIuacqueZu+W9leaIluiAheeZu+mZhuW3sue7j+i/h+acn++8gVwiKTtcclxuICAgICAgICAgICAgLy8g57un57ut5oqb5Ye66ZSZ6K+v5Lit5pat5ZCO57ut5omA5pyJIFBpcGXjgIFzdWJzY3JpYmUg5pON5L2c77yM5Zug5q2k77yaXHJcbiAgICAgICAgICAgIC8vIHRoaXMuaHR0cC5nZXQoJy8nKS5zdWJzY3JpYmUoKSDlubbkuI3kvJrop6blj5FcclxuICAgICAgICAgICAgdGhpcy5nb1RvKCcvcGFzc3BvcnQvbG9naW4nKTtcclxuICAgICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAvLyDph43mlrDkv67mlLkgYGJvZHlgIOWGheWuueS4uiBgcmVzcG9uc2VgIOWGheWuue+8jOWvueS6jue7neWkp+WkmuaVsOWcuuaZr+W3sue7j+aXoOmhu+WGjeWFs+W/g+S4muWKoeeKtuaAgeeggVxyXG4gICAgICAgICAgICAvLyDmiJbogIXkvp3nhLbkv53mjIHlrozmlbTnmoTmoLzlvI9cclxuICAgICAgICAgICAgcmV0dXJuIG9mKGV2KTtcclxuICAgICAgICAgIH1cclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGJyZWFrO1xyXG4gICAgICBjYXNlIDQwMTogLy8g5pyq55m75b2V54q25oCB56CBXHJcbiAgICAgICAgLy8g6K+35rGC6ZSZ6K+vIDQwMTogaHR0cHM6Ly9wcmV2aWV3LnByby5hbnQuZGVzaWduL2FwaS80MDEg55So5oi35rKh5pyJ5p2D6ZmQ77yI5Luk54mM44CB55So5oi35ZCN44CB5a+G56CB6ZSZ6K+v77yJ44CCXHJcbiAgICAgICAgKHRoaXMuaW5qZWN0b3IuZ2V0KERBX1NFUlZJQ0VfVE9LRU4pIGFzIElUb2tlblNlcnZpY2UpLmNsZWFyKCk7XHJcbiAgICAgICAgdGhpcy5nb1RvKCcvcGFzc3BvcnQvbG9naW4nKTtcclxuICAgICAgICBicmVhaztcclxuICAgICAgY2FzZSA0MDM6XHJcbiAgICAgIGNhc2UgNDA0OlxyXG4gICAgICBjYXNlIDUwMDpcclxuICAgICAgICB0aGlzLmdvVG8oYC9leGNlcHRpb24vJHtldi5zdGF0dXN9YCk7XHJcbiAgICAgICAgYnJlYWs7XHJcbiAgICAgIGRlZmF1bHQ6XHJcbiAgICAgICAgaWYgKGV2IGluc3RhbmNlb2YgSHR0cEVycm9yUmVzcG9uc2UpIHtcclxuICAgICAgICAgIGNvbnNvbGUud2Fybign5pyq5Y+v55+l6ZSZ6K+v77yM5aSn6YOo5YiG5piv55Sx5LqO5ZCO56uv5LiN5pSv5oyBQ09SU+aIluaXoOaViOmFjee9ruW8lei1tycsIGV2KTtcclxuICAgICAgICAgIHJldHVybiB0aHJvd0Vycm9yKGV2KTtcclxuICAgICAgICB9XHJcbiAgICAgICAgYnJlYWs7XHJcbiAgICB9XHJcbiAgICByZXR1cm4gb2YoZXYpO1xyXG4gIH1cclxuXHJcbiAgaW50ZXJjZXB0KHJlcTogSHR0cFJlcXVlc3Q8YW55PiwgbmV4dDogSHR0cEhhbmRsZXIpOiBPYnNlcnZhYmxlPEh0dHBFdmVudDxhbnk+PiB7XHJcbiAgICAvLyDnu5/kuIDliqDkuIrmnI3liqHnq6/liY3nvIBcclxuICAgIGNvbnN0IHVybCA9IHRoaXMuc2luZm8uYmFzZVVybCArIHJlcS51cmw7XHJcbiAgICAvLyBpZiAoIXVybC5zdGFydHNXaXRoKCdodHRwczovLycpICYmICF1cmwuc3RhcnRzV2l0aCgnaHR0cDovLycpKSB7XHJcbiAgICAvLyAgIHVybCA9IGVudmlyb25tZW50LlNFUlZFUl9VUkwgKyB1cmw7XHJcbiAgICAvLyB9XHJcblxyXG4gICAgLy8g5Yqg5LiK57uf5LiA55qE6K+35rGC5Y+C5pWwXHJcbiAgICBjb25zdCBzbmFtZSA9IHJlcS5oZWFkZXJzLmdldChcIlNOQU1FXCIpO1xyXG4gICAgY29uc3QgcGFyYSA9IEpTT04uc3RyaW5naWZ5KHJlcS5ib2R5KTtcclxuICAgIGNvbnN0IGFwcGlkID0gdGhpcy5zaW5mby5BUFBJRDtcclxuICAgIGNvbnN0IHRpbWVzdGFtcCA9IHRoaXMuc2luZm8uZ2V0VGltZXN0YW1wKCkudG9TdHJpbmcoKTtcclxuICAgIGNvbnN0IHNpZ24gPSB0aGlzLnNpbmZvLmdldFNpZ24oc25hbWUsIHRpbWVzdGFtcCwgcGFyYSk7XHJcbiAgICBjb25zdCB0b2tlbiA9IHRoaXMuc2luZm8uTE9HVE9LRU47XHJcbiAgICBjb25zdCB2ZXJzaW9uID0gdGhpcy5zaW5mby5WRVJTSU9OO1xyXG5cclxuICAgIGNvbnN0IGhlYWRlcnMgPSByZXEuaGVhZGVycy5zZXQoXCJBUFBJRFwiLCBhcHBpZCkuc2V0KFwiVElNRVNUQU1QXCIsIHRpbWVzdGFtcCkuc2V0KFwiU0lHTlwiLCBzaWduKS5zZXQoXCJMT0dJTlRPS0VOXCIsIHRva2VuKS5zZXQoXCJWRVJTSU9OXCIsIHZlcnNpb24pO1xyXG5cclxuXHJcbiAgICBjb25zdCBuZXdSZXEgPSByZXEuY2xvbmUoe1xyXG4gICAgICB1cmwsIGhlYWRlcnNcclxuICAgIH0pO1xyXG5cclxuXHJcbiAgICByZXR1cm4gbmV4dC5oYW5kbGUobmV3UmVxKS5waXBlKFxyXG4gICAgICBtZXJnZU1hcCgoZXZlbnQ6IGFueSkgPT4ge1xyXG4gICAgICAgIC8vIOWFgeiuuOe7n+S4gOWvueivt+axgumUmeivr+WkhOeQhlxyXG4gICAgICAgIGlmIChldmVudCBpbnN0YW5jZW9mIEh0dHBSZXNwb25zZUJhc2UpXHJcbiAgICAgICAgICByZXR1cm4gdGhpcy5oYW5kbGVEYXRhKGV2ZW50KTtcclxuICAgICAgICAvLyDoi6XkuIDliIfpg73mraPluLjvvIzliJnlkI7nu63mk43kvZxcclxuICAgICAgICByZXR1cm4gb2YoZXZlbnQpO1xyXG4gICAgICB9KSxcclxuICAgICAgY2F0Y2hFcnJvcigoZXJyOiBIdHRwRXJyb3JSZXNwb25zZSkgPT4gdGhpcy5oYW5kbGVEYXRhKGVycikpLFxyXG4gICAgKTtcclxuICB9XHJcbn1cclxuIl19