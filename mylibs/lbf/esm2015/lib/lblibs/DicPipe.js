/**
 * @fileoverview added by tsickle
 * Generated from: lib/lblibs/DicPipe.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Pipe } from '@angular/core';
import { SupDic } from './SupDic';
/*
* 字典显示管道
*   10|dicpipe:aae140
*/
export class DicPipe {
    /**
     * @param {?} supdic
     */
    constructor(supdic) {
        this.supdic = supdic;
    }
    /**
     * @param {?} value
     * @param {?} dicname
     * @return {?}
     */
    transform(value, dicname) {
        return value ? dicname ? this.supdic.getdicLabel(dicname.toUpperCase(), value) : value : value;
    }
}
DicPipe.decorators = [
    { type: Pipe, args: [{ name: 'dicpipe' },] }
];
/** @nocollapse */
DicPipe.ctorParameters = () => [
    { type: SupDic }
];
if (false) {
    /**
     * @type {?}
     * @private
     */
    DicPipe.prototype.supdic;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiRGljUGlwZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2xiZi8iLCJzb3VyY2VzIjpbImxpYi9sYmxpYnMvRGljUGlwZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7OztBQUFBLE9BQU8sRUFBRSxJQUFJLEVBQWlCLE1BQU0sZUFBZSxDQUFDO0FBQ3BELE9BQU8sRUFBRSxNQUFNLEVBQUUsTUFBTSxVQUFVLENBQUM7Ozs7O0FBT2xDLE1BQU0sT0FBTyxPQUFPOzs7O0lBRWxCLFlBQW9CLE1BQWM7UUFBZCxXQUFNLEdBQU4sTUFBTSxDQUFRO0lBRWxDLENBQUM7Ozs7OztJQUVELFNBQVMsQ0FBQyxLQUFhLEVBQUUsT0FBZTtRQUV0QyxPQUFPLEtBQUssQ0FBQyxDQUFDLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLFdBQVcsQ0FBQyxPQUFPLENBQUMsV0FBVyxFQUFFLEVBQUUsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDO0lBRWpHLENBQUM7OztZQVhGLElBQUksU0FBQyxFQUFFLElBQUksRUFBRSxTQUFTLEVBQUU7Ozs7WUFOaEIsTUFBTTs7Ozs7OztJQVNELHlCQUFzQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IFBpcGUsIFBpcGVUcmFuc2Zvcm0gfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgU3VwRGljIH0gZnJvbSAnLi9TdXBEaWMnO1xyXG5cclxuLypcclxuKiDlrZflhbjmmL7npLrnrqHpgZNcclxuKiAgIDEwfGRpY3BpcGU6YWFlMTQwXHJcbiovXHJcbkBQaXBlKHsgbmFtZTogJ2RpY3BpcGUnIH0pXHJcbmV4cG9ydCBjbGFzcyBEaWNQaXBlIGltcGxlbWVudHMgUGlwZVRyYW5zZm9ybSB7XHJcblxyXG4gIGNvbnN0cnVjdG9yKHByaXZhdGUgc3VwZGljOiBTdXBEaWMpIHtcclxuXHJcbiAgfVxyXG5cclxuICB0cmFuc2Zvcm0odmFsdWU6IHN0cmluZywgZGljbmFtZTogc3RyaW5nKSB7XHJcblxyXG4gICAgcmV0dXJuIHZhbHVlID8gZGljbmFtZSA/IHRoaXMuc3VwZGljLmdldGRpY0xhYmVsKGRpY25hbWUudG9VcHBlckNhc2UoKSwgdmFsdWUpIDogdmFsdWUgOiB2YWx1ZTtcclxuXHJcbiAgfVxyXG5cclxufSJdfQ==