/**
 * @fileoverview added by tsickle
 * Generated from: lib/ServiceConfig.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Injectable } from '@angular/core';
import { Md5 } from "ts-md5/dist/md5";
import * as i0 from "@angular/core";
export class ServiceInfo {
    constructor() {
        this.APPID = "TEST_APPID";
        this.APPKEY = "TEST_APPKEY";
        this.PWDFLAG = "H5CS3";
        this.LOGTOKEN = "";
        this.VERSION = "20190730";
        this.APPINFO = {
            "name": "测试",
            "description": ""
        };
        this.APPSUFFIX = this.APPINFO.name;
        this.user = { aac003: "", name: "", roleid: "", dept: "", avatar: "./assets/tmp/img/avatar.jpg" };
        this.baseUrl = "";
        this.molssUrl = "mohrss/chbx";
        this.uploadurl = "mohrss/chbx/upload";
        this._adminRole = [];
    }
    /**
     * @param {?} adminRole
     * @return {?}
     */
    setAdminRole(adminRole) {
        this._adminRole = adminRole;
    }
    /**
     * @return {?}
     */
    get isAdmin() {
        if (!this._adminRole || !this.user || !this.user.roleid)
            return false;
        return this._adminRole.includes(this.user.roleid);
    }
    /**
     * @param {?} sname
     * @param {?} timestamp
     * @param {?} para
     * @return {?}
     */
    getSign(sname, timestamp, para) {
        /** @type {?} */
        const hashstr = this.APPKEY + para + timestamp + sname + this.LOGTOKEN;
        return Md5.hashStr(hashstr).toString();
    }
    /**
     * @param {?} info
     * @return {?}
     */
    setSinfo(info) {
        this.APPID = info.APPID;
        this.APPKEY = info.APPKEY;
        this.VERSION = info.VERSION;
        this.APPINFO.name = info.APPNAME;
        this.APPSUFFIX = this.APPINFO.name;
    }
    /**
     * @param {?} user
     * @return {?}
     */
    setUser(user) {
        this.user = Object.assign({}, this.user, user);
        // this.user.name = user.name;
        // this.user.aac003 = user.aac003;
        this.user.roleid = user.usertype;
        // this.user.dept = user.dept;
        sessionStorage.setItem("userinfo", JSON.stringify(this.user));
    }
    /**
     * @return {?}
     */
    loaduser() {
        /** @type {?} */
        const userinfo = sessionStorage.getItem("userinfo");
        try {
            /** @type {?} */
            const u = JSON.parse(userinfo);
            this.user = Object.assign({}, this.user, u);
            // this.user.name = u.name;
            // this.user.aac003 = u.aac003;
            // this.user.roleid = u.roleid;
            // this.user.dept = u.dept;
        }
        catch (ex) {
        }
    }
    /**
     * @return {?}
     */
    getTimestamp() {
        /** @type {?} */
        const d = new Date();
        return Date.UTC(d.getFullYear(), d.getMonth(), d.getDate(), d.getHours(), d.getMinutes(), d.getSeconds(), d.getMilliseconds());
    }
    /**
     * @param {?} token
     * @return {?}
     */
    saveToken(token) {
        sessionStorage.setItem("logintoken", token);
    }
    /**
     * @return {?}
     */
    getToken() {
        this.LOGTOKEN = sessionStorage.getItem("logintoken");
        if (this.LOGTOKEN == null)
            this.LOGTOKEN = "";
    }
    /**
     * @return {?}
     */
    clearToken() {
        sessionStorage.setItem("logintoken", "");
    }
    /**
     * @param {?} md5Str
     * @return {?}
     */
    md5(md5Str) {
        return Md5.hashStr(md5Str).toString();
    }
    /**
     * @param {?} pwd
     * @return {?}
     */
    pwdmd5(pwd) {
        return this.md5(pwd + this.PWDFLAG);
    }
}
ServiceInfo.decorators = [
    { type: Injectable, args: [{
                providedIn: 'root'
            },] }
];
/** @nocollapse */
ServiceInfo.ctorParameters = () => [];
/** @nocollapse */ ServiceInfo.ngInjectableDef = i0.ɵɵdefineInjectable({ factory: function ServiceInfo_Factory() { return new ServiceInfo(); }, token: ServiceInfo, providedIn: "root" });
if (false) {
    /** @type {?} */
    ServiceInfo.prototype.APPID;
    /**
     * @type {?}
     * @private
     */
    ServiceInfo.prototype.APPKEY;
    /**
     * @type {?}
     * @private
     */
    ServiceInfo.prototype.PWDFLAG;
    /** @type {?} */
    ServiceInfo.prototype.LOGTOKEN;
    /** @type {?} */
    ServiceInfo.prototype.VERSION;
    /** @type {?} */
    ServiceInfo.prototype.APPINFO;
    /** @type {?} */
    ServiceInfo.prototype.APPSUFFIX;
    /** @type {?} */
    ServiceInfo.prototype.user;
    /** @type {?} */
    ServiceInfo.prototype.baseUrl;
    /** @type {?} */
    ServiceInfo.prototype.molssUrl;
    /** @type {?} */
    ServiceInfo.prototype.uploadurl;
    /** @type {?} */
    ServiceInfo.prototype._adminRole;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiU2VydmljZUNvbmZpZy5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2xiZi8iLCJzb3VyY2VzIjpbImxpYi9TZXJ2aWNlQ29uZmlnLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7O0FBQUEsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUMzQyxPQUFPLEVBQUUsR0FBRyxFQUFFLE1BQU0saUJBQWlCLENBQUM7O0FBS3RDLE1BQU0sT0FBTyxXQUFXO0lBRXRCO1FBRU8sVUFBSyxHQUFHLFlBQVksQ0FBQztRQUNwQixXQUFNLEdBQUcsYUFBYSxDQUFDO1FBQ3ZCLFlBQU8sR0FBRyxPQUFPLENBQUM7UUFDbkIsYUFBUSxHQUFHLEVBQUUsQ0FBQztRQUNkLFlBQU8sR0FBRyxVQUFVLENBQUE7UUFDcEIsWUFBTyxHQUFHO1lBQ2YsTUFBTSxFQUFFLElBQUk7WUFDWixhQUFhLEVBQUUsRUFBRTtTQUNsQixDQUFDO1FBQ0ssY0FBUyxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDO1FBRTlCLFNBQUksR0FBUSxFQUFFLE1BQU0sRUFBRSxFQUFFLEVBQUUsSUFBSSxFQUFFLEVBQUUsRUFBRSxNQUFNLEVBQUUsRUFBRSxFQUFFLElBQUksRUFBRSxFQUFFLEVBQUUsTUFBTSxFQUFFLDZCQUE2QixFQUFFLENBQUE7UUFFakcsWUFBTyxHQUFHLEVBQUUsQ0FBQztRQUNiLGFBQVEsR0FBRyxhQUFhLENBQUM7UUFDekIsY0FBUyxHQUFHLG9CQUFvQixDQUFDO1FBRXhDLGVBQVUsR0FBYSxFQUFFLENBQUM7SUFuQlYsQ0FBQzs7Ozs7SUFxQmpCLFlBQVksQ0FBQyxTQUFtQjtRQUM5QixJQUFJLENBQUMsVUFBVSxHQUFHLFNBQVMsQ0FBQztJQUM5QixDQUFDOzs7O0lBRUQsSUFBSSxPQUFPO1FBRVQsSUFBSSxDQUFDLElBQUksQ0FBQyxVQUFVLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNO1lBQUUsT0FBTyxLQUFLLENBQUM7UUFFdEUsT0FBTyxJQUFJLENBQUMsVUFBVSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDO0lBQ3BELENBQUM7Ozs7Ozs7SUFFRCxPQUFPLENBQUMsS0FBYSxFQUFFLFNBQWlCLEVBQUUsSUFBWTs7Y0FFOUMsT0FBTyxHQUFHLElBQUksQ0FBQyxNQUFNLEdBQUcsSUFBSSxHQUFHLFNBQVMsR0FBRyxLQUFLLEdBQUcsSUFBSSxDQUFDLFFBQVE7UUFDdEUsT0FBTyxHQUFHLENBQUMsT0FBTyxDQUFDLE9BQU8sQ0FBQyxDQUFDLFFBQVEsRUFBRSxDQUFDO0lBQ3pDLENBQUM7Ozs7O0lBRUQsUUFBUSxDQUFDLElBQUk7UUFDWCxJQUFJLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUM7UUFDeEIsSUFBSSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDO1FBQzFCLElBQUksQ0FBQyxPQUFPLEdBQUcsSUFBSSxDQUFDLE9BQU8sQ0FBQztRQUM1QixJQUFJLENBQUMsT0FBTyxDQUFDLElBQUksR0FBRyxJQUFJLENBQUMsT0FBTyxDQUFDO1FBQ2pDLElBQUksQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUM7SUFDckMsQ0FBQzs7Ozs7SUFFRCxPQUFPLENBQUMsSUFBUztRQUVmLElBQUksQ0FBQyxJQUFJLHFCQUFRLElBQUksQ0FBQyxJQUFJLEVBQUssSUFBSSxDQUFFLENBQUE7UUFDckMsOEJBQThCO1FBQzlCLGtDQUFrQztRQUNsQyxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDO1FBQ2pDLDhCQUE4QjtRQUM5QixjQUFjLENBQUMsT0FBTyxDQUFDLFVBQVUsRUFBRSxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDO0lBRWhFLENBQUM7Ozs7SUFJRCxRQUFROztjQUVBLFFBQVEsR0FBRyxjQUFjLENBQUMsT0FBTyxDQUFDLFVBQVUsQ0FBQztRQUNuRCxJQUFJOztrQkFDSSxDQUFDLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUM7WUFFOUIsSUFBSSxDQUFDLElBQUkscUJBQVEsSUFBSSxDQUFDLElBQUksRUFBSyxDQUFDLENBQUUsQ0FBQTtZQUNsQywyQkFBMkI7WUFDM0IsK0JBQStCO1lBQy9CLCtCQUErQjtZQUMvQiwyQkFBMkI7U0FFNUI7UUFBQyxPQUFPLEVBQUUsRUFBRTtTQUVaO0lBRUgsQ0FBQzs7OztJQUVELFlBQVk7O2NBQ0osQ0FBQyxHQUFHLElBQUksSUFBSSxFQUFFO1FBQ3BCLE9BQU8sSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsV0FBVyxFQUFFLEVBQzNCLENBQUMsQ0FBQyxRQUFRLEVBQUUsRUFDWixDQUFDLENBQUMsT0FBTyxFQUFFLEVBQ1gsQ0FBQyxDQUFDLFFBQVEsRUFBRSxFQUNaLENBQUMsQ0FBQyxVQUFVLEVBQUUsRUFDZCxDQUFDLENBQUMsVUFBVSxFQUFFLEVBQ2QsQ0FBQyxDQUFDLGVBQWUsRUFBRSxDQUFDLENBQUM7SUFDM0IsQ0FBQzs7Ozs7SUFFRCxTQUFTLENBQUMsS0FBYTtRQUVyQixjQUFjLENBQUMsT0FBTyxDQUFDLFlBQVksRUFBRSxLQUFLLENBQUMsQ0FBQztJQUU5QyxDQUFDOzs7O0lBRUQsUUFBUTtRQUVOLElBQUksQ0FBQyxRQUFRLEdBQUcsY0FBYyxDQUFDLE9BQU8sQ0FBQyxZQUFZLENBQUMsQ0FBQztRQUNyRCxJQUFJLElBQUksQ0FBQyxRQUFRLElBQUksSUFBSTtZQUFFLElBQUksQ0FBQyxRQUFRLEdBQUcsRUFBRSxDQUFDO0lBRWhELENBQUM7Ozs7SUFFRCxVQUFVO1FBQ1IsY0FBYyxDQUFDLE9BQU8sQ0FBQyxZQUFZLEVBQUUsRUFBRSxDQUFDLENBQUM7SUFFM0MsQ0FBQzs7Ozs7SUFFRCxHQUFHLENBQUMsTUFBYztRQUVoQixPQUFPLEdBQUcsQ0FBQyxPQUFPLENBQUMsTUFBTSxDQUFDLENBQUMsUUFBUSxFQUFFLENBQUM7SUFFeEMsQ0FBQzs7Ozs7SUFFRCxNQUFNLENBQUMsR0FBVztRQUNoQixPQUFPLElBQUksQ0FBQyxHQUFHLENBQUMsR0FBRyxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQztJQUN0QyxDQUFDOzs7WUF2SEYsVUFBVSxTQUFDO2dCQUNWLFVBQVUsRUFBRSxNQUFNO2FBQ25COzs7Ozs7O0lBS0MsNEJBQTRCOzs7OztJQUM1Qiw2QkFBK0I7Ozs7O0lBQy9CLDhCQUEwQjs7SUFDMUIsK0JBQXFCOztJQUNyQiw4QkFBMkI7O0lBQzNCLDhCQUdFOztJQUNGLGdDQUFxQzs7SUFFckMsMkJBQXdHOztJQUV4Ryw4QkFBb0I7O0lBQ3BCLCtCQUFnQzs7SUFDaEMsZ0NBQXdDOztJQUV4QyxpQ0FBMEIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBJbmplY3RhYmxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IE1kNSB9IGZyb20gXCJ0cy1tZDUvZGlzdC9tZDVcIjtcclxuXHJcbkBJbmplY3RhYmxlKHtcclxuICBwcm92aWRlZEluOiAncm9vdCdcclxufSlcclxuZXhwb3J0IGNsYXNzIFNlcnZpY2VJbmZvIHtcclxuXHJcbiAgY29uc3RydWN0b3IoKSB7IH1cclxuXHJcbiAgcHVibGljIEFQUElEID0gXCJURVNUX0FQUElEXCI7XHJcbiAgcHJpdmF0ZSBBUFBLRVkgPSBcIlRFU1RfQVBQS0VZXCI7XHJcbiAgcHJpdmF0ZSBQV0RGTEFHID0gXCJINUNTM1wiO1xyXG4gIHB1YmxpYyBMT0dUT0tFTiA9IFwiXCI7XHJcbiAgcHVibGljIFZFUlNJT04gPSBcIjIwMTkwNzMwXCJcclxuICBwdWJsaWMgQVBQSU5GTyA9IHtcclxuICAgIFwibmFtZVwiOiBcIua1i+ivlVwiLFxyXG4gICAgXCJkZXNjcmlwdGlvblwiOiBcIlwiXHJcbiAgfTtcclxuICBwdWJsaWMgQVBQU1VGRklYID0gdGhpcy5BUFBJTkZPLm5hbWU7XHJcblxyXG4gIHB1YmxpYyB1c2VyOiBhbnkgPSB7IGFhYzAwMzogXCJcIiwgbmFtZTogXCJcIiwgcm9sZWlkOiBcIlwiLCBkZXB0OiBcIlwiLCBhdmF0YXI6IFwiLi9hc3NldHMvdG1wL2ltZy9hdmF0YXIuanBnXCIgfVxyXG5cclxuICBwdWJsaWMgYmFzZVVybCA9IFwiXCI7XHJcbiAgcHVibGljIG1vbHNzVXJsID0gXCJtb2hyc3MvY2hieFwiO1xyXG4gIHB1YmxpYyB1cGxvYWR1cmwgPSBcIm1vaHJzcy9jaGJ4L3VwbG9hZFwiO1xyXG5cclxuICBfYWRtaW5Sb2xlOiBzdHJpbmdbXSA9IFtdO1xyXG5cclxuICBzZXRBZG1pblJvbGUoYWRtaW5Sb2xlOiBzdHJpbmdbXSkge1xyXG4gICAgdGhpcy5fYWRtaW5Sb2xlID0gYWRtaW5Sb2xlO1xyXG4gIH1cclxuXHJcbiAgZ2V0IGlzQWRtaW4oKSB7XHJcblxyXG4gICAgaWYgKCF0aGlzLl9hZG1pblJvbGUgfHwgIXRoaXMudXNlciB8fCAhdGhpcy51c2VyLnJvbGVpZCkgcmV0dXJuIGZhbHNlO1xyXG5cclxuICAgIHJldHVybiB0aGlzLl9hZG1pblJvbGUuaW5jbHVkZXModGhpcy51c2VyLnJvbGVpZCk7XHJcbiAgfVxyXG5cclxuICBnZXRTaWduKHNuYW1lOiBzdHJpbmcsIHRpbWVzdGFtcDogc3RyaW5nLCBwYXJhOiBzdHJpbmcpOiBzdHJpbmcge1xyXG5cclxuICAgIGNvbnN0IGhhc2hzdHIgPSB0aGlzLkFQUEtFWSArIHBhcmEgKyB0aW1lc3RhbXAgKyBzbmFtZSArIHRoaXMuTE9HVE9LRU5cclxuICAgIHJldHVybiBNZDUuaGFzaFN0cihoYXNoc3RyKS50b1N0cmluZygpO1xyXG4gIH1cclxuXHJcbiAgc2V0U2luZm8oaW5mbykge1xyXG4gICAgdGhpcy5BUFBJRCA9IGluZm8uQVBQSUQ7XHJcbiAgICB0aGlzLkFQUEtFWSA9IGluZm8uQVBQS0VZO1xyXG4gICAgdGhpcy5WRVJTSU9OID0gaW5mby5WRVJTSU9OO1xyXG4gICAgdGhpcy5BUFBJTkZPLm5hbWUgPSBpbmZvLkFQUE5BTUU7XHJcbiAgICB0aGlzLkFQUFNVRkZJWCA9IHRoaXMuQVBQSU5GTy5uYW1lO1xyXG4gIH1cclxuXHJcbiAgc2V0VXNlcih1c2VyOiBhbnkpIHtcclxuXHJcbiAgICB0aGlzLnVzZXIgPSB7IC4uLnRoaXMudXNlciwgLi4udXNlciB9XHJcbiAgICAvLyB0aGlzLnVzZXIubmFtZSA9IHVzZXIubmFtZTtcclxuICAgIC8vIHRoaXMudXNlci5hYWMwMDMgPSB1c2VyLmFhYzAwMztcclxuICAgIHRoaXMudXNlci5yb2xlaWQgPSB1c2VyLnVzZXJ0eXBlO1xyXG4gICAgLy8gdGhpcy51c2VyLmRlcHQgPSB1c2VyLmRlcHQ7XHJcbiAgICBzZXNzaW9uU3RvcmFnZS5zZXRJdGVtKFwidXNlcmluZm9cIiwgSlNPTi5zdHJpbmdpZnkodGhpcy51c2VyKSk7XHJcblxyXG4gIH1cclxuXHJcblxyXG5cclxuICBsb2FkdXNlcigpIHtcclxuXHJcbiAgICBjb25zdCB1c2VyaW5mbyA9IHNlc3Npb25TdG9yYWdlLmdldEl0ZW0oXCJ1c2VyaW5mb1wiKTtcclxuICAgIHRyeSB7XHJcbiAgICAgIGNvbnN0IHUgPSBKU09OLnBhcnNlKHVzZXJpbmZvKTtcclxuXHJcbiAgICAgIHRoaXMudXNlciA9IHsgLi4udGhpcy51c2VyLCAuLi51IH1cclxuICAgICAgLy8gdGhpcy51c2VyLm5hbWUgPSB1Lm5hbWU7XHJcbiAgICAgIC8vIHRoaXMudXNlci5hYWMwMDMgPSB1LmFhYzAwMztcclxuICAgICAgLy8gdGhpcy51c2VyLnJvbGVpZCA9IHUucm9sZWlkO1xyXG4gICAgICAvLyB0aGlzLnVzZXIuZGVwdCA9IHUuZGVwdDtcclxuXHJcbiAgICB9IGNhdGNoIChleCkge1xyXG5cclxuICAgIH1cclxuXHJcbiAgfVxyXG5cclxuICBnZXRUaW1lc3RhbXAoKTogbnVtYmVyIHtcclxuICAgIGNvbnN0IGQgPSBuZXcgRGF0ZSgpO1xyXG4gICAgcmV0dXJuIERhdGUuVVRDKGQuZ2V0RnVsbFllYXIoKVxyXG4gICAgICAsIGQuZ2V0TW9udGgoKVxyXG4gICAgICAsIGQuZ2V0RGF0ZSgpXHJcbiAgICAgICwgZC5nZXRIb3VycygpXHJcbiAgICAgICwgZC5nZXRNaW51dGVzKClcclxuICAgICAgLCBkLmdldFNlY29uZHMoKVxyXG4gICAgICAsIGQuZ2V0TWlsbGlzZWNvbmRzKCkpO1xyXG4gIH1cclxuXHJcbiAgc2F2ZVRva2VuKHRva2VuOiBzdHJpbmcpIHtcclxuXHJcbiAgICBzZXNzaW9uU3RvcmFnZS5zZXRJdGVtKFwibG9naW50b2tlblwiLCB0b2tlbik7XHJcblxyXG4gIH1cclxuXHJcbiAgZ2V0VG9rZW4oKTogdm9pZCB7XHJcblxyXG4gICAgdGhpcy5MT0dUT0tFTiA9IHNlc3Npb25TdG9yYWdlLmdldEl0ZW0oXCJsb2dpbnRva2VuXCIpO1xyXG4gICAgaWYgKHRoaXMuTE9HVE9LRU4gPT0gbnVsbCkgdGhpcy5MT0dUT0tFTiA9IFwiXCI7XHJcblxyXG4gIH1cclxuXHJcbiAgY2xlYXJUb2tlbigpOiB2b2lkIHtcclxuICAgIHNlc3Npb25TdG9yYWdlLnNldEl0ZW0oXCJsb2dpbnRva2VuXCIsIFwiXCIpO1xyXG5cclxuICB9XHJcblxyXG4gIG1kNShtZDVTdHI6IHN0cmluZyk6IHN0cmluZyB7XHJcblxyXG4gICAgcmV0dXJuIE1kNS5oYXNoU3RyKG1kNVN0cikudG9TdHJpbmcoKTtcclxuXHJcbiAgfVxyXG5cclxuICBwd2RtZDUocHdkOiBzdHJpbmcpOiBzdHJpbmcge1xyXG4gICAgcmV0dXJuIHRoaXMubWQ1KHB3ZCArIHRoaXMuUFdERkxBRyk7XHJcbiAgfVxyXG59XHJcblxyXG5cclxuIl19