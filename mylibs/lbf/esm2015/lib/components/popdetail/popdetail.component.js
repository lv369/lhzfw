/**
 * @fileoverview added by tsickle
 * Generated from: lib/components/popdetail/popdetail.component.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component, Input, Output, EventEmitter, ViewChild } from '@angular/core';
import { SFComponent } from '@delon/form';
import { NzMessageService } from 'ng-zorro-antd';
import { HttpService } from '../../lblibs/Lbjk.service';
export class PopdetailComponent {
    /**
     * @param {?} msgSrv
     * @param {?} lbservice
     */
    constructor(msgSrv, lbservice) {
        this.msgSrv = msgSrv;
        this.lbservice = lbservice;
        // 是否弹出
        this.isVisible = false;
        this.isVisibleChange = new EventEmitter();
        // 弹出框标题
        this.title = "明细框";
        // 字段信息
        this.schema = {};
        // 默认值
        this.formData = {};
        // 是否加载中
        this.loading = false;
        this.loadingChange = new EventEmitter();
        // 保存服务
        this.saveSname = '';
        // formchang事件
        this.formChange = new EventEmitter();
    }
    /**
     * @return {?}
     */
    ngOnInit() {
    }
    /**
     * @return {?}
     */
    handleCancel() {
        this.isVisible = false;
        this.isVisibleChange.emit(this.isVisible);
    }
    /**
     * @return {?}
     */
    show() {
        this.isVisible = true;
    }
    /**
     * @return {?}
     */
    hidde() {
        this.isVisible = true;
    }
    /**
     * @return {?}
     */
    reset() {
        this.sf.reset();
    }
    /**
     * @return {?}
     */
    value() {
        return this.sf.value;
    }
    // path 采用 / 来分隔，例如：/user/name
    /**
     * @param {?} path
     * @return {?}
     */
    getValue(path) {
        return this.sf.getValue(path);
    }
    /**
     * @param {?} path
     * @param {?} value
     * @return {?}
     */
    setValue(path, value) {
        this.sf.setValue(path, value);
    }
    /**
     * @return {?}
     */
    validator() {
        this.sf.validator();
    }
    /**
     * @param {?=} newSchema
     * @param {?=} newUI
     * @return {?}
     */
    refreshSchema(newSchema, newUI) {
        this.sf.refreshSchema(newSchema, newUI);
    }
    /**
     * @return {?}
     */
    getsf() {
        return this.sf;
    }
    /**
     * @param {?} val
     * @return {?}
     */
    save(val) {
        this.loading = true;
        this.loadingChange.emit(this.loading);
        if (this.saveSname && this.saveSname !== '') {
            this.lbservice.lbservice(this.saveSname, { para: val }, 500).then((/**
             * @param {?} resdata
             * @return {?}
             */
            (resdata) => {
                this.loading = false;
                this.loadingChange.emit(this.loading);
                if (resdata.code < 1) {
                    this.msgSrv.error(resdata.errmsg);
                    return;
                }
                if (this.saveFu && typeof this.saveFu === 'function') {
                    this.saveFu(val, resdata);
                }
                this.isVisible = false;
                this.isVisibleChange.emit(this.isVisible);
                this.reset();
                this.msgSrv.info("成功！");
            }));
        }
        else {
            if (this.saveFu && typeof this.saveFu === 'function') {
                this.saveFu(val, {});
            }
        }
    }
    /**
     * @param {?} value
     * @return {?}
     */
    change(value) {
        this.formChange.emit(value);
    }
}
PopdetailComponent.decorators = [
    { type: Component, args: [{
                selector: 'lb-pop',
                template: "<nz-modal [(nzVisible)]=\"isVisible\" [nzTitle]=\"title\" nzWidth=\"80%\" (nzOnCancel)=\"handleCancel()\"\n    (nzOnOk)=\"save(sf.value)\" [nzOkLoading]=\"loading\">\n\n    <sf #sf [schema]=\"schema\" [formData]=\"formData\" (formChange)=\"change($event)\" button=\"none\">\n\n    </sf>\n\n</nz-modal>",
                styles: [""]
            }] }
];
/** @nocollapse */
PopdetailComponent.ctorParameters = () => [
    { type: NzMessageService },
    { type: HttpService }
];
PopdetailComponent.propDecorators = {
    sf: [{ type: ViewChild, args: ['sf', { static: true },] }],
    isVisible: [{ type: Input }],
    isVisibleChange: [{ type: Output }],
    title: [{ type: Input }],
    schema: [{ type: Input }],
    formData: [{ type: Input }],
    loading: [{ type: Input }],
    loadingChange: [{ type: Output }],
    saveSname: [{ type: Input }],
    saveFu: [{ type: Input }],
    formChange: [{ type: Output }]
};
if (false) {
    /** @type {?} */
    PopdetailComponent.prototype.sf;
    /** @type {?} */
    PopdetailComponent.prototype.isVisible;
    /** @type {?} */
    PopdetailComponent.prototype.isVisibleChange;
    /** @type {?} */
    PopdetailComponent.prototype.title;
    /** @type {?} */
    PopdetailComponent.prototype.schema;
    /** @type {?} */
    PopdetailComponent.prototype.formData;
    /** @type {?} */
    PopdetailComponent.prototype.loading;
    /** @type {?} */
    PopdetailComponent.prototype.loadingChange;
    /** @type {?} */
    PopdetailComponent.prototype.saveSname;
    /** @type {?} */
    PopdetailComponent.prototype.saveFu;
    /** @type {?} */
    PopdetailComponent.prototype.formChange;
    /** @type {?} */
    PopdetailComponent.prototype.msgSrv;
    /**
     * @type {?}
     * @private
     */
    PopdetailComponent.prototype.lbservice;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicG9wZGV0YWlsLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2xiZi8iLCJzb3VyY2VzIjpbImxpYi9jb21wb25lbnRzL3BvcGRldGFpbC9wb3BkZXRhaWwuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7O0FBQUEsT0FBTyxFQUFFLFNBQVMsRUFBRSxLQUFLLEVBQUUsTUFBTSxFQUFFLFlBQVksRUFBRSxTQUFTLEVBQVUsTUFBTSxlQUFlLENBQUM7QUFDMUYsT0FBTyxFQUFZLFdBQVcsRUFBYyxNQUFNLGFBQWEsQ0FBQztBQUNoRSxPQUFPLEVBQUUsZ0JBQWdCLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDakQsT0FBTyxFQUFFLFdBQVcsRUFBRSxNQUFNLDJCQUEyQixDQUFDO0FBUXhELE1BQU0sT0FBTyxrQkFBa0I7Ozs7O0lBRTdCLFlBQ1MsTUFBd0IsRUFDdkIsU0FBc0I7UUFEdkIsV0FBTSxHQUFOLE1BQU0sQ0FBa0I7UUFDdkIsY0FBUyxHQUFULFNBQVMsQ0FBYTs7UUFPdkIsY0FBUyxHQUFHLEtBQUssQ0FBQztRQUNqQixvQkFBZSxHQUFHLElBQUksWUFBWSxFQUFFLENBQUM7O1FBR3RDLFVBQUssR0FBRyxLQUFLLENBQUM7O1FBR2QsV0FBTSxHQUFhLEVBQUUsQ0FBQzs7UUFHdEIsYUFBUSxHQUFRLEVBQUUsQ0FBQzs7UUFHbkIsWUFBTyxHQUFHLEtBQUssQ0FBQztRQUNmLGtCQUFhLEdBQUcsSUFBSSxZQUFZLEVBQUUsQ0FBQzs7UUFHcEMsY0FBUyxHQUFHLEVBQUUsQ0FBQzs7UUFJTCxlQUFVLEdBQUcsSUFBSSxZQUFZLEVBQUUsQ0FBQztJQTNCL0MsQ0FBQzs7OztJQTZCTCxRQUFRO0lBQ1IsQ0FBQzs7OztJQUdELFlBQVk7UUFDVixJQUFJLENBQUMsU0FBUyxHQUFHLEtBQUssQ0FBQztRQUN2QixJQUFJLENBQUMsZUFBZSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUM7SUFDNUMsQ0FBQzs7OztJQUdELElBQUk7UUFDRixJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQztJQUN4QixDQUFDOzs7O0lBRUQsS0FBSztRQUNILElBQUksQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDO0lBQ3hCLENBQUM7Ozs7SUFFRCxLQUFLO1FBQ0gsSUFBSSxDQUFDLEVBQUUsQ0FBQyxLQUFLLEVBQUUsQ0FBQztJQUNsQixDQUFDOzs7O0lBRUQsS0FBSztRQUNILE9BQU8sSUFBSSxDQUFDLEVBQUUsQ0FBQyxLQUFLLENBQUM7SUFDdkIsQ0FBQzs7Ozs7O0lBR0QsUUFBUSxDQUFDLElBQVk7UUFDbkIsT0FBTyxJQUFJLENBQUMsRUFBRSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsQ0FBQztJQUNoQyxDQUFDOzs7Ozs7SUFFRCxRQUFRLENBQUMsSUFBWSxFQUFFLEtBQVU7UUFDL0IsSUFBSSxDQUFDLEVBQUUsQ0FBQyxRQUFRLENBQUMsSUFBSSxFQUFFLEtBQUssQ0FBQyxDQUFDO0lBQ2hDLENBQUM7Ozs7SUFFRCxTQUFTO1FBQ1AsSUFBSSxDQUFDLEVBQUUsQ0FBQyxTQUFTLEVBQUUsQ0FBQztJQUN0QixDQUFDOzs7Ozs7SUFFRCxhQUFhLENBQUMsU0FBb0IsRUFBRSxLQUFrQjtRQUNwRCxJQUFJLENBQUMsRUFBRSxDQUFDLGFBQWEsQ0FBQyxTQUFTLEVBQUUsS0FBSyxDQUFDLENBQUM7SUFDMUMsQ0FBQzs7OztJQUVELEtBQUs7UUFDSCxPQUFPLElBQUksQ0FBQyxFQUFFLENBQUM7SUFDakIsQ0FBQzs7Ozs7SUFFRCxJQUFJLENBQUMsR0FBUTtRQUVYLElBQUksQ0FBQyxPQUFPLEdBQUcsSUFBSSxDQUFDO1FBQ3BCLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQztRQUV0QyxJQUFJLElBQUksQ0FBQyxTQUFTLElBQUksSUFBSSxDQUFDLFNBQVMsS0FBSyxFQUFFLEVBQUU7WUFDM0MsSUFBSSxDQUFDLFNBQVMsQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLFNBQVMsRUFBRSxFQUFFLElBQUksRUFBRSxHQUFHLEVBQUUsRUFBRSxHQUFHLENBQUMsQ0FBQyxJQUFJOzs7O1lBQUMsQ0FBQyxPQUFPLEVBQUUsRUFBRTtnQkFDNUUsSUFBSSxDQUFDLE9BQU8sR0FBRyxLQUFLLENBQUM7Z0JBQ3JCLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQztnQkFDdEMsSUFBSSxPQUFPLENBQUMsSUFBSSxHQUFHLENBQUMsRUFBRTtvQkFDcEIsSUFBSSxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLE1BQU0sQ0FBQyxDQUFDO29CQUNsQyxPQUFPO2lCQUNSO2dCQUVELElBQUksSUFBSSxDQUFDLE1BQU0sSUFBSSxPQUFPLElBQUksQ0FBQyxNQUFNLEtBQUssVUFBVSxFQUFFO29CQUNwRCxJQUFJLENBQUMsTUFBTSxDQUFDLEdBQUcsRUFBRSxPQUFPLENBQUMsQ0FBQztpQkFDM0I7Z0JBRUQsSUFBSSxDQUFDLFNBQVMsR0FBRyxLQUFLLENBQUM7Z0JBQ3ZCLElBQUksQ0FBQyxlQUFlLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQztnQkFDMUMsSUFBSSxDQUFDLEtBQUssRUFBRSxDQUFDO2dCQUNiLElBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO1lBRTFCLENBQUMsRUFBQyxDQUFDO1NBQ0o7YUFBTTtZQUVMLElBQUksSUFBSSxDQUFDLE1BQU0sSUFBSSxPQUFPLElBQUksQ0FBQyxNQUFNLEtBQUssVUFBVSxFQUFFO2dCQUNwRCxJQUFJLENBQUMsTUFBTSxDQUFDLEdBQUcsRUFBRSxFQUFFLENBQUMsQ0FBQzthQUN0QjtTQUVGO0lBRUgsQ0FBQzs7Ozs7SUFFRCxNQUFNLENBQUMsS0FBSztRQUNWLElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO0lBQzlCLENBQUM7OztZQTFIRixTQUFTLFNBQUM7Z0JBQ1QsUUFBUSxFQUFFLFFBQVE7Z0JBQ2xCLHlUQUF5Qzs7YUFFMUM7Ozs7WUFSUSxnQkFBZ0I7WUFDaEIsV0FBVzs7O2lCQWdCakIsU0FBUyxTQUFDLElBQUksRUFBRSxFQUFFLE1BQU0sRUFBRSxJQUFJLEVBQUU7d0JBR2hDLEtBQUs7OEJBQ0wsTUFBTTtvQkFHTixLQUFLO3FCQUdMLEtBQUs7dUJBR0wsS0FBSztzQkFHTCxLQUFLOzRCQUNMLE1BQU07d0JBR04sS0FBSztxQkFDTCxLQUFLO3lCQUdMLE1BQU07Ozs7SUF4QlAsZ0NBQW1EOztJQUduRCx1Q0FBMkI7O0lBQzNCLDZDQUErQzs7SUFHL0MsbUNBQXVCOztJQUd2QixvQ0FBK0I7O0lBRy9CLHNDQUE0Qjs7SUFHNUIscUNBQXlCOztJQUN6QiwyQ0FBNkM7O0lBRzdDLHVDQUF3Qjs7SUFDeEIsb0NBQXFCOztJQUdyQix3Q0FBbUQ7O0lBN0JqRCxvQ0FBK0I7Ozs7O0lBQy9CLHVDQUE4QiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgSW5wdXQsIE91dHB1dCwgRXZlbnRFbWl0dGVyLCBWaWV3Q2hpbGQsIE9uSW5pdCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgU0ZTY2hlbWEsIFNGQ29tcG9uZW50LCBTRlVJU2NoZW1hIH0gZnJvbSAnQGRlbG9uL2Zvcm0nO1xuaW1wb3J0IHsgTnpNZXNzYWdlU2VydmljZSB9IGZyb20gJ25nLXpvcnJvLWFudGQnO1xuaW1wb3J0IHsgSHR0cFNlcnZpY2UgfSBmcm9tICcuLi8uLi9sYmxpYnMvTGJqay5zZXJ2aWNlJztcblxuXG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6ICdsYi1wb3AnLFxuICB0ZW1wbGF0ZVVybDogJy4vcG9wZGV0YWlsLmNvbXBvbmVudC5odG1sJyxcbiAgc3R5bGVVcmxzOiBbJy4vcG9wZGV0YWlsLmNvbXBvbmVudC5jc3MnXVxufSlcbmV4cG9ydCBjbGFzcyBQb3BkZXRhaWxDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xuXG4gIGNvbnN0cnVjdG9yKFxuICAgIHB1YmxpYyBtc2dTcnY6IE56TWVzc2FnZVNlcnZpY2UsXG4gICAgcHJpdmF0ZSBsYnNlcnZpY2U6IEh0dHBTZXJ2aWNlLFxuICApIHsgfVxuXG5cbiAgQFZpZXdDaGlsZCgnc2YnLCB7IHN0YXRpYzogdHJ1ZSB9KSBzZjogU0ZDb21wb25lbnQ7XG5cbiAgLy8g5piv5ZCm5by55Ye6XG4gIEBJbnB1dCgpIGlzVmlzaWJsZSA9IGZhbHNlO1xuICBAT3V0cHV0KCkgaXNWaXNpYmxlQ2hhbmdlID0gbmV3IEV2ZW50RW1pdHRlcigpO1xuXG4gIC8vIOW8ueWHuuahhuagh+mimFxuICBASW5wdXQoKSB0aXRsZSA9IFwi5piO57uG5qGGXCI7XG5cbiAgLy8g5a2X5q615L+h5oGvXG4gIEBJbnB1dCgpIHNjaGVtYTogU0ZTY2hlbWEgPSB7fTtcblxuICAvLyDpu5jorqTlgLxcbiAgQElucHV0KCkgZm9ybURhdGE6IGFueSA9IHt9O1xuXG4gIC8vIOaYr+WQpuWKoOi9veS4rVxuICBASW5wdXQoKSBsb2FkaW5nID0gZmFsc2U7XG4gIEBPdXRwdXQoKSBsb2FkaW5nQ2hhbmdlID0gbmV3IEV2ZW50RW1pdHRlcigpO1xuXG4gIC8vIOS/neWtmOacjeWKoVxuICBASW5wdXQoKSBzYXZlU25hbWUgPSAnJztcbiAgQElucHV0KCkgc2F2ZUZ1OiBhbnk7XG5cbiAgLy8gZm9ybWNoYW5n5LqL5Lu2XG4gIEBPdXRwdXQoKSByZWFkb25seSBmb3JtQ2hhbmdlID0gbmV3IEV2ZW50RW1pdHRlcigpO1xuXG4gIG5nT25Jbml0KCkge1xuICB9XG5cblxuICBoYW5kbGVDYW5jZWwoKSB7XG4gICAgdGhpcy5pc1Zpc2libGUgPSBmYWxzZTtcbiAgICB0aGlzLmlzVmlzaWJsZUNoYW5nZS5lbWl0KHRoaXMuaXNWaXNpYmxlKTtcbiAgfVxuXG5cbiAgc2hvdygpIHtcbiAgICB0aGlzLmlzVmlzaWJsZSA9IHRydWU7XG4gIH1cblxuICBoaWRkZSgpIHtcbiAgICB0aGlzLmlzVmlzaWJsZSA9IHRydWU7XG4gIH1cblxuICByZXNldCgpIHtcbiAgICB0aGlzLnNmLnJlc2V0KCk7XG4gIH1cblxuICB2YWx1ZSgpOiBhbnkge1xuICAgIHJldHVybiB0aGlzLnNmLnZhbHVlO1xuICB9XG5cbiAgLy8gcGF0aCDph4fnlKggLyDmnaXliIbpmpTvvIzkvovlpoLvvJovdXNlci9uYW1lXG4gIGdldFZhbHVlKHBhdGg6IHN0cmluZyk6IGFueSB7XG4gICAgcmV0dXJuIHRoaXMuc2YuZ2V0VmFsdWUocGF0aCk7XG4gIH1cblxuICBzZXRWYWx1ZShwYXRoOiBzdHJpbmcsIHZhbHVlOiBhbnkpIHtcbiAgICB0aGlzLnNmLnNldFZhbHVlKHBhdGgsIHZhbHVlKTtcbiAgfVxuXG4gIHZhbGlkYXRvcigpIHtcbiAgICB0aGlzLnNmLnZhbGlkYXRvcigpO1xuICB9XG5cbiAgcmVmcmVzaFNjaGVtYShuZXdTY2hlbWE/OiBTRlNjaGVtYSwgbmV3VUk/OiBTRlVJU2NoZW1hKSB7XG4gICAgdGhpcy5zZi5yZWZyZXNoU2NoZW1hKG5ld1NjaGVtYSwgbmV3VUkpO1xuICB9XG5cbiAgZ2V0c2YoKTogU0ZDb21wb25lbnQge1xuICAgIHJldHVybiB0aGlzLnNmO1xuICB9XG5cbiAgc2F2ZSh2YWw6IGFueSkge1xuXG4gICAgdGhpcy5sb2FkaW5nID0gdHJ1ZTtcbiAgICB0aGlzLmxvYWRpbmdDaGFuZ2UuZW1pdCh0aGlzLmxvYWRpbmcpO1xuXG4gICAgaWYgKHRoaXMuc2F2ZVNuYW1lICYmIHRoaXMuc2F2ZVNuYW1lICE9PSAnJykge1xuICAgICAgdGhpcy5sYnNlcnZpY2UubGJzZXJ2aWNlKHRoaXMuc2F2ZVNuYW1lLCB7IHBhcmE6IHZhbCB9LCA1MDApLnRoZW4oKHJlc2RhdGEpID0+IHtcbiAgICAgICAgdGhpcy5sb2FkaW5nID0gZmFsc2U7XG4gICAgICAgIHRoaXMubG9hZGluZ0NoYW5nZS5lbWl0KHRoaXMubG9hZGluZyk7XG4gICAgICAgIGlmIChyZXNkYXRhLmNvZGUgPCAxKSB7XG4gICAgICAgICAgdGhpcy5tc2dTcnYuZXJyb3IocmVzZGF0YS5lcnJtc2cpO1xuICAgICAgICAgIHJldHVybjtcbiAgICAgICAgfVxuXG4gICAgICAgIGlmICh0aGlzLnNhdmVGdSAmJiB0eXBlb2YgdGhpcy5zYXZlRnUgPT09ICdmdW5jdGlvbicpIHtcbiAgICAgICAgICB0aGlzLnNhdmVGdSh2YWwsIHJlc2RhdGEpO1xuICAgICAgICB9XG5cbiAgICAgICAgdGhpcy5pc1Zpc2libGUgPSBmYWxzZTtcbiAgICAgICAgdGhpcy5pc1Zpc2libGVDaGFuZ2UuZW1pdCh0aGlzLmlzVmlzaWJsZSk7XG4gICAgICAgIHRoaXMucmVzZXQoKTtcbiAgICAgICAgdGhpcy5tc2dTcnYuaW5mbyhcIuaIkOWKn++8gVwiKTtcblxuICAgICAgfSk7XG4gICAgfSBlbHNlIHtcblxuICAgICAgaWYgKHRoaXMuc2F2ZUZ1ICYmIHR5cGVvZiB0aGlzLnNhdmVGdSA9PT0gJ2Z1bmN0aW9uJykge1xuICAgICAgICB0aGlzLnNhdmVGdSh2YWwsIHt9KTtcbiAgICAgIH1cblxuICAgIH1cblxuICB9XG5cbiAgY2hhbmdlKHZhbHVlKSB7XG4gICAgdGhpcy5mb3JtQ2hhbmdlLmVtaXQodmFsdWUpO1xuICB9XG5cblxufVxuIl19