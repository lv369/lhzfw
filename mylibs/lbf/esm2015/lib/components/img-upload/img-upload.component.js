/**
 * @fileoverview added by tsickle
 * Generated from: lib/components/img-upload/img-upload.component.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component, Input } from '@angular/core';
import { NzMessageService, NzModalService } from 'ng-zorro-antd';
import { Observable, Subscription } from 'rxjs';
import { HttpService } from '../../lblibs/Lbjk.service';
export class ImgUploadComponent {
    /**
     * @param {?} msgsvr
     * @param {?} lbservice
     * @param {?} modalService
     */
    constructor(msgsvr, lbservice, modalService) {
        this.msgsvr = msgsvr;
        this.lbservice = lbservice;
        this.modalService = modalService;
        /**
         * 图表列表
         */
        this.fileList = [];
        this._previewImage = '';
        this._previewVisible = false;
        /**
         * 上传按钮是否显示
         */
        this.nzShowButton = true;
        /**
         * 预览按钮以及删除按钮
         */
        this.showUploadList = {
            showPreviewIcon: true,
            showRemoveIcon: true,
            hidePreviewIconInNonImage: true
        };
        /**
         * 上传事件
         */
        this.upload = (/**
         * @param {?} item
         * @return {?}
         */
        (item) => Subscription);
        /**
         * 删除事件
         */
        this.del = (/**
         * @param {?} file
         * @return {?}
         */
        (file) => new Promise((/**
         * @return {?}
         */
        () => { })));
        this._handlePreview = (/**
         * @param {?} file
         * @return {?}
         */
        (file) => {
            this._previewImage = file.url || file.thumbUrl;
            this._previewVisible = true;
        });
        /**
         * 图片上传
         */
        this._uploadimg = (/**
         * @param {?} item
         * @return {?}
         */
        (item) => {
            if (this.upload) {
                return this.upload(item);
            }
        });
        /**
         * 移除文件
         */
        this._removeIMG = (/**
         * @param {?} file
         * @return {?}
         */
        (file) => {
            return new Observable((/**
             * @param {?} obs
             * @return {?}
             */
            (obs) => {
                this.modalService.confirm({
                    nzTitle: '删除确认',
                    nzContent: '<b style="color: red;">确认要删除这张图片么？</b>',
                    nzOkText: '确定',
                    nzOkType: 'danger',
                    nzOnOk: (/**
                     * @return {?}
                     */
                    () => {
                        if (this.del) {
                            this.del(file).then((/**
                             * @param {?} resdata
                             * @return {?}
                             */
                            (resdata) => {
                                if (resdata.code < 1) {
                                    this.msgsvr.error("图片删除失败:" + resdata.errmsg);
                                    obs.next(false);
                                }
                                else {
                                    this.msgsvr.success('图片删除成功！');
                                    obs.next(true);
                                }
                            }));
                        }
                    }),
                    nzCancelText: '取消',
                    nzOnCancel: (/**
                     * @return {?}
                     */
                    () => obs.next(false))
                });
            }));
        });
        this._beforeUpload = (/**
         * @param {?} file
         * @return {?}
         */
        (file) => {
            return new Observable((/**
             * @param {?} observer
             * @return {?}
             */
            (observer) => {
                /** @type {?} */
                const imgtype = ["image/png", "image/jpeg", "image/gif", "image/bmp"];
                //  const isImg = file.type === 'image/jpeg';
                if (!imgtype.includes(file.type)) {
                    this.msgsvr.error('只能上传图片格式!');
                    observer.complete();
                    return;
                }
                observer.next(true);
                observer.complete();
            }));
        });
    }
    /**
     * @return {?}
     */
    ngOnInit() {
    }
    /**
     * 上传成功后处理
     * @param {?} info  上传返回信息
     * @return {?}
     */
    _handleChange(info) {
        /** @type {?} */
        const fileList = info.fileList;
        // 2. read from response and show file link
        if (info.file.response) {
            if (info.file.response.code < 1) {
                this.msgsvr.error("文件上传失败:" + info.file.response.errmsg);
            }
            else {
                info.file.ftoken = info.file.response.message.ftoken;
            }
            info.file.url = info.file.response.url;
        }
        // 3. filter successfully uploaded files according to response from server
        this.fileList = fileList.filter((/**
         * @param {?} item
         * @return {?}
         */
        item => {
            if (item.response) {
                if (item.response.code > 0) {
                    item.ftoken = item.response.message.ftoken;
                    return true;
                }
                return false;
            }
            return true;
        }));
    }
}
ImgUploadComponent.decorators = [
    { type: Component, args: [{
                selector: 'lb-img',
                template: "<div>\n    <nz-upload nzListType=\"picture-card\" [(nzFileList)]=\"fileList\" [nzCustomRequest]=\"_uploadimg\"\n        [nzRemove]=\"_removeIMG\" [nzBeforeUpload]=\"_beforeUpload\" (nzChange)=\"_handleChange($event)\"\n        [nzShowButton]=\"nzShowButton\" [nzSize]=\"10*1024\" [nzShowUploadList]=\"showUploadList\"\n        [nzPreview]=\"_handlePreview\">\n        <div class=\"ant-upload-text\">\u4E0A\u4F20\u56FE\u7247</div>\n    </nz-upload>\n    <nz-modal [nzVisible]=\"_previewVisible\" [nzContent]=\"modalContent\" [nzFooter]=\"null\"\n        (nzOnCancel)=\"_previewVisible=false\">\n        <ng-template #modalContent>\n            <img [src]=\"_previewImage\" [ngStyle]=\"{ 'width': '100%' }\" />\n        </ng-template>\n    </nz-modal>\n</div>",
                styles: [""]
            }] }
];
/** @nocollapse */
ImgUploadComponent.ctorParameters = () => [
    { type: NzMessageService },
    { type: HttpService },
    { type: NzModalService }
];
ImgUploadComponent.propDecorators = {
    fileList: [{ type: Input }],
    nzShowButton: [{ type: Input }],
    showUploadList: [{ type: Input }],
    upload: [{ type: Input }],
    del: [{ type: Input }]
};
if (false) {
    /**
     * 图表列表
     * @type {?}
     */
    ImgUploadComponent.prototype.fileList;
    /** @type {?} */
    ImgUploadComponent.prototype._previewImage;
    /** @type {?} */
    ImgUploadComponent.prototype._previewVisible;
    /**
     * 上传按钮是否显示
     * @type {?}
     */
    ImgUploadComponent.prototype.nzShowButton;
    /**
     * 预览按钮以及删除按钮
     * @type {?}
     */
    ImgUploadComponent.prototype.showUploadList;
    /**
     * 上传事件
     * @type {?}
     */
    ImgUploadComponent.prototype.upload;
    /**
     * 删除事件
     * @type {?}
     */
    ImgUploadComponent.prototype.del;
    /** @type {?} */
    ImgUploadComponent.prototype._handlePreview;
    /**
     * 图片上传
     * @type {?}
     */
    ImgUploadComponent.prototype._uploadimg;
    /**
     * 移除文件
     * @type {?}
     */
    ImgUploadComponent.prototype._removeIMG;
    /** @type {?} */
    ImgUploadComponent.prototype._beforeUpload;
    /**
     * @type {?}
     * @private
     */
    ImgUploadComponent.prototype.msgsvr;
    /**
     * @type {?}
     * @private
     */
    ImgUploadComponent.prototype.lbservice;
    /**
     * @type {?}
     * @private
     */
    ImgUploadComponent.prototype.modalService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW1nLXVwbG9hZC5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9sYmYvIiwic291cmNlcyI6WyJsaWIvY29tcG9uZW50cy9pbWctdXBsb2FkL2ltZy11cGxvYWQuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7O0FBQUEsT0FBTyxFQUFFLFNBQVMsRUFBVSxLQUFLLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDekQsT0FBTyxFQUFFLGdCQUFnQixFQUFFLGNBQWMsRUFBNkIsTUFBTSxlQUFlLENBQUM7QUFDNUYsT0FBTyxFQUFFLFVBQVUsRUFBRSxZQUFZLEVBQVksTUFBTSxNQUFNLENBQUM7QUFDMUQsT0FBTyxFQUFFLFdBQVcsRUFBRSxNQUFNLDJCQUEyQixDQUFDO0FBT3hELE1BQU0sT0FBTyxrQkFBa0I7Ozs7OztJQUU3QixZQUFvQixNQUF3QixFQUNsQyxTQUFzQixFQUN0QixZQUE0QjtRQUZsQixXQUFNLEdBQU4sTUFBTSxDQUFrQjtRQUNsQyxjQUFTLEdBQVQsU0FBUyxDQUFhO1FBQ3RCLGlCQUFZLEdBQVosWUFBWSxDQUFnQjs7OztRQUs3QixhQUFRLEdBQUcsRUFBRSxDQUFDO1FBQ3ZCLGtCQUFhLEdBQUcsRUFBRSxDQUFDO1FBQ25CLG9CQUFlLEdBQUcsS0FBSyxDQUFDOzs7O1FBSWYsaUJBQVksR0FBRyxJQUFJLENBQUM7Ozs7UUFLcEIsbUJBQWMsR0FBRztZQUN4QixlQUFlLEVBQUUsSUFBSTtZQUNyQixjQUFjLEVBQUUsSUFBSTtZQUNwQix5QkFBeUIsRUFBRSxJQUFJO1NBQ2hDLENBQUM7Ozs7UUFRTyxXQUFNOzs7O1FBQUcsQ0FBQyxJQUFtQixFQUFFLEVBQUUsQ0FBQyxZQUFZLEVBQUM7Ozs7UUFJL0MsUUFBRzs7OztRQUFHLENBQUMsSUFBZ0IsRUFBRSxFQUFFLENBQUMsSUFBSSxPQUFPOzs7UUFBQyxHQUFHLEVBQUUsR0FBRyxDQUFDLEVBQUMsRUFBQztRQUU1RCxtQkFBYzs7OztRQUFHLENBQUMsSUFBZ0IsRUFBRSxFQUFFO1lBQ3BDLElBQUksQ0FBQyxhQUFhLEdBQUcsSUFBSSxDQUFDLEdBQUcsSUFBSSxJQUFJLENBQUMsUUFBUSxDQUFDO1lBQy9DLElBQUksQ0FBQyxlQUFlLEdBQUcsSUFBSSxDQUFDO1FBQzlCLENBQUMsRUFBQTs7OztRQU1ELGVBQVU7Ozs7UUFBRyxDQUFDLElBQW1CLEVBQUUsRUFBRTtZQUNuQyxJQUFJLElBQUksQ0FBQyxNQUFNLEVBQUU7Z0JBQ2YsT0FBTyxJQUFJLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxDQUFDO2FBQzFCO1FBQ0gsQ0FBQyxFQUFBOzs7O1FBS0QsZUFBVTs7OztRQUFHLENBQUMsSUFBZ0IsRUFBRSxFQUFFO1lBRWhDLE9BQU8sSUFBSSxVQUFVOzs7O1lBQVUsQ0FBQyxHQUFHLEVBQUUsRUFBRTtnQkFFckMsSUFBSSxDQUFDLFlBQVksQ0FBQyxPQUFPLENBQUM7b0JBQ3hCLE9BQU8sRUFBRSxNQUFNO29CQUNmLFNBQVMsRUFBRSx3Q0FBd0M7b0JBQ25ELFFBQVEsRUFBRSxJQUFJO29CQUNkLFFBQVEsRUFBRSxRQUFRO29CQUNsQixNQUFNOzs7b0JBQUUsR0FBRyxFQUFFO3dCQUVYLElBQUksSUFBSSxDQUFDLEdBQUcsRUFBRTs0QkFFWixJQUFJLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxDQUFDLElBQUk7Ozs7NEJBQUMsQ0FBQyxPQUFZLEVBQUUsRUFBRTtnQ0FDbkMsSUFBSSxPQUFPLENBQUMsSUFBSSxHQUFHLENBQUMsRUFBRTtvQ0FDcEIsSUFBSSxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsU0FBUyxHQUFHLE9BQU8sQ0FBQyxNQUFNLENBQUMsQ0FBQztvQ0FDOUMsR0FBRyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztpQ0FDakI7cUNBQU07b0NBQ0wsSUFBSSxDQUFDLE1BQU0sQ0FBQyxPQUFPLENBQUMsU0FBUyxDQUFDLENBQUM7b0NBQy9CLEdBQUcsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7aUNBQ2hCOzRCQUVILENBQUMsRUFBQyxDQUFBO3lCQUNIO29CQUVILENBQUMsQ0FBQTtvQkFDRCxZQUFZLEVBQUUsSUFBSTtvQkFDbEIsVUFBVTs7O29CQUFFLEdBQUcsRUFBRSxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUE7aUJBQ2xDLENBQUMsQ0FBQztZQUdMLENBQUMsRUFBQyxDQUFDO1FBQ0wsQ0FBQyxFQUFBO1FBcUNELGtCQUFhOzs7O1FBQUcsQ0FBQyxJQUFVLEVBQUUsRUFBRTtZQUM3QixPQUFPLElBQUksVUFBVTs7OztZQUFDLENBQUMsUUFBMkIsRUFBRSxFQUFFOztzQkFFOUMsT0FBTyxHQUFHLENBQUMsV0FBVyxFQUFFLFlBQVksRUFBRSxXQUFXLEVBQUUsV0FBVyxDQUFDO2dCQUVyRSw2Q0FBNkM7Z0JBQzdDLElBQUksQ0FBQyxPQUFPLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsRUFBRTtvQkFDaEMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsV0FBVyxDQUFDLENBQUM7b0JBQy9CLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQztvQkFDcEIsT0FBTztpQkFDUjtnQkFFRCxRQUFRLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO2dCQUNwQixRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7WUFFdEIsQ0FBQyxFQUFDLENBQUM7UUFDTCxDQUFDLEVBQUM7SUF6SXdDLENBQUM7Ozs7SUFzQjNDLFFBQVE7SUFDUixDQUFDOzs7Ozs7SUFtRUQsYUFBYSxDQUFDLElBQVM7O2NBRWYsUUFBUSxHQUFHLElBQUksQ0FBQyxRQUFRO1FBQzlCLDJDQUEyQztRQUMzQyxJQUFJLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxFQUFFO1lBQ3RCLElBQUksSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsSUFBSSxHQUFHLENBQUMsRUFBRTtnQkFDL0IsSUFBSSxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLE1BQU0sQ0FBQyxDQUFDO2FBQzFEO2lCQUFNO2dCQUVMLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLE9BQU8sQ0FBQyxNQUFNLENBQUM7YUFDdEQ7WUFHRCxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxHQUFHLENBQUM7U0FDeEM7UUFDRCwwRUFBMEU7UUFDMUUsSUFBSSxDQUFDLFFBQVEsR0FBRyxRQUFRLENBQUMsTUFBTTs7OztRQUFDLElBQUksQ0FBQyxFQUFFO1lBQ3JDLElBQUksSUFBSSxDQUFDLFFBQVEsRUFBRTtnQkFFakIsSUFBSSxJQUFJLENBQUMsUUFBUSxDQUFDLElBQUksR0FBRyxDQUFDLEVBQUU7b0JBRTFCLElBQUksQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQyxPQUFPLENBQUMsTUFBTSxDQUFDO29CQUMzQyxPQUFPLElBQUksQ0FBQztpQkFDYjtnQkFFRCxPQUFPLEtBQUssQ0FBQzthQUNkO1lBQ0QsT0FBTyxJQUFJLENBQUM7UUFDZCxDQUFDLEVBQUMsQ0FBQztJQUNMLENBQUM7OztZQWhJRixTQUFTLFNBQUM7Z0JBQ1QsUUFBUSxFQUFFLFFBQVE7Z0JBQ2xCLGl3QkFBMEM7O2FBRTNDOzs7O1lBUlEsZ0JBQWdCO1lBRWhCLFdBQVc7WUFGTyxjQUFjOzs7dUJBa0J0QyxLQUFLOzJCQU1MLEtBQUs7NkJBS0wsS0FBSztxQkFZTCxLQUFLO2tCQUlMLEtBQUs7Ozs7Ozs7SUEzQk4sc0NBQXVCOztJQUN2QiwyQ0FBbUI7O0lBQ25CLDZDQUF3Qjs7Ozs7SUFJeEIsMENBQTZCOzs7OztJQUs3Qiw0Q0FJRTs7Ozs7SUFRRixvQ0FBd0Q7Ozs7O0lBSXhELGlDQUE0RDs7SUFFNUQsNENBR0M7Ozs7O0lBTUQsd0NBSUM7Ozs7O0lBS0Qsd0NBZ0NDOztJQXFDRCwyQ0FnQkU7Ozs7O0lBM0lVLG9DQUFnQzs7Ozs7SUFDMUMsdUNBQThCOzs7OztJQUM5QiwwQ0FBb0MiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIE9uSW5pdCwgSW5wdXQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IE56TWVzc2FnZVNlcnZpY2UsIE56TW9kYWxTZXJ2aWNlLCBVcGxvYWRGaWxlLCBVcGxvYWRYSFJBcmdzIH0gZnJvbSAnbmctem9ycm8tYW50ZCc7XG5pbXBvcnQgeyBPYnNlcnZhYmxlLCBTdWJzY3JpcHRpb24sIE9ic2VydmVyIH0gZnJvbSAncnhqcyc7XG5pbXBvcnQgeyBIdHRwU2VydmljZSB9IGZyb20gJy4uLy4uL2xibGlicy9MYmprLnNlcnZpY2UnO1xuXG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6ICdsYi1pbWcnLFxuICB0ZW1wbGF0ZVVybDogJy4vaW1nLXVwbG9hZC5jb21wb25lbnQuaHRtbCcsXG4gIHN0eWxlVXJsczogWycuL2ltZy11cGxvYWQuY29tcG9uZW50LmNzcyddXG59KVxuZXhwb3J0IGNsYXNzIEltZ1VwbG9hZENvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XG5cbiAgY29uc3RydWN0b3IocHJpdmF0ZSBtc2dzdnI6IE56TWVzc2FnZVNlcnZpY2UsXG4gICAgcHJpdmF0ZSBsYnNlcnZpY2U6IEh0dHBTZXJ2aWNlLFxuICAgIHByaXZhdGUgbW9kYWxTZXJ2aWNlOiBOek1vZGFsU2VydmljZSkgeyB9XG5cbiAgLyoqXG4gICAqIOWbvuihqOWIl+ihqFxuICAgKi9cbiAgQElucHV0KCkgZmlsZUxpc3QgPSBbXTtcbiAgX3ByZXZpZXdJbWFnZSA9ICcnO1xuICBfcHJldmlld1Zpc2libGUgPSBmYWxzZTtcbiAgLyoqXG4gICAqIOS4iuS8oOaMiemSruaYr+WQpuaYvuekulxuICAgKi9cbiAgQElucHV0KCkgbnpTaG93QnV0dG9uID0gdHJ1ZTtcblxuICAvKipcbiAgICog6aKE6KeI5oyJ6ZKu5Lul5Y+K5Yig6Zmk5oyJ6ZKuXG4gICAqL1xuICBASW5wdXQoKSBzaG93VXBsb2FkTGlzdCA9IHtcbiAgICBzaG93UHJldmlld0ljb246IHRydWUsXG4gICAgc2hvd1JlbW92ZUljb246IHRydWUsXG4gICAgaGlkZVByZXZpZXdJY29uSW5Ob25JbWFnZTogdHJ1ZVxuICB9O1xuXG4gIG5nT25Jbml0KCkge1xuICB9XG5cbiAgLyoqXG4gICAqIOS4iuS8oOS6i+S7tlxuICAgKi9cbiAgQElucHV0KCkgdXBsb2FkID0gKGl0ZW06IFVwbG9hZFhIUkFyZ3MpID0+IFN1YnNjcmlwdGlvbjtcbiAgLyoqXG4gICAqIOWIoOmZpOS6i+S7tlxuICAgKi9cbiAgQElucHV0KCkgZGVsID0gKGZpbGU6IFVwbG9hZEZpbGUpID0+IG5ldyBQcm9taXNlKCgpID0+IHsgfSk7XG5cbiAgX2hhbmRsZVByZXZpZXcgPSAoZmlsZTogVXBsb2FkRmlsZSkgPT4ge1xuICAgIHRoaXMuX3ByZXZpZXdJbWFnZSA9IGZpbGUudXJsIHx8IGZpbGUudGh1bWJVcmw7XG4gICAgdGhpcy5fcHJldmlld1Zpc2libGUgPSB0cnVlO1xuICB9XG5cblxuICAvKipcbiAgICog5Zu+54mH5LiK5LygXG4gICAqL1xuICBfdXBsb2FkaW1nID0gKGl0ZW06IFVwbG9hZFhIUkFyZ3MpID0+IHtcbiAgICBpZiAodGhpcy51cGxvYWQpIHtcbiAgICAgIHJldHVybiB0aGlzLnVwbG9hZChpdGVtKTtcbiAgICB9XG4gIH1cblxuICAvKipcbiAgICog56e76Zmk5paH5Lu2XG4gICAqL1xuICBfcmVtb3ZlSU1HID0gKGZpbGU6IFVwbG9hZEZpbGUpID0+IHtcblxuICAgIHJldHVybiBuZXcgT2JzZXJ2YWJsZTxib29sZWFuPigob2JzKSA9PiB7XG5cbiAgICAgIHRoaXMubW9kYWxTZXJ2aWNlLmNvbmZpcm0oe1xuICAgICAgICBuelRpdGxlOiAn5Yig6Zmk56Gu6K6kJyxcbiAgICAgICAgbnpDb250ZW50OiAnPGIgc3R5bGU9XCJjb2xvcjogcmVkO1wiPuehruiupOimgeWIoOmZpOi/meW8oOWbvueJh+S5iO+8nzwvYj4nLFxuICAgICAgICBuek9rVGV4dDogJ+ehruWumicsXG4gICAgICAgIG56T2tUeXBlOiAnZGFuZ2VyJyxcbiAgICAgICAgbnpPbk9rOiAoKSA9PiB7XG5cbiAgICAgICAgICBpZiAodGhpcy5kZWwpIHtcblxuICAgICAgICAgICAgdGhpcy5kZWwoZmlsZSkudGhlbigocmVzZGF0YTogYW55KSA9PiB7XG4gICAgICAgICAgICAgIGlmIChyZXNkYXRhLmNvZGUgPCAxKSB7XG4gICAgICAgICAgICAgICAgdGhpcy5tc2dzdnIuZXJyb3IoXCLlm77niYfliKDpmaTlpLHotKU6XCIgKyByZXNkYXRhLmVycm1zZyk7XG4gICAgICAgICAgICAgICAgb2JzLm5leHQoZmFsc2UpO1xuICAgICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgIHRoaXMubXNnc3ZyLnN1Y2Nlc3MoJ+WbvueJh+WIoOmZpOaIkOWKn++8gScpO1xuICAgICAgICAgICAgICAgIG9icy5uZXh0KHRydWUpO1xuICAgICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIH0pXG4gICAgICAgICAgfVxuXG4gICAgICAgIH0sXG4gICAgICAgIG56Q2FuY2VsVGV4dDogJ+WPlua2iCcsXG4gICAgICAgIG56T25DYW5jZWw6ICgpID0+IG9icy5uZXh0KGZhbHNlKVxuICAgICAgfSk7XG5cblxuICAgIH0pO1xuICB9XG5cbiAgLyoqXG4gICAqIOS4iuS8oOaIkOWKn+WQjuWkhOeQhlxuICAgKiBAcGFyYW0gaW5mbyAg5LiK5Lyg6L+U5Zue5L+h5oGvXG4gICAqL1xuICBfaGFuZGxlQ2hhbmdlKGluZm86IGFueSk6IHZvaWQge1xuXG4gICAgY29uc3QgZmlsZUxpc3QgPSBpbmZvLmZpbGVMaXN0O1xuICAgIC8vIDIuIHJlYWQgZnJvbSByZXNwb25zZSBhbmQgc2hvdyBmaWxlIGxpbmtcbiAgICBpZiAoaW5mby5maWxlLnJlc3BvbnNlKSB7XG4gICAgICBpZiAoaW5mby5maWxlLnJlc3BvbnNlLmNvZGUgPCAxKSB7XG4gICAgICAgIHRoaXMubXNnc3ZyLmVycm9yKFwi5paH5Lu25LiK5Lyg5aSx6LSlOlwiICsgaW5mby5maWxlLnJlc3BvbnNlLmVycm1zZyk7XG4gICAgICB9IGVsc2Uge1xuXG4gICAgICAgIGluZm8uZmlsZS5mdG9rZW4gPSBpbmZvLmZpbGUucmVzcG9uc2UubWVzc2FnZS5mdG9rZW47XG4gICAgICB9XG5cblxuICAgICAgaW5mby5maWxlLnVybCA9IGluZm8uZmlsZS5yZXNwb25zZS51cmw7XG4gICAgfVxuICAgIC8vIDMuIGZpbHRlciBzdWNjZXNzZnVsbHkgdXBsb2FkZWQgZmlsZXMgYWNjb3JkaW5nIHRvIHJlc3BvbnNlIGZyb20gc2VydmVyXG4gICAgdGhpcy5maWxlTGlzdCA9IGZpbGVMaXN0LmZpbHRlcihpdGVtID0+IHtcbiAgICAgIGlmIChpdGVtLnJlc3BvbnNlKSB7XG5cbiAgICAgICAgaWYgKGl0ZW0ucmVzcG9uc2UuY29kZSA+IDApIHtcblxuICAgICAgICAgIGl0ZW0uZnRva2VuID0gaXRlbS5yZXNwb25zZS5tZXNzYWdlLmZ0b2tlbjtcbiAgICAgICAgICByZXR1cm4gdHJ1ZTtcbiAgICAgICAgfVxuXG4gICAgICAgIHJldHVybiBmYWxzZTtcbiAgICAgIH1cbiAgICAgIHJldHVybiB0cnVlO1xuICAgIH0pO1xuICB9XG5cbiAgX2JlZm9yZVVwbG9hZCA9IChmaWxlOiBGaWxlKSA9PiB7XG4gICAgcmV0dXJuIG5ldyBPYnNlcnZhYmxlKChvYnNlcnZlcjogT2JzZXJ2ZXI8Ym9vbGVhbj4pID0+IHtcblxuICAgICAgY29uc3QgaW1ndHlwZSA9IFtcImltYWdlL3BuZ1wiLCBcImltYWdlL2pwZWdcIiwgXCJpbWFnZS9naWZcIiwgXCJpbWFnZS9ibXBcIl07XG5cbiAgICAgIC8vICBjb25zdCBpc0ltZyA9IGZpbGUudHlwZSA9PT0gJ2ltYWdlL2pwZWcnO1xuICAgICAgaWYgKCFpbWd0eXBlLmluY2x1ZGVzKGZpbGUudHlwZSkpIHtcbiAgICAgICAgdGhpcy5tc2dzdnIuZXJyb3IoJ+WPquiDveS4iuS8oOWbvueJh+agvOW8jyEnKTtcbiAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcbiAgICAgICAgcmV0dXJuO1xuICAgICAgfVxuXG4gICAgICBvYnNlcnZlci5uZXh0KHRydWUpO1xuICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcblxuICAgIH0pO1xuICB9O1xufVxuIl19