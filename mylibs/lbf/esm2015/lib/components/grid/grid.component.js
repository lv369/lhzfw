/**
 * @fileoverview added by tsickle
 * Generated from: lib/components/grid/grid.component.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
import { Component, Input, Output, EventEmitter, ViewChild, TemplateRef } from '@angular/core';
import { STComponent, } from '@delon/abc';
import { SupDic } from '../../lblibs/SupDic';
import { SupExcel } from '../../lblibs/SupExcel';
import { LbRowSource } from './LbColumn';
import { InputNumber, InputBoolean } from 'ng-zorro-antd';
import { ServiceInfo } from '../../ServiceConfig';
export class GridComponent {
    /**
     * @param {?} supdic
     * @param {?} supexcel
     * @param {?} sinfo
     * @param {?} lbSource
     */
    constructor(supdic, supexcel, sinfo, lbSource) {
        this.supdic = supdic;
        this.supexcel = supexcel;
        this.sinfo = sinfo;
        this.lbSource = lbSource;
        this.grhxurl = ['/ybjg/grhx'];
        this.isGrhx = true;
        /**
         * 每页数量，当设置为 0 表示不分页，默认：10
         */
        this.ps = 10;
        this.datas = 'MohrssExInterface/gridsvr';
        this.statisticals = [];
        this._statcal = {};
        // 是否加载中
        this.loading = false;
        this.loadingChange = new EventEmitter();
        // 服务ID
        this._sname = '';
        this.reqparas = { params: { sname: this._sname, form: this._queryparas, stacal: this._statcal }, method: 'post', allInBody: true };
        this.process = (/**
         * @param {?} data
         * @return {?}
         */
        (data) => {
            return data;
        });
    }
    /**
     * 表格数值
     * @return {?}
     */
    get data() {
        return this.st._data;
    }
    /**
     * @return {?}
     */
    get innerSt() {
        return this.st;
    }
    /**
     * @param {?} columns
     * @return {?}
     */
    set columns(columns) {
        // 统计类信息
        this.statisticals = [];
        columns.forEach((/**
         * @param {?} col
         * @return {?}
         */
        col => {
            if (col.render) {
                col.render2 = col.render;
                col.render = '';
            }
            else {
                if (this.isGrhx && col.index === 'AAC003' && this.sinfo.isAdmin) {
                    col.render = 'grhx';
                }
            }
            if (col.renderTitle) {
                col.renderTitle2 = col.renderTitle;
                col.renderTitle = '';
            }
            /** @type {?} */
            let key = col.index;
            if (key instanceof Array) {
                key = key[0];
            }
            col.key = key;
            // 添加字典信息
            if (col.dic) {
                col.format = (/**
                 * @param {?} a
                 * @param {?} b
                 * @return {?}
                 */
                (a, b) => this.supdic.getdicLabel(b.dic, a[b.key]));
                col.filter = { menus: this.supdic.getDic(col.dic) };
            }
            // 添加统计类型信息
            /** @type {?} */
            const val = col.statistical;
            if (!val) {
                this.statisticals.push({ key, show: false, type: "" });
                this._statcal[key] = "";
                return;
            }
            /** @type {?} */
            const item = Object.assign({ digits: 2, currency: null }, (typeof val === 'string'
                ? { type: (/** @type {?} */ (val)) }
                : ((/** @type {?} */ (val)))));
            /** @type {?} */
            const stcal = item.type;
            item.digits = stcal === "count" ? 0 : 2;
            item.type = this.customStatistical;
            item.currency = false;
            col.statistical = item;
            this.statisticals.push({ key, show: true, type: stcal });
            this._statcal[key] = stcal;
        }));
        this._columns = columns;
    }
    /**
     * @return {?}
     */
    get columns() {
        return this._columns;
    }
    /**
     * @param {?} sname
     * @return {?}
     */
    set sname(sname) {
        this._sname = sname;
        this.reqparas.params.sname = this._sname;
    }
    // 查询参数
    /**
     * @return {?}
     */
    get queryparas() {
        return this._queryparas;
    }
    /**
     * @param {?} value
     * @return {?}
     */
    set queryparas(value) {
        this._queryparas = value;
        this.reqparas.params.form = this._queryparas;
    }
    /**
     * 加载指示符
     * @param {?} loadingIndicator
     * @return {?}
     */
    set loadingIndicator(loadingIndicator) {
        this.st.loadingIndicator = loadingIndicator;
    }
    /**
     * 延迟显示加载效果的时间（防止闪烁）
     * @param {?} loadingDelay
     * @return {?}
     */
    set loadingDelay(loadingDelay) {
        this.st.loadingDelay = loadingDelay;
    }
    /**
     * 当前页码
     * @param {?} pi
     * @return {?}
     */
    set pi(pi) {
        this.st.pi = pi;
    }
    /**
     * @return {?}
     */
    get pi() {
        return this.st.pi;
    }
    /**
     *  分页器配置
     * @param {?} page
     * @return {?}
     */
    set page(page) {
        this.st.page = page;
    }
    /**
     * @return {?}
     */
    get page() {
        return this.st.page;
    }
    /**
     * 无数据时显示内容
     * @param {?} noResult
     * @return {?}
     */
    set noResult(noResult) {
        this.st.noResult = noResult;
    }
    /**
     * @return {?}
     */
    get noResult() {
        return this.st.noResult;
    }
    /**
     * 是否显示边框
     * @param {?} bordered
     * @return {?}
     */
    set bordered(bordered) {
        this.st.bordered = bordered;
    }
    /**
     * @return {?}
     */
    get bordered() {
        return this.st.bordered;
    }
    /**
     * 表格标题
     * @param {?} header
     * @return {?}
     */
    set header(header) {
        this.st.header = header;
    }
    /**
     * 表格底部
     * @param {?} footer
     * @return {?}
     */
    set footer(footer) {
        this.st.footer = footer;
    }
    /**
     * 表格顶部额外内容，一般用于添加合计行
     * @param {?} bodyHeader
     * @return {?}
     */
    set bodyHeader(bodyHeader) {
        this.st.bodyHeader = bodyHeader;
    }
    /**
     * @param {?} expandRowByClick
     * @return {?}
     */
    set expandRowByClick(expandRowByClick) {
        this.st.expandRowByClick = expandRowByClick;
    }
    /**
     * @param {?} expandAccordion
     * @return {?}
     */
    set expandAccordion(expandAccordion) {
        this.st.expandAccordion = expandAccordion;
    }
    /**
     * @param {?} expand
     * @return {?}
     */
    set expand(expand) {
        this.st.expand = expand;
    }
    /**
     * @return {?}
     */
    get filteredData() {
        return this.st.filteredData;
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        setTimeout((/**
         * @return {?}
         */
        () => {
            this.columns.forEach((/**
             * @param {?} col
             * @return {?}
             */
            col => {
                this.restoreRender(col);
            }));
            this.st.resetColumns();
        }), 100);
    }
    /**
     * @param {?=} options
     * @return {?}
     */
    resetColumns(options) {
        this.st.resetColumns(options);
    }
    /**
     * 移除行
     * @param {?} data STData | STData[] | number
     * @return {?}
     */
    removeRow(data) {
        this.st.removeRow(data);
    }
    /**
     * 清空所有数据
     * @return {?}
     */
    clear() {
        this.st.clear();
    }
    /**
     * @param {?=} queryparas
     * @return {?}
     */
    reload(queryparas) {
        this.st.loading = true;
        if (queryparas)
            this.st.req.params.form = queryparas;
        this.st.reload();
        this.st.cd();
    }
    /**
     * @param {?} index
     * @param {?} data
     * @return {?}
     */
    setRow(index, data) {
        this.st.setRow(index, data);
    }
    /**
     * @param {?} event
     * @return {?}
     */
    reset(event) {
        this.loading = true;
        this.loadingChange.emit(true);
        this.st.reset(event);
        this.st.cd();
    }
    /**
     * @private
     * @param {?} values
     * @param {?} col
     * @param {?} list
     * @param {?=} rawData
     * @return {?}
     */
    customStatistical(values, col, list, rawData) {
        if (rawData.code < 1)
            return { value: "", text: "" };
        if (rawData.page.statistical) {
            /** @type {?} */
            let key = col.index;
            if (key instanceof Array) {
                key = key[0];
            }
            if (rawData.page.statistical[key]) {
                return { value: rawData.page.statistical[key], text: "" };
            }
            return { value: "", text: "" };
        }
        return { value: "", text: "" };
    }
    // 导出excel
    /**
     * @param {?=} opt
     * @return {?}
     */
    export(opt) {
        /** @type {?} */
        const data = this.st._data;
        this.st.export(data, opt);
    }
    /**
     * @return {?}
     */
    exportAll() {
        this.loading = true;
        this.supexcel.export(this.columns, this._sname, this.st.req.params.form, this.st.total).then((/**
         * @return {?}
         */
        () => {
            this.loading = false;
        }));
    }
    /**
     * @private
     * @param {?} item
     * @return {?}
     */
    restoreRender(item) {
        if (item.renderTitle2) {
            item.__renderTitle = this.lbSource.getTitle(item.renderTitle2);
        }
        if (item.render2) {
            item.__render = this.lbSource.getRow(item.render2);
        }
    }
}
GridComponent.decorators = [
    { type: Component, args: [{
                selector: 'lb-grid',
                template: "<st #st [data]=\"datas\" [req]=\"reqparas\" [loading]=\"loading\" [body]=\"bodyTpl\" [scroll]=\"scroll\" multiSort [ps]=\"ps\"\n    [res]=\"{reName :{list:'message.list',total:'page.COUNT',process: dataProcess}}\" [columns]=\"columns\">\n\n    <ng-template st-row=\"grhx\" let-item let-c=\"column\">\n        <a [routerLink]=\"grhxurl\" [queryParams]=\"item\">{{item[c.indexKey]}}</a>\n    </ng-template>\n\n    <ng-template #bodyTpl let-s>\n        <tr class=\"bg-grey-lighter\">\n            <ng-container *ngFor=\"let statis of statisticals\">\n                <td>\n                    <div *ngIf=\"statis.show&&s[statis.key]\">\n                        {{ s[statis.key].text}}\n                    </div>\n                </td>\n            </ng-container>\n        </tr>\n    </ng-template>\n</st>",
                providers: [LbRowSource],
                styles: [""]
            }] }
];
/** @nocollapse */
GridComponent.ctorParameters = () => [
    { type: SupDic },
    { type: SupExcel },
    { type: ServiceInfo },
    { type: LbRowSource }
];
GridComponent.propDecorators = {
    grhxurl: [{ type: Input }],
    isGrhx: [{ type: Input }],
    columns: [{ type: Input, args: ['columns',] }],
    sname: [{ type: Input, args: ['sname',] }],
    queryparas: [{ type: Input }],
    ps: [{ type: Input }],
    loading: [{ type: Input }],
    loadingChange: [{ type: Output }],
    loadingIndicator: [{ type: Input }],
    loadingDelay: [{ type: Input }],
    scroll: [{ type: Input }],
    pi: [{ type: Input }],
    noResult: [{ type: Input }],
    bordered: [{ type: Input }],
    header: [{ type: Input }],
    footer: [{ type: Input }],
    bodyHeader: [{ type: Input }],
    expandRowByClick: [{ type: Input }],
    expandAccordion: [{ type: Input }],
    expand: [{ type: Input }],
    st: [{ type: ViewChild, args: ['st', { static: true },] }]
};
tslib_1.__decorate([
    InputBoolean(),
    tslib_1.__metadata("design:type", Object)
], GridComponent.prototype, "isGrhx", void 0);
tslib_1.__decorate([
    InputNumber(),
    tslib_1.__metadata("design:type", Number),
    tslib_1.__metadata("design:paramtypes", [Number])
], GridComponent.prototype, "loadingDelay", null);
tslib_1.__decorate([
    InputBoolean(),
    tslib_1.__metadata("design:type", Boolean),
    tslib_1.__metadata("design:paramtypes", [Boolean])
], GridComponent.prototype, "bordered", null);
tslib_1.__decorate([
    InputBoolean(),
    tslib_1.__metadata("design:type", Boolean),
    tslib_1.__metadata("design:paramtypes", [Boolean])
], GridComponent.prototype, "expandRowByClick", null);
tslib_1.__decorate([
    InputBoolean(),
    tslib_1.__metadata("design:type", Boolean),
    tslib_1.__metadata("design:paramtypes", [Boolean])
], GridComponent.prototype, "expandAccordion", null);
tslib_1.__decorate([
    InputBoolean(),
    tslib_1.__metadata("design:type", TemplateRef),
    tslib_1.__metadata("design:paramtypes", [TemplateRef])
], GridComponent.prototype, "expand", null);
if (false) {
    /** @type {?} */
    GridComponent.prototype.grhxurl;
    /** @type {?} */
    GridComponent.prototype.isGrhx;
    /**
     * 每页数量，当设置为 0 表示不分页，默认：10
     * @type {?}
     */
    GridComponent.prototype.ps;
    /** @type {?} */
    GridComponent.prototype.datas;
    /** @type {?} */
    GridComponent.prototype.statisticals;
    /** @type {?} */
    GridComponent.prototype._statcal;
    /** @type {?} */
    GridComponent.prototype.loading;
    /** @type {?} */
    GridComponent.prototype.loadingChange;
    /**
     * 横向或纵向支持滚动，也可用于指定滚动区域的宽高度：{ x: "300px", y: "300px" }
     * @type {?}
     */
    GridComponent.prototype.scroll;
    /** @type {?} */
    GridComponent.prototype._columns;
    /** @type {?} */
    GridComponent.prototype._sname;
    /** @type {?} */
    GridComponent.prototype._queryparas;
    /** @type {?} */
    GridComponent.prototype.st;
    /** @type {?} */
    GridComponent.prototype.reqparas;
    /** @type {?} */
    GridComponent.prototype.dataProcess;
    /**
     * @type {?}
     * @private
     */
    GridComponent.prototype.process;
    /**
     * @type {?}
     * @private
     */
    GridComponent.prototype.supdic;
    /**
     * @type {?}
     * @private
     */
    GridComponent.prototype.supexcel;
    /**
     * @type {?}
     * @private
     */
    GridComponent.prototype.sinfo;
    /**
     * @type {?}
     * @private
     */
    GridComponent.prototype.lbSource;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZ3JpZC5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9sYmYvIiwic291cmNlcyI6WyJsaWIvY29tcG9uZW50cy9ncmlkL2dyaWQuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7OztBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQVUsS0FBSyxFQUFFLE1BQU0sRUFBRSxZQUFZLEVBQUUsU0FBUyxFQUFFLFdBQVcsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUd2RyxPQUFPLEVBQ0wsV0FBVyxHQVNaLE1BQU0sWUFBWSxDQUFDO0FBQ3BCLE9BQU8sRUFBRSxNQUFNLEVBQUUsTUFBTSxxQkFBcUIsQ0FBQztBQUM3QyxPQUFPLEVBQUUsUUFBUSxFQUFFLE1BQU0sdUJBQXVCLENBQUM7QUFDakQsT0FBTyxFQUFFLFdBQVcsRUFBRSxNQUFNLFlBQVksQ0FBQztBQUN6QyxPQUFPLEVBQUUsV0FBVyxFQUFFLFlBQVksRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUMxRCxPQUFPLEVBQUUsV0FBVyxFQUFFLE1BQU0scUJBQXFCLENBQUM7QUFRbEQsTUFBTSxPQUFPLGFBQWE7Ozs7Ozs7SUFheEIsWUFBb0IsTUFBYyxFQUFVLFFBQWtCLEVBQVUsS0FBa0IsRUFBVSxRQUFxQjtRQUFyRyxXQUFNLEdBQU4sTUFBTSxDQUFRO1FBQVUsYUFBUSxHQUFSLFFBQVEsQ0FBVTtRQUFVLFVBQUssR0FBTCxLQUFLLENBQWE7UUFBVSxhQUFRLEdBQVIsUUFBUSxDQUFhO1FBSWhILFlBQU8sR0FBRyxDQUFDLFlBQVksQ0FBQyxDQUFBO1FBQ1IsV0FBTSxHQUFHLElBQUksQ0FBQzs7OztRQStGOUIsT0FBRSxHQUFHLEVBQUUsQ0FBQztRQUVqQixVQUFLLEdBQXNCLDJCQUEyQixDQUFDO1FBRXZELGlCQUFZLEdBQUcsRUFBRSxDQUFDO1FBQ2xCLGFBQVEsR0FBRyxFQUFFLENBQUM7O1FBR0wsWUFBTyxHQUFHLEtBQUssQ0FBQztRQUNmLGtCQUFhLEdBQUcsSUFBSSxZQUFZLEVBQUUsQ0FBQzs7UUFnSDdDLFdBQU0sR0FBRyxFQUFFLENBQUM7UUFJWixhQUFRLEdBQUcsRUFBRSxNQUFNLEVBQUUsRUFBRSxLQUFLLEVBQUUsSUFBSSxDQUFDLE1BQU0sRUFBRSxJQUFJLEVBQUUsSUFBSSxDQUFDLFdBQVcsRUFBRSxNQUFNLEVBQUUsSUFBSSxDQUFDLFFBQVEsRUFBRSxFQUFFLE1BQU0sRUFBRSxNQUFNLEVBQUUsU0FBUyxFQUFFLElBQUksRUFBRSxDQUFDO1FBMER0SCxZQUFPOzs7O1FBQUcsQ0FBQyxJQUFjLEVBQUUsRUFBRTtZQUNuQyxPQUFPLElBQUksQ0FBQztRQUNkLENBQUMsRUFBQztJQTNSRixDQUFDOzs7OztJQVZELElBQUksSUFBSTtRQUNOLE9BQU8sSUFBSSxDQUFDLEVBQUUsQ0FBQyxLQUFLLENBQUM7SUFDdkIsQ0FBQzs7OztJQUVELElBQUksT0FBTztRQUNULE9BQU8sSUFBSSxDQUFDLEVBQUUsQ0FBQztJQUNqQixDQUFDOzs7OztJQVNELElBQ0ksT0FBTyxDQUFDLE9BQW1CO1FBRTdCLFFBQVE7UUFDUixJQUFJLENBQUMsWUFBWSxHQUFHLEVBQUUsQ0FBQztRQUV2QixPQUFPLENBQUMsT0FBTzs7OztRQUFDLEdBQUcsQ0FBQyxFQUFFO1lBRXBCLElBQUksR0FBRyxDQUFDLE1BQU0sRUFBRTtnQkFDZCxHQUFHLENBQUMsT0FBTyxHQUFHLEdBQUcsQ0FBQyxNQUFNLENBQUM7Z0JBQ3pCLEdBQUcsQ0FBQyxNQUFNLEdBQUcsRUFBRSxDQUFDO2FBQ2pCO2lCQUFNO2dCQUNMLElBQUksSUFBSSxDQUFDLE1BQU0sSUFBSSxHQUFHLENBQUMsS0FBSyxLQUFLLFFBQVEsSUFBSSxJQUFJLENBQUMsS0FBSyxDQUFDLE9BQU8sRUFBRTtvQkFDL0QsR0FBRyxDQUFDLE1BQU0sR0FBRyxNQUFNLENBQUM7aUJBQ3JCO2FBQ0Y7WUFFRCxJQUFJLEdBQUcsQ0FBQyxXQUFXLEVBQUU7Z0JBQ25CLEdBQUcsQ0FBQyxZQUFZLEdBQUcsR0FBRyxDQUFDLFdBQVcsQ0FBQztnQkFDbkMsR0FBRyxDQUFDLFdBQVcsR0FBRyxFQUFFLENBQUM7YUFDdEI7O2dCQUVHLEdBQUcsR0FBRyxHQUFHLENBQUMsS0FBSztZQUNuQixJQUFJLEdBQUcsWUFBWSxLQUFLLEVBQUU7Z0JBQ3hCLEdBQUcsR0FBRyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUM7YUFDZDtZQUVELEdBQUcsQ0FBQyxHQUFHLEdBQUcsR0FBRyxDQUFDO1lBRWQsU0FBUztZQUNULElBQUksR0FBRyxDQUFDLEdBQUcsRUFBRTtnQkFDWCxHQUFHLENBQUMsTUFBTTs7Ozs7Z0JBQUcsQ0FBQyxDQUFTLEVBQUUsQ0FBVyxFQUFFLEVBQUUsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUMsR0FBRyxFQUFFLENBQUMsQ0FBQyxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQSxDQUFDO2dCQUNsRixHQUFHLENBQUMsTUFBTSxHQUFHLEVBQUUsS0FBSyxFQUFFLElBQUksQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFBO2FBQ3BEOzs7a0JBSUssR0FBRyxHQUFHLEdBQUcsQ0FBQyxXQUFXO1lBQzNCLElBQUksQ0FBQyxHQUFHLEVBQUU7Z0JBRVIsSUFBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsRUFBRSxHQUFHLEVBQUUsSUFBSSxFQUFFLEtBQUssRUFBRSxJQUFJLEVBQUUsRUFBRSxFQUFFLENBQUMsQ0FBQztnQkFDdkQsSUFBSSxDQUFDLFFBQVEsQ0FBQyxHQUFHLENBQUMsR0FBRyxFQUFFLENBQUM7Z0JBQ3hCLE9BQU87YUFDUjs7a0JBQ0ssSUFBSSxtQkFDUixNQUFNLEVBQUUsQ0FBQyxFQUNULFFBQVEsRUFBRSxJQUFJLElBQ1gsQ0FBQyxPQUFPLEdBQUcsS0FBSyxRQUFRO2dCQUN6QixDQUFDLENBQUMsRUFBRSxJQUFJLEVBQUUsbUJBQUEsR0FBRyxFQUFxQixFQUFFO2dCQUNwQyxDQUFDLENBQUMsQ0FBQyxtQkFBQSxHQUFHLEVBQWlCLENBQUMsQ0FBQyxDQUM1Qjs7a0JBRUssS0FBSyxHQUFHLElBQUksQ0FBQyxJQUFJO1lBRXZCLElBQUksQ0FBQyxNQUFNLEdBQUcsS0FBSyxLQUFLLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFFeEMsSUFBSSxDQUFDLElBQUksR0FBRyxJQUFJLENBQUMsaUJBQWlCLENBQUM7WUFDbkMsSUFBSSxDQUFDLFFBQVEsR0FBRyxLQUFLLENBQUM7WUFDdEIsR0FBRyxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUM7WUFDdkIsSUFBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsRUFBRSxHQUFHLEVBQUUsSUFBSSxFQUFFLElBQUksRUFBRSxJQUFJLEVBQUUsS0FBSyxFQUFFLENBQUMsQ0FBQztZQUN6RCxJQUFJLENBQUMsUUFBUSxDQUFDLEdBQUcsQ0FBQyxHQUFHLEtBQUssQ0FBQztRQUM3QixDQUFDLEVBQUMsQ0FBQztRQUVILElBQUksQ0FBQyxRQUFRLEdBQUcsT0FBTyxDQUFDO0lBQzFCLENBQUM7Ozs7SUFFRCxJQUFJLE9BQU87UUFDVCxPQUFPLElBQUksQ0FBQyxRQUFRLENBQUM7SUFDdkIsQ0FBQzs7Ozs7SUFJRCxJQUFvQixLQUFLLENBQUMsS0FBYTtRQUNyQyxJQUFJLENBQUMsTUFBTSxHQUFHLEtBQUssQ0FBQztRQUNwQixJQUFJLENBQUMsUUFBUSxDQUFDLE1BQU0sQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQztJQUMzQyxDQUFDOzs7OztJQUlELElBQ0ksVUFBVTtRQUNaLE9BQU8sSUFBSSxDQUFDLFdBQVcsQ0FBQztJQUMxQixDQUFDOzs7OztJQUVELElBQUksVUFBVSxDQUFDLEtBQVU7UUFFdkIsSUFBSSxDQUFDLFdBQVcsR0FBRyxLQUFLLENBQUM7UUFDekIsSUFBSSxDQUFDLFFBQVEsQ0FBQyxNQUFNLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUM7SUFDL0MsQ0FBQzs7Ozs7O0lBbUJELElBQWEsZ0JBQWdCLENBQUMsZ0JBQW1DO1FBQy9ELElBQUksQ0FBQyxFQUFFLENBQUMsZ0JBQWdCLEdBQUcsZ0JBQWdCLENBQUM7SUFDOUMsQ0FBQzs7Ozs7O0lBSXVCLElBQUksWUFBWSxDQUFDLFlBQW9CO1FBQzNELElBQUksQ0FBQyxFQUFFLENBQUMsWUFBWSxHQUFHLFlBQVksQ0FBQztJQUN0QyxDQUFDOzs7Ozs7SUFhRCxJQUFhLEVBQUUsQ0FBQyxFQUFVO1FBQ3hCLElBQUksQ0FBQyxFQUFFLENBQUMsRUFBRSxHQUFHLEVBQUUsQ0FBQztJQUNsQixDQUFDOzs7O0lBQ0QsSUFBSSxFQUFFO1FBQ0osT0FBTyxJQUFJLENBQUMsRUFBRSxDQUFDLEVBQUUsQ0FBQztJQUNwQixDQUFDOzs7Ozs7SUFLRCxJQUFJLElBQUksQ0FBQyxJQUFZO1FBRW5CLElBQUksQ0FBQyxFQUFFLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQztJQUN0QixDQUFDOzs7O0lBRUQsSUFBSSxJQUFJO1FBQ04sT0FBTyxJQUFJLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQztJQUN0QixDQUFDOzs7Ozs7SUFLRCxJQUFhLFFBQVEsQ0FBQyxRQUFvQztRQUN4RCxJQUFJLENBQUMsRUFBRSxDQUFDLFFBQVEsR0FBRyxRQUFRLENBQUM7SUFDOUIsQ0FBQzs7OztJQUVELElBQUksUUFBUTtRQUNWLE9BQU8sSUFBSSxDQUFDLEVBQUUsQ0FBQyxRQUFRLENBQUM7SUFDMUIsQ0FBQzs7Ozs7O0lBS3dCLElBQUksUUFBUSxDQUFDLFFBQWlCO1FBQ3JELElBQUksQ0FBQyxFQUFFLENBQUMsUUFBUSxHQUFHLFFBQVEsQ0FBQztJQUM5QixDQUFDOzs7O0lBQ0QsSUFBSSxRQUFRO1FBQ1YsT0FBTyxJQUFJLENBQUMsRUFBRSxDQUFDLFFBQVEsQ0FBQztJQUMxQixDQUFDOzs7Ozs7SUFLRCxJQUFhLE1BQU0sQ0FBQyxNQUFrQztRQUNwRCxJQUFJLENBQUMsRUFBRSxDQUFDLE1BQU0sR0FBRyxNQUFNLENBQUE7SUFDekIsQ0FBQzs7Ozs7O0lBS0QsSUFBYSxNQUFNLENBQUMsTUFBa0M7UUFDcEQsSUFBSSxDQUFDLEVBQUUsQ0FBQyxNQUFNLEdBQUcsTUFBTSxDQUFBO0lBQ3pCLENBQUM7Ozs7OztJQUtELElBQWEsVUFBVSxDQUFDLFVBQTZDO1FBQ25FLElBQUksQ0FBQyxFQUFFLENBQUMsVUFBVSxHQUFHLFVBQVUsQ0FBQztJQUVsQyxDQUFDOzs7OztJQUd3QixJQUFJLGdCQUFnQixDQUFDLGdCQUF5QjtRQUNyRSxJQUFJLENBQUMsRUFBRSxDQUFDLGdCQUFnQixHQUFHLGdCQUFnQixDQUFDO0lBQzlDLENBQUM7Ozs7O0lBRXdCLElBQUksZUFBZSxDQUFDLGVBQXdCO1FBQ25FLElBQUksQ0FBQyxFQUFFLENBQUMsZUFBZSxHQUFHLGVBQWUsQ0FBQztJQUM1QyxDQUFDOzs7OztJQUN3QixJQUFJLE1BQU0sQ0FBQyxNQUdsQztRQUNBLElBQUksQ0FBQyxFQUFFLENBQUMsTUFBTSxHQUFHLE1BQU0sQ0FBQztJQUMxQixDQUFDOzs7O0lBRUQsSUFBSSxZQUFZO1FBQ2QsT0FBTyxJQUFJLENBQUMsRUFBRSxDQUFDLFlBQVksQ0FBQztJQUM5QixDQUFDOzs7O0lBZUQsUUFBUTtRQUNOLFVBQVU7OztRQUFDLEdBQUcsRUFBRTtZQUVkLElBQUksQ0FBQyxPQUFPLENBQUMsT0FBTzs7OztZQUFDLEdBQUcsQ0FBQyxFQUFFO2dCQUN6QixJQUFJLENBQUMsYUFBYSxDQUFDLEdBQUcsQ0FBQyxDQUFDO1lBQzFCLENBQUMsRUFBQyxDQUFBO1lBRUYsSUFBSSxDQUFDLEVBQUUsQ0FBQyxZQUFZLEVBQUUsQ0FBQztRQUV6QixDQUFDLEdBQUUsR0FBRyxDQUFDLENBQUM7SUFFVixDQUFDOzs7OztJQUVELFlBQVksQ0FBQyxPQUE4QjtRQUN6QyxJQUFJLENBQUMsRUFBRSxDQUFDLFlBQVksQ0FBQyxPQUFPLENBQUMsQ0FBQztJQUNoQyxDQUFDOzs7Ozs7SUFLRCxTQUFTLENBQUMsSUFBSTtRQUNaLElBQUksQ0FBQyxFQUFFLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxDQUFDO0lBQzFCLENBQUM7Ozs7O0lBS0QsS0FBSztRQUNILElBQUksQ0FBQyxFQUFFLENBQUMsS0FBSyxFQUFFLENBQUM7SUFDbEIsQ0FBQzs7Ozs7SUFJRCxNQUFNLENBQUMsVUFBZ0I7UUFFckIsSUFBSSxDQUFDLEVBQUUsQ0FBQyxPQUFPLEdBQUcsSUFBSSxDQUFDO1FBQ3ZCLElBQUksVUFBVTtZQUNaLElBQUksQ0FBQyxFQUFFLENBQUMsR0FBRyxDQUFDLE1BQU0sQ0FBQyxJQUFJLEdBQUcsVUFBVSxDQUFDO1FBQ3ZDLElBQUksQ0FBQyxFQUFFLENBQUMsTUFBTSxFQUFFLENBQUM7UUFDakIsSUFBSSxDQUFDLEVBQUUsQ0FBQyxFQUFFLEVBQUUsQ0FBQztJQUVmLENBQUM7Ozs7OztJQUVELE1BQU0sQ0FBQyxLQUFhLEVBQUUsSUFBWTtRQUNoQyxJQUFJLENBQUMsRUFBRSxDQUFDLE1BQU0sQ0FBQyxLQUFLLEVBQUUsSUFBSSxDQUFDLENBQUM7SUFDOUIsQ0FBQzs7Ozs7SUFFRCxLQUFLLENBQUMsS0FBVTtRQUNkLElBQUksQ0FBQyxPQUFPLEdBQUcsSUFBSSxDQUFDO1FBQ3BCLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQzlCLElBQUksQ0FBQyxFQUFFLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQ3JCLElBQUksQ0FBQyxFQUFFLENBQUMsRUFBRSxFQUFFLENBQUM7SUFDZixDQUFDOzs7Ozs7Ozs7SUFNTyxpQkFBaUIsQ0FBQyxNQUFnQixFQUFFLEdBQWEsRUFBRSxJQUFjLEVBQUUsT0FBYTtRQUV0RixJQUFJLE9BQU8sQ0FBQyxJQUFJLEdBQUcsQ0FBQztZQUFFLE9BQU8sRUFBRSxLQUFLLEVBQUUsRUFBRSxFQUFFLElBQUksRUFBRSxFQUFFLEVBQUUsQ0FBQztRQUVyRCxJQUFJLE9BQU8sQ0FBQyxJQUFJLENBQUMsV0FBVyxFQUFFOztnQkFFeEIsR0FBRyxHQUFHLEdBQUcsQ0FBQyxLQUFLO1lBQ25CLElBQUksR0FBRyxZQUFZLEtBQUssRUFBRTtnQkFDeEIsR0FBRyxHQUFHLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQzthQUNkO1lBR0QsSUFBSSxPQUFPLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxHQUFHLENBQUMsRUFBRTtnQkFFakMsT0FBTyxFQUFFLEtBQUssRUFBRSxPQUFPLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxHQUFHLENBQUMsRUFBRSxJQUFJLEVBQUUsRUFBRSxFQUFFLENBQUM7YUFDM0Q7WUFFRCxPQUFPLEVBQUUsS0FBSyxFQUFFLEVBQUUsRUFBRSxJQUFJLEVBQUUsRUFBRSxFQUFFLENBQUM7U0FDaEM7UUFFRCxPQUFPLEVBQUUsS0FBSyxFQUFFLEVBQUUsRUFBRSxJQUFJLEVBQUUsRUFBRSxFQUFFLENBQUM7SUFFakMsQ0FBQzs7Ozs7O0lBR0QsTUFBTSxDQUFDLEdBQXFCOztjQUVwQixJQUFJLEdBQUcsSUFBSSxDQUFDLEVBQUUsQ0FBQyxLQUFLO1FBQzFCLElBQUksQ0FBQyxFQUFFLENBQUMsTUFBTSxDQUFDLElBQUksRUFBRSxHQUFHLENBQUMsQ0FBQztJQUM1QixDQUFDOzs7O0lBRUQsU0FBUztRQUVQLElBQUksQ0FBQyxPQUFPLEdBQUcsSUFBSSxDQUFDO1FBRXBCLElBQUksQ0FBQyxRQUFRLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxPQUFPLEVBQUUsSUFBSSxDQUFDLE1BQU0sRUFBRSxJQUFJLENBQUMsRUFBRSxDQUFDLEdBQUcsQ0FBQyxNQUFNLENBQUMsSUFBSSxFQUFFLElBQUksQ0FBQyxFQUFFLENBQUMsS0FBSyxDQUFDLENBQUMsSUFBSTs7O1FBQUMsR0FBRyxFQUFFO1lBQ2hHLElBQUksQ0FBQyxPQUFPLEdBQUcsS0FBSyxDQUFDO1FBQ3ZCLENBQUMsRUFBQyxDQUFDO0lBRUwsQ0FBQzs7Ozs7O0lBRU8sYUFBYSxDQUFDLElBQWM7UUFDbEMsSUFBSSxJQUFJLENBQUMsWUFBWSxFQUFFO1lBQ3JCLElBQUksQ0FBQyxhQUFhLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxDQUFDO1NBRWhFO1FBQ0QsSUFBSSxJQUFJLENBQUMsT0FBTyxFQUFFO1lBQ2hCLElBQUksQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDO1NBRXBEO0lBRUgsQ0FBQzs7O1lBcldGLFNBQVMsU0FBQztnQkFDVCxRQUFRLEVBQUUsU0FBUztnQkFDbkIsK3lCQUFvQztnQkFFcEMsU0FBUyxFQUFFLENBQUMsV0FBVyxDQUFDOzthQUN6Qjs7OztZQVhRLE1BQU07WUFDTixRQUFRO1lBR1IsV0FBVztZQUZYLFdBQVc7OztzQkEyQmpCLEtBQUs7cUJBQ0wsS0FBSztzQkFFTCxLQUFLLFNBQUMsU0FBUztvQkF3RWYsS0FBSyxTQUFDLE9BQU87eUJBT2IsS0FBSztpQkFjTCxLQUFLO3NCQVFMLEtBQUs7NEJBQ0wsTUFBTTsrQkFLTixLQUFLOzJCQU1MLEtBQUs7cUJBT0wsS0FBSztpQkFRTCxLQUFLO3VCQXNCTCxLQUFLO3VCQVdMLEtBQUs7cUJBVUwsS0FBSztxQkFPTCxLQUFLO3lCQU9MLEtBQUs7K0JBTUwsS0FBSzs4QkFJTCxLQUFLO3FCQUdMLEtBQUs7aUJBa0JMLFNBQVMsU0FBQyxJQUFJLEVBQUUsRUFBRSxNQUFNLEVBQUUsSUFBSSxFQUFFOztBQTFOUjtJQUFmLFlBQVksRUFBRTs7NkNBQWU7QUFtSGY7SUFBZCxXQUFXLEVBQUU7OztpREFFdEI7QUE4Q3dCO0lBQWYsWUFBWSxFQUFFOzs7NkNBRXZCO0FBNEJ3QjtJQUFmLFlBQVksRUFBRTs7O3FEQUV2QjtBQUV3QjtJQUFmLFlBQVksRUFBRTs7O29EQUV2QjtBQUN3QjtJQUFmLFlBQVksRUFBRTtzQ0FBb0IsV0FBVzs2Q0FBWCxXQUFXOzJDQUt0RDs7O0lBOU1ELGdDQUFpQzs7SUFDakMsK0JBQXVDOzs7OztJQStGdkMsMkJBQWlCOztJQUVqQiw4QkFBdUQ7O0lBRXZELHFDQUFrQjs7SUFDbEIsaUNBQWM7O0lBR2QsZ0NBQXlCOztJQUN6QixzQ0FBNkM7Ozs7O0lBa0I3QywrQkFHRTs7SUF3RkYsaUNBQXFCOztJQUdyQiwrQkFBWTs7SUFDWixvQ0FBaUI7O0lBQ2pCLDJCQUFtRDs7SUFFbkQsaUNBQThIOztJQUU5SCxvQ0FBMEQ7Ozs7O0lBd0QxRCxnQ0FFRTs7Ozs7SUE3UlUsK0JBQXNCOzs7OztJQUFFLGlDQUEwQjs7Ozs7SUFBRSw4QkFBMEI7Ozs7O0lBQUUsaUNBQTZCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBPbkluaXQsIElucHV0LCBPdXRwdXQsIEV2ZW50RW1pdHRlciwgVmlld0NoaWxkLCBUZW1wbGF0ZVJlZiB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuXG5cbmltcG9ydCB7XG4gIFNUQ29tcG9uZW50LFxuICBTVENvbHVtbixcbiAgU1REYXRhLFxuICBTVEV4cG9ydE9wdGlvbnMsXG4gIFNUU3RhdGlzdGljYWwsXG4gIFNUU3RhdGlzdGljYWxUeXBlLFxuICBTVFBhZ2UsXG4gIFNUU3RhdGlzdGljYWxSZXN1bHRzLFxuICBTVFJlc2V0Q29sdW1uc09wdGlvbixcbn0gZnJvbSAnQGRlbG9uL2FiYyc7XG5pbXBvcnQgeyBTdXBEaWMgfSBmcm9tICcuLi8uLi9sYmxpYnMvU3VwRGljJztcbmltcG9ydCB7IFN1cEV4Y2VsIH0gZnJvbSAnLi4vLi4vbGJsaWJzL1N1cEV4Y2VsJztcbmltcG9ydCB7IExiUm93U291cmNlIH0gZnJvbSAnLi9MYkNvbHVtbic7XG5pbXBvcnQgeyBJbnB1dE51bWJlciwgSW5wdXRCb29sZWFuIH0gZnJvbSAnbmctem9ycm8tYW50ZCc7XG5pbXBvcnQgeyBTZXJ2aWNlSW5mbyB9IGZyb20gJy4uLy4uL1NlcnZpY2VDb25maWcnO1xuXG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6ICdsYi1ncmlkJyxcbiAgdGVtcGxhdGVVcmw6ICcuL2dyaWQuY29tcG9uZW50Lmh0bWwnLFxuICBzdHlsZVVybHM6IFsnLi9ncmlkLmNvbXBvbmVudC5jc3MnXSxcbiAgcHJvdmlkZXJzOiBbTGJSb3dTb3VyY2VdXG59KVxuZXhwb3J0IGNsYXNzIEdyaWRDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xuXG4gIC8qKlxuICAgKiDooajmoLzmlbDlgLxcbiAgICovXG4gIGdldCBkYXRhKCk6IFNURGF0YVtdIHtcbiAgICByZXR1cm4gdGhpcy5zdC5fZGF0YTtcbiAgfVxuXG4gIGdldCBpbm5lclN0KCk6IFNUQ29tcG9uZW50IHtcbiAgICByZXR1cm4gdGhpcy5zdDtcbiAgfVxuXG4gIGNvbnN0cnVjdG9yKHByaXZhdGUgc3VwZGljOiBTdXBEaWMsIHByaXZhdGUgc3VwZXhjZWw6IFN1cEV4Y2VsLCBwcml2YXRlIHNpbmZvOiBTZXJ2aWNlSW5mbywgcHJpdmF0ZSBsYlNvdXJjZTogTGJSb3dTb3VyY2UpIHtcblxuICB9XG5cbiAgQElucHV0KCkgZ3JoeHVybCA9IFsnL3liamcvZ3JoeCddXG4gIEBJbnB1dCgpIEBJbnB1dEJvb2xlYW4oKSBpc0dyaHggPSB0cnVlO1xuXG4gIEBJbnB1dCgnY29sdW1ucycpXG4gIHNldCBjb2x1bW5zKGNvbHVtbnM6IFNUQ29sdW1uW10pIHtcblxuICAgIC8vIOe7n+iuoeexu+S/oeaBr1xuICAgIHRoaXMuc3RhdGlzdGljYWxzID0gW107XG5cbiAgICBjb2x1bW5zLmZvckVhY2goY29sID0+IHtcblxuICAgICAgaWYgKGNvbC5yZW5kZXIpIHtcbiAgICAgICAgY29sLnJlbmRlcjIgPSBjb2wucmVuZGVyO1xuICAgICAgICBjb2wucmVuZGVyID0gJyc7XG4gICAgICB9IGVsc2Uge1xuICAgICAgICBpZiAodGhpcy5pc0dyaHggJiYgY29sLmluZGV4ID09PSAnQUFDMDAzJyAmJiB0aGlzLnNpbmZvLmlzQWRtaW4pIHtcbiAgICAgICAgICBjb2wucmVuZGVyID0gJ2dyaHgnO1xuICAgICAgICB9XG4gICAgICB9XG5cbiAgICAgIGlmIChjb2wucmVuZGVyVGl0bGUpIHtcbiAgICAgICAgY29sLnJlbmRlclRpdGxlMiA9IGNvbC5yZW5kZXJUaXRsZTtcbiAgICAgICAgY29sLnJlbmRlclRpdGxlID0gJyc7XG4gICAgICB9XG5cbiAgICAgIGxldCBrZXkgPSBjb2wuaW5kZXg7XG4gICAgICBpZiAoa2V5IGluc3RhbmNlb2YgQXJyYXkpIHtcbiAgICAgICAga2V5ID0ga2V5WzBdO1xuICAgICAgfVxuXG4gICAgICBjb2wua2V5ID0ga2V5O1xuXG4gICAgICAvLyDmt7vliqDlrZflhbjkv6Hmga9cbiAgICAgIGlmIChjb2wuZGljKSB7XG4gICAgICAgIGNvbC5mb3JtYXQgPSAoYTogU1REYXRhLCBiOiBTVENvbHVtbikgPT4gdGhpcy5zdXBkaWMuZ2V0ZGljTGFiZWwoYi5kaWMsIGFbYi5rZXldKTtcbiAgICAgICAgY29sLmZpbHRlciA9IHsgbWVudXM6IHRoaXMuc3VwZGljLmdldERpYyhjb2wuZGljKSB9XG4gICAgICB9XG5cblxuICAgICAgLy8g5re75Yqg57uf6K6h57G75Z6L5L+h5oGvXG4gICAgICBjb25zdCB2YWwgPSBjb2wuc3RhdGlzdGljYWw7XG4gICAgICBpZiAoIXZhbCkge1xuXG4gICAgICAgIHRoaXMuc3RhdGlzdGljYWxzLnB1c2goeyBrZXksIHNob3c6IGZhbHNlLCB0eXBlOiBcIlwiIH0pO1xuICAgICAgICB0aGlzLl9zdGF0Y2FsW2tleV0gPSBcIlwiO1xuICAgICAgICByZXR1cm47XG4gICAgICB9XG4gICAgICBjb25zdCBpdGVtOiBTVFN0YXRpc3RpY2FsID0ge1xuICAgICAgICBkaWdpdHM6IDIsXG4gICAgICAgIGN1cnJlbmN5OiBudWxsLFxuICAgICAgICAuLi4odHlwZW9mIHZhbCA9PT0gJ3N0cmluZydcbiAgICAgICAgICA/IHsgdHlwZTogdmFsIGFzIFNUU3RhdGlzdGljYWxUeXBlIH1cbiAgICAgICAgICA6ICh2YWwgYXMgU1RTdGF0aXN0aWNhbCkpLFxuICAgICAgfTtcblxuICAgICAgY29uc3Qgc3RjYWwgPSBpdGVtLnR5cGU7XG5cbiAgICAgIGl0ZW0uZGlnaXRzID0gc3RjYWwgPT09IFwiY291bnRcIiA/IDAgOiAyO1xuXG4gICAgICBpdGVtLnR5cGUgPSB0aGlzLmN1c3RvbVN0YXRpc3RpY2FsO1xuICAgICAgaXRlbS5jdXJyZW5jeSA9IGZhbHNlO1xuICAgICAgY29sLnN0YXRpc3RpY2FsID0gaXRlbTtcbiAgICAgIHRoaXMuc3RhdGlzdGljYWxzLnB1c2goeyBrZXksIHNob3c6IHRydWUsIHR5cGU6IHN0Y2FsIH0pO1xuICAgICAgdGhpcy5fc3RhdGNhbFtrZXldID0gc3RjYWw7XG4gICAgfSk7XG5cbiAgICB0aGlzLl9jb2x1bW5zID0gY29sdW1ucztcbiAgfVxuXG4gIGdldCBjb2x1bW5zKCk6IFNUQ29sdW1uW10ge1xuICAgIHJldHVybiB0aGlzLl9jb2x1bW5zO1xuICB9XG5cblxuXG4gIEBJbnB1dCgnc25hbWUnKSBzZXQgc25hbWUoc25hbWU6IHN0cmluZykge1xuICAgIHRoaXMuX3NuYW1lID0gc25hbWU7XG4gICAgdGhpcy5yZXFwYXJhcy5wYXJhbXMuc25hbWUgPSB0aGlzLl9zbmFtZTtcbiAgfVxuXG4gIC8vIOafpeivouWPguaVsFxuXG4gIEBJbnB1dCgpXG4gIGdldCBxdWVyeXBhcmFzKCkge1xuICAgIHJldHVybiB0aGlzLl9xdWVyeXBhcmFzO1xuICB9XG5cbiAgc2V0IHF1ZXJ5cGFyYXModmFsdWU6IGFueSkge1xuXG4gICAgdGhpcy5fcXVlcnlwYXJhcyA9IHZhbHVlO1xuICAgIHRoaXMucmVxcGFyYXMucGFyYW1zLmZvcm0gPSB0aGlzLl9xdWVyeXBhcmFzO1xuICB9XG5cbiAgLyoqXG4gICAqIOavj+mhteaVsOmHj++8jOW9k+iuvue9ruS4uiAwIOihqOekuuS4jeWIhumhte+8jOm7mOiupO+8mjEwXG4gICAqL1xuICBASW5wdXQoKSBwcyA9IDEwO1xuXG4gIGRhdGFzOiBzdHJpbmcgfCBTVERhdGFbXSA9ICdNb2hyc3NFeEludGVyZmFjZS9ncmlkc3ZyJztcblxuICBzdGF0aXN0aWNhbHMgPSBbXTtcbiAgX3N0YXRjYWwgPSB7fTtcblxuICAvLyDmmK/lkKbliqDovb3kuK1cbiAgQElucHV0KCkgbG9hZGluZyA9IGZhbHNlO1xuICBAT3V0cHV0KCkgbG9hZGluZ0NoYW5nZSA9IG5ldyBFdmVudEVtaXR0ZXIoKTtcblxuICAvKipcbiAgICog5Yqg6L295oyH56S656ymXG4gICAqL1xuICBASW5wdXQoKSBzZXQgbG9hZGluZ0luZGljYXRvcihsb2FkaW5nSW5kaWNhdG9yOiBUZW1wbGF0ZVJlZjx2b2lkPikge1xuICAgIHRoaXMuc3QubG9hZGluZ0luZGljYXRvciA9IGxvYWRpbmdJbmRpY2F0b3I7XG4gIH1cbiAgLyoqXG4gICAqIOW7tui/n+aYvuekuuWKoOi9veaViOaenOeahOaXtumXtO+8iOmYsuatoumXqueDge+8iVxuICAgKi9cbiAgQElucHV0KCkgQElucHV0TnVtYmVyKCkgc2V0IGxvYWRpbmdEZWxheShsb2FkaW5nRGVsYXk6IG51bWJlcikge1xuICAgIHRoaXMuc3QubG9hZGluZ0RlbGF5ID0gbG9hZGluZ0RlbGF5O1xuICB9XG5cbiAgLyoqXG4gICAqIOaoquWQkeaIlue6teWQkeaUr+aMgea7muWKqO+8jOS5n+WPr+eUqOS6juaMh+Wumua7muWKqOWMuuWfn+eahOWuvemrmOW6pu+8mnsgeDogXCIzMDBweFwiLCB5OiBcIjMwMHB4XCIgfVxuICAgKi9cbiAgQElucHV0KCkgc2Nyb2xsOiB7XG4gICAgeT86IHN0cmluZztcbiAgICB4Pzogc3RyaW5nO1xuICB9O1xuXG4gIC8qKlxuICAgKiDlvZPliY3pobXnoIFcbiAgICovXG4gIEBJbnB1dCgpIHNldCBwaShwaTogbnVtYmVyKSB7XG4gICAgdGhpcy5zdC5waSA9IHBpO1xuICB9XG4gIGdldCBwaSgpOiBudW1iZXIge1xuICAgIHJldHVybiB0aGlzLnN0LnBpO1xuICB9XG5cbiAgLyoqXG4gICAqICDliIbpobXlmajphY3nva5cbiAgICovXG4gIHNldCBwYWdlKHBhZ2U6IFNUUGFnZSkge1xuXG4gICAgdGhpcy5zdC5wYWdlID0gcGFnZTtcbiAgfVxuXG4gIGdldCBwYWdlKCk6IFNUUGFnZSB7XG4gICAgcmV0dXJuIHRoaXMuc3QucGFnZTtcbiAgfVxuXG4gIC8qKlxuICAgKiDml6DmlbDmja7ml7bmmL7npLrlhoXlrrlcbiAgICovXG4gIEBJbnB1dCgpIHNldCBub1Jlc3VsdChub1Jlc3VsdDogc3RyaW5nIHwgVGVtcGxhdGVSZWY8dm9pZD4pIHtcbiAgICB0aGlzLnN0Lm5vUmVzdWx0ID0gbm9SZXN1bHQ7XG4gIH1cblxuICBnZXQgbm9SZXN1bHQoKTogc3RyaW5nIHwgVGVtcGxhdGVSZWY8dm9pZD4ge1xuICAgIHJldHVybiB0aGlzLnN0Lm5vUmVzdWx0O1xuICB9XG5cbiAgLyoqXG4gICAqIOaYr+WQpuaYvuekuui+ueahhlxuICAgKi9cbiAgQElucHV0KCkgQElucHV0Qm9vbGVhbigpIHNldCBib3JkZXJlZChib3JkZXJlZDogYm9vbGVhbikge1xuICAgIHRoaXMuc3QuYm9yZGVyZWQgPSBib3JkZXJlZDtcbiAgfVxuICBnZXQgYm9yZGVyZWQoKTogYm9vbGVhbiB7XG4gICAgcmV0dXJuIHRoaXMuc3QuYm9yZGVyZWQ7XG4gIH1cblxuICAvKipcbiAgICog6KGo5qC85qCH6aKYXG4gICAqL1xuICBASW5wdXQoKSBzZXQgaGVhZGVyKGhlYWRlcjogc3RyaW5nIHwgVGVtcGxhdGVSZWY8dm9pZD4pIHtcbiAgICB0aGlzLnN0LmhlYWRlciA9IGhlYWRlclxuICB9XG5cbiAgLyoqXG4gICAqIOihqOagvOW6lemDqFxuICAgKi9cbiAgQElucHV0KCkgc2V0IGZvb3Rlcihmb290ZXI6IHN0cmluZyB8IFRlbXBsYXRlUmVmPHZvaWQ+KSB7XG4gICAgdGhpcy5zdC5mb290ZXIgPSBmb290ZXJcbiAgfVxuXG4gIC8qKlxuICAgKiDooajmoLzpobbpg6jpop3lpJblhoXlrrnvvIzkuIDoiKznlKjkuo7mt7vliqDlkIjorqHooYxcbiAgICovXG4gIEBJbnB1dCgpIHNldCBib2R5SGVhZGVyKGJvZHlIZWFkZXI6IFRlbXBsYXRlUmVmPFNUU3RhdGlzdGljYWxSZXN1bHRzPikge1xuICAgIHRoaXMuc3QuYm9keUhlYWRlciA9IGJvZHlIZWFkZXI7XG5cbiAgfVxuXG5cbiAgQElucHV0KCkgQElucHV0Qm9vbGVhbigpIHNldCBleHBhbmRSb3dCeUNsaWNrKGV4cGFuZFJvd0J5Q2xpY2s6IGJvb2xlYW4pIHtcbiAgICB0aGlzLnN0LmV4cGFuZFJvd0J5Q2xpY2sgPSBleHBhbmRSb3dCeUNsaWNrO1xuICB9XG5cbiAgQElucHV0KCkgQElucHV0Qm9vbGVhbigpIHNldCBleHBhbmRBY2NvcmRpb24oZXhwYW5kQWNjb3JkaW9uOiBib29sZWFuKSB7XG4gICAgdGhpcy5zdC5leHBhbmRBY2NvcmRpb24gPSBleHBhbmRBY2NvcmRpb247XG4gIH1cbiAgQElucHV0KCkgQElucHV0Qm9vbGVhbigpIHNldCBleHBhbmQoZXhwYW5kOiBUZW1wbGF0ZVJlZjx7XG4gICAgJGltcGxpY2l0OiB7fTtcbiAgICBjb2x1bW46IFNUQ29sdW1uO1xuICB9Pikge1xuICAgIHRoaXMuc3QuZXhwYW5kID0gZXhwYW5kO1xuICB9XG5cbiAgZ2V0IGZpbHRlcmVkRGF0YSgpOiBQcm9taXNlPFNURGF0YVtdPiB7XG4gICAgcmV0dXJuIHRoaXMuc3QuZmlsdGVyZWREYXRhO1xuICB9XG5cblxuICAvLyDliJfkv6Hmga9cbiAgX2NvbHVtbnM6IFNUQ29sdW1uW107XG5cbiAgLy8g5pyN5YqhSURcbiAgX3NuYW1lID0gJyc7XG4gIF9xdWVyeXBhcmFzOiBhbnk7XG4gIEBWaWV3Q2hpbGQoJ3N0JywgeyBzdGF0aWM6IHRydWUgfSkgc3Q6IFNUQ29tcG9uZW50O1xuXG4gIHJlcXBhcmFzID0geyBwYXJhbXM6IHsgc25hbWU6IHRoaXMuX3NuYW1lLCBmb3JtOiB0aGlzLl9xdWVyeXBhcmFzLCBzdGFjYWw6IHRoaXMuX3N0YXRjYWwgfSwgbWV0aG9kOiAncG9zdCcsIGFsbEluQm9keTogdHJ1ZSB9O1xuXG4gIGRhdGFQcm9jZXNzPzogKGRhdGE6IFNURGF0YVtdLCByYXdEYXRhPzogYW55KSA9PiBTVERhdGFbXTtcblxuICBuZ09uSW5pdCgpIHtcbiAgICBzZXRUaW1lb3V0KCgpID0+IHtcblxuICAgICAgdGhpcy5jb2x1bW5zLmZvckVhY2goY29sID0+IHtcbiAgICAgICAgdGhpcy5yZXN0b3JlUmVuZGVyKGNvbCk7XG4gICAgICB9KVxuXG4gICAgICB0aGlzLnN0LnJlc2V0Q29sdW1ucygpO1xuXG4gICAgfSwgMTAwKTtcblxuICB9XG5cbiAgcmVzZXRDb2x1bW5zKG9wdGlvbnM/OiBTVFJlc2V0Q29sdW1uc09wdGlvbikge1xuICAgIHRoaXMuc3QucmVzZXRDb2x1bW5zKG9wdGlvbnMpO1xuICB9XG4gIC8qKlxuICAgKiDnp7vpmaTooYwgXG4gICAqIEBwYXJhbSBkYXRhIFNURGF0YSB8IFNURGF0YVtdIHwgbnVtYmVyXG4gICAqL1xuICByZW1vdmVSb3coZGF0YSkge1xuICAgIHRoaXMuc3QucmVtb3ZlUm93KGRhdGEpO1xuICB9XG5cbiAgLyoqXG4gICAqIOa4heepuuaJgOacieaVsOaNrlxuICAgKi9cbiAgY2xlYXIoKSB7XG4gICAgdGhpcy5zdC5jbGVhcigpO1xuICB9XG5cblxuXG4gIHJlbG9hZChxdWVyeXBhcmFzPzogYW55KSB7XG5cbiAgICB0aGlzLnN0LmxvYWRpbmcgPSB0cnVlO1xuICAgIGlmIChxdWVyeXBhcmFzKVxuICAgICAgdGhpcy5zdC5yZXEucGFyYW1zLmZvcm0gPSBxdWVyeXBhcmFzO1xuICAgIHRoaXMuc3QucmVsb2FkKCk7XG4gICAgdGhpcy5zdC5jZCgpO1xuXG4gIH1cblxuICBzZXRSb3coaW5kZXg6IG51bWJlciwgZGF0YTogU1REYXRhKSB7XG4gICAgdGhpcy5zdC5zZXRSb3coaW5kZXgsIGRhdGEpO1xuICB9XG5cbiAgcmVzZXQoZXZlbnQ6IGFueSkge1xuICAgIHRoaXMubG9hZGluZyA9IHRydWU7XG4gICAgdGhpcy5sb2FkaW5nQ2hhbmdlLmVtaXQodHJ1ZSk7XG4gICAgdGhpcy5zdC5yZXNldChldmVudCk7XG4gICAgdGhpcy5zdC5jZCgpO1xuICB9XG5cbiAgcHJpdmF0ZSBwcm9jZXNzID0gKGRhdGE6IFNURGF0YVtdKSA9PiB7XG4gICAgcmV0dXJuIGRhdGE7XG4gIH07XG5cbiAgcHJpdmF0ZSBjdXN0b21TdGF0aXN0aWNhbCh2YWx1ZXM6IG51bWJlcltdLCBjb2w6IFNUQ29sdW1uLCBsaXN0OiBTVERhdGFbXSwgcmF3RGF0YT86IGFueSkge1xuXG4gICAgaWYgKHJhd0RhdGEuY29kZSA8IDEpIHJldHVybiB7IHZhbHVlOiBcIlwiLCB0ZXh0OiBcIlwiIH07XG5cbiAgICBpZiAocmF3RGF0YS5wYWdlLnN0YXRpc3RpY2FsKSB7XG5cbiAgICAgIGxldCBrZXkgPSBjb2wuaW5kZXg7XG4gICAgICBpZiAoa2V5IGluc3RhbmNlb2YgQXJyYXkpIHtcbiAgICAgICAga2V5ID0ga2V5WzBdO1xuICAgICAgfVxuXG5cbiAgICAgIGlmIChyYXdEYXRhLnBhZ2Uuc3RhdGlzdGljYWxba2V5XSkge1xuXG4gICAgICAgIHJldHVybiB7IHZhbHVlOiByYXdEYXRhLnBhZ2Uuc3RhdGlzdGljYWxba2V5XSwgdGV4dDogXCJcIiB9O1xuICAgICAgfVxuXG4gICAgICByZXR1cm4geyB2YWx1ZTogXCJcIiwgdGV4dDogXCJcIiB9O1xuICAgIH1cblxuICAgIHJldHVybiB7IHZhbHVlOiBcIlwiLCB0ZXh0OiBcIlwiIH07XG5cbiAgfVxuXG4gIC8vIOWvvOWHumV4Y2VsXG4gIGV4cG9ydChvcHQ/OiBTVEV4cG9ydE9wdGlvbnMpIHtcblxuICAgIGNvbnN0IGRhdGEgPSB0aGlzLnN0Ll9kYXRhO1xuICAgIHRoaXMuc3QuZXhwb3J0KGRhdGEsIG9wdCk7XG4gIH1cblxuICBleHBvcnRBbGwoKSB7XG5cbiAgICB0aGlzLmxvYWRpbmcgPSB0cnVlO1xuXG4gICAgdGhpcy5zdXBleGNlbC5leHBvcnQodGhpcy5jb2x1bW5zLCB0aGlzLl9zbmFtZSwgdGhpcy5zdC5yZXEucGFyYW1zLmZvcm0sIHRoaXMuc3QudG90YWwpLnRoZW4oKCkgPT4ge1xuICAgICAgdGhpcy5sb2FkaW5nID0gZmFsc2U7XG4gICAgfSk7XG5cbiAgfVxuXG4gIHByaXZhdGUgcmVzdG9yZVJlbmRlcihpdGVtOiBTVENvbHVtbikge1xuICAgIGlmIChpdGVtLnJlbmRlclRpdGxlMikge1xuICAgICAgaXRlbS5fX3JlbmRlclRpdGxlID0gdGhpcy5sYlNvdXJjZS5nZXRUaXRsZShpdGVtLnJlbmRlclRpdGxlMik7XG5cbiAgICB9XG4gICAgaWYgKGl0ZW0ucmVuZGVyMikge1xuICAgICAgaXRlbS5fX3JlbmRlciA9IHRoaXMubGJTb3VyY2UuZ2V0Um93KGl0ZW0ucmVuZGVyMik7XG5cbiAgICB9XG5cbiAgfVxuXG59XG4iXX0=