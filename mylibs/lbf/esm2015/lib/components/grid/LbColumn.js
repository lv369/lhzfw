/**
 * @fileoverview added by tsickle
 * Generated from: lib/components/grid/LbColumn.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Directive, Host, Injectable, Input, TemplateRef } from '@angular/core';
export class LbRowSource {
    constructor() {
        this.titles = {};
        this.rows = {};
    }
    /**
     * @param {?} type
     * @param {?} path
     * @param {?} ref
     * @return {?}
     */
    add(type, path, ref) {
        this[type === 'title' ? 'titles' : 'rows'][path] = ref;
    }
    /**
     * @param {?} path
     * @return {?}
     */
    getTitle(path) {
        return this.titles[path];
    }
    /**
     * @param {?} path
     * @return {?}
     */
    getRow(path) {
        return this.rows[path];
    }
}
LbRowSource.decorators = [
    { type: Injectable }
];
/** @nocollapse */
LbRowSource.ctorParameters = () => [];
if (false) {
    /**
     * @type {?}
     * @private
     */
    LbRowSource.prototype.titles;
    /**
     * @type {?}
     * @private
     */
    LbRowSource.prototype.rows;
}
export class LbRowDirective {
    /**
     * @param {?} ref
     * @param {?} source
     */
    constructor(ref, source) {
        this.ref = ref;
        this.source = source;
        console.log('lb-row constructor');
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        this.source.add(this.type, this.id, this.ref);
        console.log(this.id, this.ref);
    }
}
LbRowDirective.decorators = [
    { type: Directive, args: [{ selector: '[lb-row]' },] }
];
/** @nocollapse */
LbRowDirective.ctorParameters = () => [
    { type: TemplateRef },
    { type: LbRowSource, decorators: [{ type: Host }] }
];
LbRowDirective.propDecorators = {
    id: [{ type: Input, args: ['lb-row',] }],
    type: [{ type: Input }]
};
if (false) {
    /** @type {?} */
    LbRowDirective.prototype.id;
    /** @type {?} */
    LbRowDirective.prototype.type;
    /**
     * @type {?}
     * @private
     */
    LbRowDirective.prototype.ref;
    /**
     * @type {?}
     * @private
     */
    LbRowDirective.prototype.source;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiTGJDb2x1bW4uanMiLCJzb3VyY2VSb290Ijoibmc6Ly9sYmYvIiwic291cmNlcyI6WyJsaWIvY29tcG9uZW50cy9ncmlkL0xiQ29sdW1uLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7O0FBQUEsT0FBTyxFQUFFLFNBQVMsRUFBRSxJQUFJLEVBQUUsVUFBVSxFQUFFLEtBQUssRUFBVSxXQUFXLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFHeEYsTUFBTSxPQUFPLFdBQVc7SUFJdEI7UUFIUSxXQUFNLEdBQXlDLEVBQUUsQ0FBQztRQUNsRCxTQUFJLEdBQXlDLEVBQUUsQ0FBQztJQUl4RCxDQUFDOzs7Ozs7O0lBRUQsR0FBRyxDQUFDLElBQVksRUFBRSxJQUFZLEVBQUUsR0FBc0I7UUFDcEQsSUFBSSxDQUFDLElBQUksS0FBSyxPQUFPLENBQUMsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsTUFBTSxDQUFDLENBQUMsSUFBSSxDQUFDLEdBQUcsR0FBRyxDQUFDO0lBQ3pELENBQUM7Ozs7O0lBRUQsUUFBUSxDQUFDLElBQVk7UUFDbkIsT0FBTyxJQUFJLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxDQUFDO0lBQzNCLENBQUM7Ozs7O0lBRUQsTUFBTSxDQUFDLElBQVk7UUFDakIsT0FBTyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO0lBQ3pCLENBQUM7OztZQW5CRixVQUFVOzs7Ozs7Ozs7SUFFVCw2QkFBMEQ7Ozs7O0lBQzFELDJCQUF3RDs7QUFvQjFELE1BQU0sT0FBTyxjQUFjOzs7OztJQU96QixZQUFvQixHQUFzQixFQUFrQixNQUFtQjtRQUEzRCxRQUFHLEdBQUgsR0FBRyxDQUFtQjtRQUFrQixXQUFNLEdBQU4sTUFBTSxDQUFhO1FBQzdFLE9BQU8sQ0FBQyxHQUFHLENBQUMsb0JBQW9CLENBQUMsQ0FBQTtJQUNuQyxDQUFDOzs7O0lBRUQsUUFBUTtRQUVOLElBQUksQ0FBQyxNQUFNLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxJQUFJLEVBQUUsSUFBSSxDQUFDLEVBQUUsRUFBRSxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUM7UUFDOUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsRUFBRSxFQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQztJQUNoQyxDQUFDOzs7WUFoQkYsU0FBUyxTQUFDLEVBQUUsUUFBUSxFQUFFLFVBQVUsRUFBRTs7OztZQXhCa0IsV0FBVztZQWdDTSxXQUFXLHVCQUFsQyxJQUFJOzs7aUJBTmhELEtBQUssU0FBQyxRQUFRO21CQUdkLEtBQUs7Ozs7SUFITiw0QkFDVzs7SUFFWCw4QkFDYzs7Ozs7SUFFRiw2QkFBOEI7Ozs7O0lBQUUsZ0NBQW1DIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgRGlyZWN0aXZlLCBIb3N0LCBJbmplY3RhYmxlLCBJbnB1dCwgT25Jbml0LCBUZW1wbGF0ZVJlZiB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5cclxuQEluamVjdGFibGUoKVxyXG5leHBvcnQgY2xhc3MgTGJSb3dTb3VyY2Uge1xyXG4gIHByaXZhdGUgdGl0bGVzOiB7IFtrZXk6IHN0cmluZ106IFRlbXBsYXRlUmVmPHZvaWQ+IH0gPSB7fTtcclxuICBwcml2YXRlIHJvd3M6IHsgW2tleTogc3RyaW5nXTogVGVtcGxhdGVSZWY8dm9pZD4gfSA9IHt9O1xyXG5cclxuICBjb25zdHJ1Y3Rvcigpe1xyXG5cclxuICB9XHJcblxyXG4gIGFkZCh0eXBlOiBzdHJpbmcsIHBhdGg6IHN0cmluZywgcmVmOiBUZW1wbGF0ZVJlZjx2b2lkPikge1xyXG4gICAgdGhpc1t0eXBlID09PSAndGl0bGUnID8gJ3RpdGxlcycgOiAncm93cyddW3BhdGhdID0gcmVmO1xyXG4gIH1cclxuXHJcbiAgZ2V0VGl0bGUocGF0aDogc3RyaW5nKSB7XHJcbiAgICByZXR1cm4gdGhpcy50aXRsZXNbcGF0aF07XHJcbiAgfVxyXG5cclxuICBnZXRSb3cocGF0aDogc3RyaW5nKSB7XHJcbiAgICByZXR1cm4gdGhpcy5yb3dzW3BhdGhdO1xyXG4gIH1cclxufVxyXG5cclxuQERpcmVjdGl2ZSh7IHNlbGVjdG9yOiAnW2xiLXJvd10nIH0pXHJcbmV4cG9ydCBjbGFzcyBMYlJvd0RpcmVjdGl2ZSBpbXBsZW1lbnRzIE9uSW5pdCB7XHJcbiAgQElucHV0KCdsYi1yb3cnKVxyXG4gIGlkOiBzdHJpbmc7XHJcblxyXG4gIEBJbnB1dCgpXHJcbiAgdHlwZTogJ3RpdGxlJztcclxuXHJcbiAgY29uc3RydWN0b3IocHJpdmF0ZSByZWY6IFRlbXBsYXRlUmVmPHZvaWQ+LCBASG9zdCgpIHByaXZhdGUgc291cmNlOiBMYlJvd1NvdXJjZSkge1xyXG4gICAgY29uc29sZS5sb2coJ2xiLXJvdyBjb25zdHJ1Y3RvcicpXHJcbiAgfVxyXG5cclxuICBuZ09uSW5pdCgpOiB2b2lkIHtcclxuICAgIFxyXG4gICAgdGhpcy5zb3VyY2UuYWRkKHRoaXMudHlwZSwgdGhpcy5pZCwgdGhpcy5yZWYpO1xyXG4gICAgY29uc29sZS5sb2codGhpcy5pZCx0aGlzLnJlZik7XHJcbiAgfVxyXG59XHJcbiJdfQ==