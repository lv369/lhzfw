/**
 * @fileoverview added by tsickle
 * Generated from: lib/lbf.module.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { NgModule } from '@angular/core';
import { LbfComponent } from './lbf.component';
import { DicPipe } from './lblibs/DicPipe';
import { NgZorroAntdModule } from 'ng-zorro-antd';
import { AlainThemeModule } from '@delon/theme';
import { DelonABCModule } from '@delon/abc';
import { DelonChartModule } from '@delon/chart';
import { DelonACLModule } from '@delon/acl';
import { DelonUtilModule } from '@delon/util';
import { DelonAuthModule } from '@delon/auth';
import { DelonFormModule } from '@delon/form';
import { DelonCacheModule } from '@delon/cache';
import { GridComponent } from './components/grid/grid.component';
import { ImgUploadComponent } from './components/img-upload/img-upload.component';
import { PopdetailComponent } from './components/popdetail/popdetail.component';
import { CommonModule } from '@angular/common';
import { LbRowDirective } from '../public-api';
import { RouterModule } from '@angular/router';
/** @type {?} */
const LbfComponents = [LbfComponent, GridComponent, DicPipe, ImgUploadComponent, PopdetailComponent, LbRowDirective];
export class LbfModule {
}
LbfModule.decorators = [
    { type: NgModule, args: [{
                declarations: [...LbfComponents],
                imports: [
                    CommonModule,
                    NgZorroAntdModule,
                    RouterModule,
                    AlainThemeModule.forRoot(),
                    DelonABCModule,
                    DelonChartModule,
                    DelonACLModule,
                    DelonCacheModule,
                    DelonUtilModule,
                    DelonAuthModule,
                    DelonFormModule.forRoot(),
                ],
                exports: [...LbfComponents]
            },] }
];
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibGJmLm1vZHVsZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2xiZi8iLCJzb3VyY2VzIjpbImxpYi9sYmYubW9kdWxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7O0FBQUEsT0FBTyxFQUFFLFFBQVEsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUN6QyxPQUFPLEVBQUUsWUFBWSxFQUFFLE1BQU0saUJBQWlCLENBQUM7QUFDL0MsT0FBTyxFQUFFLE9BQU8sRUFBRSxNQUFNLGtCQUFrQixDQUFDO0FBQzNDLE9BQU8sRUFBRSxpQkFBaUIsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUNsRCxPQUFPLEVBQUUsZ0JBQWdCLEVBQUUsTUFBTSxjQUFjLENBQUM7QUFDaEQsT0FBTyxFQUFFLGNBQWMsRUFBRSxNQUFNLFlBQVksQ0FBQztBQUM1QyxPQUFPLEVBQUUsZ0JBQWdCLEVBQUUsTUFBTSxjQUFjLENBQUM7QUFDaEQsT0FBTyxFQUFFLGNBQWMsRUFBRSxNQUFNLFlBQVksQ0FBQztBQUM1QyxPQUFPLEVBQUUsZUFBZSxFQUFFLE1BQU0sYUFBYSxDQUFDO0FBQzlDLE9BQU8sRUFBRSxlQUFlLEVBQUUsTUFBTSxhQUFhLENBQUM7QUFDOUMsT0FBTyxFQUFFLGVBQWUsRUFBRSxNQUFNLGFBQWEsQ0FBQztBQUM5QyxPQUFPLEVBQUUsZ0JBQWdCLEVBQUUsTUFBTSxjQUFjLENBQUM7QUFDaEQsT0FBTyxFQUFFLGFBQWEsRUFBRSxNQUFNLGtDQUFrQyxDQUFDO0FBQ2pFLE9BQU8sRUFBRSxrQkFBa0IsRUFBRSxNQUFNLDhDQUE4QyxDQUFDO0FBQ2xGLE9BQU8sRUFBRSxrQkFBa0IsRUFBRSxNQUFNLDRDQUE0QyxDQUFDO0FBQ2hGLE9BQU8sRUFBRSxZQUFZLEVBQUUsTUFBTSxpQkFBaUIsQ0FBQztBQUMvQyxPQUFPLEVBQUUsY0FBYyxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQy9DLE9BQU8sRUFBRSxZQUFZLEVBQUUsTUFBTSxpQkFBaUIsQ0FBQzs7TUFHekMsYUFBYSxHQUFHLENBQUMsWUFBWSxFQUFFLGFBQWEsRUFBRSxPQUFPLEVBQUUsa0JBQWtCLEVBQUUsa0JBQWtCLEVBQUUsY0FBYyxDQUFDO0FBb0JwSCxNQUFNLE9BQU8sU0FBUzs7O1lBakJyQixRQUFRLFNBQUM7Z0JBQ1IsWUFBWSxFQUFFLENBQUMsR0FBRyxhQUFhLENBQUM7Z0JBQ2hDLE9BQU8sRUFBRTtvQkFDUCxZQUFZO29CQUNaLGlCQUFpQjtvQkFDakIsWUFBWTtvQkFDWixnQkFBZ0IsQ0FBQyxPQUFPLEVBQUU7b0JBQzFCLGNBQWM7b0JBQ2QsZ0JBQWdCO29CQUNoQixjQUFjO29CQUNkLGdCQUFnQjtvQkFDaEIsZUFBZTtvQkFDZixlQUFlO29CQUNmLGVBQWUsQ0FBQyxPQUFPLEVBQUU7aUJBQzFCO2dCQUNELE9BQU8sRUFBRSxDQUFDLEdBQUcsYUFBYSxDQUFDO2FBQzVCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgTmdNb2R1bGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IExiZkNvbXBvbmVudCB9IGZyb20gJy4vbGJmLmNvbXBvbmVudCc7XG5pbXBvcnQgeyBEaWNQaXBlIH0gZnJvbSAnLi9sYmxpYnMvRGljUGlwZSc7XG5pbXBvcnQgeyBOZ1pvcnJvQW50ZE1vZHVsZSB9IGZyb20gJ25nLXpvcnJvLWFudGQnO1xuaW1wb3J0IHsgQWxhaW5UaGVtZU1vZHVsZSB9IGZyb20gJ0BkZWxvbi90aGVtZSc7XG5pbXBvcnQgeyBEZWxvbkFCQ01vZHVsZSB9IGZyb20gJ0BkZWxvbi9hYmMnO1xuaW1wb3J0IHsgRGVsb25DaGFydE1vZHVsZSB9IGZyb20gJ0BkZWxvbi9jaGFydCc7XG5pbXBvcnQgeyBEZWxvbkFDTE1vZHVsZSB9IGZyb20gJ0BkZWxvbi9hY2wnO1xuaW1wb3J0IHsgRGVsb25VdGlsTW9kdWxlIH0gZnJvbSAnQGRlbG9uL3V0aWwnO1xuaW1wb3J0IHsgRGVsb25BdXRoTW9kdWxlIH0gZnJvbSAnQGRlbG9uL2F1dGgnO1xuaW1wb3J0IHsgRGVsb25Gb3JtTW9kdWxlIH0gZnJvbSAnQGRlbG9uL2Zvcm0nO1xuaW1wb3J0IHsgRGVsb25DYWNoZU1vZHVsZSB9IGZyb20gJ0BkZWxvbi9jYWNoZSc7XG5pbXBvcnQgeyBHcmlkQ29tcG9uZW50IH0gZnJvbSAnLi9jb21wb25lbnRzL2dyaWQvZ3JpZC5jb21wb25lbnQnO1xuaW1wb3J0IHsgSW1nVXBsb2FkQ29tcG9uZW50IH0gZnJvbSAnLi9jb21wb25lbnRzL2ltZy11cGxvYWQvaW1nLXVwbG9hZC5jb21wb25lbnQnO1xuaW1wb3J0IHsgUG9wZGV0YWlsQ29tcG9uZW50IH0gZnJvbSAnLi9jb21wb25lbnRzL3BvcGRldGFpbC9wb3BkZXRhaWwuY29tcG9uZW50JztcbmltcG9ydCB7IENvbW1vbk1vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbic7XG5pbXBvcnQgeyBMYlJvd0RpcmVjdGl2ZSB9IGZyb20gJy4uL3B1YmxpYy1hcGknO1xuaW1wb3J0IHsgUm91dGVyTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvcm91dGVyJztcblxuXG5jb25zdCBMYmZDb21wb25lbnRzID0gW0xiZkNvbXBvbmVudCwgR3JpZENvbXBvbmVudCwgRGljUGlwZSwgSW1nVXBsb2FkQ29tcG9uZW50LCBQb3BkZXRhaWxDb21wb25lbnQsIExiUm93RGlyZWN0aXZlXTtcblxuXG5ATmdNb2R1bGUoe1xuICBkZWNsYXJhdGlvbnM6IFsuLi5MYmZDb21wb25lbnRzXSxcbiAgaW1wb3J0czogW1xuICAgIENvbW1vbk1vZHVsZSxcbiAgICBOZ1pvcnJvQW50ZE1vZHVsZSxcbiAgICBSb3V0ZXJNb2R1bGUsXG4gICAgQWxhaW5UaGVtZU1vZHVsZS5mb3JSb290KCksXG4gICAgRGVsb25BQkNNb2R1bGUsXG4gICAgRGVsb25DaGFydE1vZHVsZSxcbiAgICBEZWxvbkFDTE1vZHVsZSxcbiAgICBEZWxvbkNhY2hlTW9kdWxlLFxuICAgIERlbG9uVXRpbE1vZHVsZSxcbiAgICBEZWxvbkF1dGhNb2R1bGUsXG4gICAgRGVsb25Gb3JtTW9kdWxlLmZvclJvb3QoKSxcbiAgXSxcbiAgZXhwb3J0czogWy4uLkxiZkNvbXBvbmVudHNdXG59KVxuZXhwb3J0IGNsYXNzIExiZk1vZHVsZSB7IH1cbiJdfQ==