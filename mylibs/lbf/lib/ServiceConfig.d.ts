export declare class ServiceInfo {
    constructor();
    APPID: string;
    private APPKEY;
    private PWDFLAG;
    LOGTOKEN: string;
    VERSION: string;
    APPINFO: {
        "name": string;
        "description": string;
    };
    APPSUFFIX: string;
    user: any;
    baseUrl: string;
    molssUrl: string;
    uploadurl: string;
    _adminRole: string[];
    setAdminRole(adminRole: string[]): void;
    readonly isAdmin: boolean;
    getSign(sname: string, timestamp: string, para: string): string;
    setSinfo(info: any): void;
    setUser(user: any): void;
    loaduser(): void;
    getTimestamp(): number;
    saveToken(token: string): void;
    getToken(): void;
    clearToken(): void;
    md5(md5Str: string): string;
    pwdmd5(pwd: string): string;
}
