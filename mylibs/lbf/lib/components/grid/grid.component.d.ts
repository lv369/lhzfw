import { OnInit, EventEmitter, TemplateRef } from '@angular/core';
import { STComponent, STColumn, STData, STExportOptions, STPage, STStatisticalResults, STResetColumnsOption } from '@delon/abc';
import { SupDic } from '../../lblibs/SupDic';
import { SupExcel } from '../../lblibs/SupExcel';
import { LbRowSource } from './LbColumn';
import { ServiceInfo } from '../../ServiceConfig';
export declare class GridComponent implements OnInit {
    private supdic;
    private supexcel;
    private sinfo;
    private lbSource;
    /**
     * 表格数值
     */
    readonly data: STData[];
    readonly innerSt: STComponent;
    constructor(supdic: SupDic, supexcel: SupExcel, sinfo: ServiceInfo, lbSource: LbRowSource);
    grhxurl: string[];
    isGrhx: boolean;
    columns: STColumn[];
    sname: string;
    queryparas: any;
    /**
     * 每页数量，当设置为 0 表示不分页，默认：10
     */
    ps: number;
    datas: string | STData[];
    statisticals: any[];
    _statcal: {};
    loading: boolean;
    loadingChange: EventEmitter<any>;
    /**
     * 加载指示符
     */
    loadingIndicator: TemplateRef<void>;
    /**
     * 延迟显示加载效果的时间（防止闪烁）
     */
    loadingDelay: number;
    /**
     * 横向或纵向支持滚动，也可用于指定滚动区域的宽高度：{ x: "300px", y: "300px" }
     */
    scroll: {
        y?: string;
        x?: string;
    };
    /**
     * 当前页码
     */
    pi: number;
    /**
     *  分页器配置
     */
    page: STPage;
    /**
     * 无数据时显示内容
     */
    noResult: string | TemplateRef<void>;
    /**
     * 是否显示边框
     */
    bordered: boolean;
    /**
     * 表格标题
     */
    header: string | TemplateRef<void>;
    /**
     * 表格底部
     */
    footer: string | TemplateRef<void>;
    /**
     * 表格顶部额外内容，一般用于添加合计行
     */
    bodyHeader: TemplateRef<STStatisticalResults>;
    expandRowByClick: boolean;
    expandAccordion: boolean;
    expand: TemplateRef<{
        $implicit: {};
        column: STColumn;
    }>;
    readonly filteredData: Promise<STData[]>;
    _columns: STColumn[];
    _sname: string;
    _queryparas: any;
    st: STComponent;
    reqparas: {
        params: {
            sname: string;
            form: any;
            stacal: {};
        };
        method: string;
        allInBody: boolean;
    };
    dataProcess?: (data: STData[], rawData?: any) => STData[];
    ngOnInit(): void;
    resetColumns(options?: STResetColumnsOption): void;
    /**
     * 移除行
     * @param data STData | STData[] | number
     */
    removeRow(data: any): void;
    /**
     * 清空所有数据
     */
    clear(): void;
    reload(queryparas?: any): void;
    setRow(index: number, data: STData): void;
    reset(event: any): void;
    private process;
    private customStatistical;
    export(opt?: STExportOptions): void;
    exportAll(): void;
    private restoreRender;
}
