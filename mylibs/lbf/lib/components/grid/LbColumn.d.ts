import { OnInit, TemplateRef } from '@angular/core';
export declare class LbRowSource {
    private titles;
    private rows;
    constructor();
    add(type: string, path: string, ref: TemplateRef<void>): void;
    getTitle(path: string): TemplateRef<void>;
    getRow(path: string): TemplateRef<void>;
}
export declare class LbRowDirective implements OnInit {
    private ref;
    private source;
    id: string;
    type: 'title';
    constructor(ref: TemplateRef<void>, source: LbRowSource);
    ngOnInit(): void;
}
