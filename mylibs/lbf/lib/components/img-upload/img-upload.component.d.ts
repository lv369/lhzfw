import { OnInit } from '@angular/core';
import { NzMessageService, NzModalService, UploadFile, UploadXHRArgs } from 'ng-zorro-antd';
import { Observable, Subscription } from 'rxjs';
import { HttpService } from '../../lblibs/Lbjk.service';
export declare class ImgUploadComponent implements OnInit {
    private msgsvr;
    private lbservice;
    private modalService;
    constructor(msgsvr: NzMessageService, lbservice: HttpService, modalService: NzModalService);
    /**
     * 图表列表
     */
    fileList: any[];
    _previewImage: string;
    _previewVisible: boolean;
    /**
     * 上传按钮是否显示
     */
    nzShowButton: boolean;
    /**
     * 预览按钮以及删除按钮
     */
    showUploadList: {
        showPreviewIcon: boolean;
        showRemoveIcon: boolean;
        hidePreviewIconInNonImage: boolean;
    };
    ngOnInit(): void;
    /**
     * 上传事件
     */
    upload: (item: UploadXHRArgs) => typeof Subscription;
    /**
     * 删除事件
     */
    del: (file: UploadFile) => Promise<unknown>;
    _handlePreview: (file: UploadFile) => void;
    /**
     * 图片上传
     */
    _uploadimg: (item: UploadXHRArgs) => typeof Subscription;
    /**
     * 移除文件
     */
    _removeIMG: (file: UploadFile) => Observable<boolean>;
    /**
     * 上传成功后处理
     * @param info  上传返回信息
     */
    _handleChange(info: any): void;
    _beforeUpload: (file: File) => Observable<boolean>;
}
