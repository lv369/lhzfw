import { HttpClient } from '@angular/common/http';
import { UploadXHRArgs } from 'ng-zorro-antd';
import { Subscription, Observable } from 'rxjs';
import { ServiceInfo } from '../ServiceConfig';
export declare class HttpService {
    private httpClient;
    private serviceInfo;
    constructor(httpClient: HttpClient, serviceInfo: ServiceInfo);
    /**
     * 登录服务
     * @param username 用户名
     * @param password 用户密码
     * @returns 登录结果 Promise<any>
     */
    login(username: string, password: string): Promise<any>;
    changeRole(roleid: string): Promise<any>;
    lbservice2(sname: string, para: {}, debounce?: number, anonymous?: boolean): Observable<{}>;
    /**
     *  访问服务端
     * @param sname 服务名
     * @param para 入参
     * @param debounce 防抖设置，默认0
     * @param anonymous 是否匿名访问，默认否
     */
    lbservice(sname: string, para: {}, debounce?: number, anonymous?: boolean): Promise<any>;
    /**
     * 文件上传类服务
     *   只支持单文件上传
     * @param sname 服务名
     * @param item 文件上传信息
     * @param para 参数
     */
    lbfileupload(sname: string, item: UploadXHRArgs, para: object): Subscription;
}
