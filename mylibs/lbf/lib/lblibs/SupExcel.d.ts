import { STColumn, XlsxService } from '@delon/abc';
import { HttpService } from './Lbjk.service';
export declare class SupExcel {
    private lbservice;
    private xlsx;
    constructor(lbservice: HttpService, xlsx: XlsxService);
    /**
     * 导出excel
     * @param columns 字段信息
     * @param sname 查询服务名
     * @param para 查询服务参数
     * @param total 总条数
     * @param onetime 单次请求数，默认20
     */
    export(columns: STColumn[], sname: string, para: any, total: number, onetime?: number): Promise<unknown>;
}
