import { Injector } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent } from '@angular/common/http';
import { Observable } from 'rxjs';
import { NzMessageService } from 'ng-zorro-antd';
import { ServiceInfo } from '../ServiceConfig';
/**
 * 默认HTTP拦截器，其注册细节见 `app.module.ts`
 */
export declare class LbfwInterceptor implements HttpInterceptor {
    private injector;
    constructor(injector: Injector);
    readonly msg: NzMessageService;
    private goTo;
    readonly sinfo: ServiceInfo;
    private checkStatus;
    private handleData;
    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>>;
}
