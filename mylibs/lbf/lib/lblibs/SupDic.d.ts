import { SFSchemaEnumType } from '@delon/form';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { HttpService } from './Lbjk.service';
export declare class SupDic {
    private lbservice;
    private httpclient;
    constructor(lbservice: HttpService, httpclient: HttpClient);
    private dicCache;
    ka010203: any;
    /**
     * 获取字典信息,返回数组
     *  @param dicName 字典值
     *  @returns 字典信息，未取到返回[]
     */
    getDic(dicName: string): any;
    /**
     * 获取字典信息,返回object
     *  @param dicName 字典值
     *  @returns 字典信息
     */
    getDicObj(dicName: string): any;
    /**
     * 获取指定字典指定值对应的标签值
     * @param key 字典名称
     * @param val 值
     */
    getdicLabel(key: string, val: string): any;
    /**
     * 异步方式获取字典信息，返回SF的字典数组
     *   始终从数据库中获取，获取的字典将会写入到缓存中
     * @param dicName 字典名称
     */
    getSFDicAsync(dicName: string): Observable<SFSchemaEnumType[]>;
    /**
     * 获取SF格式的字典信息
     * @param dicName 字典值
     */
    getSFDic(dicName: string): SFSchemaEnumType[];
    /**
     * 加载所有字典
     */
    loadAllDic(): Promise<void>;
}
