import { PipeTransform } from '@angular/core';
import { SupDic } from './SupDic';
export declare class DicPipe implements PipeTransform {
    private supdic;
    constructor(supdic: SupDic);
    transform(value: string, dicname: string): any;
}
