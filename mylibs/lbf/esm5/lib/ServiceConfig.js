/**
 * @fileoverview added by tsickle
 * Generated from: lib/ServiceConfig.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
import { Injectable } from '@angular/core';
import { Md5 } from "ts-md5/dist/md5";
import * as i0 from "@angular/core";
var ServiceInfo = /** @class */ (function () {
    function ServiceInfo() {
        this.APPID = "TEST_APPID";
        this.APPKEY = "TEST_APPKEY";
        this.PWDFLAG = "H5CS3";
        this.LOGTOKEN = "";
        this.VERSION = "20190730";
        this.APPINFO = {
            "name": "测试",
            "description": ""
        };
        this.APPSUFFIX = this.APPINFO.name;
        this.user = { aac003: "", name: "", roleid: "", dept: "", avatar: "./assets/tmp/img/avatar.jpg" };
        this.baseUrl = "";
        this.molssUrl = "mohrss/chbx";
        this.uploadurl = "mohrss/chbx/upload";
        this._adminRole = [];
    }
    /**
     * @param {?} adminRole
     * @return {?}
     */
    ServiceInfo.prototype.setAdminRole = /**
     * @param {?} adminRole
     * @return {?}
     */
    function (adminRole) {
        this._adminRole = adminRole;
    };
    Object.defineProperty(ServiceInfo.prototype, "isAdmin", {
        get: /**
         * @return {?}
         */
        function () {
            if (!this._adminRole || !this.user || !this.user.roleid)
                return false;
            return this._adminRole.includes(this.user.roleid);
        },
        enumerable: true,
        configurable: true
    });
    /**
     * @param {?} sname
     * @param {?} timestamp
     * @param {?} para
     * @return {?}
     */
    ServiceInfo.prototype.getSign = /**
     * @param {?} sname
     * @param {?} timestamp
     * @param {?} para
     * @return {?}
     */
    function (sname, timestamp, para) {
        /** @type {?} */
        var hashstr = this.APPKEY + para + timestamp + sname + this.LOGTOKEN;
        return Md5.hashStr(hashstr).toString();
    };
    /**
     * @param {?} info
     * @return {?}
     */
    ServiceInfo.prototype.setSinfo = /**
     * @param {?} info
     * @return {?}
     */
    function (info) {
        this.APPID = info.APPID;
        this.APPKEY = info.APPKEY;
        this.VERSION = info.VERSION;
        this.APPINFO.name = info.APPNAME;
        this.APPSUFFIX = this.APPINFO.name;
    };
    /**
     * @param {?} user
     * @return {?}
     */
    ServiceInfo.prototype.setUser = /**
     * @param {?} user
     * @return {?}
     */
    function (user) {
        this.user = tslib_1.__assign({}, this.user, user);
        // this.user.name = user.name;
        // this.user.aac003 = user.aac003;
        this.user.roleid = user.usertype;
        // this.user.dept = user.dept;
        sessionStorage.setItem("userinfo", JSON.stringify(this.user));
    };
    /**
     * @return {?}
     */
    ServiceInfo.prototype.loaduser = /**
     * @return {?}
     */
    function () {
        /** @type {?} */
        var userinfo = sessionStorage.getItem("userinfo");
        try {
            /** @type {?} */
            var u = JSON.parse(userinfo);
            this.user = tslib_1.__assign({}, this.user, u);
            // this.user.name = u.name;
            // this.user.aac003 = u.aac003;
            // this.user.roleid = u.roleid;
            // this.user.dept = u.dept;
        }
        catch (ex) {
        }
    };
    /**
     * @return {?}
     */
    ServiceInfo.prototype.getTimestamp = /**
     * @return {?}
     */
    function () {
        /** @type {?} */
        var d = new Date();
        return Date.UTC(d.getFullYear(), d.getMonth(), d.getDate(), d.getHours(), d.getMinutes(), d.getSeconds(), d.getMilliseconds());
    };
    /**
     * @param {?} token
     * @return {?}
     */
    ServiceInfo.prototype.saveToken = /**
     * @param {?} token
     * @return {?}
     */
    function (token) {
        sessionStorage.setItem("logintoken", token);
    };
    /**
     * @return {?}
     */
    ServiceInfo.prototype.getToken = /**
     * @return {?}
     */
    function () {
        this.LOGTOKEN = sessionStorage.getItem("logintoken");
        if (this.LOGTOKEN == null)
            this.LOGTOKEN = "";
    };
    /**
     * @return {?}
     */
    ServiceInfo.prototype.clearToken = /**
     * @return {?}
     */
    function () {
        sessionStorage.setItem("logintoken", "");
    };
    /**
     * @param {?} md5Str
     * @return {?}
     */
    ServiceInfo.prototype.md5 = /**
     * @param {?} md5Str
     * @return {?}
     */
    function (md5Str) {
        return Md5.hashStr(md5Str).toString();
    };
    /**
     * @param {?} pwd
     * @return {?}
     */
    ServiceInfo.prototype.pwdmd5 = /**
     * @param {?} pwd
     * @return {?}
     */
    function (pwd) {
        return this.md5(pwd + this.PWDFLAG);
    };
    ServiceInfo.decorators = [
        { type: Injectable, args: [{
                    providedIn: 'root'
                },] }
    ];
    /** @nocollapse */
    ServiceInfo.ctorParameters = function () { return []; };
    /** @nocollapse */ ServiceInfo.ngInjectableDef = i0.ɵɵdefineInjectable({ factory: function ServiceInfo_Factory() { return new ServiceInfo(); }, token: ServiceInfo, providedIn: "root" });
    return ServiceInfo;
}());
export { ServiceInfo };
if (false) {
    /** @type {?} */
    ServiceInfo.prototype.APPID;
    /**
     * @type {?}
     * @private
     */
    ServiceInfo.prototype.APPKEY;
    /**
     * @type {?}
     * @private
     */
    ServiceInfo.prototype.PWDFLAG;
    /** @type {?} */
    ServiceInfo.prototype.LOGTOKEN;
    /** @type {?} */
    ServiceInfo.prototype.VERSION;
    /** @type {?} */
    ServiceInfo.prototype.APPINFO;
    /** @type {?} */
    ServiceInfo.prototype.APPSUFFIX;
    /** @type {?} */
    ServiceInfo.prototype.user;
    /** @type {?} */
    ServiceInfo.prototype.baseUrl;
    /** @type {?} */
    ServiceInfo.prototype.molssUrl;
    /** @type {?} */
    ServiceInfo.prototype.uploadurl;
    /** @type {?} */
    ServiceInfo.prototype._adminRole;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiU2VydmljZUNvbmZpZy5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2xiZi8iLCJzb3VyY2VzIjpbImxpYi9TZXJ2aWNlQ29uZmlnLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7OztBQUFBLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDM0MsT0FBTyxFQUFFLEdBQUcsRUFBRSxNQUFNLGlCQUFpQixDQUFDOztBQUV0QztJQUtFO1FBRU8sVUFBSyxHQUFHLFlBQVksQ0FBQztRQUNwQixXQUFNLEdBQUcsYUFBYSxDQUFDO1FBQ3ZCLFlBQU8sR0FBRyxPQUFPLENBQUM7UUFDbkIsYUFBUSxHQUFHLEVBQUUsQ0FBQztRQUNkLFlBQU8sR0FBRyxVQUFVLENBQUE7UUFDcEIsWUFBTyxHQUFHO1lBQ2YsTUFBTSxFQUFFLElBQUk7WUFDWixhQUFhLEVBQUUsRUFBRTtTQUNsQixDQUFDO1FBQ0ssY0FBUyxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDO1FBRTlCLFNBQUksR0FBUSxFQUFFLE1BQU0sRUFBRSxFQUFFLEVBQUUsSUFBSSxFQUFFLEVBQUUsRUFBRSxNQUFNLEVBQUUsRUFBRSxFQUFFLElBQUksRUFBRSxFQUFFLEVBQUUsTUFBTSxFQUFFLDZCQUE2QixFQUFFLENBQUE7UUFFakcsWUFBTyxHQUFHLEVBQUUsQ0FBQztRQUNiLGFBQVEsR0FBRyxhQUFhLENBQUM7UUFDekIsY0FBUyxHQUFHLG9CQUFvQixDQUFDO1FBRXhDLGVBQVUsR0FBYSxFQUFFLENBQUM7SUFuQlYsQ0FBQzs7Ozs7SUFxQmpCLGtDQUFZOzs7O0lBQVosVUFBYSxTQUFtQjtRQUM5QixJQUFJLENBQUMsVUFBVSxHQUFHLFNBQVMsQ0FBQztJQUM5QixDQUFDO0lBRUQsc0JBQUksZ0NBQU87Ozs7UUFBWDtZQUVFLElBQUksQ0FBQyxJQUFJLENBQUMsVUFBVSxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTTtnQkFBRSxPQUFPLEtBQUssQ0FBQztZQUV0RSxPQUFPLElBQUksQ0FBQyxVQUFVLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUM7UUFDcEQsQ0FBQzs7O09BQUE7Ozs7Ozs7SUFFRCw2QkFBTzs7Ozs7O0lBQVAsVUFBUSxLQUFhLEVBQUUsU0FBaUIsRUFBRSxJQUFZOztZQUU5QyxPQUFPLEdBQUcsSUFBSSxDQUFDLE1BQU0sR0FBRyxJQUFJLEdBQUcsU0FBUyxHQUFHLEtBQUssR0FBRyxJQUFJLENBQUMsUUFBUTtRQUN0RSxPQUFPLEdBQUcsQ0FBQyxPQUFPLENBQUMsT0FBTyxDQUFDLENBQUMsUUFBUSxFQUFFLENBQUM7SUFDekMsQ0FBQzs7Ozs7SUFFRCw4QkFBUTs7OztJQUFSLFVBQVMsSUFBSTtRQUNYLElBQUksQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQztRQUN4QixJQUFJLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUM7UUFDMUIsSUFBSSxDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUMsT0FBTyxDQUFDO1FBQzVCLElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUM7UUFDakMsSUFBSSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQztJQUNyQyxDQUFDOzs7OztJQUVELDZCQUFPOzs7O0lBQVAsVUFBUSxJQUFTO1FBRWYsSUFBSSxDQUFDLElBQUksd0JBQVEsSUFBSSxDQUFDLElBQUksRUFBSyxJQUFJLENBQUUsQ0FBQTtRQUNyQyw4QkFBOEI7UUFDOUIsa0NBQWtDO1FBQ2xDLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUM7UUFDakMsOEJBQThCO1FBQzlCLGNBQWMsQ0FBQyxPQUFPLENBQUMsVUFBVSxFQUFFLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUM7SUFFaEUsQ0FBQzs7OztJQUlELDhCQUFROzs7SUFBUjs7WUFFUSxRQUFRLEdBQUcsY0FBYyxDQUFDLE9BQU8sQ0FBQyxVQUFVLENBQUM7UUFDbkQsSUFBSTs7Z0JBQ0ksQ0FBQyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDO1lBRTlCLElBQUksQ0FBQyxJQUFJLHdCQUFRLElBQUksQ0FBQyxJQUFJLEVBQUssQ0FBQyxDQUFFLENBQUE7WUFDbEMsMkJBQTJCO1lBQzNCLCtCQUErQjtZQUMvQiwrQkFBK0I7WUFDL0IsMkJBQTJCO1NBRTVCO1FBQUMsT0FBTyxFQUFFLEVBQUU7U0FFWjtJQUVILENBQUM7Ozs7SUFFRCxrQ0FBWTs7O0lBQVo7O1lBQ1EsQ0FBQyxHQUFHLElBQUksSUFBSSxFQUFFO1FBQ3BCLE9BQU8sSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsV0FBVyxFQUFFLEVBQzNCLENBQUMsQ0FBQyxRQUFRLEVBQUUsRUFDWixDQUFDLENBQUMsT0FBTyxFQUFFLEVBQ1gsQ0FBQyxDQUFDLFFBQVEsRUFBRSxFQUNaLENBQUMsQ0FBQyxVQUFVLEVBQUUsRUFDZCxDQUFDLENBQUMsVUFBVSxFQUFFLEVBQ2QsQ0FBQyxDQUFDLGVBQWUsRUFBRSxDQUFDLENBQUM7SUFDM0IsQ0FBQzs7Ozs7SUFFRCwrQkFBUzs7OztJQUFULFVBQVUsS0FBYTtRQUVyQixjQUFjLENBQUMsT0FBTyxDQUFDLFlBQVksRUFBRSxLQUFLLENBQUMsQ0FBQztJQUU5QyxDQUFDOzs7O0lBRUQsOEJBQVE7OztJQUFSO1FBRUUsSUFBSSxDQUFDLFFBQVEsR0FBRyxjQUFjLENBQUMsT0FBTyxDQUFDLFlBQVksQ0FBQyxDQUFDO1FBQ3JELElBQUksSUFBSSxDQUFDLFFBQVEsSUFBSSxJQUFJO1lBQUUsSUFBSSxDQUFDLFFBQVEsR0FBRyxFQUFFLENBQUM7SUFFaEQsQ0FBQzs7OztJQUVELGdDQUFVOzs7SUFBVjtRQUNFLGNBQWMsQ0FBQyxPQUFPLENBQUMsWUFBWSxFQUFFLEVBQUUsQ0FBQyxDQUFDO0lBRTNDLENBQUM7Ozs7O0lBRUQseUJBQUc7Ozs7SUFBSCxVQUFJLE1BQWM7UUFFaEIsT0FBTyxHQUFHLENBQUMsT0FBTyxDQUFDLE1BQU0sQ0FBQyxDQUFDLFFBQVEsRUFBRSxDQUFDO0lBRXhDLENBQUM7Ozs7O0lBRUQsNEJBQU07Ozs7SUFBTixVQUFPLEdBQVc7UUFDaEIsT0FBTyxJQUFJLENBQUMsR0FBRyxDQUFDLEdBQUcsR0FBRyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUM7SUFDdEMsQ0FBQzs7Z0JBdkhGLFVBQVUsU0FBQztvQkFDVixVQUFVLEVBQUUsTUFBTTtpQkFDbkI7Ozs7O3NCQUxEO0NBMkhDLEFBeEhELElBd0hDO1NBckhZLFdBQVc7OztJQUl0Qiw0QkFBNEI7Ozs7O0lBQzVCLDZCQUErQjs7Ozs7SUFDL0IsOEJBQTBCOztJQUMxQiwrQkFBcUI7O0lBQ3JCLDhCQUEyQjs7SUFDM0IsOEJBR0U7O0lBQ0YsZ0NBQXFDOztJQUVyQywyQkFBd0c7O0lBRXhHLDhCQUFvQjs7SUFDcEIsK0JBQWdDOztJQUNoQyxnQ0FBd0M7O0lBRXhDLGlDQUEwQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEluamVjdGFibGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgTWQ1IH0gZnJvbSBcInRzLW1kNS9kaXN0L21kNVwiO1xyXG5cclxuQEluamVjdGFibGUoe1xyXG4gIHByb3ZpZGVkSW46ICdyb290J1xyXG59KVxyXG5leHBvcnQgY2xhc3MgU2VydmljZUluZm8ge1xyXG5cclxuICBjb25zdHJ1Y3RvcigpIHsgfVxyXG5cclxuICBwdWJsaWMgQVBQSUQgPSBcIlRFU1RfQVBQSURcIjtcclxuICBwcml2YXRlIEFQUEtFWSA9IFwiVEVTVF9BUFBLRVlcIjtcclxuICBwcml2YXRlIFBXREZMQUcgPSBcIkg1Q1MzXCI7XHJcbiAgcHVibGljIExPR1RPS0VOID0gXCJcIjtcclxuICBwdWJsaWMgVkVSU0lPTiA9IFwiMjAxOTA3MzBcIlxyXG4gIHB1YmxpYyBBUFBJTkZPID0ge1xyXG4gICAgXCJuYW1lXCI6IFwi5rWL6K+VXCIsXHJcbiAgICBcImRlc2NyaXB0aW9uXCI6IFwiXCJcclxuICB9O1xyXG4gIHB1YmxpYyBBUFBTVUZGSVggPSB0aGlzLkFQUElORk8ubmFtZTtcclxuXHJcbiAgcHVibGljIHVzZXI6IGFueSA9IHsgYWFjMDAzOiBcIlwiLCBuYW1lOiBcIlwiLCByb2xlaWQ6IFwiXCIsIGRlcHQ6IFwiXCIsIGF2YXRhcjogXCIuL2Fzc2V0cy90bXAvaW1nL2F2YXRhci5qcGdcIiB9XHJcblxyXG4gIHB1YmxpYyBiYXNlVXJsID0gXCJcIjtcclxuICBwdWJsaWMgbW9sc3NVcmwgPSBcIm1vaHJzcy9jaGJ4XCI7XHJcbiAgcHVibGljIHVwbG9hZHVybCA9IFwibW9ocnNzL2NoYngvdXBsb2FkXCI7XHJcblxyXG4gIF9hZG1pblJvbGU6IHN0cmluZ1tdID0gW107XHJcblxyXG4gIHNldEFkbWluUm9sZShhZG1pblJvbGU6IHN0cmluZ1tdKSB7XHJcbiAgICB0aGlzLl9hZG1pblJvbGUgPSBhZG1pblJvbGU7XHJcbiAgfVxyXG5cclxuICBnZXQgaXNBZG1pbigpIHtcclxuXHJcbiAgICBpZiAoIXRoaXMuX2FkbWluUm9sZSB8fCAhdGhpcy51c2VyIHx8ICF0aGlzLnVzZXIucm9sZWlkKSByZXR1cm4gZmFsc2U7XHJcblxyXG4gICAgcmV0dXJuIHRoaXMuX2FkbWluUm9sZS5pbmNsdWRlcyh0aGlzLnVzZXIucm9sZWlkKTtcclxuICB9XHJcblxyXG4gIGdldFNpZ24oc25hbWU6IHN0cmluZywgdGltZXN0YW1wOiBzdHJpbmcsIHBhcmE6IHN0cmluZyk6IHN0cmluZyB7XHJcblxyXG4gICAgY29uc3QgaGFzaHN0ciA9IHRoaXMuQVBQS0VZICsgcGFyYSArIHRpbWVzdGFtcCArIHNuYW1lICsgdGhpcy5MT0dUT0tFTlxyXG4gICAgcmV0dXJuIE1kNS5oYXNoU3RyKGhhc2hzdHIpLnRvU3RyaW5nKCk7XHJcbiAgfVxyXG5cclxuICBzZXRTaW5mbyhpbmZvKSB7XHJcbiAgICB0aGlzLkFQUElEID0gaW5mby5BUFBJRDtcclxuICAgIHRoaXMuQVBQS0VZID0gaW5mby5BUFBLRVk7XHJcbiAgICB0aGlzLlZFUlNJT04gPSBpbmZvLlZFUlNJT047XHJcbiAgICB0aGlzLkFQUElORk8ubmFtZSA9IGluZm8uQVBQTkFNRTtcclxuICAgIHRoaXMuQVBQU1VGRklYID0gdGhpcy5BUFBJTkZPLm5hbWU7XHJcbiAgfVxyXG5cclxuICBzZXRVc2VyKHVzZXI6IGFueSkge1xyXG5cclxuICAgIHRoaXMudXNlciA9IHsgLi4udGhpcy51c2VyLCAuLi51c2VyIH1cclxuICAgIC8vIHRoaXMudXNlci5uYW1lID0gdXNlci5uYW1lO1xyXG4gICAgLy8gdGhpcy51c2VyLmFhYzAwMyA9IHVzZXIuYWFjMDAzO1xyXG4gICAgdGhpcy51c2VyLnJvbGVpZCA9IHVzZXIudXNlcnR5cGU7XHJcbiAgICAvLyB0aGlzLnVzZXIuZGVwdCA9IHVzZXIuZGVwdDtcclxuICAgIHNlc3Npb25TdG9yYWdlLnNldEl0ZW0oXCJ1c2VyaW5mb1wiLCBKU09OLnN0cmluZ2lmeSh0aGlzLnVzZXIpKTtcclxuXHJcbiAgfVxyXG5cclxuXHJcblxyXG4gIGxvYWR1c2VyKCkge1xyXG5cclxuICAgIGNvbnN0IHVzZXJpbmZvID0gc2Vzc2lvblN0b3JhZ2UuZ2V0SXRlbShcInVzZXJpbmZvXCIpO1xyXG4gICAgdHJ5IHtcclxuICAgICAgY29uc3QgdSA9IEpTT04ucGFyc2UodXNlcmluZm8pO1xyXG5cclxuICAgICAgdGhpcy51c2VyID0geyAuLi50aGlzLnVzZXIsIC4uLnUgfVxyXG4gICAgICAvLyB0aGlzLnVzZXIubmFtZSA9IHUubmFtZTtcclxuICAgICAgLy8gdGhpcy51c2VyLmFhYzAwMyA9IHUuYWFjMDAzO1xyXG4gICAgICAvLyB0aGlzLnVzZXIucm9sZWlkID0gdS5yb2xlaWQ7XHJcbiAgICAgIC8vIHRoaXMudXNlci5kZXB0ID0gdS5kZXB0O1xyXG5cclxuICAgIH0gY2F0Y2ggKGV4KSB7XHJcblxyXG4gICAgfVxyXG5cclxuICB9XHJcblxyXG4gIGdldFRpbWVzdGFtcCgpOiBudW1iZXIge1xyXG4gICAgY29uc3QgZCA9IG5ldyBEYXRlKCk7XHJcbiAgICByZXR1cm4gRGF0ZS5VVEMoZC5nZXRGdWxsWWVhcigpXHJcbiAgICAgICwgZC5nZXRNb250aCgpXHJcbiAgICAgICwgZC5nZXREYXRlKClcclxuICAgICAgLCBkLmdldEhvdXJzKClcclxuICAgICAgLCBkLmdldE1pbnV0ZXMoKVxyXG4gICAgICAsIGQuZ2V0U2Vjb25kcygpXHJcbiAgICAgICwgZC5nZXRNaWxsaXNlY29uZHMoKSk7XHJcbiAgfVxyXG5cclxuICBzYXZlVG9rZW4odG9rZW46IHN0cmluZykge1xyXG5cclxuICAgIHNlc3Npb25TdG9yYWdlLnNldEl0ZW0oXCJsb2dpbnRva2VuXCIsIHRva2VuKTtcclxuXHJcbiAgfVxyXG5cclxuICBnZXRUb2tlbigpOiB2b2lkIHtcclxuXHJcbiAgICB0aGlzLkxPR1RPS0VOID0gc2Vzc2lvblN0b3JhZ2UuZ2V0SXRlbShcImxvZ2ludG9rZW5cIik7XHJcbiAgICBpZiAodGhpcy5MT0dUT0tFTiA9PSBudWxsKSB0aGlzLkxPR1RPS0VOID0gXCJcIjtcclxuXHJcbiAgfVxyXG5cclxuICBjbGVhclRva2VuKCk6IHZvaWQge1xyXG4gICAgc2Vzc2lvblN0b3JhZ2Uuc2V0SXRlbShcImxvZ2ludG9rZW5cIiwgXCJcIik7XHJcblxyXG4gIH1cclxuXHJcbiAgbWQ1KG1kNVN0cjogc3RyaW5nKTogc3RyaW5nIHtcclxuXHJcbiAgICByZXR1cm4gTWQ1Lmhhc2hTdHIobWQ1U3RyKS50b1N0cmluZygpO1xyXG5cclxuICB9XHJcblxyXG4gIHB3ZG1kNShwd2Q6IHN0cmluZyk6IHN0cmluZyB7XHJcbiAgICByZXR1cm4gdGhpcy5tZDUocHdkICsgdGhpcy5QV0RGTEFHKTtcclxuICB9XHJcbn1cclxuXHJcblxyXG4iXX0=