/**
 * @fileoverview added by tsickle
 * Generated from: lib/lbf.component.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component } from '@angular/core';
var LbfComponent = /** @class */ (function () {
    function LbfComponent() {
    }
    /**
     * @return {?}
     */
    LbfComponent.prototype.ngOnInit = /**
     * @return {?}
     */
    function () {
    };
    LbfComponent.decorators = [
        { type: Component, args: [{
                    selector: 'lb-lbf',
                    template: "\n    <p>\n      lbf works!\n    </p>\n  "
                }] }
    ];
    /** @nocollapse */
    LbfComponent.ctorParameters = function () { return []; };
    return LbfComponent;
}());
export { LbfComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibGJmLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2xiZi8iLCJzb3VyY2VzIjpbImxpYi9sYmYuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7O0FBQUEsT0FBTyxFQUFFLFNBQVMsRUFBVSxNQUFNLGVBQWUsQ0FBQztBQUVsRDtJQVdFO0lBQWdCLENBQUM7Ozs7SUFFakIsK0JBQVE7OztJQUFSO0lBQ0EsQ0FBQzs7Z0JBZEYsU0FBUyxTQUFDO29CQUNULFFBQVEsRUFBRSxRQUFRO29CQUNsQixRQUFRLEVBQUUsMkNBSVQ7aUJBRUY7Ozs7SUFRRCxtQkFBQztDQUFBLEFBaEJELElBZ0JDO1NBUFksWUFBWSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5cbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogJ2xiLWxiZicsXG4gIHRlbXBsYXRlOiBgXG4gICAgPHA+XG4gICAgICBsYmYgd29ya3MhXG4gICAgPC9wPlxuICBgLFxuICBzdHlsZXM6IFtdXG59KVxuZXhwb3J0IGNsYXNzIExiZkNvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XG5cbiAgY29uc3RydWN0b3IoKSB7IH1cblxuICBuZ09uSW5pdCgpIHtcbiAgfVxuXG59XG4iXX0=