/**
 * @fileoverview added by tsickle
 * Generated from: lib/components/popdetail/popdetail.component.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component, Input, Output, EventEmitter, ViewChild } from '@angular/core';
import { SFComponent } from '@delon/form';
import { NzMessageService } from 'ng-zorro-antd';
import { HttpService } from '../../lblibs/Lbjk.service';
var PopdetailComponent = /** @class */ (function () {
    function PopdetailComponent(msgSrv, lbservice) {
        this.msgSrv = msgSrv;
        this.lbservice = lbservice;
        // 是否弹出
        this.isVisible = false;
        this.isVisibleChange = new EventEmitter();
        // 弹出框标题
        this.title = "明细框";
        // 字段信息
        this.schema = {};
        // 默认值
        this.formData = {};
        // 是否加载中
        this.loading = false;
        this.loadingChange = new EventEmitter();
        // 保存服务
        this.saveSname = '';
        // formchang事件
        this.formChange = new EventEmitter();
    }
    /**
     * @return {?}
     */
    PopdetailComponent.prototype.ngOnInit = /**
     * @return {?}
     */
    function () {
    };
    /**
     * @return {?}
     */
    PopdetailComponent.prototype.handleCancel = /**
     * @return {?}
     */
    function () {
        this.isVisible = false;
        this.isVisibleChange.emit(this.isVisible);
    };
    /**
     * @return {?}
     */
    PopdetailComponent.prototype.show = /**
     * @return {?}
     */
    function () {
        this.isVisible = true;
    };
    /**
     * @return {?}
     */
    PopdetailComponent.prototype.hidde = /**
     * @return {?}
     */
    function () {
        this.isVisible = true;
    };
    /**
     * @return {?}
     */
    PopdetailComponent.prototype.reset = /**
     * @return {?}
     */
    function () {
        this.sf.reset();
    };
    /**
     * @return {?}
     */
    PopdetailComponent.prototype.value = /**
     * @return {?}
     */
    function () {
        return this.sf.value;
    };
    // path 采用 / 来分隔，例如：/user/name
    // path 采用 / 来分隔，例如：/user/name
    /**
     * @param {?} path
     * @return {?}
     */
    PopdetailComponent.prototype.getValue = 
    // path 采用 / 来分隔，例如：/user/name
    /**
     * @param {?} path
     * @return {?}
     */
    function (path) {
        return this.sf.getValue(path);
    };
    /**
     * @param {?} path
     * @param {?} value
     * @return {?}
     */
    PopdetailComponent.prototype.setValue = /**
     * @param {?} path
     * @param {?} value
     * @return {?}
     */
    function (path, value) {
        this.sf.setValue(path, value);
    };
    /**
     * @return {?}
     */
    PopdetailComponent.prototype.validator = /**
     * @return {?}
     */
    function () {
        this.sf.validator();
    };
    /**
     * @param {?=} newSchema
     * @param {?=} newUI
     * @return {?}
     */
    PopdetailComponent.prototype.refreshSchema = /**
     * @param {?=} newSchema
     * @param {?=} newUI
     * @return {?}
     */
    function (newSchema, newUI) {
        this.sf.refreshSchema(newSchema, newUI);
    };
    /**
     * @return {?}
     */
    PopdetailComponent.prototype.getsf = /**
     * @return {?}
     */
    function () {
        return this.sf;
    };
    /**
     * @param {?} val
     * @return {?}
     */
    PopdetailComponent.prototype.save = /**
     * @param {?} val
     * @return {?}
     */
    function (val) {
        var _this = this;
        this.loading = true;
        this.loadingChange.emit(this.loading);
        if (this.saveSname && this.saveSname !== '') {
            this.lbservice.lbservice(this.saveSname, { para: val }, 500).then((/**
             * @param {?} resdata
             * @return {?}
             */
            function (resdata) {
                _this.loading = false;
                _this.loadingChange.emit(_this.loading);
                if (resdata.code < 1) {
                    _this.msgSrv.error(resdata.errmsg);
                    return;
                }
                if (_this.saveFu && typeof _this.saveFu === 'function') {
                    _this.saveFu(val, resdata);
                }
                _this.isVisible = false;
                _this.isVisibleChange.emit(_this.isVisible);
                _this.reset();
                _this.msgSrv.info("成功！");
            }));
        }
        else {
            if (this.saveFu && typeof this.saveFu === 'function') {
                this.saveFu(val, {});
            }
        }
    };
    /**
     * @param {?} value
     * @return {?}
     */
    PopdetailComponent.prototype.change = /**
     * @param {?} value
     * @return {?}
     */
    function (value) {
        this.formChange.emit(value);
    };
    PopdetailComponent.decorators = [
        { type: Component, args: [{
                    selector: 'lb-pop',
                    template: "<nz-modal [(nzVisible)]=\"isVisible\" [nzTitle]=\"title\" nzWidth=\"80%\" (nzOnCancel)=\"handleCancel()\"\n    (nzOnOk)=\"save(sf.value)\" [nzOkLoading]=\"loading\">\n\n    <sf #sf [schema]=\"schema\" [formData]=\"formData\" (formChange)=\"change($event)\" button=\"none\">\n\n    </sf>\n\n</nz-modal>",
                    styles: [""]
                }] }
    ];
    /** @nocollapse */
    PopdetailComponent.ctorParameters = function () { return [
        { type: NzMessageService },
        { type: HttpService }
    ]; };
    PopdetailComponent.propDecorators = {
        sf: [{ type: ViewChild, args: ['sf', { static: true },] }],
        isVisible: [{ type: Input }],
        isVisibleChange: [{ type: Output }],
        title: [{ type: Input }],
        schema: [{ type: Input }],
        formData: [{ type: Input }],
        loading: [{ type: Input }],
        loadingChange: [{ type: Output }],
        saveSname: [{ type: Input }],
        saveFu: [{ type: Input }],
        formChange: [{ type: Output }]
    };
    return PopdetailComponent;
}());
export { PopdetailComponent };
if (false) {
    /** @type {?} */
    PopdetailComponent.prototype.sf;
    /** @type {?} */
    PopdetailComponent.prototype.isVisible;
    /** @type {?} */
    PopdetailComponent.prototype.isVisibleChange;
    /** @type {?} */
    PopdetailComponent.prototype.title;
    /** @type {?} */
    PopdetailComponent.prototype.schema;
    /** @type {?} */
    PopdetailComponent.prototype.formData;
    /** @type {?} */
    PopdetailComponent.prototype.loading;
    /** @type {?} */
    PopdetailComponent.prototype.loadingChange;
    /** @type {?} */
    PopdetailComponent.prototype.saveSname;
    /** @type {?} */
    PopdetailComponent.prototype.saveFu;
    /** @type {?} */
    PopdetailComponent.prototype.formChange;
    /** @type {?} */
    PopdetailComponent.prototype.msgSrv;
    /**
     * @type {?}
     * @private
     */
    PopdetailComponent.prototype.lbservice;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicG9wZGV0YWlsLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2xiZi8iLCJzb3VyY2VzIjpbImxpYi9jb21wb25lbnRzL3BvcGRldGFpbC9wb3BkZXRhaWwuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7O0FBQUEsT0FBTyxFQUFFLFNBQVMsRUFBRSxLQUFLLEVBQUUsTUFBTSxFQUFFLFlBQVksRUFBRSxTQUFTLEVBQVUsTUFBTSxlQUFlLENBQUM7QUFDMUYsT0FBTyxFQUFZLFdBQVcsRUFBYyxNQUFNLGFBQWEsQ0FBQztBQUNoRSxPQUFPLEVBQUUsZ0JBQWdCLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDakQsT0FBTyxFQUFFLFdBQVcsRUFBRSxNQUFNLDJCQUEyQixDQUFDO0FBR3hEO0lBT0UsNEJBQ1MsTUFBd0IsRUFDdkIsU0FBc0I7UUFEdkIsV0FBTSxHQUFOLE1BQU0sQ0FBa0I7UUFDdkIsY0FBUyxHQUFULFNBQVMsQ0FBYTs7UUFPdkIsY0FBUyxHQUFHLEtBQUssQ0FBQztRQUNqQixvQkFBZSxHQUFHLElBQUksWUFBWSxFQUFFLENBQUM7O1FBR3RDLFVBQUssR0FBRyxLQUFLLENBQUM7O1FBR2QsV0FBTSxHQUFhLEVBQUUsQ0FBQzs7UUFHdEIsYUFBUSxHQUFRLEVBQUUsQ0FBQzs7UUFHbkIsWUFBTyxHQUFHLEtBQUssQ0FBQztRQUNmLGtCQUFhLEdBQUcsSUFBSSxZQUFZLEVBQUUsQ0FBQzs7UUFHcEMsY0FBUyxHQUFHLEVBQUUsQ0FBQzs7UUFJTCxlQUFVLEdBQUcsSUFBSSxZQUFZLEVBQUUsQ0FBQztJQTNCL0MsQ0FBQzs7OztJQTZCTCxxQ0FBUTs7O0lBQVI7SUFDQSxDQUFDOzs7O0lBR0QseUNBQVk7OztJQUFaO1FBQ0UsSUFBSSxDQUFDLFNBQVMsR0FBRyxLQUFLLENBQUM7UUFDdkIsSUFBSSxDQUFDLGVBQWUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDO0lBQzVDLENBQUM7Ozs7SUFHRCxpQ0FBSTs7O0lBQUo7UUFDRSxJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQztJQUN4QixDQUFDOzs7O0lBRUQsa0NBQUs7OztJQUFMO1FBQ0UsSUFBSSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUM7SUFDeEIsQ0FBQzs7OztJQUVELGtDQUFLOzs7SUFBTDtRQUNFLElBQUksQ0FBQyxFQUFFLENBQUMsS0FBSyxFQUFFLENBQUM7SUFDbEIsQ0FBQzs7OztJQUVELGtDQUFLOzs7SUFBTDtRQUNFLE9BQU8sSUFBSSxDQUFDLEVBQUUsQ0FBQyxLQUFLLENBQUM7SUFDdkIsQ0FBQztJQUVELDhCQUE4Qjs7Ozs7O0lBQzlCLHFDQUFROzs7Ozs7SUFBUixVQUFTLElBQVk7UUFDbkIsT0FBTyxJQUFJLENBQUMsRUFBRSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsQ0FBQztJQUNoQyxDQUFDOzs7Ozs7SUFFRCxxQ0FBUTs7Ozs7SUFBUixVQUFTLElBQVksRUFBRSxLQUFVO1FBQy9CLElBQUksQ0FBQyxFQUFFLENBQUMsUUFBUSxDQUFDLElBQUksRUFBRSxLQUFLLENBQUMsQ0FBQztJQUNoQyxDQUFDOzs7O0lBRUQsc0NBQVM7OztJQUFUO1FBQ0UsSUFBSSxDQUFDLEVBQUUsQ0FBQyxTQUFTLEVBQUUsQ0FBQztJQUN0QixDQUFDOzs7Ozs7SUFFRCwwQ0FBYTs7Ozs7SUFBYixVQUFjLFNBQW9CLEVBQUUsS0FBa0I7UUFDcEQsSUFBSSxDQUFDLEVBQUUsQ0FBQyxhQUFhLENBQUMsU0FBUyxFQUFFLEtBQUssQ0FBQyxDQUFDO0lBQzFDLENBQUM7Ozs7SUFFRCxrQ0FBSzs7O0lBQUw7UUFDRSxPQUFPLElBQUksQ0FBQyxFQUFFLENBQUM7SUFDakIsQ0FBQzs7Ozs7SUFFRCxpQ0FBSTs7OztJQUFKLFVBQUssR0FBUTtRQUFiLGlCQWdDQztRQTlCQyxJQUFJLENBQUMsT0FBTyxHQUFHLElBQUksQ0FBQztRQUNwQixJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUM7UUFFdEMsSUFBSSxJQUFJLENBQUMsU0FBUyxJQUFJLElBQUksQ0FBQyxTQUFTLEtBQUssRUFBRSxFQUFFO1lBQzNDLElBQUksQ0FBQyxTQUFTLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxTQUFTLEVBQUUsRUFBRSxJQUFJLEVBQUUsR0FBRyxFQUFFLEVBQUUsR0FBRyxDQUFDLENBQUMsSUFBSTs7OztZQUFDLFVBQUMsT0FBTztnQkFDeEUsS0FBSSxDQUFDLE9BQU8sR0FBRyxLQUFLLENBQUM7Z0JBQ3JCLEtBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLEtBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQztnQkFDdEMsSUFBSSxPQUFPLENBQUMsSUFBSSxHQUFHLENBQUMsRUFBRTtvQkFDcEIsS0FBSSxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLE1BQU0sQ0FBQyxDQUFDO29CQUNsQyxPQUFPO2lCQUNSO2dCQUVELElBQUksS0FBSSxDQUFDLE1BQU0sSUFBSSxPQUFPLEtBQUksQ0FBQyxNQUFNLEtBQUssVUFBVSxFQUFFO29CQUNwRCxLQUFJLENBQUMsTUFBTSxDQUFDLEdBQUcsRUFBRSxPQUFPLENBQUMsQ0FBQztpQkFDM0I7Z0JBRUQsS0FBSSxDQUFDLFNBQVMsR0FBRyxLQUFLLENBQUM7Z0JBQ3ZCLEtBQUksQ0FBQyxlQUFlLENBQUMsSUFBSSxDQUFDLEtBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQztnQkFDMUMsS0FBSSxDQUFDLEtBQUssRUFBRSxDQUFDO2dCQUNiLEtBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO1lBRTFCLENBQUMsRUFBQyxDQUFDO1NBQ0o7YUFBTTtZQUVMLElBQUksSUFBSSxDQUFDLE1BQU0sSUFBSSxPQUFPLElBQUksQ0FBQyxNQUFNLEtBQUssVUFBVSxFQUFFO2dCQUNwRCxJQUFJLENBQUMsTUFBTSxDQUFDLEdBQUcsRUFBRSxFQUFFLENBQUMsQ0FBQzthQUN0QjtTQUVGO0lBRUgsQ0FBQzs7Ozs7SUFFRCxtQ0FBTTs7OztJQUFOLFVBQU8sS0FBSztRQUNWLElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO0lBQzlCLENBQUM7O2dCQTFIRixTQUFTLFNBQUM7b0JBQ1QsUUFBUSxFQUFFLFFBQVE7b0JBQ2xCLHlUQUF5Qzs7aUJBRTFDOzs7O2dCQVJRLGdCQUFnQjtnQkFDaEIsV0FBVzs7O3FCQWdCakIsU0FBUyxTQUFDLElBQUksRUFBRSxFQUFFLE1BQU0sRUFBRSxJQUFJLEVBQUU7NEJBR2hDLEtBQUs7a0NBQ0wsTUFBTTt3QkFHTixLQUFLO3lCQUdMLEtBQUs7MkJBR0wsS0FBSzswQkFHTCxLQUFLO2dDQUNMLE1BQU07NEJBR04sS0FBSzt5QkFDTCxLQUFLOzZCQUdMLE1BQU07O0lBd0ZULHlCQUFDO0NBQUEsQUE3SEQsSUE2SEM7U0F4SFksa0JBQWtCOzs7SUFRN0IsZ0NBQW1EOztJQUduRCx1Q0FBMkI7O0lBQzNCLDZDQUErQzs7SUFHL0MsbUNBQXVCOztJQUd2QixvQ0FBK0I7O0lBRy9CLHNDQUE0Qjs7SUFHNUIscUNBQXlCOztJQUN6QiwyQ0FBNkM7O0lBRzdDLHVDQUF3Qjs7SUFDeEIsb0NBQXFCOztJQUdyQix3Q0FBbUQ7O0lBN0JqRCxvQ0FBK0I7Ozs7O0lBQy9CLHVDQUE4QiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgSW5wdXQsIE91dHB1dCwgRXZlbnRFbWl0dGVyLCBWaWV3Q2hpbGQsIE9uSW5pdCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgU0ZTY2hlbWEsIFNGQ29tcG9uZW50LCBTRlVJU2NoZW1hIH0gZnJvbSAnQGRlbG9uL2Zvcm0nO1xuaW1wb3J0IHsgTnpNZXNzYWdlU2VydmljZSB9IGZyb20gJ25nLXpvcnJvLWFudGQnO1xuaW1wb3J0IHsgSHR0cFNlcnZpY2UgfSBmcm9tICcuLi8uLi9sYmxpYnMvTGJqay5zZXJ2aWNlJztcblxuXG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6ICdsYi1wb3AnLFxuICB0ZW1wbGF0ZVVybDogJy4vcG9wZGV0YWlsLmNvbXBvbmVudC5odG1sJyxcbiAgc3R5bGVVcmxzOiBbJy4vcG9wZGV0YWlsLmNvbXBvbmVudC5jc3MnXVxufSlcbmV4cG9ydCBjbGFzcyBQb3BkZXRhaWxDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xuXG4gIGNvbnN0cnVjdG9yKFxuICAgIHB1YmxpYyBtc2dTcnY6IE56TWVzc2FnZVNlcnZpY2UsXG4gICAgcHJpdmF0ZSBsYnNlcnZpY2U6IEh0dHBTZXJ2aWNlLFxuICApIHsgfVxuXG5cbiAgQFZpZXdDaGlsZCgnc2YnLCB7IHN0YXRpYzogdHJ1ZSB9KSBzZjogU0ZDb21wb25lbnQ7XG5cbiAgLy8g5piv5ZCm5by55Ye6XG4gIEBJbnB1dCgpIGlzVmlzaWJsZSA9IGZhbHNlO1xuICBAT3V0cHV0KCkgaXNWaXNpYmxlQ2hhbmdlID0gbmV3IEV2ZW50RW1pdHRlcigpO1xuXG4gIC8vIOW8ueWHuuahhuagh+mimFxuICBASW5wdXQoKSB0aXRsZSA9IFwi5piO57uG5qGGXCI7XG5cbiAgLy8g5a2X5q615L+h5oGvXG4gIEBJbnB1dCgpIHNjaGVtYTogU0ZTY2hlbWEgPSB7fTtcblxuICAvLyDpu5jorqTlgLxcbiAgQElucHV0KCkgZm9ybURhdGE6IGFueSA9IHt9O1xuXG4gIC8vIOaYr+WQpuWKoOi9veS4rVxuICBASW5wdXQoKSBsb2FkaW5nID0gZmFsc2U7XG4gIEBPdXRwdXQoKSBsb2FkaW5nQ2hhbmdlID0gbmV3IEV2ZW50RW1pdHRlcigpO1xuXG4gIC8vIOS/neWtmOacjeWKoVxuICBASW5wdXQoKSBzYXZlU25hbWUgPSAnJztcbiAgQElucHV0KCkgc2F2ZUZ1OiBhbnk7XG5cbiAgLy8gZm9ybWNoYW5n5LqL5Lu2XG4gIEBPdXRwdXQoKSByZWFkb25seSBmb3JtQ2hhbmdlID0gbmV3IEV2ZW50RW1pdHRlcigpO1xuXG4gIG5nT25Jbml0KCkge1xuICB9XG5cblxuICBoYW5kbGVDYW5jZWwoKSB7XG4gICAgdGhpcy5pc1Zpc2libGUgPSBmYWxzZTtcbiAgICB0aGlzLmlzVmlzaWJsZUNoYW5nZS5lbWl0KHRoaXMuaXNWaXNpYmxlKTtcbiAgfVxuXG5cbiAgc2hvdygpIHtcbiAgICB0aGlzLmlzVmlzaWJsZSA9IHRydWU7XG4gIH1cblxuICBoaWRkZSgpIHtcbiAgICB0aGlzLmlzVmlzaWJsZSA9IHRydWU7XG4gIH1cblxuICByZXNldCgpIHtcbiAgICB0aGlzLnNmLnJlc2V0KCk7XG4gIH1cblxuICB2YWx1ZSgpOiBhbnkge1xuICAgIHJldHVybiB0aGlzLnNmLnZhbHVlO1xuICB9XG5cbiAgLy8gcGF0aCDph4fnlKggLyDmnaXliIbpmpTvvIzkvovlpoLvvJovdXNlci9uYW1lXG4gIGdldFZhbHVlKHBhdGg6IHN0cmluZyk6IGFueSB7XG4gICAgcmV0dXJuIHRoaXMuc2YuZ2V0VmFsdWUocGF0aCk7XG4gIH1cblxuICBzZXRWYWx1ZShwYXRoOiBzdHJpbmcsIHZhbHVlOiBhbnkpIHtcbiAgICB0aGlzLnNmLnNldFZhbHVlKHBhdGgsIHZhbHVlKTtcbiAgfVxuXG4gIHZhbGlkYXRvcigpIHtcbiAgICB0aGlzLnNmLnZhbGlkYXRvcigpO1xuICB9XG5cbiAgcmVmcmVzaFNjaGVtYShuZXdTY2hlbWE/OiBTRlNjaGVtYSwgbmV3VUk/OiBTRlVJU2NoZW1hKSB7XG4gICAgdGhpcy5zZi5yZWZyZXNoU2NoZW1hKG5ld1NjaGVtYSwgbmV3VUkpO1xuICB9XG5cbiAgZ2V0c2YoKTogU0ZDb21wb25lbnQge1xuICAgIHJldHVybiB0aGlzLnNmO1xuICB9XG5cbiAgc2F2ZSh2YWw6IGFueSkge1xuXG4gICAgdGhpcy5sb2FkaW5nID0gdHJ1ZTtcbiAgICB0aGlzLmxvYWRpbmdDaGFuZ2UuZW1pdCh0aGlzLmxvYWRpbmcpO1xuXG4gICAgaWYgKHRoaXMuc2F2ZVNuYW1lICYmIHRoaXMuc2F2ZVNuYW1lICE9PSAnJykge1xuICAgICAgdGhpcy5sYnNlcnZpY2UubGJzZXJ2aWNlKHRoaXMuc2F2ZVNuYW1lLCB7IHBhcmE6IHZhbCB9LCA1MDApLnRoZW4oKHJlc2RhdGEpID0+IHtcbiAgICAgICAgdGhpcy5sb2FkaW5nID0gZmFsc2U7XG4gICAgICAgIHRoaXMubG9hZGluZ0NoYW5nZS5lbWl0KHRoaXMubG9hZGluZyk7XG4gICAgICAgIGlmIChyZXNkYXRhLmNvZGUgPCAxKSB7XG4gICAgICAgICAgdGhpcy5tc2dTcnYuZXJyb3IocmVzZGF0YS5lcnJtc2cpO1xuICAgICAgICAgIHJldHVybjtcbiAgICAgICAgfVxuXG4gICAgICAgIGlmICh0aGlzLnNhdmVGdSAmJiB0eXBlb2YgdGhpcy5zYXZlRnUgPT09ICdmdW5jdGlvbicpIHtcbiAgICAgICAgICB0aGlzLnNhdmVGdSh2YWwsIHJlc2RhdGEpO1xuICAgICAgICB9XG5cbiAgICAgICAgdGhpcy5pc1Zpc2libGUgPSBmYWxzZTtcbiAgICAgICAgdGhpcy5pc1Zpc2libGVDaGFuZ2UuZW1pdCh0aGlzLmlzVmlzaWJsZSk7XG4gICAgICAgIHRoaXMucmVzZXQoKTtcbiAgICAgICAgdGhpcy5tc2dTcnYuaW5mbyhcIuaIkOWKn++8gVwiKTtcblxuICAgICAgfSk7XG4gICAgfSBlbHNlIHtcblxuICAgICAgaWYgKHRoaXMuc2F2ZUZ1ICYmIHR5cGVvZiB0aGlzLnNhdmVGdSA9PT0gJ2Z1bmN0aW9uJykge1xuICAgICAgICB0aGlzLnNhdmVGdSh2YWwsIHt9KTtcbiAgICAgIH1cblxuICAgIH1cblxuICB9XG5cbiAgY2hhbmdlKHZhbHVlKSB7XG4gICAgdGhpcy5mb3JtQ2hhbmdlLmVtaXQodmFsdWUpO1xuICB9XG5cblxufVxuIl19