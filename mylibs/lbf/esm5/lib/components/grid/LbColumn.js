/**
 * @fileoverview added by tsickle
 * Generated from: lib/components/grid/LbColumn.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Directive, Host, Injectable, Input, TemplateRef } from '@angular/core';
var LbRowSource = /** @class */ (function () {
    function LbRowSource() {
        this.titles = {};
        this.rows = {};
    }
    /**
     * @param {?} type
     * @param {?} path
     * @param {?} ref
     * @return {?}
     */
    LbRowSource.prototype.add = /**
     * @param {?} type
     * @param {?} path
     * @param {?} ref
     * @return {?}
     */
    function (type, path, ref) {
        this[type === 'title' ? 'titles' : 'rows'][path] = ref;
    };
    /**
     * @param {?} path
     * @return {?}
     */
    LbRowSource.prototype.getTitle = /**
     * @param {?} path
     * @return {?}
     */
    function (path) {
        return this.titles[path];
    };
    /**
     * @param {?} path
     * @return {?}
     */
    LbRowSource.prototype.getRow = /**
     * @param {?} path
     * @return {?}
     */
    function (path) {
        return this.rows[path];
    };
    LbRowSource.decorators = [
        { type: Injectable }
    ];
    /** @nocollapse */
    LbRowSource.ctorParameters = function () { return []; };
    return LbRowSource;
}());
export { LbRowSource };
if (false) {
    /**
     * @type {?}
     * @private
     */
    LbRowSource.prototype.titles;
    /**
     * @type {?}
     * @private
     */
    LbRowSource.prototype.rows;
}
var LbRowDirective = /** @class */ (function () {
    function LbRowDirective(ref, source) {
        this.ref = ref;
        this.source = source;
        console.log('lb-row constructor');
    }
    /**
     * @return {?}
     */
    LbRowDirective.prototype.ngOnInit = /**
     * @return {?}
     */
    function () {
        this.source.add(this.type, this.id, this.ref);
        console.log(this.id, this.ref);
    };
    LbRowDirective.decorators = [
        { type: Directive, args: [{ selector: '[lb-row]' },] }
    ];
    /** @nocollapse */
    LbRowDirective.ctorParameters = function () { return [
        { type: TemplateRef },
        { type: LbRowSource, decorators: [{ type: Host }] }
    ]; };
    LbRowDirective.propDecorators = {
        id: [{ type: Input, args: ['lb-row',] }],
        type: [{ type: Input }]
    };
    return LbRowDirective;
}());
export { LbRowDirective };
if (false) {
    /** @type {?} */
    LbRowDirective.prototype.id;
    /** @type {?} */
    LbRowDirective.prototype.type;
    /**
     * @type {?}
     * @private
     */
    LbRowDirective.prototype.ref;
    /**
     * @type {?}
     * @private
     */
    LbRowDirective.prototype.source;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiTGJDb2x1bW4uanMiLCJzb3VyY2VSb290Ijoibmc6Ly9sYmYvIiwic291cmNlcyI6WyJsaWIvY29tcG9uZW50cy9ncmlkL0xiQ29sdW1uLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7O0FBQUEsT0FBTyxFQUFFLFNBQVMsRUFBRSxJQUFJLEVBQUUsVUFBVSxFQUFFLEtBQUssRUFBVSxXQUFXLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFFeEY7SUFLRTtRQUhRLFdBQU0sR0FBeUMsRUFBRSxDQUFDO1FBQ2xELFNBQUksR0FBeUMsRUFBRSxDQUFDO0lBSXhELENBQUM7Ozs7Ozs7SUFFRCx5QkFBRzs7Ozs7O0lBQUgsVUFBSSxJQUFZLEVBQUUsSUFBWSxFQUFFLEdBQXNCO1FBQ3BELElBQUksQ0FBQyxJQUFJLEtBQUssT0FBTyxDQUFDLENBQUMsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxDQUFDLElBQUksQ0FBQyxHQUFHLEdBQUcsQ0FBQztJQUN6RCxDQUFDOzs7OztJQUVELDhCQUFROzs7O0lBQVIsVUFBUyxJQUFZO1FBQ25CLE9BQU8sSUFBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsQ0FBQztJQUMzQixDQUFDOzs7OztJQUVELDRCQUFNOzs7O0lBQU4sVUFBTyxJQUFZO1FBQ2pCLE9BQU8sSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztJQUN6QixDQUFDOztnQkFuQkYsVUFBVTs7OztJQW9CWCxrQkFBQztDQUFBLEFBcEJELElBb0JDO1NBbkJZLFdBQVc7Ozs7OztJQUN0Qiw2QkFBMEQ7Ozs7O0lBQzFELDJCQUF3RDs7QUFtQjFEO0lBUUUsd0JBQW9CLEdBQXNCLEVBQWtCLE1BQW1CO1FBQTNELFFBQUcsR0FBSCxHQUFHLENBQW1CO1FBQWtCLFdBQU0sR0FBTixNQUFNLENBQWE7UUFDN0UsT0FBTyxDQUFDLEdBQUcsQ0FBQyxvQkFBb0IsQ0FBQyxDQUFBO0lBQ25DLENBQUM7Ozs7SUFFRCxpQ0FBUTs7O0lBQVI7UUFFRSxJQUFJLENBQUMsTUFBTSxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsSUFBSSxFQUFFLElBQUksQ0FBQyxFQUFFLEVBQUUsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDO1FBQzlDLE9BQU8sQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLEVBQUUsRUFBQyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUM7SUFDaEMsQ0FBQzs7Z0JBaEJGLFNBQVMsU0FBQyxFQUFFLFFBQVEsRUFBRSxVQUFVLEVBQUU7Ozs7Z0JBeEJrQixXQUFXO2dCQWdDTSxXQUFXLHVCQUFsQyxJQUFJOzs7cUJBTmhELEtBQUssU0FBQyxRQUFRO3VCQUdkLEtBQUs7O0lBWVIscUJBQUM7Q0FBQSxBQWpCRCxJQWlCQztTQWhCWSxjQUFjOzs7SUFDekIsNEJBQ1c7O0lBRVgsOEJBQ2M7Ozs7O0lBRUYsNkJBQThCOzs7OztJQUFFLGdDQUFtQyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IERpcmVjdGl2ZSwgSG9zdCwgSW5qZWN0YWJsZSwgSW5wdXQsIE9uSW5pdCwgVGVtcGxhdGVSZWYgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuXHJcbkBJbmplY3RhYmxlKClcclxuZXhwb3J0IGNsYXNzIExiUm93U291cmNlIHtcclxuICBwcml2YXRlIHRpdGxlczogeyBba2V5OiBzdHJpbmddOiBUZW1wbGF0ZVJlZjx2b2lkPiB9ID0ge307XHJcbiAgcHJpdmF0ZSByb3dzOiB7IFtrZXk6IHN0cmluZ106IFRlbXBsYXRlUmVmPHZvaWQ+IH0gPSB7fTtcclxuXHJcbiAgY29uc3RydWN0b3IoKXtcclxuXHJcbiAgfVxyXG5cclxuICBhZGQodHlwZTogc3RyaW5nLCBwYXRoOiBzdHJpbmcsIHJlZjogVGVtcGxhdGVSZWY8dm9pZD4pIHtcclxuICAgIHRoaXNbdHlwZSA9PT0gJ3RpdGxlJyA/ICd0aXRsZXMnIDogJ3Jvd3MnXVtwYXRoXSA9IHJlZjtcclxuICB9XHJcblxyXG4gIGdldFRpdGxlKHBhdGg6IHN0cmluZykge1xyXG4gICAgcmV0dXJuIHRoaXMudGl0bGVzW3BhdGhdO1xyXG4gIH1cclxuXHJcbiAgZ2V0Um93KHBhdGg6IHN0cmluZykge1xyXG4gICAgcmV0dXJuIHRoaXMucm93c1twYXRoXTtcclxuICB9XHJcbn1cclxuXHJcbkBEaXJlY3RpdmUoeyBzZWxlY3RvcjogJ1tsYi1yb3ddJyB9KVxyXG5leHBvcnQgY2xhc3MgTGJSb3dEaXJlY3RpdmUgaW1wbGVtZW50cyBPbkluaXQge1xyXG4gIEBJbnB1dCgnbGItcm93JylcclxuICBpZDogc3RyaW5nO1xyXG5cclxuICBASW5wdXQoKVxyXG4gIHR5cGU6ICd0aXRsZSc7XHJcblxyXG4gIGNvbnN0cnVjdG9yKHByaXZhdGUgcmVmOiBUZW1wbGF0ZVJlZjx2b2lkPiwgQEhvc3QoKSBwcml2YXRlIHNvdXJjZTogTGJSb3dTb3VyY2UpIHtcclxuICAgIGNvbnNvbGUubG9nKCdsYi1yb3cgY29uc3RydWN0b3InKVxyXG4gIH1cclxuXHJcbiAgbmdPbkluaXQoKTogdm9pZCB7XHJcbiAgICBcclxuICAgIHRoaXMuc291cmNlLmFkZCh0aGlzLnR5cGUsIHRoaXMuaWQsIHRoaXMucmVmKTtcclxuICAgIGNvbnNvbGUubG9nKHRoaXMuaWQsdGhpcy5yZWYpO1xyXG4gIH1cclxufVxyXG4iXX0=