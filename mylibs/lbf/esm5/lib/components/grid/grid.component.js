/**
 * @fileoverview added by tsickle
 * Generated from: lib/components/grid/grid.component.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
import { Component, Input, Output, EventEmitter, ViewChild, TemplateRef } from '@angular/core';
import { STComponent, } from '@delon/abc';
import { SupDic } from '../../lblibs/SupDic';
import { SupExcel } from '../../lblibs/SupExcel';
import { LbRowSource } from './LbColumn';
import { InputNumber, InputBoolean } from 'ng-zorro-antd';
import { ServiceInfo } from '../../ServiceConfig';
var GridComponent = /** @class */ (function () {
    function GridComponent(supdic, supexcel, sinfo, lbSource) {
        this.supdic = supdic;
        this.supexcel = supexcel;
        this.sinfo = sinfo;
        this.lbSource = lbSource;
        this.grhxurl = ['/ybjg/grhx'];
        this.isGrhx = true;
        /**
         * 每页数量，当设置为 0 表示不分页，默认：10
         */
        this.ps = 10;
        this.datas = 'MohrssExInterface/gridsvr';
        this.statisticals = [];
        this._statcal = {};
        // 是否加载中
        this.loading = false;
        this.loadingChange = new EventEmitter();
        // 服务ID
        this._sname = '';
        this.reqparas = { params: { sname: this._sname, form: this._queryparas, stacal: this._statcal }, method: 'post', allInBody: true };
        this.process = (/**
         * @param {?} data
         * @return {?}
         */
        function (data) {
            return data;
        });
    }
    Object.defineProperty(GridComponent.prototype, "data", {
        /**
         * 表格数值
         */
        get: /**
         * 表格数值
         * @return {?}
         */
        function () {
            return this.st._data;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(GridComponent.prototype, "innerSt", {
        get: /**
         * @return {?}
         */
        function () {
            return this.st;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(GridComponent.prototype, "columns", {
        get: /**
         * @return {?}
         */
        function () {
            return this._columns;
        },
        set: /**
         * @param {?} columns
         * @return {?}
         */
        function (columns) {
            var _this = this;
            // 统计类信息
            this.statisticals = [];
            columns.forEach((/**
             * @param {?} col
             * @return {?}
             */
            function (col) {
                if (col.render) {
                    col.render2 = col.render;
                    col.render = '';
                }
                else {
                    if (_this.isGrhx && col.index === 'AAC003' && _this.sinfo.isAdmin) {
                        col.render = 'grhx';
                    }
                }
                if (col.renderTitle) {
                    col.renderTitle2 = col.renderTitle;
                    col.renderTitle = '';
                }
                /** @type {?} */
                var key = col.index;
                if (key instanceof Array) {
                    key = key[0];
                }
                col.key = key;
                // 添加字典信息
                if (col.dic) {
                    col.format = (/**
                     * @param {?} a
                     * @param {?} b
                     * @return {?}
                     */
                    function (a, b) { return _this.supdic.getdicLabel(b.dic, a[b.key]); });
                    col.filter = { menus: _this.supdic.getDic(col.dic) };
                }
                // 添加统计类型信息
                /** @type {?} */
                var val = col.statistical;
                if (!val) {
                    _this.statisticals.push({ key: key, show: false, type: "" });
                    _this._statcal[key] = "";
                    return;
                }
                /** @type {?} */
                var item = tslib_1.__assign({ digits: 2, currency: null }, (typeof val === 'string'
                    ? { type: (/** @type {?} */ (val)) }
                    : ((/** @type {?} */ (val)))));
                /** @type {?} */
                var stcal = item.type;
                item.digits = stcal === "count" ? 0 : 2;
                item.type = _this.customStatistical;
                item.currency = false;
                col.statistical = item;
                _this.statisticals.push({ key: key, show: true, type: stcal });
                _this._statcal[key] = stcal;
            }));
            this._columns = columns;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(GridComponent.prototype, "sname", {
        set: /**
         * @param {?} sname
         * @return {?}
         */
        function (sname) {
            this._sname = sname;
            this.reqparas.params.sname = this._sname;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(GridComponent.prototype, "queryparas", {
        // 查询参数
        get: 
        // 查询参数
        /**
         * @return {?}
         */
        function () {
            return this._queryparas;
        },
        set: /**
         * @param {?} value
         * @return {?}
         */
        function (value) {
            this._queryparas = value;
            this.reqparas.params.form = this._queryparas;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(GridComponent.prototype, "loadingIndicator", {
        /**
         * 加载指示符
         */
        set: /**
         * 加载指示符
         * @param {?} loadingIndicator
         * @return {?}
         */
        function (loadingIndicator) {
            this.st.loadingIndicator = loadingIndicator;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(GridComponent.prototype, "loadingDelay", {
        /**
         * 延迟显示加载效果的时间（防止闪烁）
         */
        set: /**
         * 延迟显示加载效果的时间（防止闪烁）
         * @param {?} loadingDelay
         * @return {?}
         */
        function (loadingDelay) {
            this.st.loadingDelay = loadingDelay;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(GridComponent.prototype, "pi", {
        get: /**
         * @return {?}
         */
        function () {
            return this.st.pi;
        },
        /**
         * 当前页码
         */
        set: /**
         * 当前页码
         * @param {?} pi
         * @return {?}
         */
        function (pi) {
            this.st.pi = pi;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(GridComponent.prototype, "page", {
        get: /**
         * @return {?}
         */
        function () {
            return this.st.page;
        },
        /**
         *  分页器配置
         */
        set: /**
         *  分页器配置
         * @param {?} page
         * @return {?}
         */
        function (page) {
            this.st.page = page;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(GridComponent.prototype, "noResult", {
        get: /**
         * @return {?}
         */
        function () {
            return this.st.noResult;
        },
        /**
         * 无数据时显示内容
         */
        set: /**
         * 无数据时显示内容
         * @param {?} noResult
         * @return {?}
         */
        function (noResult) {
            this.st.noResult = noResult;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(GridComponent.prototype, "bordered", {
        get: /**
         * @return {?}
         */
        function () {
            return this.st.bordered;
        },
        /**
         * 是否显示边框
         */
        set: /**
         * 是否显示边框
         * @param {?} bordered
         * @return {?}
         */
        function (bordered) {
            this.st.bordered = bordered;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(GridComponent.prototype, "header", {
        /**
         * 表格标题
         */
        set: /**
         * 表格标题
         * @param {?} header
         * @return {?}
         */
        function (header) {
            this.st.header = header;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(GridComponent.prototype, "footer", {
        /**
         * 表格底部
         */
        set: /**
         * 表格底部
         * @param {?} footer
         * @return {?}
         */
        function (footer) {
            this.st.footer = footer;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(GridComponent.prototype, "bodyHeader", {
        /**
         * 表格顶部额外内容，一般用于添加合计行
         */
        set: /**
         * 表格顶部额外内容，一般用于添加合计行
         * @param {?} bodyHeader
         * @return {?}
         */
        function (bodyHeader) {
            this.st.bodyHeader = bodyHeader;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(GridComponent.prototype, "expandRowByClick", {
        set: /**
         * @param {?} expandRowByClick
         * @return {?}
         */
        function (expandRowByClick) {
            this.st.expandRowByClick = expandRowByClick;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(GridComponent.prototype, "expandAccordion", {
        set: /**
         * @param {?} expandAccordion
         * @return {?}
         */
        function (expandAccordion) {
            this.st.expandAccordion = expandAccordion;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(GridComponent.prototype, "expand", {
        set: /**
         * @param {?} expand
         * @return {?}
         */
        function (expand) {
            this.st.expand = expand;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(GridComponent.prototype, "filteredData", {
        get: /**
         * @return {?}
         */
        function () {
            return this.st.filteredData;
        },
        enumerable: true,
        configurable: true
    });
    /**
     * @return {?}
     */
    GridComponent.prototype.ngOnInit = /**
     * @return {?}
     */
    function () {
        var _this = this;
        setTimeout((/**
         * @return {?}
         */
        function () {
            _this.columns.forEach((/**
             * @param {?} col
             * @return {?}
             */
            function (col) {
                _this.restoreRender(col);
            }));
            _this.st.resetColumns();
        }), 100);
    };
    /**
     * @param {?=} options
     * @return {?}
     */
    GridComponent.prototype.resetColumns = /**
     * @param {?=} options
     * @return {?}
     */
    function (options) {
        this.st.resetColumns(options);
    };
    /**
     * 移除行
     * @param data STData | STData[] | number
     */
    /**
     * 移除行
     * @param {?} data STData | STData[] | number
     * @return {?}
     */
    GridComponent.prototype.removeRow = /**
     * 移除行
     * @param {?} data STData | STData[] | number
     * @return {?}
     */
    function (data) {
        this.st.removeRow(data);
    };
    /**
     * 清空所有数据
     */
    /**
     * 清空所有数据
     * @return {?}
     */
    GridComponent.prototype.clear = /**
     * 清空所有数据
     * @return {?}
     */
    function () {
        this.st.clear();
    };
    /**
     * @param {?=} queryparas
     * @return {?}
     */
    GridComponent.prototype.reload = /**
     * @param {?=} queryparas
     * @return {?}
     */
    function (queryparas) {
        this.st.loading = true;
        if (queryparas)
            this.st.req.params.form = queryparas;
        this.st.reload();
        this.st.cd();
    };
    /**
     * @param {?} index
     * @param {?} data
     * @return {?}
     */
    GridComponent.prototype.setRow = /**
     * @param {?} index
     * @param {?} data
     * @return {?}
     */
    function (index, data) {
        this.st.setRow(index, data);
    };
    /**
     * @param {?} event
     * @return {?}
     */
    GridComponent.prototype.reset = /**
     * @param {?} event
     * @return {?}
     */
    function (event) {
        this.loading = true;
        this.loadingChange.emit(true);
        this.st.reset(event);
        this.st.cd();
    };
    /**
     * @private
     * @param {?} values
     * @param {?} col
     * @param {?} list
     * @param {?=} rawData
     * @return {?}
     */
    GridComponent.prototype.customStatistical = /**
     * @private
     * @param {?} values
     * @param {?} col
     * @param {?} list
     * @param {?=} rawData
     * @return {?}
     */
    function (values, col, list, rawData) {
        if (rawData.code < 1)
            return { value: "", text: "" };
        if (rawData.page.statistical) {
            /** @type {?} */
            var key = col.index;
            if (key instanceof Array) {
                key = key[0];
            }
            if (rawData.page.statistical[key]) {
                return { value: rawData.page.statistical[key], text: "" };
            }
            return { value: "", text: "" };
        }
        return { value: "", text: "" };
    };
    // 导出excel
    // 导出excel
    /**
     * @param {?=} opt
     * @return {?}
     */
    GridComponent.prototype.export = 
    // 导出excel
    /**
     * @param {?=} opt
     * @return {?}
     */
    function (opt) {
        /** @type {?} */
        var data = this.st._data;
        this.st.export(data, opt);
    };
    /**
     * @return {?}
     */
    GridComponent.prototype.exportAll = /**
     * @return {?}
     */
    function () {
        var _this = this;
        this.loading = true;
        this.supexcel.export(this.columns, this._sname, this.st.req.params.form, this.st.total).then((/**
         * @return {?}
         */
        function () {
            _this.loading = false;
        }));
    };
    /**
     * @private
     * @param {?} item
     * @return {?}
     */
    GridComponent.prototype.restoreRender = /**
     * @private
     * @param {?} item
     * @return {?}
     */
    function (item) {
        if (item.renderTitle2) {
            item.__renderTitle = this.lbSource.getTitle(item.renderTitle2);
        }
        if (item.render2) {
            item.__render = this.lbSource.getRow(item.render2);
        }
    };
    GridComponent.decorators = [
        { type: Component, args: [{
                    selector: 'lb-grid',
                    template: "<st #st [data]=\"datas\" [req]=\"reqparas\" [loading]=\"loading\" [body]=\"bodyTpl\" [scroll]=\"scroll\" multiSort [ps]=\"ps\"\n    [res]=\"{reName :{list:'message.list',total:'page.COUNT',process: dataProcess}}\" [columns]=\"columns\">\n\n    <ng-template st-row=\"grhx\" let-item let-c=\"column\">\n        <a [routerLink]=\"grhxurl\" [queryParams]=\"item\">{{item[c.indexKey]}}</a>\n    </ng-template>\n\n    <ng-template #bodyTpl let-s>\n        <tr class=\"bg-grey-lighter\">\n            <ng-container *ngFor=\"let statis of statisticals\">\n                <td>\n                    <div *ngIf=\"statis.show&&s[statis.key]\">\n                        {{ s[statis.key].text}}\n                    </div>\n                </td>\n            </ng-container>\n        </tr>\n    </ng-template>\n</st>",
                    providers: [LbRowSource],
                    styles: [""]
                }] }
    ];
    /** @nocollapse */
    GridComponent.ctorParameters = function () { return [
        { type: SupDic },
        { type: SupExcel },
        { type: ServiceInfo },
        { type: LbRowSource }
    ]; };
    GridComponent.propDecorators = {
        grhxurl: [{ type: Input }],
        isGrhx: [{ type: Input }],
        columns: [{ type: Input, args: ['columns',] }],
        sname: [{ type: Input, args: ['sname',] }],
        queryparas: [{ type: Input }],
        ps: [{ type: Input }],
        loading: [{ type: Input }],
        loadingChange: [{ type: Output }],
        loadingIndicator: [{ type: Input }],
        loadingDelay: [{ type: Input }],
        scroll: [{ type: Input }],
        pi: [{ type: Input }],
        noResult: [{ type: Input }],
        bordered: [{ type: Input }],
        header: [{ type: Input }],
        footer: [{ type: Input }],
        bodyHeader: [{ type: Input }],
        expandRowByClick: [{ type: Input }],
        expandAccordion: [{ type: Input }],
        expand: [{ type: Input }],
        st: [{ type: ViewChild, args: ['st', { static: true },] }]
    };
    tslib_1.__decorate([
        InputBoolean(),
        tslib_1.__metadata("design:type", Object)
    ], GridComponent.prototype, "isGrhx", void 0);
    tslib_1.__decorate([
        InputNumber(),
        tslib_1.__metadata("design:type", Number),
        tslib_1.__metadata("design:paramtypes", [Number])
    ], GridComponent.prototype, "loadingDelay", null);
    tslib_1.__decorate([
        InputBoolean(),
        tslib_1.__metadata("design:type", Boolean),
        tslib_1.__metadata("design:paramtypes", [Boolean])
    ], GridComponent.prototype, "bordered", null);
    tslib_1.__decorate([
        InputBoolean(),
        tslib_1.__metadata("design:type", Boolean),
        tslib_1.__metadata("design:paramtypes", [Boolean])
    ], GridComponent.prototype, "expandRowByClick", null);
    tslib_1.__decorate([
        InputBoolean(),
        tslib_1.__metadata("design:type", Boolean),
        tslib_1.__metadata("design:paramtypes", [Boolean])
    ], GridComponent.prototype, "expandAccordion", null);
    tslib_1.__decorate([
        InputBoolean(),
        tslib_1.__metadata("design:type", TemplateRef),
        tslib_1.__metadata("design:paramtypes", [TemplateRef])
    ], GridComponent.prototype, "expand", null);
    return GridComponent;
}());
export { GridComponent };
if (false) {
    /** @type {?} */
    GridComponent.prototype.grhxurl;
    /** @type {?} */
    GridComponent.prototype.isGrhx;
    /**
     * 每页数量，当设置为 0 表示不分页，默认：10
     * @type {?}
     */
    GridComponent.prototype.ps;
    /** @type {?} */
    GridComponent.prototype.datas;
    /** @type {?} */
    GridComponent.prototype.statisticals;
    /** @type {?} */
    GridComponent.prototype._statcal;
    /** @type {?} */
    GridComponent.prototype.loading;
    /** @type {?} */
    GridComponent.prototype.loadingChange;
    /**
     * 横向或纵向支持滚动，也可用于指定滚动区域的宽高度：{ x: "300px", y: "300px" }
     * @type {?}
     */
    GridComponent.prototype.scroll;
    /** @type {?} */
    GridComponent.prototype._columns;
    /** @type {?} */
    GridComponent.prototype._sname;
    /** @type {?} */
    GridComponent.prototype._queryparas;
    /** @type {?} */
    GridComponent.prototype.st;
    /** @type {?} */
    GridComponent.prototype.reqparas;
    /** @type {?} */
    GridComponent.prototype.dataProcess;
    /**
     * @type {?}
     * @private
     */
    GridComponent.prototype.process;
    /**
     * @type {?}
     * @private
     */
    GridComponent.prototype.supdic;
    /**
     * @type {?}
     * @private
     */
    GridComponent.prototype.supexcel;
    /**
     * @type {?}
     * @private
     */
    GridComponent.prototype.sinfo;
    /**
     * @type {?}
     * @private
     */
    GridComponent.prototype.lbSource;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZ3JpZC5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9sYmYvIiwic291cmNlcyI6WyJsaWIvY29tcG9uZW50cy9ncmlkL2dyaWQuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7OztBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQVUsS0FBSyxFQUFFLE1BQU0sRUFBRSxZQUFZLEVBQUUsU0FBUyxFQUFFLFdBQVcsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUd2RyxPQUFPLEVBQ0wsV0FBVyxHQVNaLE1BQU0sWUFBWSxDQUFDO0FBQ3BCLE9BQU8sRUFBRSxNQUFNLEVBQUUsTUFBTSxxQkFBcUIsQ0FBQztBQUM3QyxPQUFPLEVBQUUsUUFBUSxFQUFFLE1BQU0sdUJBQXVCLENBQUM7QUFDakQsT0FBTyxFQUFFLFdBQVcsRUFBRSxNQUFNLFlBQVksQ0FBQztBQUN6QyxPQUFPLEVBQUUsV0FBVyxFQUFFLFlBQVksRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUMxRCxPQUFPLEVBQUUsV0FBVyxFQUFFLE1BQU0scUJBQXFCLENBQUM7QUFFbEQ7SUFtQkUsdUJBQW9CLE1BQWMsRUFBVSxRQUFrQixFQUFVLEtBQWtCLEVBQVUsUUFBcUI7UUFBckcsV0FBTSxHQUFOLE1BQU0sQ0FBUTtRQUFVLGFBQVEsR0FBUixRQUFRLENBQVU7UUFBVSxVQUFLLEdBQUwsS0FBSyxDQUFhO1FBQVUsYUFBUSxHQUFSLFFBQVEsQ0FBYTtRQUloSCxZQUFPLEdBQUcsQ0FBQyxZQUFZLENBQUMsQ0FBQTtRQUNSLFdBQU0sR0FBRyxJQUFJLENBQUM7Ozs7UUErRjlCLE9BQUUsR0FBRyxFQUFFLENBQUM7UUFFakIsVUFBSyxHQUFzQiwyQkFBMkIsQ0FBQztRQUV2RCxpQkFBWSxHQUFHLEVBQUUsQ0FBQztRQUNsQixhQUFRLEdBQUcsRUFBRSxDQUFDOztRQUdMLFlBQU8sR0FBRyxLQUFLLENBQUM7UUFDZixrQkFBYSxHQUFHLElBQUksWUFBWSxFQUFFLENBQUM7O1FBZ0g3QyxXQUFNLEdBQUcsRUFBRSxDQUFDO1FBSVosYUFBUSxHQUFHLEVBQUUsTUFBTSxFQUFFLEVBQUUsS0FBSyxFQUFFLElBQUksQ0FBQyxNQUFNLEVBQUUsSUFBSSxFQUFFLElBQUksQ0FBQyxXQUFXLEVBQUUsTUFBTSxFQUFFLElBQUksQ0FBQyxRQUFRLEVBQUUsRUFBRSxNQUFNLEVBQUUsTUFBTSxFQUFFLFNBQVMsRUFBRSxJQUFJLEVBQUUsQ0FBQztRQTBEdEgsWUFBTzs7OztRQUFHLFVBQUMsSUFBYztZQUMvQixPQUFPLElBQUksQ0FBQztRQUNkLENBQUMsRUFBQztJQTNSRixDQUFDO0lBVkQsc0JBQUksK0JBQUk7UUFIUjs7V0FFRzs7Ozs7UUFDSDtZQUNFLE9BQU8sSUFBSSxDQUFDLEVBQUUsQ0FBQyxLQUFLLENBQUM7UUFDdkIsQ0FBQzs7O09BQUE7SUFFRCxzQkFBSSxrQ0FBTzs7OztRQUFYO1lBQ0UsT0FBTyxJQUFJLENBQUMsRUFBRSxDQUFDO1FBQ2pCLENBQUM7OztPQUFBO0lBU0Qsc0JBQ0ksa0NBQU87Ozs7UUFpRVg7WUFDRSxPQUFPLElBQUksQ0FBQyxRQUFRLENBQUM7UUFDdkIsQ0FBQzs7Ozs7UUFwRUQsVUFDWSxPQUFtQjtZQUQvQixpQkFnRUM7WUE3REMsUUFBUTtZQUNSLElBQUksQ0FBQyxZQUFZLEdBQUcsRUFBRSxDQUFDO1lBRXZCLE9BQU8sQ0FBQyxPQUFPOzs7O1lBQUMsVUFBQSxHQUFHO2dCQUVqQixJQUFJLEdBQUcsQ0FBQyxNQUFNLEVBQUU7b0JBQ2QsR0FBRyxDQUFDLE9BQU8sR0FBRyxHQUFHLENBQUMsTUFBTSxDQUFDO29CQUN6QixHQUFHLENBQUMsTUFBTSxHQUFHLEVBQUUsQ0FBQztpQkFDakI7cUJBQU07b0JBQ0wsSUFBSSxLQUFJLENBQUMsTUFBTSxJQUFJLEdBQUcsQ0FBQyxLQUFLLEtBQUssUUFBUSxJQUFJLEtBQUksQ0FBQyxLQUFLLENBQUMsT0FBTyxFQUFFO3dCQUMvRCxHQUFHLENBQUMsTUFBTSxHQUFHLE1BQU0sQ0FBQztxQkFDckI7aUJBQ0Y7Z0JBRUQsSUFBSSxHQUFHLENBQUMsV0FBVyxFQUFFO29CQUNuQixHQUFHLENBQUMsWUFBWSxHQUFHLEdBQUcsQ0FBQyxXQUFXLENBQUM7b0JBQ25DLEdBQUcsQ0FBQyxXQUFXLEdBQUcsRUFBRSxDQUFDO2lCQUN0Qjs7b0JBRUcsR0FBRyxHQUFHLEdBQUcsQ0FBQyxLQUFLO2dCQUNuQixJQUFJLEdBQUcsWUFBWSxLQUFLLEVBQUU7b0JBQ3hCLEdBQUcsR0FBRyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUM7aUJBQ2Q7Z0JBRUQsR0FBRyxDQUFDLEdBQUcsR0FBRyxHQUFHLENBQUM7Z0JBRWQsU0FBUztnQkFDVCxJQUFJLEdBQUcsQ0FBQyxHQUFHLEVBQUU7b0JBQ1gsR0FBRyxDQUFDLE1BQU07Ozs7O29CQUFHLFVBQUMsQ0FBUyxFQUFFLENBQVcsSUFBSyxPQUFBLEtBQUksQ0FBQyxNQUFNLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQyxHQUFHLEVBQUUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxFQUF4QyxDQUF3QyxDQUFBLENBQUM7b0JBQ2xGLEdBQUcsQ0FBQyxNQUFNLEdBQUcsRUFBRSxLQUFLLEVBQUUsS0FBSSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsR0FBRyxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUE7aUJBQ3BEOzs7b0JBSUssR0FBRyxHQUFHLEdBQUcsQ0FBQyxXQUFXO2dCQUMzQixJQUFJLENBQUMsR0FBRyxFQUFFO29CQUVSLEtBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLEVBQUUsR0FBRyxLQUFBLEVBQUUsSUFBSSxFQUFFLEtBQUssRUFBRSxJQUFJLEVBQUUsRUFBRSxFQUFFLENBQUMsQ0FBQztvQkFDdkQsS0FBSSxDQUFDLFFBQVEsQ0FBQyxHQUFHLENBQUMsR0FBRyxFQUFFLENBQUM7b0JBQ3hCLE9BQU87aUJBQ1I7O29CQUNLLElBQUksc0JBQ1IsTUFBTSxFQUFFLENBQUMsRUFDVCxRQUFRLEVBQUUsSUFBSSxJQUNYLENBQUMsT0FBTyxHQUFHLEtBQUssUUFBUTtvQkFDekIsQ0FBQyxDQUFDLEVBQUUsSUFBSSxFQUFFLG1CQUFBLEdBQUcsRUFBcUIsRUFBRTtvQkFDcEMsQ0FBQyxDQUFDLENBQUMsbUJBQUEsR0FBRyxFQUFpQixDQUFDLENBQUMsQ0FDNUI7O29CQUVLLEtBQUssR0FBRyxJQUFJLENBQUMsSUFBSTtnQkFFdkIsSUFBSSxDQUFDLE1BQU0sR0FBRyxLQUFLLEtBQUssT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztnQkFFeEMsSUFBSSxDQUFDLElBQUksR0FBRyxLQUFJLENBQUMsaUJBQWlCLENBQUM7Z0JBQ25DLElBQUksQ0FBQyxRQUFRLEdBQUcsS0FBSyxDQUFDO2dCQUN0QixHQUFHLENBQUMsV0FBVyxHQUFHLElBQUksQ0FBQztnQkFDdkIsS0FBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsRUFBRSxHQUFHLEtBQUEsRUFBRSxJQUFJLEVBQUUsSUFBSSxFQUFFLElBQUksRUFBRSxLQUFLLEVBQUUsQ0FBQyxDQUFDO2dCQUN6RCxLQUFJLENBQUMsUUFBUSxDQUFDLEdBQUcsQ0FBQyxHQUFHLEtBQUssQ0FBQztZQUM3QixDQUFDLEVBQUMsQ0FBQztZQUVILElBQUksQ0FBQyxRQUFRLEdBQUcsT0FBTyxDQUFDO1FBQzFCLENBQUM7OztPQUFBO0lBUUQsc0JBQW9CLGdDQUFLOzs7OztRQUF6QixVQUEwQixLQUFhO1lBQ3JDLElBQUksQ0FBQyxNQUFNLEdBQUcsS0FBSyxDQUFDO1lBQ3BCLElBQUksQ0FBQyxRQUFRLENBQUMsTUFBTSxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDO1FBQzNDLENBQUM7OztPQUFBO0lBSUQsc0JBQ0kscUNBQVU7UUFIZCxPQUFPOzs7Ozs7UUFFUDtZQUVFLE9BQU8sSUFBSSxDQUFDLFdBQVcsQ0FBQztRQUMxQixDQUFDOzs7OztRQUVELFVBQWUsS0FBVTtZQUV2QixJQUFJLENBQUMsV0FBVyxHQUFHLEtBQUssQ0FBQztZQUN6QixJQUFJLENBQUMsUUFBUSxDQUFDLE1BQU0sQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQztRQUMvQyxDQUFDOzs7T0FOQTtJQXlCRCxzQkFBYSwyQ0FBZ0I7UUFIN0I7O1dBRUc7Ozs7OztRQUNILFVBQThCLGdCQUFtQztZQUMvRCxJQUFJLENBQUMsRUFBRSxDQUFDLGdCQUFnQixHQUFHLGdCQUFnQixDQUFDO1FBQzlDLENBQUM7OztPQUFBO0lBSXVCLHNCQUFJLHVDQUFZO1FBSHhDOztXQUVHOzs7Ozs7UUFDcUIsVUFBaUIsWUFBb0I7WUFDM0QsSUFBSSxDQUFDLEVBQUUsQ0FBQyxZQUFZLEdBQUcsWUFBWSxDQUFDO1FBQ3RDLENBQUM7OztPQUFBO0lBYUQsc0JBQWEsNkJBQUU7Ozs7UUFHZjtZQUNFLE9BQU8sSUFBSSxDQUFDLEVBQUUsQ0FBQyxFQUFFLENBQUM7UUFDcEIsQ0FBQztRQVJEOztXQUVHOzs7Ozs7UUFDSCxVQUFnQixFQUFVO1lBQ3hCLElBQUksQ0FBQyxFQUFFLENBQUMsRUFBRSxHQUFHLEVBQUUsQ0FBQztRQUNsQixDQUFDOzs7T0FBQTtJQVFELHNCQUFJLCtCQUFJOzs7O1FBS1I7WUFDRSxPQUFPLElBQUksQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDO1FBQ3RCLENBQUM7UUFWRDs7V0FFRzs7Ozs7O1FBQ0gsVUFBUyxJQUFZO1lBRW5CLElBQUksQ0FBQyxFQUFFLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQztRQUN0QixDQUFDOzs7T0FBQTtJQVNELHNCQUFhLG1DQUFROzs7O1FBSXJCO1lBQ0UsT0FBTyxJQUFJLENBQUMsRUFBRSxDQUFDLFFBQVEsQ0FBQztRQUMxQixDQUFDO1FBVEQ7O1dBRUc7Ozs7OztRQUNILFVBQXNCLFFBQW9DO1lBQ3hELElBQUksQ0FBQyxFQUFFLENBQUMsUUFBUSxHQUFHLFFBQVEsQ0FBQztRQUM5QixDQUFDOzs7T0FBQTtJQVN3QixzQkFBSSxtQ0FBUTs7OztRQUdyQztZQUNFLE9BQU8sSUFBSSxDQUFDLEVBQUUsQ0FBQyxRQUFRLENBQUM7UUFDMUIsQ0FBQztRQVJEOztXQUVHOzs7Ozs7UUFDc0IsVUFBYSxRQUFpQjtZQUNyRCxJQUFJLENBQUMsRUFBRSxDQUFDLFFBQVEsR0FBRyxRQUFRLENBQUM7UUFDOUIsQ0FBQzs7O09BQUE7SUFRRCxzQkFBYSxpQ0FBTTtRQUhuQjs7V0FFRzs7Ozs7O1FBQ0gsVUFBb0IsTUFBa0M7WUFDcEQsSUFBSSxDQUFDLEVBQUUsQ0FBQyxNQUFNLEdBQUcsTUFBTSxDQUFBO1FBQ3pCLENBQUM7OztPQUFBO0lBS0Qsc0JBQWEsaUNBQU07UUFIbkI7O1dBRUc7Ozs7OztRQUNILFVBQW9CLE1BQWtDO1lBQ3BELElBQUksQ0FBQyxFQUFFLENBQUMsTUFBTSxHQUFHLE1BQU0sQ0FBQTtRQUN6QixDQUFDOzs7T0FBQTtJQUtELHNCQUFhLHFDQUFVO1FBSHZCOztXQUVHOzs7Ozs7UUFDSCxVQUF3QixVQUE2QztZQUNuRSxJQUFJLENBQUMsRUFBRSxDQUFDLFVBQVUsR0FBRyxVQUFVLENBQUM7UUFFbEMsQ0FBQzs7O09BQUE7SUFHd0Isc0JBQUksMkNBQWdCOzs7OztRQUFwQixVQUFxQixnQkFBeUI7WUFDckUsSUFBSSxDQUFDLEVBQUUsQ0FBQyxnQkFBZ0IsR0FBRyxnQkFBZ0IsQ0FBQztRQUM5QyxDQUFDOzs7T0FBQTtJQUV3QixzQkFBSSwwQ0FBZTs7Ozs7UUFBbkIsVUFBb0IsZUFBd0I7WUFDbkUsSUFBSSxDQUFDLEVBQUUsQ0FBQyxlQUFlLEdBQUcsZUFBZSxDQUFDO1FBQzVDLENBQUM7OztPQUFBO0lBQ3dCLHNCQUFJLGlDQUFNOzs7OztRQUFWLFVBQVcsTUFHbEM7WUFDQSxJQUFJLENBQUMsRUFBRSxDQUFDLE1BQU0sR0FBRyxNQUFNLENBQUM7UUFDMUIsQ0FBQzs7O09BQUE7SUFFRCxzQkFBSSx1Q0FBWTs7OztRQUFoQjtZQUNFLE9BQU8sSUFBSSxDQUFDLEVBQUUsQ0FBQyxZQUFZLENBQUM7UUFDOUIsQ0FBQzs7O09BQUE7Ozs7SUFlRCxnQ0FBUTs7O0lBQVI7UUFBQSxpQkFXQztRQVZDLFVBQVU7OztRQUFDO1lBRVQsS0FBSSxDQUFDLE9BQU8sQ0FBQyxPQUFPOzs7O1lBQUMsVUFBQSxHQUFHO2dCQUN0QixLQUFJLENBQUMsYUFBYSxDQUFDLEdBQUcsQ0FBQyxDQUFDO1lBQzFCLENBQUMsRUFBQyxDQUFBO1lBRUYsS0FBSSxDQUFDLEVBQUUsQ0FBQyxZQUFZLEVBQUUsQ0FBQztRQUV6QixDQUFDLEdBQUUsR0FBRyxDQUFDLENBQUM7SUFFVixDQUFDOzs7OztJQUVELG9DQUFZOzs7O0lBQVosVUFBYSxPQUE4QjtRQUN6QyxJQUFJLENBQUMsRUFBRSxDQUFDLFlBQVksQ0FBQyxPQUFPLENBQUMsQ0FBQztJQUNoQyxDQUFDO0lBQ0Q7OztPQUdHOzs7Ozs7SUFDSCxpQ0FBUzs7Ozs7SUFBVCxVQUFVLElBQUk7UUFDWixJQUFJLENBQUMsRUFBRSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsQ0FBQztJQUMxQixDQUFDO0lBRUQ7O09BRUc7Ozs7O0lBQ0gsNkJBQUs7Ozs7SUFBTDtRQUNFLElBQUksQ0FBQyxFQUFFLENBQUMsS0FBSyxFQUFFLENBQUM7SUFDbEIsQ0FBQzs7Ozs7SUFJRCw4QkFBTTs7OztJQUFOLFVBQU8sVUFBZ0I7UUFFckIsSUFBSSxDQUFDLEVBQUUsQ0FBQyxPQUFPLEdBQUcsSUFBSSxDQUFDO1FBQ3ZCLElBQUksVUFBVTtZQUNaLElBQUksQ0FBQyxFQUFFLENBQUMsR0FBRyxDQUFDLE1BQU0sQ0FBQyxJQUFJLEdBQUcsVUFBVSxDQUFDO1FBQ3ZDLElBQUksQ0FBQyxFQUFFLENBQUMsTUFBTSxFQUFFLENBQUM7UUFDakIsSUFBSSxDQUFDLEVBQUUsQ0FBQyxFQUFFLEVBQUUsQ0FBQztJQUVmLENBQUM7Ozs7OztJQUVELDhCQUFNOzs7OztJQUFOLFVBQU8sS0FBYSxFQUFFLElBQVk7UUFDaEMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxNQUFNLENBQUMsS0FBSyxFQUFFLElBQUksQ0FBQyxDQUFDO0lBQzlCLENBQUM7Ozs7O0lBRUQsNkJBQUs7Ozs7SUFBTCxVQUFNLEtBQVU7UUFDZCxJQUFJLENBQUMsT0FBTyxHQUFHLElBQUksQ0FBQztRQUNwQixJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUM5QixJQUFJLENBQUMsRUFBRSxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUNyQixJQUFJLENBQUMsRUFBRSxDQUFDLEVBQUUsRUFBRSxDQUFDO0lBQ2YsQ0FBQzs7Ozs7Ozs7O0lBTU8seUNBQWlCOzs7Ozs7OztJQUF6QixVQUEwQixNQUFnQixFQUFFLEdBQWEsRUFBRSxJQUFjLEVBQUUsT0FBYTtRQUV0RixJQUFJLE9BQU8sQ0FBQyxJQUFJLEdBQUcsQ0FBQztZQUFFLE9BQU8sRUFBRSxLQUFLLEVBQUUsRUFBRSxFQUFFLElBQUksRUFBRSxFQUFFLEVBQUUsQ0FBQztRQUVyRCxJQUFJLE9BQU8sQ0FBQyxJQUFJLENBQUMsV0FBVyxFQUFFOztnQkFFeEIsR0FBRyxHQUFHLEdBQUcsQ0FBQyxLQUFLO1lBQ25CLElBQUksR0FBRyxZQUFZLEtBQUssRUFBRTtnQkFDeEIsR0FBRyxHQUFHLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQzthQUNkO1lBR0QsSUFBSSxPQUFPLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxHQUFHLENBQUMsRUFBRTtnQkFFakMsT0FBTyxFQUFFLEtBQUssRUFBRSxPQUFPLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxHQUFHLENBQUMsRUFBRSxJQUFJLEVBQUUsRUFBRSxFQUFFLENBQUM7YUFDM0Q7WUFFRCxPQUFPLEVBQUUsS0FBSyxFQUFFLEVBQUUsRUFBRSxJQUFJLEVBQUUsRUFBRSxFQUFFLENBQUM7U0FDaEM7UUFFRCxPQUFPLEVBQUUsS0FBSyxFQUFFLEVBQUUsRUFBRSxJQUFJLEVBQUUsRUFBRSxFQUFFLENBQUM7SUFFakMsQ0FBQztJQUVELFVBQVU7Ozs7OztJQUNWLDhCQUFNOzs7Ozs7SUFBTixVQUFPLEdBQXFCOztZQUVwQixJQUFJLEdBQUcsSUFBSSxDQUFDLEVBQUUsQ0FBQyxLQUFLO1FBQzFCLElBQUksQ0FBQyxFQUFFLENBQUMsTUFBTSxDQUFDLElBQUksRUFBRSxHQUFHLENBQUMsQ0FBQztJQUM1QixDQUFDOzs7O0lBRUQsaUNBQVM7OztJQUFUO1FBQUEsaUJBUUM7UUFOQyxJQUFJLENBQUMsT0FBTyxHQUFHLElBQUksQ0FBQztRQUVwQixJQUFJLENBQUMsUUFBUSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsT0FBTyxFQUFFLElBQUksQ0FBQyxNQUFNLEVBQUUsSUFBSSxDQUFDLEVBQUUsQ0FBQyxHQUFHLENBQUMsTUFBTSxDQUFDLElBQUksRUFBRSxJQUFJLENBQUMsRUFBRSxDQUFDLEtBQUssQ0FBQyxDQUFDLElBQUk7OztRQUFDO1lBQzNGLEtBQUksQ0FBQyxPQUFPLEdBQUcsS0FBSyxDQUFDO1FBQ3ZCLENBQUMsRUFBQyxDQUFDO0lBRUwsQ0FBQzs7Ozs7O0lBRU8scUNBQWE7Ozs7O0lBQXJCLFVBQXNCLElBQWM7UUFDbEMsSUFBSSxJQUFJLENBQUMsWUFBWSxFQUFFO1lBQ3JCLElBQUksQ0FBQyxhQUFhLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxDQUFDO1NBRWhFO1FBQ0QsSUFBSSxJQUFJLENBQUMsT0FBTyxFQUFFO1lBQ2hCLElBQUksQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDO1NBRXBEO0lBRUgsQ0FBQzs7Z0JBcldGLFNBQVMsU0FBQztvQkFDVCxRQUFRLEVBQUUsU0FBUztvQkFDbkIsK3lCQUFvQztvQkFFcEMsU0FBUyxFQUFFLENBQUMsV0FBVyxDQUFDOztpQkFDekI7Ozs7Z0JBWFEsTUFBTTtnQkFDTixRQUFRO2dCQUdSLFdBQVc7Z0JBRlgsV0FBVzs7OzBCQTJCakIsS0FBSzt5QkFDTCxLQUFLOzBCQUVMLEtBQUssU0FBQyxTQUFTO3dCQXdFZixLQUFLLFNBQUMsT0FBTzs2QkFPYixLQUFLO3FCQWNMLEtBQUs7MEJBUUwsS0FBSztnQ0FDTCxNQUFNO21DQUtOLEtBQUs7K0JBTUwsS0FBSzt5QkFPTCxLQUFLO3FCQVFMLEtBQUs7MkJBc0JMLEtBQUs7MkJBV0wsS0FBSzt5QkFVTCxLQUFLO3lCQU9MLEtBQUs7NkJBT0wsS0FBSzttQ0FNTCxLQUFLO2tDQUlMLEtBQUs7eUJBR0wsS0FBSztxQkFrQkwsU0FBUyxTQUFDLElBQUksRUFBRSxFQUFFLE1BQU0sRUFBRSxJQUFJLEVBQUU7O0lBMU5SO1FBQWYsWUFBWSxFQUFFOztpREFBZTtJQW1IZjtRQUFkLFdBQVcsRUFBRTs7O3FEQUV0QjtJQThDd0I7UUFBZixZQUFZLEVBQUU7OztpREFFdkI7SUE0QndCO1FBQWYsWUFBWSxFQUFFOzs7eURBRXZCO0lBRXdCO1FBQWYsWUFBWSxFQUFFOzs7d0RBRXZCO0lBQ3dCO1FBQWYsWUFBWSxFQUFFOzBDQUFvQixXQUFXO2lEQUFYLFdBQVc7K0NBS3REO0lBa0lILG9CQUFDO0NBQUEsQUF2V0QsSUF1V0M7U0FqV1ksYUFBYTs7O0lBaUJ4QixnQ0FBaUM7O0lBQ2pDLCtCQUF1Qzs7Ozs7SUErRnZDLDJCQUFpQjs7SUFFakIsOEJBQXVEOztJQUV2RCxxQ0FBa0I7O0lBQ2xCLGlDQUFjOztJQUdkLGdDQUF5Qjs7SUFDekIsc0NBQTZDOzs7OztJQWtCN0MsK0JBR0U7O0lBd0ZGLGlDQUFxQjs7SUFHckIsK0JBQVk7O0lBQ1osb0NBQWlCOztJQUNqQiwyQkFBbUQ7O0lBRW5ELGlDQUE4SDs7SUFFOUgsb0NBQTBEOzs7OztJQXdEMUQsZ0NBRUU7Ozs7O0lBN1JVLCtCQUFzQjs7Ozs7SUFBRSxpQ0FBMEI7Ozs7O0lBQUUsOEJBQTBCOzs7OztJQUFFLGlDQUE2QiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0LCBJbnB1dCwgT3V0cHV0LCBFdmVudEVtaXR0ZXIsIFZpZXdDaGlsZCwgVGVtcGxhdGVSZWYgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcblxuXG5pbXBvcnQge1xuICBTVENvbXBvbmVudCxcbiAgU1RDb2x1bW4sXG4gIFNURGF0YSxcbiAgU1RFeHBvcnRPcHRpb25zLFxuICBTVFN0YXRpc3RpY2FsLFxuICBTVFN0YXRpc3RpY2FsVHlwZSxcbiAgU1RQYWdlLFxuICBTVFN0YXRpc3RpY2FsUmVzdWx0cyxcbiAgU1RSZXNldENvbHVtbnNPcHRpb24sXG59IGZyb20gJ0BkZWxvbi9hYmMnO1xuaW1wb3J0IHsgU3VwRGljIH0gZnJvbSAnLi4vLi4vbGJsaWJzL1N1cERpYyc7XG5pbXBvcnQgeyBTdXBFeGNlbCB9IGZyb20gJy4uLy4uL2xibGlicy9TdXBFeGNlbCc7XG5pbXBvcnQgeyBMYlJvd1NvdXJjZSB9IGZyb20gJy4vTGJDb2x1bW4nO1xuaW1wb3J0IHsgSW5wdXROdW1iZXIsIElucHV0Qm9vbGVhbiB9IGZyb20gJ25nLXpvcnJvLWFudGQnO1xuaW1wb3J0IHsgU2VydmljZUluZm8gfSBmcm9tICcuLi8uLi9TZXJ2aWNlQ29uZmlnJztcblxuQENvbXBvbmVudCh7XG4gIHNlbGVjdG9yOiAnbGItZ3JpZCcsXG4gIHRlbXBsYXRlVXJsOiAnLi9ncmlkLmNvbXBvbmVudC5odG1sJyxcbiAgc3R5bGVVcmxzOiBbJy4vZ3JpZC5jb21wb25lbnQuY3NzJ10sXG4gIHByb3ZpZGVyczogW0xiUm93U291cmNlXVxufSlcbmV4cG9ydCBjbGFzcyBHcmlkQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcblxuICAvKipcbiAgICog6KGo5qC85pWw5YC8XG4gICAqL1xuICBnZXQgZGF0YSgpOiBTVERhdGFbXSB7XG4gICAgcmV0dXJuIHRoaXMuc3QuX2RhdGE7XG4gIH1cblxuICBnZXQgaW5uZXJTdCgpOiBTVENvbXBvbmVudCB7XG4gICAgcmV0dXJuIHRoaXMuc3Q7XG4gIH1cblxuICBjb25zdHJ1Y3Rvcihwcml2YXRlIHN1cGRpYzogU3VwRGljLCBwcml2YXRlIHN1cGV4Y2VsOiBTdXBFeGNlbCwgcHJpdmF0ZSBzaW5mbzogU2VydmljZUluZm8sIHByaXZhdGUgbGJTb3VyY2U6IExiUm93U291cmNlKSB7XG5cbiAgfVxuXG4gIEBJbnB1dCgpIGdyaHh1cmwgPSBbJy95YmpnL2dyaHgnXVxuICBASW5wdXQoKSBASW5wdXRCb29sZWFuKCkgaXNHcmh4ID0gdHJ1ZTtcblxuICBASW5wdXQoJ2NvbHVtbnMnKVxuICBzZXQgY29sdW1ucyhjb2x1bW5zOiBTVENvbHVtbltdKSB7XG5cbiAgICAvLyDnu5/orqHnsbvkv6Hmga9cbiAgICB0aGlzLnN0YXRpc3RpY2FscyA9IFtdO1xuXG4gICAgY29sdW1ucy5mb3JFYWNoKGNvbCA9PiB7XG5cbiAgICAgIGlmIChjb2wucmVuZGVyKSB7XG4gICAgICAgIGNvbC5yZW5kZXIyID0gY29sLnJlbmRlcjtcbiAgICAgICAgY29sLnJlbmRlciA9ICcnO1xuICAgICAgfSBlbHNlIHtcbiAgICAgICAgaWYgKHRoaXMuaXNHcmh4ICYmIGNvbC5pbmRleCA9PT0gJ0FBQzAwMycgJiYgdGhpcy5zaW5mby5pc0FkbWluKSB7XG4gICAgICAgICAgY29sLnJlbmRlciA9ICdncmh4JztcbiAgICAgICAgfVxuICAgICAgfVxuXG4gICAgICBpZiAoY29sLnJlbmRlclRpdGxlKSB7XG4gICAgICAgIGNvbC5yZW5kZXJUaXRsZTIgPSBjb2wucmVuZGVyVGl0bGU7XG4gICAgICAgIGNvbC5yZW5kZXJUaXRsZSA9ICcnO1xuICAgICAgfVxuXG4gICAgICBsZXQga2V5ID0gY29sLmluZGV4O1xuICAgICAgaWYgKGtleSBpbnN0YW5jZW9mIEFycmF5KSB7XG4gICAgICAgIGtleSA9IGtleVswXTtcbiAgICAgIH1cblxuICAgICAgY29sLmtleSA9IGtleTtcblxuICAgICAgLy8g5re75Yqg5a2X5YW45L+h5oGvXG4gICAgICBpZiAoY29sLmRpYykge1xuICAgICAgICBjb2wuZm9ybWF0ID0gKGE6IFNURGF0YSwgYjogU1RDb2x1bW4pID0+IHRoaXMuc3VwZGljLmdldGRpY0xhYmVsKGIuZGljLCBhW2Iua2V5XSk7XG4gICAgICAgIGNvbC5maWx0ZXIgPSB7IG1lbnVzOiB0aGlzLnN1cGRpYy5nZXREaWMoY29sLmRpYykgfVxuICAgICAgfVxuXG5cbiAgICAgIC8vIOa3u+WKoOe7n+iuoeexu+Wei+S/oeaBr1xuICAgICAgY29uc3QgdmFsID0gY29sLnN0YXRpc3RpY2FsO1xuICAgICAgaWYgKCF2YWwpIHtcblxuICAgICAgICB0aGlzLnN0YXRpc3RpY2Fscy5wdXNoKHsga2V5LCBzaG93OiBmYWxzZSwgdHlwZTogXCJcIiB9KTtcbiAgICAgICAgdGhpcy5fc3RhdGNhbFtrZXldID0gXCJcIjtcbiAgICAgICAgcmV0dXJuO1xuICAgICAgfVxuICAgICAgY29uc3QgaXRlbTogU1RTdGF0aXN0aWNhbCA9IHtcbiAgICAgICAgZGlnaXRzOiAyLFxuICAgICAgICBjdXJyZW5jeTogbnVsbCxcbiAgICAgICAgLi4uKHR5cGVvZiB2YWwgPT09ICdzdHJpbmcnXG4gICAgICAgICAgPyB7IHR5cGU6IHZhbCBhcyBTVFN0YXRpc3RpY2FsVHlwZSB9XG4gICAgICAgICAgOiAodmFsIGFzIFNUU3RhdGlzdGljYWwpKSxcbiAgICAgIH07XG5cbiAgICAgIGNvbnN0IHN0Y2FsID0gaXRlbS50eXBlO1xuXG4gICAgICBpdGVtLmRpZ2l0cyA9IHN0Y2FsID09PSBcImNvdW50XCIgPyAwIDogMjtcblxuICAgICAgaXRlbS50eXBlID0gdGhpcy5jdXN0b21TdGF0aXN0aWNhbDtcbiAgICAgIGl0ZW0uY3VycmVuY3kgPSBmYWxzZTtcbiAgICAgIGNvbC5zdGF0aXN0aWNhbCA9IGl0ZW07XG4gICAgICB0aGlzLnN0YXRpc3RpY2Fscy5wdXNoKHsga2V5LCBzaG93OiB0cnVlLCB0eXBlOiBzdGNhbCB9KTtcbiAgICAgIHRoaXMuX3N0YXRjYWxba2V5XSA9IHN0Y2FsO1xuICAgIH0pO1xuXG4gICAgdGhpcy5fY29sdW1ucyA9IGNvbHVtbnM7XG4gIH1cblxuICBnZXQgY29sdW1ucygpOiBTVENvbHVtbltdIHtcbiAgICByZXR1cm4gdGhpcy5fY29sdW1ucztcbiAgfVxuXG5cblxuICBASW5wdXQoJ3NuYW1lJykgc2V0IHNuYW1lKHNuYW1lOiBzdHJpbmcpIHtcbiAgICB0aGlzLl9zbmFtZSA9IHNuYW1lO1xuICAgIHRoaXMucmVxcGFyYXMucGFyYW1zLnNuYW1lID0gdGhpcy5fc25hbWU7XG4gIH1cblxuICAvLyDmn6Xor6Llj4LmlbBcblxuICBASW5wdXQoKVxuICBnZXQgcXVlcnlwYXJhcygpIHtcbiAgICByZXR1cm4gdGhpcy5fcXVlcnlwYXJhcztcbiAgfVxuXG4gIHNldCBxdWVyeXBhcmFzKHZhbHVlOiBhbnkpIHtcblxuICAgIHRoaXMuX3F1ZXJ5cGFyYXMgPSB2YWx1ZTtcbiAgICB0aGlzLnJlcXBhcmFzLnBhcmFtcy5mb3JtID0gdGhpcy5fcXVlcnlwYXJhcztcbiAgfVxuXG4gIC8qKlxuICAgKiDmr4/pobXmlbDph4/vvIzlvZPorr7nva7kuLogMCDooajnpLrkuI3liIbpobXvvIzpu5jorqTvvJoxMFxuICAgKi9cbiAgQElucHV0KCkgcHMgPSAxMDtcblxuICBkYXRhczogc3RyaW5nIHwgU1REYXRhW10gPSAnTW9ocnNzRXhJbnRlcmZhY2UvZ3JpZHN2cic7XG5cbiAgc3RhdGlzdGljYWxzID0gW107XG4gIF9zdGF0Y2FsID0ge307XG5cbiAgLy8g5piv5ZCm5Yqg6L295LitXG4gIEBJbnB1dCgpIGxvYWRpbmcgPSBmYWxzZTtcbiAgQE91dHB1dCgpIGxvYWRpbmdDaGFuZ2UgPSBuZXcgRXZlbnRFbWl0dGVyKCk7XG5cbiAgLyoqXG4gICAqIOWKoOi9veaMh+ekuuesplxuICAgKi9cbiAgQElucHV0KCkgc2V0IGxvYWRpbmdJbmRpY2F0b3IobG9hZGluZ0luZGljYXRvcjogVGVtcGxhdGVSZWY8dm9pZD4pIHtcbiAgICB0aGlzLnN0LmxvYWRpbmdJbmRpY2F0b3IgPSBsb2FkaW5nSW5kaWNhdG9yO1xuICB9XG4gIC8qKlxuICAgKiDlu7bov5/mmL7npLrliqDovb3mlYjmnpznmoTml7bpl7TvvIjpmLLmraLpl6rng4HvvIlcbiAgICovXG4gIEBJbnB1dCgpIEBJbnB1dE51bWJlcigpIHNldCBsb2FkaW5nRGVsYXkobG9hZGluZ0RlbGF5OiBudW1iZXIpIHtcbiAgICB0aGlzLnN0LmxvYWRpbmdEZWxheSA9IGxvYWRpbmdEZWxheTtcbiAgfVxuXG4gIC8qKlxuICAgKiDmqKrlkJHmiJbnurXlkJHmlK/mjIHmu5rliqjvvIzkuZ/lj6/nlKjkuo7mjIflrprmu5rliqjljLrln5/nmoTlrr3pq5jluqbvvJp7IHg6IFwiMzAwcHhcIiwgeTogXCIzMDBweFwiIH1cbiAgICovXG4gIEBJbnB1dCgpIHNjcm9sbDoge1xuICAgIHk/OiBzdHJpbmc7XG4gICAgeD86IHN0cmluZztcbiAgfTtcblxuICAvKipcbiAgICog5b2T5YmN6aG156CBXG4gICAqL1xuICBASW5wdXQoKSBzZXQgcGkocGk6IG51bWJlcikge1xuICAgIHRoaXMuc3QucGkgPSBwaTtcbiAgfVxuICBnZXQgcGkoKTogbnVtYmVyIHtcbiAgICByZXR1cm4gdGhpcy5zdC5waTtcbiAgfVxuXG4gIC8qKlxuICAgKiAg5YiG6aG15Zmo6YWN572uXG4gICAqL1xuICBzZXQgcGFnZShwYWdlOiBTVFBhZ2UpIHtcblxuICAgIHRoaXMuc3QucGFnZSA9IHBhZ2U7XG4gIH1cblxuICBnZXQgcGFnZSgpOiBTVFBhZ2Uge1xuICAgIHJldHVybiB0aGlzLnN0LnBhZ2U7XG4gIH1cblxuICAvKipcbiAgICog5peg5pWw5o2u5pe25pi+56S65YaF5a65XG4gICAqL1xuICBASW5wdXQoKSBzZXQgbm9SZXN1bHQobm9SZXN1bHQ6IHN0cmluZyB8IFRlbXBsYXRlUmVmPHZvaWQ+KSB7XG4gICAgdGhpcy5zdC5ub1Jlc3VsdCA9IG5vUmVzdWx0O1xuICB9XG5cbiAgZ2V0IG5vUmVzdWx0KCk6IHN0cmluZyB8IFRlbXBsYXRlUmVmPHZvaWQ+IHtcbiAgICByZXR1cm4gdGhpcy5zdC5ub1Jlc3VsdDtcbiAgfVxuXG4gIC8qKlxuICAgKiDmmK/lkKbmmL7npLrovrnmoYZcbiAgICovXG4gIEBJbnB1dCgpIEBJbnB1dEJvb2xlYW4oKSBzZXQgYm9yZGVyZWQoYm9yZGVyZWQ6IGJvb2xlYW4pIHtcbiAgICB0aGlzLnN0LmJvcmRlcmVkID0gYm9yZGVyZWQ7XG4gIH1cbiAgZ2V0IGJvcmRlcmVkKCk6IGJvb2xlYW4ge1xuICAgIHJldHVybiB0aGlzLnN0LmJvcmRlcmVkO1xuICB9XG5cbiAgLyoqXG4gICAqIOihqOagvOagh+mimFxuICAgKi9cbiAgQElucHV0KCkgc2V0IGhlYWRlcihoZWFkZXI6IHN0cmluZyB8IFRlbXBsYXRlUmVmPHZvaWQ+KSB7XG4gICAgdGhpcy5zdC5oZWFkZXIgPSBoZWFkZXJcbiAgfVxuXG4gIC8qKlxuICAgKiDooajmoLzlupXpg6hcbiAgICovXG4gIEBJbnB1dCgpIHNldCBmb290ZXIoZm9vdGVyOiBzdHJpbmcgfCBUZW1wbGF0ZVJlZjx2b2lkPikge1xuICAgIHRoaXMuc3QuZm9vdGVyID0gZm9vdGVyXG4gIH1cblxuICAvKipcbiAgICog6KGo5qC86aG26YOo6aKd5aSW5YaF5a6577yM5LiA6Iis55So5LqO5re75Yqg5ZCI6K6h6KGMXG4gICAqL1xuICBASW5wdXQoKSBzZXQgYm9keUhlYWRlcihib2R5SGVhZGVyOiBUZW1wbGF0ZVJlZjxTVFN0YXRpc3RpY2FsUmVzdWx0cz4pIHtcbiAgICB0aGlzLnN0LmJvZHlIZWFkZXIgPSBib2R5SGVhZGVyO1xuXG4gIH1cblxuXG4gIEBJbnB1dCgpIEBJbnB1dEJvb2xlYW4oKSBzZXQgZXhwYW5kUm93QnlDbGljayhleHBhbmRSb3dCeUNsaWNrOiBib29sZWFuKSB7XG4gICAgdGhpcy5zdC5leHBhbmRSb3dCeUNsaWNrID0gZXhwYW5kUm93QnlDbGljaztcbiAgfVxuXG4gIEBJbnB1dCgpIEBJbnB1dEJvb2xlYW4oKSBzZXQgZXhwYW5kQWNjb3JkaW9uKGV4cGFuZEFjY29yZGlvbjogYm9vbGVhbikge1xuICAgIHRoaXMuc3QuZXhwYW5kQWNjb3JkaW9uID0gZXhwYW5kQWNjb3JkaW9uO1xuICB9XG4gIEBJbnB1dCgpIEBJbnB1dEJvb2xlYW4oKSBzZXQgZXhwYW5kKGV4cGFuZDogVGVtcGxhdGVSZWY8e1xuICAgICRpbXBsaWNpdDoge307XG4gICAgY29sdW1uOiBTVENvbHVtbjtcbiAgfT4pIHtcbiAgICB0aGlzLnN0LmV4cGFuZCA9IGV4cGFuZDtcbiAgfVxuXG4gIGdldCBmaWx0ZXJlZERhdGEoKTogUHJvbWlzZTxTVERhdGFbXT4ge1xuICAgIHJldHVybiB0aGlzLnN0LmZpbHRlcmVkRGF0YTtcbiAgfVxuXG5cbiAgLy8g5YiX5L+h5oGvXG4gIF9jb2x1bW5zOiBTVENvbHVtbltdO1xuXG4gIC8vIOacjeWKoUlEXG4gIF9zbmFtZSA9ICcnO1xuICBfcXVlcnlwYXJhczogYW55O1xuICBAVmlld0NoaWxkKCdzdCcsIHsgc3RhdGljOiB0cnVlIH0pIHN0OiBTVENvbXBvbmVudDtcblxuICByZXFwYXJhcyA9IHsgcGFyYW1zOiB7IHNuYW1lOiB0aGlzLl9zbmFtZSwgZm9ybTogdGhpcy5fcXVlcnlwYXJhcywgc3RhY2FsOiB0aGlzLl9zdGF0Y2FsIH0sIG1ldGhvZDogJ3Bvc3QnLCBhbGxJbkJvZHk6IHRydWUgfTtcblxuICBkYXRhUHJvY2Vzcz86IChkYXRhOiBTVERhdGFbXSwgcmF3RGF0YT86IGFueSkgPT4gU1REYXRhW107XG5cbiAgbmdPbkluaXQoKSB7XG4gICAgc2V0VGltZW91dCgoKSA9PiB7XG5cbiAgICAgIHRoaXMuY29sdW1ucy5mb3JFYWNoKGNvbCA9PiB7XG4gICAgICAgIHRoaXMucmVzdG9yZVJlbmRlcihjb2wpO1xuICAgICAgfSlcblxuICAgICAgdGhpcy5zdC5yZXNldENvbHVtbnMoKTtcblxuICAgIH0sIDEwMCk7XG5cbiAgfVxuXG4gIHJlc2V0Q29sdW1ucyhvcHRpb25zPzogU1RSZXNldENvbHVtbnNPcHRpb24pIHtcbiAgICB0aGlzLnN0LnJlc2V0Q29sdW1ucyhvcHRpb25zKTtcbiAgfVxuICAvKipcbiAgICog56e76Zmk6KGMIFxuICAgKiBAcGFyYW0gZGF0YSBTVERhdGEgfCBTVERhdGFbXSB8IG51bWJlclxuICAgKi9cbiAgcmVtb3ZlUm93KGRhdGEpIHtcbiAgICB0aGlzLnN0LnJlbW92ZVJvdyhkYXRhKTtcbiAgfVxuXG4gIC8qKlxuICAgKiDmuIXnqbrmiYDmnInmlbDmja5cbiAgICovXG4gIGNsZWFyKCkge1xuICAgIHRoaXMuc3QuY2xlYXIoKTtcbiAgfVxuXG5cblxuICByZWxvYWQocXVlcnlwYXJhcz86IGFueSkge1xuXG4gICAgdGhpcy5zdC5sb2FkaW5nID0gdHJ1ZTtcbiAgICBpZiAocXVlcnlwYXJhcylcbiAgICAgIHRoaXMuc3QucmVxLnBhcmFtcy5mb3JtID0gcXVlcnlwYXJhcztcbiAgICB0aGlzLnN0LnJlbG9hZCgpO1xuICAgIHRoaXMuc3QuY2QoKTtcblxuICB9XG5cbiAgc2V0Um93KGluZGV4OiBudW1iZXIsIGRhdGE6IFNURGF0YSkge1xuICAgIHRoaXMuc3Quc2V0Um93KGluZGV4LCBkYXRhKTtcbiAgfVxuXG4gIHJlc2V0KGV2ZW50OiBhbnkpIHtcbiAgICB0aGlzLmxvYWRpbmcgPSB0cnVlO1xuICAgIHRoaXMubG9hZGluZ0NoYW5nZS5lbWl0KHRydWUpO1xuICAgIHRoaXMuc3QucmVzZXQoZXZlbnQpO1xuICAgIHRoaXMuc3QuY2QoKTtcbiAgfVxuXG4gIHByaXZhdGUgcHJvY2VzcyA9IChkYXRhOiBTVERhdGFbXSkgPT4ge1xuICAgIHJldHVybiBkYXRhO1xuICB9O1xuXG4gIHByaXZhdGUgY3VzdG9tU3RhdGlzdGljYWwodmFsdWVzOiBudW1iZXJbXSwgY29sOiBTVENvbHVtbiwgbGlzdDogU1REYXRhW10sIHJhd0RhdGE/OiBhbnkpIHtcblxuICAgIGlmIChyYXdEYXRhLmNvZGUgPCAxKSByZXR1cm4geyB2YWx1ZTogXCJcIiwgdGV4dDogXCJcIiB9O1xuXG4gICAgaWYgKHJhd0RhdGEucGFnZS5zdGF0aXN0aWNhbCkge1xuXG4gICAgICBsZXQga2V5ID0gY29sLmluZGV4O1xuICAgICAgaWYgKGtleSBpbnN0YW5jZW9mIEFycmF5KSB7XG4gICAgICAgIGtleSA9IGtleVswXTtcbiAgICAgIH1cblxuXG4gICAgICBpZiAocmF3RGF0YS5wYWdlLnN0YXRpc3RpY2FsW2tleV0pIHtcblxuICAgICAgICByZXR1cm4geyB2YWx1ZTogcmF3RGF0YS5wYWdlLnN0YXRpc3RpY2FsW2tleV0sIHRleHQ6IFwiXCIgfTtcbiAgICAgIH1cblxuICAgICAgcmV0dXJuIHsgdmFsdWU6IFwiXCIsIHRleHQ6IFwiXCIgfTtcbiAgICB9XG5cbiAgICByZXR1cm4geyB2YWx1ZTogXCJcIiwgdGV4dDogXCJcIiB9O1xuXG4gIH1cblxuICAvLyDlr7zlh7pleGNlbFxuICBleHBvcnQob3B0PzogU1RFeHBvcnRPcHRpb25zKSB7XG5cbiAgICBjb25zdCBkYXRhID0gdGhpcy5zdC5fZGF0YTtcbiAgICB0aGlzLnN0LmV4cG9ydChkYXRhLCBvcHQpO1xuICB9XG5cbiAgZXhwb3J0QWxsKCkge1xuXG4gICAgdGhpcy5sb2FkaW5nID0gdHJ1ZTtcblxuICAgIHRoaXMuc3VwZXhjZWwuZXhwb3J0KHRoaXMuY29sdW1ucywgdGhpcy5fc25hbWUsIHRoaXMuc3QucmVxLnBhcmFtcy5mb3JtLCB0aGlzLnN0LnRvdGFsKS50aGVuKCgpID0+IHtcbiAgICAgIHRoaXMubG9hZGluZyA9IGZhbHNlO1xuICAgIH0pO1xuXG4gIH1cblxuICBwcml2YXRlIHJlc3RvcmVSZW5kZXIoaXRlbTogU1RDb2x1bW4pIHtcbiAgICBpZiAoaXRlbS5yZW5kZXJUaXRsZTIpIHtcbiAgICAgIGl0ZW0uX19yZW5kZXJUaXRsZSA9IHRoaXMubGJTb3VyY2UuZ2V0VGl0bGUoaXRlbS5yZW5kZXJUaXRsZTIpO1xuXG4gICAgfVxuICAgIGlmIChpdGVtLnJlbmRlcjIpIHtcbiAgICAgIGl0ZW0uX19yZW5kZXIgPSB0aGlzLmxiU291cmNlLmdldFJvdyhpdGVtLnJlbmRlcjIpO1xuXG4gICAgfVxuXG4gIH1cblxufVxuIl19