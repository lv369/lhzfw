/**
 * @fileoverview added by tsickle
 * Generated from: lib/components/img-upload/img-upload.component.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component, Input } from '@angular/core';
import { NzMessageService, NzModalService } from 'ng-zorro-antd';
import { Observable, Subscription } from 'rxjs';
import { HttpService } from '../../lblibs/Lbjk.service';
var ImgUploadComponent = /** @class */ (function () {
    function ImgUploadComponent(msgsvr, lbservice, modalService) {
        var _this = this;
        this.msgsvr = msgsvr;
        this.lbservice = lbservice;
        this.modalService = modalService;
        /**
         * 图表列表
         */
        this.fileList = [];
        this._previewImage = '';
        this._previewVisible = false;
        /**
         * 上传按钮是否显示
         */
        this.nzShowButton = true;
        /**
         * 预览按钮以及删除按钮
         */
        this.showUploadList = {
            showPreviewIcon: true,
            showRemoveIcon: true,
            hidePreviewIconInNonImage: true
        };
        /**
         * 上传事件
         */
        this.upload = (/**
         * @param {?} item
         * @return {?}
         */
        function (item) { return Subscription; });
        /**
         * 删除事件
         */
        this.del = (/**
         * @param {?} file
         * @return {?}
         */
        function (file) { return new Promise((/**
         * @return {?}
         */
        function () { })); });
        this._handlePreview = (/**
         * @param {?} file
         * @return {?}
         */
        function (file) {
            _this._previewImage = file.url || file.thumbUrl;
            _this._previewVisible = true;
        });
        /**
         * 图片上传
         */
        this._uploadimg = (/**
         * @param {?} item
         * @return {?}
         */
        function (item) {
            if (_this.upload) {
                return _this.upload(item);
            }
        });
        /**
         * 移除文件
         */
        this._removeIMG = (/**
         * @param {?} file
         * @return {?}
         */
        function (file) {
            return new Observable((/**
             * @param {?} obs
             * @return {?}
             */
            function (obs) {
                _this.modalService.confirm({
                    nzTitle: '删除确认',
                    nzContent: '<b style="color: red;">确认要删除这张图片么？</b>',
                    nzOkText: '确定',
                    nzOkType: 'danger',
                    nzOnOk: (/**
                     * @return {?}
                     */
                    function () {
                        if (_this.del) {
                            _this.del(file).then((/**
                             * @param {?} resdata
                             * @return {?}
                             */
                            function (resdata) {
                                if (resdata.code < 1) {
                                    _this.msgsvr.error("图片删除失败:" + resdata.errmsg);
                                    obs.next(false);
                                }
                                else {
                                    _this.msgsvr.success('图片删除成功！');
                                    obs.next(true);
                                }
                            }));
                        }
                    }),
                    nzCancelText: '取消',
                    nzOnCancel: (/**
                     * @return {?}
                     */
                    function () { return obs.next(false); })
                });
            }));
        });
        this._beforeUpload = (/**
         * @param {?} file
         * @return {?}
         */
        function (file) {
            return new Observable((/**
             * @param {?} observer
             * @return {?}
             */
            function (observer) {
                /** @type {?} */
                var imgtype = ["image/png", "image/jpeg", "image/gif", "image/bmp"];
                //  const isImg = file.type === 'image/jpeg';
                if (!imgtype.includes(file.type)) {
                    _this.msgsvr.error('只能上传图片格式!');
                    observer.complete();
                    return;
                }
                observer.next(true);
                observer.complete();
            }));
        });
    }
    /**
     * @return {?}
     */
    ImgUploadComponent.prototype.ngOnInit = /**
     * @return {?}
     */
    function () {
    };
    /**
     * 上传成功后处理
     * @param info  上传返回信息
     */
    /**
     * 上传成功后处理
     * @param {?} info  上传返回信息
     * @return {?}
     */
    ImgUploadComponent.prototype._handleChange = /**
     * 上传成功后处理
     * @param {?} info  上传返回信息
     * @return {?}
     */
    function (info) {
        /** @type {?} */
        var fileList = info.fileList;
        // 2. read from response and show file link
        if (info.file.response) {
            if (info.file.response.code < 1) {
                this.msgsvr.error("文件上传失败:" + info.file.response.errmsg);
            }
            else {
                info.file.ftoken = info.file.response.message.ftoken;
            }
            info.file.url = info.file.response.url;
        }
        // 3. filter successfully uploaded files according to response from server
        this.fileList = fileList.filter((/**
         * @param {?} item
         * @return {?}
         */
        function (item) {
            if (item.response) {
                if (item.response.code > 0) {
                    item.ftoken = item.response.message.ftoken;
                    return true;
                }
                return false;
            }
            return true;
        }));
    };
    ImgUploadComponent.decorators = [
        { type: Component, args: [{
                    selector: 'lb-img',
                    template: "<div>\n    <nz-upload nzListType=\"picture-card\" [(nzFileList)]=\"fileList\" [nzCustomRequest]=\"_uploadimg\"\n        [nzRemove]=\"_removeIMG\" [nzBeforeUpload]=\"_beforeUpload\" (nzChange)=\"_handleChange($event)\"\n        [nzShowButton]=\"nzShowButton\" [nzSize]=\"10*1024\" [nzShowUploadList]=\"showUploadList\"\n        [nzPreview]=\"_handlePreview\">\n        <div class=\"ant-upload-text\">\u4E0A\u4F20\u56FE\u7247</div>\n    </nz-upload>\n    <nz-modal [nzVisible]=\"_previewVisible\" [nzContent]=\"modalContent\" [nzFooter]=\"null\"\n        (nzOnCancel)=\"_previewVisible=false\">\n        <ng-template #modalContent>\n            <img [src]=\"_previewImage\" [ngStyle]=\"{ 'width': '100%' }\" />\n        </ng-template>\n    </nz-modal>\n</div>",
                    styles: [""]
                }] }
    ];
    /** @nocollapse */
    ImgUploadComponent.ctorParameters = function () { return [
        { type: NzMessageService },
        { type: HttpService },
        { type: NzModalService }
    ]; };
    ImgUploadComponent.propDecorators = {
        fileList: [{ type: Input }],
        nzShowButton: [{ type: Input }],
        showUploadList: [{ type: Input }],
        upload: [{ type: Input }],
        del: [{ type: Input }]
    };
    return ImgUploadComponent;
}());
export { ImgUploadComponent };
if (false) {
    /**
     * 图表列表
     * @type {?}
     */
    ImgUploadComponent.prototype.fileList;
    /** @type {?} */
    ImgUploadComponent.prototype._previewImage;
    /** @type {?} */
    ImgUploadComponent.prototype._previewVisible;
    /**
     * 上传按钮是否显示
     * @type {?}
     */
    ImgUploadComponent.prototype.nzShowButton;
    /**
     * 预览按钮以及删除按钮
     * @type {?}
     */
    ImgUploadComponent.prototype.showUploadList;
    /**
     * 上传事件
     * @type {?}
     */
    ImgUploadComponent.prototype.upload;
    /**
     * 删除事件
     * @type {?}
     */
    ImgUploadComponent.prototype.del;
    /** @type {?} */
    ImgUploadComponent.prototype._handlePreview;
    /**
     * 图片上传
     * @type {?}
     */
    ImgUploadComponent.prototype._uploadimg;
    /**
     * 移除文件
     * @type {?}
     */
    ImgUploadComponent.prototype._removeIMG;
    /** @type {?} */
    ImgUploadComponent.prototype._beforeUpload;
    /**
     * @type {?}
     * @private
     */
    ImgUploadComponent.prototype.msgsvr;
    /**
     * @type {?}
     * @private
     */
    ImgUploadComponent.prototype.lbservice;
    /**
     * @type {?}
     * @private
     */
    ImgUploadComponent.prototype.modalService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW1nLXVwbG9hZC5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9sYmYvIiwic291cmNlcyI6WyJsaWIvY29tcG9uZW50cy9pbWctdXBsb2FkL2ltZy11cGxvYWQuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7O0FBQUEsT0FBTyxFQUFFLFNBQVMsRUFBVSxLQUFLLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDekQsT0FBTyxFQUFFLGdCQUFnQixFQUFFLGNBQWMsRUFBNkIsTUFBTSxlQUFlLENBQUM7QUFDNUYsT0FBTyxFQUFFLFVBQVUsRUFBRSxZQUFZLEVBQVksTUFBTSxNQUFNLENBQUM7QUFDMUQsT0FBTyxFQUFFLFdBQVcsRUFBRSxNQUFNLDJCQUEyQixDQUFDO0FBRXhEO0lBT0UsNEJBQW9CLE1BQXdCLEVBQ2xDLFNBQXNCLEVBQ3RCLFlBQTRCO1FBRnRDLGlCQUUyQztRQUZ2QixXQUFNLEdBQU4sTUFBTSxDQUFrQjtRQUNsQyxjQUFTLEdBQVQsU0FBUyxDQUFhO1FBQ3RCLGlCQUFZLEdBQVosWUFBWSxDQUFnQjs7OztRQUs3QixhQUFRLEdBQUcsRUFBRSxDQUFDO1FBQ3ZCLGtCQUFhLEdBQUcsRUFBRSxDQUFDO1FBQ25CLG9CQUFlLEdBQUcsS0FBSyxDQUFDOzs7O1FBSWYsaUJBQVksR0FBRyxJQUFJLENBQUM7Ozs7UUFLcEIsbUJBQWMsR0FBRztZQUN4QixlQUFlLEVBQUUsSUFBSTtZQUNyQixjQUFjLEVBQUUsSUFBSTtZQUNwQix5QkFBeUIsRUFBRSxJQUFJO1NBQ2hDLENBQUM7Ozs7UUFRTyxXQUFNOzs7O1FBQUcsVUFBQyxJQUFtQixJQUFLLE9BQUEsWUFBWSxFQUFaLENBQVksRUFBQzs7OztRQUkvQyxRQUFHOzs7O1FBQUcsVUFBQyxJQUFnQixJQUFLLE9BQUEsSUFBSSxPQUFPOzs7UUFBQyxjQUFRLENBQUMsRUFBQyxFQUF0QixDQUFzQixFQUFDO1FBRTVELG1CQUFjOzs7O1FBQUcsVUFBQyxJQUFnQjtZQUNoQyxLQUFJLENBQUMsYUFBYSxHQUFHLElBQUksQ0FBQyxHQUFHLElBQUksSUFBSSxDQUFDLFFBQVEsQ0FBQztZQUMvQyxLQUFJLENBQUMsZUFBZSxHQUFHLElBQUksQ0FBQztRQUM5QixDQUFDLEVBQUE7Ozs7UUFNRCxlQUFVOzs7O1FBQUcsVUFBQyxJQUFtQjtZQUMvQixJQUFJLEtBQUksQ0FBQyxNQUFNLEVBQUU7Z0JBQ2YsT0FBTyxLQUFJLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxDQUFDO2FBQzFCO1FBQ0gsQ0FBQyxFQUFBOzs7O1FBS0QsZUFBVTs7OztRQUFHLFVBQUMsSUFBZ0I7WUFFNUIsT0FBTyxJQUFJLFVBQVU7Ozs7WUFBVSxVQUFDLEdBQUc7Z0JBRWpDLEtBQUksQ0FBQyxZQUFZLENBQUMsT0FBTyxDQUFDO29CQUN4QixPQUFPLEVBQUUsTUFBTTtvQkFDZixTQUFTLEVBQUUsd0NBQXdDO29CQUNuRCxRQUFRLEVBQUUsSUFBSTtvQkFDZCxRQUFRLEVBQUUsUUFBUTtvQkFDbEIsTUFBTTs7O29CQUFFO3dCQUVOLElBQUksS0FBSSxDQUFDLEdBQUcsRUFBRTs0QkFFWixLQUFJLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxDQUFDLElBQUk7Ozs7NEJBQUMsVUFBQyxPQUFZO2dDQUMvQixJQUFJLE9BQU8sQ0FBQyxJQUFJLEdBQUcsQ0FBQyxFQUFFO29DQUNwQixLQUFJLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxTQUFTLEdBQUcsT0FBTyxDQUFDLE1BQU0sQ0FBQyxDQUFDO29DQUM5QyxHQUFHLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO2lDQUNqQjtxQ0FBTTtvQ0FDTCxLQUFJLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxTQUFTLENBQUMsQ0FBQztvQ0FDL0IsR0FBRyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztpQ0FDaEI7NEJBRUgsQ0FBQyxFQUFDLENBQUE7eUJBQ0g7b0JBRUgsQ0FBQyxDQUFBO29CQUNELFlBQVksRUFBRSxJQUFJO29CQUNsQixVQUFVOzs7b0JBQUUsY0FBTSxPQUFBLEdBQUcsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLEVBQWYsQ0FBZSxDQUFBO2lCQUNsQyxDQUFDLENBQUM7WUFHTCxDQUFDLEVBQUMsQ0FBQztRQUNMLENBQUMsRUFBQTtRQXFDRCxrQkFBYTs7OztRQUFHLFVBQUMsSUFBVTtZQUN6QixPQUFPLElBQUksVUFBVTs7OztZQUFDLFVBQUMsUUFBMkI7O29CQUUxQyxPQUFPLEdBQUcsQ0FBQyxXQUFXLEVBQUUsWUFBWSxFQUFFLFdBQVcsRUFBRSxXQUFXLENBQUM7Z0JBRXJFLDZDQUE2QztnQkFDN0MsSUFBSSxDQUFDLE9BQU8sQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxFQUFFO29CQUNoQyxLQUFJLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxXQUFXLENBQUMsQ0FBQztvQkFDL0IsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO29CQUNwQixPQUFPO2lCQUNSO2dCQUVELFFBQVEsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7Z0JBQ3BCLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQztZQUV0QixDQUFDLEVBQUMsQ0FBQztRQUNMLENBQUMsRUFBQztJQXpJd0MsQ0FBQzs7OztJQXNCM0MscUNBQVE7OztJQUFSO0lBQ0EsQ0FBQztJQStERDs7O09BR0c7Ozs7OztJQUNILDBDQUFhOzs7OztJQUFiLFVBQWMsSUFBUzs7WUFFZixRQUFRLEdBQUcsSUFBSSxDQUFDLFFBQVE7UUFDOUIsMkNBQTJDO1FBQzNDLElBQUksSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLEVBQUU7WUFDdEIsSUFBSSxJQUFJLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxJQUFJLEdBQUcsQ0FBQyxFQUFFO2dCQUMvQixJQUFJLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsTUFBTSxDQUFDLENBQUM7YUFDMUQ7aUJBQU07Z0JBRUwsSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsT0FBTyxDQUFDLE1BQU0sQ0FBQzthQUN0RDtZQUdELElBQUksQ0FBQyxJQUFJLENBQUMsR0FBRyxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLEdBQUcsQ0FBQztTQUN4QztRQUNELDBFQUEwRTtRQUMxRSxJQUFJLENBQUMsUUFBUSxHQUFHLFFBQVEsQ0FBQyxNQUFNOzs7O1FBQUMsVUFBQSxJQUFJO1lBQ2xDLElBQUksSUFBSSxDQUFDLFFBQVEsRUFBRTtnQkFFakIsSUFBSSxJQUFJLENBQUMsUUFBUSxDQUFDLElBQUksR0FBRyxDQUFDLEVBQUU7b0JBRTFCLElBQUksQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQyxPQUFPLENBQUMsTUFBTSxDQUFDO29CQUMzQyxPQUFPLElBQUksQ0FBQztpQkFDYjtnQkFFRCxPQUFPLEtBQUssQ0FBQzthQUNkO1lBQ0QsT0FBTyxJQUFJLENBQUM7UUFDZCxDQUFDLEVBQUMsQ0FBQztJQUNMLENBQUM7O2dCQWhJRixTQUFTLFNBQUM7b0JBQ1QsUUFBUSxFQUFFLFFBQVE7b0JBQ2xCLGl3QkFBMEM7O2lCQUUzQzs7OztnQkFSUSxnQkFBZ0I7Z0JBRWhCLFdBQVc7Z0JBRk8sY0FBYzs7OzJCQWtCdEMsS0FBSzsrQkFNTCxLQUFLO2lDQUtMLEtBQUs7eUJBWUwsS0FBSztzQkFJTCxLQUFLOztJQTBHUix5QkFBQztDQUFBLEFBbkpELElBbUpDO1NBOUlZLGtCQUFrQjs7Ozs7O0lBUzdCLHNDQUF1Qjs7SUFDdkIsMkNBQW1COztJQUNuQiw2Q0FBd0I7Ozs7O0lBSXhCLDBDQUE2Qjs7Ozs7SUFLN0IsNENBSUU7Ozs7O0lBUUYsb0NBQXdEOzs7OztJQUl4RCxpQ0FBNEQ7O0lBRTVELDRDQUdDOzs7OztJQU1ELHdDQUlDOzs7OztJQUtELHdDQWdDQzs7SUFxQ0QsMkNBZ0JFOzs7OztJQTNJVSxvQ0FBZ0M7Ozs7O0lBQzFDLHVDQUE4Qjs7Ozs7SUFDOUIsMENBQW9DIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBPbkluaXQsIElucHV0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBOek1lc3NhZ2VTZXJ2aWNlLCBOek1vZGFsU2VydmljZSwgVXBsb2FkRmlsZSwgVXBsb2FkWEhSQXJncyB9IGZyb20gJ25nLXpvcnJvLWFudGQnO1xuaW1wb3J0IHsgT2JzZXJ2YWJsZSwgU3Vic2NyaXB0aW9uLCBPYnNlcnZlciB9IGZyb20gJ3J4anMnO1xuaW1wb3J0IHsgSHR0cFNlcnZpY2UgfSBmcm9tICcuLi8uLi9sYmxpYnMvTGJqay5zZXJ2aWNlJztcblxuQENvbXBvbmVudCh7XG4gIHNlbGVjdG9yOiAnbGItaW1nJyxcbiAgdGVtcGxhdGVVcmw6ICcuL2ltZy11cGxvYWQuY29tcG9uZW50Lmh0bWwnLFxuICBzdHlsZVVybHM6IFsnLi9pbWctdXBsb2FkLmNvbXBvbmVudC5jc3MnXVxufSlcbmV4cG9ydCBjbGFzcyBJbWdVcGxvYWRDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xuXG4gIGNvbnN0cnVjdG9yKHByaXZhdGUgbXNnc3ZyOiBOek1lc3NhZ2VTZXJ2aWNlLFxuICAgIHByaXZhdGUgbGJzZXJ2aWNlOiBIdHRwU2VydmljZSxcbiAgICBwcml2YXRlIG1vZGFsU2VydmljZTogTnpNb2RhbFNlcnZpY2UpIHsgfVxuXG4gIC8qKlxuICAgKiDlm77ooajliJfooahcbiAgICovXG4gIEBJbnB1dCgpIGZpbGVMaXN0ID0gW107XG4gIF9wcmV2aWV3SW1hZ2UgPSAnJztcbiAgX3ByZXZpZXdWaXNpYmxlID0gZmFsc2U7XG4gIC8qKlxuICAgKiDkuIrkvKDmjInpkq7mmK/lkKbmmL7npLpcbiAgICovXG4gIEBJbnB1dCgpIG56U2hvd0J1dHRvbiA9IHRydWU7XG5cbiAgLyoqXG4gICAqIOmihOiniOaMiemSruS7peWPiuWIoOmZpOaMiemSrlxuICAgKi9cbiAgQElucHV0KCkgc2hvd1VwbG9hZExpc3QgPSB7XG4gICAgc2hvd1ByZXZpZXdJY29uOiB0cnVlLFxuICAgIHNob3dSZW1vdmVJY29uOiB0cnVlLFxuICAgIGhpZGVQcmV2aWV3SWNvbkluTm9uSW1hZ2U6IHRydWVcbiAgfTtcblxuICBuZ09uSW5pdCgpIHtcbiAgfVxuXG4gIC8qKlxuICAgKiDkuIrkvKDkuovku7ZcbiAgICovXG4gIEBJbnB1dCgpIHVwbG9hZCA9IChpdGVtOiBVcGxvYWRYSFJBcmdzKSA9PiBTdWJzY3JpcHRpb247XG4gIC8qKlxuICAgKiDliKDpmaTkuovku7ZcbiAgICovXG4gIEBJbnB1dCgpIGRlbCA9IChmaWxlOiBVcGxvYWRGaWxlKSA9PiBuZXcgUHJvbWlzZSgoKSA9PiB7IH0pO1xuXG4gIF9oYW5kbGVQcmV2aWV3ID0gKGZpbGU6IFVwbG9hZEZpbGUpID0+IHtcbiAgICB0aGlzLl9wcmV2aWV3SW1hZ2UgPSBmaWxlLnVybCB8fCBmaWxlLnRodW1iVXJsO1xuICAgIHRoaXMuX3ByZXZpZXdWaXNpYmxlID0gdHJ1ZTtcbiAgfVxuXG5cbiAgLyoqXG4gICAqIOWbvueJh+S4iuS8oFxuICAgKi9cbiAgX3VwbG9hZGltZyA9IChpdGVtOiBVcGxvYWRYSFJBcmdzKSA9PiB7XG4gICAgaWYgKHRoaXMudXBsb2FkKSB7XG4gICAgICByZXR1cm4gdGhpcy51cGxvYWQoaXRlbSk7XG4gICAgfVxuICB9XG5cbiAgLyoqXG4gICAqIOenu+mZpOaWh+S7tlxuICAgKi9cbiAgX3JlbW92ZUlNRyA9IChmaWxlOiBVcGxvYWRGaWxlKSA9PiB7XG5cbiAgICByZXR1cm4gbmV3IE9ic2VydmFibGU8Ym9vbGVhbj4oKG9icykgPT4ge1xuXG4gICAgICB0aGlzLm1vZGFsU2VydmljZS5jb25maXJtKHtcbiAgICAgICAgbnpUaXRsZTogJ+WIoOmZpOehruiupCcsXG4gICAgICAgIG56Q29udGVudDogJzxiIHN0eWxlPVwiY29sb3I6IHJlZDtcIj7noa7orqTopoHliKDpmaTov5nlvKDlm77niYfkuYjvvJ88L2I+JyxcbiAgICAgICAgbnpPa1RleHQ6ICfnoa7lrponLFxuICAgICAgICBuek9rVHlwZTogJ2RhbmdlcicsXG4gICAgICAgIG56T25PazogKCkgPT4ge1xuXG4gICAgICAgICAgaWYgKHRoaXMuZGVsKSB7XG5cbiAgICAgICAgICAgIHRoaXMuZGVsKGZpbGUpLnRoZW4oKHJlc2RhdGE6IGFueSkgPT4ge1xuICAgICAgICAgICAgICBpZiAocmVzZGF0YS5jb2RlIDwgMSkge1xuICAgICAgICAgICAgICAgIHRoaXMubXNnc3ZyLmVycm9yKFwi5Zu+54mH5Yig6Zmk5aSx6LSlOlwiICsgcmVzZGF0YS5lcnJtc2cpO1xuICAgICAgICAgICAgICAgIG9icy5uZXh0KGZhbHNlKTtcbiAgICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICB0aGlzLm1zZ3N2ci5zdWNjZXNzKCflm77niYfliKDpmaTmiJDlip/vvIEnKTtcbiAgICAgICAgICAgICAgICBvYnMubmV4dCh0cnVlKTtcbiAgICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICB9KVxuICAgICAgICAgIH1cblxuICAgICAgICB9LFxuICAgICAgICBuekNhbmNlbFRleHQ6ICflj5bmtognLFxuICAgICAgICBuek9uQ2FuY2VsOiAoKSA9PiBvYnMubmV4dChmYWxzZSlcbiAgICAgIH0pO1xuXG5cbiAgICB9KTtcbiAgfVxuXG4gIC8qKlxuICAgKiDkuIrkvKDmiJDlip/lkI7lpITnkIZcbiAgICogQHBhcmFtIGluZm8gIOS4iuS8oOi/lOWbnuS/oeaBr1xuICAgKi9cbiAgX2hhbmRsZUNoYW5nZShpbmZvOiBhbnkpOiB2b2lkIHtcblxuICAgIGNvbnN0IGZpbGVMaXN0ID0gaW5mby5maWxlTGlzdDtcbiAgICAvLyAyLiByZWFkIGZyb20gcmVzcG9uc2UgYW5kIHNob3cgZmlsZSBsaW5rXG4gICAgaWYgKGluZm8uZmlsZS5yZXNwb25zZSkge1xuICAgICAgaWYgKGluZm8uZmlsZS5yZXNwb25zZS5jb2RlIDwgMSkge1xuICAgICAgICB0aGlzLm1zZ3N2ci5lcnJvcihcIuaWh+S7tuS4iuS8oOWksei0pTpcIiArIGluZm8uZmlsZS5yZXNwb25zZS5lcnJtc2cpO1xuICAgICAgfSBlbHNlIHtcblxuICAgICAgICBpbmZvLmZpbGUuZnRva2VuID0gaW5mby5maWxlLnJlc3BvbnNlLm1lc3NhZ2UuZnRva2VuO1xuICAgICAgfVxuXG5cbiAgICAgIGluZm8uZmlsZS51cmwgPSBpbmZvLmZpbGUucmVzcG9uc2UudXJsO1xuICAgIH1cbiAgICAvLyAzLiBmaWx0ZXIgc3VjY2Vzc2Z1bGx5IHVwbG9hZGVkIGZpbGVzIGFjY29yZGluZyB0byByZXNwb25zZSBmcm9tIHNlcnZlclxuICAgIHRoaXMuZmlsZUxpc3QgPSBmaWxlTGlzdC5maWx0ZXIoaXRlbSA9PiB7XG4gICAgICBpZiAoaXRlbS5yZXNwb25zZSkge1xuXG4gICAgICAgIGlmIChpdGVtLnJlc3BvbnNlLmNvZGUgPiAwKSB7XG5cbiAgICAgICAgICBpdGVtLmZ0b2tlbiA9IGl0ZW0ucmVzcG9uc2UubWVzc2FnZS5mdG9rZW47XG4gICAgICAgICAgcmV0dXJuIHRydWU7XG4gICAgICAgIH1cblxuICAgICAgICByZXR1cm4gZmFsc2U7XG4gICAgICB9XG4gICAgICByZXR1cm4gdHJ1ZTtcbiAgICB9KTtcbiAgfVxuXG4gIF9iZWZvcmVVcGxvYWQgPSAoZmlsZTogRmlsZSkgPT4ge1xuICAgIHJldHVybiBuZXcgT2JzZXJ2YWJsZSgob2JzZXJ2ZXI6IE9ic2VydmVyPGJvb2xlYW4+KSA9PiB7XG5cbiAgICAgIGNvbnN0IGltZ3R5cGUgPSBbXCJpbWFnZS9wbmdcIiwgXCJpbWFnZS9qcGVnXCIsIFwiaW1hZ2UvZ2lmXCIsIFwiaW1hZ2UvYm1wXCJdO1xuXG4gICAgICAvLyAgY29uc3QgaXNJbWcgPSBmaWxlLnR5cGUgPT09ICdpbWFnZS9qcGVnJztcbiAgICAgIGlmICghaW1ndHlwZS5pbmNsdWRlcyhmaWxlLnR5cGUpKSB7XG4gICAgICAgIHRoaXMubXNnc3ZyLmVycm9yKCflj6rog73kuIrkvKDlm77niYfmoLzlvI8hJyk7XG4gICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XG4gICAgICAgIHJldHVybjtcbiAgICAgIH1cblxuICAgICAgb2JzZXJ2ZXIubmV4dCh0cnVlKTtcbiAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XG5cbiAgICB9KTtcbiAgfTtcbn1cbiJdfQ==