/**
 * @fileoverview added by tsickle
 * Generated from: lib/lblibs/SupExcel.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Injectable } from '@angular/core';
import { XlsxService } from '@delon/abc';
import { forkJoin } from 'rxjs';
import { HttpService } from './Lbjk.service';
import * as i0 from "@angular/core";
import * as i1 from "./Lbjk.service";
import * as i2 from "@delon/abc/xlsx";
var SupExcel = /** @class */ (function () {
    function SupExcel(lbservice, xlsx) {
        this.lbservice = lbservice;
        this.xlsx = xlsx;
    }
    /**
     * 导出excel
     * @param columns 字段信息
     * @param sname 查询服务名
     * @param para 查询服务参数
     * @param total 总条数
     * @param onetime 单次请求数，默认20
     */
    /**
     * 导出excel
     * @param {?} columns 字段信息
     * @param {?} sname 查询服务名
     * @param {?} para 查询服务参数
     * @param {?} total 总条数
     * @param {?=} onetime 单次请求数，默认20
     * @return {?}
     */
    SupExcel.prototype.export = /**
     * 导出excel
     * @param {?} columns 字段信息
     * @param {?} sname 查询服务名
     * @param {?} para 查询服务参数
     * @param {?} total 总条数
     * @param {?=} onetime 单次请求数，默认20
     * @return {?}
     */
    function (columns, sname, para, total, onetime) {
        var _this = this;
        if (onetime === void 0) { onetime = 20; }
        /** @type {?} */
        var data = [columns.map((/**
             * @param {?} i
             * @return {?}
             */
            function (i) { return i.title; }))];
        /** @type {?} */
        var pageCount = Math.ceil(total / 100);
        // 请求信息
        /** @type {?} */
        var reqtime = Array(pageCount).fill({}).map((/**
         * @param {?} _item
         * @param {?} idx
         * @return {?}
         */
        function (_item, idx) {
            return {
                PAGEIDX: idx,
                PAGESIZE: 100,
            };
        }));
        /** @type {?} */
        var obs = [];
        // 把20次的请求合一起
        reqtime.forEach((/**
         * @param {?} page
         * @return {?}
         */
        function (page) {
            obs.push(_this.lbservice.lbservice2(sname, { para: para, page: page }));
        }));
        /** @type {?} */
        var allcall = forkJoin(obs);
        return new Promise((/**
         * @param {?} resolve
         * @param {?} _
         * @return {?}
         */
        function (resolve, _) {
            allcall.subscribe((/**
             * @param {?} results
             * @return {?}
             */
            function (results) {
                results.forEach((/**
                 * @param {?} item
                 * @return {?}
                 */
                function (item) {
                    if (item.code > 0) {
                        item.message.list.forEach((/**
                         * @param {?} i
                         * @return {?}
                         */
                        function (i) {
                            return data.push(columns.map((/**
                             * @param {?} c
                             * @return {?}
                             */
                            function (c) { return i[(/** @type {?} */ (c.index))]; })));
                        }));
                    }
                }));
                _this.xlsx.export({
                    sheets: [
                        {
                            data: data,
                            name: 'sheet1',
                        },
                    ],
                }).then((/**
                 * @return {?}
                 */
                function () {
                    resolve(null);
                }));
            }));
        }));
    };
    SupExcel.decorators = [
        { type: Injectable, args: [{ providedIn: 'root' },] }
    ];
    /** @nocollapse */
    SupExcel.ctorParameters = function () { return [
        { type: HttpService },
        { type: XlsxService }
    ]; };
    /** @nocollapse */ SupExcel.ngInjectableDef = i0.ɵɵdefineInjectable({ factory: function SupExcel_Factory() { return new SupExcel(i0.ɵɵinject(i1.HttpService), i0.ɵɵinject(i2.XlsxService)); }, token: SupExcel, providedIn: "root" });
    return SupExcel;
}());
export { SupExcel };
if (false) {
    /**
     * @type {?}
     * @private
     */
    SupExcel.prototype.lbservice;
    /**
     * @type {?}
     * @private
     */
    SupExcel.prototype.xlsx;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiU3VwRXhjZWwuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9sYmYvIiwic291cmNlcyI6WyJsaWIvbGJsaWJzL1N1cEV4Y2VsLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7O0FBQUEsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUMzQyxPQUFPLEVBQVksV0FBVyxFQUFFLE1BQU0sWUFBWSxDQUFDO0FBQ25ELE9BQU8sRUFBTSxRQUFRLEVBQUUsTUFBTSxNQUFNLENBQUM7QUFDcEMsT0FBTyxFQUFFLFdBQVcsRUFBRSxNQUFNLGdCQUFnQixDQUFDOzs7O0FBRTdDO0lBR0Usa0JBQW9CLFNBQXNCLEVBQVUsSUFBaUI7UUFBakQsY0FBUyxHQUFULFNBQVMsQ0FBYTtRQUFVLFNBQUksR0FBSixJQUFJLENBQWE7SUFFckUsQ0FBQztJQUdEOzs7Ozs7O09BT0c7Ozs7Ozs7Ozs7SUFDSCx5QkFBTTs7Ozs7Ozs7O0lBQU4sVUFBTyxPQUFtQixFQUFFLEtBQWEsRUFBRSxJQUFTLEVBQUUsS0FBYSxFQUFFLE9BQW9CO1FBQXpGLGlCQXdEQztRQXhEb0Usd0JBQUEsRUFBQSxZQUFvQjs7WUFFakYsSUFBSSxHQUFHLENBQUMsT0FBTyxDQUFDLEdBQUc7Ozs7WUFBQyxVQUFBLENBQUMsSUFBSSxPQUFBLENBQUMsQ0FBQyxLQUFLLEVBQVAsQ0FBTyxFQUFDLENBQUM7O1lBRWxDLFNBQVMsR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLEtBQUssR0FBRyxHQUFHLENBQUM7OztZQUlsQyxPQUFPLEdBQUcsS0FBSyxDQUFDLFNBQVMsQ0FBQyxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBQyxHQUFHOzs7OztRQUFDLFVBQUMsS0FBVSxFQUFFLEdBQVc7WUFDcEUsT0FBTztnQkFDTCxPQUFPLEVBQUUsR0FBRztnQkFDWixRQUFRLEVBQUUsR0FBRzthQUNkLENBQUM7UUFDSixDQUFDLEVBQUM7O1lBSUksR0FBRyxHQUFHLEVBQUU7UUFFZCxhQUFhO1FBQ2IsT0FBTyxDQUFDLE9BQU87Ozs7UUFBQyxVQUFBLElBQUk7WUFDbEIsR0FBRyxDQUFDLElBQUksQ0FBQyxLQUFJLENBQUMsU0FBUyxDQUFDLFVBQVUsQ0FBQyxLQUFLLEVBQUUsRUFBRSxJQUFJLE1BQUEsRUFBRSxJQUFJLE1BQUEsRUFBRSxDQUFDLENBQUMsQ0FBQztRQUU3RCxDQUFDLEVBQUMsQ0FBQzs7WUFFRyxPQUFPLEdBQUcsUUFBUSxDQUFDLEdBQUcsQ0FBQztRQUU3QixPQUFPLElBQUksT0FBTzs7Ozs7UUFBQyxVQUFDLE9BQU8sRUFBRSxDQUFDO1lBQzVCLE9BQU8sQ0FBQyxTQUFTOzs7O1lBQUMsVUFBQSxPQUFPO2dCQUV2QixPQUFPLENBQUMsT0FBTzs7OztnQkFBQyxVQUFDLElBQVM7b0JBRXhCLElBQUksSUFBSSxDQUFDLElBQUksR0FBRyxDQUFDLEVBQUU7d0JBRWpCLElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLE9BQU87Ozs7d0JBQUMsVUFBQSxDQUFDOzRCQUN6QixPQUFBLElBQUksQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLEdBQUc7Ozs7NEJBQUMsVUFBQSxDQUFDLElBQUksT0FBQSxDQUFDLENBQUMsbUJBQUEsQ0FBQyxDQUFDLEtBQUssRUFBVSxDQUFDLEVBQXBCLENBQW9CLEVBQUMsQ0FBQzt3QkFBakQsQ0FBaUQsRUFDbEQsQ0FBQztxQkFDSDtnQkFFSCxDQUFDLEVBQUMsQ0FBQztnQkFFSCxLQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQztvQkFDZixNQUFNLEVBQUU7d0JBQ047NEJBQ0UsSUFBSSxNQUFBOzRCQUNKLElBQUksRUFBRSxRQUFRO3lCQUNmO3FCQUNGO2lCQUNGLENBQUMsQ0FBQyxJQUFJOzs7Z0JBQUM7b0JBQ04sT0FBTyxDQUFDLElBQUksQ0FBQyxDQUFDO2dCQUNoQixDQUFDLEVBQUMsQ0FBQTtZQUNKLENBQUMsRUFBQyxDQUFDO1FBR0wsQ0FBQyxFQUFDLENBQUE7SUFFSixDQUFDOztnQkF4RUYsVUFBVSxTQUFDLEVBQUUsVUFBVSxFQUFFLE1BQU0sRUFBRTs7OztnQkFGekIsV0FBVztnQkFGRCxXQUFXOzs7bUJBRDlCO0NBZ0ZDLEFBM0VELElBMkVDO1NBMUVZLFFBQVE7Ozs7OztJQUVQLDZCQUE4Qjs7Ozs7SUFBRSx3QkFBeUIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBJbmplY3RhYmxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IFNUQ29sdW1uLCBYbHN4U2VydmljZSB9IGZyb20gJ0BkZWxvbi9hYmMnO1xyXG5pbXBvcnQgeyBvZiwgZm9ya0pvaW4gfSBmcm9tICdyeGpzJztcclxuaW1wb3J0IHsgSHR0cFNlcnZpY2UgfSBmcm9tICcuL0xiamsuc2VydmljZSc7XHJcblxyXG5ASW5qZWN0YWJsZSh7IHByb3ZpZGVkSW46ICdyb290JyB9KVxyXG5leHBvcnQgY2xhc3MgU3VwRXhjZWwge1xyXG5cclxuICBjb25zdHJ1Y3Rvcihwcml2YXRlIGxic2VydmljZTogSHR0cFNlcnZpY2UsIHByaXZhdGUgeGxzeDogWGxzeFNlcnZpY2UpIHtcclxuXHJcbiAgfVxyXG5cclxuXHJcbiAgLyoqXHJcbiAgICog5a+85Ye6ZXhjZWxcclxuICAgKiBAcGFyYW0gY29sdW1ucyDlrZfmrrXkv6Hmga8gXHJcbiAgICogQHBhcmFtIHNuYW1lIOafpeivouacjeWKoeWQjVxyXG4gICAqIEBwYXJhbSBwYXJhIOafpeivouacjeWKoeWPguaVsFxyXG4gICAqIEBwYXJhbSB0b3RhbCDmgLvmnaHmlbBcclxuICAgKiBAcGFyYW0gb25ldGltZSDljZXmrKHor7fmsYLmlbDvvIzpu5jorqQyMFxyXG4gICAqL1xyXG4gIGV4cG9ydChjb2x1bW5zOiBTVENvbHVtbltdLCBzbmFtZTogc3RyaW5nLCBwYXJhOiBhbnksIHRvdGFsOiBudW1iZXIsIG9uZXRpbWU6IG51bWJlciA9IDIwKSB7XHJcblxyXG4gICAgY29uc3QgZGF0YSA9IFtjb2x1bW5zLm1hcChpID0+IGkudGl0bGUpXTtcclxuXHJcbiAgICBjb25zdCBwYWdlQ291bnQgPSBNYXRoLmNlaWwodG90YWwgLyAxMDApO1xyXG5cclxuXHJcbiAgICAvLyDor7fmsYLkv6Hmga9cclxuICAgIGNvbnN0IHJlcXRpbWUgPSBBcnJheShwYWdlQ291bnQpLmZpbGwoe30pLm1hcCgoX2l0ZW06IGFueSwgaWR4OiBudW1iZXIpID0+IHtcclxuICAgICAgcmV0dXJuIHtcclxuICAgICAgICBQQUdFSURYOiBpZHgsXHJcbiAgICAgICAgUEFHRVNJWkU6IDEwMCxcclxuICAgICAgfTtcclxuICAgIH0pO1xyXG5cclxuXHJcblxyXG4gICAgY29uc3Qgb2JzID0gW107XHJcblxyXG4gICAgLy8g5oqKMjDmrKHnmoTor7fmsYLlkIjkuIDotbdcclxuICAgIHJlcXRpbWUuZm9yRWFjaChwYWdlID0+IHtcclxuICAgICAgb2JzLnB1c2godGhpcy5sYnNlcnZpY2UubGJzZXJ2aWNlMihzbmFtZSwgeyBwYXJhLCBwYWdlIH0pKTtcclxuXHJcbiAgICB9KTtcclxuXHJcbiAgICBjb25zdCBhbGxjYWxsID0gZm9ya0pvaW4ob2JzKTtcclxuXHJcbiAgICByZXR1cm4gbmV3IFByb21pc2UoKHJlc29sdmUsIF8pID0+IHtcclxuICAgICAgYWxsY2FsbC5zdWJzY3JpYmUocmVzdWx0cyA9PiB7XHJcblxyXG4gICAgICAgIHJlc3VsdHMuZm9yRWFjaCgoaXRlbTogYW55KSA9PiB7XHJcblxyXG4gICAgICAgICAgaWYgKGl0ZW0uY29kZSA+IDApIHtcclxuXHJcbiAgICAgICAgICAgIGl0ZW0ubWVzc2FnZS5saXN0LmZvckVhY2goaSA9PlxyXG4gICAgICAgICAgICAgIGRhdGEucHVzaChjb2x1bW5zLm1hcChjID0+IGlbYy5pbmRleCBhcyBzdHJpbmddKSksXHJcbiAgICAgICAgICAgICk7XHJcbiAgICAgICAgICB9XHJcblxyXG4gICAgICAgIH0pO1xyXG5cclxuICAgICAgICB0aGlzLnhsc3guZXhwb3J0KHtcclxuICAgICAgICAgIHNoZWV0czogW1xyXG4gICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgZGF0YSxcclxuICAgICAgICAgICAgICBuYW1lOiAnc2hlZXQxJyxcclxuICAgICAgICAgICAgfSxcclxuICAgICAgICAgIF0sXHJcbiAgICAgICAgfSkudGhlbigoKSA9PiB7XHJcbiAgICAgICAgICByZXNvbHZlKG51bGwpO1xyXG4gICAgICAgIH0pXHJcbiAgICAgIH0pO1xyXG5cclxuXHJcbiAgICB9KVxyXG5cclxuICB9XHJcblxyXG5cclxufSJdfQ==