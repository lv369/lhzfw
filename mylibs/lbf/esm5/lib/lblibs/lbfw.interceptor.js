/**
 * @fileoverview added by tsickle
 * Generated from: lib/lblibs/lbfw.interceptor.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Injectable, Injector } from '@angular/core';
import { Router } from '@angular/router';
import { HttpErrorResponse, HttpResponseBase, HttpResponse } from '@angular/common/http';
import { of, throwError } from 'rxjs';
import { mergeMap, catchError } from 'rxjs/operators';
import { NzMessageService, NzNotificationService } from 'ng-zorro-antd';
import { _HttpClient } from '@delon/theme';
import { DA_SERVICE_TOKEN } from '@delon/auth';
import { ServiceInfo } from '../ServiceConfig';
/** @type {?} */
var CODEMESSAGE = {
    200: '服务器成功返回请求的数据。',
    201: '新建或修改数据成功。',
    202: '一个请求已经进入后台排队（异步任务）。',
    204: '删除数据成功。',
    400: '发出的请求有错误，服务器没有进行新建或修改数据的操作。',
    401: '用户未登录或者登录已过期。',
    403: '用户得到授权，但是访问是被禁止的。',
    404: '发出的请求针对的是不存在的记录，服务器没有进行操作。',
    406: '请求的格式不可得。',
    410: '请求的资源被永久删除，且不会再得到的。',
    422: '当创建一个对象时，发生一个验证错误。',
    500: '服务器发生错误，请检查服务器。',
    502: '网关错误。',
    503: '服务不可用，服务器暂时过载或维护。',
    504: '网关超时。',
};
/**
 * 默认HTTP拦截器，其注册细节见 `app.module.ts`
 */
var LbfwInterceptor = /** @class */ (function () {
    function LbfwInterceptor(injector) {
        this.injector = injector;
    }
    Object.defineProperty(LbfwInterceptor.prototype, "msg", {
        get: /**
         * @return {?}
         */
        function () {
            return this.injector.get(NzMessageService);
        },
        enumerable: true,
        configurable: true
    });
    /**
     * @private
     * @param {?} url
     * @return {?}
     */
    LbfwInterceptor.prototype.goTo = /**
     * @private
     * @param {?} url
     * @return {?}
     */
    function (url) {
        var _this = this;
        setTimeout((/**
         * @return {?}
         */
        function () { return _this.injector.get(Router).navigateByUrl(url); }));
    };
    Object.defineProperty(LbfwInterceptor.prototype, "sinfo", {
        get: /**
         * @return {?}
         */
        function () {
            return this.injector.get(ServiceInfo);
        },
        enumerable: true,
        configurable: true
    });
    /**
     * @private
     * @param {?} ev
     * @return {?}
     */
    LbfwInterceptor.prototype.checkStatus = /**
     * @private
     * @param {?} ev
     * @return {?}
     */
    function (ev) {
        if (ev.status >= 200 && ev.status < 300)
            return;
        /** @type {?} */
        var errortext = CODEMESSAGE[ev.status] || ev.statusText;
        this.injector.get(NzNotificationService).error("\u8BF7\u6C42\u9519\u8BEF " + ev.status, errortext);
    };
    /**
     * @private
     * @param {?} ev
     * @return {?}
     */
    LbfwInterceptor.prototype.handleData = /**
     * @private
     * @param {?} ev
     * @return {?}
     */
    function (ev) {
        // 可能会因为 `throw` 导出无法执行 `_HttpClient` 的 `end()` 操作
        if (ev.status > 0) {
            this.injector.get(_HttpClient).end();
        }
        this.checkStatus(ev);
        // 业务处理：一些通用操作
        switch (ev.status) {
            case 200:
                // 处理未登录的错误 {code: -322, errmsg: "未登录或者登陆已经过期！"}
                if (ev instanceof HttpResponse) {
                    /** @type {?} */
                    var body = ev.body;
                    if (body && body.code === -322) {
                        this.msg.error("未登录或者登陆已经过期！");
                        // 继续抛出错误中断后续所有 Pipe、subscribe 操作，因此：
                        // this.http.get('/').subscribe() 并不会触发
                        this.goTo('/passport/login');
                        break;
                    }
                    else {
                        // 重新修改 `body` 内容为 `response` 内容，对于绝大多数场景已经无须再关心业务状态码
                        // 或者依然保持完整的格式
                        return of(ev);
                    }
                }
                break;
            case 401: // 未登录状态码
                // 请求错误 401: https://preview.pro.ant.design/api/401 用户没有权限（令牌、用户名、密码错误）。
                ((/** @type {?} */ (this.injector.get(DA_SERVICE_TOKEN)))).clear();
                this.goTo('/passport/login');
                break;
            case 403:
            case 404:
            case 500:
                this.goTo("/exception/" + ev.status);
                break;
            default:
                if (ev instanceof HttpErrorResponse) {
                    console.warn('未可知错误，大部分是由于后端不支持CORS或无效配置引起', ev);
                    return throwError(ev);
                }
                break;
        }
        return of(ev);
    };
    /**
     * @param {?} req
     * @param {?} next
     * @return {?}
     */
    LbfwInterceptor.prototype.intercept = /**
     * @param {?} req
     * @param {?} next
     * @return {?}
     */
    function (req, next) {
        var _this = this;
        // 统一加上服务端前缀
        /** @type {?} */
        var url = this.sinfo.baseUrl + req.url;
        // if (!url.startsWith('https://') && !url.startsWith('http://')) {
        //   url = environment.SERVER_URL + url;
        // }
        // 加上统一的请求参数
        /** @type {?} */
        var sname = req.headers.get("SNAME");
        /** @type {?} */
        var para = JSON.stringify(req.body);
        /** @type {?} */
        var appid = this.sinfo.APPID;
        /** @type {?} */
        var timestamp = this.sinfo.getTimestamp().toString();
        /** @type {?} */
        var sign = this.sinfo.getSign(sname, timestamp, para);
        /** @type {?} */
        var token = this.sinfo.LOGTOKEN;
        /** @type {?} */
        var version = this.sinfo.VERSION;
        /** @type {?} */
        var headers = req.headers.set("APPID", appid).set("TIMESTAMP", timestamp).set("SIGN", sign).set("LOGINTOKEN", token).set("VERSION", version);
        /** @type {?} */
        var newReq = req.clone({
            url: url, headers: headers
        });
        return next.handle(newReq).pipe(mergeMap((/**
         * @param {?} event
         * @return {?}
         */
        function (event) {
            // 允许统一对请求错误处理
            if (event instanceof HttpResponseBase)
                return _this.handleData(event);
            // 若一切都正常，则后续操作
            return of(event);
        })), catchError((/**
         * @param {?} err
         * @return {?}
         */
        function (err) { return _this.handleData(err); })));
    };
    LbfwInterceptor.decorators = [
        { type: Injectable }
    ];
    /** @nocollapse */
    LbfwInterceptor.ctorParameters = function () { return [
        { type: Injector }
    ]; };
    return LbfwInterceptor;
}());
export { LbfwInterceptor };
if (false) {
    /**
     * @type {?}
     * @private
     */
    LbfwInterceptor.prototype.injector;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibGJmdy5pbnRlcmNlcHRvci5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2xiZi8iLCJzb3VyY2VzIjpbImxpYi9sYmxpYnMvbGJmdy5pbnRlcmNlcHRvci50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7OztBQUFBLE9BQU8sRUFBRSxVQUFVLEVBQUUsUUFBUSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQ3JELE9BQU8sRUFBRSxNQUFNLEVBQUUsTUFBTSxpQkFBaUIsQ0FBQztBQUN6QyxPQUFPLEVBQTZDLGlCQUFpQixFQUFhLGdCQUFnQixFQUFFLFlBQVksRUFBRSxNQUFNLHNCQUFzQixDQUFDO0FBQy9JLE9BQU8sRUFBYyxFQUFFLEVBQUUsVUFBVSxFQUFFLE1BQU0sTUFBTSxDQUFDO0FBQ2xELE9BQU8sRUFBRSxRQUFRLEVBQUUsVUFBVSxFQUFFLE1BQU0sZ0JBQWdCLENBQUM7QUFDdEQsT0FBTyxFQUFFLGdCQUFnQixFQUFFLHFCQUFxQixFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQ3hFLE9BQU8sRUFBRSxXQUFXLEVBQUUsTUFBTSxjQUFjLENBQUM7QUFDM0MsT0FBTyxFQUFFLGdCQUFnQixFQUFpQixNQUFNLGFBQWEsQ0FBQztBQUM5RCxPQUFPLEVBQUUsV0FBVyxFQUFFLE1BQU0sa0JBQWtCLENBQUM7O0lBR3pDLFdBQVcsR0FBRztJQUNsQixHQUFHLEVBQUUsZUFBZTtJQUNwQixHQUFHLEVBQUUsWUFBWTtJQUNqQixHQUFHLEVBQUUscUJBQXFCO0lBQzFCLEdBQUcsRUFBRSxTQUFTO0lBQ2QsR0FBRyxFQUFFLDZCQUE2QjtJQUNsQyxHQUFHLEVBQUUsZUFBZTtJQUNwQixHQUFHLEVBQUUsbUJBQW1CO0lBQ3hCLEdBQUcsRUFBRSw0QkFBNEI7SUFDakMsR0FBRyxFQUFFLFdBQVc7SUFDaEIsR0FBRyxFQUFFLHFCQUFxQjtJQUMxQixHQUFHLEVBQUUsb0JBQW9CO0lBQ3pCLEdBQUcsRUFBRSxpQkFBaUI7SUFDdEIsR0FBRyxFQUFFLE9BQU87SUFDWixHQUFHLEVBQUUsbUJBQW1CO0lBQ3hCLEdBQUcsRUFBRSxPQUFPO0NBQ2I7Ozs7QUFLRDtJQUVFLHlCQUFvQixRQUFrQjtRQUFsQixhQUFRLEdBQVIsUUFBUSxDQUFVO0lBQUksQ0FBQztJQUUzQyxzQkFBSSxnQ0FBRzs7OztRQUFQO1lBQ0UsT0FBTyxJQUFJLENBQUMsUUFBUSxDQUFDLEdBQUcsQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDO1FBQzdDLENBQUM7OztPQUFBOzs7Ozs7SUFFTyw4QkFBSTs7Ozs7SUFBWixVQUFhLEdBQVc7UUFBeEIsaUJBRUM7UUFEQyxVQUFVOzs7UUFBQyxjQUFNLE9BQUEsS0FBSSxDQUFDLFFBQVEsQ0FBQyxHQUFHLENBQUMsTUFBTSxDQUFDLENBQUMsYUFBYSxDQUFDLEdBQUcsQ0FBQyxFQUE1QyxDQUE0QyxFQUFDLENBQUM7SUFDakUsQ0FBQztJQUdELHNCQUFJLGtDQUFLOzs7O1FBQVQ7WUFDRSxPQUFPLElBQUksQ0FBQyxRQUFRLENBQUMsR0FBRyxDQUFDLFdBQVcsQ0FBQyxDQUFDO1FBQ3hDLENBQUM7OztPQUFBOzs7Ozs7SUFHTyxxQ0FBVzs7Ozs7SUFBbkIsVUFBb0IsRUFBb0I7UUFDdEMsSUFBSSxFQUFFLENBQUMsTUFBTSxJQUFJLEdBQUcsSUFBSSxFQUFFLENBQUMsTUFBTSxHQUFHLEdBQUc7WUFBRSxPQUFPOztZQUUxQyxTQUFTLEdBQUcsV0FBVyxDQUFDLEVBQUUsQ0FBQyxNQUFNLENBQUMsSUFBSSxFQUFFLENBQUMsVUFBVTtRQUN6RCxJQUFJLENBQUMsUUFBUSxDQUFDLEdBQUcsQ0FBQyxxQkFBcUIsQ0FBQyxDQUFDLEtBQUssQ0FDNUMsOEJBQVEsRUFBRSxDQUFDLE1BQVEsRUFDbkIsU0FBUyxDQUNWLENBQUM7SUFDSixDQUFDOzs7Ozs7SUFFTyxvQ0FBVTs7Ozs7SUFBbEIsVUFBbUIsRUFBb0I7UUFFckMsa0RBQWtEO1FBQ2xELElBQUksRUFBRSxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUU7WUFDakIsSUFBSSxDQUFDLFFBQVEsQ0FBQyxHQUFHLENBQUMsV0FBVyxDQUFDLENBQUMsR0FBRyxFQUFFLENBQUM7U0FDdEM7UUFDRCxJQUFJLENBQUMsV0FBVyxDQUFDLEVBQUUsQ0FBQyxDQUFDO1FBQ3JCLGNBQWM7UUFDZCxRQUFRLEVBQUUsQ0FBQyxNQUFNLEVBQUU7WUFDakIsS0FBSyxHQUFHO2dCQUVOLGdEQUFnRDtnQkFDaEQsSUFBSSxFQUFFLFlBQVksWUFBWSxFQUFFOzt3QkFFeEIsSUFBSSxHQUFRLEVBQUUsQ0FBQyxJQUFJO29CQUN6QixJQUFJLElBQUksSUFBSSxJQUFJLENBQUMsSUFBSSxLQUFLLENBQUMsR0FBRyxFQUFFO3dCQUM5QixJQUFJLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxjQUFjLENBQUMsQ0FBQzt3QkFDL0IscUNBQXFDO3dCQUNyQyx1Q0FBdUM7d0JBQ3ZDLElBQUksQ0FBQyxJQUFJLENBQUMsaUJBQWlCLENBQUMsQ0FBQzt3QkFDN0IsTUFBTTtxQkFDUDt5QkFBTTt3QkFDTCxxREFBcUQ7d0JBQ3JELGNBQWM7d0JBQ2QsT0FBTyxFQUFFLENBQUMsRUFBRSxDQUFDLENBQUM7cUJBQ2Y7aUJBQ0Y7Z0JBRUQsTUFBTTtZQUNSLEtBQUssR0FBRyxFQUFFLFNBQVM7Z0JBQ2pCLHdFQUF3RTtnQkFDeEUsQ0FBQyxtQkFBQSxJQUFJLENBQUMsUUFBUSxDQUFDLEdBQUcsQ0FBQyxnQkFBZ0IsQ0FBQyxFQUFpQixDQUFDLENBQUMsS0FBSyxFQUFFLENBQUM7Z0JBQy9ELElBQUksQ0FBQyxJQUFJLENBQUMsaUJBQWlCLENBQUMsQ0FBQztnQkFDN0IsTUFBTTtZQUNSLEtBQUssR0FBRyxDQUFDO1lBQ1QsS0FBSyxHQUFHLENBQUM7WUFDVCxLQUFLLEdBQUc7Z0JBQ04sSUFBSSxDQUFDLElBQUksQ0FBQyxnQkFBYyxFQUFFLENBQUMsTUFBUSxDQUFDLENBQUM7Z0JBQ3JDLE1BQU07WUFDUjtnQkFDRSxJQUFJLEVBQUUsWUFBWSxpQkFBaUIsRUFBRTtvQkFDbkMsT0FBTyxDQUFDLElBQUksQ0FBQyw4QkFBOEIsRUFBRSxFQUFFLENBQUMsQ0FBQztvQkFDakQsT0FBTyxVQUFVLENBQUMsRUFBRSxDQUFDLENBQUM7aUJBQ3ZCO2dCQUNELE1BQU07U0FDVDtRQUNELE9BQU8sRUFBRSxDQUFDLEVBQUUsQ0FBQyxDQUFDO0lBQ2hCLENBQUM7Ozs7OztJQUVELG1DQUFTOzs7OztJQUFULFVBQVUsR0FBcUIsRUFBRSxJQUFpQjtRQUFsRCxpQkFrQ0M7OztZQWhDTyxHQUFHLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxPQUFPLEdBQUcsR0FBRyxDQUFDLEdBQUc7Ozs7OztZQU1sQyxLQUFLLEdBQUcsR0FBRyxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsT0FBTyxDQUFDOztZQUNoQyxJQUFJLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDOztZQUMvQixLQUFLLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxLQUFLOztZQUN4QixTQUFTLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxZQUFZLEVBQUUsQ0FBQyxRQUFRLEVBQUU7O1lBQ2hELElBQUksR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxLQUFLLEVBQUUsU0FBUyxFQUFFLElBQUksQ0FBQzs7WUFDakQsS0FBSyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsUUFBUTs7WUFDM0IsT0FBTyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsT0FBTzs7WUFFNUIsT0FBTyxHQUFHLEdBQUcsQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLE9BQU8sRUFBRSxLQUFLLENBQUMsQ0FBQyxHQUFHLENBQUMsV0FBVyxFQUFFLFNBQVMsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxNQUFNLEVBQUUsSUFBSSxDQUFDLENBQUMsR0FBRyxDQUFDLFlBQVksRUFBRSxLQUFLLENBQUMsQ0FBQyxHQUFHLENBQUMsU0FBUyxFQUFFLE9BQU8sQ0FBQzs7WUFHeEksTUFBTSxHQUFHLEdBQUcsQ0FBQyxLQUFLLENBQUM7WUFDdkIsR0FBRyxLQUFBLEVBQUUsT0FBTyxTQUFBO1NBQ2IsQ0FBQztRQUdGLE9BQU8sSUFBSSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsQ0FBQyxJQUFJLENBQzdCLFFBQVE7Ozs7UUFBQyxVQUFDLEtBQVU7WUFDbEIsY0FBYztZQUNkLElBQUksS0FBSyxZQUFZLGdCQUFnQjtnQkFDbkMsT0FBTyxLQUFJLENBQUMsVUFBVSxDQUFDLEtBQUssQ0FBQyxDQUFDO1lBQ2hDLGVBQWU7WUFDZixPQUFPLEVBQUUsQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUNuQixDQUFDLEVBQUMsRUFDRixVQUFVOzs7O1FBQUMsVUFBQyxHQUFzQixJQUFLLE9BQUEsS0FBSSxDQUFDLFVBQVUsQ0FBQyxHQUFHLENBQUMsRUFBcEIsQ0FBb0IsRUFBQyxDQUM3RCxDQUFDO0lBQ0osQ0FBQzs7Z0JBL0dGLFVBQVU7Ozs7Z0JBaENVLFFBQVE7O0lBZ0o3QixzQkFBQztDQUFBLEFBaEhELElBZ0hDO1NBL0dZLGVBQWU7Ozs7OztJQUNkLG1DQUEwQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEluamVjdGFibGUsIEluamVjdG9yIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IFJvdXRlciB9IGZyb20gJ0Bhbmd1bGFyL3JvdXRlcic7XHJcbmltcG9ydCB7IEh0dHBJbnRlcmNlcHRvciwgSHR0cFJlcXVlc3QsIEh0dHBIYW5kbGVyLCBIdHRwRXJyb3JSZXNwb25zZSwgSHR0cEV2ZW50LCBIdHRwUmVzcG9uc2VCYXNlLCBIdHRwUmVzcG9uc2UgfSBmcm9tICdAYW5ndWxhci9jb21tb24vaHR0cCc7XHJcbmltcG9ydCB7IE9ic2VydmFibGUsIG9mLCB0aHJvd0Vycm9yIH0gZnJvbSAncnhqcyc7XHJcbmltcG9ydCB7IG1lcmdlTWFwLCBjYXRjaEVycm9yIH0gZnJvbSAncnhqcy9vcGVyYXRvcnMnO1xyXG5pbXBvcnQgeyBOek1lc3NhZ2VTZXJ2aWNlLCBOek5vdGlmaWNhdGlvblNlcnZpY2UgfSBmcm9tICduZy16b3Jyby1hbnRkJztcclxuaW1wb3J0IHsgX0h0dHBDbGllbnQgfSBmcm9tICdAZGVsb24vdGhlbWUnO1xyXG5pbXBvcnQgeyBEQV9TRVJWSUNFX1RPS0VOLCBJVG9rZW5TZXJ2aWNlIH0gZnJvbSAnQGRlbG9uL2F1dGgnO1xyXG5pbXBvcnQgeyBTZXJ2aWNlSW5mbyB9IGZyb20gJy4uL1NlcnZpY2VDb25maWcnO1xyXG5cclxuXHJcbmNvbnN0IENPREVNRVNTQUdFID0ge1xyXG4gIDIwMDogJ+acjeWKoeWZqOaIkOWKn+i/lOWbnuivt+axgueahOaVsOaNruOAgicsXHJcbiAgMjAxOiAn5paw5bu65oiW5L+u5pS55pWw5o2u5oiQ5Yqf44CCJyxcclxuICAyMDI6ICfkuIDkuKror7fmsYLlt7Lnu4/ov5vlhaXlkI7lj7DmjpLpmJ/vvIjlvILmraXku7vliqHvvInjgIInLFxyXG4gIDIwNDogJ+WIoOmZpOaVsOaNruaIkOWKn+OAgicsXHJcbiAgNDAwOiAn5Y+R5Ye655qE6K+35rGC5pyJ6ZSZ6K+v77yM5pyN5Yqh5Zmo5rKh5pyJ6L+b6KGM5paw5bu65oiW5L+u5pS55pWw5o2u55qE5pON5L2c44CCJyxcclxuICA0MDE6ICfnlKjmiLfmnKrnmbvlvZXmiJbogIXnmbvlvZXlt7Lov4fmnJ/jgIInLFxyXG4gIDQwMzogJ+eUqOaIt+W+l+WIsOaOiOadg++8jOS9huaYr+iuv+mXruaYr+iiq+emgeatoueahOOAgicsXHJcbiAgNDA0OiAn5Y+R5Ye655qE6K+35rGC6ZKI5a+555qE5piv5LiN5a2Y5Zyo55qE6K6w5b2V77yM5pyN5Yqh5Zmo5rKh5pyJ6L+b6KGM5pON5L2c44CCJyxcclxuICA0MDY6ICfor7fmsYLnmoTmoLzlvI/kuI3lj6/lvpfjgIInLFxyXG4gIDQxMDogJ+ivt+axgueahOi1hOa6kOiiq+awuOS5heWIoOmZpO+8jOS4lOS4jeS8muWGjeW+l+WIsOeahOOAgicsXHJcbiAgNDIyOiAn5b2T5Yib5bu65LiA5Liq5a+56LGh5pe277yM5Y+R55Sf5LiA5Liq6aqM6K+B6ZSZ6K+v44CCJyxcclxuICA1MDA6ICfmnI3liqHlmajlj5HnlJ/plJnor6/vvIzor7fmo4Dmn6XmnI3liqHlmajjgIInLFxyXG4gIDUwMjogJ+e9keWFs+mUmeivr+OAgicsXHJcbiAgNTAzOiAn5pyN5Yqh5LiN5Y+v55So77yM5pyN5Yqh5Zmo5pqC5pe26L+H6L295oiW57u05oqk44CCJyxcclxuICA1MDQ6ICfnvZHlhbPotoXml7bjgIInLFxyXG59O1xyXG5cclxuLyoqXHJcbiAqIOm7mOiupEhUVFDmi6bmiKrlmajvvIzlhbbms6jlhoznu4boioLop4EgYGFwcC5tb2R1bGUudHNgXHJcbiAqL1xyXG5ASW5qZWN0YWJsZSgpXHJcbmV4cG9ydCBjbGFzcyBMYmZ3SW50ZXJjZXB0b3IgaW1wbGVtZW50cyBIdHRwSW50ZXJjZXB0b3Ige1xyXG4gIGNvbnN0cnVjdG9yKHByaXZhdGUgaW5qZWN0b3I6IEluamVjdG9yKSB7IH1cclxuXHJcbiAgZ2V0IG1zZygpOiBOek1lc3NhZ2VTZXJ2aWNlIHtcclxuICAgIHJldHVybiB0aGlzLmluamVjdG9yLmdldChOek1lc3NhZ2VTZXJ2aWNlKTtcclxuICB9XHJcblxyXG4gIHByaXZhdGUgZ29Ubyh1cmw6IHN0cmluZykge1xyXG4gICAgc2V0VGltZW91dCgoKSA9PiB0aGlzLmluamVjdG9yLmdldChSb3V0ZXIpLm5hdmlnYXRlQnlVcmwodXJsKSk7XHJcbiAgfVxyXG5cclxuXHJcbiAgZ2V0IHNpbmZvKCk6IFNlcnZpY2VJbmZvIHtcclxuICAgIHJldHVybiB0aGlzLmluamVjdG9yLmdldChTZXJ2aWNlSW5mbyk7XHJcbiAgfVxyXG5cclxuXHJcbiAgcHJpdmF0ZSBjaGVja1N0YXR1cyhldjogSHR0cFJlc3BvbnNlQmFzZSkge1xyXG4gICAgaWYgKGV2LnN0YXR1cyA+PSAyMDAgJiYgZXYuc3RhdHVzIDwgMzAwKSByZXR1cm47XHJcblxyXG4gICAgY29uc3QgZXJyb3J0ZXh0ID0gQ09ERU1FU1NBR0VbZXYuc3RhdHVzXSB8fCBldi5zdGF0dXNUZXh0O1xyXG4gICAgdGhpcy5pbmplY3Rvci5nZXQoTnpOb3RpZmljYXRpb25TZXJ2aWNlKS5lcnJvcihcclxuICAgICAgYOivt+axgumUmeivryAke2V2LnN0YXR1c31gLFxyXG4gICAgICBlcnJvcnRleHRcclxuICAgICk7XHJcbiAgfVxyXG5cclxuICBwcml2YXRlIGhhbmRsZURhdGEoZXY6IEh0dHBSZXNwb25zZUJhc2UpOiBPYnNlcnZhYmxlPGFueT4ge1xyXG5cclxuICAgIC8vIOWPr+iDveS8muWboOS4uiBgdGhyb3dgIOWvvOWHuuaXoOazleaJp+ihjCBgX0h0dHBDbGllbnRgIOeahCBgZW5kKClgIOaTjeS9nFxyXG4gICAgaWYgKGV2LnN0YXR1cyA+IDApIHtcclxuICAgICAgdGhpcy5pbmplY3Rvci5nZXQoX0h0dHBDbGllbnQpLmVuZCgpO1xyXG4gICAgfVxyXG4gICAgdGhpcy5jaGVja1N0YXR1cyhldik7XHJcbiAgICAvLyDkuJrliqHlpITnkIbvvJrkuIDkupvpgJrnlKjmk43kvZxcclxuICAgIHN3aXRjaCAoZXYuc3RhdHVzKSB7XHJcbiAgICAgIGNhc2UgMjAwOlxyXG5cclxuICAgICAgICAvLyDlpITnkIbmnKrnmbvlvZXnmoTplJnor68ge2NvZGU6IC0zMjIsIGVycm1zZzogXCLmnKrnmbvlvZXmiJbogIXnmbvpmYblt7Lnu4/ov4fmnJ/vvIFcIn1cclxuICAgICAgICBpZiAoZXYgaW5zdGFuY2VvZiBIdHRwUmVzcG9uc2UpIHtcclxuXHJcbiAgICAgICAgICBjb25zdCBib2R5OiBhbnkgPSBldi5ib2R5O1xyXG4gICAgICAgICAgaWYgKGJvZHkgJiYgYm9keS5jb2RlID09PSAtMzIyKSB7XHJcbiAgICAgICAgICAgIHRoaXMubXNnLmVycm9yKFwi5pyq55m75b2V5oiW6ICF55m76ZmG5bey57uP6L+H5pyf77yBXCIpO1xyXG4gICAgICAgICAgICAvLyDnu6fnu63mipvlh7rplJnor6/kuK3mlq3lkI7nu63miYDmnIkgUGlwZeOAgXN1YnNjcmliZSDmk43kvZzvvIzlm6DmraTvvJpcclxuICAgICAgICAgICAgLy8gdGhpcy5odHRwLmdldCgnLycpLnN1YnNjcmliZSgpIOW5tuS4jeS8muinpuWPkVxyXG4gICAgICAgICAgICB0aGlzLmdvVG8oJy9wYXNzcG9ydC9sb2dpbicpO1xyXG4gICAgICAgICAgICBicmVhaztcclxuICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIC8vIOmHjeaWsOS/ruaUuSBgYm9keWAg5YaF5a655Li6IGByZXNwb25zZWAg5YaF5a6577yM5a+55LqO57ud5aSn5aSa5pWw5Zy65pmv5bey57uP5peg6aG75YaN5YWz5b+D5Lia5Yqh54q25oCB56CBXHJcbiAgICAgICAgICAgIC8vIOaIluiAheS+neeEtuS/neaMgeWujOaVtOeahOagvOW8j1xyXG4gICAgICAgICAgICByZXR1cm4gb2YoZXYpO1xyXG4gICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgYnJlYWs7XHJcbiAgICAgIGNhc2UgNDAxOiAvLyDmnKrnmbvlvZXnirbmgIHnoIFcclxuICAgICAgICAvLyDor7fmsYLplJnor68gNDAxOiBodHRwczovL3ByZXZpZXcucHJvLmFudC5kZXNpZ24vYXBpLzQwMSDnlKjmiLfmsqHmnInmnYPpmZDvvIjku6TniYzjgIHnlKjmiLflkI3jgIHlr4bnoIHplJnor6/vvInjgIJcclxuICAgICAgICAodGhpcy5pbmplY3Rvci5nZXQoREFfU0VSVklDRV9UT0tFTikgYXMgSVRva2VuU2VydmljZSkuY2xlYXIoKTtcclxuICAgICAgICB0aGlzLmdvVG8oJy9wYXNzcG9ydC9sb2dpbicpO1xyXG4gICAgICAgIGJyZWFrO1xyXG4gICAgICBjYXNlIDQwMzpcclxuICAgICAgY2FzZSA0MDQ6XHJcbiAgICAgIGNhc2UgNTAwOlxyXG4gICAgICAgIHRoaXMuZ29UbyhgL2V4Y2VwdGlvbi8ke2V2LnN0YXR1c31gKTtcclxuICAgICAgICBicmVhaztcclxuICAgICAgZGVmYXVsdDpcclxuICAgICAgICBpZiAoZXYgaW5zdGFuY2VvZiBIdHRwRXJyb3JSZXNwb25zZSkge1xyXG4gICAgICAgICAgY29uc29sZS53YXJuKCfmnKrlj6/nn6XplJnor6/vvIzlpKfpg6jliIbmmK/nlLHkuo7lkI7nq6/kuI3mlK/mjIFDT1JT5oiW5peg5pWI6YWN572u5byV6LW3JywgZXYpO1xyXG4gICAgICAgICAgcmV0dXJuIHRocm93RXJyb3IoZXYpO1xyXG4gICAgICAgIH1cclxuICAgICAgICBicmVhaztcclxuICAgIH1cclxuICAgIHJldHVybiBvZihldik7XHJcbiAgfVxyXG5cclxuICBpbnRlcmNlcHQocmVxOiBIdHRwUmVxdWVzdDxhbnk+LCBuZXh0OiBIdHRwSGFuZGxlcik6IE9ic2VydmFibGU8SHR0cEV2ZW50PGFueT4+IHtcclxuICAgIC8vIOe7n+S4gOWKoOS4iuacjeWKoeerr+WJjee8gFxyXG4gICAgY29uc3QgdXJsID0gdGhpcy5zaW5mby5iYXNlVXJsICsgcmVxLnVybDtcclxuICAgIC8vIGlmICghdXJsLnN0YXJ0c1dpdGgoJ2h0dHBzOi8vJykgJiYgIXVybC5zdGFydHNXaXRoKCdodHRwOi8vJykpIHtcclxuICAgIC8vICAgdXJsID0gZW52aXJvbm1lbnQuU0VSVkVSX1VSTCArIHVybDtcclxuICAgIC8vIH1cclxuXHJcbiAgICAvLyDliqDkuIrnu5/kuIDnmoTor7fmsYLlj4LmlbBcclxuICAgIGNvbnN0IHNuYW1lID0gcmVxLmhlYWRlcnMuZ2V0KFwiU05BTUVcIik7XHJcbiAgICBjb25zdCBwYXJhID0gSlNPTi5zdHJpbmdpZnkocmVxLmJvZHkpO1xyXG4gICAgY29uc3QgYXBwaWQgPSB0aGlzLnNpbmZvLkFQUElEO1xyXG4gICAgY29uc3QgdGltZXN0YW1wID0gdGhpcy5zaW5mby5nZXRUaW1lc3RhbXAoKS50b1N0cmluZygpO1xyXG4gICAgY29uc3Qgc2lnbiA9IHRoaXMuc2luZm8uZ2V0U2lnbihzbmFtZSwgdGltZXN0YW1wLCBwYXJhKTtcclxuICAgIGNvbnN0IHRva2VuID0gdGhpcy5zaW5mby5MT0dUT0tFTjtcclxuICAgIGNvbnN0IHZlcnNpb24gPSB0aGlzLnNpbmZvLlZFUlNJT047XHJcblxyXG4gICAgY29uc3QgaGVhZGVycyA9IHJlcS5oZWFkZXJzLnNldChcIkFQUElEXCIsIGFwcGlkKS5zZXQoXCJUSU1FU1RBTVBcIiwgdGltZXN0YW1wKS5zZXQoXCJTSUdOXCIsIHNpZ24pLnNldChcIkxPR0lOVE9LRU5cIiwgdG9rZW4pLnNldChcIlZFUlNJT05cIiwgdmVyc2lvbik7XHJcblxyXG5cclxuICAgIGNvbnN0IG5ld1JlcSA9IHJlcS5jbG9uZSh7XHJcbiAgICAgIHVybCwgaGVhZGVyc1xyXG4gICAgfSk7XHJcblxyXG5cclxuICAgIHJldHVybiBuZXh0LmhhbmRsZShuZXdSZXEpLnBpcGUoXHJcbiAgICAgIG1lcmdlTWFwKChldmVudDogYW55KSA9PiB7XHJcbiAgICAgICAgLy8g5YWB6K6457uf5LiA5a+56K+35rGC6ZSZ6K+v5aSE55CGXHJcbiAgICAgICAgaWYgKGV2ZW50IGluc3RhbmNlb2YgSHR0cFJlc3BvbnNlQmFzZSlcclxuICAgICAgICAgIHJldHVybiB0aGlzLmhhbmRsZURhdGEoZXZlbnQpO1xyXG4gICAgICAgIC8vIOiLpeS4gOWIh+mDveato+W4uO+8jOWImeWQjue7reaTjeS9nFxyXG4gICAgICAgIHJldHVybiBvZihldmVudCk7XHJcbiAgICAgIH0pLFxyXG4gICAgICBjYXRjaEVycm9yKChlcnI6IEh0dHBFcnJvclJlc3BvbnNlKSA9PiB0aGlzLmhhbmRsZURhdGEoZXJyKSksXHJcbiAgICApO1xyXG4gIH1cclxufVxyXG4iXX0=