/**
 * @fileoverview added by tsickle
 * Generated from: lib/lblibs/Lbjk.service.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpEventType, HttpResponse, HttpRequest } from '@angular/common/http';
import { catchError, debounceTime, distinctUntilChanged } from 'rxjs/operators';
import { ServiceInfo } from '../ServiceConfig';
import * as i0 from "@angular/core";
import * as i1 from "@angular/common/http";
import * as i2 from "../ServiceConfig";
var HttpService = /** @class */ (function () {
    function HttpService(httpClient, serviceInfo) {
        this.httpClient = httpClient;
        this.serviceInfo = serviceInfo;
    }
    /**
     * 登录服务
     * @param username 用户名
     * @param password 用户密码
     * @returns 登录结果 Promise<any>
     */
    /**
     * 登录服务
     * @param {?} username 用户名
     * @param {?} password 用户密码
     * @return {?} 登录结果 Promise<any>
     */
    HttpService.prototype.login = /**
     * 登录服务
     * @param {?} username 用户名
     * @param {?} password 用户密码
     * @return {?} 登录结果 Promise<any>
     */
    function (username, password) {
        var _this = this;
        /** @type {?} */
        var c = localStorage.getItem('usercache');
        if (!c) {
            c = "{}";
        }
        /** @type {?} */
        var usercache = JSON.parse(c);
        /** @type {?} */
        var ROLEID = null;
        if (usercache && usercache[username]) {
            ROLEID = usercache[username].roleid;
        }
        return new Promise((/**
         * @param {?} resolve
         * @param {?} _
         * @return {?}
         */
        function (resolve, _) {
            _this.lbservice("FR_login", { "para": { "USERID": username, "PWD": _this.serviceInfo.pwdmd5(password), ROLEID: ROLEID } }, 500, true).then((/**
             * @param {?} resdata
             * @return {?}
             */
            function (resdata) {
                // 设置登录token
                if (resdata.code > 0) {
                    _this.serviceInfo.LOGTOKEN = resdata.message.LOGINTOKEN;
                    _this.serviceInfo.saveToken(resdata.message.LOGINTOKEN);
                    _this.serviceInfo.setUser({ aac003: resdata.message.NAME, name: username, dept: resdata.message.DEPT, roleid: resdata.message.ROLEID });
                    if (!usercache) {
                        usercache = {};
                    }
                    if (!usercache[username]) {
                        usercache[username] = { li: [], cdr: [] };
                    }
                    usercache[username].li.push(new Date());
                    usercache[username].roleid = resdata.message.ROLEID;
                    localStorage.setItem('usercache', JSON.stringify(usercache));
                }
                resolve(resdata);
            }));
        }));
    };
    /**
     * @param {?} roleid
     * @return {?}
     */
    HttpService.prototype.changeRole = /**
     * @param {?} roleid
     * @return {?}
     */
    function (roleid) {
        var _this = this;
        /** @type {?} */
        var c = localStorage.getItem('usercache');
        if (!c) {
            c = "{}";
        }
        /** @type {?} */
        var usercache = JSON.parse(c);
        return new Promise((/**
         * @param {?} resolve
         * @param {?} _
         * @return {?}
         */
        function (resolve, _) {
            _this.lbservice("FR_cdr", { "para": { ROLEID: roleid } }).then((/**
             * @param {?} resdata
             * @return {?}
             */
            function (resdata) {
                // 设置登录token
                if (resdata.code > 0) {
                    _this.serviceInfo.LOGTOKEN = resdata.message.LOGINTOKEN;
                    _this.serviceInfo.user.roleid = roleid;
                    if (!usercache) {
                        usercache = {};
                    }
                    if (!usercache[_this.serviceInfo.user.name]) {
                        usercache[_this.serviceInfo.user.name] = { li: [], cdr: [] };
                    }
                    usercache[_this.serviceInfo.user.name].cdr.push(new Date());
                    usercache[_this.serviceInfo.user.name].roleid = resdata.message.ROLEID;
                    localStorage.setItem('usercache', JSON.stringify(usercache));
                }
                resolve(resdata);
            }));
        }));
    };
    /**
     * @param {?} sname
     * @param {?} para
     * @param {?=} debounce
     * @param {?=} anonymous
     * @return {?}
     */
    HttpService.prototype.lbservice2 = /**
     * @param {?} sname
     * @param {?} para
     * @param {?=} debounce
     * @param {?=} anonymous
     * @return {?}
     */
    function (sname, para, debounce, anonymous) {
        if (debounce === void 0) { debounce = 0; }
        if (anonymous === void 0) { anonymous = false; }
        /** @type {?} */
        var httpOptions = {
            headers: new HttpHeaders({
                'SNAME': sname
            })
        };
        /** @type {?} */
        var url = this.serviceInfo.molssUrl;
        if (anonymous) {
            url = url + '?_allow_anonymous=true';
        }
        return this.httpClient.post(url, para, httpOptions);
    };
    /**
     *  访问服务端
     * @param sname 服务名
     * @param para 入参
     * @param debounce 防抖设置，默认0
     * @param anonymous 是否匿名访问，默认否
     */
    /**
     *  访问服务端
     * @param {?} sname 服务名
     * @param {?} para 入参
     * @param {?=} debounce 防抖设置，默认0
     * @param {?=} anonymous 是否匿名访问，默认否
     * @return {?}
     */
    HttpService.prototype.lbservice = /**
     *  访问服务端
     * @param {?} sname 服务名
     * @param {?} para 入参
     * @param {?=} debounce 防抖设置，默认0
     * @param {?=} anonymous 是否匿名访问，默认否
     * @return {?}
     */
    function (sname, para, debounce, anonymous) {
        var _this = this;
        if (debounce === void 0) { debounce = 0; }
        if (anonymous === void 0) { anonymous = false; }
        /** @type {?} */
        var httpOptions = {
            headers: new HttpHeaders({
                'SNAME': sname
            })
        };
        /** @type {?} */
        var url = this.serviceInfo.molssUrl;
        if (anonymous) {
            url = url + '?_allow_anonymous=true';
        }
        return new Promise((/**
         * @param {?} resolve
         * @param {?} _
         * @return {?}
         */
        function (resolve, _) {
            /** @type {?} */
            var result = { code: 0, errmsg: "请求失败", msg: {} };
            _this.httpClient.post(url, para, httpOptions)
                .pipe(debounceTime(debounce), distinctUntilChanged(), 
            // 接收其他拦截器后产生的异常消息
            catchError((/**
             * @param {?} error
             * @return {?}
             */
            function (error) {
                console.log("调用失败", error);
                resolve({ code: -100, errmsg: "请求失败" });
                return error;
            })))
                .subscribe((/**
             * @param {?} resdata
             * @return {?}
             */
            function (resdata) {
                // setting language data
                // console.log(resdata)
                result = resdata;
            }), (/**
             * @return {?}
             */
            function () { }), (/**
             * @return {?}
             */
            function () { resolve(result); }));
        }));
    };
    /**
     * 文件上传类服务
     *   只支持单文件上传
     * @param sname 服务名
     * @param item 文件上传信息
     * @param para 参数
     */
    /**
     * 文件上传类服务
     *   只支持单文件上传
     * @param {?} sname 服务名
     * @param {?} item 文件上传信息
     * @param {?} para 参数
     * @return {?}
     */
    HttpService.prototype.lbfileupload = /**
     * 文件上传类服务
     *   只支持单文件上传
     * @param {?} sname 服务名
     * @param {?} item 文件上传信息
     * @param {?} para 参数
     * @return {?}
     */
    function (sname, item, para) {
        /** @type {?} */
        var formData = new FormData();
        formData.append('file', (/** @type {?} */ (item.file)));
        formData.append('paras', JSON.stringify({ para: para }));
        /** @type {?} */
        var req = new HttpRequest('POST', item.action ? item.action : this.serviceInfo.uploadurl, formData, {
            headers: new HttpHeaders({
                'SNAME': sname
            }),
            reportProgress: true,
            withCredentials: true
        });
        // 始终返回一个 `Subscription` 对象，nz-upload 会在适当时机自动取消订阅
        return this.httpClient.request(req).subscribe((/**
         * @param {?} event
         * @return {?}
         */
        function (event) {
            if (event.type === HttpEventType.UploadProgress) {
                if (event.total > 0) {
                    // tslint:disable-next-line:no-any
                    ((/** @type {?} */ (event))).percent = event.loaded / event.total * 100;
                }
                // 处理上传进度条，必须指定 `percent` 属性来表示进度
                item.onProgress(event, item.file);
            }
            else if (event instanceof HttpResponse) {
                // 处理成功
                item.onSuccess(event.body, item.file, event);
            }
        }), (/**
         * @param {?} err
         * @return {?}
         */
        function (err) {
            // 处理失败
            item.onError(err, item.file);
        }));
    };
    HttpService.decorators = [
        { type: Injectable, args: [{ providedIn: 'root', },] }
    ];
    /** @nocollapse */
    HttpService.ctorParameters = function () { return [
        { type: HttpClient },
        { type: ServiceInfo }
    ]; };
    /** @nocollapse */ HttpService.ngInjectableDef = i0.ɵɵdefineInjectable({ factory: function HttpService_Factory() { return new HttpService(i0.ɵɵinject(i1.HttpClient), i0.ɵɵinject(i2.ServiceInfo)); }, token: HttpService, providedIn: "root" });
    return HttpService;
}());
export { HttpService };
if (false) {
    /**
     * @type {?}
     * @private
     */
    HttpService.prototype.httpClient;
    /**
     * @type {?}
     * @private
     */
    HttpService.prototype.serviceInfo;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiTGJqay5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vbGJmLyIsInNvdXJjZXMiOlsibGliL2xibGlicy9MYmprLnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7QUFBQSxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQzNDLE9BQU8sRUFBRSxVQUFVLEVBQUUsV0FBVyxFQUFhLGFBQWEsRUFBRSxZQUFZLEVBQUUsV0FBVyxFQUFFLE1BQU0sc0JBQXNCLENBQUM7QUFDcEgsT0FBTyxFQUFFLFVBQVUsRUFBRSxZQUFZLEVBQUUsb0JBQW9CLEVBQUUsTUFBTSxnQkFBZ0IsQ0FBQztBQUdoRixPQUFPLEVBQUUsV0FBVyxFQUFFLE1BQU0sa0JBQWtCLENBQUM7Ozs7QUFJL0M7SUFJSSxxQkFDWSxVQUFzQixFQUN0QixXQUF3QjtRQUR4QixlQUFVLEdBQVYsVUFBVSxDQUFZO1FBQ3RCLGdCQUFXLEdBQVgsV0FBVyxDQUFhO0lBRXBDLENBQUM7SUFFRDs7Ozs7T0FLRzs7Ozs7OztJQUNILDJCQUFLOzs7Ozs7SUFBTCxVQUFNLFFBQWdCLEVBQUUsUUFBZ0I7UUFBeEMsaUJBdUNDOztZQXJDTyxDQUFDLEdBQUcsWUFBWSxDQUFDLE9BQU8sQ0FBQyxXQUFXLENBQUM7UUFDekMsSUFBSSxDQUFDLENBQUMsRUFBRTtZQUNKLENBQUMsR0FBRyxJQUFJLENBQUM7U0FDWjs7WUFFRyxTQUFTLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUM7O1lBQ3pCLE1BQU0sR0FBRyxJQUFJO1FBQ2pCLElBQUksU0FBUyxJQUFJLFNBQVMsQ0FBQyxRQUFRLENBQUMsRUFBRTtZQUNsQyxNQUFNLEdBQUcsU0FBUyxDQUFDLFFBQVEsQ0FBQyxDQUFDLE1BQU0sQ0FBQztTQUN2QztRQUVELE9BQU8sSUFBSSxPQUFPOzs7OztRQUFDLFVBQUMsT0FBTyxFQUFFLENBQUM7WUFFMUIsS0FBSSxDQUFDLFNBQVMsQ0FBQyxVQUFVLEVBQUUsRUFBRSxNQUFNLEVBQUUsRUFBRSxRQUFRLEVBQUUsUUFBUSxFQUFFLEtBQUssRUFBRSxLQUFJLENBQUMsV0FBVyxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsRUFBRSxNQUFNLFFBQUEsRUFBRSxFQUFFLEVBQUUsR0FBRyxFQUFFLElBQUksQ0FBQyxDQUFDLElBQUk7Ozs7WUFBQyxVQUFDLE9BQU87Z0JBRXJJLFlBQVk7Z0JBQ1osSUFBSSxPQUFPLENBQUMsSUFBSSxHQUFHLENBQUMsRUFBRTtvQkFDbEIsS0FBSSxDQUFDLFdBQVcsQ0FBQyxRQUFRLEdBQUcsT0FBTyxDQUFDLE9BQU8sQ0FBQyxVQUFVLENBQUM7b0JBQ3ZELEtBQUksQ0FBQyxXQUFXLENBQUMsU0FBUyxDQUFDLE9BQU8sQ0FBQyxPQUFPLENBQUMsVUFBVSxDQUFDLENBQUM7b0JBQ3ZELEtBQUksQ0FBQyxXQUFXLENBQUMsT0FBTyxDQUFDLEVBQUUsTUFBTSxFQUFFLE9BQU8sQ0FBQyxPQUFPLENBQUMsSUFBSSxFQUFFLElBQUksRUFBRSxRQUFRLEVBQUUsSUFBSSxFQUFFLE9BQU8sQ0FBQyxPQUFPLENBQUMsSUFBSSxFQUFFLE1BQU0sRUFBRSxPQUFPLENBQUMsT0FBTyxDQUFDLE1BQU0sRUFBRSxDQUFDLENBQUM7b0JBQ3ZJLElBQUksQ0FBQyxTQUFTLEVBQUU7d0JBQ1osU0FBUyxHQUFHLEVBQUUsQ0FBQztxQkFDbEI7b0JBQ0QsSUFBSSxDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUMsRUFBRTt3QkFDdEIsU0FBUyxDQUFDLFFBQVEsQ0FBQyxHQUFHLEVBQUUsRUFBRSxFQUFFLEVBQUUsRUFBRSxHQUFHLEVBQUUsRUFBRSxFQUFFLENBQUM7cUJBQzdDO29CQUVELFNBQVMsQ0FBQyxRQUFRLENBQUMsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLElBQUksSUFBSSxFQUFFLENBQUMsQ0FBQztvQkFDeEMsU0FBUyxDQUFDLFFBQVEsQ0FBQyxDQUFDLE1BQU0sR0FBRyxPQUFPLENBQUMsT0FBTyxDQUFDLE1BQU0sQ0FBQztvQkFDcEQsWUFBWSxDQUFDLE9BQU8sQ0FBQyxXQUFXLEVBQUUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDO2lCQUNoRTtnQkFFRCxPQUFPLENBQUMsT0FBTyxDQUFDLENBQUM7WUFFckIsQ0FBQyxFQUFDLENBQUE7UUFFTixDQUFDLEVBQUMsQ0FBQTtJQUNOLENBQUM7Ozs7O0lBRUQsZ0NBQVU7Ozs7SUFBVixVQUFXLE1BQWM7UUFBekIsaUJBbUNDOztZQWpDTyxDQUFDLEdBQUcsWUFBWSxDQUFDLE9BQU8sQ0FBQyxXQUFXLENBQUM7UUFDekMsSUFBSSxDQUFDLENBQUMsRUFBRTtZQUNKLENBQUMsR0FBRyxJQUFJLENBQUM7U0FDWjs7WUFFRyxTQUFTLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUM7UUFHN0IsT0FBTyxJQUFJLE9BQU87Ozs7O1FBQUMsVUFBQyxPQUFPLEVBQUUsQ0FBQztZQUUxQixLQUFJLENBQUMsU0FBUyxDQUFDLFFBQVEsRUFBRSxFQUFFLE1BQU0sRUFBRSxFQUFFLE1BQU0sRUFBRSxNQUFNLEVBQUUsRUFBRSxDQUFDLENBQUMsSUFBSTs7OztZQUFDLFVBQUMsT0FBTztnQkFFbEUsWUFBWTtnQkFDWixJQUFJLE9BQU8sQ0FBQyxJQUFJLEdBQUcsQ0FBQyxFQUFFO29CQUNsQixLQUFJLENBQUMsV0FBVyxDQUFDLFFBQVEsR0FBRyxPQUFPLENBQUMsT0FBTyxDQUFDLFVBQVUsQ0FBQztvQkFDdkQsS0FBSSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsTUFBTSxHQUFHLE1BQU0sQ0FBQztvQkFDdEMsSUFBSSxDQUFDLFNBQVMsRUFBRTt3QkFDWixTQUFTLEdBQUcsRUFBRSxDQUFDO3FCQUNsQjtvQkFDRCxJQUFJLENBQUMsU0FBUyxDQUFDLEtBQUksQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxFQUFFO3dCQUN4QyxTQUFTLENBQUMsS0FBSSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsRUFBRSxFQUFFLEVBQUUsRUFBRSxFQUFFLEdBQUcsRUFBRSxFQUFFLEVBQUUsQ0FBQztxQkFDL0Q7b0JBRUQsU0FBUyxDQUFDLEtBQUksQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsSUFBSSxJQUFJLEVBQUUsQ0FBQyxDQUFDO29CQUMzRCxTQUFTLENBQUMsS0FBSSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsTUFBTSxHQUFHLE9BQU8sQ0FBQyxPQUFPLENBQUMsTUFBTSxDQUFDO29CQUN0RSxZQUFZLENBQUMsT0FBTyxDQUFDLFdBQVcsRUFBRSxJQUFJLENBQUMsU0FBUyxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUM7aUJBQ2hFO2dCQUVELE9BQU8sQ0FBQyxPQUFPLENBQUMsQ0FBQztZQUVyQixDQUFDLEVBQUMsQ0FBQTtRQUVOLENBQUMsRUFBQyxDQUFBO0lBQ04sQ0FBQzs7Ozs7Ozs7SUFFRCxnQ0FBVTs7Ozs7OztJQUFWLFVBQVcsS0FBYSxFQUFFLElBQVEsRUFBRSxRQUFvQixFQUFFLFNBQTBCO1FBQWhELHlCQUFBLEVBQUEsWUFBb0I7UUFBRSwwQkFBQSxFQUFBLGlCQUEwQjs7WUFDMUUsV0FBVyxHQUFHO1lBQ2hCLE9BQU8sRUFBRSxJQUFJLFdBQVcsQ0FBQztnQkFDckIsT0FBTyxFQUFFLEtBQUs7YUFDakIsQ0FBQztTQUNMOztZQUVHLEdBQUcsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLFFBQVE7UUFDbkMsSUFBSSxTQUFTLEVBQUU7WUFDWCxHQUFHLEdBQUcsR0FBRyxHQUFHLHdCQUF3QixDQUFDO1NBQ3hDO1FBRUQsT0FBTyxJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxHQUFHLEVBQUUsSUFBSSxFQUFFLFdBQVcsQ0FBQyxDQUFDO0lBRXhELENBQUM7SUFFRDs7Ozs7O09BTUc7Ozs7Ozs7OztJQUVILCtCQUFTOzs7Ozs7OztJQUFULFVBQVUsS0FBYSxFQUFFLElBQVEsRUFBRSxRQUFvQixFQUFFLFNBQTBCO1FBQW5GLGlCQXVDQztRQXZDa0MseUJBQUEsRUFBQSxZQUFvQjtRQUFFLDBCQUFBLEVBQUEsaUJBQTBCOztZQUV6RSxXQUFXLEdBQUc7WUFDaEIsT0FBTyxFQUFFLElBQUksV0FBVyxDQUFDO2dCQUNyQixPQUFPLEVBQUUsS0FBSzthQUNqQixDQUFDO1NBQ0w7O1lBRUcsR0FBRyxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsUUFBUTtRQUNuQyxJQUFJLFNBQVMsRUFBRTtZQUNYLEdBQUcsR0FBRyxHQUFHLEdBQUcsd0JBQXdCLENBQUM7U0FDeEM7UUFFRCxPQUFPLElBQUksT0FBTzs7Ozs7UUFBQyxVQUFDLE9BQU8sRUFBRSxDQUFDOztnQkFFdEIsTUFBTSxHQUFRLEVBQUUsSUFBSSxFQUFFLENBQUMsRUFBRSxNQUFNLEVBQUUsTUFBTSxFQUFFLEdBQUcsRUFBRSxFQUFFLEVBQUU7WUFFdEQsS0FBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsR0FBRyxFQUFFLElBQUksRUFBRSxXQUFXLENBQUM7aUJBQ3ZDLElBQUksQ0FDRCxZQUFZLENBQUMsUUFBUSxDQUFDLEVBQ3RCLG9CQUFvQixFQUFFO1lBQ3RCLGtCQUFrQjtZQUNsQixVQUFVOzs7O1lBQUMsVUFBQyxLQUFLO2dCQUNiLE9BQU8sQ0FBQyxHQUFHLENBQUMsTUFBTSxFQUFFLEtBQUssQ0FBQyxDQUFBO2dCQUMxQixPQUFPLENBQUMsRUFBRSxJQUFJLEVBQUUsQ0FBQyxHQUFHLEVBQUUsTUFBTSxFQUFFLE1BQU0sRUFBRSxDQUFDLENBQUM7Z0JBQ3hDLE9BQU8sS0FBSyxDQUFDO1lBQ2pCLENBQUMsRUFBQyxDQUNMO2lCQUNBLFNBQVM7Ozs7WUFDTixVQUFDLE9BQU87Z0JBQ0osd0JBQXdCO2dCQUN4Qix1QkFBdUI7Z0JBQ3ZCLE1BQU0sR0FBRyxPQUFPLENBQUM7WUFFckIsQ0FBQzs7O1lBQ0QsY0FBUSxDQUFDOzs7WUFDVCxjQUFRLE9BQU8sQ0FBQyxNQUFNLENBQUMsQ0FBQSxDQUFDLENBQUMsRUFDNUIsQ0FBQztRQUNWLENBQUMsRUFBQyxDQUFDO0lBQ1AsQ0FBQztJQUVEOzs7Ozs7T0FNRzs7Ozs7Ozs7O0lBQ0gsa0NBQVk7Ozs7Ozs7O0lBQVosVUFBYSxLQUFhLEVBQUUsSUFBbUIsRUFBRSxJQUFZOztZQUduRCxRQUFRLEdBQUcsSUFBSSxRQUFRLEVBQUU7UUFDL0IsUUFBUSxDQUFDLE1BQU0sQ0FBQyxNQUFNLEVBQUUsbUJBQUEsSUFBSSxDQUFDLElBQUksRUFBTyxDQUFDLENBQUM7UUFDMUMsUUFBUSxDQUFDLE1BQU0sQ0FBQyxPQUFPLEVBQUUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxFQUFFLElBQUksTUFBQSxFQUFFLENBQUMsQ0FBQyxDQUFDOztZQUc3QyxHQUFHLEdBQUcsSUFBSSxXQUFXLENBQUMsTUFBTSxFQUFFLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsU0FBUyxFQUFFLFFBQVEsRUFBRTtZQUNsRyxPQUFPLEVBQUUsSUFBSSxXQUFXLENBQUM7Z0JBQ3JCLE9BQU8sRUFBRSxLQUFLO2FBQ2pCLENBQUM7WUFDRixjQUFjLEVBQUUsSUFBSTtZQUNwQixlQUFlLEVBQUUsSUFBSTtTQUN4QixDQUFDO1FBRUYsa0RBQWtEO1FBQ2xELE9BQU8sSUFBSSxDQUFDLFVBQVUsQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLENBQUMsU0FBUzs7OztRQUFDLFVBQUMsS0FBb0I7WUFDL0QsSUFBSSxLQUFLLENBQUMsSUFBSSxLQUFLLGFBQWEsQ0FBQyxjQUFjLEVBQUU7Z0JBQzdDLElBQUksS0FBSyxDQUFDLEtBQUssR0FBRyxDQUFDLEVBQUU7b0JBQ2pCLGtDQUFrQztvQkFDbEMsQ0FBQyxtQkFBQSxLQUFLLEVBQU8sQ0FBQyxDQUFDLE9BQU8sR0FBRyxLQUFLLENBQUMsTUFBTSxHQUFHLEtBQUssQ0FBQyxLQUFLLEdBQUcsR0FBRyxDQUFDO2lCQUM3RDtnQkFDRCxpQ0FBaUM7Z0JBQ2pDLElBQUksQ0FBQyxVQUFVLENBQUMsS0FBSyxFQUFFLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQzthQUNyQztpQkFBTSxJQUFJLEtBQUssWUFBWSxZQUFZLEVBQUU7Z0JBQ3RDLE9BQU87Z0JBQ1AsSUFBSSxDQUFDLFNBQVMsQ0FBQyxLQUFLLENBQUMsSUFBSSxFQUFFLElBQUksQ0FBQyxJQUFJLEVBQUUsS0FBSyxDQUFDLENBQUM7YUFDaEQ7UUFDTCxDQUFDOzs7O1FBQUUsVUFBQyxHQUFHO1lBQ0gsT0FBTztZQUNQLElBQUksQ0FBQyxPQUFPLENBQUMsR0FBRyxFQUFFLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUNqQyxDQUFDLEVBQUMsQ0FBQztJQUNQLENBQUM7O2dCQXZNSixVQUFVLFNBQUMsRUFBRSxVQUFVLEVBQUUsTUFBTSxHQUFHOzs7O2dCQVIxQixVQUFVO2dCQUlWLFdBQVc7OztzQkFMcEI7Q0FrTkMsQUF6TUQsSUF5TUM7U0F4TVksV0FBVzs7Ozs7O0lBSWhCLGlDQUE4Qjs7Ozs7SUFDOUIsa0NBQWdDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSW5qZWN0YWJsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBIdHRwQ2xpZW50LCBIdHRwSGVhZGVycywgSHR0cEV2ZW50LCBIdHRwRXZlbnRUeXBlLCBIdHRwUmVzcG9uc2UsIEh0dHBSZXF1ZXN0IH0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uL2h0dHAnO1xyXG5pbXBvcnQgeyBjYXRjaEVycm9yLCBkZWJvdW5jZVRpbWUsIGRpc3RpbmN0VW50aWxDaGFuZ2VkIH0gZnJvbSAncnhqcy9vcGVyYXRvcnMnO1xyXG5pbXBvcnQgeyBVcGxvYWRYSFJBcmdzIH0gZnJvbSAnbmctem9ycm8tYW50ZCc7XHJcbmltcG9ydCB7IFN1YnNjcmlwdGlvbiwgT2JzZXJ2YWJsZSB9IGZyb20gJ3J4anMnO1xyXG5pbXBvcnQgeyBTZXJ2aWNlSW5mbyB9IGZyb20gJy4uL1NlcnZpY2VDb25maWcnO1xyXG5cclxuXHJcblxyXG5ASW5qZWN0YWJsZSh7IHByb3ZpZGVkSW46ICdyb290JywgfSlcclxuZXhwb3J0IGNsYXNzIEh0dHBTZXJ2aWNlIHtcclxuXHJcblxyXG4gICAgY29uc3RydWN0b3IoXHJcbiAgICAgICAgcHJpdmF0ZSBodHRwQ2xpZW50OiBIdHRwQ2xpZW50LFxyXG4gICAgICAgIHByaXZhdGUgc2VydmljZUluZm86IFNlcnZpY2VJbmZvLFxyXG4gICAgKSB7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiDnmbvlvZXmnI3liqFcclxuICAgICAqIEBwYXJhbSB1c2VybmFtZSDnlKjmiLflkI1cclxuICAgICAqIEBwYXJhbSBwYXNzd29yZCDnlKjmiLflr4bnoIFcclxuICAgICAqIEByZXR1cm5zIOeZu+W9lee7k+aenCBQcm9taXNlPGFueT5cclxuICAgICAqL1xyXG4gICAgbG9naW4odXNlcm5hbWU6IHN0cmluZywgcGFzc3dvcmQ6IHN0cmluZyk6IFByb21pc2U8YW55PiB7XHJcblxyXG4gICAgICAgIGxldCBjID0gbG9jYWxTdG9yYWdlLmdldEl0ZW0oJ3VzZXJjYWNoZScpO1xyXG4gICAgICAgIGlmICghYykge1xyXG4gICAgICAgICAgICBjID0gXCJ7fVwiO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgbGV0IHVzZXJjYWNoZSA9IEpTT04ucGFyc2UoYyk7XHJcbiAgICAgICAgbGV0IFJPTEVJRCA9IG51bGw7XHJcbiAgICAgICAgaWYgKHVzZXJjYWNoZSAmJiB1c2VyY2FjaGVbdXNlcm5hbWVdKSB7XHJcbiAgICAgICAgICAgIFJPTEVJRCA9IHVzZXJjYWNoZVt1c2VybmFtZV0ucm9sZWlkO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgcmV0dXJuIG5ldyBQcm9taXNlKChyZXNvbHZlLCBfKSA9PiB7XHJcblxyXG4gICAgICAgICAgICB0aGlzLmxic2VydmljZShcIkZSX2xvZ2luXCIsIHsgXCJwYXJhXCI6IHsgXCJVU0VSSURcIjogdXNlcm5hbWUsIFwiUFdEXCI6IHRoaXMuc2VydmljZUluZm8ucHdkbWQ1KHBhc3N3b3JkKSwgUk9MRUlEIH0gfSwgNTAwLCB0cnVlKS50aGVuKChyZXNkYXRhKSA9PiB7XHJcblxyXG4gICAgICAgICAgICAgICAgLy8g6K6+572u55m75b2VdG9rZW5cclxuICAgICAgICAgICAgICAgIGlmIChyZXNkYXRhLmNvZGUgPiAwKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5zZXJ2aWNlSW5mby5MT0dUT0tFTiA9IHJlc2RhdGEubWVzc2FnZS5MT0dJTlRPS0VOO1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuc2VydmljZUluZm8uc2F2ZVRva2VuKHJlc2RhdGEubWVzc2FnZS5MT0dJTlRPS0VOKTtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLnNlcnZpY2VJbmZvLnNldFVzZXIoeyBhYWMwMDM6IHJlc2RhdGEubWVzc2FnZS5OQU1FLCBuYW1lOiB1c2VybmFtZSwgZGVwdDogcmVzZGF0YS5tZXNzYWdlLkRFUFQsIHJvbGVpZDogcmVzZGF0YS5tZXNzYWdlLlJPTEVJRCB9KTtcclxuICAgICAgICAgICAgICAgICAgICBpZiAoIXVzZXJjYWNoZSkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB1c2VyY2FjaGUgPSB7fTtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKCF1c2VyY2FjaGVbdXNlcm5hbWVdKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHVzZXJjYWNoZVt1c2VybmFtZV0gPSB7IGxpOiBbXSwgY2RyOiBbXSB9O1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgICAgICAgICAgdXNlcmNhY2hlW3VzZXJuYW1lXS5saS5wdXNoKG5ldyBEYXRlKCkpO1xyXG4gICAgICAgICAgICAgICAgICAgIHVzZXJjYWNoZVt1c2VybmFtZV0ucm9sZWlkID0gcmVzZGF0YS5tZXNzYWdlLlJPTEVJRDtcclxuICAgICAgICAgICAgICAgICAgICBsb2NhbFN0b3JhZ2Uuc2V0SXRlbSgndXNlcmNhY2hlJywgSlNPTi5zdHJpbmdpZnkodXNlcmNhY2hlKSk7XHJcbiAgICAgICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAgICAgcmVzb2x2ZShyZXNkYXRhKTtcclxuXHJcbiAgICAgICAgICAgIH0pXHJcblxyXG4gICAgICAgIH0pXHJcbiAgICB9XHJcblxyXG4gICAgY2hhbmdlUm9sZShyb2xlaWQ6IHN0cmluZyk6IFByb21pc2U8YW55PiB7XHJcblxyXG4gICAgICAgIGxldCBjID0gbG9jYWxTdG9yYWdlLmdldEl0ZW0oJ3VzZXJjYWNoZScpO1xyXG4gICAgICAgIGlmICghYykge1xyXG4gICAgICAgICAgICBjID0gXCJ7fVwiO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgbGV0IHVzZXJjYWNoZSA9IEpTT04ucGFyc2UoYyk7XHJcblxyXG5cclxuICAgICAgICByZXR1cm4gbmV3IFByb21pc2UoKHJlc29sdmUsIF8pID0+IHtcclxuXHJcbiAgICAgICAgICAgIHRoaXMubGJzZXJ2aWNlKFwiRlJfY2RyXCIsIHsgXCJwYXJhXCI6IHsgUk9MRUlEOiByb2xlaWQgfSB9KS50aGVuKChyZXNkYXRhKSA9PiB7XHJcblxyXG4gICAgICAgICAgICAgICAgLy8g6K6+572u55m75b2VdG9rZW5cclxuICAgICAgICAgICAgICAgIGlmIChyZXNkYXRhLmNvZGUgPiAwKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5zZXJ2aWNlSW5mby5MT0dUT0tFTiA9IHJlc2RhdGEubWVzc2FnZS5MT0dJTlRPS0VOO1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuc2VydmljZUluZm8udXNlci5yb2xlaWQgPSByb2xlaWQ7XHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKCF1c2VyY2FjaGUpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdXNlcmNhY2hlID0ge307XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgIGlmICghdXNlcmNhY2hlW3RoaXMuc2VydmljZUluZm8udXNlci5uYW1lXSkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB1c2VyY2FjaGVbdGhpcy5zZXJ2aWNlSW5mby51c2VyLm5hbWVdID0geyBsaTogW10sIGNkcjogW10gfTtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAgICAgICAgIHVzZXJjYWNoZVt0aGlzLnNlcnZpY2VJbmZvLnVzZXIubmFtZV0uY2RyLnB1c2gobmV3IERhdGUoKSk7XHJcbiAgICAgICAgICAgICAgICAgICAgdXNlcmNhY2hlW3RoaXMuc2VydmljZUluZm8udXNlci5uYW1lXS5yb2xlaWQgPSByZXNkYXRhLm1lc3NhZ2UuUk9MRUlEO1xyXG4gICAgICAgICAgICAgICAgICAgIGxvY2FsU3RvcmFnZS5zZXRJdGVtKCd1c2VyY2FjaGUnLCBKU09OLnN0cmluZ2lmeSh1c2VyY2FjaGUpKTtcclxuICAgICAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgICAgICByZXNvbHZlKHJlc2RhdGEpO1xyXG5cclxuICAgICAgICAgICAgfSlcclxuXHJcbiAgICAgICAgfSlcclxuICAgIH1cclxuXHJcbiAgICBsYnNlcnZpY2UyKHNuYW1lOiBzdHJpbmcsIHBhcmE6IHt9LCBkZWJvdW5jZTogbnVtYmVyID0gMCwgYW5vbnltb3VzOiBib29sZWFuID0gZmFsc2UpOiBPYnNlcnZhYmxlPHt9PiB7XHJcbiAgICAgICAgY29uc3QgaHR0cE9wdGlvbnMgPSB7XHJcbiAgICAgICAgICAgIGhlYWRlcnM6IG5ldyBIdHRwSGVhZGVycyh7XHJcbiAgICAgICAgICAgICAgICAnU05BTUUnOiBzbmFtZVxyXG4gICAgICAgICAgICB9KVxyXG4gICAgICAgIH07XHJcblxyXG4gICAgICAgIGxldCB1cmwgPSB0aGlzLnNlcnZpY2VJbmZvLm1vbHNzVXJsO1xyXG4gICAgICAgIGlmIChhbm9ueW1vdXMpIHtcclxuICAgICAgICAgICAgdXJsID0gdXJsICsgJz9fYWxsb3dfYW5vbnltb3VzPXRydWUnO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgcmV0dXJuIHRoaXMuaHR0cENsaWVudC5wb3N0KHVybCwgcGFyYSwgaHR0cE9wdGlvbnMpO1xyXG5cclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqICDorr/pl67mnI3liqHnq69cclxuICAgICAqIEBwYXJhbSBzbmFtZSDmnI3liqHlkI1cclxuICAgICAqIEBwYXJhbSBwYXJhIOWFpeWPglxyXG4gICAgICogQHBhcmFtIGRlYm91bmNlIOmYsuaKluiuvue9ru+8jOm7mOiupDBcclxuICAgICAqIEBwYXJhbSBhbm9ueW1vdXMg5piv5ZCm5Yy/5ZCN6K6/6Zeu77yM6buY6K6k5ZCmXHJcbiAgICAgKi9cclxuXHJcbiAgICBsYnNlcnZpY2Uoc25hbWU6IHN0cmluZywgcGFyYToge30sIGRlYm91bmNlOiBudW1iZXIgPSAwLCBhbm9ueW1vdXM6IGJvb2xlYW4gPSBmYWxzZSk6IFByb21pc2U8YW55PiB7XHJcblxyXG4gICAgICAgIGNvbnN0IGh0dHBPcHRpb25zID0ge1xyXG4gICAgICAgICAgICBoZWFkZXJzOiBuZXcgSHR0cEhlYWRlcnMoe1xyXG4gICAgICAgICAgICAgICAgJ1NOQU1FJzogc25hbWVcclxuICAgICAgICAgICAgfSlcclxuICAgICAgICB9O1xyXG5cclxuICAgICAgICBsZXQgdXJsID0gdGhpcy5zZXJ2aWNlSW5mby5tb2xzc1VybDtcclxuICAgICAgICBpZiAoYW5vbnltb3VzKSB7XHJcbiAgICAgICAgICAgIHVybCA9IHVybCArICc/X2FsbG93X2Fub255bW91cz10cnVlJztcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIHJldHVybiBuZXcgUHJvbWlzZSgocmVzb2x2ZSwgXykgPT4ge1xyXG5cclxuICAgICAgICAgICAgbGV0IHJlc3VsdDogYW55ID0geyBjb2RlOiAwLCBlcnJtc2c6IFwi6K+35rGC5aSx6LSlXCIsIG1zZzoge30gfTtcclxuXHJcbiAgICAgICAgICAgIHRoaXMuaHR0cENsaWVudC5wb3N0KHVybCwgcGFyYSwgaHR0cE9wdGlvbnMpXHJcbiAgICAgICAgICAgICAgICAucGlwZShcclxuICAgICAgICAgICAgICAgICAgICBkZWJvdW5jZVRpbWUoZGVib3VuY2UpLFxyXG4gICAgICAgICAgICAgICAgICAgIGRpc3RpbmN0VW50aWxDaGFuZ2VkKCksXHJcbiAgICAgICAgICAgICAgICAgICAgLy8g5o6l5pS25YW25LuW5oum5oiq5Zmo5ZCO5Lqn55Sf55qE5byC5bi45raI5oGvXHJcbiAgICAgICAgICAgICAgICAgICAgY2F0Y2hFcnJvcigoZXJyb3IpID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgY29uc29sZS5sb2coXCLosIPnlKjlpLHotKVcIiwgZXJyb3IpXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHJlc29sdmUoeyBjb2RlOiAtMTAwLCBlcnJtc2c6IFwi6K+35rGC5aSx6LSlXCIgfSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiBlcnJvcjtcclxuICAgICAgICAgICAgICAgICAgICB9KSxcclxuICAgICAgICAgICAgICAgIClcclxuICAgICAgICAgICAgICAgIC5zdWJzY3JpYmUoXHJcbiAgICAgICAgICAgICAgICAgICAgKHJlc2RhdGEpID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgLy8gc2V0dGluZyBsYW5ndWFnZSBkYXRhXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIC8vIGNvbnNvbGUubG9nKHJlc2RhdGEpXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHJlc3VsdCA9IHJlc2RhdGE7XHJcblxyXG4gICAgICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICAgICAgICAgKCkgPT4geyB9LFxyXG4gICAgICAgICAgICAgICAgICAgICgpID0+IHsgcmVzb2x2ZShyZXN1bHQpIH0sXHJcbiAgICAgICAgICAgICAgICApO1xyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICog5paH5Lu25LiK5Lyg57G75pyN5YqhXHJcbiAgICAgKiAgIOWPquaUr+aMgeWNleaWh+S7tuS4iuS8oFxyXG4gICAgICogQHBhcmFtIHNuYW1lIOacjeWKoeWQjVxyXG4gICAgICogQHBhcmFtIGl0ZW0g5paH5Lu25LiK5Lyg5L+h5oGvXHJcbiAgICAgKiBAcGFyYW0gcGFyYSDlj4LmlbBcclxuICAgICAqL1xyXG4gICAgbGJmaWxldXBsb2FkKHNuYW1lOiBzdHJpbmcsIGl0ZW06IFVwbG9hZFhIUkFyZ3MsIHBhcmE6IG9iamVjdCk6IFN1YnNjcmlwdGlvbiB7XHJcblxyXG5cclxuICAgICAgICBjb25zdCBmb3JtRGF0YSA9IG5ldyBGb3JtRGF0YSgpO1xyXG4gICAgICAgIGZvcm1EYXRhLmFwcGVuZCgnZmlsZScsIGl0ZW0uZmlsZSBhcyBhbnkpO1xyXG4gICAgICAgIGZvcm1EYXRhLmFwcGVuZCgncGFyYXMnLCBKU09OLnN0cmluZ2lmeSh7IHBhcmEgfSkpO1xyXG5cclxuXHJcbiAgICAgICAgY29uc3QgcmVxID0gbmV3IEh0dHBSZXF1ZXN0KCdQT1NUJywgaXRlbS5hY3Rpb24gPyBpdGVtLmFjdGlvbiA6IHRoaXMuc2VydmljZUluZm8udXBsb2FkdXJsLCBmb3JtRGF0YSwge1xyXG4gICAgICAgICAgICBoZWFkZXJzOiBuZXcgSHR0cEhlYWRlcnMoe1xyXG4gICAgICAgICAgICAgICAgJ1NOQU1FJzogc25hbWVcclxuICAgICAgICAgICAgfSksXHJcbiAgICAgICAgICAgIHJlcG9ydFByb2dyZXNzOiB0cnVlLFxyXG4gICAgICAgICAgICB3aXRoQ3JlZGVudGlhbHM6IHRydWVcclxuICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgLy8g5aeL57uI6L+U5Zue5LiA5LiqIGBTdWJzY3JpcHRpb25gIOWvueixoe+8jG56LXVwbG9hZCDkvJrlnKjpgILlvZPml7bmnLroh6rliqjlj5bmtojorqLpmIVcclxuICAgICAgICByZXR1cm4gdGhpcy5odHRwQ2xpZW50LnJlcXVlc3QocmVxKS5zdWJzY3JpYmUoKGV2ZW50OiBIdHRwRXZlbnQ8e30+KSA9PiB7XHJcbiAgICAgICAgICAgIGlmIChldmVudC50eXBlID09PSBIdHRwRXZlbnRUeXBlLlVwbG9hZFByb2dyZXNzKSB7XHJcbiAgICAgICAgICAgICAgICBpZiAoZXZlbnQudG90YWwgPiAwKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgLy8gdHNsaW50OmRpc2FibGUtbmV4dC1saW5lOm5vLWFueVxyXG4gICAgICAgICAgICAgICAgICAgIChldmVudCBhcyBhbnkpLnBlcmNlbnQgPSBldmVudC5sb2FkZWQgLyBldmVudC50b3RhbCAqIDEwMDtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIC8vIOWkhOeQhuS4iuS8oOi/m+W6puadoe+8jOW/hemhu+aMh+WumiBgcGVyY2VudGAg5bGe5oCn5p2l6KGo56S66L+b5bqmXHJcbiAgICAgICAgICAgICAgICBpdGVtLm9uUHJvZ3Jlc3MoZXZlbnQsIGl0ZW0uZmlsZSk7XHJcbiAgICAgICAgICAgIH0gZWxzZSBpZiAoZXZlbnQgaW5zdGFuY2VvZiBIdHRwUmVzcG9uc2UpIHtcclxuICAgICAgICAgICAgICAgIC8vIOWkhOeQhuaIkOWKn1xyXG4gICAgICAgICAgICAgICAgaXRlbS5vblN1Y2Nlc3MoZXZlbnQuYm9keSwgaXRlbS5maWxlLCBldmVudCk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9LCAoZXJyKSA9PiB7XHJcbiAgICAgICAgICAgIC8vIOWkhOeQhuWksei0pVxyXG4gICAgICAgICAgICBpdGVtLm9uRXJyb3IoZXJyLCBpdGVtLmZpbGUpO1xyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxufSJdfQ==