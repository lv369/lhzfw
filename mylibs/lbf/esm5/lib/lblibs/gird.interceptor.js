/**
 * @fileoverview added by tsickle
 * Generated from: lib/lblibs/gird.interceptor.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
import { Injectable, Injector } from '@angular/core';
import { HttpResponse, HttpResponseBase, } from '@angular/common/http';
import { of } from 'rxjs';
import { mergeMap } from 'rxjs/operators';
import { NzMessageService } from 'ng-zorro-antd';
import { ServiceInfo } from '../ServiceConfig';
var GridInterceptor = /** @class */ (function () {
    function GridInterceptor(injector) {
        this.injector = injector;
    }
    /**
     * @param {?} req
     * @param {?} next
     * @return {?}
     */
    GridInterceptor.prototype.intercept = /**
     * @param {?} req
     * @param {?} next
     * @return {?}
     */
    function (req, next) {
        var e_1, _a;
        var _this = this;
        // 统一加上服务端前缀
        /** @type {?} */
        var url = req.url;
        if (url !== "MohrssExInterface/gridsvr") {
            return next.handle(req);
        }
        url = this.sinfo.molssUrl;
        /** @type {?} */
        var body = req.body;
        /** @type {?} */
        var sname = body.sname;
        /** @type {?} */
        var sorts = {};
        /** @type {?} */
        var filters = {};
        /** @type {?} */
        var stacal = body.stacal;
        for (var p in body) {
            if (!"sname,pi,ps,sort,form,stacal,".includes(p + ",")) {
                /** @type {?} */
                var v = body[p];
                filters[p] = v;
            }
        }
        /** @type {?} */
        var sortstr = body.sort;
        if (sortstr) {
            // AAE070.descend-AKC258.descend
            /** @type {?} */
            var ss = sortstr.split("-");
            try {
                for (var ss_1 = tslib_1.__values(ss), ss_1_1 = ss_1.next(); !ss_1_1.done; ss_1_1 = ss_1.next()) {
                    var s = ss_1_1.value;
                    /** @type {?} */
                    var sort = s.split(".");
                    if (sort.length > 1 && sort[0] && sort[1]) {
                        sorts[sort[0]] = sort[1];
                    }
                }
            }
            catch (e_1_1) { e_1 = { error: e_1_1 }; }
            finally {
                try {
                    if (ss_1_1 && !ss_1_1.done && (_a = ss_1.return)) _a.call(ss_1);
                }
                finally { if (e_1) throw e_1.error; }
            }
        }
        /** @type {?} */
        var para = body.form;
        /** @type {?} */
        var newbody = {
            para: para,
            page: { PAGESIZE: body.ps, PAGEIDX: body.pi, SORT: sorts, FILTER: filters, STACAL: stacal },
        };
        /** @type {?} */
        var headers = req.headers.set("SNAME", sname);
        /** @type {?} */
        var newReq = req.clone({
            url: url, headers: headers, body: newbody
        });
        return next.handle(newReq).pipe(mergeMap((/**
         * @param {?} event
         * @return {?}
         */
        function (event) {
            if (event instanceof HttpResponseBase) {
                _this.handleData(event);
            }
            return of(event);
        })));
    };
    Object.defineProperty(GridInterceptor.prototype, "sinfo", {
        get: /**
         * @return {?}
         */
        function () {
            return this.injector.get(ServiceInfo);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(GridInterceptor.prototype, "msg", {
        get: /**
         * @return {?}
         */
        function () {
            return this.injector.get(NzMessageService);
        },
        enumerable: true,
        configurable: true
    });
    /**
     * @private
     * @param {?} ev
     * @return {?}
     */
    GridInterceptor.prototype.handleData = /**
     * @private
     * @param {?} ev
     * @return {?}
     */
    function (ev) {
        if (ev instanceof HttpResponse) {
            /** @type {?} */
            var body = ev.body;
            if (body) {
                if (body.code < 1) {
                    this.msg.error(body.errmsg);
                }
                else {
                    // 添加序号
                    /** @type {?} */
                    var list = body.message.list;
                    if (list) {
                        list.map((/**
                         * @param {?} row
                         * @param {?} index
                         * @return {?}
                         */
                        function (row, index) { return row._idx = index; }));
                    }
                }
            }
        }
        return of(ev);
    };
    GridInterceptor.decorators = [
        { type: Injectable }
    ];
    /** @nocollapse */
    GridInterceptor.ctorParameters = function () { return [
        { type: Injector }
    ]; };
    return GridInterceptor;
}());
export { GridInterceptor };
if (false) {
    /**
     * @type {?}
     * @private
     */
    GridInterceptor.prototype.injector;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZ2lyZC5pbnRlcmNlcHRvci5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2xiZi8iLCJzb3VyY2VzIjpbImxpYi9sYmxpYnMvZ2lyZC5pbnRlcmNlcHRvci50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7QUFBQSxPQUFPLEVBQUUsVUFBVSxFQUFFLFFBQVEsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUNyRCxPQUFPLEVBT0wsWUFBWSxFQUVaLGdCQUFnQixHQUNqQixNQUFNLHNCQUFzQixDQUFDO0FBQzlCLE9BQU8sRUFBYyxFQUFFLEVBQUUsTUFBTSxNQUFNLENBQUM7QUFFdEMsT0FBTyxFQUFFLFFBQVEsRUFBRSxNQUFNLGdCQUFnQixDQUFDO0FBQzFDLE9BQU8sRUFBRSxnQkFBZ0IsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUNqRCxPQUFPLEVBQUUsV0FBVyxFQUFFLE1BQU0sa0JBQWtCLENBQUM7QUFFL0M7SUFFRSx5QkFBb0IsUUFBa0I7UUFBbEIsYUFBUSxHQUFSLFFBQVEsQ0FBVTtJQUFJLENBQUM7Ozs7OztJQUczQyxtQ0FBUzs7Ozs7SUFBVCxVQUNFLEdBQXFCLEVBQ3JCLElBQWlCOztRQUZuQixpQkFxRUM7OztZQTFESyxHQUFHLEdBQUcsR0FBRyxDQUFDLEdBQUc7UUFDakIsSUFBSSxHQUFHLEtBQUssMkJBQTJCLEVBQUU7WUFDdkMsT0FBTyxJQUFJLENBQUMsTUFBTSxDQUFDLEdBQUcsQ0FBQyxDQUFDO1NBQ3pCO1FBRUQsR0FBRyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDOztZQUVwQixJQUFJLEdBQUcsR0FBRyxDQUFDLElBQUk7O1lBQ2YsS0FBSyxHQUFHLElBQUksQ0FBQyxLQUFLOztZQUNsQixLQUFLLEdBQUcsRUFBRTs7WUFDVixPQUFPLEdBQUcsRUFBRTs7WUFDWixNQUFNLEdBQUcsSUFBSSxDQUFDLE1BQU07UUFFMUIsS0FBSyxJQUFNLENBQUMsSUFBSSxJQUFJLEVBQUU7WUFFcEIsSUFBSSxDQUFDLCtCQUErQixDQUFDLFFBQVEsQ0FBQyxDQUFDLEdBQUcsR0FBRyxDQUFDLEVBQUU7O29CQUVoRCxDQUFDLEdBQUcsSUFBSSxDQUFDLENBQUMsQ0FBQztnQkFDakIsT0FBTyxDQUFDLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQzthQUNoQjtTQUVGOztZQUNLLE9BQU8sR0FBVyxJQUFJLENBQUMsSUFBSTtRQUdqQyxJQUFJLE9BQU8sRUFBRTs7O2dCQUVMLEVBQUUsR0FBRyxPQUFPLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQzs7Z0JBQzdCLEtBQWdCLElBQUEsT0FBQSxpQkFBQSxFQUFFLENBQUEsc0JBQUEsc0NBQUU7b0JBQWYsSUFBTSxDQUFDLGVBQUE7O3dCQUVKLElBQUksR0FBRyxDQUFDLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQztvQkFDekIsSUFBSSxJQUFJLENBQUMsTUFBTSxHQUFHLENBQUMsSUFBSSxJQUFJLENBQUMsQ0FBQyxDQUFDLElBQUksSUFBSSxDQUFDLENBQUMsQ0FBQyxFQUFFO3dCQUN6QyxLQUFLLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLEdBQUcsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDO3FCQUMxQjtpQkFDRjs7Ozs7Ozs7O1NBQ0Y7O1lBRUssSUFBSSxHQUFHLElBQUksQ0FBQyxJQUFJOztZQUVoQixPQUFPLEdBQUc7WUFDZCxJQUFJLE1BQUE7WUFDSixJQUFJLEVBQUUsRUFBRSxRQUFRLEVBQUUsSUFBSSxDQUFDLEVBQUUsRUFBRSxPQUFPLEVBQUUsSUFBSSxDQUFDLEVBQUUsRUFBRSxJQUFJLEVBQUUsS0FBSyxFQUFFLE1BQU0sRUFBRSxPQUFPLEVBQUUsTUFBTSxFQUFFLE1BQU0sRUFBRTtTQUM1Rjs7WUFHSyxPQUFPLEdBQUcsR0FBRyxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsT0FBTyxFQUFFLEtBQUssQ0FBQzs7WUFFekMsTUFBTSxHQUFHLEdBQUcsQ0FBQyxLQUFLLENBQUM7WUFDdkIsR0FBRyxLQUFBLEVBQUUsT0FBTyxTQUFBLEVBQUUsSUFBSSxFQUFFLE9BQU87U0FDNUIsQ0FBQztRQUVGLE9BQU8sSUFBSSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsQ0FBQyxJQUFJLENBQUMsUUFBUTs7OztRQUFDLFVBQUMsS0FBVTtZQUNsRCxJQUFJLEtBQUssWUFBWSxnQkFBZ0IsRUFBRTtnQkFDckMsS0FBSSxDQUFDLFVBQVUsQ0FBQyxLQUFLLENBQUMsQ0FBQzthQUN4QjtZQUNELE9BQU8sRUFBRSxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBRW5CLENBQUMsRUFBQyxDQUFDLENBQUM7SUFDTixDQUFDO0lBR0Qsc0JBQUksa0NBQUs7Ozs7UUFBVDtZQUNFLE9BQU8sSUFBSSxDQUFDLFFBQVEsQ0FBQyxHQUFHLENBQUMsV0FBVyxDQUFDLENBQUM7UUFDeEMsQ0FBQzs7O09BQUE7SUFDRCxzQkFBSSxnQ0FBRzs7OztRQUFQO1lBQ0UsT0FBTyxJQUFJLENBQUMsUUFBUSxDQUFDLEdBQUcsQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDO1FBQzdDLENBQUM7OztPQUFBOzs7Ozs7SUFFTyxvQ0FBVTs7Ozs7SUFBbEIsVUFBbUIsRUFBb0I7UUFFckMsSUFBSSxFQUFFLFlBQVksWUFBWSxFQUFFOztnQkFFeEIsSUFBSSxHQUFRLEVBQUUsQ0FBQyxJQUFJO1lBQ3pCLElBQUksSUFBSSxFQUFFO2dCQUNSLElBQUksSUFBSSxDQUFDLElBQUksR0FBRyxDQUFDLEVBQUU7b0JBQ2pCLElBQUksQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQztpQkFDN0I7cUJBQ0k7Ozt3QkFHRyxJQUFJLEdBQVUsSUFBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJO29CQUNyQyxJQUFJLElBQUksRUFBRTt3QkFDUixJQUFJLENBQUMsR0FBRzs7Ozs7d0JBQUMsVUFBQyxHQUFRLEVBQUUsS0FBYSxJQUFLLE9BQUEsR0FBRyxDQUFDLElBQUksR0FBRyxLQUFLLEVBQWhCLENBQWdCLEVBQUMsQ0FBQztxQkFDekQ7aUJBR0Y7YUFDRjtTQUlGO1FBR0QsT0FBTyxFQUFFLENBQUMsRUFBRSxDQUFDLENBQUM7SUFFaEIsQ0FBQzs7Z0JBaEhGLFVBQVU7Ozs7Z0JBbEJVLFFBQVE7O0lBcUk3QixzQkFBQztDQUFBLEFBbkhELElBbUhDO1NBbEhZLGVBQWU7Ozs7OztJQUNkLG1DQUEwQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEluamVjdGFibGUsIEluamVjdG9yIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7XHJcbiAgSHR0cEludGVyY2VwdG9yLFxyXG4gIEh0dHBSZXF1ZXN0LFxyXG4gIEh0dHBIYW5kbGVyLFxyXG4gIEh0dHBTZW50RXZlbnQsXHJcbiAgSHR0cEhlYWRlclJlc3BvbnNlLFxyXG4gIEh0dHBQcm9ncmVzc0V2ZW50LFxyXG4gIEh0dHBSZXNwb25zZSxcclxuICBIdHRwVXNlckV2ZW50LFxyXG4gIEh0dHBSZXNwb25zZUJhc2UsXHJcbn0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uL2h0dHAnO1xyXG5pbXBvcnQgeyBPYnNlcnZhYmxlLCBvZiB9IGZyb20gJ3J4anMnO1xyXG5pbXBvcnQgeyBfSHR0cENsaWVudCB9IGZyb20gJ0BkZWxvbi90aGVtZSc7XHJcbmltcG9ydCB7IG1lcmdlTWFwIH0gZnJvbSAncnhqcy9vcGVyYXRvcnMnO1xyXG5pbXBvcnQgeyBOek1lc3NhZ2VTZXJ2aWNlIH0gZnJvbSAnbmctem9ycm8tYW50ZCc7XHJcbmltcG9ydCB7IFNlcnZpY2VJbmZvIH0gZnJvbSAnLi4vU2VydmljZUNvbmZpZyc7XHJcblxyXG5ASW5qZWN0YWJsZSgpXHJcbmV4cG9ydCBjbGFzcyBHcmlkSW50ZXJjZXB0b3IgaW1wbGVtZW50cyBIdHRwSW50ZXJjZXB0b3Ige1xyXG4gIGNvbnN0cnVjdG9yKHByaXZhdGUgaW5qZWN0b3I6IEluamVjdG9yKSB7IH1cclxuXHJcblxyXG4gIGludGVyY2VwdChcclxuICAgIHJlcTogSHR0cFJlcXVlc3Q8YW55PixcclxuICAgIG5leHQ6IEh0dHBIYW5kbGVyLFxyXG4gICk6IE9ic2VydmFibGU8XHJcbiAgICB8IEh0dHBTZW50RXZlbnRcclxuICAgIHwgSHR0cEhlYWRlclJlc3BvbnNlXHJcbiAgICB8IEh0dHBQcm9ncmVzc0V2ZW50XHJcbiAgICB8IEh0dHBSZXNwb25zZTxhbnk+XHJcbiAgICB8IEh0dHBVc2VyRXZlbnQ8YW55PlxyXG4gID4ge1xyXG4gICAgLy8g57uf5LiA5Yqg5LiK5pyN5Yqh56uv5YmN57yAXHJcbiAgICBsZXQgdXJsID0gcmVxLnVybDtcclxuICAgIGlmICh1cmwgIT09IFwiTW9ocnNzRXhJbnRlcmZhY2UvZ3JpZHN2clwiKSB7XHJcbiAgICAgIHJldHVybiBuZXh0LmhhbmRsZShyZXEpO1xyXG4gICAgfVxyXG5cclxuICAgIHVybCA9IHRoaXMuc2luZm8ubW9sc3NVcmw7XHJcblxyXG4gICAgY29uc3QgYm9keSA9IHJlcS5ib2R5O1xyXG4gICAgY29uc3Qgc25hbWUgPSBib2R5LnNuYW1lO1xyXG4gICAgY29uc3Qgc29ydHMgPSB7fTtcclxuICAgIGNvbnN0IGZpbHRlcnMgPSB7fTtcclxuICAgIGNvbnN0IHN0YWNhbCA9IGJvZHkuc3RhY2FsO1xyXG5cclxuICAgIGZvciAoY29uc3QgcCBpbiBib2R5KSB7XHJcblxyXG4gICAgICBpZiAoIVwic25hbWUscGkscHMsc29ydCxmb3JtLHN0YWNhbCxcIi5pbmNsdWRlcyhwICsgXCIsXCIpKSB7XHJcblxyXG4gICAgICAgIGNvbnN0IHYgPSBib2R5W3BdO1xyXG4gICAgICAgIGZpbHRlcnNbcF0gPSB2O1xyXG4gICAgICB9XHJcblxyXG4gICAgfVxyXG4gICAgY29uc3Qgc29ydHN0cjogc3RyaW5nID0gYm9keS5zb3J0O1xyXG5cclxuXHJcbiAgICBpZiAoc29ydHN0cikge1xyXG4gICAgICAvLyBBQUUwNzAuZGVzY2VuZC1BS0MyNTguZGVzY2VuZFxyXG4gICAgICBjb25zdCBzcyA9IHNvcnRzdHIuc3BsaXQoXCItXCIpO1xyXG4gICAgICBmb3IgKGNvbnN0IHMgb2Ygc3MpIHtcclxuXHJcbiAgICAgICAgY29uc3Qgc29ydCA9IHMuc3BsaXQoXCIuXCIpO1xyXG4gICAgICAgIGlmIChzb3J0Lmxlbmd0aCA+IDEgJiYgc29ydFswXSAmJiBzb3J0WzFdKSB7XHJcbiAgICAgICAgICBzb3J0c1tzb3J0WzBdXSA9IHNvcnRbMV07XHJcbiAgICAgICAgfVxyXG4gICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgY29uc3QgcGFyYSA9IGJvZHkuZm9ybTtcclxuXHJcbiAgICBjb25zdCBuZXdib2R5ID0ge1xyXG4gICAgICBwYXJhLFxyXG4gICAgICBwYWdlOiB7IFBBR0VTSVpFOiBib2R5LnBzLCBQQUdFSURYOiBib2R5LnBpLCBTT1JUOiBzb3J0cywgRklMVEVSOiBmaWx0ZXJzLCBTVEFDQUw6IHN0YWNhbCB9LFxyXG4gICAgfVxyXG5cclxuXHJcbiAgICBjb25zdCBoZWFkZXJzID0gcmVxLmhlYWRlcnMuc2V0KFwiU05BTUVcIiwgc25hbWUpO1xyXG5cclxuICAgIGNvbnN0IG5ld1JlcSA9IHJlcS5jbG9uZSh7XHJcbiAgICAgIHVybCwgaGVhZGVycywgYm9keTogbmV3Ym9keVxyXG4gICAgfSk7XHJcblxyXG4gICAgcmV0dXJuIG5leHQuaGFuZGxlKG5ld1JlcSkucGlwZShtZXJnZU1hcCgoZXZlbnQ6IGFueSkgPT4ge1xyXG4gICAgICBpZiAoZXZlbnQgaW5zdGFuY2VvZiBIdHRwUmVzcG9uc2VCYXNlKSB7XHJcbiAgICAgICAgdGhpcy5oYW5kbGVEYXRhKGV2ZW50KTtcclxuICAgICAgfVxyXG4gICAgICByZXR1cm4gb2YoZXZlbnQpO1xyXG5cclxuICAgIH0pKTtcclxuICB9XHJcblxyXG5cclxuICBnZXQgc2luZm8oKTogU2VydmljZUluZm8ge1xyXG4gICAgcmV0dXJuIHRoaXMuaW5qZWN0b3IuZ2V0KFNlcnZpY2VJbmZvKTtcclxuICB9XHJcbiAgZ2V0IG1zZygpOiBOek1lc3NhZ2VTZXJ2aWNlIHtcclxuICAgIHJldHVybiB0aGlzLmluamVjdG9yLmdldChOek1lc3NhZ2VTZXJ2aWNlKTtcclxuICB9XHJcblxyXG4gIHByaXZhdGUgaGFuZGxlRGF0YShldjogSHR0cFJlc3BvbnNlQmFzZSk6IE9ic2VydmFibGU8YW55PiB7XHJcblxyXG4gICAgaWYgKGV2IGluc3RhbmNlb2YgSHR0cFJlc3BvbnNlKSB7XHJcblxyXG4gICAgICBjb25zdCBib2R5OiBhbnkgPSBldi5ib2R5O1xyXG4gICAgICBpZiAoYm9keSkge1xyXG4gICAgICAgIGlmIChib2R5LmNvZGUgPCAxKSB7XHJcbiAgICAgICAgICB0aGlzLm1zZy5lcnJvcihib2R5LmVycm1zZyk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGVsc2Uge1xyXG4gICAgICAgICAgLy8g5re75Yqg5bqP5Y+3XHJcblxyXG4gICAgICAgICAgY29uc3QgbGlzdDogYW55W10gPSBib2R5Lm1lc3NhZ2UubGlzdDtcclxuICAgICAgICAgIGlmIChsaXN0KSB7XHJcbiAgICAgICAgICAgIGxpc3QubWFwKChyb3c6IGFueSwgaW5kZXg6IG51bWJlcikgPT4gcm93Ll9pZHggPSBpbmRleCk7XHJcbiAgICAgICAgICB9XHJcblxyXG5cclxuICAgICAgICB9XHJcbiAgICAgIH1cclxuXHJcblxyXG5cclxuICAgIH1cclxuXHJcblxyXG4gICAgcmV0dXJuIG9mKGV2KTtcclxuXHJcbiAgfVxyXG5cclxuXHJcbn0iXX0=