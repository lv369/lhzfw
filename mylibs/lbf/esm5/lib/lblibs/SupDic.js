/**
 * @fileoverview added by tsickle
 * Generated from: lib/lblibs/SupDic.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { HttpService } from './Lbjk.service';
import * as i0 from "@angular/core";
import * as i1 from "./Lbjk.service";
import * as i2 from "@angular/common/http";
var SupDic = /** @class */ (function () {
    function SupDic(lbservice, httpclient) {
        this.lbservice = lbservice;
        this.httpclient = httpclient;
        this.dicCache = new Object();
        this.ka010203 = {};
    }
    /**
     * 获取字典信息,返回数组
     *  @param dicName 字典值
     *  @returns 字典信息，未取到返回[]
     */
    /**
     * 获取字典信息,返回数组
     * @param {?} dicName 字典值
     * @return {?} 字典信息，未取到返回[]
     */
    SupDic.prototype.getDic = /**
     * 获取字典信息,返回数组
     * @param {?} dicName 字典值
     * @return {?} 字典信息，未取到返回[]
     */
    function (dicName) {
        if (this.dicCache && this.dicCache.hasOwnProperty(dicName)) {
            return this.dicCache[dicName].diclist;
        }
        return [];
    };
    /**
     * 获取字典信息,返回object
     *  @param dicName 字典值
     *  @returns 字典信息
     */
    /**
     * 获取字典信息,返回object
     * @param {?} dicName 字典值
     * @return {?} 字典信息
     */
    SupDic.prototype.getDicObj = /**
     * 获取字典信息,返回object
     * @param {?} dicName 字典值
     * @return {?} 字典信息
     */
    function (dicName) {
        if (this.dicCache && this.dicCache.hasOwnProperty(dicName)) {
            return this.dicCache[dicName].dicobj;
        }
    };
    /**
     * 获取指定字典指定值对应的标签值
     * @param key 字典名称
     * @param val 值
     */
    /**
     * 获取指定字典指定值对应的标签值
     * @param {?} key 字典名称
     * @param {?} val 值
     * @return {?}
     */
    SupDic.prototype.getdicLabel = /**
     * 获取指定字典指定值对应的标签值
     * @param {?} key 字典名称
     * @param {?} val 值
     * @return {?}
     */
    function (key, val) {
        /** @type {?} */
        var dic = this.getDicObj(key);
        if (dic && dic.hasOwnProperty(val)) {
            return dic[val].CNAME;
        }
        return val;
    };
    /**
     * 异步方式获取字典信息，返回SF的字典数组
     *   始终从数据库中获取，获取的字典将会写入到缓存中
     * @param dicName 字典名称
     */
    /**
     * 异步方式获取字典信息，返回SF的字典数组
     *   始终从数据库中获取，获取的字典将会写入到缓存中
     * @param {?} dicName 字典名称
     * @return {?}
     */
    SupDic.prototype.getSFDicAsync = /**
     * 异步方式获取字典信息，返回SF的字典数组
     *   始终从数据库中获取，获取的字典将会写入到缓存中
     * @param {?} dicName 字典名称
     * @return {?}
     */
    function (dicName) {
        // const dic = this.getSFDic(dicName);
        // if (dic.length > 0) {
        //   return of(dic);
        // }
        // const dic = this.getSFDic(dicName);
        // if (dic.length > 0) {
        //   return of(dic);
        // }
        /** @type {?} */
        var aaa = this.lbservice;
        /** @type {?} */
        var dicCache = this.dicCache;
        return new Observable((/**
         * @param {?} observer
         * @return {?}
         */
        function subscribe(observer) {
            // FR_GetDIC
            aaa.lbservice('FR_GetAllDIC', {}).then((/**
             * @param {?} resdata
             * @return {?}
             */
            function (resdata) {
                var e_1, _a;
                if (resdata.code > 0) {
                    /** @type {?} */
                    var dicobj = new Object();
                    /** @type {?} */
                    var dicarr = resdata.message.list;
                    /** @type {?} */
                    var reslist = [];
                    try {
                        for (var dicarr_1 = tslib_1.__values(dicarr), dicarr_1_1 = dicarr_1.next(); !dicarr_1_1.done; dicarr_1_1 = dicarr_1.next()) {
                            var el = dicarr_1_1.value;
                            el.text = el.CNAME;
                            el.value = el.CCODE;
                            dicobj[el.CCODE] = el;
                            reslist.push(el);
                        }
                    }
                    catch (e_1_1) { e_1 = { error: e_1_1 }; }
                    finally {
                        try {
                            if (dicarr_1_1 && !dicarr_1_1.done && (_a = dicarr_1.return)) _a.call(dicarr_1);
                        }
                        finally { if (e_1) throw e_1.error; }
                    }
                    dicCache[dicName] = { diclist: dicarr, dicobj: dicobj };
                    observer.next(reslist);
                    observer.complete();
                }
            }));
        }));
    };
    /**
     * 获取SF格式的字典信息
     * @param dicName 字典值
     */
    /**
     * 获取SF格式的字典信息
     * @param {?} dicName 字典值
     * @return {?}
     */
    SupDic.prototype.getSFDic = /**
     * 获取SF格式的字典信息
     * @param {?} dicName 字典值
     * @return {?}
     */
    function (dicName) {
        var e_2, _a;
        /** @type {?} */
        var reslist = [];
        /** @type {?} */
        var diclist = this.getDic(dicName);
        try {
            for (var diclist_1 = tslib_1.__values(diclist), diclist_1_1 = diclist_1.next(); !diclist_1_1.done; diclist_1_1 = diclist_1.next()) {
                var dic = diclist_1_1.value;
                reslist.push({ label: dic.CNAME, value: dic.CCODE });
            }
        }
        catch (e_2_1) { e_2 = { error: e_2_1 }; }
        finally {
            try {
                if (diclist_1_1 && !diclist_1_1.done && (_a = diclist_1.return)) _a.call(diclist_1);
            }
            finally { if (e_2) throw e_2.error; }
        }
        return reslist;
    };
    /**
     * 加载所有字典
     */
    /**
     * 加载所有字典
     * @return {?}
     */
    SupDic.prototype.loadAllDic = /**
     * 加载所有字典
     * @return {?}
     */
    function () {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var _this = this;
            return tslib_1.__generator(this, function (_a) {
                // FR_GetAllDIC
                return [2 /*return*/, new Promise((/**
                     * @param {?} resolve
                     * @param {?} _
                     * @return {?}
                     */
                    function (resolve, _) {
                        _this.lbservice.lbservice('FR_GetAllDIC', {}).then((/**
                         * @param {?} resdata
                         * @return {?}
                         */
                        function (resdata) {
                            var e_3, _a;
                            if (resdata.code > 0) {
                                // this.dicCache=resdata.message;
                                for (var d in resdata.message) {
                                    if (resdata.message.hasOwnProperty(d)) {
                                        /** @type {?} */
                                        var a = new Object();
                                        /** @type {?} */
                                        var dicarr = resdata.message[d];
                                        try {
                                            for (var dicarr_2 = (e_3 = void 0, tslib_1.__values(dicarr)), dicarr_2_1 = dicarr_2.next(); !dicarr_2_1.done; dicarr_2_1 = dicarr_2.next()) {
                                                var el = dicarr_2_1.value;
                                                el.text = el.CNAME;
                                                el.value = el.CCODE;
                                                a[el.CCODE] = el;
                                            }
                                        }
                                        catch (e_3_1) { e_3 = { error: e_3_1 }; }
                                        finally {
                                            try {
                                                if (dicarr_2_1 && !dicarr_2_1.done && (_a = dicarr_2.return)) _a.call(dicarr_2);
                                            }
                                            finally { if (e_3) throw e_3.error; }
                                        }
                                        /** @type {?} */
                                        var dic = { diclist: dicarr, dicobj: a };
                                        _this.dicCache[d] = dic;
                                    }
                                }
                            }
                            resolve();
                        }));
                    }))];
            });
        });
    };
    SupDic.decorators = [
        { type: Injectable, args: [{ providedIn: 'root' },] }
    ];
    /** @nocollapse */
    SupDic.ctorParameters = function () { return [
        { type: HttpService },
        { type: HttpClient }
    ]; };
    /** @nocollapse */ SupDic.ngInjectableDef = i0.ɵɵdefineInjectable({ factory: function SupDic_Factory() { return new SupDic(i0.ɵɵinject(i1.HttpService), i0.ɵɵinject(i2.HttpClient)); }, token: SupDic, providedIn: "root" });
    return SupDic;
}());
export { SupDic };
if (false) {
    /**
     * @type {?}
     * @private
     */
    SupDic.prototype.dicCache;
    /** @type {?} */
    SupDic.prototype.ka010203;
    /**
     * @type {?}
     * @private
     */
    SupDic.prototype.lbservice;
    /**
     * @type {?}
     * @private
     */
    SupDic.prototype.httpclient;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiU3VwRGljLmpzIiwic291cmNlUm9vdCI6Im5nOi8vbGJmLyIsInNvdXJjZXMiOlsibGliL2xibGlicy9TdXBEaWMudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7O0FBQUEsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUUzQyxPQUFPLEVBQUUsVUFBVSxFQUFrQixNQUFNLE1BQU0sQ0FBQztBQUNsRCxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sc0JBQXNCLENBQUM7QUFDbEQsT0FBTyxFQUFFLFdBQVcsRUFBRSxNQUFNLGdCQUFnQixDQUFDOzs7O0FBRTdDO0lBRUUsZ0JBQW9CLFNBQXNCLEVBQVUsVUFBc0I7UUFBdEQsY0FBUyxHQUFULFNBQVMsQ0FBYTtRQUFVLGVBQVUsR0FBVixVQUFVLENBQVk7UUFDbEUsYUFBUSxHQUFRLElBQUksTUFBTSxFQUFFLENBQUM7UUFDOUIsYUFBUSxHQUFRLEVBQUUsQ0FBQztJQUZvRCxDQUFDO0lBSS9FOzs7O09BSUc7Ozs7OztJQUNILHVCQUFNOzs7OztJQUFOLFVBQU8sT0FBZTtRQUNwQixJQUFJLElBQUksQ0FBQyxRQUFRLElBQUksSUFBSSxDQUFDLFFBQVEsQ0FBQyxjQUFjLENBQUMsT0FBTyxDQUFDLEVBQUU7WUFDMUQsT0FBTyxJQUFJLENBQUMsUUFBUSxDQUFDLE9BQU8sQ0FBQyxDQUFDLE9BQU8sQ0FBQztTQUN2QztRQUVELE9BQU8sRUFBRSxDQUFDO0lBQ1osQ0FBQztJQUVEOzs7O09BSUc7Ozs7OztJQUNILDBCQUFTOzs7OztJQUFULFVBQVUsT0FBZTtRQUN2QixJQUFJLElBQUksQ0FBQyxRQUFRLElBQUksSUFBSSxDQUFDLFFBQVEsQ0FBQyxjQUFjLENBQUMsT0FBTyxDQUFDLEVBQUU7WUFDMUQsT0FBTyxJQUFJLENBQUMsUUFBUSxDQUFDLE9BQU8sQ0FBQyxDQUFDLE1BQU0sQ0FBQztTQUN0QztJQUNILENBQUM7SUFFRDs7OztPQUlHOzs7Ozs7O0lBQ0gsNEJBQVc7Ozs7OztJQUFYLFVBQVksR0FBVyxFQUFFLEdBQVc7O1lBQzVCLEdBQUcsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQztRQUMvQixJQUFJLEdBQUcsSUFBSSxHQUFHLENBQUMsY0FBYyxDQUFDLEdBQUcsQ0FBQyxFQUFFO1lBQ2xDLE9BQU8sR0FBRyxDQUFDLEdBQUcsQ0FBQyxDQUFDLEtBQUssQ0FBQztTQUN2QjtRQUVELE9BQU8sR0FBRyxDQUFDO0lBQ2IsQ0FBQztJQUdEOzs7O09BSUc7Ozs7Ozs7SUFDSCw4QkFBYTs7Ozs7O0lBQWIsVUFBYyxPQUFlO1FBRTNCLHNDQUFzQztRQUN0Qyx3QkFBd0I7UUFDeEIsb0JBQW9CO1FBQ3BCLElBQUk7Ozs7OztZQUVFLEdBQUcsR0FBRyxJQUFJLENBQUMsU0FBUzs7WUFDcEIsUUFBUSxHQUFHLElBQUksQ0FBQyxRQUFRO1FBRTlCLE9BQU8sSUFBSSxVQUFVOzs7O1FBQXFCLFNBQVMsU0FBUyxDQUFDLFFBQVE7WUFFbkUsWUFBWTtZQUNaLEdBQUcsQ0FBQyxTQUFTLENBQUMsY0FBYyxFQUFFLEVBQUUsQ0FBQyxDQUFDLElBQUk7Ozs7WUFBQyxVQUFBLE9BQU87O2dCQUU1QyxJQUFJLE9BQU8sQ0FBQyxJQUFJLEdBQUcsQ0FBQyxFQUFFOzt3QkFFZCxNQUFNLEdBQVEsSUFBSSxNQUFNLEVBQUU7O3dCQUMxQixNQUFNLEdBQVUsT0FBTyxDQUFDLE9BQU8sQ0FBQyxJQUFJOzt3QkFDcEMsT0FBTyxHQUF1QixFQUFFOzt3QkFDdEMsS0FBaUIsSUFBQSxXQUFBLGlCQUFBLE1BQU0sQ0FBQSw4QkFBQSxrREFBRTs0QkFBcEIsSUFBTSxFQUFFLG1CQUFBOzRCQUNYLEVBQUUsQ0FBQyxJQUFJLEdBQUcsRUFBRSxDQUFDLEtBQUssQ0FBQzs0QkFDbkIsRUFBRSxDQUFDLEtBQUssR0FBRyxFQUFFLENBQUMsS0FBSyxDQUFDOzRCQUNwQixNQUFNLENBQUMsRUFBRSxDQUFDLEtBQUssQ0FBQyxHQUFHLEVBQUUsQ0FBQzs0QkFDdEIsT0FBTyxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBQzt5QkFDbEI7Ozs7Ozs7OztvQkFFRCxRQUFRLENBQUMsT0FBTyxDQUFDLEdBQUcsRUFBRSxPQUFPLEVBQUUsTUFBTSxFQUFFLE1BQU0sUUFBQSxFQUFFLENBQUM7b0JBQ2hELFFBQVEsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUM7b0JBQ3ZCLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQztpQkFFckI7WUFDSCxDQUFDLEVBQUMsQ0FBQztRQUVMLENBQUMsRUFBQyxDQUFDO0lBQ0wsQ0FBQztJQUVEOzs7T0FHRzs7Ozs7O0lBQ0gseUJBQVE7Ozs7O0lBQVIsVUFBUyxPQUFlOzs7WUFHaEIsT0FBTyxHQUF1QixFQUFFOztZQUVoQyxPQUFPLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxPQUFPLENBQUM7O1lBQ3BDLEtBQWtCLElBQUEsWUFBQSxpQkFBQSxPQUFPLENBQUEsZ0NBQUEscURBQUU7Z0JBQXRCLElBQU0sR0FBRyxvQkFBQTtnQkFDWixPQUFPLENBQUMsSUFBSSxDQUFDLEVBQUUsS0FBSyxFQUFFLEdBQUcsQ0FBQyxLQUFLLEVBQUUsS0FBSyxFQUFFLEdBQUcsQ0FBQyxLQUFLLEVBQUUsQ0FBQyxDQUFDO2FBQ3REOzs7Ozs7Ozs7UUFHRCxPQUFPLE9BQU8sQ0FBQztJQUNqQixDQUFDO0lBRUQ7O09BRUc7Ozs7O0lBQ0csMkJBQVU7Ozs7SUFBaEI7Ozs7Z0JBQ0UsZUFBZTtnQkFFZixzQkFBTyxJQUFJLE9BQU87Ozs7O29CQUFDLFVBQUMsT0FBTyxFQUFFLENBQUM7d0JBRTVCLEtBQUksQ0FBQyxTQUFTLENBQUMsU0FBUyxDQUFDLGNBQWMsRUFBRSxFQUFFLENBQUMsQ0FBQyxJQUFJOzs7O3dCQUFDLFVBQUEsT0FBTzs7NEJBRXZELElBQUksT0FBTyxDQUFDLElBQUksR0FBRyxDQUFDLEVBQUU7Z0NBQ3BCLGlDQUFpQztnQ0FFakMsS0FBSyxJQUFNLENBQUMsSUFBSSxPQUFPLENBQUMsT0FBTyxFQUFFO29DQUMvQixJQUFJLE9BQU8sQ0FBQyxPQUFPLENBQUMsY0FBYyxDQUFDLENBQUMsQ0FBQyxFQUFFOzs0Q0FFL0IsQ0FBQyxHQUFRLElBQUksTUFBTSxFQUFFOzs0Q0FDckIsTUFBTSxHQUFVLE9BQU8sQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDOzs0Q0FDeEMsS0FBaUIsSUFBQSwwQkFBQSxpQkFBQSxNQUFNLENBQUEsQ0FBQSw4QkFBQSxrREFBRTtnREFBcEIsSUFBTSxFQUFFLG1CQUFBO2dEQUNYLEVBQUUsQ0FBQyxJQUFJLEdBQUcsRUFBRSxDQUFDLEtBQUssQ0FBQztnREFDbkIsRUFBRSxDQUFDLEtBQUssR0FBRyxFQUFFLENBQUMsS0FBSyxDQUFDO2dEQUNwQixDQUFDLENBQUMsRUFBRSxDQUFDLEtBQUssQ0FBQyxHQUFHLEVBQUUsQ0FBQzs2Q0FDbEI7Ozs7Ozs7Ozs7NENBRUssR0FBRyxHQUFHLEVBQUUsT0FBTyxFQUFFLE1BQU0sRUFBRSxNQUFNLEVBQUUsQ0FBQyxFQUFFO3dDQUUxQyxLQUFJLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxHQUFHLEdBQUcsQ0FBQztxQ0FDeEI7aUNBQ0Y7NkJBRUY7NEJBRUQsT0FBTyxFQUFFLENBQUM7d0JBQ1osQ0FBQyxFQUFDLENBQUM7b0JBRUwsQ0FBQyxFQUFDLEVBQUM7OztLQUVKOztnQkE5SUYsVUFBVSxTQUFDLEVBQUUsVUFBVSxFQUFFLE1BQU0sRUFBRTs7OztnQkFGekIsV0FBVztnQkFEWCxVQUFVOzs7aUJBSG5CO0NBcUpDLEFBL0lELElBK0lDO1NBOUlZLE1BQU07Ozs7OztJQUVqQiwwQkFBcUM7O0lBQ3JDLDBCQUEwQjs7Ozs7SUFGZCwyQkFBOEI7Ozs7O0lBQUUsNEJBQThCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSW5qZWN0YWJsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBTRlNjaGVtYUVudW1UeXBlIH0gZnJvbSAnQGRlbG9uL2Zvcm0nO1xyXG5pbXBvcnQgeyBPYnNlcnZhYmxlLCBvZiwgb2JzZXJ2YWJsZSB9IGZyb20gJ3J4anMnO1xyXG5pbXBvcnQgeyBIdHRwQ2xpZW50IH0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uL2h0dHAnO1xyXG5pbXBvcnQgeyBIdHRwU2VydmljZSB9IGZyb20gJy4vTGJqay5zZXJ2aWNlJztcclxuXHJcbkBJbmplY3RhYmxlKHsgcHJvdmlkZWRJbjogJ3Jvb3QnIH0pXHJcbmV4cG9ydCBjbGFzcyBTdXBEaWMge1xyXG4gIGNvbnN0cnVjdG9yKHByaXZhdGUgbGJzZXJ2aWNlOiBIdHRwU2VydmljZSwgcHJpdmF0ZSBodHRwY2xpZW50OiBIdHRwQ2xpZW50KSB7IH1cclxuICBwcml2YXRlIGRpY0NhY2hlOiBhbnkgPSBuZXcgT2JqZWN0KCk7XHJcbiAgcHVibGljIGthMDEwMjAzOiBhbnkgPSB7fTtcclxuXHJcbiAgLyoqXHJcbiAgICog6I635Y+W5a2X5YW45L+h5oGvLOi/lOWbnuaVsOe7hFxyXG4gICAqICBAcGFyYW0gZGljTmFtZSDlrZflhbjlgLxcclxuICAgKiAgQHJldHVybnMg5a2X5YW45L+h5oGv77yM5pyq5Y+W5Yiw6L+U5ZueW11cclxuICAgKi9cclxuICBnZXREaWMoZGljTmFtZTogc3RyaW5nKSB7XHJcbiAgICBpZiAodGhpcy5kaWNDYWNoZSAmJiB0aGlzLmRpY0NhY2hlLmhhc093blByb3BlcnR5KGRpY05hbWUpKSB7XHJcbiAgICAgIHJldHVybiB0aGlzLmRpY0NhY2hlW2RpY05hbWVdLmRpY2xpc3Q7XHJcbiAgICB9XHJcblxyXG4gICAgcmV0dXJuIFtdO1xyXG4gIH1cclxuXHJcbiAgLyoqXHJcbiAgICog6I635Y+W5a2X5YW45L+h5oGvLOi/lOWbnm9iamVjdFxyXG4gICAqICBAcGFyYW0gZGljTmFtZSDlrZflhbjlgLxcclxuICAgKiAgQHJldHVybnMg5a2X5YW45L+h5oGvXHJcbiAgICovXHJcbiAgZ2V0RGljT2JqKGRpY05hbWU6IHN0cmluZykge1xyXG4gICAgaWYgKHRoaXMuZGljQ2FjaGUgJiYgdGhpcy5kaWNDYWNoZS5oYXNPd25Qcm9wZXJ0eShkaWNOYW1lKSkge1xyXG4gICAgICByZXR1cm4gdGhpcy5kaWNDYWNoZVtkaWNOYW1lXS5kaWNvYmo7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICAvKipcclxuICAgKiDojrflj5bmjIflrprlrZflhbjmjIflrprlgLzlr7nlupTnmoTmoIfnrb7lgLxcclxuICAgKiBAcGFyYW0ga2V5IOWtl+WFuOWQjeensFxyXG4gICAqIEBwYXJhbSB2YWwg5YC8XHJcbiAgICovXHJcbiAgZ2V0ZGljTGFiZWwoa2V5OiBzdHJpbmcsIHZhbDogc3RyaW5nKSB7XHJcbiAgICBjb25zdCBkaWMgPSB0aGlzLmdldERpY09iaihrZXkpO1xyXG4gICAgaWYgKGRpYyAmJiBkaWMuaGFzT3duUHJvcGVydHkodmFsKSkge1xyXG4gICAgICByZXR1cm4gZGljW3ZhbF0uQ05BTUU7XHJcbiAgICB9XHJcblxyXG4gICAgcmV0dXJuIHZhbDtcclxuICB9XHJcblxyXG5cclxuICAvKipcclxuICAgKiDlvILmraXmlrnlvI/ojrflj5blrZflhbjkv6Hmga/vvIzov5Tlm55TRueahOWtl+WFuOaVsOe7hFxyXG4gICAqICAg5aeL57uI5LuO5pWw5o2u5bqT5Lit6I635Y+W77yM6I635Y+W55qE5a2X5YW45bCG5Lya5YaZ5YWl5Yiw57yT5a2Y5LitXHJcbiAgICogQHBhcmFtIGRpY05hbWUg5a2X5YW45ZCN56ewXHJcbiAgICovXHJcbiAgZ2V0U0ZEaWNBc3luYyhkaWNOYW1lOiBzdHJpbmcpOiBPYnNlcnZhYmxlPFNGU2NoZW1hRW51bVR5cGVbXT4ge1xyXG5cclxuICAgIC8vIGNvbnN0IGRpYyA9IHRoaXMuZ2V0U0ZEaWMoZGljTmFtZSk7XHJcbiAgICAvLyBpZiAoZGljLmxlbmd0aCA+IDApIHtcclxuICAgIC8vICAgcmV0dXJuIG9mKGRpYyk7XHJcbiAgICAvLyB9XHJcblxyXG4gICAgY29uc3QgYWFhID0gdGhpcy5sYnNlcnZpY2U7XHJcbiAgICBjb25zdCBkaWNDYWNoZSA9IHRoaXMuZGljQ2FjaGU7XHJcblxyXG4gICAgcmV0dXJuIG5ldyBPYnNlcnZhYmxlPFNGU2NoZW1hRW51bVR5cGVbXT4oZnVuY3Rpb24gc3Vic2NyaWJlKG9ic2VydmVyKSB7XHJcblxyXG4gICAgICAvLyBGUl9HZXRESUNcclxuICAgICAgYWFhLmxic2VydmljZSgnRlJfR2V0QWxsRElDJywge30pLnRoZW4ocmVzZGF0YSA9PiB7XHJcblxyXG4gICAgICAgIGlmIChyZXNkYXRhLmNvZGUgPiAwKSB7XHJcblxyXG4gICAgICAgICAgY29uc3QgZGljb2JqOiBhbnkgPSBuZXcgT2JqZWN0KCk7XHJcbiAgICAgICAgICBjb25zdCBkaWNhcnI6IGFueVtdID0gcmVzZGF0YS5tZXNzYWdlLmxpc3Q7XHJcbiAgICAgICAgICBjb25zdCByZXNsaXN0OiBTRlNjaGVtYUVudW1UeXBlW10gPSBbXTtcclxuICAgICAgICAgIGZvciAoY29uc3QgZWwgb2YgZGljYXJyKSB7XHJcbiAgICAgICAgICAgIGVsLnRleHQgPSBlbC5DTkFNRTtcclxuICAgICAgICAgICAgZWwudmFsdWUgPSBlbC5DQ09ERTtcclxuICAgICAgICAgICAgZGljb2JqW2VsLkNDT0RFXSA9IGVsO1xyXG4gICAgICAgICAgICByZXNsaXN0LnB1c2goZWwpO1xyXG4gICAgICAgICAgfVxyXG5cclxuICAgICAgICAgIGRpY0NhY2hlW2RpY05hbWVdID0geyBkaWNsaXN0OiBkaWNhcnIsIGRpY29iaiB9O1xyXG4gICAgICAgICAgb2JzZXJ2ZXIubmV4dChyZXNsaXN0KTtcclxuICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcblxyXG4gICAgICAgIH1cclxuICAgICAgfSk7XHJcblxyXG4gICAgfSk7XHJcbiAgfVxyXG5cclxuICAvKipcclxuICAgKiDojrflj5ZTRuagvOW8j+eahOWtl+WFuOS/oeaBr1xyXG4gICAqIEBwYXJhbSBkaWNOYW1lIOWtl+WFuOWAvFxyXG4gICAqL1xyXG4gIGdldFNGRGljKGRpY05hbWU6IHN0cmluZyk6IFNGU2NoZW1hRW51bVR5cGVbXSB7XHJcblxyXG5cclxuICAgIGNvbnN0IHJlc2xpc3Q6IFNGU2NoZW1hRW51bVR5cGVbXSA9IFtdO1xyXG5cclxuICAgIGNvbnN0IGRpY2xpc3QgPSB0aGlzLmdldERpYyhkaWNOYW1lKTtcclxuICAgIGZvciAoY29uc3QgZGljIG9mIGRpY2xpc3QpIHtcclxuICAgICAgcmVzbGlzdC5wdXNoKHsgbGFiZWw6IGRpYy5DTkFNRSwgdmFsdWU6IGRpYy5DQ09ERSB9KTtcclxuICAgIH1cclxuXHJcblxyXG4gICAgcmV0dXJuIHJlc2xpc3Q7XHJcbiAgfVxyXG5cclxuICAvKipcclxuICAgKiDliqDovb3miYDmnInlrZflhbhcclxuICAgKi9cclxuICBhc3luYyBsb2FkQWxsRGljKCk6IFByb21pc2U8dm9pZD4ge1xyXG4gICAgLy8gRlJfR2V0QWxsRElDXHJcblxyXG4gICAgcmV0dXJuIG5ldyBQcm9taXNlKChyZXNvbHZlLCBfKSA9PiB7XHJcblxyXG4gICAgICB0aGlzLmxic2VydmljZS5sYnNlcnZpY2UoJ0ZSX0dldEFsbERJQycsIHt9KS50aGVuKHJlc2RhdGEgPT4ge1xyXG5cclxuICAgICAgICBpZiAocmVzZGF0YS5jb2RlID4gMCkge1xyXG4gICAgICAgICAgLy8gdGhpcy5kaWNDYWNoZT1yZXNkYXRhLm1lc3NhZ2U7XHJcblxyXG4gICAgICAgICAgZm9yIChjb25zdCBkIGluIHJlc2RhdGEubWVzc2FnZSkge1xyXG4gICAgICAgICAgICBpZiAocmVzZGF0YS5tZXNzYWdlLmhhc093blByb3BlcnR5KGQpKSB7XHJcblxyXG4gICAgICAgICAgICAgIGNvbnN0IGE6IGFueSA9IG5ldyBPYmplY3QoKTtcclxuICAgICAgICAgICAgICBjb25zdCBkaWNhcnI6IGFueVtdID0gcmVzZGF0YS5tZXNzYWdlW2RdO1xyXG4gICAgICAgICAgICAgIGZvciAoY29uc3QgZWwgb2YgZGljYXJyKSB7XHJcbiAgICAgICAgICAgICAgICBlbC50ZXh0ID0gZWwuQ05BTUU7XHJcbiAgICAgICAgICAgICAgICBlbC52YWx1ZSA9IGVsLkNDT0RFO1xyXG4gICAgICAgICAgICAgICAgYVtlbC5DQ09ERV0gPSBlbDtcclxuICAgICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAgIGNvbnN0IGRpYyA9IHsgZGljbGlzdDogZGljYXJyLCBkaWNvYmo6IGEgfTtcclxuXHJcbiAgICAgICAgICAgICAgdGhpcy5kaWNDYWNoZVtkXSA9IGRpYztcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgfVxyXG5cclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIHJlc29sdmUoKTtcclxuICAgICAgfSk7XHJcblxyXG4gICAgfSk7XHJcblxyXG4gIH1cclxufVxyXG4iXX0=