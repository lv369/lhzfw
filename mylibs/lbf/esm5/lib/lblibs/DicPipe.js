/**
 * @fileoverview added by tsickle
 * Generated from: lib/lblibs/DicPipe.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Pipe } from '@angular/core';
import { SupDic } from './SupDic';
/*
* 字典显示管道
*   10|dicpipe:aae140
*/
var DicPipe = /** @class */ (function () {
    function DicPipe(supdic) {
        this.supdic = supdic;
    }
    /**
     * @param {?} value
     * @param {?} dicname
     * @return {?}
     */
    DicPipe.prototype.transform = /**
     * @param {?} value
     * @param {?} dicname
     * @return {?}
     */
    function (value, dicname) {
        return value ? dicname ? this.supdic.getdicLabel(dicname.toUpperCase(), value) : value : value;
    };
    DicPipe.decorators = [
        { type: Pipe, args: [{ name: 'dicpipe' },] }
    ];
    /** @nocollapse */
    DicPipe.ctorParameters = function () { return [
        { type: SupDic }
    ]; };
    return DicPipe;
}());
export { DicPipe };
if (false) {
    /**
     * @type {?}
     * @private
     */
    DicPipe.prototype.supdic;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiRGljUGlwZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2xiZi8iLCJzb3VyY2VzIjpbImxpYi9sYmxpYnMvRGljUGlwZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7OztBQUFBLE9BQU8sRUFBRSxJQUFJLEVBQWlCLE1BQU0sZUFBZSxDQUFDO0FBQ3BELE9BQU8sRUFBRSxNQUFNLEVBQUUsTUFBTSxVQUFVLENBQUM7Ozs7O0FBTWxDO0lBR0UsaUJBQW9CLE1BQWM7UUFBZCxXQUFNLEdBQU4sTUFBTSxDQUFRO0lBRWxDLENBQUM7Ozs7OztJQUVELDJCQUFTOzs7OztJQUFULFVBQVUsS0FBYSxFQUFFLE9BQWU7UUFFdEMsT0FBTyxLQUFLLENBQUMsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxXQUFXLENBQUMsT0FBTyxDQUFDLFdBQVcsRUFBRSxFQUFFLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQztJQUVqRyxDQUFDOztnQkFYRixJQUFJLFNBQUMsRUFBRSxJQUFJLEVBQUUsU0FBUyxFQUFFOzs7O2dCQU5oQixNQUFNOztJQW1CZixjQUFDO0NBQUEsQUFiRCxJQWFDO1NBWlksT0FBTzs7Ozs7O0lBRU4seUJBQXNCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgUGlwZSwgUGlwZVRyYW5zZm9ybSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBTdXBEaWMgfSBmcm9tICcuL1N1cERpYyc7XHJcblxyXG4vKlxyXG4qIOWtl+WFuOaYvuekuueuoemBk1xyXG4qICAgMTB8ZGljcGlwZTphYWUxNDBcclxuKi9cclxuQFBpcGUoeyBuYW1lOiAnZGljcGlwZScgfSlcclxuZXhwb3J0IGNsYXNzIERpY1BpcGUgaW1wbGVtZW50cyBQaXBlVHJhbnNmb3JtIHtcclxuXHJcbiAgY29uc3RydWN0b3IocHJpdmF0ZSBzdXBkaWM6IFN1cERpYykge1xyXG5cclxuICB9XHJcblxyXG4gIHRyYW5zZm9ybSh2YWx1ZTogc3RyaW5nLCBkaWNuYW1lOiBzdHJpbmcpIHtcclxuXHJcbiAgICByZXR1cm4gdmFsdWUgPyBkaWNuYW1lID8gdGhpcy5zdXBkaWMuZ2V0ZGljTGFiZWwoZGljbmFtZS50b1VwcGVyQ2FzZSgpLCB2YWx1ZSkgOiB2YWx1ZSA6IHZhbHVlO1xyXG5cclxuICB9XHJcblxyXG59Il19