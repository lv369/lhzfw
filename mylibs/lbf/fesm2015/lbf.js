import { Component, Injectable, ɵɵdefineInjectable, ɵɵinject, Pipe, Directive, TemplateRef, Host, Input, EventEmitter, Output, ViewChild, NgModule, Injector } from '@angular/core';
import { __awaiter, __decorate, __metadata } from 'tslib';
import { Observable, forkJoin, Subscription, of, throwError } from 'rxjs';
import { HttpHeaders, HttpRequest, HttpEventType, HttpResponse, HttpClient, HttpResponseBase, HttpErrorResponse } from '@angular/common/http';
import { debounceTime, distinctUntilChanged, catchError, mergeMap } from 'rxjs/operators';
import { Md5 } from 'ts-md5/dist/md5';
import { InputBoolean, InputNumber, NzMessageService, NzModalService, NgZorroAntdModule, NzNotificationService } from 'ng-zorro-antd';
import { AlainThemeModule, _HttpClient } from '@delon/theme';
import { XlsxService, DelonABCModule } from '@delon/abc';
import { DelonChartModule } from '@delon/chart';
import { DelonACLModule } from '@delon/acl';
import { DelonUtilModule } from '@delon/util';
import { DelonAuthModule, DA_SERVICE_TOKEN } from '@delon/auth';
import { DelonFormModule } from '@delon/form';
import { DelonCacheModule } from '@delon/cache';
import { XlsxService as XlsxService$1 } from '@delon/abc/xlsx';
import { CommonModule } from '@angular/common';
import { RouterModule, Router } from '@angular/router';

/**
 * @fileoverview added by tsickle
 * Generated from: lib/lbf.component.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class LbfComponent {
    constructor() { }
    /**
     * @return {?}
     */
    ngOnInit() {
    }
}
LbfComponent.decorators = [
    { type: Component, args: [{
                selector: 'lb-lbf',
                template: `
    <p>
      lbf works!
    </p>
  `
            }] }
];
/** @nocollapse */
LbfComponent.ctorParameters = () => [];

/**
 * @fileoverview added by tsickle
 * Generated from: lib/ServiceConfig.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class ServiceInfo {
    constructor() {
        this.APPID = "TEST_APPID";
        this.APPKEY = "TEST_APPKEY";
        this.PWDFLAG = "H5CS3";
        this.LOGTOKEN = "";
        this.VERSION = "20190730";
        this.APPINFO = {
            "name": "测试",
            "description": ""
        };
        this.APPSUFFIX = this.APPINFO.name;
        this.user = { aac003: "", name: "", roleid: "", dept: "", avatar: "./assets/tmp/img/avatar.jpg" };
        this.baseUrl = "";
        this.molssUrl = "mohrss/chbx";
        this.uploadurl = "mohrss/chbx/upload";
        this._adminRole = [];
    }
    /**
     * @param {?} adminRole
     * @return {?}
     */
    setAdminRole(adminRole) {
        this._adminRole = adminRole;
    }
    /**
     * @return {?}
     */
    get isAdmin() {
        if (!this._adminRole || !this.user || !this.user.roleid)
            return false;
        return this._adminRole.includes(this.user.roleid);
    }
    /**
     * @param {?} sname
     * @param {?} timestamp
     * @param {?} para
     * @return {?}
     */
    getSign(sname, timestamp, para) {
        /** @type {?} */
        const hashstr = this.APPKEY + para + timestamp + sname + this.LOGTOKEN;
        return Md5.hashStr(hashstr).toString();
    }
    /**
     * @param {?} info
     * @return {?}
     */
    setSinfo(info) {
        this.APPID = info.APPID;
        this.APPKEY = info.APPKEY;
        this.VERSION = info.VERSION;
        this.APPINFO.name = info.APPNAME;
        this.APPSUFFIX = this.APPINFO.name;
    }
    /**
     * @param {?} user
     * @return {?}
     */
    setUser(user) {
        this.user = Object.assign({}, this.user, user);
        // this.user.name = user.name;
        // this.user.aac003 = user.aac003;
        this.user.roleid = user.usertype;
        // this.user.dept = user.dept;
        sessionStorage.setItem("userinfo", JSON.stringify(this.user));
    }
    /**
     * @return {?}
     */
    loaduser() {
        /** @type {?} */
        const userinfo = sessionStorage.getItem("userinfo");
        try {
            /** @type {?} */
            const u = JSON.parse(userinfo);
            this.user = Object.assign({}, this.user, u);
            // this.user.name = u.name;
            // this.user.aac003 = u.aac003;
            // this.user.roleid = u.roleid;
            // this.user.dept = u.dept;
        }
        catch (ex) {
        }
    }
    /**
     * @return {?}
     */
    getTimestamp() {
        /** @type {?} */
        const d = new Date();
        return Date.UTC(d.getFullYear(), d.getMonth(), d.getDate(), d.getHours(), d.getMinutes(), d.getSeconds(), d.getMilliseconds());
    }
    /**
     * @param {?} token
     * @return {?}
     */
    saveToken(token) {
        sessionStorage.setItem("logintoken", token);
    }
    /**
     * @return {?}
     */
    getToken() {
        this.LOGTOKEN = sessionStorage.getItem("logintoken");
        if (this.LOGTOKEN == null)
            this.LOGTOKEN = "";
    }
    /**
     * @return {?}
     */
    clearToken() {
        sessionStorage.setItem("logintoken", "");
    }
    /**
     * @param {?} md5Str
     * @return {?}
     */
    md5(md5Str) {
        return Md5.hashStr(md5Str).toString();
    }
    /**
     * @param {?} pwd
     * @return {?}
     */
    pwdmd5(pwd) {
        return this.md5(pwd + this.PWDFLAG);
    }
}
ServiceInfo.decorators = [
    { type: Injectable, args: [{
                providedIn: 'root'
            },] }
];
/** @nocollapse */
ServiceInfo.ctorParameters = () => [];
/** @nocollapse */ ServiceInfo.ngInjectableDef = ɵɵdefineInjectable({ factory: function ServiceInfo_Factory() { return new ServiceInfo(); }, token: ServiceInfo, providedIn: "root" });
if (false) {
    /** @type {?} */
    ServiceInfo.prototype.APPID;
    /**
     * @type {?}
     * @private
     */
    ServiceInfo.prototype.APPKEY;
    /**
     * @type {?}
     * @private
     */
    ServiceInfo.prototype.PWDFLAG;
    /** @type {?} */
    ServiceInfo.prototype.LOGTOKEN;
    /** @type {?} */
    ServiceInfo.prototype.VERSION;
    /** @type {?} */
    ServiceInfo.prototype.APPINFO;
    /** @type {?} */
    ServiceInfo.prototype.APPSUFFIX;
    /** @type {?} */
    ServiceInfo.prototype.user;
    /** @type {?} */
    ServiceInfo.prototype.baseUrl;
    /** @type {?} */
    ServiceInfo.prototype.molssUrl;
    /** @type {?} */
    ServiceInfo.prototype.uploadurl;
    /** @type {?} */
    ServiceInfo.prototype._adminRole;
}

/**
 * @fileoverview added by tsickle
 * Generated from: lib/lblibs/Lbjk.service.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class HttpService {
    /**
     * @param {?} httpClient
     * @param {?} serviceInfo
     */
    constructor(httpClient, serviceInfo) {
        this.httpClient = httpClient;
        this.serviceInfo = serviceInfo;
    }
    /**
     * 登录服务
     * @param {?} username 用户名
     * @param {?} password 用户密码
     * @return {?} 登录结果 Promise<any>
     */
    login(username, password) {
        /** @type {?} */
        let c = localStorage.getItem('usercache');
        if (!c) {
            c = "{}";
        }
        /** @type {?} */
        let usercache = JSON.parse(c);
        /** @type {?} */
        let ROLEID = null;
        if (usercache && usercache[username]) {
            ROLEID = usercache[username].roleid;
        }
        return new Promise((/**
         * @param {?} resolve
         * @param {?} _
         * @return {?}
         */
        (resolve, _) => {
            this.lbservice("FR_login", { "para": { "USERID": username, "PWD": this.serviceInfo.pwdmd5(password), ROLEID } }, 500, true).then((/**
             * @param {?} resdata
             * @return {?}
             */
            (resdata) => {
                // 设置登录token
                if (resdata.code > 0) {
                    this.serviceInfo.LOGTOKEN = resdata.message.LOGINTOKEN;
                    this.serviceInfo.saveToken(resdata.message.LOGINTOKEN);
                    this.serviceInfo.setUser({ aac003: resdata.message.NAME, name: username, dept: resdata.message.DEPT, roleid: resdata.message.ROLEID });
                    if (!usercache) {
                        usercache = {};
                    }
                    if (!usercache[username]) {
                        usercache[username] = { li: [], cdr: [] };
                    }
                    usercache[username].li.push(new Date());
                    usercache[username].roleid = resdata.message.ROLEID;
                    localStorage.setItem('usercache', JSON.stringify(usercache));
                }
                resolve(resdata);
            }));
        }));
    }
    /**
     * @param {?} roleid
     * @return {?}
     */
    changeRole(roleid) {
        /** @type {?} */
        let c = localStorage.getItem('usercache');
        if (!c) {
            c = "{}";
        }
        /** @type {?} */
        let usercache = JSON.parse(c);
        return new Promise((/**
         * @param {?} resolve
         * @param {?} _
         * @return {?}
         */
        (resolve, _) => {
            this.lbservice("FR_cdr", { "para": { ROLEID: roleid } }).then((/**
             * @param {?} resdata
             * @return {?}
             */
            (resdata) => {
                // 设置登录token
                if (resdata.code > 0) {
                    this.serviceInfo.LOGTOKEN = resdata.message.LOGINTOKEN;
                    this.serviceInfo.user.roleid = roleid;
                    if (!usercache) {
                        usercache = {};
                    }
                    if (!usercache[this.serviceInfo.user.name]) {
                        usercache[this.serviceInfo.user.name] = { li: [], cdr: [] };
                    }
                    usercache[this.serviceInfo.user.name].cdr.push(new Date());
                    usercache[this.serviceInfo.user.name].roleid = resdata.message.ROLEID;
                    localStorage.setItem('usercache', JSON.stringify(usercache));
                }
                resolve(resdata);
            }));
        }));
    }
    /**
     * @param {?} sname
     * @param {?} para
     * @param {?=} debounce
     * @param {?=} anonymous
     * @return {?}
     */
    lbservice2(sname, para, debounce = 0, anonymous = false) {
        /** @type {?} */
        const httpOptions = {
            headers: new HttpHeaders({
                'SNAME': sname
            })
        };
        /** @type {?} */
        let url = this.serviceInfo.molssUrl;
        if (anonymous) {
            url = url + '?_allow_anonymous=true';
        }
        return this.httpClient.post(url, para, httpOptions);
    }
    /**
     *  访问服务端
     * @param {?} sname 服务名
     * @param {?} para 入参
     * @param {?=} debounce 防抖设置，默认0
     * @param {?=} anonymous 是否匿名访问，默认否
     * @return {?}
     */
    lbservice(sname, para, debounce = 0, anonymous = false) {
        /** @type {?} */
        const httpOptions = {
            headers: new HttpHeaders({
                'SNAME': sname
            })
        };
        /** @type {?} */
        let url = this.serviceInfo.molssUrl;
        if (anonymous) {
            url = url + '?_allow_anonymous=true';
        }
        return new Promise((/**
         * @param {?} resolve
         * @param {?} _
         * @return {?}
         */
        (resolve, _) => {
            /** @type {?} */
            let result = { code: 0, errmsg: "请求失败", msg: {} };
            this.httpClient.post(url, para, httpOptions)
                .pipe(debounceTime(debounce), distinctUntilChanged(), 
            // 接收其他拦截器后产生的异常消息
            catchError((/**
             * @param {?} error
             * @return {?}
             */
            (error) => {
                console.log("调用失败", error);
                resolve({ code: -100, errmsg: "请求失败" });
                return error;
            })))
                .subscribe((/**
             * @param {?} resdata
             * @return {?}
             */
            (resdata) => {
                // setting language data
                // console.log(resdata)
                result = resdata;
            }), (/**
             * @return {?}
             */
            () => { }), (/**
             * @return {?}
             */
            () => { resolve(result); }));
        }));
    }
    /**
     * 文件上传类服务
     *   只支持单文件上传
     * @param {?} sname 服务名
     * @param {?} item 文件上传信息
     * @param {?} para 参数
     * @return {?}
     */
    lbfileupload(sname, item, para) {
        /** @type {?} */
        const formData = new FormData();
        formData.append('file', (/** @type {?} */ (item.file)));
        formData.append('paras', JSON.stringify({ para }));
        /** @type {?} */
        const req = new HttpRequest('POST', item.action ? item.action : this.serviceInfo.uploadurl, formData, {
            headers: new HttpHeaders({
                'SNAME': sname
            }),
            reportProgress: true,
            withCredentials: true
        });
        // 始终返回一个 `Subscription` 对象，nz-upload 会在适当时机自动取消订阅
        return this.httpClient.request(req).subscribe((/**
         * @param {?} event
         * @return {?}
         */
        (event) => {
            if (event.type === HttpEventType.UploadProgress) {
                if (event.total > 0) {
                    // tslint:disable-next-line:no-any
                    ((/** @type {?} */ (event))).percent = event.loaded / event.total * 100;
                }
                // 处理上传进度条，必须指定 `percent` 属性来表示进度
                item.onProgress(event, item.file);
            }
            else if (event instanceof HttpResponse) {
                // 处理成功
                item.onSuccess(event.body, item.file, event);
            }
        }), (/**
         * @param {?} err
         * @return {?}
         */
        (err) => {
            // 处理失败
            item.onError(err, item.file);
        }));
    }
}
HttpService.decorators = [
    { type: Injectable, args: [{ providedIn: 'root', },] }
];
/** @nocollapse */
HttpService.ctorParameters = () => [
    { type: HttpClient },
    { type: ServiceInfo }
];
/** @nocollapse */ HttpService.ngInjectableDef = ɵɵdefineInjectable({ factory: function HttpService_Factory() { return new HttpService(ɵɵinject(HttpClient), ɵɵinject(ServiceInfo)); }, token: HttpService, providedIn: "root" });
if (false) {
    /**
     * @type {?}
     * @private
     */
    HttpService.prototype.httpClient;
    /**
     * @type {?}
     * @private
     */
    HttpService.prototype.serviceInfo;
}

/**
 * @fileoverview added by tsickle
 * Generated from: lib/lblibs/SupDic.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class SupDic {
    /**
     * @param {?} lbservice
     * @param {?} httpclient
     */
    constructor(lbservice, httpclient) {
        this.lbservice = lbservice;
        this.httpclient = httpclient;
        this.dicCache = new Object();
        this.ka010203 = {};
    }
    /**
     * 获取字典信息,返回数组
     * @param {?} dicName 字典值
     * @return {?} 字典信息，未取到返回[]
     */
    getDic(dicName) {
        if (this.dicCache && this.dicCache.hasOwnProperty(dicName)) {
            return this.dicCache[dicName].diclist;
        }
        return [];
    }
    /**
     * 获取字典信息,返回object
     * @param {?} dicName 字典值
     * @return {?} 字典信息
     */
    getDicObj(dicName) {
        if (this.dicCache && this.dicCache.hasOwnProperty(dicName)) {
            return this.dicCache[dicName].dicobj;
        }
    }
    /**
     * 获取指定字典指定值对应的标签值
     * @param {?} key 字典名称
     * @param {?} val 值
     * @return {?}
     */
    getdicLabel(key, val) {
        /** @type {?} */
        const dic = this.getDicObj(key);
        if (dic && dic.hasOwnProperty(val)) {
            return dic[val].CNAME;
        }
        return val;
    }
    /**
     * 异步方式获取字典信息，返回SF的字典数组
     *   始终从数据库中获取，获取的字典将会写入到缓存中
     * @param {?} dicName 字典名称
     * @return {?}
     */
    getSFDicAsync(dicName) {
        // const dic = this.getSFDic(dicName);
        // if (dic.length > 0) {
        //   return of(dic);
        // }
        // const dic = this.getSFDic(dicName);
        // if (dic.length > 0) {
        //   return of(dic);
        // }
        /** @type {?} */
        const aaa = this.lbservice;
        /** @type {?} */
        const dicCache = this.dicCache;
        return new Observable((/**
         * @param {?} observer
         * @return {?}
         */
        function subscribe(observer) {
            // FR_GetDIC
            aaa.lbservice('FR_GetAllDIC', {}).then((/**
             * @param {?} resdata
             * @return {?}
             */
            resdata => {
                if (resdata.code > 0) {
                    /** @type {?} */
                    const dicobj = new Object();
                    /** @type {?} */
                    const dicarr = resdata.message.list;
                    /** @type {?} */
                    const reslist = [];
                    for (const el of dicarr) {
                        el.text = el.CNAME;
                        el.value = el.CCODE;
                        dicobj[el.CCODE] = el;
                        reslist.push(el);
                    }
                    dicCache[dicName] = { diclist: dicarr, dicobj };
                    observer.next(reslist);
                    observer.complete();
                }
            }));
        }));
    }
    /**
     * 获取SF格式的字典信息
     * @param {?} dicName 字典值
     * @return {?}
     */
    getSFDic(dicName) {
        /** @type {?} */
        const reslist = [];
        /** @type {?} */
        const diclist = this.getDic(dicName);
        for (const dic of diclist) {
            reslist.push({ label: dic.CNAME, value: dic.CCODE });
        }
        return reslist;
    }
    /**
     * 加载所有字典
     * @return {?}
     */
    loadAllDic() {
        return __awaiter(this, void 0, void 0, function* () {
            // FR_GetAllDIC
            return new Promise((/**
             * @param {?} resolve
             * @param {?} _
             * @return {?}
             */
            (resolve, _) => {
                this.lbservice.lbservice('FR_GetAllDIC', {}).then((/**
                 * @param {?} resdata
                 * @return {?}
                 */
                resdata => {
                    if (resdata.code > 0) {
                        // this.dicCache=resdata.message;
                        for (const d in resdata.message) {
                            if (resdata.message.hasOwnProperty(d)) {
                                /** @type {?} */
                                const a = new Object();
                                /** @type {?} */
                                const dicarr = resdata.message[d];
                                for (const el of dicarr) {
                                    el.text = el.CNAME;
                                    el.value = el.CCODE;
                                    a[el.CCODE] = el;
                                }
                                /** @type {?} */
                                const dic = { diclist: dicarr, dicobj: a };
                                this.dicCache[d] = dic;
                            }
                        }
                    }
                    resolve();
                }));
            }));
        });
    }
}
SupDic.decorators = [
    { type: Injectable, args: [{ providedIn: 'root' },] }
];
/** @nocollapse */
SupDic.ctorParameters = () => [
    { type: HttpService },
    { type: HttpClient }
];
/** @nocollapse */ SupDic.ngInjectableDef = ɵɵdefineInjectable({ factory: function SupDic_Factory() { return new SupDic(ɵɵinject(HttpService), ɵɵinject(HttpClient)); }, token: SupDic, providedIn: "root" });
if (false) {
    /**
     * @type {?}
     * @private
     */
    SupDic.prototype.dicCache;
    /** @type {?} */
    SupDic.prototype.ka010203;
    /**
     * @type {?}
     * @private
     */
    SupDic.prototype.lbservice;
    /**
     * @type {?}
     * @private
     */
    SupDic.prototype.httpclient;
}

/**
 * @fileoverview added by tsickle
 * Generated from: lib/lblibs/DicPipe.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*
* 字典显示管道
*   10|dicpipe:aae140
*/
class DicPipe {
    /**
     * @param {?} supdic
     */
    constructor(supdic) {
        this.supdic = supdic;
    }
    /**
     * @param {?} value
     * @param {?} dicname
     * @return {?}
     */
    transform(value, dicname) {
        return value ? dicname ? this.supdic.getdicLabel(dicname.toUpperCase(), value) : value : value;
    }
}
DicPipe.decorators = [
    { type: Pipe, args: [{ name: 'dicpipe' },] }
];
/** @nocollapse */
DicPipe.ctorParameters = () => [
    { type: SupDic }
];
if (false) {
    /**
     * @type {?}
     * @private
     */
    DicPipe.prototype.supdic;
}

/**
 * @fileoverview added by tsickle
 * Generated from: lib/lblibs/SupExcel.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class SupExcel {
    /**
     * @param {?} lbservice
     * @param {?} xlsx
     */
    constructor(lbservice, xlsx) {
        this.lbservice = lbservice;
        this.xlsx = xlsx;
    }
    /**
     * 导出excel
     * @param {?} columns 字段信息
     * @param {?} sname 查询服务名
     * @param {?} para 查询服务参数
     * @param {?} total 总条数
     * @param {?=} onetime 单次请求数，默认20
     * @return {?}
     */
    export(columns, sname, para, total, onetime = 20) {
        /** @type {?} */
        const data = [columns.map((/**
             * @param {?} i
             * @return {?}
             */
            i => i.title))];
        /** @type {?} */
        const pageCount = Math.ceil(total / 100);
        // 请求信息
        /** @type {?} */
        const reqtime = Array(pageCount).fill({}).map((/**
         * @param {?} _item
         * @param {?} idx
         * @return {?}
         */
        (_item, idx) => {
            return {
                PAGEIDX: idx,
                PAGESIZE: 100,
            };
        }));
        /** @type {?} */
        const obs = [];
        // 把20次的请求合一起
        reqtime.forEach((/**
         * @param {?} page
         * @return {?}
         */
        page => {
            obs.push(this.lbservice.lbservice2(sname, { para, page }));
        }));
        /** @type {?} */
        const allcall = forkJoin(obs);
        return new Promise((/**
         * @param {?} resolve
         * @param {?} _
         * @return {?}
         */
        (resolve, _) => {
            allcall.subscribe((/**
             * @param {?} results
             * @return {?}
             */
            results => {
                results.forEach((/**
                 * @param {?} item
                 * @return {?}
                 */
                (item) => {
                    if (item.code > 0) {
                        item.message.list.forEach((/**
                         * @param {?} i
                         * @return {?}
                         */
                        i => data.push(columns.map((/**
                         * @param {?} c
                         * @return {?}
                         */
                        c => i[(/** @type {?} */ (c.index))])))));
                    }
                }));
                this.xlsx.export({
                    sheets: [
                        {
                            data,
                            name: 'sheet1',
                        },
                    ],
                }).then((/**
                 * @return {?}
                 */
                () => {
                    resolve(null);
                }));
            }));
        }));
    }
}
SupExcel.decorators = [
    { type: Injectable, args: [{ providedIn: 'root' },] }
];
/** @nocollapse */
SupExcel.ctorParameters = () => [
    { type: HttpService },
    { type: XlsxService }
];
/** @nocollapse */ SupExcel.ngInjectableDef = ɵɵdefineInjectable({ factory: function SupExcel_Factory() { return new SupExcel(ɵɵinject(HttpService), ɵɵinject(XlsxService$1)); }, token: SupExcel, providedIn: "root" });
if (false) {
    /**
     * @type {?}
     * @private
     */
    SupExcel.prototype.lbservice;
    /**
     * @type {?}
     * @private
     */
    SupExcel.prototype.xlsx;
}

/**
 * @fileoverview added by tsickle
 * Generated from: lib/components/grid/LbColumn.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class LbRowSource {
    constructor() {
        this.titles = {};
        this.rows = {};
    }
    /**
     * @param {?} type
     * @param {?} path
     * @param {?} ref
     * @return {?}
     */
    add(type, path, ref) {
        this[type === 'title' ? 'titles' : 'rows'][path] = ref;
    }
    /**
     * @param {?} path
     * @return {?}
     */
    getTitle(path) {
        return this.titles[path];
    }
    /**
     * @param {?} path
     * @return {?}
     */
    getRow(path) {
        return this.rows[path];
    }
}
LbRowSource.decorators = [
    { type: Injectable }
];
/** @nocollapse */
LbRowSource.ctorParameters = () => [];
if (false) {
    /**
     * @type {?}
     * @private
     */
    LbRowSource.prototype.titles;
    /**
     * @type {?}
     * @private
     */
    LbRowSource.prototype.rows;
}
class LbRowDirective {
    /**
     * @param {?} ref
     * @param {?} source
     */
    constructor(ref, source) {
        this.ref = ref;
        this.source = source;
        console.log('lb-row constructor');
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        this.source.add(this.type, this.id, this.ref);
        console.log(this.id, this.ref);
    }
}
LbRowDirective.decorators = [
    { type: Directive, args: [{ selector: '[lb-row]' },] }
];
/** @nocollapse */
LbRowDirective.ctorParameters = () => [
    { type: TemplateRef },
    { type: LbRowSource, decorators: [{ type: Host }] }
];
LbRowDirective.propDecorators = {
    id: [{ type: Input, args: ['lb-row',] }],
    type: [{ type: Input }]
};
if (false) {
    /** @type {?} */
    LbRowDirective.prototype.id;
    /** @type {?} */
    LbRowDirective.prototype.type;
    /**
     * @type {?}
     * @private
     */
    LbRowDirective.prototype.ref;
    /**
     * @type {?}
     * @private
     */
    LbRowDirective.prototype.source;
}

/**
 * @fileoverview added by tsickle
 * Generated from: lib/components/grid/grid.component.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class GridComponent {
    /**
     * @param {?} supdic
     * @param {?} supexcel
     * @param {?} sinfo
     * @param {?} lbSource
     */
    constructor(supdic, supexcel, sinfo, lbSource) {
        this.supdic = supdic;
        this.supexcel = supexcel;
        this.sinfo = sinfo;
        this.lbSource = lbSource;
        this.grhxurl = ['/ybjg/grhx'];
        this.isGrhx = true;
        /**
         * 每页数量，当设置为 0 表示不分页，默认：10
         */
        this.ps = 10;
        this.datas = 'MohrssExInterface/gridsvr';
        this.statisticals = [];
        this._statcal = {};
        // 是否加载中
        this.loading = false;
        this.loadingChange = new EventEmitter();
        // 服务ID
        this._sname = '';
        this.reqparas = { params: { sname: this._sname, form: this._queryparas, stacal: this._statcal }, method: 'post', allInBody: true };
        this.process = (/**
         * @param {?} data
         * @return {?}
         */
        (data) => {
            return data;
        });
    }
    /**
     * 表格数值
     * @return {?}
     */
    get data() {
        return this.st._data;
    }
    /**
     * @return {?}
     */
    get innerSt() {
        return this.st;
    }
    /**
     * @param {?} columns
     * @return {?}
     */
    set columns(columns) {
        // 统计类信息
        this.statisticals = [];
        columns.forEach((/**
         * @param {?} col
         * @return {?}
         */
        col => {
            if (col.render) {
                col.render2 = col.render;
                col.render = '';
            }
            else {
                if (this.isGrhx && col.index === 'AAC003' && this.sinfo.isAdmin) {
                    col.render = 'grhx';
                }
            }
            if (col.renderTitle) {
                col.renderTitle2 = col.renderTitle;
                col.renderTitle = '';
            }
            /** @type {?} */
            let key = col.index;
            if (key instanceof Array) {
                key = key[0];
            }
            col.key = key;
            // 添加字典信息
            if (col.dic) {
                col.format = (/**
                 * @param {?} a
                 * @param {?} b
                 * @return {?}
                 */
                (a, b) => this.supdic.getdicLabel(b.dic, a[b.key]));
                col.filter = { menus: this.supdic.getDic(col.dic) };
            }
            // 添加统计类型信息
            /** @type {?} */
            const val = col.statistical;
            if (!val) {
                this.statisticals.push({ key, show: false, type: "" });
                this._statcal[key] = "";
                return;
            }
            /** @type {?} */
            const item = Object.assign({ digits: 2, currency: null }, (typeof val === 'string'
                ? { type: (/** @type {?} */ (val)) }
                : ((/** @type {?} */ (val)))));
            /** @type {?} */
            const stcal = item.type;
            item.digits = stcal === "count" ? 0 : 2;
            item.type = this.customStatistical;
            item.currency = false;
            col.statistical = item;
            this.statisticals.push({ key, show: true, type: stcal });
            this._statcal[key] = stcal;
        }));
        this._columns = columns;
    }
    /**
     * @return {?}
     */
    get columns() {
        return this._columns;
    }
    /**
     * @param {?} sname
     * @return {?}
     */
    set sname(sname) {
        this._sname = sname;
        this.reqparas.params.sname = this._sname;
    }
    // 查询参数
    /**
     * @return {?}
     */
    get queryparas() {
        return this._queryparas;
    }
    /**
     * @param {?} value
     * @return {?}
     */
    set queryparas(value) {
        this._queryparas = value;
        this.reqparas.params.form = this._queryparas;
    }
    /**
     * 加载指示符
     * @param {?} loadingIndicator
     * @return {?}
     */
    set loadingIndicator(loadingIndicator) {
        this.st.loadingIndicator = loadingIndicator;
    }
    /**
     * 延迟显示加载效果的时间（防止闪烁）
     * @param {?} loadingDelay
     * @return {?}
     */
    set loadingDelay(loadingDelay) {
        this.st.loadingDelay = loadingDelay;
    }
    /**
     * 当前页码
     * @param {?} pi
     * @return {?}
     */
    set pi(pi) {
        this.st.pi = pi;
    }
    /**
     * @return {?}
     */
    get pi() {
        return this.st.pi;
    }
    /**
     *  分页器配置
     * @param {?} page
     * @return {?}
     */
    set page(page) {
        this.st.page = page;
    }
    /**
     * @return {?}
     */
    get page() {
        return this.st.page;
    }
    /**
     * 无数据时显示内容
     * @param {?} noResult
     * @return {?}
     */
    set noResult(noResult) {
        this.st.noResult = noResult;
    }
    /**
     * @return {?}
     */
    get noResult() {
        return this.st.noResult;
    }
    /**
     * 是否显示边框
     * @param {?} bordered
     * @return {?}
     */
    set bordered(bordered) {
        this.st.bordered = bordered;
    }
    /**
     * @return {?}
     */
    get bordered() {
        return this.st.bordered;
    }
    /**
     * 表格标题
     * @param {?} header
     * @return {?}
     */
    set header(header) {
        this.st.header = header;
    }
    /**
     * 表格底部
     * @param {?} footer
     * @return {?}
     */
    set footer(footer) {
        this.st.footer = footer;
    }
    /**
     * 表格顶部额外内容，一般用于添加合计行
     * @param {?} bodyHeader
     * @return {?}
     */
    set bodyHeader(bodyHeader) {
        this.st.bodyHeader = bodyHeader;
    }
    /**
     * @param {?} expandRowByClick
     * @return {?}
     */
    set expandRowByClick(expandRowByClick) {
        this.st.expandRowByClick = expandRowByClick;
    }
    /**
     * @param {?} expandAccordion
     * @return {?}
     */
    set expandAccordion(expandAccordion) {
        this.st.expandAccordion = expandAccordion;
    }
    /**
     * @param {?} expand
     * @return {?}
     */
    set expand(expand) {
        this.st.expand = expand;
    }
    /**
     * @return {?}
     */
    get filteredData() {
        return this.st.filteredData;
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        setTimeout((/**
         * @return {?}
         */
        () => {
            this.columns.forEach((/**
             * @param {?} col
             * @return {?}
             */
            col => {
                this.restoreRender(col);
            }));
            this.st.resetColumns();
        }), 100);
    }
    /**
     * @param {?=} options
     * @return {?}
     */
    resetColumns(options) {
        this.st.resetColumns(options);
    }
    /**
     * 移除行
     * @param {?} data STData | STData[] | number
     * @return {?}
     */
    removeRow(data) {
        this.st.removeRow(data);
    }
    /**
     * 清空所有数据
     * @return {?}
     */
    clear() {
        this.st.clear();
    }
    /**
     * @param {?=} queryparas
     * @return {?}
     */
    reload(queryparas) {
        this.st.loading = true;
        if (queryparas)
            this.st.req.params.form = queryparas;
        this.st.reload();
        this.st.cd();
    }
    /**
     * @param {?} index
     * @param {?} data
     * @return {?}
     */
    setRow(index, data) {
        this.st.setRow(index, data);
    }
    /**
     * @param {?} event
     * @return {?}
     */
    reset(event) {
        this.loading = true;
        this.loadingChange.emit(true);
        this.st.reset(event);
        this.st.cd();
    }
    /**
     * @private
     * @param {?} values
     * @param {?} col
     * @param {?} list
     * @param {?=} rawData
     * @return {?}
     */
    customStatistical(values, col, list, rawData) {
        if (rawData.code < 1)
            return { value: "", text: "" };
        if (rawData.page.statistical) {
            /** @type {?} */
            let key = col.index;
            if (key instanceof Array) {
                key = key[0];
            }
            if (rawData.page.statistical[key]) {
                return { value: rawData.page.statistical[key], text: "" };
            }
            return { value: "", text: "" };
        }
        return { value: "", text: "" };
    }
    // 导出excel
    /**
     * @param {?=} opt
     * @return {?}
     */
    export(opt) {
        /** @type {?} */
        const data = this.st._data;
        this.st.export(data, opt);
    }
    /**
     * @return {?}
     */
    exportAll() {
        this.loading = true;
        this.supexcel.export(this.columns, this._sname, this.st.req.params.form, this.st.total).then((/**
         * @return {?}
         */
        () => {
            this.loading = false;
        }));
    }
    /**
     * @private
     * @param {?} item
     * @return {?}
     */
    restoreRender(item) {
        if (item.renderTitle2) {
            item.__renderTitle = this.lbSource.getTitle(item.renderTitle2);
        }
        if (item.render2) {
            item.__render = this.lbSource.getRow(item.render2);
        }
    }
}
GridComponent.decorators = [
    { type: Component, args: [{
                selector: 'lb-grid',
                template: "<st #st [data]=\"datas\" [req]=\"reqparas\" [loading]=\"loading\" [body]=\"bodyTpl\" [scroll]=\"scroll\" multiSort [ps]=\"ps\"\n    [res]=\"{reName :{list:'message.list',total:'page.COUNT',process: dataProcess}}\" [columns]=\"columns\">\n\n    <ng-template st-row=\"grhx\" let-item let-c=\"column\">\n        <a [routerLink]=\"grhxurl\" [queryParams]=\"item\">{{item[c.indexKey]}}</a>\n    </ng-template>\n\n    <ng-template #bodyTpl let-s>\n        <tr class=\"bg-grey-lighter\">\n            <ng-container *ngFor=\"let statis of statisticals\">\n                <td>\n                    <div *ngIf=\"statis.show&&s[statis.key]\">\n                        {{ s[statis.key].text}}\n                    </div>\n                </td>\n            </ng-container>\n        </tr>\n    </ng-template>\n</st>",
                providers: [LbRowSource],
                styles: [""]
            }] }
];
/** @nocollapse */
GridComponent.ctorParameters = () => [
    { type: SupDic },
    { type: SupExcel },
    { type: ServiceInfo },
    { type: LbRowSource }
];
GridComponent.propDecorators = {
    grhxurl: [{ type: Input }],
    isGrhx: [{ type: Input }],
    columns: [{ type: Input, args: ['columns',] }],
    sname: [{ type: Input, args: ['sname',] }],
    queryparas: [{ type: Input }],
    ps: [{ type: Input }],
    loading: [{ type: Input }],
    loadingChange: [{ type: Output }],
    loadingIndicator: [{ type: Input }],
    loadingDelay: [{ type: Input }],
    scroll: [{ type: Input }],
    pi: [{ type: Input }],
    noResult: [{ type: Input }],
    bordered: [{ type: Input }],
    header: [{ type: Input }],
    footer: [{ type: Input }],
    bodyHeader: [{ type: Input }],
    expandRowByClick: [{ type: Input }],
    expandAccordion: [{ type: Input }],
    expand: [{ type: Input }],
    st: [{ type: ViewChild, args: ['st', { static: true },] }]
};
__decorate([
    InputBoolean(),
    __metadata("design:type", Object)
], GridComponent.prototype, "isGrhx", void 0);
__decorate([
    InputNumber(),
    __metadata("design:type", Number),
    __metadata("design:paramtypes", [Number])
], GridComponent.prototype, "loadingDelay", null);
__decorate([
    InputBoolean(),
    __metadata("design:type", Boolean),
    __metadata("design:paramtypes", [Boolean])
], GridComponent.prototype, "bordered", null);
__decorate([
    InputBoolean(),
    __metadata("design:type", Boolean),
    __metadata("design:paramtypes", [Boolean])
], GridComponent.prototype, "expandRowByClick", null);
__decorate([
    InputBoolean(),
    __metadata("design:type", Boolean),
    __metadata("design:paramtypes", [Boolean])
], GridComponent.prototype, "expandAccordion", null);
__decorate([
    InputBoolean(),
    __metadata("design:type", TemplateRef),
    __metadata("design:paramtypes", [TemplateRef])
], GridComponent.prototype, "expand", null);
if (false) {
    /** @type {?} */
    GridComponent.prototype.grhxurl;
    /** @type {?} */
    GridComponent.prototype.isGrhx;
    /**
     * 每页数量，当设置为 0 表示不分页，默认：10
     * @type {?}
     */
    GridComponent.prototype.ps;
    /** @type {?} */
    GridComponent.prototype.datas;
    /** @type {?} */
    GridComponent.prototype.statisticals;
    /** @type {?} */
    GridComponent.prototype._statcal;
    /** @type {?} */
    GridComponent.prototype.loading;
    /** @type {?} */
    GridComponent.prototype.loadingChange;
    /**
     * 横向或纵向支持滚动，也可用于指定滚动区域的宽高度：{ x: "300px", y: "300px" }
     * @type {?}
     */
    GridComponent.prototype.scroll;
    /** @type {?} */
    GridComponent.prototype._columns;
    /** @type {?} */
    GridComponent.prototype._sname;
    /** @type {?} */
    GridComponent.prototype._queryparas;
    /** @type {?} */
    GridComponent.prototype.st;
    /** @type {?} */
    GridComponent.prototype.reqparas;
    /** @type {?} */
    GridComponent.prototype.dataProcess;
    /**
     * @type {?}
     * @private
     */
    GridComponent.prototype.process;
    /**
     * @type {?}
     * @private
     */
    GridComponent.prototype.supdic;
    /**
     * @type {?}
     * @private
     */
    GridComponent.prototype.supexcel;
    /**
     * @type {?}
     * @private
     */
    GridComponent.prototype.sinfo;
    /**
     * @type {?}
     * @private
     */
    GridComponent.prototype.lbSource;
}

/**
 * @fileoverview added by tsickle
 * Generated from: lib/components/img-upload/img-upload.component.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class ImgUploadComponent {
    /**
     * @param {?} msgsvr
     * @param {?} lbservice
     * @param {?} modalService
     */
    constructor(msgsvr, lbservice, modalService) {
        this.msgsvr = msgsvr;
        this.lbservice = lbservice;
        this.modalService = modalService;
        /**
         * 图表列表
         */
        this.fileList = [];
        this._previewImage = '';
        this._previewVisible = false;
        /**
         * 上传按钮是否显示
         */
        this.nzShowButton = true;
        /**
         * 预览按钮以及删除按钮
         */
        this.showUploadList = {
            showPreviewIcon: true,
            showRemoveIcon: true,
            hidePreviewIconInNonImage: true
        };
        /**
         * 上传事件
         */
        this.upload = (/**
         * @param {?} item
         * @return {?}
         */
        (item) => Subscription);
        /**
         * 删除事件
         */
        this.del = (/**
         * @param {?} file
         * @return {?}
         */
        (file) => new Promise((/**
         * @return {?}
         */
        () => { })));
        this._handlePreview = (/**
         * @param {?} file
         * @return {?}
         */
        (file) => {
            this._previewImage = file.url || file.thumbUrl;
            this._previewVisible = true;
        });
        /**
         * 图片上传
         */
        this._uploadimg = (/**
         * @param {?} item
         * @return {?}
         */
        (item) => {
            if (this.upload) {
                return this.upload(item);
            }
        });
        /**
         * 移除文件
         */
        this._removeIMG = (/**
         * @param {?} file
         * @return {?}
         */
        (file) => {
            return new Observable((/**
             * @param {?} obs
             * @return {?}
             */
            (obs) => {
                this.modalService.confirm({
                    nzTitle: '删除确认',
                    nzContent: '<b style="color: red;">确认要删除这张图片么？</b>',
                    nzOkText: '确定',
                    nzOkType: 'danger',
                    nzOnOk: (/**
                     * @return {?}
                     */
                    () => {
                        if (this.del) {
                            this.del(file).then((/**
                             * @param {?} resdata
                             * @return {?}
                             */
                            (resdata) => {
                                if (resdata.code < 1) {
                                    this.msgsvr.error("图片删除失败:" + resdata.errmsg);
                                    obs.next(false);
                                }
                                else {
                                    this.msgsvr.success('图片删除成功！');
                                    obs.next(true);
                                }
                            }));
                        }
                    }),
                    nzCancelText: '取消',
                    nzOnCancel: (/**
                     * @return {?}
                     */
                    () => obs.next(false))
                });
            }));
        });
        this._beforeUpload = (/**
         * @param {?} file
         * @return {?}
         */
        (file) => {
            return new Observable((/**
             * @param {?} observer
             * @return {?}
             */
            (observer) => {
                /** @type {?} */
                const imgtype = ["image/png", "image/jpeg", "image/gif", "image/bmp"];
                //  const isImg = file.type === 'image/jpeg';
                if (!imgtype.includes(file.type)) {
                    this.msgsvr.error('只能上传图片格式!');
                    observer.complete();
                    return;
                }
                observer.next(true);
                observer.complete();
            }));
        });
    }
    /**
     * @return {?}
     */
    ngOnInit() {
    }
    /**
     * 上传成功后处理
     * @param {?} info  上传返回信息
     * @return {?}
     */
    _handleChange(info) {
        /** @type {?} */
        const fileList = info.fileList;
        // 2. read from response and show file link
        if (info.file.response) {
            if (info.file.response.code < 1) {
                this.msgsvr.error("文件上传失败:" + info.file.response.errmsg);
            }
            else {
                info.file.ftoken = info.file.response.message.ftoken;
            }
            info.file.url = info.file.response.url;
        }
        // 3. filter successfully uploaded files according to response from server
        this.fileList = fileList.filter((/**
         * @param {?} item
         * @return {?}
         */
        item => {
            if (item.response) {
                if (item.response.code > 0) {
                    item.ftoken = item.response.message.ftoken;
                    return true;
                }
                return false;
            }
            return true;
        }));
    }
}
ImgUploadComponent.decorators = [
    { type: Component, args: [{
                selector: 'lb-img',
                template: "<div>\n    <nz-upload nzListType=\"picture-card\" [(nzFileList)]=\"fileList\" [nzCustomRequest]=\"_uploadimg\"\n        [nzRemove]=\"_removeIMG\" [nzBeforeUpload]=\"_beforeUpload\" (nzChange)=\"_handleChange($event)\"\n        [nzShowButton]=\"nzShowButton\" [nzSize]=\"10*1024\" [nzShowUploadList]=\"showUploadList\"\n        [nzPreview]=\"_handlePreview\">\n        <div class=\"ant-upload-text\">\u4E0A\u4F20\u56FE\u7247</div>\n    </nz-upload>\n    <nz-modal [nzVisible]=\"_previewVisible\" [nzContent]=\"modalContent\" [nzFooter]=\"null\"\n        (nzOnCancel)=\"_previewVisible=false\">\n        <ng-template #modalContent>\n            <img [src]=\"_previewImage\" [ngStyle]=\"{ 'width': '100%' }\" />\n        </ng-template>\n    </nz-modal>\n</div>",
                styles: [""]
            }] }
];
/** @nocollapse */
ImgUploadComponent.ctorParameters = () => [
    { type: NzMessageService },
    { type: HttpService },
    { type: NzModalService }
];
ImgUploadComponent.propDecorators = {
    fileList: [{ type: Input }],
    nzShowButton: [{ type: Input }],
    showUploadList: [{ type: Input }],
    upload: [{ type: Input }],
    del: [{ type: Input }]
};
if (false) {
    /**
     * 图表列表
     * @type {?}
     */
    ImgUploadComponent.prototype.fileList;
    /** @type {?} */
    ImgUploadComponent.prototype._previewImage;
    /** @type {?} */
    ImgUploadComponent.prototype._previewVisible;
    /**
     * 上传按钮是否显示
     * @type {?}
     */
    ImgUploadComponent.prototype.nzShowButton;
    /**
     * 预览按钮以及删除按钮
     * @type {?}
     */
    ImgUploadComponent.prototype.showUploadList;
    /**
     * 上传事件
     * @type {?}
     */
    ImgUploadComponent.prototype.upload;
    /**
     * 删除事件
     * @type {?}
     */
    ImgUploadComponent.prototype.del;
    /** @type {?} */
    ImgUploadComponent.prototype._handlePreview;
    /**
     * 图片上传
     * @type {?}
     */
    ImgUploadComponent.prototype._uploadimg;
    /**
     * 移除文件
     * @type {?}
     */
    ImgUploadComponent.prototype._removeIMG;
    /** @type {?} */
    ImgUploadComponent.prototype._beforeUpload;
    /**
     * @type {?}
     * @private
     */
    ImgUploadComponent.prototype.msgsvr;
    /**
     * @type {?}
     * @private
     */
    ImgUploadComponent.prototype.lbservice;
    /**
     * @type {?}
     * @private
     */
    ImgUploadComponent.prototype.modalService;
}

/**
 * @fileoverview added by tsickle
 * Generated from: lib/components/popdetail/popdetail.component.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class PopdetailComponent {
    /**
     * @param {?} msgSrv
     * @param {?} lbservice
     */
    constructor(msgSrv, lbservice) {
        this.msgSrv = msgSrv;
        this.lbservice = lbservice;
        // 是否弹出
        this.isVisible = false;
        this.isVisibleChange = new EventEmitter();
        // 弹出框标题
        this.title = "明细框";
        // 字段信息
        this.schema = {};
        // 默认值
        this.formData = {};
        // 是否加载中
        this.loading = false;
        this.loadingChange = new EventEmitter();
        // 保存服务
        this.saveSname = '';
        // formchang事件
        this.formChange = new EventEmitter();
    }
    /**
     * @return {?}
     */
    ngOnInit() {
    }
    /**
     * @return {?}
     */
    handleCancel() {
        this.isVisible = false;
        this.isVisibleChange.emit(this.isVisible);
    }
    /**
     * @return {?}
     */
    show() {
        this.isVisible = true;
    }
    /**
     * @return {?}
     */
    hidde() {
        this.isVisible = true;
    }
    /**
     * @return {?}
     */
    reset() {
        this.sf.reset();
    }
    /**
     * @return {?}
     */
    value() {
        return this.sf.value;
    }
    // path 采用 / 来分隔，例如：/user/name
    /**
     * @param {?} path
     * @return {?}
     */
    getValue(path) {
        return this.sf.getValue(path);
    }
    /**
     * @param {?} path
     * @param {?} value
     * @return {?}
     */
    setValue(path, value) {
        this.sf.setValue(path, value);
    }
    /**
     * @return {?}
     */
    validator() {
        this.sf.validator();
    }
    /**
     * @param {?=} newSchema
     * @param {?=} newUI
     * @return {?}
     */
    refreshSchema(newSchema, newUI) {
        this.sf.refreshSchema(newSchema, newUI);
    }
    /**
     * @return {?}
     */
    getsf() {
        return this.sf;
    }
    /**
     * @param {?} val
     * @return {?}
     */
    save(val) {
        this.loading = true;
        this.loadingChange.emit(this.loading);
        if (this.saveSname && this.saveSname !== '') {
            this.lbservice.lbservice(this.saveSname, { para: val }, 500).then((/**
             * @param {?} resdata
             * @return {?}
             */
            (resdata) => {
                this.loading = false;
                this.loadingChange.emit(this.loading);
                if (resdata.code < 1) {
                    this.msgSrv.error(resdata.errmsg);
                    return;
                }
                if (this.saveFu && typeof this.saveFu === 'function') {
                    this.saveFu(val, resdata);
                }
                this.isVisible = false;
                this.isVisibleChange.emit(this.isVisible);
                this.reset();
                this.msgSrv.info("成功！");
            }));
        }
        else {
            if (this.saveFu && typeof this.saveFu === 'function') {
                this.saveFu(val, {});
            }
        }
    }
    /**
     * @param {?} value
     * @return {?}
     */
    change(value) {
        this.formChange.emit(value);
    }
}
PopdetailComponent.decorators = [
    { type: Component, args: [{
                selector: 'lb-pop',
                template: "<nz-modal [(nzVisible)]=\"isVisible\" [nzTitle]=\"title\" nzWidth=\"80%\" (nzOnCancel)=\"handleCancel()\"\n    (nzOnOk)=\"save(sf.value)\" [nzOkLoading]=\"loading\">\n\n    <sf #sf [schema]=\"schema\" [formData]=\"formData\" (formChange)=\"change($event)\" button=\"none\">\n\n    </sf>\n\n</nz-modal>",
                styles: [""]
            }] }
];
/** @nocollapse */
PopdetailComponent.ctorParameters = () => [
    { type: NzMessageService },
    { type: HttpService }
];
PopdetailComponent.propDecorators = {
    sf: [{ type: ViewChild, args: ['sf', { static: true },] }],
    isVisible: [{ type: Input }],
    isVisibleChange: [{ type: Output }],
    title: [{ type: Input }],
    schema: [{ type: Input }],
    formData: [{ type: Input }],
    loading: [{ type: Input }],
    loadingChange: [{ type: Output }],
    saveSname: [{ type: Input }],
    saveFu: [{ type: Input }],
    formChange: [{ type: Output }]
};
if (false) {
    /** @type {?} */
    PopdetailComponent.prototype.sf;
    /** @type {?} */
    PopdetailComponent.prototype.isVisible;
    /** @type {?} */
    PopdetailComponent.prototype.isVisibleChange;
    /** @type {?} */
    PopdetailComponent.prototype.title;
    /** @type {?} */
    PopdetailComponent.prototype.schema;
    /** @type {?} */
    PopdetailComponent.prototype.formData;
    /** @type {?} */
    PopdetailComponent.prototype.loading;
    /** @type {?} */
    PopdetailComponent.prototype.loadingChange;
    /** @type {?} */
    PopdetailComponent.prototype.saveSname;
    /** @type {?} */
    PopdetailComponent.prototype.saveFu;
    /** @type {?} */
    PopdetailComponent.prototype.formChange;
    /** @type {?} */
    PopdetailComponent.prototype.msgSrv;
    /**
     * @type {?}
     * @private
     */
    PopdetailComponent.prototype.lbservice;
}

/**
 * @fileoverview added by tsickle
 * Generated from: lib/lbf.module.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/** @type {?} */
const LbfComponents = [LbfComponent, GridComponent, DicPipe, ImgUploadComponent, PopdetailComponent, LbRowDirective];
class LbfModule {
}
LbfModule.decorators = [
    { type: NgModule, args: [{
                declarations: [...LbfComponents],
                imports: [
                    CommonModule,
                    NgZorroAntdModule,
                    RouterModule,
                    AlainThemeModule.forRoot(),
                    DelonABCModule,
                    DelonChartModule,
                    DelonACLModule,
                    DelonCacheModule,
                    DelonUtilModule,
                    DelonAuthModule,
                    DelonFormModule.forRoot(),
                ],
                exports: [...LbfComponents]
            },] }
];

/**
 * @fileoverview added by tsickle
 * Generated from: lib/lbf.service.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class LbfService {
    constructor() { }
}
LbfService.decorators = [
    { type: Injectable, args: [{
                providedIn: 'root'
            },] }
];
/** @nocollapse */
LbfService.ctorParameters = () => [];
/** @nocollapse */ LbfService.ngInjectableDef = ɵɵdefineInjectable({ factory: function LbfService_Factory() { return new LbfService(); }, token: LbfService, providedIn: "root" });

/**
 * @fileoverview added by tsickle
 * Generated from: lib/lblibs/gird.interceptor.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class GridInterceptor {
    /**
     * @param {?} injector
     */
    constructor(injector) {
        this.injector = injector;
    }
    /**
     * @param {?} req
     * @param {?} next
     * @return {?}
     */
    intercept(req, next) {
        // 统一加上服务端前缀
        /** @type {?} */
        let url = req.url;
        if (url !== "MohrssExInterface/gridsvr") {
            return next.handle(req);
        }
        url = this.sinfo.molssUrl;
        /** @type {?} */
        const body = req.body;
        /** @type {?} */
        const sname = body.sname;
        /** @type {?} */
        const sorts = {};
        /** @type {?} */
        const filters = {};
        /** @type {?} */
        const stacal = body.stacal;
        for (const p in body) {
            if (!"sname,pi,ps,sort,form,stacal,".includes(p + ",")) {
                /** @type {?} */
                const v = body[p];
                filters[p] = v;
            }
        }
        /** @type {?} */
        const sortstr = body.sort;
        if (sortstr) {
            // AAE070.descend-AKC258.descend
            /** @type {?} */
            const ss = sortstr.split("-");
            for (const s of ss) {
                /** @type {?} */
                const sort = s.split(".");
                if (sort.length > 1 && sort[0] && sort[1]) {
                    sorts[sort[0]] = sort[1];
                }
            }
        }
        /** @type {?} */
        const para = body.form;
        /** @type {?} */
        const newbody = {
            para,
            page: { PAGESIZE: body.ps, PAGEIDX: body.pi, SORT: sorts, FILTER: filters, STACAL: stacal },
        };
        /** @type {?} */
        const headers = req.headers.set("SNAME", sname);
        /** @type {?} */
        const newReq = req.clone({
            url, headers, body: newbody
        });
        return next.handle(newReq).pipe(mergeMap((/**
         * @param {?} event
         * @return {?}
         */
        (event) => {
            if (event instanceof HttpResponseBase) {
                this.handleData(event);
            }
            return of(event);
        })));
    }
    /**
     * @return {?}
     */
    get sinfo() {
        return this.injector.get(ServiceInfo);
    }
    /**
     * @return {?}
     */
    get msg() {
        return this.injector.get(NzMessageService);
    }
    /**
     * @private
     * @param {?} ev
     * @return {?}
     */
    handleData(ev) {
        if (ev instanceof HttpResponse) {
            /** @type {?} */
            const body = ev.body;
            if (body) {
                if (body.code < 1) {
                    this.msg.error(body.errmsg);
                }
                else {
                    // 添加序号
                    /** @type {?} */
                    const list = body.message.list;
                    if (list) {
                        list.map((/**
                         * @param {?} row
                         * @param {?} index
                         * @return {?}
                         */
                        (row, index) => row._idx = index));
                    }
                }
            }
        }
        return of(ev);
    }
}
GridInterceptor.decorators = [
    { type: Injectable }
];
/** @nocollapse */
GridInterceptor.ctorParameters = () => [
    { type: Injector }
];
if (false) {
    /**
     * @type {?}
     * @private
     */
    GridInterceptor.prototype.injector;
}

/**
 * @fileoverview added by tsickle
 * Generated from: lib/lblibs/lbfw.interceptor.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/** @type {?} */
const CODEMESSAGE = {
    200: '服务器成功返回请求的数据。',
    201: '新建或修改数据成功。',
    202: '一个请求已经进入后台排队（异步任务）。',
    204: '删除数据成功。',
    400: '发出的请求有错误，服务器没有进行新建或修改数据的操作。',
    401: '用户未登录或者登录已过期。',
    403: '用户得到授权，但是访问是被禁止的。',
    404: '发出的请求针对的是不存在的记录，服务器没有进行操作。',
    406: '请求的格式不可得。',
    410: '请求的资源被永久删除，且不会再得到的。',
    422: '当创建一个对象时，发生一个验证错误。',
    500: '服务器发生错误，请检查服务器。',
    502: '网关错误。',
    503: '服务不可用，服务器暂时过载或维护。',
    504: '网关超时。',
};
/**
 * 默认HTTP拦截器，其注册细节见 `app.module.ts`
 */
class LbfwInterceptor {
    /**
     * @param {?} injector
     */
    constructor(injector) {
        this.injector = injector;
    }
    /**
     * @return {?}
     */
    get msg() {
        return this.injector.get(NzMessageService);
    }
    /**
     * @private
     * @param {?} url
     * @return {?}
     */
    goTo(url) {
        setTimeout((/**
         * @return {?}
         */
        () => this.injector.get(Router).navigateByUrl(url)));
    }
    /**
     * @return {?}
     */
    get sinfo() {
        return this.injector.get(ServiceInfo);
    }
    /**
     * @private
     * @param {?} ev
     * @return {?}
     */
    checkStatus(ev) {
        if (ev.status >= 200 && ev.status < 300)
            return;
        /** @type {?} */
        const errortext = CODEMESSAGE[ev.status] || ev.statusText;
        this.injector.get(NzNotificationService).error(`请求错误 ${ev.status}`, errortext);
    }
    /**
     * @private
     * @param {?} ev
     * @return {?}
     */
    handleData(ev) {
        // 可能会因为 `throw` 导出无法执行 `_HttpClient` 的 `end()` 操作
        if (ev.status > 0) {
            this.injector.get(_HttpClient).end();
        }
        this.checkStatus(ev);
        // 业务处理：一些通用操作
        switch (ev.status) {
            case 200:
                // 处理未登录的错误 {code: -322, errmsg: "未登录或者登陆已经过期！"}
                if (ev instanceof HttpResponse) {
                    /** @type {?} */
                    const body = ev.body;
                    if (body && body.code === -322) {
                        this.msg.error("未登录或者登陆已经过期！");
                        // 继续抛出错误中断后续所有 Pipe、subscribe 操作，因此：
                        // this.http.get('/').subscribe() 并不会触发
                        this.goTo('/passport/login');
                        break;
                    }
                    else {
                        // 重新修改 `body` 内容为 `response` 内容，对于绝大多数场景已经无须再关心业务状态码
                        // 或者依然保持完整的格式
                        return of(ev);
                    }
                }
                break;
            case 401: // 未登录状态码
                // 请求错误 401: https://preview.pro.ant.design/api/401 用户没有权限（令牌、用户名、密码错误）。
                ((/** @type {?} */ (this.injector.get(DA_SERVICE_TOKEN)))).clear();
                this.goTo('/passport/login');
                break;
            case 403:
            case 404:
            case 500:
                this.goTo(`/exception/${ev.status}`);
                break;
            default:
                if (ev instanceof HttpErrorResponse) {
                    console.warn('未可知错误，大部分是由于后端不支持CORS或无效配置引起', ev);
                    return throwError(ev);
                }
                break;
        }
        return of(ev);
    }
    /**
     * @param {?} req
     * @param {?} next
     * @return {?}
     */
    intercept(req, next) {
        // 统一加上服务端前缀
        /** @type {?} */
        const url = this.sinfo.baseUrl + req.url;
        // if (!url.startsWith('https://') && !url.startsWith('http://')) {
        //   url = environment.SERVER_URL + url;
        // }
        // 加上统一的请求参数
        /** @type {?} */
        const sname = req.headers.get("SNAME");
        /** @type {?} */
        const para = JSON.stringify(req.body);
        /** @type {?} */
        const appid = this.sinfo.APPID;
        /** @type {?} */
        const timestamp = this.sinfo.getTimestamp().toString();
        /** @type {?} */
        const sign = this.sinfo.getSign(sname, timestamp, para);
        /** @type {?} */
        const token = this.sinfo.LOGTOKEN;
        /** @type {?} */
        const version = this.sinfo.VERSION;
        /** @type {?} */
        const headers = req.headers.set("APPID", appid).set("TIMESTAMP", timestamp).set("SIGN", sign).set("LOGINTOKEN", token).set("VERSION", version);
        /** @type {?} */
        const newReq = req.clone({
            url, headers
        });
        return next.handle(newReq).pipe(mergeMap((/**
         * @param {?} event
         * @return {?}
         */
        (event) => {
            // 允许统一对请求错误处理
            if (event instanceof HttpResponseBase)
                return this.handleData(event);
            // 若一切都正常，则后续操作
            return of(event);
        })), catchError((/**
         * @param {?} err
         * @return {?}
         */
        (err) => this.handleData(err))));
    }
}
LbfwInterceptor.decorators = [
    { type: Injectable }
];
/** @nocollapse */
LbfwInterceptor.ctorParameters = () => [
    { type: Injector }
];
if (false) {
    /**
     * @type {?}
     * @private
     */
    LbfwInterceptor.prototype.injector;
}

/**
 * @fileoverview added by tsickle
 * Generated from: public-api.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */

/**
 * @fileoverview added by tsickle
 * Generated from: lbf.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */

export { DicPipe, GridComponent, GridInterceptor, HttpService, ImgUploadComponent, LbRowDirective, LbRowSource, LbfComponent, LbfModule, LbfService, LbfwInterceptor, PopdetailComponent, ServiceInfo, SupDic, SupExcel };
//# sourceMappingURL=lbf.js.map
